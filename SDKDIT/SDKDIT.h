//
//  SDKDIT.h
//  SDKDIT
//
//  Created by Александр Путинцев on 07.05.2018.
//  Copyright © 2018 Александр Путинцев. All rights reserved.
//

#import <UIKit/UIKit.h>

// Handling response:
// 1. normal (statusCode from doc (include 200)) - response format in doc
// 2. redirect (statusCode == 303) - response format: {"LocationPost": "https://...", "MD": "...", "PaReq": "...", "TermUrl": "..."}
// 3. error (request error, timeout, json parsing, ...) - check NSError

@interface SDKDIT : NSObject

@property (nonatomic) BOOL develEnvironment;    // YES - devel env, NO - stable env (default)
@property (nonatomic) BOOL debugMode;           // YES - enable debug logging, default NO

// GET /card/feature/{bin}
-(void)getBank:(NSString*)bin
      complete:(void(^)(NSInteger statusCode, id response, NSError* error))block;

// GET /payments/{batchId}
-(void)getPayments:(NSString*)batchId
          complete:(void(^)(NSInteger statusCode, id response, NSError* error))block;

// PUT /payments/{batchId}
-(void)startPayment:(NSString*)batchId
               body:(NSDictionary*)body
           complete:(void(^)(NSInteger statusCode, id response, NSError* error))block;

// GET /payments/status/{batchId}
-(void)getPaymentStatus:(NSString*)batchId
               complete:(void(^)(NSInteger statusCode, id response, NSError* error))block;

// GET /card/{batchId}
-(void)getCards:(NSString*)batchId
       complete:(void(^)(NSInteger statusCode, id response, NSError* error))block;

// PUT /card/{cardId}/{batchId}
-(void)renameCard:(NSString*)cardId
            batch:(NSString*)batchId
             body:(NSDictionary*)body
         complete:(void(^)(NSInteger statusCode, id response, NSError* error))block;

// DELETE /card/{cardId}/{batchId}
-(void)removeCard:(NSString*)cardId
            batch:(NSString*)batchId
         complete:(void(^)(NSInteger statusCode, id response, NSError* error))block;


// required 'Privacy - Camera usage description' key in Info.plist
// expiryYear: The full four digit year.
// expiryMonth: January == 1
// expiryMonth & expiryYear may be 0, if expiry information was not requested.
-(void)scanCardWithViewController:(UIViewController*)vc
                         complete:(void(^)(NSString* cardNumber, NSUInteger expiryYear, NSUInteger expiryMonth))complete;

@end


Pod::Spec.new do |s|
  s.name             = 'SDKDIT'
  s.version          = '0.1.0'
  s.summary          = 'MediaCom DIT SDK'
  s.description      = 'MediaCom DIT SDK package.'
  s.homepage         = 'https://bitbucket.org/teammediacom/sdkditru_ios/'
  s.license          = { :type => 'MIT' }
  s.author           = { 'MediaCom' => 'mdcm@mdcm.co' }
  s.source           = { :git => 'https://putintsev@bitbucket.org/teammediacom/sdkditru_ios.git', :tag => s.version.to_s }
  
  s.ios.deployment_target = '9.0'

  s.source_files = '**/*.h'
  s.vendored_libraries = 'libMediaComSDKDIT.a'

  s.resource_bundles = {
    'SDKDITLib' => ['*.pfx']
  }

  # s.frameworks = 'UIKit', 'MapKit'
  
  s.dependency 'CardIO', '~> 5.4'

end

//
//  TableViewDataSourceStructureTests.swift
//  Portable
//
//  Created by Ivan Erasov on 31.01.17.
//  Copyright © 2015. All rights reserved.
//

import XCTest
@testable import Mobile_Mos_ru

class TableViewDataSourceStructureTests: XCTestCase {

    func testAppendSection () {
        
        //given
        let dataStruct = TableViewDataSourceStructure()
        dataStruct.appendSection()
        dataStruct.appendSection()
        dataStruct.appendSection()

        //when
        let numberOfSections = dataStruct.numberOfSections()
        
        //then
        XCTAssertEqual(numberOfSections, 3)
        XCTAssertTrue(dataStruct.isEmpty())
    }
    
    func testEmptyStructure() {
        
        //given
        let dataStruct = TableViewDataSourceStructure()

        //when
        let numberOfSections = dataStruct.numberOfSections()

        //then
        XCTAssertEqual(numberOfSections, 0)
        XCTAssertTrue(dataStruct.isEmpty())
    }
    
    func testAppendSectionWithHeader() {
        
        //given
        let dataStruct = TableViewDataSourceStructure()
        let headerObject = MockHeader()
        dataStruct.appendSection(with: headerObject)
        
        //when
        let numberOfSections = dataStruct.numberOfSections()
        let gottenHeader = dataStruct.headerObject(for: 0) as! MockHeader
        
        //then
        XCTAssertEqual(headerObject, gottenHeader)
        XCTAssertEqual(numberOfSections, 1)
    }
    
    func testAppendCellObject() {
        
        //given
        let dataStruct = TableViewDataSourceStructure()
        dataStruct.appendSection()
        
        let cell1 = MockCellObject()
        let cell2 = MockCellObject()
        dataStruct.appendCellObject(cell1)
        dataStruct.appendCellObject(cell2)
        
        //when
        let numberOfCells = dataStruct.numberOfObjects(at: 0)
        let obj1 = dataStruct.cellObject(at: IndexPath(row: 0, section: 0)) as! MockCellObject
        let obj2 = dataStruct.cellObject(at: IndexPath(row: 1, section: 0)) as! MockCellObject
        
        //then
        XCTAssertEqual(numberOfCells, 2)
        XCTAssertEqual(obj1, cell1)
        XCTAssertEqual(obj2, cell2)
    }
    
    func testCellObjectAtOutOfBoundsIndexPath() {
        
        //given
        let dataStruct = TableViewDataSourceStructure()
        dataStruct.appendSection()
        
        let cell1 = MockCellObject()
        let cell2 = MockCellObject()
        dataStruct.appendCellObject(cell1)
        dataStruct.appendCellObject(cell2)
        
        //when
        let cellObject = dataStruct.cellObject(at: IndexPath(row: 0, section: 3))
        let cellObject2 = dataStruct.cellObject(at: IndexPath(row: 2, section: 0))

        //then
        XCTAssertNil(cellObject)
        XCTAssertNil(cellObject2)
    }
    
    func testIsEmpty() {
        
        //given
        let dataStruct1 = TableViewDataSourceStructure()
        dataStruct1.appendSection()
        
        let dataStruct2 = TableViewDataSourceStructure()
        dataStruct2.appendSection()
        dataStruct2.appendSection()
        
        let cell1 = MockCellObject()
        let cell2 = MockCellObject()
        dataStruct2.appendCellObject(cell1)
        dataStruct2.appendCellObject(cell2)
        
        //when
        let isEmpty1 = dataStruct1.isEmpty()
        let isEmpty2 = dataStruct2.isEmpty()
        
        //then
        XCTAssertTrue(isEmpty1)
        XCTAssertFalse(isEmpty2)
    }
    
    func testIndexPathOfFirstObjectPassing() {
        
        //given
        let identifier = "test"
        
        let dataStruct = TableViewDataSourceStructure()
        dataStruct.appendSection()
        
        let cell11 = MockCellObject()
        let cell12 = MockCellObject()
        dataStruct.appendCellObject(cell11)
        dataStruct.appendCellObject(cell12)
        
        dataStruct.appendSection()
        
        let cell21 = MockCellObject()
        let cell22 = MockCellObjectWithId(with: identifier)
        let cell23 = MockCellObject()
        dataStruct.appendCellObject(cell21)
        dataStruct.appendCellObject(cell22)
        dataStruct.appendCellObject(cell23)
        
        //when
        let indexPath = dataStruct.indexPathOfFirstObjectPassing { cell in
            
            guard let cellWithId = cell as? CellObjectWithId else {
                return false
            }
            
            return cellWithId.itemId == identifier
        }
        
        //then
        XCTAssertNotNil(indexPath)
        XCTAssertEqual(indexPath!.row, 1)
        XCTAssertEqual(indexPath!.section, 1)
    }
    
    func testEnumerateCellObjectsUsing() {
        
        //given
        let oldIdentifier = "test"
        let newIdentifier = "new"
        
        let dataStruct = TableViewDataSourceStructure()
        dataStruct.appendSection()
        
        let cell11 = MockCellObjectWithId(with: oldIdentifier)
        let cell12 = MockCellObject()
        dataStruct.appendCellObject(cell11)
        dataStruct.appendCellObject(cell12)
        
        dataStruct.appendSection()
        
        let cell21 = MockCellObjectWithId(with: oldIdentifier)
        let cell22 = MockCellObjectWithId(with: oldIdentifier)
        let cell23 = MockCellObjectWithId(with: oldIdentifier)
        dataStruct.appendCellObject(cell21)
        dataStruct.appendCellObject(cell22)
        dataStruct.appendCellObject(cell23)
        
        //when
        dataStruct.enumerateCellObjectsUsing { cell, _ in
            
            guard let cellWithId = cell as? MockCellObjectWithId else {
                return
            }
            
            cellWithId.itemId = newIdentifier
        }
        
        //then
        for section in 0 ..< dataStruct.numberOfSections() {
            for row in 0 ..< dataStruct.numberOfObjects(at: section) {
                
                let indexPath = IndexPath(row: row, section: section)
                
                if let cellWithId = dataStruct.cellObject(at: indexPath) as? CellObjectWithId {
                    XCTAssertEqual(cellWithId.itemId, newIdentifier)
                }
            }
        }
    }
    
    func testAppendCellObjectWithoutSection() {
        
        //given
        let dataStruct = TableViewDataSourceStructure()
        
        let cell1 = MockCellObject()
        let cell2 = MockCellObject()
        dataStruct.appendCellObject(cell1)
        dataStruct.appendCellObject(cell2)
        
        //when
        let numberOfCells = dataStruct.numberOfObjects(at: 0)
        let obj = dataStruct.cellObject(at: IndexPath(row: 0, section: 0)) as! MockCellObject
        let obj2 = dataStruct.cellObject(at: IndexPath(row: 1, section: 0)) as! MockCellObject
        
        //then
        XCTAssertEqual(numberOfCells, 2)
        XCTAssertEqual(obj, cell1)
        XCTAssertEqual(obj2, cell2)
        
    }
    
    func testAddCellObjectTo() {
        
        //given
        let dataStruct = TableViewDataSourceStructure()
        dataStruct.appendSection()
        
        let cell11 = MockCellObject()
        let cell12 = MockCellObject()
        dataStruct.appendCellObject(cell11)
        dataStruct.appendCellObject(cell12)
        
        dataStruct.appendSection()
        
        let cell21 = MockCellObject()
        let cell22 = MockCellObject()
        let cell23 = MockCellObject()
        dataStruct.appendCellObject(cell21)
        dataStruct.appendCellObject(cell22)
        dataStruct.appendCellObject(cell23)
        
        let cellTo22 = MockCellObject()
        let indexPath22 = IndexPath(row: 1, section: 1)
        let cell31 = MockCellObject()
        let indexPath31 = IndexPath(row: 0, section: 2)
        
        //when
        dataStruct.addCellObject(cellTo22, to: indexPath22)
        dataStruct.addCellObject(cell31, to: indexPath31)
        //then
        XCTAssertEqual(dataStruct.cellObject(at: indexPath22) as! MockCellObject, cellTo22)
        XCTAssertEqual(dataStruct.cellObject(at: indexPath31) as! MockCellObject, cell31)
    }
    
    func testRemoveCellObject() {
        
        //given
        let dataStruct = TableViewDataSourceStructure()
        dataStruct.appendSection()
        
        let cell11 = MockCellObject()
        let cell12 = MockCellObject()
        dataStruct.appendCellObject(cell11)
        dataStruct.appendCellObject(cell12)
        
        dataStruct.appendSection()
        
        let cell21 = MockCellObject()
        let cell22 = MockCellObject()
        let cell23 = MockCellObject()
        dataStruct.appendCellObject(cell21)
        dataStruct.appendCellObject(cell22)
        dataStruct.appendCellObject(cell23)
        
        //when
        let removedCell = dataStruct.removeCellObject(at: IndexPath(row: 1, section: 1)) as! MockCellObject
        
        //then
        XCTAssertEqual(removedCell, cell22)
    }
    
    func testReplaceCellObject() {
        
        //given
        let dataStruct = TableViewDataSourceStructure()
        dataStruct.appendSection()
        
        let cell11 = MockCellObject()
        let cell12 = MockCellObject()
        dataStruct.appendCellObject(cell11)
        dataStruct.appendCellObject(cell12)
        
        dataStruct.appendSection()
        
        let cell21 = MockCellObject()
        let cell22 = MockCellObject()
        let cell23 = MockCellObject()
        dataStruct.appendCellObject(cell21)
        dataStruct.appendCellObject(cell22)
        dataStruct.appendCellObject(cell23)
        
        let newCell22 = MockCellObject()
        let indexPath22 = IndexPath(row: 1, section: 1)
        
        //when
        dataStruct.replaceCellObject(at: indexPath22, with: newCell22)
        
        //then
        XCTAssertEqual(dataStruct.cellObject(at: indexPath22) as! MockCellObject, newCell22)
    }
    
    func testAppendHeader() {
        
        //given
        let dataStruct = TableViewDataSourceStructure()
        
        dataStruct.appendSection()
        dataStruct.appendSection()
        dataStruct.appendSection()
        
        let headerObject = MockHeader()
        dataStruct.appendHeaderObject(headerObject)
        
        let firstHeader = MockHeader()
        dataStruct.addHeaderObject(firstHeader, to: 0)
        
        //when
        let header1 = dataStruct.headerObject(for: 2) as! MockHeader
        let header2 = dataStruct.headerObject(for: 0) as! MockHeader
        
        //then
        XCTAssertEqual(headerObject, header1)
        XCTAssertEqual(firstHeader, header2)
    }
    
    func testNotExistHeader() {
        
        //given
        let dataStruct = TableViewDataSourceStructure()
        
        dataStruct.appendSection()
        dataStruct.appendSection()
        
        let headerObject = MockHeader()
        dataStruct.appendHeaderObject(headerObject)
        
        //when
        let header = dataStruct.headerObject(for: 0)
        
        //then
        XCTAssertNil(header)
    }
    
    func testAppendFooter() {
        
        //given
        let dataStruct = TableViewDataSourceStructure()
        
        dataStruct.appendSection()
        dataStruct.appendSection()
        dataStruct.appendSection()
        
        let footerObject = MockFooter()
        dataStruct.appendFooterObject(footerObject)
        
        let firstFooter = MockFooter()
        dataStruct.addFooterObject(firstFooter, to: 0)
        
        //when
        let footer1 = dataStruct.footerObject(for: 2) as! MockFooter
        let footer2 = dataStruct.footerObject(for: 0) as! MockFooter
        
        //then
        XCTAssertEqual(footerObject, footer1)
        XCTAssertEqual(firstFooter, footer2)
    }
    
    func testNotExistFooter() {
        
        //given
        let dataStruct = TableViewDataSourceStructure()
        
        dataStruct.appendSection()
        dataStruct.appendSection()
        
        let footerObject = MockFooter()
        dataStruct.appendFooterObject(footerObject)
        
        //when
        let footer = dataStruct.footerObject(for: 0)
        
        //then
        XCTAssertNil(footer)
    }
    
    func testAppendSectionWithCellObjects() {
        
        //given
        let dataStruct = TableViewDataSourceStructure()
        
        let cell1 = MockCellObject()
        let cell2 = MockCellObject()
        dataStruct.appendSection()
        dataStruct.appendSection(with: [cell1, cell2])
        
        //when
        let numberOfSections = dataStruct.numberOfSections()
        let obj = dataStruct.cellObject(at: IndexPath(row: 0, section: 1)) as! MockCellObject
        let obj2 = dataStruct.cellObject(at: IndexPath(row: 1, section: 1)) as! MockCellObject
        
        //then
        XCTAssertEqual(numberOfSections, 2)
        XCTAssertEqual(obj, cell1)
        XCTAssertEqual(obj2, cell2)
    }
    
    
    func testAppendSectionWithHeaderCellObjects() {
        
        //given
        let dataStruct = TableViewDataSourceStructure()
        
        let cell1 = MockCellObject()
        let cell2 = MockCellObject()
        
        let headerObject = MockHeader()
        dataStruct.appendSection()
        dataStruct.appendSection(with: headerObject, and: [cell1, cell2])
        
        //when
        let numberOfSections = dataStruct.numberOfSections()
        let obj = dataStruct.cellObject(at: IndexPath(row: 0, section: 1)) as! MockCellObject
        let obj2 = dataStruct.cellObject(at: IndexPath(row: 1, section: 1)) as! MockCellObject
        let header = dataStruct.headerObject(for: 1) as! MockHeader
        
        //then
        XCTAssertEqual(numberOfSections, 2)
        XCTAssertEqual(obj, cell1)
        XCTAssertEqual(obj2, cell2)
        XCTAssertEqual(header, headerObject)
    }
    
    func testCellObject() {
        
        //given
        let cellObject = MockCellObject()
        let cellObjectType = type(of: cellObject)
        
        //when
        let reuseId = cellObjectType.cellReuseIdentifier()
        
        //then
        XCTAssertEqual(reuseId, String(describing: MockCellObject.self))
    }
    
    func testHeaderObject() {
        
        //given
        let headerObject = MockHeader()
        let headerObjectType = type(of: headerObject)
        
        //when
        let reuseId = headerObjectType.headerReuseIdentifier()
        
        //then
        XCTAssertEqual(reuseId, String(describing: MockHeader.self))
    }
    
    func testFooterObject() {
        
        //given
        let footerObject = MockFooter()
        let footerObjectType = type(of: footerObject)
        
        //when
        let reuseId = footerObjectType.footerReuseIdentifier()
        
        //then
        XCTAssertEqual(reuseId, String(describing: MockFooter.self))
    }
    
    func testCellObjectNibName() {
        
        //given
        let cellObject1 = MockCellObject()
        let cellObjectType1 = type(of: cellObject1)
        let expectedCellNibName1 = "MockCell"
        
        let cellObject2 = MockCell()
        let cellObjectType2 = type(of: cellObject2)
        let expectedCellNibName2 = "MockCell"
        
        //when
        let nibName1 = cellObjectType1.cellNibName()
        let nibName2 = cellObjectType2.cellNibName()
        
        //then
        XCTAssertEqual(nibName1, expectedCellNibName1)
        XCTAssertEqual(nibName2, expectedCellNibName2)
    }
    
    func testHeaderObjectNibName() {
        
        //given
        let headerObject1 = MockHeaderObject()
        let headerObjectType1 = type(of: headerObject1)
        let expectedHeaderNibName1 = "MockHeader"
        
        let headerObject2 = MockHeader()
        let headerObjectType2 = type(of: headerObject2)
        let expectedHeaderNibName2 = "MockHeader"
        
        //when
        let nibName1 = headerObjectType1.headerNibName()
        let nibName2 = headerObjectType2.headerNibName()
        
        //then
        XCTAssertEqual(nibName1, expectedHeaderNibName1)
        XCTAssertEqual(nibName2, expectedHeaderNibName2)
    }
    
    func testFooterObjectNibName() {
        
        //given
        let footerObject1 = MockFooterObject()
        let footerObjectType1 = type(of: footerObject1)
        let expectedFooterNibName1 = "MockFooter"
        
        let footerObject2 = MockFooter()
        let footerObjectType2 = type(of: footerObject2)
        let expectedFooterNibName2 = "MockFooter"
        
        //when
        let nibName1 = footerObjectType1.footerNibName()
        let nibName2 = footerObjectType2.footerNibName()
        
        //then
        XCTAssertEqual(nibName1, expectedFooterNibName1)
        XCTAssertEqual(nibName2, expectedFooterNibName2)
    }
}

extension TableViewDataSourceStructureTests {
    
    //наследуем NSObject, чтобы работал XCTAssertEqual

    class MockHeader: NSObject, HeaderObject {}
    
    class MockHeaderObject: NSObject, HeaderObject {}
    
    class MockFooter: NSObject, FooterObject {}
    
    class MockFooterObject: NSObject, FooterObject {}
    
    class MockCell: NSObject, CellObject {}
    
    class MockCellObject: NSObject, CellObject {}
    
    class MockCellObjectWithId: NSObject, CellObjectWithId {
        
        var itemId: String
        
        init(with identifier: String) {
            itemId = identifier
        }
    }
}


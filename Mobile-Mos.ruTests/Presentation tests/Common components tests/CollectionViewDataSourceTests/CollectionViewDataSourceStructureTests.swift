//
//  CollectionViewDataSourceStructureTests.swift
//  Portable
//
//  Created by Ivan Erasov on 22.01.16.
//  Copyright © 2016. All rights reserved.
//

import XCTest
@testable import Mobile_Mos_ru

class CollectionViewDataSourceStructureTests: XCTestCase {
    
    func testCellObjectAtIndexPath() {
        
        //given
        let dataStructure = CollectionViewDataSourceStructure()
        
        let expectedIdentifier1 = "1"
        let expectedIdentifier2 = "2"
        
        dataStructure.appendSection(with: [CellObjectMock(identifier: expectedIdentifier1), CellObjectMock(identifier: "")])
        dataStructure.appendSection(with: [CellObjectMock(identifier: expectedIdentifier2)])
        
        //when
        let cellObject1 = dataStructure.cellObject(at: IndexPath(row: 0, section: 0)) as? CellObjectMock
        let cellObject2 = dataStructure.cellObject(at: IndexPath(row: 0, section: 1)) as? CellObjectMock
        let cellObject3 = dataStructure.cellObject(at: IndexPath(row: 0, section: 2))
        
        //then
        XCTAssertEqual(cellObject1!.itemId, expectedIdentifier1)
        XCTAssertEqual(cellObject2!.itemId, expectedIdentifier2)
        XCTAssertNil(cellObject3)
    }
    
    func testIsEmpty() {
        
        //given
        let dataStruct1 = CollectionViewDataSourceStructure()
        dataStruct1.appendSection()
        
        let dataStruct2 = CollectionViewDataSourceStructure()
        dataStruct2.appendSection()
        dataStruct2.appendSection()
        
        let cell1 = CellObjectMock(identifier: "")
        let cell2 = CellObjectMock(identifier: "")
        dataStruct2.appendCellObject(cell1)
        dataStruct2.appendCellObject(cell2)
        
        //when
        let isEmpty1 = dataStruct1.isEmpty()
        let isEmpty2 = dataStruct2.isEmpty()
        
        //then
        XCTAssertTrue(isEmpty1)
        XCTAssertFalse(isEmpty2)
    }
    
    func testIndexPathOfFirstObjectPassingTest() {
        
        //given
        let dataStructure = CollectionViewDataSourceStructure()
        
        let expectedIdentifier = "1"
        let notExpectedIdentifier = "2"
        
        let cellObjects = [CellObjectMock(identifier: ""), CellObjectMock(identifier: expectedIdentifier), CellObjectMock(identifier: ""), CellObjectMock(identifier: ""), CellObjectMock(identifier: "")]
        dataStructure.appendSection(with: cellObjects)
        
        //when
        let indexPath1 = dataStructure.indexPathOfFirstObjectPassing { object in
            
            if let objectMock = object as? CellObjectMock {
                return objectMock.itemId == expectedIdentifier
            }
            
            return false
        }
        
        let indexPath2 = dataStructure.indexPathOfFirstObjectPassing { object in
            
            if let objectMock = object as? CellObjectMock {
                return objectMock.itemId == notExpectedIdentifier
            }
            
            return false
        }
        
        //then
        XCTAssertEqual(indexPath1!.row, 1)
        XCTAssertEqual(indexPath1!.section, 0)
        XCTAssertNil(indexPath2)
    }
    
    func testRemoveCellObject() {
        
        //given
        let dataStruct = CollectionViewDataSourceStructure()
        dataStruct.appendSection()
        
        let cell11 = CellObjectMock(identifier: "")
        let cell12 = CellObjectMock(identifier: "")
        dataStruct.appendCellObject(cell11)
        dataStruct.appendCellObject(cell12)
        
        dataStruct.appendSection()
        
        let cell21 = CellObjectMock(identifier: "")
        let cell22 = CellObjectMock(identifier: "removing")
        let cell23 = CellObjectMock(identifier: "")
        dataStruct.appendCellObject(cell21)
        dataStruct.appendCellObject(cell22)
        dataStruct.appendCellObject(cell23)
        
        //when
        let removedCell = dataStruct.removeCellObject(at: IndexPath(row: 1, section: 1)) as! CellObjectMock
        
        //then
        XCTAssertEqual(removedCell.itemId, cell22.itemId)
    }
    
    func testReplaceCellObject() {
        
        //given
        let dataStructure = CollectionViewDataSourceStructure()
        
        let expectedIdentifier = "1"
        let newIdentifier = "2"
        
        let cellObjects = [CellObjectMock(identifier: ""), CellObjectMock(identifier: expectedIdentifier), CellObjectMock(identifier: ""), CellObjectMock(identifier: ""), CellObjectMock(identifier: "")]
        dataStructure.appendSection(with: cellObjects)
        
        let newObject = CellObjectMock(identifier: newIdentifier)
        let indexPath = IndexPath(row: 1, section: 0)
        
        //when
        dataStructure.replaceCellObject(at: indexPath, with: newObject)
        
        //then
        XCTAssertEqual(newIdentifier, (dataStructure.cellObject(at: indexPath) as! CellObjectMock).itemId)
    }
    
    func testHeaderObjectForSection() {
        
        //given
        let dataStructure = CollectionViewDataSourceStructure()
        
        let expectedIdentifier1 = "1"
        let expectedIdentifier2 = "2"
        
        dataStructure.appendSection()
        dataStructure.appendHeaderObject(HeaderObjectMock(identifier: expectedIdentifier1))
        
        dataStructure.appendSection()
        
        dataStructure.appendSection()
        dataStructure.appendHeaderObject(HeaderObjectMock(identifier: expectedIdentifier2))
        
        //when
        let headerObject1 = dataStructure.headerObject(for: 0) as? HeaderObjectMock
        let headerObject2 = dataStructure.headerObject(for: 1)
        let headerObject3 = dataStructure.headerObject(for: 2) as? HeaderObjectMock
        
        //then
        XCTAssertEqual(headerObject1!.itemId, expectedIdentifier1)
        XCTAssertNil(headerObject2)
        XCTAssertEqual(headerObject3!.itemId, expectedIdentifier2)
    }
    
    func testFooterObjectForSection() {
        
        //given
        let dataStructure = CollectionViewDataSourceStructure()
        
        let expectedIdentifier1 = "1"
        let expectedIdentifier2 = "2"
        
        dataStructure.appendSection()
        dataStructure.appendFooterObject(FooterObjectMock(identifier: expectedIdentifier1))
        
        dataStructure.appendSection()
        
        dataStructure.appendSection()
        dataStructure.appendFooterObject(FooterObjectMock(identifier: expectedIdentifier2))
        
        //when
        let footerObject1 = dataStructure.footerObject(for: 0) as? FooterObjectMock
        let footerObject2 = dataStructure.footerObject(for: 1)
        let footerObject3 = dataStructure.footerObject(for: 2) as? FooterObjectMock
        
        //then
        XCTAssertEqual(footerObject1!.itemId, expectedIdentifier1)
        XCTAssertNil(footerObject2)
        XCTAssertEqual(footerObject3!.itemId, expectedIdentifier2)
    }
    
    func testNumberOfSections() {
        
        //given
        let dataStructure = CollectionViewDataSourceStructure()
        
        dataStructure.appendSection(with: [CellObjectMock(identifier: ""), CellObjectMock(identifier: "")])
        dataStructure.appendSection(with: [CellObjectMock(identifier: "")])
        
        //when
        let sectionCount = dataStructure.numberOfSections()
        
        //then
        XCTAssertEqual(sectionCount, 2)
    }
    
    func testNumberOfObjectsAtSection() {
        
        //given
        let dataStructure = CollectionViewDataSourceStructure()
        
        dataStructure.appendSection(with: [CellObjectMock(identifier: ""), CellObjectMock(identifier: "")])
        dataStructure.appendSection(with: [CellObjectMock(identifier: "")])
        
        //when
        let count1 = dataStructure.numberOfObjects(at: 0)
        let count2 = dataStructure.numberOfObjects(at: 1)
        let count3 = dataStructure.numberOfObjects(at: 2)
        
        //then
        XCTAssertEqual(count1, 2)
        XCTAssertEqual(count2, 1)
        XCTAssertEqual(count3, 0)
    }
    
    func testAppendSection() {
        
        //given
        let dataStructure = CollectionViewDataSourceStructure()
        let countBefore = dataStructure.numberOfSections()
        
        //when
        dataStructure.appendSection()
        
        //then
        XCTAssertEqual(countBefore + 1, dataStructure.numberOfSections())
    }
    
    func testAppendSectionWithHeader() {
        
        //given
        let dataStructure = CollectionViewDataSourceStructure()
        let countSectionsBefore = dataStructure.numberOfSections()
        
        let expectedIdentifier = "1"
        let headerObject = HeaderObjectMock(identifier: expectedIdentifier)
        
        //when
        dataStructure.appendSection(with: headerObject)
        
        let resultHeaderObject = dataStructure.headerObject(for: dataStructure.numberOfSections() - 1) as? HeaderObjectMock
        
        //then
        XCTAssertEqual(countSectionsBefore + 1, dataStructure.numberOfSections())
        XCTAssertEqual(resultHeaderObject!.itemId, expectedIdentifier)
    }
    
    func testAppendSectionWithCellObjects() {
        
        //given
        let dataStructure = CollectionViewDataSourceStructure()
        
        let expectedIdentifier1 = "1"
        let expectedIdentifier2 = "2"
        
        let cellObjects: [CellObject] = [CellObjectMock(identifier: expectedIdentifier1) , CellObjectMock(identifier: expectedIdentifier2)]
        
        //when
        dataStructure.appendSection(with: cellObjects)
        
        //then
        XCTAssertEqual(cellObjects.count, dataStructure.numberOfObjects(at: 0))
        for (index, cellObject) in cellObjects.enumerated() {
            let mock = cellObject as! CellObjectMock
            let anotherMock = dataStructure.cellObject(at: IndexPath(row: index, section: 0)) as! CellObjectMock
            XCTAssertEqual(mock.itemId, anotherMock.itemId)
        }
    }
    
    func testAppendSectionWithHeaderAndCellObjects() {
        
        //given
        let dataStructure = CollectionViewDataSourceStructure()
        
        let expectedIdentifier1 = "1"
        let expectedIdentifier2 = "2"
        
        let cellObjects: [CellObject] = [CellObjectMock(identifier: expectedIdentifier1) , CellObjectMock(identifier: expectedIdentifier2)]
        
        let expectedIdentifier = "1"
        let headerObject = HeaderObjectMock(identifier: expectedIdentifier)
        
        //when
        dataStructure.appendSection(with: headerObject, and: cellObjects)
        
        let resultHeaderObject = dataStructure.headerObject(for: dataStructure.numberOfSections() - 1) as? HeaderObjectMock
        
        //then
        XCTAssertEqual(cellObjects.count, dataStructure.numberOfObjects(at: 0))
        for (index, cellObject) in cellObjects.enumerated() {
            let mock = cellObject as! CellObjectMock
            let anotherMock = dataStructure.cellObject(at: IndexPath(row: index, section: 0)) as! CellObjectMock
            XCTAssertEqual(mock.itemId, anotherMock.itemId)
        }
        XCTAssertEqual(resultHeaderObject!.itemId, expectedIdentifier)
    }
    
    func testAppendCellObject() {
        
        //given
        let dataStructure = CollectionViewDataSourceStructure()
        
        let expectedIdentifier1 = "1"
        let expectedIdentifier2 = "2"
        
        let cellObject1 = CellObjectMock(identifier: expectedIdentifier1)
        let cellObject2 = CellObjectMock(identifier: expectedIdentifier2)
        
        //when
        dataStructure.appendCellObject(cellObject1)
        dataStructure.appendCellObject(cellObject2)
        
        //then
        XCTAssertEqual(dataStructure.numberOfSections(), 1)
        
        let resultCellObject1 = dataStructure.cellObject(at: IndexPath(row: 0, section: 0)) as? CellObjectMock
        let resultCellObject2 = dataStructure.cellObject(at: IndexPath(row: 1, section: 0)) as? CellObjectMock
        
        XCTAssertEqual(resultCellObject1!.itemId, expectedIdentifier1)
        XCTAssertEqual(resultCellObject2!.itemId, expectedIdentifier2)
    }
    
    func testAppendHeaderObject() {
        
        //given
        let dataStructure = CollectionViewDataSourceStructure()
        
        let expectedIdentifier = "1"
        let headerObject = HeaderObjectMock(identifier: expectedIdentifier)
        
        //when
        dataStructure.appendHeaderObject(headerObject)
        
        let resultHeaderObject = dataStructure.headerObject(for: dataStructure.numberOfSections() - 1) as? HeaderObjectMock
        
        //then
        XCTAssertEqual(resultHeaderObject!.itemId, expectedIdentifier)
    }
    
    func testAppendFooterObject() {
        
        //given
        let dataStructure = CollectionViewDataSourceStructure()
        
        let expectedIdentifier = "1"
        let footerObject = FooterObjectMock(identifier: expectedIdentifier)
        
        //when
        dataStructure.appendFooterObject(footerObject)
        
        let resultFooterObject = dataStructure.footerObject(for: dataStructure.numberOfSections() - 1) as? FooterObjectMock
        
        //then
        XCTAssertEqual(resultFooterObject!.itemId, expectedIdentifier)
    }
    
    func testAddCellObjectToIndexPath() {
        
        //given
        let dataStructure = CollectionViewDataSourceStructure()
        
        let expectedIdentifier1 = "1"
        let expectedIdentifier2 = "2"
        
        let cellObject1 = CellObjectMock(identifier: expectedIdentifier1)
        let cellObject2 = CellObjectMock(identifier: expectedIdentifier2)
        let cellObject3 = CellObjectMock(identifier: "")
        let cellObject4 = CellObjectMock(identifier: "")
        
        //when
        dataStructure.addCellObject(cellObject1, to: IndexPath(row: 0, section: 0))
        dataStructure.addCellObject(cellObject2, to: IndexPath(row: 0, section: 0))
        dataStructure.addCellObject(cellObject3, to: IndexPath(row: 5, section: 0))
        dataStructure.addCellObject(cellObject4, to: IndexPath(row: 1, section: 2))
        
        let resultCellObject1 = dataStructure.cellObject(at: IndexPath(row: 0, section: 0)) as? CellObjectMock
        let resultCellObject2 = dataStructure.cellObject(at: IndexPath(row: 1, section: 0)) as? CellObjectMock
        
        //then
        XCTAssertEqual(dataStructure.numberOfSections(), 1)
        XCTAssertEqual(dataStructure.numberOfObjects(at: 0), 2)
        XCTAssertEqual(resultCellObject1!.itemId, expectedIdentifier2)
        XCTAssertEqual(resultCellObject2!.itemId, expectedIdentifier1)
    }
    
    func testAddHeaderObjectToSection() {
        
        //given
        let dataStructure = CollectionViewDataSourceStructure()
        
        let expectedIdentifier = "1"
        let headerObject = HeaderObjectMock(identifier: expectedIdentifier)
        
        //when
        dataStructure.addHeaderObject(headerObject, to: 1)
        
        let resultHeaderObject = dataStructure.headerObject(for: 1) as? HeaderObjectMock
        
        //then
        XCTAssertEqual(resultHeaderObject!.itemId, expectedIdentifier)
    }
    
    func testAddFooterObjectToSection() {
        
        //given
        let dataStructure = CollectionViewDataSourceStructure()
        
        let expectedIdentifier = "1"
        let footerObject = FooterObjectMock(identifier: expectedIdentifier)
        
        //when
        dataStructure.addFooterObject(footerObject, to: 1)
        
        let resultFooterObject = dataStructure.footerObject(for: 1) as? FooterObjectMock
        
        //then
        XCTAssertEqual(resultFooterObject!.itemId, expectedIdentifier)
    }
}

extension CollectionViewDataSourceStructureTests {
    
    struct CellObjectMock: CellObject {
        
        var itemId = ""
        
        init(identifier: String) {
            itemId = identifier
        }
    }
    
    struct HeaderObjectMock: HeaderObject {
        
        var itemId = ""
        
        init(identifier: String) {
            itemId = identifier
        }
    }
    
    struct FooterObjectMock: FooterObject {
        
        var itemId = ""
        
        init(identifier: String) {
            itemId = identifier
        }
    }
}

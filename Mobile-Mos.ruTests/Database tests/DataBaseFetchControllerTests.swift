//
//  DataBaseFetchControllerTests.swift
//  Mobile-Mos.ruTests
//
//  Created by Ildar Zalyalov on 16.05.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import XCTest
@testable import Mobile_Mos_ru

class DataBaseFetchControllerTests: XCTestCase {
    var databaseFetchController: DataBaseFetchController<DatabaseModelMock>!
    
    override func setUp() {
        super.setUp()
        
    
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    
    func testDatabaseFetchControllerResultCorrect() {
        //given
        let models = [DatabaseModelMock(value: "AnyValue"),DatabaseModelMock(value: "AnyValue2"), DatabaseModelMock(value: "AnyValue3")]
        let limit = 1
        let fetchRequest = DataBaseFetchRequest(with: DatabaseModelMock.self, limitedBy: limit)
        let mockDatabaseController = DatabaseFetchControllerMock(with: fetchRequest, and: models)
        databaseFetchController = DataBaseFetchController<DatabaseModelMock>(fetchController: mockDatabaseController)
        
        //when
        let result = databaseFetchController.results
        
        //then
        XCTAssert(result.count == limit, "Not correct count of objects")
    }
    
    func testDatabaseFetchControllerFetchRequestCorrect() {
        //given
        let models = [DatabaseModelMock(value: "AnyValue"),DatabaseModelMock(value: "AnyValue2"), DatabaseModelMock(value: "AnyValue3")]
        let limit = 1
        let fetchRequest = DataBaseFetchRequest(with: DatabaseModelMock.self, limitedBy: limit)
        let mockDatabaseController = DatabaseFetchControllerMock(with: fetchRequest, and: models)
        databaseFetchController = DataBaseFetchController<DatabaseModelMock>(fetchController: mockDatabaseController)
        
        //when
        let databaseFetchRequest = databaseFetchController.fetchRequest
        
        //then
        XCTAssertEqual(fetchRequest.limit, databaseFetchRequest.limit, "Fetch requests are not equal")
    }
    
    
}

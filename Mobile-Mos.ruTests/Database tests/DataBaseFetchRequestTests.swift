//
//  DataBaseFetchRequestTests.swift
//  Mobile-Mos.ruTests
//
//  Created by Ildar Zalyalov on 17.05.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import XCTest
@testable import Mobile_Mos_ru

class DataBaseFetchRequestTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    
    func testDatabaseFetchRequestInitWithLimit() {
        
        //given
        let limit = 2
        
        //when
        let fetchRequest = DataBaseFetchRequest(with: DatabaseModelMock.self, limitedBy: limit)
        let requestLimit = fetchRequest.limit
        
        //then
        XCTAssertEqual(limit, requestLimit, "Limits are not equal")
    }
    
    func testDatabaseFetchRequestInitWithPredicate() {
        
        //given
        let predicate = NSPredicate(format: "itemId = %@", "someField")
        
        //when
        let fetchRequest = DataBaseFetchRequest(with: DatabaseModelMock.self, filteredWith: predicate)
        let requestPredicate = fetchRequest.filterPredicate
        
        //then
        XCTAssertEqual(predicate, requestPredicate, "Predicates are not equal")
    }
    
    func testDatabaseFetchRequestInitWithPredicateFormat() {
        
        //given
        let predicateFormatString = "itemId = %@"
        let arguments = "someField"
        let predicateFormat = "itemId == \"someField\""
        
        //when
        let fetchRequest = DataBaseFetchRequest(with: DatabaseModelMock.self, filteredWith: predicateFormatString, arguments: arguments)
        let requestPredicate = fetchRequest.filterPredicate
        
        //then
        XCTAssertEqual(predicateFormat, requestPredicate?.predicateFormat, "Predicates formats are not equal")
    }
    
    func testDatabaseFetchRequestInitWithLimitAndPredicate() {
        
        //given
        let limit = 2
        let predicate = NSPredicate(format: "itemId = %@", "someField")
        
        //when
        let fetchRequest = DataBaseFetchRequest(with: DatabaseModelMock.self, limitedBy: limit, filteredWith: predicate)
        let requestLimit = fetchRequest.limit
        let requestPredicate = fetchRequest.filterPredicate
        
        //then
        XCTAssertEqual(limit, requestLimit, "Limits are not equal")
        XCTAssertEqual(predicate, requestPredicate, "Predicates are not equal")
    }
    
    func testDatabaseFetchRequestInitLimitAndPredicateStringFormat() {
        
        //given
        let limit = 2
        let predicateFormatString = "itemId = %@"
        let arguments = "someField"
        let predicateFormat = "itemId == \"someField\""
        
        //when
        let fetchRequest = DataBaseFetchRequest(with: DatabaseModelMock.self, limitedBy: limit, filteredWith: predicateFormatString, arguments: arguments)
        let requestLimit = fetchRequest.limit
        let requestPredicate = fetchRequest.filterPredicate
        
        //then
        XCTAssertEqual(limit, requestLimit, "Limits are not equal")
        XCTAssertEqual(predicateFormat, requestPredicate?.predicateFormat, "Predicates formats are not equal")
    }
    
    func testDatabaseFetchRequestInitWithSortDescriptor() {
        
        //given
        let dataBaseSortDescriptor = DataBaseSortDescriptor(sortKeyPath: "someField", sortAscending: true)
        let dataBaseSortDescriptor2 = DataBaseSortDescriptor(sortKeyPath: "someField2", sortAscending: false)
        let arrayOfDescriptors = [dataBaseSortDescriptor, dataBaseSortDescriptor2]
        
        //when
        var fetchRequest = DataBaseFetchRequest(with: DatabaseModelMock.self, sortedBy: dataBaseSortDescriptor.sortKeyPath, ascending: dataBaseSortDescriptor.sortAscending)
        fetchRequest.sortDescriptors.append(dataBaseSortDescriptor2)
        
        let requestSortDecriptors = fetchRequest.sortDescriptors
        
        //then
        XCTAssertEqual(arrayOfDescriptors.count, requestSortDecriptors.count, "Count of sortDecriptors not right")
    }
    
    func testDatabaseFetchRequestInitWithLimitAndSortDescriptor() {
        
        //given
        let limit = 2
        let dataBaseSortDescriptor = DataBaseSortDescriptor(sortKeyPath: "someField", sortAscending: true)
        let dataBaseSortDescriptor2 = DataBaseSortDescriptor(sortKeyPath: "someField2", sortAscending: false)
        let arrayOfDescriptors = [dataBaseSortDescriptor, dataBaseSortDescriptor2]
        
        //when
        var fetchRequest = DataBaseFetchRequest(with: DatabaseModelMock.self, limitedBy: limit, sortedBy: dataBaseSortDescriptor.sortKeyPath, ascending: dataBaseSortDescriptor.sortAscending)
        fetchRequest.sortDescriptors.append(dataBaseSortDescriptor2)
        
        let requestLimit = fetchRequest.limit
        let requestSortDecriptors = fetchRequest.sortDescriptors
        
        //then
        XCTAssertEqual(limit, requestLimit, "Limits are not equal")
        XCTAssertEqual(arrayOfDescriptors.count, requestSortDecriptors.count, "Count of sortDecriptors not right")
    }
}

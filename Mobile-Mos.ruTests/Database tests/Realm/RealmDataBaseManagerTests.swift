//
//  RealmDataBaseManagerTests.swift
//  Mobile-Mos.ruTests
//
//  Created by Ildar Zalyalov on 17.05.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import XCTest
import RealmSwift
@testable import Mobile_Mos_ru

class RealmDataBaseManagerTests: XCTestCase {
    
    var realmDatabaseManager: DataBaseManager!
    
    typealias Class = RealmDataBaseManagerTests

    override func setUp() {
        super.setUp()
        realmDatabaseManager = RealmDataBaseManager(shouldUseEncryptedStorage: false)
    }
    
    override func tearDown() {
        realmDatabaseManager = nil
        super.tearDown()
    }
    
    override class func setUp() {
        
        
        super.setUp()
    }
    
    override class func tearDown() {
        
        
        super.tearDown()
    }
    
    func testDatabaseManagerSave() {
        
        //given
        let testModel1 = DatabaseModelMock(value: "someValue")
        let testModel2 = DatabaseModelMock(value: "someValue")
        let arrayOfTestObjects = [testModel1, testModel2]
        
        let fetchRequest = DataBaseFetchRequest(with: DatabaseModelMock.self)
        
        //when
        try! realmDatabaseManager.save(models: arrayOfTestObjects)
        let fetchedTestObjects = try! realmDatabaseManager.fetch(with: fetchRequest)
        
        //then
        XCTAssertEqual(arrayOfTestObjects.count, fetchedTestObjects.count, "Objects is data base not correct")
    }
    
    func testDatabaseManagerSaveAsync() {
        
        //given
        let expectationSave = expectation(description: "Save async")
        let testModel1 = DatabaseModelMock2(value: "someValue")
        let testModel2 = DatabaseModelMock2(value: "someValue")
        let arrayOfTestObjects = [testModel1, testModel2]
        
        let fetchRequest = DataBaseFetchRequest(with: DatabaseModelMock2.self)
        
        //when
        realmDatabaseManager.saveAsync(models: arrayOfTestObjects) { (result) in
            
            self.clearAllModelsDatabase(with: DatabaseModelMock2.self)
            
            switch result {
            case .success:
                
                let fetchedTestObjects = try! self.realmDatabaseManager.fetch(with: fetchRequest)
                XCTAssertEqual(arrayOfTestObjects.count, fetchedTestObjects.count, "Objects is data base not correct")
                
                expectationSave.fulfill()
            case .error:
                XCTFail("Error when save async")
            }
        }
        
        //then
        waitForExpectations(timeout: 3.0) { (error) in
            if let error = error {
                XCTFail("Error when save async: \(error)")
            }
        }
        
    }
    
    func testDatabaseManagerFetch() {
       
        //given
        let testModel1 = DatabaseModelMock(value: "someValue3")
        let testModel2 = DatabaseModelMock(value: "someValue4")
        let arrayOfTestObjects = [testModel1, testModel2]
        
        let fetchRequest = DataBaseFetchRequest(with: DatabaseModelMock.self)
        
        try! realmDatabaseManager.save(models: arrayOfTestObjects)
        
        //when
        let fetchedModels = try! realmDatabaseManager.fetch(with: fetchRequest)
        
        //then
        XCTAssertEqual(arrayOfTestObjects.count, fetchedModels.count, "Fetched models not equal")
        XCTAssertEqual(testModel1.value, fetchedModels.first?.value, "First models not equal")
        XCTAssertEqual(testModel2.value, fetchedModels.last?.value, "Last models not equal")
    }
    
    func testDatabaseManagerFetchAsync() {
        
        //given
        let expectationSave = expectation(description: "Fetch async")
        let testModel1 = DatabaseModelMock3(value: "someValue3")
        let testModel2 = DatabaseModelMock3(value: "someValue4")
        let arrayOfTestObjects = [testModel1, testModel2]
        
        let fetchRequest = DataBaseFetchRequest(with: DatabaseModelMock3.self)
        
        try! realmDatabaseManager.save(models: arrayOfTestObjects)
        
        //when
        realmDatabaseManager.fetchAsync(with: fetchRequest) { (result) in
            
            self.clearAllModelsDatabase(with: DatabaseModelMock3.self)
            
            switch result {
            case .success (let models):
                
                XCTAssertEqual(arrayOfTestObjects.count, models.count, "Fetched models not equal")
                XCTAssertEqual(testModel1.value, models.first?.value, "First models not equal")
                XCTAssertEqual(testModel2.value, models.last?.value, "Last models not equal")
                
                expectationSave.fulfill()
            case .error:
                XCTFail("Error when save async")
            }
        }
       
        //then
        waitForExpectations(timeout: 3.0) { (error) in
            if let error = error {
                XCTFail("Error when save async: \(error)")
            }
        }
    }
    
    //MARK: Helpers
    
    fileprivate func clearAllInDatabase(completion: DataBaseOperationResultBlock? = nil) {
        
        let dispatchGroup = DispatchGroup()
        var finalResult: DataBaseOperationResult = .success
        
        dispatchGroup.enter()
        self.realmDatabaseManager.deleteAllModelsAsync(of: DatabaseModelMock.self) { (result) in
            
            defer { dispatchGroup.leave() }
            
            switch result {
                
            case .error (let error):
                finalResult = .error(error)
                print("Error when deleting all models: \(error.localizedDescription)")
            default:
                return
            }
        }
        
        dispatchGroup.enter()
        self.realmDatabaseManager.deleteAllModelsAsync(of: DatabaseModelMock2.self) { (result) in
            
            defer { dispatchGroup.leave() }
            
            switch result {
                
            case .error (let error):
                finalResult = .error(error)
                print("Error when deleting all models: \(error.localizedDescription)")
            default:
                return
            }
        }
        
        dispatchGroup.enter()
        self.realmDatabaseManager.deleteAllModelsAsync(of: DatabaseModelMock3.self) { (result) in
            
            defer { dispatchGroup.leave() }
            
            switch result {
                
            case .error (let error):
                finalResult = .error(error)
                print("Error when deleting all models: \(error.localizedDescription)")
            default:
                return
            }
        }
        
        dispatchGroup.notify(queue: DispatchQueue.main) {
            completion?(finalResult)
        }
    }
    
    fileprivate func clearAllModelsDatabase<ModelType: Persistable>(with modelType:ModelType.Type, completion: DataBaseOperationResultBlock? = nil) {
        
        if modelType == DatabaseModelMock.self {
            
            self.realmDatabaseManager.deleteAllModelsAsync(of: DatabaseModelMock.self) { (result) in
                
                completion?(result)
                
                switch result {
                    
                case .error (let error):
                    print("Error when deleting all models: \(error.localizedDescription)")
                default:
                    return
                }
            }
        }
       
        if modelType == DatabaseModelMock2.self {
            
            self.realmDatabaseManager.deleteAllModelsAsync(of: DatabaseModelMock2.self) { (result) in
                
                completion?(result)
                
                switch result {
                    
                case .error (let error):
                    print("Error when deleting all models: \(error.localizedDescription)")
                default:
                    return
                }
            }
        }
        
        if modelType == DatabaseModelMock3.self {
            
            self.realmDatabaseManager.deleteAllModelsAsync(of: DatabaseModelMock3.self) { (result) in
                
                completion?(result)
                
                switch result {
                    
                case .error (let error):
                    print("Error when deleting all models: \(error.localizedDescription)")
                default:
                    return
                }
            }
        }
    }
}

//
//  DatabaseModelMock.swift
//  Mobile-Mos.ruTests
//
//  Created by Ildar Zalyalov on 16.05.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

@testable import Mobile_Mos_ru

/// Мок модели базы данных
struct DatabaseModelMock: BaseModel {
    var value: String = ""
}


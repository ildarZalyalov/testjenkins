//
//  DatabaseFetchControllerMock.swift
//  Mobile-Mos.ruTests
//
//  Created by Ildar Zalyalov on 16.05.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation
@testable import Mobile_Mos_ru

/// Мок контроллера базы данных 
class DatabaseFetchControllerMock<ModelType: Persistable>: DataBaseFetchControllerProtocol {
    typealias Model = ModelType
    
    let fetchRequest: DataBaseFetchRequest<ModelType>
    let fetchResults: [ModelType]
    
    var results: [ModelType] {
        
        if let limit = fetchRequest.limit {
            
            switch fetchRequest.limitType {
            case .prefix:
                return Array(fetchResults.prefix(limit))
            case .suffix:
                return Array(fetchResults.suffix(limit))
            }
        
        }
        else {
            return fetchResults
        }
    }
    
    var onUpdate: DataBaseFetchUpdateBlock?
    
    init(with fetchRequest: DataBaseFetchRequest<ModelType>, and fetchResults: [ModelType]) {
        
        self.fetchRequest = fetchRequest
        self.fetchResults = fetchResults
        
        let updates = DataBaseFetchUpdate(deletions: [], insertions: [], modifications: [])
        onUpdate?(updates)
    }
    
    
}

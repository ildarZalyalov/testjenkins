//
//  DatabasePersistableModelMock2.swift
//  Mobile-Mos.ruTests
//
//  Created by Ildar Zalyalov on 18.05.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation
import RealmSwift
@testable import Mobile_Mos_ru

@objcMembers
/// Мок модели рилма
class DatabasePersistableModelMock2: Object {
    
    dynamic var value: String = String()
    dynamic var uniqId: String = UUID().uuidString
    
    override static func primaryKey() -> String? {
        return #keyPath(uniqId)
    }
}

extension DatabaseModelMock2: Persistable {
    
    /// Создать объект для сохранения в БД
    func toPersistableObject() -> DatabasePersistableModelMock2{
        
        let object = DatabasePersistableModelMock2()
        object.value = value
        
        return object
    }
    
    /// Обновить значения полей из сохраненного в БД объекта
    ///
    /// - Parameter object: объект из БД
    mutating func updateFromPersistableObject(object: DatabasePersistableModelMock2) {
        value = object.value
    }
}

//
//  MapItemDetailsBuilderTests.swift
//  Mobile-Mos.ruTests
//
//  Created by Ildar Zalyalov on 13.04.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import XCTest
@testable import Mobile_Mos_ru

class MapItemDetailsBuilderTests: XCTestCase {
    var mapItemDetailsBuilder: ItemDetailsBuilder!
    
    override func setUp() {
        super.setUp()
        mapItemDetailsBuilder = MapItemDetailsBuilderImplementation()
    }
    
    override func tearDown() {
        mapItemDetailsBuilder = nil 
        super.tearDown()
    }
    
    func testMapItemDetailsNotEmptyAndCorrect() {
        //given
        let mapItem = MapItem(globalId: 0,
                              name: "Tet",
                              address: "address",
                              itemDescription: "desc",
                              publicPhone: "89871742488",
                              website: nil,
                              workingHours: [],
                              workingHoursClarification: nil,
                              location: moscowLocation,
                              isFavorited: false,
                              stringForSharing: nil)
        
        //when
       let dataSource = mapItemDetailsBuilder.createItemDetails(from: mapItem, formattedDistance: nil)
        
        //then
        XCTAssertNotNil(dataSource, "Datasource is nil")
        XCTAssert(!dataSource.name.isHidden, "Name is empty")
        XCTAssertEqual(mapItem.address, dataSource.address.text, "Address not equal")
        XCTAssertTrue(dataSource.workingHours.isHidden, "Working hours empty, but not hidden")
    }
    
    func testMapItemDetailInfoViewHiddenWhenFieldsEmpty() {
        
        //given
        let mapItem = MapItem(globalId: 0,
                              name: "Tet",
                              address: nil,
                              itemDescription: nil,
                              publicPhone: nil,
                              website: nil,
                              workingHours: [],
                              workingHoursClarification: nil,
                              location: moscowLocation,
                              isFavorited: false,
                              stringForSharing: nil)
        
        //when
        let dataSource = mapItemDetailsBuilder.createItemDetails(from: mapItem, formattedDistance: nil)
        
        //then
        XCTAssertTrue(dataSource.isInfoViewHidden, "Not hidden")
    }
    
    func testOverlapItemDetailsNotEmpty() {
        //given
        let overlap = Overlap()
        
        //when
        let datasource = mapItemDetailsBuilder.createItemDetails(from: overlap)
        
        //then
        XCTAssertNotNil(datasource, "Datasource is empty")
    }
    
    func testOverlapShareStringNotEmpty() {
        //given
        let overlap = Overlap()
        
        //when
        let datasource = mapItemDetailsBuilder.createItemDetails(from: overlap)
        
        //then
        XCTAssertFalse(datasource.shareString.isEmpty, "Share string is empty")
    }
    
    func testOverlapFieldsNotEmptyWhenFilled() {
        //given
        let overlap = Overlap(id: "uniqId",
                              name: "Name",
                              address: "Address",
                              fromDate: Date(),
                              toDate: Date(),
                              lastUpdateDate: Date(),
                              phone: "+79871742488",
                              customer: "Customer",
                              requester: "Requester",
                              okrugName: "Best Okgur",
                              eventType: "Type",
                              description: "some really descr",
                              lanesAvailable: 2,
                              lanesClosed: 2)
        
        //when
        let datasource = mapItemDetailsBuilder.createItemDetails(from: overlap)
        
        //then
        XCTAssertEqual(overlap.name, datasource.name.text, "Name not equal")
        XCTAssertEqual(overlap.address, datasource.address.text, "Address not equal")
        XCTAssertEqual(overlap.description, datasource.description.text, "Description not equal")
        XCTAssertFalse(datasource.dateToDateFrom.text.isEmpty, "Dates string empty")
        XCTAssertFalse(datasource.lastUpdateDate.text.isEmpty, "Last update date string is empty")
        XCTAssertEqual(overlap.phone, datasource.cellPhone.text, "Phone not equal")
        XCTAssertEqual(overlap.customer, datasource.customer.text, "Customer not equal")
        XCTAssertEqual(overlap.requester, datasource.requester.text, "Requester not equal")
        XCTAssertEqual(overlap.okrugName, datasource.okrug.text, "Okrug name not equal")
        XCTAssertFalse(datasource.roadLines.text.isEmpty, "Road lines is empty")
        
    }
}

//
//  WeatherStringBuilderTests.swift
//  Mobile-Mos.ruTests
//
//  Created by Ildar Zalyalov on 13.04.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import XCTest
@testable import Mobile_Mos_ru

class WeatherStringBuilderTests: XCTestCase {
    var weatherStringBuilder: WeatherStringBuilder!
    
    override func setUp() {
        super.setUp()
        
        weatherStringBuilder = WeatherStringBuilder()
    }
    
    override func tearDown() {
        weatherStringBuilder = nil
        super.tearDown()
    }
    
    func testWeatherBuildedStringNotEmpty() {
        //given
        let temperature = 15
        let someTempText = "Облачно"
        
        //when
       let buildedStringData = weatherStringBuilder.buildWeatherString(from: temperature, weatherText: someTempText)
       
        //then
        XCTAssertFalse(buildedStringData.title.isEmpty, "Заголовок пустой")
        XCTAssertFalse(buildedStringData.subtitle.isEmpty, "Сабтайтл пустой")
    }
    
    func testWeatherBuildedStringTitleContainsPlusWhenValueIsPositive() {
        //given
        let temperature = 15
        let someTempText = "Облачно"
        let positiveChar:Character = "+"
        
        //when
        let buildedStringData = weatherStringBuilder.buildWeatherString(from: temperature, weatherText: someTempText)
        
        //then
        XCTAssertTrue(buildedStringData.title.contains(positiveChar), "Заголовок Не содержит знака +")
    }
    
    func testWeatherBuildedStringTitleContainsMinusWhenValueIsNegative() {
        //given
        let temperature = -15
        let someTempText = "Облачно"
        let negativeChar:Character = "-"
        
        //when
        let buildedStringData = weatherStringBuilder.buildWeatherString(from: temperature, weatherText: someTempText)
        
        //then
        XCTAssertTrue(buildedStringData.title.contains(negativeChar), "Заголовок Не содержит знака -")
    }
    
}

//
//  XmlStringsBuilderTests.swift
//  Mobile-Mos.ruTests
//
//  Created by Ildar Zalyalov on 13.04.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import XCTest
@testable import Mobile_Mos_ru

class XmlStringsBuilderTests: XCTestCase {
    var xmlStringsBuilder: XmlStringsBuilder!
    
    override func setUp() {
        super.setUp()
        xmlStringsBuilder = XmlStringsBuilder.getXmlStringsBuilder()
    }
    
    override func tearDown() {
        xmlStringsBuilder = nil
        super.tearDown()
    }
    
    func testXmlStringBuilderStringNotEmpty() {
        //given
        let title = "title"
        let email = "email"
        let feedback = "feedback"
        
        //when
        let buildedString = xmlStringsBuilder.buildHPSMCreateRequestXmlString(with: title, email: email, feedBackDescription: feedback)
        
        //then
        XCTAssertFalse(buildedString.isEmpty, "Строка пустая")
    }
    
    
}

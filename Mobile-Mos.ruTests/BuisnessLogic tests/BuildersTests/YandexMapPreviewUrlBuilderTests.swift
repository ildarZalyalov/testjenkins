//
//  YandexMapPreviewUrlBuilderTests.swift
//  Mobile-Mos.ruTests
//
//  Created by Ildar Zalyalov on 13.04.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import XCTest
@testable import Mobile_Mos_ru

class YandexMapPreviewUrlBuilderTests: XCTestCase {
    var yandexMapPreviewUrlBuilder: YandexMapPreviewUrlBuilder!
    
    override func setUp() {
        super.setUp()
        
        let implementation = YandexMapPreviewUrlBuilderImplementation()
        implementation.parametersEncoder = HTTPRequestParameterEncoderDefault()
        
        yandexMapPreviewUrlBuilder = implementation
    }
    
    override func tearDown() {
        yandexMapPreviewUrlBuilder = nil
        super.tearDown()
    }
    
    func testPreviewUrlValid() {
        
        //given
        let location = moscowLocation
        let mapPreviewLocationSpan: YandexMapPreviewUrlBuilder.CoordinateSpan = (0.002, 0.002)
        let size = CGSize(width: 100, height: 100)
        let sharedApplication = UIApplication.shared
       
        //when
        let url = yandexMapPreviewUrlBuilder.urlForMapPreview(with: location, span: mapPreviewLocationSpan, size: size)
        
        //then
        XCTAssert(sharedApplication.canOpenURL(url), "Url not valid")
    }
    
    func testPreviewUrlValidIfSizeZero() {
        
        //given
        let location = moscowLocation
        let mapPreviewLocationSpan: YandexMapPreviewUrlBuilder.CoordinateSpan = (0.002, 0.002)
        let size = CGSize.zero
        let sharedApplication = UIApplication.shared
        
        //when
        let url = yandexMapPreviewUrlBuilder.urlForMapPreview(with: location, span: mapPreviewLocationSpan, size: size)
        
        //then
        XCTAssert(sharedApplication.canOpenURL(url), "Url not valid")
    }
    
}

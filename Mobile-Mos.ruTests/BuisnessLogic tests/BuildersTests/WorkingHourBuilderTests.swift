//
//  WorkingHourBuilderTests.swift
//  Mobile-Mos.ruTests
//
//  Created by Ildar Zalyalov on 13.04.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import XCTest
@testable import Mobile_Mos_ru

class WorkingHourBuilderTests: XCTestCase {
    var workingHourBuilder: WorkingHourBuilder!
    
    override func setUp() {
        super.setUp()
        
        workingHourBuilder = WorkingHourBuilderImplementation()
    }
    
    override func tearDown() {
        workingHourBuilder = nil
        super.tearDown()
    }
    
    func testWorkingHourNotEmpty() {
        
        //given
        let workingHours = [
            MapItemWorkingHour(dayOfWeek: "понедельник", hours: "08:00-21:00"),
            MapItemWorkingHour(dayOfWeek: "вторник", hours: "08:00-21:00"),
            MapItemWorkingHour(dayOfWeek: "среда", hours: "08:00-21:00"),
            MapItemWorkingHour(dayOfWeek: "четверг", hours: "08:00-21:00"),
            MapItemWorkingHour(dayOfWeek: "пятница", hours: "08:00-21:00"),
            MapItemWorkingHour(dayOfWeek: "суббота", hours: "Выходной"),
            MapItemWorkingHour(dayOfWeek: "воскресенье", hours: "Выходной"),
            ]
        //when
        let workingHour = workingHourBuilder.checkOpenHour(with: workingHours)
        
        //then
        XCTAssertNotNil(workingHour, "Working hour nil")
    }
    
    func testWorkingHourNotEmptyIfWorkingTimeDayAndNight() {
        //given
        let convienceString = "круглосуточно"
        
        let workingHours = [
            MapItemWorkingHour(dayOfWeek: "понедельник", hours: convienceString),
            MapItemWorkingHour(dayOfWeek: "вторник", hours: convienceString),
            MapItemWorkingHour(dayOfWeek: "среда", hours: convienceString),
            MapItemWorkingHour(dayOfWeek: "четврег", hours: convienceString),
            MapItemWorkingHour(dayOfWeek: "пятница", hours: convienceString),
            MapItemWorkingHour(dayOfWeek: "суббота", hours: "Выходной"),
            MapItemWorkingHour(dayOfWeek: "воскресенье", hours: "Выходной"),
            ]
        
        //when
        let workingHour = workingHourBuilder.checkOpenHour(with: workingHours)
        
        //then
        XCTAssertNotNil(workingHour, "Working hour nil")
    }
    
    func testWorkingHourNotEmptyIfPlaceClosed() {
        //given
        let closeString = "закрыто"
        
        let workingHours = [
            MapItemWorkingHour(dayOfWeek: "понедельник", hours: closeString),
            MapItemWorkingHour(dayOfWeek: "вторник", hours: closeString),
            MapItemWorkingHour(dayOfWeek: "среда", hours: closeString),
            MapItemWorkingHour(dayOfWeek: "четврег", hours: closeString),
            MapItemWorkingHour(dayOfWeek: "пятница", hours: closeString),
            MapItemWorkingHour(dayOfWeek: "суббота", hours: "Выходной"),
            MapItemWorkingHour(dayOfWeek: "воскресенье", hours: "Выходной"),
            ]
        
        //when
        let workingHour = workingHourBuilder.checkOpenHour(with: workingHours)
        
        //then
        XCTAssertNotNil(workingHour, "Working hour nil")
    }
}

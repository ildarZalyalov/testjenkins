//
//  OnboardingBuilderTests.swift
//  Mobile-Mos.ruTests
//
//  Created by Ildar Zalyalov on 13.04.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import XCTest
@testable import Mobile_Mos_ru

class OnboardingBuilderTests: XCTestCase {
    var onboardingBuilder: OnboardingBuilder!
    
    override func setUp() {
        super.setUp()
        
        onboardingBuilder = OnboardingBuilder.getOnboardingBuilder()
    }
    
    override func tearDown() {
        onboardingBuilder = nil
        super.tearDown()
    }
    
    
}

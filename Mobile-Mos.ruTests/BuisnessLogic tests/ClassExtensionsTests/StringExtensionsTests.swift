//
//  StringExtensionsTests.swift
//  Mobile-Mos.ruTests
//
//  Created by Ildar Zalyalov on 27.04.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import XCTest
@testable import Mobile_Mos_ru

class StringExtensionsTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testSizeOfStringMoreThanZero() {
        //given
        let someString = "some string"
        let font = UIFont.systemFont(ofSize: 14)
        
        //when
        let size = someString.sizeOfString(with: font)
        
        //then
        XCTAssert(size.width > 0 && size.height > 0, "Size of string not correct")
    }
    
    func testSizeOfStringFontSystemLessthanFontSystemBold() {
        //given
        let someString = "some string"
        let regulafont = UIFont.systemFont(ofSize: 14)
        let boldFont = UIFont.boldSystemFont(ofSize: 14)
        
        //when
        let regularSize = someString.sizeOfString(with: regulafont)
        let boldSize = someString.sizeOfString(with: boldFont)
        
        //then
        XCTAssertNotEqual(regularSize, boldSize, "Size are equal but font's not")
    }
    
    func testSizeOfStringWithWidth() {
        //given
        let someString = "some string"
        let regulafont = UIFont.systemFont(ofSize: 18)
        let maxWidth: CGFloat = 10
        
        //when
        let regularSize = someString.sizeOfString(with: regulafont, constrainedTo: maxWidth)
        
        //then
        XCTAssert(regularSize.width <= maxWidth, "width of size more than maxWidth")
    }
    
    func testFirstLetterCapitalized() {
        //given
        let someString = "some string"
        let firstCharOfStringUpper = String(someString.first!).localizedUppercase
        let firstCharOfStringLower = String(someString.first!).localizedLowercase
        
        //when
        let capitalizedString = someString.firstLetterCapitalized()
        
        //then
        XCTAssertEqual(firstCharOfStringUpper, String(capitalizedString.first!), "First character Not uppercased")
        XCTAssertNotEqual(firstCharOfStringLower, String(capitalizedString.first!), "First character Not uppercased")
    }
    
    func testTrimAllWhitespaces() {
        //given
        let someString = "some string   asadsa"
        let whitespace: Character = " "
        
        //when
        let trimmetString = someString.trimWhiteSpaces()
        
        //then
        XCTAssertFalse(trimmetString.contains(whitespace), "String contains spaces")
    }
}

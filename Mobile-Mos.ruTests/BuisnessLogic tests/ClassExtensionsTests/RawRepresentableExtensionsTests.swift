//
//  RawRepresentableExtensionsTests.swift
//  Mobile-Mos.ruTests
//
//  Created by Ildar Zalyalov on 27.04.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import XCTest
@testable import Mobile_Mos_ru

class RawRepresentableExtensionsTests: XCTestCase {
    
    enum MockEnumWithOptionalRaws: String {
       case someFirstCase = "first"
       case someSecondCase = "second"
    }
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testOptionalRawValueEnumInitializationNotNil() {
        //given
        let optionalRawValue: String? = "first"
        
        //when
        let enumValue = MockEnumWithOptionalRaws(rawValueOptional: optionalRawValue)
        
        //then
        XCTAssertNotNil(enumValue, "Enum is nil")
    }
    
    func testOptionalRawValueEnumInitializationIsNil() {
        //given
        let optionalRawValue: String? = "third"
        
        //when
        let enumValue = MockEnumWithOptionalRaws(rawValueOptional: optionalRawValue)
        
        //then
        XCTAssertNil(enumValue, "Enum is not nil")
    }
    
    func testOptionalRawValueEnumHasCorrectValues() {
        //given
        let optionalRawValue: String? = "second"
        
        //when
        let enumValue = MockEnumWithOptionalRaws(rawValueOptional: optionalRawValue)
        
        //then
        XCTAssertEqual(optionalRawValue, enumValue?.rawValue, "Values not equal")
    }
}


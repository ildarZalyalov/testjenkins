//
//  ErrorExtensionsTests.swift
//  Mobile-Mos.ruTests
//
//  Created by Ildar Zalyalov on 19.04.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import XCTest
@testable import Mobile_Mos_ru

class ErrorExtensionsTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testErrorIsInternetConnectionError() {
        //given
        let error = NSError(domain:NSURLErrorDomain,
                            code: NSURLErrorNotConnectedToInternet,
                            userInfo: nil)
        
        let falsePositiveError = NSError(domain:NSURLErrorDomain,
                                         code: NSURLErrorNetworkConnectionLost,
                                         userInfo: nil)
        
        //when
        let isNoInternetConnectionError = error.isNoInternetConnectionError()
        let falsePositiveState = falsePositiveError.isNoInternetConnectionError()
        
        //then
        XCTAssertFalse(falsePositiveState, "Not valid error state")
        XCTAssertTrue(isNoInternetConnectionError, "Not valid error state")
    }
    
    func testErrorIsNetworkRequestCanceledError() {
        //given
        let error = NSError(domain:NSURLErrorDomain,
                            code: NSURLErrorCancelled,
                            userInfo: nil)
        
        let falsePositiveError = NSError(domain:NSURLErrorDomain,
                                         code: NSURLErrorCannotFindHost,
                                         userInfo: nil)
        
        //when
        let isRequestCanceledErrorState = error.isNetworkRequestCancelled()
        let falsePositiveState = falsePositiveError.isNetworkRequestCancelled()
        
        //then
        XCTAssertFalse(falsePositiveState, "Not valid error state")
        XCTAssertTrue(isRequestCanceledErrorState, "Not valid error state")
    }
}

//
//  NSDictionaryExtensionsTests.swift
//  Mobile-Mos.ruTests
//
//  Created by Ildar Zalyalov on 19.04.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import XCTest
@testable import Mobile_Mos_ru

class NSDictionaryExtensionsTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testJoinTwoDictionaryWithPlus() {
        //given
        let firstDictionary = ["one":1, "two":2, "three":3, "four":4, "five":5]
        let secondDictionary = ["one":1, "two":2, "three":3, "four":4, "five":5, "six":6]
        
        //when
        let finalDictionary = firstDictionary + secondDictionary
        
        //then
        XCTAssertNotEqual(firstDictionary.count, finalDictionary.count, "Count is equal")
        XCTAssertNotNil(finalDictionary["six"], "Value is Nil")
    }
    
    func testJoinTwoDictionaryWithPlusEqual() {
        //given
        var firstDictionary = ["one":1, "two":2, "three":3, "four":4, "five":5]
        let secondDictionary = ["one":1, "two":2, "three":3, "four":4, "five":5, "six":6]
        
        //when
        firstDictionary += secondDictionary
        
        //then
        XCTAssertEqual(firstDictionary.count, secondDictionary.count, "Count is equal")
        XCTAssertNotNil(firstDictionary["six"], "Value is Nil")
    }
    
    func testDisJoinTwoDictionaryWithMinusEqualEmptyIfFirstLessThanSecond() {
        //given
        var firstDictionary = ["one":1, "two":2, "three":3, "four":4, "five":5]
        let secondDictionary = ["one":1, "two":2, "three":3, "four":4, "five":5, "six":6]
        
        //when
        firstDictionary -= secondDictionary
        
        //then
        XCTAssertTrue(firstDictionary.isEmpty, "First dict not empty")
    }
    
    func testDisJoinTwoDictionaryWithMinusEqualNotEmptyIfFirstMoreThanSecond() {
        //given
        let value = 6
        var firstDictionary = ["one":1, "two":2, "three":3, "four":4, "five":5, "six":value]
        let secondDictionary = ["one":1, "two":2, "three":3, "four":4, "five":5]
        
        //when
        firstDictionary -= secondDictionary
        
        //then
        XCTAssertFalse(firstDictionary.isEmpty, "First dict empty")
        XCTAssertEqual(value, firstDictionary.first?.value, "Final values not equal")
    }
    
    func testSubscribeExtension() {
        //given
        let value = 15
        let someInnerDictionary = ["child": ["childSecond": value]]
        let testDictionary: NSDictionary = ["Root":someInnerDictionary]
        
        //when
        let finalValue = testDictionary["Root", "child", "childSecond"] as? Int
        
        //then
        XCTAssertNotNil(finalValue, "Final value is nil")
        XCTAssertEqual(value, finalValue, "Values not equal")
    }
    
    func testValueForKeypathsInArray() {
        //given
        let value = 15
        let someInnerDictionary = ["child": ["childSecond": value]]
        let testDictionary: NSDictionary = ["Root":someInnerDictionary]
        
        //when
        let finalValue = testDictionary.valueForKeyPathInArray(path: ["Root", "child", "childSecond"]) as? Int
        
        //then
        XCTAssertNotNil(finalValue, "Final value is nil")
        XCTAssertEqual(value, finalValue, "Values not equal")
    }
}

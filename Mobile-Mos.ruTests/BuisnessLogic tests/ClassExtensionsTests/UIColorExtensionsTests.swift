//
//  UIColorExtensionsTests.swift
//  Mobile-Mos.ruTests
//
//  Created by Ildar Zalyalov on 27.04.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import XCTest
@testable import Mobile_Mos_ru

class UIColorExtensionsTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testColorFromHexCorrectAndNotEmpty() {
        //given
        let hexString = "#ff0000"
        let color = UIColor.red
        
        //when
        let hexColor = UIColor.fromHexString(hexString: hexString)
        
        //then
        XCTAssertNotNil(hexColor, "Hex color is nil")
        XCTAssertEqual(color, hexColor, "Color not equal")
    }
    
    func testIntHexFromColor() {
        //given
        let color = UIColor.red
        
        //when
        let intHex = color.toHex()
        let colorFromHex = UIColor.fromHex(hex: intHex)
        
        //then
        
        XCTAssertEqual(color, colorFromHex, "Colors not equal")
    }
}

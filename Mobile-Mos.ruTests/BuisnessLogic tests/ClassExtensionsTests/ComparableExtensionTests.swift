//
//  ComparableExtensionTests.swift
//  Mobile-Mos.ruTests
//
//  Created by Ildar Zalyalov on 19.04.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import XCTest
@testable import Mobile_Mos_ru

class ComparableExtensionTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testComparableClampedCorrectlyWithInt() {
        //given
        let someValue: Int = 15
        let closedRange: ClosedRange = 0 ... 9
        
        //when
        let finalClampedValue = someValue.clamped(to: closedRange)
        
        //then
        XCTAssertNotEqual(closedRange.lowerBound, finalClampedValue)
        XCTAssertEqual(closedRange.upperBound, finalClampedValue)
    }
    
    func testComparableClampedCorrectlyWithDouble() {
        //given
        let someValue: Double = 15.0
        let closedRange: ClosedRange = 1.0 ... 13.25
        
        //when
        let finalClampedValue = someValue.clamped(to: closedRange)
        
        //then
        XCTAssertNotEqual(closedRange.lowerBound, finalClampedValue)
        XCTAssertEqual(closedRange.upperBound, finalClampedValue)
    }
}

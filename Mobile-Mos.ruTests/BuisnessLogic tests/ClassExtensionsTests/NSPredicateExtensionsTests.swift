//
//  NSPredicateExtensionsTests.swift
//  Mobile-Mos.ruTests
//
//  Created by Ildar Zalyalov on 19.04.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import XCTest
@testable import Mobile_Mos_ru

class NSPredicateExtensionsTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testPredicateConfiguration() {
        //given
        let testArray: NSArray = [ModelMock(name: "name1", someValue: 12),
                         ModelMock(name: "name2", someValue: 15),
                         ModelMock(name: "name3", someValue: 1),
                         ModelMock(name: "name1", someValue: 12)]
        let predicateString = "name = %@"
        let finalObjectsCount = 2
        let firstObjectInArray = testArray[0] as! ModelMock
        let predicateConfiguration = NSPredicateConfiguration(with: predicateString, and: [firstObjectInArray.name])
        
        let predicate = NSPredicate(with: predicateConfiguration)
        
        //when
        let filteredArray = testArray.filtered(using: predicate)
        
        //then
        XCTAssertFalse(filteredArray.isEmpty, "Array is empty")
        XCTAssertEqual(finalObjectsCount, filteredArray.count)
    }
    
    func testPredicateConfigurationAndCondition() {
        //given
        let testArray: NSArray = [ModelMock(name: "name1", someValue: 12),
                                  ModelMock(name: "name2", someValue: 15),
                                  ModelMock(name: "name3", someValue: 1),
                                  ModelMock(name: "name1", someValue: 1)]
        let firstPredicateString = "name = %@"
        let secondPredicateString = "someValue = %d"
        
        let finalObjectsCount = 1
        let valueToSearch = testArray.lastObject as! ModelMock
        let firstObjectInArray = testArray.firstObject as! ModelMock
        
        var predicateConfiguration = NSPredicateConfiguration(with: firstPredicateString, and: [firstObjectInArray.name])
        let predicateCondition = (format: secondPredicateString, arguments: [valueToSearch.someValue] as [Any])
        
        //when
        predicateConfiguration.and(condition: predicateCondition)
        
        let predicate = NSPredicate(with: predicateConfiguration)
        let filteredArray = testArray.filtered(using: predicate)
        
        //then
        XCTAssertFalse(filteredArray.isEmpty, "Array is empty")
        XCTAssertEqual(finalObjectsCount, filteredArray.count)
    }
    
    func testPredicateConfigurationOrCondition() {
        //given
        let testArray: NSArray = [ModelMock(name: "name1", someValue: 12),
                                  ModelMock(name: "name2", someValue: 15),
                                  ModelMock(name: "name3", someValue: 1),
                                  ModelMock(name: "name1", someValue: 1)]
        let finalObjectsCount = 1
        let orConditionValueToSearch = 15
        let nameToSearch = "name5"
        
        var predicateConfiguration = NSPredicateConfiguration(with: "name = %@", and: [nameToSearch])
        let predicateCondition = (format: "someValue = %d", arguments: [orConditionValueToSearch] as [Any])
        
        //when
        predicateConfiguration.or(condition: predicateCondition)
        
        let predicate = NSPredicate(with: predicateConfiguration)
        
        let filteredArray = testArray.filtered(using: predicate) as! [ModelMock]
        
        //then
        XCTAssertFalse(filteredArray.isEmpty, "Array is empty")
        XCTAssertEqual(finalObjectsCount, filteredArray.count)
    }
    
}

fileprivate class ModelMock: NSObject {
   @objc var name: String
   @objc var someValue: Int
    
    init(name: String, someValue: Int) {
        self.name = name
        self.someValue = someValue
    }
    
}

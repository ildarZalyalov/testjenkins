//
//  CollectionExtensionsTests.swift
//  Mobile-Mos.ruTests
//
//  Created by Ildar Zalyalov on 18.04.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import XCTest
@testable import Mobile_Mos_ru

class CollectionExtensionsTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testArrayToDictionaryKeysNotEmpty() {
        //given
        let someArray: [Int] = [0,1,2,3,4,5]
        
        //when
        let dictionary = someArray.toDictionary{ $0 }
        
        //then
        XCTAssertFalse(dictionary.keys.isEmpty, "Key are empty")
    }
    
    func testArrayToDictionaryValuesNotEmpty() {
        //given
        let someArray: [Int] = [0,1,2,3,4,5]
        
        //when
        let dictionary = someArray.toDictionary{ $0 }
        
        //then
        XCTAssertFalse(dictionary.values.isEmpty, "Values are empty")
    }
    
    func testArrayToDictionaryElementsCountEqual() {
        //given
        let someArray: [Int] = [0,1,2,3,4,5]
        
        //when
        let dictionary = someArray.toDictionary{ $0 }
        
        //then
        XCTAssertEqual(someArray.count, dictionary.values.count, "Elements count not equal")
    }
    
    func testArrayToDictionaryElementsAreEqual() {
        //given
        let someArray: [Int] = [0,1,2,3,4,5]
        
        //when
        let dictionary = someArray.toDictionary{ $0 }
        
        //then
        XCTAssertEqual(someArray.count, dictionary.values.count, "Elements not equal")
    }
    
    func testSequanceToDictionaryNotEmpty() {
        //given
        let someSequance: Set = ["Some", "value", "to", "test"]
        
        //when
        let dictionary = someSequance.toDictionary{ $0 }
        
        //then
        XCTAssertFalse(dictionary.keys.isEmpty, "Key are empty")
    }
    
    func testSequanceToDictionaryValuesNotEmpty() {
        //given
        let someSequance: Set = ["Some", "value", "to", "test"]
        
        //when
        let dictionary = someSequance.toDictionary{ $0 }
        
        //then
        XCTAssertFalse(dictionary.values.isEmpty, "Values are empty")
    }
    
    func testSequanceToDictionaryElementsCountEqual() {
        //given
        let someSequance: Set = ["Some", "value", "to", "test"]
        
        //when
        let dictionary = someSequance.toDictionary{ $0 }
        
        //then
        XCTAssertEqual(someSequance.count, dictionary.values.count, "Elements count not equal")
    }
    
    func testSequanceToDictionaryElementsAreEqual() {
        //given
        let someSequance: Set = ["Some", "value", "to", "test"]
        
        //when
        let dictionary = someSequance.toDictionary{ $0 }
        
        //then
        XCTAssertEqual(someSequance, Set(dictionary.values.compactMap({ $0 })), "Elements not equal")
    }
    
    func testSafeIndexOfCollectionNotNilIfValid() {
        //given
        let someArray = ["one", "two", "thee"]
        let index = 0
        
        //when
        let value = someArray[safe: index]
        
        //then
        XCTAssertNotNil(value, "Valid value is nil :(")
    }
    
    func testSafeIndexOfCollectionNilIfInValid() {
        //given
        let someArray = ["one", "two", "thee"]
        
        //when
        let value = someArray[safe: someArray.count + 1]
        
        //then
        XCTAssertNil(value, "Valid value is nil :(")
    }
    
    func testSafeIndexOfCollectionSomeValueEqualToParent() {
        //given
        let someArray = ["one", "two", "thee"]
        let index = 0
        
        //when
        let value = someArray[safe: index]
        
        //then
        XCTAssertEqual(value, someArray[index], "Value of same index not equal")
    }
    
    func testToIndicesDictionaryIndexOfFirstValueEqual() {
        //given
        let someArray = ["one", "two", "three"]
        let firstValue = someArray.first
        let firstValueIndex = 0
        
        //when
        let indecesDictionary = someArray.toIndicesDictionary()
        let indexValue = indecesDictionary[firstValueIndex]
        
        //then
        XCTAssertEqual(indexValue, firstValue, "Index not equal")
    }
    
    func testToIndicesDictionaryValuesEqual() {
        //given
        let someArray = ["one", "two", "three"]
        
        //when
        let indecesDictionary = someArray.toIndicesDictionary()
        
        //then
        XCTAssertEqual(someArray.count, indecesDictionary.values.count, "Elements count not equal")
    }
    
    func testToIndicesDictionaryNotEmpty() {
        //given
        let someArray = ["one", "two", "three"]
        
        //when
        let indecesDictionary = someArray.toIndicesDictionary()
        
        //then
        XCTAssertFalse(indecesDictionary.values.isEmpty, "Final sequance is empty")
    }
    
    func testLastArrayValueIsCorrect() {
        //given
        let someArray = ["one", "three", "two", "three", "two"]
        let conditionValue = "three"
        let conditionIndex = 3
        
        //when
        let lastValue = someArray.last { $0.count > 3 }
        
        //then
        XCTAssertNotNil(lastValue, "Last value is empty")
        XCTAssertEqual(conditionValue, lastValue, "Last value not correct")
        XCTAssertEqual(lastValue, someArray[conditionIndex], "Values not equal")
    }
    
    func testLastArrayValueAndIndexIsCorrect() {
        //given
        let someArray = ["one", "three", "two", "three", "two"]
        let index = 2
        let finalSearchedIndex = 4
        let valueAtIndex = someArray[index]
        
        //when
        let lastValueWithIndex = someArray.lastElementAndIndex{ $0 == valueAtIndex }
        
        //then
        XCTAssertNotNil(lastValueWithIndex, "Last value is empty")
        XCTAssertEqual(finalSearchedIndex, lastValueWithIndex?.index, "Indexes not equal")
        XCTAssertEqual(valueAtIndex, lastValueWithIndex?.element, "Values not equal")
    }
    
    func testRemoveDictionaryValueByKeysIsCorrect() {
        //given
        var someDict = ["one":1, "two":2, "three":3, "four":4, "five":5]
        let objectsCount = someDict.count
        let arrayOfKeys = Array(someDict.keys.compactMap({ $0 }).dropFirst())
        
        //when
        someDict.removeValues(for: arrayOfKeys)
        
        //then
        XCTAssert(someDict.count == (objectsCount - arrayOfKeys.count), "Count of final dict not correct")
    }
}

//
//  UIApplicationExtensionsTests.swift
//  Mobile-Mos.ruTests
//
//  Created by Ildar Zalyalov on 27.04.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import XCTest
@testable import Mobile_Mos_ru

class UIApplicationExtensionsTests: XCTestCase {
    var application: UIApplication!
    
    override func setUp() {
        super.setUp()
        application = UIApplication.shared
    }
    
    override func tearDown() {
        application = nil
        super.tearDown()
    }
    
}

//
//  YandexMapPreviewUrlBuilder.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 15.08.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Билдер веб-ссылок на превью карт Яндекса
protocol YandexMapPreviewUrlBuilder {
    
    /// Область относительно координаты
    typealias CoordinateSpan = (longitudeSpan: Double, latitudeSpan: Double)
    
    /// Получить ссылку на превью карты для координаты
    ///
    /// - Parameters:
    ///   - location: координата
    ///   - span: область показа относительно координаты
    ///   - size: размер изображения с превью
    /// - Returns: ссылка на превью карты
    func urlForMapPreview(with location: Location, span: CoordinateSpan, size: CGSize) -> URL
}

//
//  YandexMapPreviewUrlBuilderImplementation.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 15.08.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class YandexMapPreviewUrlBuilderImplementation: YandexMapPreviewUrlBuilder {
    
    /// Максимально поддерживаемое API разрешение картинки
    let maxSize = CGSize(width: 650, height: 450)
    
    var parametersEncoder: HTTPRequestParameterEncoder! {
        didSet {
            parametersEncoder?.encoding = .url
        }
    }
    
    func urlForMapPreview(with location: Location, span: CoordinateSpan, size: CGSize) -> URL {
        
        let scale = UIScreen.main.scale
        
        var requestedSize = size
        requestedSize.width *= scale
        requestedSize.height *= scale
        
        let imageSize = CGSize(width: min(requestedSize.width, maxSize.width),
                               height: min(requestedSize.height, maxSize.height))
        
        let parameters: [String : Any] = [
            "ll" : "\(location.longitude),\(location.latitude)",
            "spn" : "\(span.longitudeSpan),\(span.latitudeSpan)",
            "size" : "\(Int(imageSize.width)),\(Int(imageSize.height))",
            "scale" : scale,
            "l" : "map"]

        let queryString = parametersEncoder.encodeQueryString(parameters)
        let urlString = baseUrlStringYandexMapsStatic + parametersEncoder.queryStringDelimiter + queryString
        
        // здесь ставим ! т.к. у нас есть все параметры, чтобы собрать корректный url
        // если он не собирается, значит имеем критическую ошибку
        return URL(string: urlString)!
    }
}

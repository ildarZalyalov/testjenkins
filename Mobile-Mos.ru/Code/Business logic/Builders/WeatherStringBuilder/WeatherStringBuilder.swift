//
//  WeatherStringBuilder.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 10.04.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Данные для передачи из билдера
/// title - текст с температурой
/// subtitle - описание погоды
typealias WeatherStringBuilderOutput = (title: String, subtitle: String)

/// Билдер строки для погоды
class WeatherStringBuilder {
    
    /// Создать строки погоды из температуры и описания погоды
    ///
    /// - Parameters:
    ///   - temperature: температура в Цельсиях
    ///   - weatherText: описание погоды (а ля "Облачно")
    /// - Returns: данные со строкой
    func buildWeatherString(from temperature: Int, weatherText: String) -> WeatherStringBuilderOutput {
        
        var temperatureChar = ""
        
        if temperature > 0 {
            temperatureChar = "+"
        }
        else if temperature < 0 {
            temperatureChar = "-"
        }
        
        let title = "В Москве \(temperatureChar)\(temperature)°"
        let subtitile = weatherText.lowercased().firstLetterCapitalized()
        
        return (title, subtitile)
    }
}

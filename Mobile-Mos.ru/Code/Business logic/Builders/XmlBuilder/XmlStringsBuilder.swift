//
//  XmlStringsBuilder.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 13.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

class XmlStringsBuilder {
    
    /// Создать строку xml для метода HPSM Create
    ///
    /// - Parameters:
    ///   - email: email отправителя
    ///   - description: описание отправителя
    /// - Returns: строка xml
    func buildHPSMCreateRequestXmlString(from feedbackInfo: FeedbackInfo) -> String {
        
       let problemType = "ПМПМ - Шаблон для регистрации обращений из мобильного приложения Моя Москва"
       let headerXml = "<?xml version=\"1.0\" encoding=\"utf-8\" standalone=\"no\"?>"
        
       let xmlString =
        "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ns=\"http://schemas.hp.com/SM/7\" xmlns:com=\"http://schemas.hp.com/SM/7/Common\" xmlns:xm=\"http://www.w3.org/2005/05/xmlmime\">"
        + "<soapenv:Header/>"
            + "<soapenv:Body>"
                + "<ns:CreateHPSMInteractionsFromMosRuRequest>"
                    + "<ns:model>"
                        + "<ns:keys>"
                        + "<ns:ID></ns:ID>"
                        + "<ns:AdditionalField1>_фос_мп_</ns:AdditionalField1>"
                        + "</ns:keys>"
                        + "<ns:instance>"
                            + "<ns:ID></ns:ID>"
                            + "<ns:OpenTime></ns:OpenTime>"
                            + "<ns:Description>"
                            + "<ns:Description>Суть обращения: \(feedbackInfo.title)"
                            + "\n\(feedbackInfo.feedBackDescription)"
                            + "\n\(feedbackInfo.userPhone)"
                            + "\n\(feedbackInfo.deviceInfo)</ns:Description>"
                            + "</ns:Description>"
                            + "<ns:Email>\(feedbackInfo.email)</ns:Email>"
                            + "<ns:User>\(feedbackInfo.email)</ns:User>"
                            + "<ns:Title>\(feedbackInfo.title)</ns:Title>"
                            + "<ns:ProblemType>\(problemType)</ns:ProblemType>"
                            + "<ns:Portal></ns:Portal>"
                        + "</ns:instance>"
                    + "</ns:model>"
                + "</ns:CreateHPSMInteractionsFromMosRuRequest>"
            + "</soapenv:Body>"
        + "</soapenv:Envelope>"
        
        return headerXml + xmlString
    }
   
}

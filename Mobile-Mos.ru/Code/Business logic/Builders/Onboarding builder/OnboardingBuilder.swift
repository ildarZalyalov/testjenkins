//
//  OnboardingBuilder.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 09.04.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Строитель онбординга
final class OnboardingBuilder {
    
    var storyboardFactory: StoryboardFactory!
    var userDefaults: UserDefaults!
    var notificationCenter: NotificationCenter!
    
    fileprivate let onboardingHideDuration: TimeInterval = 0.4
    
    fileprivate weak var keyWindow: UIWindow?
    fileprivate var onboardingWindow: UIWindow?
    
    deinit {
        notificationCenter?.removeObserver(self)
    }
    
    /// Был ли завершен онбординг
    fileprivate(set) var onboardingWasCompleted: Bool {
        
        get {
            let key = ApplicationUserDefaultsKey.onboardingWasCompleted.rawValue
            return userDefaults.bool(forKey: key)
        }
        
        set {
            
            let key = ApplicationUserDefaultsKey.onboardingWasCompleted.rawValue
            
            userDefaults.set(newValue, forKey: key)
            userDefaults.synchronize()
        }
    }
    
    /// Вызывается, когда главное окно становится ключевым (проверяем внутри метода)
    
    /// Окно онбординга делается ключевым, но только после настройки системой интерфейса
    /// Если сразу сделать ключевым онбординг, то при возврате статуса "ключевое" основному
    /// окну, в нем будут наблюдать визуальные глитчи с клавиатурой
    @objc fileprivate func onWindowBecameKey(notification: Notification) {
        
        guard let window = notification.object as? UIWindow else { return }
        guard window == keyWindow else { return }
        
        onboardingWindow?.makeKey()
        notificationCenter?.removeObserver(self)
    }
    
    /// Показать онбординг, если нужно
    ///
    /// - Parameters:
    ///   - keyWindow: главное окно приложения
    ///   - configurator: конфигуратор для настройки уведомлений
    /// - Returns: был ли показан онбординг
    func presentOnboardingIfNeeded(above keyWindow: UIWindow?, configuringNotificationsWith configurator: RemoteNotificationsConfigurator?) -> Bool {
        
        guard !onboardingWasCompleted else { return false }
        guard let mainWindow = keyWindow else { return false }
        
        let onboardingStoryboard = storyboardFactory.getStoryboard(with: .onboarding)
        guard let onboardingController = onboardingStoryboard.instantiateInitialViewController() else { return false }
        
        if let configurableController = onboardingController as? ConfigurableModuleController {
            
            var configuration = OnboardingModuleConfiguration()
            configuration.onboardingBuilder = self
            configuration.remoteNotificationsConfigurator = configurator
            
            configurableController.configureModule(with: configuration)
        }
        
        self.keyWindow = mainWindow
        
        onboardingWindow = UIWindow(frame: mainWindow.bounds)
        onboardingWindow?.windowLevel = UIWindowLevelStatusBar
        onboardingWindow?.backgroundColor = UIColor.white
        onboardingWindow?.rootViewController = onboardingController
        
        if mainWindow.isKeyWindow {
            onboardingWindow?.makeKeyAndVisible()
        }
        else {
            onboardingWindow?.isHidden = false
            notificationCenter.addObserver(self, selector: #selector(onWindowBecameKey(notification:)), name: .UIWindowDidBecomeKey, object: nil)
        }
        
        return true
    }
    
    /// Спрятать онбординг
    ///
    /// - Parameter animated: применять ли анимацию
    func hideOnboarding(animated: Bool) {
        
        let hideOnboardingClosure: () -> () = { [weak self] in
            
            self?.onboardingWasCompleted = true
            
            self?.keyWindow?.makeKeyAndVisible()
            
            self?.onboardingWindow?.isHidden = true
            self?.onboardingWindow = nil
        }
        
        guard animated else {
            hideOnboardingClosure()
            return
        }
        
        UIView.animate(withDuration: onboardingHideDuration, animations: { [weak self] in
            self?.onboardingWindow?.alpha = 0
        }) { _ in
            hideOnboardingClosure()
        }
    }
}

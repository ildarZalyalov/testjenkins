//
//  OnboardingBuilderConfigurator.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 09.04.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Конфигурация зависимостей для OnboardingBuilder
extension OnboardingBuilder {
    
    static func getOnboardingBuilder() -> OnboardingBuilder {
        
        let onboardingBuilder = OnboardingBuilder()
        let storyboardFactory = StoryboardFactoryImplementation()
        let userDefaults = UserDefaults.standard
        let notificationCenter = NotificationCenter.default
        
        onboardingBuilder.storyboardFactory = storyboardFactory
        onboardingBuilder.userDefaults = userDefaults
        onboardingBuilder.notificationCenter = notificationCenter
        
        return onboardingBuilder
    }
}

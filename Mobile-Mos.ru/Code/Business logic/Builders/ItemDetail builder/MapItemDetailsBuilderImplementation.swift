//
//  MapItemDetailsBuilderImplementation.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 20.09.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class MapItemDetailsBuilderImplementation: ItemDetailsBuilder {
    
    fileprivate let emptyTextPlaceholder = " - "
    fileprivate let prefixToPhone = "+7 "
    fileprivate let distanceStringPostfix = " от вас"
    fileprivate let colonString = ": "
    fileprivate let roadLinesSeparator = " из "
    fileprivate let datesSeparator = " — "
    fileprivate let lineSpacing: CGFloat = 1.9
    
    fileprivate lazy var dateFormatter: DateFormatter = {
       let formatter = DateFormatter()
       formatter.dateFormat = "dd.MM.yyyy"
        return formatter
    }()
    
    fileprivate lazy var lastUpdateDateFormatter: DateFormatter = {
        
        let localeIdentifier = "ru_RU"
        
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: localeIdentifier)
        formatter.timeZone = TimeZone.autoupdatingCurrent
        formatter.dateFormat = "dd MMMM HH:mm"
        
        return formatter
    }()
    
    func createItemDetails(from item: MapItem,
                           formattedDistance: FormattedDistanceStrings?) -> MapItemDetailDataSource {
        
        var datasource = MapItemDetailDataSource()
        var isInfoViewHidden = true
    
        let nameWithState        = check(optionalString: item.name)
        let adressWithState      = check(optionalString: item.address)
        let cellPhoneWithState   = check(optionalString: item.publicPhone)
        let webAdressWithState   = check(optionalString: item.website)
       
        let workingHours         = getStringWorkingHours(from: item.workingHours)
        let descriptionWithState = getAttributedDescription(from: item.itemDescription)
        let distanceText         = getDistanceString(from: formattedDistance)
        
        isInfoViewHidden = adressWithState.isHidden && cellPhoneWithState.isHidden && webAdressWithState.isHidden
        
        datasource.name         = nameWithState
        datasource.description  = descriptionWithState
        datasource.address      = adressWithState
        datasource.cellPhone    = (prefixToPhone + cellPhoneWithState.text, cellPhoneWithState.isHidden)
        datasource.webAdress    = webAdressWithState
        datasource.distance     = distanceText
        datasource.workingHours = (workingHours, !workingHours.isEmpty ? false : true)
        datasource.isInfoViewHidden = isInfoViewHidden
        
        return datasource
    }
    
    func createItemDetails(from item: Overlap) -> OverlapItemDetailDataSource {
        
        var datasource = OverlapItemDetailDataSource()
       
        var dateString = ""
        let toDate = dateFormatter.string(from: item.toDate)
        let fromDate = dateFormatter.string(from: item.fromDate)
        
        dateString = fromDate + datesSeparator + toDate
        
        let dateToDateFromString = dateString
        
        let lastUpdateDateString = StringsHelper.dataUpToDateString + " \(lastUpdateDateFormatter.string(from: item.lastUpdateDate))"
        
        let roadLinesString = "\(item.lanesClosed)" + roadLinesSeparator + "\(item.lanesAvailable)"
        
        let nameWithState            = check(optionalString: item.name)
        let adressWithState          = check(optionalString: item.address)
        let cellPhoneWithState       = check(optionalString: item.phone)
        let descriptionWithState     = check(optionalString: item.description)
        let dateToDateFromWithState  = check(optionalString: dateToDateFromString)
        let roadLinesWithState       = check(optionalString: roadLinesString)
        let requesterWithState       = check(optionalString: item.requester)
        let customerWithState        = check(optionalString: item.customer)
        let okrugWithState           = check(optionalString: item.okrugName)
        let overlapTypeWithState     = check(optionalString: item.eventType)
        let lastUpdateDateWithState  = check(optionalString: lastUpdateDateString)
            
        datasource.name           = nameWithState
        datasource.description    = descriptionWithState
        datasource.address        = adressWithState
        datasource.cellPhone      = (cellPhoneWithState.text, cellPhoneWithState.isHidden)
        datasource.dateToDateFrom = dateToDateFromWithState
        datasource.roadLines      = roadLinesWithState
        datasource.requester      = requesterWithState
        datasource.customer       = customerWithState
        datasource.okrug          = okrugWithState
        datasource.overlapType    = overlapTypeWithState
        datasource.lastUpdateDate = lastUpdateDateWithState
        
        addShareString(to: &datasource)
        
        return datasource
    }
    
    /// Проверить опциональную строку и вернуть тупль со строкой и стейтом
    ///
    /// - Parameter optionalString: опциональная строка
    /// - Returns: тупль со строкой и стейтом
    fileprivate func check(optionalString: String?) -> DetailWithState {
        
        if let string = optionalString, !string.isEmpty {
            
            return (string, false)
        }
        return ("", true)
    }
    
    
    fileprivate func getStringWorkingHours(from workingHours: [MapItemWorkingHour]) -> [String] {
        
        var arrayOfHours = [String]()
        workingHours.forEach{
            arrayOfHours.append( $0.dayOfWeek.firstLetterCapitalized()
                + colonString + $0.hours
            )}
        return arrayOfHours
    }
    
    fileprivate func addShareString(to datasource: inout OverlapItemDetailDataSource) {
        
        var shareString = ""
        
        shareString += "\nДата перекрытия: \n" + datasource.dateToDateFrom.text
        
        if !datasource.name.isHidden {
            shareString += "\nНазвание: \n" + datasource.name.text
        }
        
        if !datasource.description.isHidden {
            shareString += "\nОписание: \n" + datasource.description.text
        }
        if !datasource.cellPhone.isHidden {
            shareString += "\nТелефон: \n" + datasource.cellPhone.text
        }
        if !datasource.requester.isHidden {
            shareString += "\nЗаказчик: \n" + datasource.requester.text
        }
        if !datasource.overlapType.isHidden {
            shareString += "\nТип перекрытия: \n" + datasource.overlapType.text
        }
        if !datasource.customer.isHidden {
            shareString += "\nПодрядчик: \n" + datasource.customer.text
        }
        if !datasource.address.isHidden {
            shareString += "\nАдрес: \n" + datasource.address.text
        }
        if !datasource.okrug.isHidden {
             shareString += "\nОкруг: \n" + datasource.okrug.text
        }
        
        datasource.shareString = shareString
    }
    
    /// Получить атрибутед строку с состоянием из опциональной строки
    ///
    /// - Parameter string: опциональная строка
    /// - Returns: тупль с атрибутед строкой и состоянием
    fileprivate func getAttributedDescription(from string: String?) -> AttributedDetailWithState {
        
        let state = check(optionalString: string).isHidden
        let description = string ?? ""
        
        let attributeString = NSMutableAttributedString(string: description)
        let style = NSMutableParagraphStyle()
        style.lineSpacing = lineSpacing
        attributeString.addAttribute(NSAttributedStringKey.paragraphStyle, value: style, range: NSRange(location: 0, length: description.count))
        
        return (attributeString, state)
    }
    
    /// Получить готовую строку с дистанцией из опционального форматтера
    ///
    /// - Parameter formattedStrings: опциональный форматтер
    /// - Returns: готовая строка с дистанцией
    fileprivate func getDistanceString(from formattedStrings: FormattedDistanceStrings?) -> String {
        
        guard let distance = formattedStrings  else {
            return emptyTextPlaceholder + distanceStringPostfix
        }
        
       let finalString = distance.distanceString + " " + distance.distanceUnitString + distanceStringPostfix
        
        return finalString
    }
}


//
//  ItemDetailsBuilder.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 20.09.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Тупль, хранящий строку и состояние для конкретного поля
typealias DetailWithState = (text: String, isHidden: Bool)

/// Тупль, хранящий атрибутет строку и состояние для конкретного поля
typealias AttributedDetailWithState = (attributedString: NSAttributedString, isHidden: Bool)

/// Тупль, хранящий массив строк и состояние для конкретного поля
typealias WorkHoursArrayWithState = (array: [String], isHidden: Bool)

/// Объект с полями для детального отображения карточки объекта
struct MapItemDetailDataSource {
    
    var name:           DetailWithState = ("", true)
    var description:    AttributedDetailWithState = (NSAttributedString(), true)
    var workingHours:   WorkHoursArrayWithState = ([String](), true)
    var address:         DetailWithState = ("", true)
    var distance:       String          =  ""
    var cellPhone:      DetailWithState = ("", true)
    var webAdress:      DetailWithState = ("", true)
    var isInfoViewHidden:Bool = true
}

/// Объект с полями для детального отображения карточки объекта перекрытия
struct OverlapItemDetailDataSource {
    
    var name:           DetailWithState = ("", true)
    var dateToDateFrom: DetailWithState = ("", true)
    var roadLines:      DetailWithState = ("", true)
    var description:    DetailWithState = ("", true)
    var address:        DetailWithState = ("", true)
    var cellPhone:      DetailWithState = ("", true)
    var requester:      DetailWithState = ("", true)
    var customer:       DetailWithState = ("", true)
    var okrug:          DetailWithState = ("", true)
    var overlapType:    DetailWithState = ("", true)
    var lastUpdateDate: DetailWithState = ("", true)
    
    var shareString: String = ""
}

protocol ItemDetailsBuilder: class {
    
    /// Создать объект с данными для карточки объекта карты
    ///
    /// - Parameter item: объект карты
    /// - Returns: объект с полями для детального отображения карточки объекта
    func createItemDetails(from item: MapItem, formattedDistance: FormattedDistanceStrings?) -> MapItemDetailDataSource
    
    /// Создать объект с данными для карточки объекта перекрытия
    ///
    /// - Parameter item: объект карты
    /// - Returns: объект с полями для детального отображения карточки объекта
    func createItemDetails(from item: Overlap) -> OverlapItemDetailDataSource
}


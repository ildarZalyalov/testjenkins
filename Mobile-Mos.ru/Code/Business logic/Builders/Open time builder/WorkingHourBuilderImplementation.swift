//
//  WorkingHourBuilderImplementation.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 20.09.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class WorkingHourBuilderImplementation: WorkingHourBuilder {
    
    fileprivate let dayDateFormat = "EEEE"
    fileprivate let localeIdentifier = "ru_RU"
    fileprivate let fromHourLetterOffset = 2
    fileprivate let beforeHourLetterOffset = -5
    fileprivate let fromHourHoursLetterOffset = -3
    fileprivate let maxHourOffset = -3
    fileprivate let maxHourMinutsOffset = -2
    fileprivate let convenienceTime = "00:00-24:00"
    
    fileprivate var currentDate: Date = {
       return Date()
    }()
    
    fileprivate lazy var dateFormatter: DateFormatter = {

        let formatter = DateFormatter()
        formatter.dateFormat = self.dayDateFormat
        formatter.locale = Locale(identifier: localeIdentifier)
        
        return formatter
    }()
    
    func checkOpenHour(with workingHours: [MapItemWorkingHour]) -> WorkingHour? {
        
        guard !workingHours.isEmpty else { return nil }
        
        let currentDayName = dateFormatter.string(from: currentDate).capitalized
        
        //проверяем по первому значению, что здание/место работает не круглостуточно
        guard workingHours.first?.hours != StringsHelper.conveniencePlaceName else {
            
            return (true, StringsHelper.conveniencePlaceName)
        }
        
        //проверяем по первому значению, что здание/место в принципе открыто
        guard workingHours.first?.hours != StringsHelper.closedPlaceName else {
            
            return (false, StringsHelper.closedPlaceName)
        }
        
        for workingHour in workingHours {
            
            if workingHour.dayOfWeek.lowercased() == currentDayName.lowercased() {
                
                guard workingHour.hours.lowercased() != StringsHelper.weekendString else {
                    return (false, StringsHelper.weekendString.firstLetterCapitalized())
                }
                
                guard workingHour.hours.lowercased() != convenienceTime else {
                    return (true, StringsHelper.conveniencePlaceName)
                }
                
                let beginIndex = workingHour.hours.index(workingHour.hours.startIndex, offsetBy: fromHourLetterOffset)
                let endIndex = workingHour.hours.index(workingHour.hours.endIndex, offsetBy: beforeHourLetterOffset)
                
                let minHourStringForToday = String(workingHour.hours[..<beginIndex])
                let maxHourStringForToday = String(workingHour.hours[endIndex...])
                
                let hourIndex = maxHourStringForToday.index(maxHourStringForToday.endIndex, offsetBy: maxHourOffset)
                let minutsIndex = maxHourStringForToday.index(maxHourStringForToday.endIndex, offsetBy: maxHourMinutsOffset)
                
                let maxMinutsInt = Int(maxHourStringForToday[minutsIndex...])
                
                if let maxHourInt = Int(maxHourStringForToday[..<hourIndex]),
                   let minHourInt = minHourStringForToday.first == "0" ? Int (minHourStringForToday[minHourStringForToday.startIndex...]) : Int(minHourStringForToday),
                   let maxMinute = maxMinutsInt {
            
                    let openState = checkTime(minHourInt, maxTime: maxHourInt, maxMinute: maxMinute)
                    return (openState,
                            openState ? maxHourStringForToday : minHourStringForToday + ":00")
                }
            }
        }
        
        return nil
    }
    
    //MARK: Helpers
    
    /// Проверить время на вхождение в отрезок от минимума до максимума, учитывая время типа "14:00 - 02:00"
    ///
    /// - Parameters:
    ///   - minTime: минимальный час
    ///   - maxTime: максимальный час
    ///   - maxMinute: максимальная минута
    /// - Returns: булевое значение, входит или не входит
    fileprivate func checkTime(_ minTime: Int, maxTime: Int,  maxMinute: Int) -> Bool {
        
        var isInsideRange = false
        
        let calendar = Calendar.current
        let now = Date()
        
        if let minDate = calendar.date(
            bySettingHour: minTime,
            minute: 0,
            second: 0,
            of: now),
            let maxDate = calendar.date(
                bySettingHour: maxTime,
                minute: maxMinute,
                second: 0,
                of: now) {
            
            if now >= minDate && now <= maxDate {
                isInsideRange = true
            }
        }
        
        return isInsideRange
    }
}

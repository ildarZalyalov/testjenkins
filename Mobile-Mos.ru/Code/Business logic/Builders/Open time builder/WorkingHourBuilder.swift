//
//  WorkingHourBuilder.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 20.09.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Время работы
typealias WorkingHour = (isOpen: Bool, time: String)

/// Протокол билдера для проверки открыт или закрыт объект по времени
protocol WorkingHourBuilder: class {
    
    
    /// Проверить открыт ли объект от рабочего времени и текущего времени
    ///
    /// - Parameter workingHours: массив рабочих дней/часов
    /// - Returns:  стейт открыт/закрыт, рабочее время (если открыт то до скольких, если закрыт то до скольких)
    func checkOpenHour(with workingHours: [MapItemWorkingHour]) -> WorkingHour?
}

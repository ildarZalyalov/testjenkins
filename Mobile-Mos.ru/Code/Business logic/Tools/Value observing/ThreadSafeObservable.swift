//
//  StateObservable.swift
//  Portable
//
//  Created by Ivan Erasov on 09.11.2017.
//  Copyright © 2017. All rights reserved.
//

import Foundation

/// Значение, за которым можно следить с любого потока
class ThreadSafeObservable<ObservedType> {
    
    /// Обработчик изменения значения
    typealias ObservedValueChangedHandler = (_ oldValue: ObservedType, _ newValue: ObservedType) -> ()
    
    /// Наблюдатель за значением
    fileprivate struct Observer<ObservedType> {
        
        /// Слабая ссылка на объект, который наблюдает за изменениями
        weak var observerObject: AnyObject?
        
        /// Обработчик, который вызывается при изменении
        let handler: ObservedValueChangedHandler
        
        init(observerObject: AnyObject, handler: @escaping ObservedValueChangedHandler) {
            self.observerObject = observerObject
            self.handler = handler
        }
    }
    
    fileprivate let handlingQueue = DispatchQueue(label: "com.any.app.thread.safe.value.observing")
    fileprivate lazy var observers = [Observer<ObservedType>]()
    
    /// Для синхронизации используется последовательная очередь
    /// Вызывающий поток не ждет окончания вызова обработчиков нотификаций
    /// Обработчики будут вызываться также на этой очереди
    /// self не захватывается блоками т.к. обработка нотификаций может быть сколь угодно долгой
    
    init(_ value: ObservedType) {
        self.value = value
    }
    
    /// Значение, за которым наблюдают
    var value: ObservedType {
        didSet {
            handlingQueue.async { [weak self] in
                self?.cleanDeadObservers()
                self?.notifyObservers(oldValue: oldValue)
            }
        }
    }
    
    //MARK: - Приватные методы (доступ к ним должен быть синхронизирован)
    
    /// Добавить наблюдателя
    ///
    /// - Parameter observer: наблюдатель
    fileprivate func add(observer: Observer<ObservedType>) {
        observers.append(observer)
    }
    
    /// Удалить ссылки на наблюдателей, объекты которых был уничтожены
    fileprivate func cleanDeadObservers() {
        observers = observers.filter { $0.observerObject != nil }
    }
    
    /// Сообщить текущим наблюдателям об изменении
    ///
    /// - Parameter oldValue: старое значение
    fileprivate func notifyObservers(oldValue: ObservedType) {
        for observer in observers {
            observer.handler(oldValue, value)
        }
    }
    
    //MARK: - Открытые методы
    
    /// Добавить объект, который будет получать нотификации об изменениях
    ///
    /// - warning: Обработчик будет вызываться НЕ на главном потоке
    /// - Parameters:
    ///   - observer: объект
    ///   - handler: обработчик изменений
    func add(observer: AnyObject, handler: @escaping ObservedValueChangedHandler) {
        handlingQueue.async { [weak self] in
            self?.add(observer: Observer(observerObject: observer, handler: handler))
            self?.cleanDeadObservers()
        }
    }
}

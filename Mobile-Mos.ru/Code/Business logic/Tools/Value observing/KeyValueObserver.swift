//
//  KVOObserver.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 09.11.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Наблюдатель за значением по KVO (для объектов, которые не NSObject сами)
class KeyValueObserver: NSObject {
    
    /// Обработчик изменения значения
    typealias ObservedValueChangedHandler = (_ change: [NSKeyValueChangeKey : Any]?) -> ()
    
    /// Объект, который наблюдает за изменениями
    let observedObject: NSObject
    
    /// Key path, за которым наблюдают
    let observedKeyPath: String
    
    /// Контекст наблюдения
    let observationContext: UnsafeMutableRawPointer?
    
    /// Обработчик изменения значения
    let handler: ObservedValueChangedHandler
    
    init(observedObject: NSObject,
         observedKeyPath: String,
         observationContext: UnsafeMutableRawPointer?,
         handler: @escaping ObservedValueChangedHandler) {
        
        self.observedObject = observedObject
        self.observedKeyPath = observedKeyPath
        self.observationContext = observationContext
        self.handler = handler
        
        super.init()
        
        observedObject.addObserver(self, forKeyPath: observedKeyPath, options: [.initial, .old, .new], context: observationContext)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        guard keyPath == observedKeyPath && context == observationContext else {
            super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
            return
        }
        
        handler(change)
    }
    
    deinit {
        observedObject.removeObserver(self,
                                      forKeyPath: observedKeyPath,
                                      context: observationContext)
    }
}

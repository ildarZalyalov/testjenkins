//
//  WeblinkPreviewLoaderImplementation.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 14.09.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class WeblinkPreviewLoaderImplementation: WeblinkPreviewLoader {
    
    let userAgentHeaderKey = "User-Agent"
    let userAgentHeaderValue = "Mozilla/5.0 (compatible; YandexBot/3.0; +http://yandex.com/bots)"
    
    let trueValueString = "true"
    
    var openGraphParser: OpenGraphParser!
    
    fileprivate func call(completion: @escaping WeblinkPreviewDataObtainHandler, with data: WeblinkPreviewData) {
        
        if Thread.isMainThread {
            completion(data)
        } else {
            DispatchQueue.main.async(execute: {
                completion(data)
            })
        }
    }
    
    //MARK: - WeblinkPreviewLoader
    
    func obtainPreview(for url: URL, requireUserGeneratedImages: Bool, completion: @escaping WeblinkPreviewDataObtainHandler) {
        
        var request = URLRequest(url: url)
        request.setValue(userAgentHeaderValue, forHTTPHeaderField: userAgentHeaderKey)
        
        URLSession.shared.dataTask(with: request) { [weak self] (data, _, _) in
            
            guard let strongSelf = self else { return }
            
            var previewData = WeblinkPreviewData()
            
            defer {
                strongSelf.call(completion: completion, with: previewData)
            }
            
            guard let htmlData = data else {
                return
            }
            
            guard let htmlString = String(data: htmlData, encoding: .utf8) else {
                return
            }
            
            let openGraphData = strongSelf.openGraphParser.parse(html: htmlString)
            
            var shouldTryParseImage = true
            if requireUserGeneratedImages {
                shouldTryParseImage = openGraphData[.imageUserGenerated] == strongSelf.trueValueString
            }
            
            previewData.title = openGraphData[.title]
            previewData.description = openGraphData[.description]
            previewData.topImage = shouldTryParseImage ? openGraphData[.image] : nil
            previewData.topVideo = openGraphData[.video]
            
        }.resume()
    }
}

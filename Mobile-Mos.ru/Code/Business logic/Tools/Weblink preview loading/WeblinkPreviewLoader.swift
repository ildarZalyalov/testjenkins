//
//  WeblinkPreviewLoader.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 14.09.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Данные для превью по веб-ссылке
struct WeblinkPreviewData {
    
    /// Заголовок
    var title: String?
    
    /// Описание
    var description: String?
    
    /// Главная картинка
    var topImage: String?
    
    /// Главное видео
    var topVideo: String?
}

/// Обработчик получения данных для превью по веб-ссылке
typealias WeblinkPreviewDataObtainHandler = (WeblinkPreviewData) -> ()

/// Загрузчик превью для веб-ссылок
protocol WeblinkPreviewLoader {
    
    /// Получить превью для веб-ссылки
    ///
    /// - Parameters:
    ///   - url: веб-ссылка
    ///   - requireUserGeneratedImages: нужно ли для главной картинки парсить только картинки, сгенерированные пользователем (image:user_generated)
    ///   - completion: обработчик получения данных для превью
    func obtainPreview(for url: URL, requireUserGeneratedImages: Bool, completion: @escaping WeblinkPreviewDataObtainHandler)
}

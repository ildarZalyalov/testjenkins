//
//  OpenGraphParser.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 18.09.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Ключи для метаданных Open Graph, которые будут распарсены
enum OpenGraphMetadata: String {
    
    // Базовые данные
    case title
    case type
    case image
    case url
    
    // Опционально
    case audio
    case description
    case determiner
    case locale
    case localeAlternate = "locale:alternate"
    case siteName = "site_name"
    case video
    
    // Картинка
    case imageUrl           = "image:url"
    case imageSecure_url    = "image:secure_url"
    case imageType          = "image:type"
    case imageWidth         = "image:width"
    case imageHeight        = "image:height"
    case imageUserGenerated = "image:user_generated"
    
    // Музыка
    case musicDuration    = "music:duration"
    case musicAlbum       = "music:album"
    case musicAlbumDisc   = "music:album:disc"
    case musicAlbumMusic  = "music:album:track"
    case musicMusician    = "music:musician"
    case musicSong        = "music:song"
    case musicSongDisc    = "music:song:disc"
    case musicSongTrack   = "music:song:track"
    case musicReleaseDate = "music:releaseDate"
    case musicCreator     = "music:creator"
    
    // Видео
    case videoActor       = "video:actor"
    case videoActorRole   = "video:actor:role"
    case videoDirector    = "video:director"
    case videoWriter      = "video:writer"
    case videoDuration    = "video:duration"
    case videoReleaseDate = "video:releaseDate"
    case videoTag         = "video:tag"
    case videoSeries      = "video:series"
    
    // Статья
    case articlePublishedTime  = "article:publishedTime"
    case articleModifiedTime   = "article:modifiedTime"
    case articleExpirationTime = "article:expirationTime"
    case articleAuthor         = "article:author"
    case articleSection        = "article:section"
    case articleTag            = "article:tag"
    
    // Книга
    case bookAuthor      = "book:author"
    case bookIsbn        = "book:isbn"
    case bookReleaseDate = "book:releaseDate"
    case bookTag         = "book:tag"
    
    // Профиль
    case profileFirstName = "profile:firstName"
    case profileLastName  = "profile:lastName"
    case profileUsername  = "profile:username"
    case profileGender    = "profile:gender"
}

/// Парсер Open Graph метаданных
protocol OpenGraphParser {
    
    /// Распарсить HTML страницу
    ///
    /// - Parameter html: страница HTML
    /// - Returns: Open Graph метаданные
    func parse(html: String) -> [OpenGraphMetadata: String]
}

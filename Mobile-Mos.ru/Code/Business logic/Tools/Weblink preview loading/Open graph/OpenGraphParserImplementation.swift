//
//  OpenGraphParserImplementation.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 18.09.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class OpenGraphParserImplementation: OpenGraphParser {
    
    let mnemonicStrings: [String : String] = ["&nbsp;" : "",
                                              "&lt;" : "<",
                                              "&gt;" : ">",
                                              "&quot;" : "\"",
                                              "&amp;" : "&",
                                              "&laquo;" : "«",
                                              "&raquo;" : "»",
                                              "&amp;laquo;" : "«",
                                              "&amp;raquo;" : "»",
                                              "&sect;" : "§",
                                              "&copy;" : "©",
                                              "&plusmn;" : "±"]
    
    let trimmedCharactersSet = CharacterSet(charactersIn: "“”")
    
    func parse(html: String) -> [OpenGraphMetadata: String] {
        
        // regular expressions для извлечения тэга meta
        let metatagRegex  = try! NSRegularExpression(
            pattern: "<meta(?:\".*?\"|\'.*?\'|[^\'\"])*?>",
            options: [.dotMatchesLineSeparators]
        )
        
        let metaTagMatches = metatagRegex.matches(in: html,
                                                  options: [],
                                                  range: NSMakeRange(0, html.count))
        if metaTagMatches.isEmpty {
            return [:]
        }
        
        // regular expressions для извлечения свойств og и их content
        let propertyRegexp = try! NSRegularExpression(
            pattern: "\\sproperty=(?:\"|\')og:([a-zA_Z:]+?)(?:\"|\')",
            options: []
        )
        let contentRegexp = try! NSRegularExpression(
            pattern: "\\scontent=(?:\"|\')(.*?)(?:\"|\')",
            options: []
        )
        
        let nsString = html as NSString
        
        let attributes = metaTagMatches.reduce([OpenGraphMetadata: String]()) { (attributes, result) -> [OpenGraphMetadata: String] in
            
            var copiedAttributes = attributes
            
            let property = { [weak self] () -> (name: String, content: String)? in
                
                guard let strongSelf = self else { return nil }
                guard result.numberOfRanges > 0 else { return nil }
                
                let metaTag = nsString.substring(with: result.range(at: 0))
                let nsMetaTag = metaTag as NSString
                
                let propertyMatches = propertyRegexp.matches(in: metaTag,
                                                             options: [],
                                                             range: NSMakeRange(0, metaTag.count))
                
                guard let propertyResult = propertyMatches.first else { return nil }
                guard propertyResult.numberOfRanges > 1 else { return nil }
                
                let contentMatches = contentRegexp.matches(in: metaTag, options: [], range: NSMakeRange(0, metaTag.count))
                
                guard let contentResult = contentMatches.first else { return nil }
                guard contentResult.numberOfRanges > 1 else { return nil }
                
                let property = nsMetaTag.substring(with: propertyResult.range(at: 1))
                var content = nsMetaTag.substring(with: contentResult.range(at: 1))
                
                for (encoded, decoded) in strongSelf.mnemonicStrings {
                    content = content.replacingOccurrences(of: encoded, with: decoded)
                }
                
                return (name: property, content: content)
            }()
            
            if let property = property, let metadata = OpenGraphMetadata(rawValue: property.name) {
                copiedAttributes[metadata] = property.content.trimmingCharacters(in: trimmedCharactersSet)
            }
            
            return copiedAttributes
        }
        
        return attributes
    }
}

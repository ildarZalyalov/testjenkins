//
//  Socket.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 04.05.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Протокол для типов, которые можно пересылать по сокету (используется для ограничения типов, которые передаются как аргументы при отправке)
protocol SocketTransferable {}

extension Bool : SocketTransferable {}
extension Double : SocketTransferable {}
extension Int : SocketTransferable {}
extension Array : SocketTransferable {}
extension NSArray : SocketTransferable {}
extension Dictionary : SocketTransferable {}
extension NSDictionary : SocketTransferable {}
extension Data : SocketTransferable {}
extension NSData : SocketTransferable {}
extension String : SocketTransferable {}
extension NSString : SocketTransferable {}
extension NSNull : SocketTransferable {}

/// Стандартные события для сокета
///
/// - connect: успешное соединение
/// - disconnect: разрыв соединения
/// - reconnect: начало попыток восстановить соединение
/// - reconnectAttempt: начало конкретной попытки восстановления соединения
/// - statusChange: изменение статуса соединения (подключения к серверу)
/// - error: ошибка (сервер ответил не согласно протоколу)
enum SocketEvent: String {
    case connect
    case disconnect
    case reconnect
    case reconnectAttempt
    case statusChange
    case error
}

extension NSNotification.Name {
    
    static let SocketDidReceiveConnect: NSNotification.Name = NSNotification.Name("SocketDidReceiveConnect")
    
    static let SocketDidReceiveDisconnect: NSNotification.Name = NSNotification.Name("SocketDidReceiveDisconnect")
}

/// Блок обработки подтверждения получения сообщения сервером, первый аргумент равен true, если блок вызвался по таймауту
typealias SocketEventConfirmationHandler = (Bool, [Any]) -> Void

/// Блок отправки подверждения получения сообщения серверу
typealias SocketEventConfirmationEmitter = ([SocketTransferable]) -> Void

/// Блок обработки сообщения от сервера, первый параметр - массив аргументов сообщения
typealias SocketEventHandler = ([Any], SocketEventConfirmationEmitter) -> Void

/// Протокол описывает соединение по сокету
protocol Socket: class {
    
    /// Подключен ли сокет к серверу
    var isConnected: Bool { get }
    
    /// Подключиться к серверу
    func connect()
    
    /// Отключиться от сервера
    func disconnect()
    
    /// Отправить сообщение. Если у сокета проблемы с подключением, то сообщение будет отправлено при восстановлении соединения.
    ///
    /// - Parameter event: сообщение
    func send(event: String)
    
    /// Отправить сообщение. Если у сокета проблемы с подключением, то сообщение будет отправлено при восстановлении соединения.
    ///
    /// - Parameters:
    ///   - event: сообщение
    ///   - arguments: параметры сообщения (любые дополнительные данные)
    func send(event: String, with arguments: [SocketTransferable])
    
    /// Отправить сообщение с подтверждением от сервера о получении. Если у сокета проблемы с подключением, то попытки отправки прекратятся по таймауту.
    ///
    /// - Parameters:
    ///   - event: сообщение
    ///   - seconds: время ожидания подтверждения в секундах
    ///   - handler: обработчик, который вызовется при получении подтверждения или окончании времени ожидания
    func send(event: String, timingOutAfter seconds: Double, withConfirmation handler: @escaping SocketEventConfirmationHandler)
    
    /// Отправить сообщение с подтверждением от сервера о получении. Обработчик подтверждения будет вызван не на главном потоке. Если у сокета проблемы с подключением, то попытки отправки прекратятся по таймауту.
    ///
    /// - Parameters:
    ///   - event: сообщение
    ///   - seconds: время ожидания подтверждения в секундах
    ///   - handler: обработчик, который вызовется при получении подтверждения или окончании времени ожидания
    func send(event: String, timingOutAsyncAfter seconds: Double, withConfirmation handler: @escaping SocketEventConfirmationHandler)
    
    /// Отправить сообщение с подтверждением от сервера о получении. Если у сокета проблемы с подключением, то попытки отправки прекратятся по таймауту.
    ///
    /// - Parameters:
    ///   - event: сообщение
    ///   - arguments: параметры сообщения (любые дополнительные данные)
    ///   - seconds: время ожидания подтверждения в секундах
    ///   - handler: обработчик, который вызовется при получении подтверждения или окончании времени ожидания
    func send(event: String, with arguments: [SocketTransferable], timingOutAfter seconds: Double, withConfirmation handler: @escaping SocketEventConfirmationHandler)
    
    /// Отправить сообщение с подтверждением от сервера о получении. Обработчик подтверждения будет вызван не на главном потоке. Если у сокета проблемы с подключением, то попытки отправки прекратятся по таймауту.
    ///
    /// - Parameters:
    ///   - event: сообщение
    ///   - arguments: параметры сообщения (любые дополнительные данные)
    ///   - seconds: время ожидания подтверждения в секундах
    ///   - handler: обработчик, который вызовется при получении подтверждения или окончании времени ожидания
    func send(event: String, with arguments: [SocketTransferable], timingOutAsyncAfter seconds: Double, withConfirmation handler: @escaping SocketEventConfirmationHandler)
    
    /// Добавить обработчик для сообщения
    ///
    /// - Parameters:
    ///   - event: сообщение
    ///   - handler: обработчик, который вызовется при получении сообщения
    /// - Returns: идентификатор обработчика
    @discardableResult func on(event: String, handler: @escaping SocketEventHandler) -> UUID
    
    /// Добавить обработчик для сообщения, который будет выполняться не на главном потоке
    ///
    /// - Parameters:
    ///   - event: сообщение
    ///   - handler: обработчик, который вызовется при получении сообщения
    /// - Returns: идентификатор обработчика
    @discardableResult func onAsync(event: String, handler: @escaping SocketEventHandler) -> UUID
    
    /// Добавить обработчик для сообщения, который выполнится лишь один раз
    ///
    /// - Parameters:
    ///   - event: сообщение
    ///   - handler: обработчик, который вызовется при получении сообщения
    /// - Returns: идентификатор обработчика
    @discardableResult func once(event: String, handler: @escaping SocketEventHandler) -> UUID
    
    /// Добавить обработчик для сообщения, который выполнится лишь один раз не на главном потоке
    ///
    /// - Parameters:
    ///   - event: сообщение
    ///   - handler: обработчик, который вызовется при получении сообщения
    /// - Returns: идентификатор обработчика
    @discardableResult func onceAsync(event: String, handler: @escaping SocketEventHandler) -> UUID
    
    /// Убрать все обработчики для сообщения
    ///
    /// - Parameter event: сообщение
    func off(event: String)
    
    /// Убрать конкретный обработчик
    ///
    /// - Parameter handlerId: идентификатор обработчика
    func off(handlerId: UUID)
}

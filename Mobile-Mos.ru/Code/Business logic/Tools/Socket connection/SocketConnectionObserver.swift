//
//  SocketConnectionObserver.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 01.09.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Делегат наблюдателя за соединением
protocol SocketConnectionObserverDelegate: class {
    
    /// Изменилось состояние соединения
    ///
    /// - Parameters:
    ///   - observer: наблюдатель
    ///   - connected: есть ли соединение
    func socketObserver(observer: SocketConnectionObserver, observedConnectionChangeTo connected: Bool)
}

/// Надсмотрщик (😏) за соединением
class SocketConnectionObserver {
    
    fileprivate let socket: Socket
    fileprivate var handlerId: UUID?
    
    init(with socket: Socket) {
        self.socket = socket
    }
    
    deinit {
        self.delegate = nil
    }
    
    /// Есть ли соединение
    var isSocketConnected: Bool {
        return socket.isConnected
    }
    
    /// Делегат
    weak var delegate: SocketConnectionObserverDelegate? {
        
        didSet {
            
            if delegate != nil {
                
                handlerId = socket.onAsync(event: SocketEvent.statusChange.rawValue) { [weak self] _,_  in
                    DispatchQueue.main.async(execute: { [weak self] in
                        guard let strongSelf = self else { return }
                        strongSelf.delegate?.socketObserver(observer: strongSelf, observedConnectionChangeTo: strongSelf.socket.isConnected)
                    })
                }
            }
            else {
                
                if let handlerId = handlerId {
                    socket.off(handlerId: handlerId)
                }
            }
        }
    }
}

//
//  SocketIOImplementation.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 04.05.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import SocketIO

/// Блок для получения параметров соединения (query параметров при соединении по long polling)
typealias SocketIOConnectionParametersObtainer = () -> [String: Any]?

/// Конфигурация сокета SocketIO
struct SocketIOConfiguration {
    
    /// Url сервера
    let socketURL: URL
    
    /// Надо ли включить логирование
    var enableLogging = false
    
    /// Путь на сервере (если есть)
    var serverPath: String?
    
    /// Неймпспейс (если есть)
    var namespace: String?
    
    /// Блок для получения параметров соединения (query параметров при соединении по long polling)
    var connectionParametersObtainer: SocketIOConnectionParametersObtainer?
    
    init(with socketURL: URL) {
        self.socketURL = socketURL
    }
}

class SocketIOImplementation: Socket {
    
    fileprivate typealias EmitBlock = () -> ()?
    
    let socketManager: SocketManager
    let socket: SocketIOClient
    let notificationCenter: NotificationCenter
    let enableLogging: Bool
    
    /// Заголовок HTTP с именем хоста
    fileprivate let httpHostHeaderKey = "Host"
    
    /// Таймаут, после которого реконнектится сокет при потере соединения
    fileprivate let socketReconnectTimeout: TimeInterval = 3
    
    /// Интервал таймаутов, внутри которого рандомно выбирается таймаут реконнекта при получении ошибки сервера
    fileprivate let serverErrorReconnectTimeoutRange: Range<TimeInterval> = 1..<3
    
    /// Таймаут, добавляемый к итоговому для каждой последующей попытки реконнекта при получении ошибки сервера
    /// (итоговый = рандом + шаг * номер попытки)
    fileprivate let socketErrorReconnectTimeoutStep: TimeInterval = 2
    
    /// Максимальный номер попытки реконнекта при получении ошибки сервера
    /// (попытки реконнекта продолжатся и после, но итоговый таумаут перестанет расти)
    fileprivate let serverErrorReconnectMaxAttempt = 9
    
    /// Множитель таймаута реконнекта при получении ошибки сервера
    /// (используется для получения не целых значений секунд)
    fileprivate let serverErrorReconnectTimeoutMultiplier: TimeInterval = 100
    
    
    /// Текущее количество попыток реконнекта при получении ошибки сервера
    fileprivate lazy var serverErrorReconnectAttempt = 0
    
    /// Обработчик попытки реконнекта при получении ошибки сервера
    fileprivate lazy var serverErrorReconnectPerformer = DelayedWorkPerformer { [weak self] in
        self?.setupConnectionAndConnect()
    }
    
    
    /// События, ожидающие отправки после коннекта сокета
    fileprivate var suspendedEmits = [EmitBlock]()
    
    /// Объект, у которого можно получить параметры соединения
    fileprivate var connectionParametersObtainer: SocketIOConnectionParametersObtainer?
    
    /// Был ли выполнен дисконнект путем вызова метода disconnect()
    fileprivate var clientDisconnectedManually = false
    
    /// Последовательная очередь блоков, которая используется для вызова обработчиков SocketIOClient
    /// Любые манипуляции с suspendedEmits идут также через эту очередь, чтобы синхронизировать доступ к переменной во время вызовов tryEmitting и обработки сообщения connect
    fileprivate var serialQueue: DispatchQueue = DispatchQueue(label: "socket.io.client.handlers.queue")
    
    init(with configuration: SocketIOConfiguration, and center: NotificationCenter) {
        
        var options: SocketIOClientConfiguration = []
        
        options.insert(.log(configuration.enableLogging))
        options.insert(.handleQueue(serialQueue))
        options.insert(.reconnectWait(Int(socketReconnectTimeout)))

        //чтобы при каждом коннекте заново создавались параметры коннекта, в частности присваивались новые параметры соединения
        options.insert(.forceNew(true))
    
        if let path = configuration.serverPath, !path.isEmpty {
            options.insert(.path(path))
        }
        if let connectionParameters = configuration.connectionParametersObtainer?() {
            options.insert(.connectParams(connectionParameters))
        }
        if let host = configuration.socketURL.host {
            options.insert(.extraHeaders([httpHostHeaderKey : host]))
        }
        
        socketManager = SocketManager(socketURL: configuration.socketURL, config: options)
        
        if let namespace = configuration.namespace {
            socket = socketManager.socket(forNamespace: namespace)
        }
        else {
            socket = socketManager.defaultSocket
        }
        
        notificationCenter = center
        enableLogging = configuration.enableLogging
        connectionParametersObtainer = configuration.connectionParametersObtainer
        
        addServerConnectHandler()
        addServerErrorHandler()
        
        notificationCenter.addObserver(self,
                                       selector: #selector(applicationDidBecomeActive),
                                       name: .UIApplicationDidBecomeActive, object: nil)
        notificationCenter.addObserver(self,
                                       selector: #selector(applicationDidEnterBackground),
                                       name: .UIApplicationDidEnterBackground,
                                       object: nil)
    }
    
    deinit {
        serialQueue.suspend()
        notificationCenter.removeObserver(self)
        disconnect()
    }
    
    //MARK: - Приватные методы
    
    fileprivate func generateRandomTimeout() -> TimeInterval {
        
        let multiplier = serverErrorReconnectTimeoutMultiplier
        let step = socketErrorReconnectTimeoutStep
        let attempt = serverErrorReconnectAttempt
        let lowerBound = serverErrorReconnectTimeoutRange.lowerBound * multiplier
        let upperBound = serverErrorReconnectTimeoutRange.upperBound * multiplier
        
        var timeout = lowerBound + TimeInterval(arc4random_uniform(UInt32(upperBound - lowerBound)))
        timeout /= multiplier
        timeout += step * TimeInterval(attempt)
        
        return timeout
    }
    
    fileprivate func setupConnectionAndConnect() {
        
        if let connectionParameters = connectionParametersObtainer?() {
            socketManager.config.insert(.connectParams(connectionParameters))
        }
        
        serverErrorReconnectPerformer.cancelDelayedPerform()
        
        socketManager.connect()
        socket.connect()
    }
    
    fileprivate func tryEmitting(emit: @escaping EmitBlock) {
        
        if isConnected {
            emit()
        }
        else {
            serialQueue.async { [weak self] in
                self?.suspendedEmits.append(emit)
            }
        }
    }
    
    fileprivate func addServerConnectHandler() {
        
        socket.on(SocketEvent.connect.rawValue, callback: { [weak self] _, _ in
            
            guard let strongSelf = self else { return }
            
            strongSelf.serverErrorReconnectAttempt = 0
            
            for emit in strongSelf.suspendedEmits {
                emit()
            }
            
            strongSelf.suspendedEmits.removeAll()
        })
    }
    
    fileprivate func addServerErrorHandler() {
        
        socket.on(SocketEvent.error.rawValue, callback: { [weak self] _, _ in
            
            guard let strongSelf = self else { return }
            
            let timeout = strongSelf.generateRandomTimeout()
            let maxAttemptCount = strongSelf.serverErrorReconnectMaxAttempt
            let reconnectPerformer = strongSelf.serverErrorReconnectPerformer
            let queue = strongSelf.serialQueue
            
            if strongSelf.serverErrorReconnectAttempt < maxAttemptCount {
                strongSelf.serverErrorReconnectAttempt += 1
            }
            
            strongSelf.socketManager.disconnect()
            reconnectPerformer.perform(on: queue, afterDelay: timeout)
        })
    }
    
    fileprivate func on(event: String, once: Bool , async: Bool, handler: @escaping SocketEventHandler) -> UUID {
        
        let callback: NormalCallback = { (arguments, confirmationEmitter) in
            
            let confirmationBlock: SocketEventConfirmationEmitter = { confirmationArguments in
                confirmationEmitter.with(confirmationArguments)
            }
            
            if async {
                handler(arguments, confirmationBlock)
            }
            else {
                DispatchQueue.main.async {
                    handler(arguments, confirmationBlock)
                }
            }
        }
        
        if once {
            return socket.once(event, callback: callback)
        }
        else {
            return socket.on(event, callback: callback)
        }
    }
    
    fileprivate func send(event: String,
                          with arguments: [SocketTransferable],
                          timingOutAfter seconds: Double,
                          asynchronously: Bool,
                          withConfirmation handler: @escaping SocketEventConfirmationHandler) {
        
        // если таймаут нулевой или меньше, то гарантируем его вызов через секунду
        var timeout = seconds
        if timeout < 1 {
            timeout = 1
        }
        
        socket.emitWithAck(event, with: arguments).timingOut(after: timeout, callback: { confirmationArguments in
            
            let timedOut = !confirmationArguments.isEmpty && (confirmationArguments.first as? String) == SocketAckStatus.noAck.rawValue
            
            if asynchronously {
                handler(timedOut, confirmationArguments)
            }
            else {
                DispatchQueue.main.async {
                    handler(timedOut, confirmationArguments)
                }
            }
        })
    }
    
    //MARK: - Нотификации
    
    @objc func applicationDidBecomeActive() {
        
        guard socketManager.status != .connected && socketManager.status != .connecting else {
            return
        }
        
        if !clientDisconnectedManually {
            setupConnectionAndConnect()
        }
    }
    
    @objc func applicationDidEnterBackground() {
        socketManager.disconnect()
    }
    
    //MARK: - Socket
    
    var isConnected: Bool {
        return socket.status == .connected
    }
    
    func connect() {
        
        guard !isConnected else {
            return
        }
        
        clientDisconnectedManually = false
        setupConnectionAndConnect()
    }
    
    func disconnect() {
        clientDisconnectedManually = true
        socketManager.disconnect()
    }
    
    func send(event: String) {
        send(event: event, with: [])
    }
    
    func send(event: String, with arguments: [SocketTransferable]) {
        
        let emit = { [weak self] in
            self?.socket.emit(event, with: arguments)
        }
        
        tryEmitting(emit: emit)
    }
    
    func send(event: String, timingOutAfter seconds: Double, withConfirmation handler: @escaping SocketEventConfirmationHandler) {
        send(event: event, with: [], timingOutAfter: seconds, withConfirmation: handler)
    }
    
    func send(event: String, timingOutAsyncAfter seconds: Double, withConfirmation handler: @escaping SocketEventConfirmationHandler) {
        send(event: event, with: [], timingOutAsyncAfter: seconds, withConfirmation: handler)
    }
    
    func send(event: String, with arguments: [SocketTransferable], timingOutAfter seconds: Double, withConfirmation handler: @escaping SocketEventConfirmationHandler) {
        send(event: event, with: arguments, timingOutAfter: seconds, asynchronously: false, withConfirmation: handler)
    }
    
    func send(event: String, with arguments: [SocketTransferable], timingOutAsyncAfter seconds: Double, withConfirmation handler: @escaping SocketEventConfirmationHandler) {
        send(event: event, with: arguments, timingOutAfter: seconds, asynchronously: true, withConfirmation: handler)
    }
    
    @discardableResult func on(event: String, handler: @escaping SocketEventHandler) -> UUID {
        return on(event: event, once: false, async: false, handler: handler)
    }
    
    @discardableResult func onAsync(event: String, handler: @escaping SocketEventHandler) -> UUID {
        return on(event: event, once: false, async: true, handler: handler)
    }
    
    @discardableResult func once(event: String, handler: @escaping SocketEventHandler) -> UUID {
        return on(event: event, once: true, async: false, handler: handler)
    }
    
    @discardableResult func onceAsync(event: String, handler: @escaping SocketEventHandler) -> UUID {
        return on(event: event, once: true, async: true, handler: handler)
    }
    
    func off(event: String) {
        socket.off(event)
    }
    
    func off(handlerId: UUID){
        socket.off(id: handlerId)
    }
}

//
//  UUSID.swift
//  Portable
//
//  Created by Ivan Erasov on 16.06.17.
//  Copyright © 2017. All rights reserved.
//

import Foundation

/// Уникальный сортируемый идентификатор
struct UUSID: Equatable, Hashable {
    
    /// Последний сгенерированный временной компонент идентификатора
    fileprivate static var lastGeneratedTimeComponent: String?
    
    /// Текущий инкрементируемый компонент идентификатора
    fileprivate static var incrementingComponent: UInt = 0
    
    /// Форматтер чисел, который используется для получения числового представления идентификатора
    fileprivate static var numberFormatter = NumberFormatter()
    
    /// Строковое представление идентификатора
    fileprivate(set) var uusidString: String
    
    /// Числовое представление идентификатора
    var uusidNumber: NSNumber {
        // для составления строки используем тот же форматтер, поэтому не волнуемся за результат
        return UUSID.numberFormatter.number(from: uusidString)!
    }
    
    init() {
        
        // берем текущее время в строковом представлении (временной компонент)
        let timeComponent = String(UInt64(Date().timeIntervalSince1970))
        uusidString = timeComponent
        
        // если время совпадает с последним сгенерированным
        if timeComponent == UUSID.lastGeneratedTimeComponent {
            
            // добавляем инкрементируемый компонент
            UUSID.incrementingComponent += 1
            uusidString += UUSID.numberFormatter.decimalSeparator + String(UUSID.incrementingComponent)
        }
        else {
            
            // иначе сбрасываем инкрементируемый компонент
            UUSID.incrementingComponent = 0
        }
        
        UUSID.lastGeneratedTimeComponent = timeComponent
    }
    
    //MARK: - Equatable
    
    static func ==(lhs: UUSID, rhs: UUSID) -> Bool {
        return lhs.uusidString == rhs.uusidString
    }
    
    //MARK: - Hashable
    
    var hashValue: Int {
        return uusidString.hashValue
    }
}

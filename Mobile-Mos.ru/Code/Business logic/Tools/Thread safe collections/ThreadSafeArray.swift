//
//  ThreadSafeArray.swift
//  Portable
//
//  Created by Ivan Erasov on 20.06.17.
//  Copyright © 2017. All rights reserved.
//

import Foundation

/// Потокобезопасный массив (доступ к элементам возможен с разных потоков одновременно)
class ThreadSafeArray<Element> {
    
    fileprivate var array = [Element]()
    fileprivate let queue = DispatchQueue(label: "com.any.app.thread.safe.array", attributes: .concurrent)
    
    /// Для синхронизации используется очередь
    /// Чтения выполняются параллельно, вызывающий поток ждет только окончания операции чтения
    /// Записи выполняются последовательно и блокируют другие блоки с помощью барьера, вызывающий поток не ждет окончания операции записи
    /// self захватывается блоками т.к. все равно будет освобожден, а операции короткие
    
    subscript(index: Int) -> Element {
        
        set {
            queue.async(flags:.barrier) {
                self.array[index] = newValue
            }
        }
        
        get {
            
            var element: Element!
            
            queue.sync {
                element = array[index]
            }
            
            return element
        }
    }
    
    var count: Int {
        
        var count: Int!
        
        queue.sync {
            count = array.count
        }
        
        return count
    }
    
    var isEmpty: Bool {
        
        var isEmpty: Bool!
        
        queue.sync {
            isEmpty = array.isEmpty
        }
        
        return isEmpty
    }
    
    var first: Element? {
        
        var element: Element?
        
        queue.sync {
            element = array.first
        }
        
        return element
    }
    
    var last: Element? {
        
        var element: Element?
        
        queue.sync {
            element = array.last
        }
        
        return element
    }
    
    func currentElements() -> [Element] {
        
        var elements: [Element]!

        queue.sync {
            elements = Array(array)
        }
        
        return elements
    }
    
    func append(element: Element) {
        queue.async(flags: .barrier) {
            self.array.append(element)
        }
    }
    
    func append<S>(contentsOf newElements: S) where S : Sequence, S.Iterator.Element == Element {
        queue.async(flags: .barrier) {
            self.array.append(contentsOf: newElements)
        }
    }
    
    func remove(at index: Int) {
        queue.async(flags: .barrier) {
            self.array.remove(at: index)
        }
    }
    
    func removeFirst() {
        queue.async(flags: .barrier) {
            guard !self.array.isEmpty else { return }
            self.array.remove(at: 0)
        }
    }
    
    func removeLast() {
        queue.async(flags: .barrier) {
            guard !self.array.isEmpty else { return }
            self.array.remove(at: self.array.count - 1)
        }
    }
    
    func remove(bounds: ClosedRange<Int>) {
        queue.async(flags: .barrier) {
            self.array.removeSubrange(bounds)
        }
    }
    
    func removeAll() {
        queue.async(flags: .barrier) {
            self.array.removeAll()
        }
    }
}

//
//  ThreadSafeSerialEventQueue.swift
//  Portable
//
//  Created by Ivan Erasov on 23.08.17.
//  Copyright © 2017. All rights reserved.
//

import Foundation

/// Информация о событии - блоки кода, создающие и обрабатывающие события
struct SerialEventBlock<EventResult> {
    
    /// Блок кода, который отправит/создаст событие
    typealias EventEmitter = () -> ()
    
    /// Блок кода, обрабатывающий событие
    typealias EventHandler = (EventResult) -> ()
    
    var emitter: EventEmitter
    var handler: EventHandler
}

/// Потокобезопасная последовательная очередь событий
/// Позволяет организовать последовательную обработку событий одного типа
class ThreadSafeSerialEventQueue<EventResult>: ThreadSafeQueue<SerialEventBlock<EventResult>> {
    
    /// Поставлена ли очередь на паузу
    fileprivate(set) var isPaused: Bool = false
    
    /// Поставить очередь на паузу
    func pause() {
        guard !isPaused else { return }
        isPaused = true
    }
    
    /// Возобновить обработку событий в очереди
    func resume() {
        guard isPaused else { return }
        isPaused = false
        first?.emitter()
    }
    
    override func enqueue(element: SerialEventBlock<EventResult>) {
        
        let shouldExecuteEvent = isEmpty && !isPaused
        
        super.enqueue(element: element)
        
        if shouldExecuteEvent {
            element.emitter()
        }
    }
    
    override func dequeue() -> SerialEventBlock<EventResult>? {
        
        let dequeuedElement = super.dequeue()
        
        if !isPaused {
            first?.emitter()
        }
        
        return dequeuedElement
    }
}

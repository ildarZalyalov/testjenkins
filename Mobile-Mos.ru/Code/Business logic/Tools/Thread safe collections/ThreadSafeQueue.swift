//
//  ThreadSafeQueue.swift
//  Portable
//
//  Created by Ivan Erasov on 23.08.17.
//  Copyright © 2017. All rights reserved.
//

import Foundation

/// Потокобезопасная очередь (доступ к элементам возможен с разных потоков одновременно)
class ThreadSafeQueue<Element> {
    
    fileprivate var array = [Element]()
    fileprivate let queue = DispatchQueue(label: "com.any.app.thread.safe.queue", attributes: .concurrent)
    
    /// Для синхронизации используется очередь
    /// Чтения выполняются параллельно, вызывающий поток ждет только окончания операции чтения
    /// Записи выполняются последовательно и блокируют другие блоки с помощью барьера, вызывающий поток не ждет окончания операции записи
    /// self захватывается блоками т.к. все равно будет освобожден, а операции короткие
    
    subscript(index: Int) -> Element {
        
        set {
            queue.async(flags:.barrier) {
                self.array[index] = newValue
            }
        }
        
        get {
            
            var element: Element!
            
            queue.sync {
                element = array[index]
            }
            
            return element
        }
    }
    
    var count: Int {
        
        var count: Int!
        
        queue.sync {
            count = array.count
        }
        
        return count
    }
    
    var isEmpty: Bool {
        
        var isEmpty: Bool!
        
        queue.sync {
            isEmpty = array.isEmpty
        }
        
        return isEmpty
    }
    
    var first: Element? {
        
        var element: Element?
        
        queue.sync {
            element = array.first
        }
        
        return element
    }
    
    var last: Element? {
        
        var element: Element?
        
        queue.sync {
            element = array.last
        }
        
        return element
    }
    
    func currentElements() -> [Element] {
        
        var elements: [Element]!
        
        queue.sync {
            elements = Array(array)
        }
        
        return elements
    }
    
    func enqueue(element: Element) {
        queue.async(flags: .barrier) {
            self.array.append(element)
        }
    }
    
    func dequeue() -> Element? {
        
        var element: Element?
        
        queue.sync {
            element = array.isEmpty ? nil : array.removeFirst()
        }
        
        return element
    }
    
    func removeAll() {
        queue.async(flags: .barrier) {
            self.array.removeAll()
        }
    }
}

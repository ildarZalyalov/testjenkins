//
//  SunsetDetector.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 07.08.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Определитель времени заката/восхода
protocol SunsetDetector {
    
    /// День ли сейчас
    var isDaytime: Bool { get }
    
    /// Ночь ли сейчас
    var isNighttime: Bool { get }
    
    /// Время восхода для даты
    ///
    /// - Parameter date: дата
    /// - Returns: время восхода
    func sunrise(for date: Date) -> Date?
    
    /// Время заката для даты
    ///
    /// - Parameter date: дата
    /// - Returns: время заката
    func sunset(for date: Date) -> Date?
}

//
//  SunsetDetectorImplementation.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 07.08.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import CoreLocation
import Solar

class SunsetDetectorImplementation: SunsetDetector {
    
    /// Закэшированный календарь
    fileprivate static var calendar = Calendar.autoupdatingCurrent
    
    /// Закэшированная дата сегодняшнего дня
    fileprivate static var lastCalculatedTodayDate = Date()
    
    
    /// Закэшированный экземпляр Solar для подсчет дня/ночи для сегодня
    fileprivate static var cachedTodaySolar: Solar!
    
    fileprivate typealias Detector = SunsetDetectorImplementation
    
    //MARK: - Приватные методы
    
    /// Обновляем закэшированный экземпляр Solar, если это нужно
    fileprivate func updateCachedTodaySolarIfNeeded() {
        
        if !Detector.calendar.isDateInToday(Detector.lastCalculatedTodayDate) {
            Detector.lastCalculatedTodayDate = Date()
            Detector.cachedTodaySolar = nil
        }
        
        if Detector.cachedTodaySolar == nil {
            let coordinate = CLLocationCoordinate2D(latitude: moscowLocation.latitude, longitude: moscowLocation.longitude)
            Detector.cachedTodaySolar = Solar(for: Detector.lastCalculatedTodayDate, coordinate: coordinate)
        }
    }
    
    //MARK: - SunsetDetector
    
    var isDaytime: Bool {
        
        updateCachedTodaySolarIfNeeded()
        
        //считаем так потому, что https://github.com/ceeK/Solar/issues/24
        
        guard
            let sunrise = Detector.cachedTodaySolar.sunrise,
            let sunset = Detector.cachedTodaySolar.sunset
            else {
                return false
        }
        
        let beginningOfDay = sunrise.secondsFromMidnight()
        let endOfDay = sunset.secondsFromMidnight()
        let currentTime = Detector.lastCalculatedTodayDate.secondsFromMidnight()
        
        
        let isSunriseOrLater = currentTime >= beginningOfDay
        let isBeforeSunset = currentTime < endOfDay
        
        return isSunriseOrLater && isBeforeSunset
    }
    
    var isNighttime: Bool {
        return !isDaytime
    }
    
    func sunrise(for date: Date) -> Date? {
        let coordinate = CLLocationCoordinate2D(latitude: moscowLocation.latitude, longitude: moscowLocation.longitude)
        return Solar(for: date, coordinate: coordinate)?.sunrise
    }
    
    func sunset(for date: Date) -> Date? {
        let coordinate = CLLocationCoordinate2D(latitude: moscowLocation.latitude, longitude: moscowLocation.longitude)
        return Solar(for: date, coordinate: coordinate)?.sunset
    }
}

fileprivate extension Date {
    
    /// Получаем секунды от полуночи
    func secondsFromMidnight() -> Int {
        
        let hours = Calendar.current.component(.hour, from: self)
        let minutes = Calendar.current.component(.minute, from: self)
        let seconds = Calendar.current.component(.second, from: self)
        
        return hours * 60 * 60 + minutes * 60 + seconds
    }
}

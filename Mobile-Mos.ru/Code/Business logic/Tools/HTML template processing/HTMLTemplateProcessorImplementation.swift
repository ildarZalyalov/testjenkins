//
//  HTMLTemplateProcessorImplementation.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 04.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import Stencil

class HTMLTemplateProcessorImplementation: HTMLTemplateProcessor {
    
    fileprivate func call(completion: @escaping HTMLTemplateProcessingHandler, with string: String?) {
        
        if Thread.isMainThread {
            completion(string)
        } else {
            DispatchQueue.main.async(execute: {
                completion(string)
            })
        }
    }
    
    //MARK: - HTMLTemplateProcessor
    
    func processTemplate(at url: URL, with valuesJsonString: String, completion: @escaping HTMLTemplateProcessingHandler) {
        
        URLSession.shared.dataTask(with: URLRequest(url: url)) { [weak self] (data, _, _) in
            
            guard let strongSelf = self else { return }
            
            var templateString: String? = nil
            
            defer {
                strongSelf.call(completion: completion, with: templateString)
            }
            
            guard let htmlData = data else { return }
            guard let htmlString = String(data: htmlData, encoding: .utf8) else { return }
            
            do {
                guard let valuesJsonData = valuesJsonString.data(using: .utf8) else { return }
                let valuesJsonObject = try JSONSerialization.jsonObject(with: valuesJsonData, options: [])
                guard let values = valuesJsonObject as? [String : Any] else { return }
                
                let template = Template(templateString: htmlString)
                templateString = try template.render(values)
            }
            catch(_) {
                return
            }
            
            }.resume()
    }
}

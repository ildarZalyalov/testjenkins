//
//  HTMLTemplateProcessor.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 04.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Обработчик получения шаблона HTML - принимает на вход строку с HTML кодом или nil, если произошла ошибка
typealias HTMLTemplateProcessingHandler = (String?) -> ()

/// Преобразователь шаблона HTML в HTML код
protocol HTMLTemplateProcessor {
    
    /// Преобразовать шаблон по веб-ссылке
    ///
    /// - Parameters:
    ///   - url: веб-ссылка с шаблоном
    ///   - valuesJsonString: значения для подстановки в шаблон (строка json)
    ///   - completion: обработчик получения шаблона
    func processTemplate(at url: URL, with valuesJsonString: String, completion: @escaping HTMLTemplateProcessingHandler)
}

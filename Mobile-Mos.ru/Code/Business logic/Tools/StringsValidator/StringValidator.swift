//
//  StringValidator.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 30.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

class StringValidator {
    
    /// Провалидировать email строку и вернуть true/false
    ///
    /// - Parameter emailString: email строка
    /// - Returns: true/false
    func validateEmail(_ emailString: String) -> Bool {
        
        let emailFormat = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,10}"
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
        
        return emailPredicate.evaluate(with: emailString)
    }
}

//
//  UIImageLoadingAndCacheManager.swift
//  Portable
//
//  Created by Ivan Erasov on 28.02.17.
//  Copyright © 2017. All rights reserved.
//

import Foundation
import SDWebImage

/// Менеджер для загрузки и кеширования изображений
class UIImageLoadingAndCacheManager {
    
    //MARK: - Встроенные типы
    
    /// Тип кеша
    ///
    /// - none: не кешировано
    /// - memory: кешировано в памяти
    /// - disk: кешировано на диске
    enum CacheType {
        
        case none
        case memory
        case disk
        
        fileprivate init(with cacheType: SDImageCacheType) {
            switch cacheType {
            case .memory:
                self = .memory
            case .disk:
                self = .disk
            default:
                self = .none
            }
        }
    }
    
    /// Доступные опции
    ///
    /// - none: нет опций
    /// - cacheMemoryOnly: сохранять только в кеше в памяти
    /// - queryDataWhenInMemory: даже при наличии кеша в памяти искать изображение на диске
    enum Options {
        
        case none
        case cacheMemoryOnly
        case queryDataWhenInMemory
        
        fileprivate var sdWebImageOption: SDWebImageOptions {
            switch self {
            case .cacheMemoryOnly:
                return SDWebImageOptions.cacheMemoryOnly
            case .queryDataWhenInMemory:
                return SDWebImageOptions.queryDataWhenInMemory
            default:
                return SDWebImageOptions()
            }
        }
    }
    
    /// Коды ошибок загрузки изображений
    ///
    /// - imageLoadFailedUnknownError: код неизвестной ошибки загрузки изображения
    enum UIImageLoadErrorCode: Int {
        case imageLoadFailedUnknownError
    }
    
    /// Результат загрузки изображения
    ///
    /// - success: успех, связанные значения - изображение, его байтовое представление (если загрузка шла с диска) и тип кеша, из которого оно было получено
    /// - failure: провал, связанное значение - ошибка загрузки
    enum UIImageLoadResult {
        case success(image: UIImage, imageData: Data?, cacheType: CacheType)
        case failure(error: Error)
    }
    
    /// Результат предзагрузки в кеш изображений - кол-во загруженных в кеш ссылок и кол-во пропущенных ссылок
    typealias UIImagePrefetchResult = (finishedUrlsCount: UInt, skippedUrlsCount: UInt)
    
    //MARK: - Поля
    
    /// Домен ошибок загрузки изображения
    static let imageLoadingLibraryErrorDomain = "ru.mos.image-load.error"
    
    //MARK: - Методы
    
    /// Загружает и помещает в кеш изображение по ссылке. В качестве ключа кеширования используется ссылка.
    ///
    /// - Parameters:
    ///   - url: ссылка на изображение, если передать nil, то загрузка вызовет блок завершения с ошибкой
    ///   - options: опции загрузки или кеширования изображения
    ///   - completion: блок завершения загрузки изображения
    func loadImage(from url: URL?, with options: [Options]? = nil, completion: @escaping (UIImageLoadResult) -> Void) {
        
        var sdWebImageOptions = SDWebImageOptions()
        options?.forEach({ sdWebImageOptions = sdWebImageOptions.union($0.sdWebImageOption) })
        
        SDWebImageManager.shared().loadImage(with: url, options: sdWebImageOptions, progress: nil) { image, data, error, cacheType, _, _ in
            
            var result: UIImageLoadResult
            defer {
                completion(result)
            }
            
            guard error == nil else {
                result = .failure(error: error!)
                return
            }
            
            guard image != nil else {
                
                let domain = UIImageLoadingAndCacheManager.imageLoadingLibraryErrorDomain
                let errorCode = UIImageLoadErrorCode.imageLoadFailedUnknownError.rawValue
                let loadError = NSError(domain: domain, code: errorCode, userInfo: nil)
                
                result = .failure(error: loadError)
                
                return
            }
            
            result = .success(image: image!, imageData: data, cacheType: CacheType(with: cacheType))
        }
    }
    
    /// Кешировать изображение по ссылке, которая используется в качестве ключа кеширования.
    ///
    /// - Parameters:
    ///   - image: изображение
    ///   - url: ссылка
    func cache(_ image: UIImage, from url: URL) {
        SDWebImageManager.shared().saveImage(toCache: image, for: url)
    }
    
    /// Предзагрузить в кеш изображения по ссылкам
    ///
    /// - Parameters:
    ///   - urls: ссылки на изображения
    ///   - completion: блок завершения предзагрузки
    func prefetchImages(from urls: [URL]?, completion: ((UIImagePrefetchResult) -> Void)?) {
        SDWebImagePrefetcher.shared().prefetchURLs(urls, progress: nil) { (finishedUrlsCount, skippedUrlsCount) in
            completion?((finishedUrlsCount, skippedUrlsCount))
        }
    }
}

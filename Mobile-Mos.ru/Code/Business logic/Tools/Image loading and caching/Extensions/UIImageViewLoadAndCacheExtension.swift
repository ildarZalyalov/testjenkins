//
//  UIImageViewLoadAndCacheExtension.swift
//  Portable
//
//  Created by Ivan Erasov on 28.02.17.
//  Copyright © 2017. All rights reserved.
//

import Foundation

extension UIImageView {
    
    fileprivate static let sharedImageManager = UIImageLoadingAndCacheManager()
    typealias Options = UIImageLoadingAndCacheManager.Options
    typealias UIImageLoadResult = UIImageLoadingAndCacheManager.UIImageLoadResult
    
    /// Загрузить и отобразить изображение по ссылке
    ///
    /// - Parameters:
    ///   - url: ссылка на изображение
    ///   - placeholder: изображение, которое будет отображаться пока идет загрузка
    ///   - options: опции загрузки или кеширования изображения
    ///   - completion: блок завершения загрузки
    func loadAndDisplayImage(from url: URL?, placeholder: UIImage? = nil, with options: [Options]? = nil, completion: ((UIImageLoadResult) -> Void)? = nil) {
        
        image = placeholder
        
        UIImageView.sharedImageManager.loadImage(from: url, with: options) { [weak self] result in
            
            guard let strongSelf = self else {
                return
            }
            
            defer {
                if let completion = completion {
                    completion(result)
                }
            }
            
            switch result {
            case .success(let image, _, _):
                strongSelf.image = image
            default:
                return
            }
        }
    }
}

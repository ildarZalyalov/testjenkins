//
//  FLAnimatedImageViewLoadAndCacheExtension.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 07.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation
import MobileCoreServices
import FLAnimatedImage

extension FLAnimatedImageView {
    
    /// Отобразить GIF из байтовых данных
    ///
    /// - Parameters:
    ///   - imageData: байтовые данные
    ///   - completion: обработчик завершения подготовки и отображения GIF
    func displayGif(with imageData: Data, completion: (() -> Void?)?) {
        DispatchQueue.global(qos: .userInitiated).async { [weak self] in
            let animatedImage = FLAnimatedImage(animatedGIFData: imageData)
            DispatchQueue.main.async { [weak self] in
                self?.animatedImage = animatedImage
                self?.image = nil
                completion?()
            }
        }
    }
    
    /// Загрузить и отобразить изображение по ссылке с поддержкой отображения GIF
    ///
    /// - Parameters:
    ///   - url: ссылка на изображение
    ///   - placeholder: изображение, которое будет отображаться пока идет загрузка
    ///   - options: опции загрузки или кеширования изображения
    ///   - completion: блок завершения загрузки
    func loadAndDisplayImageWithGifSupport(from url: URL?, placeholder: UIImage? = nil, with options: [Options]? = nil, completion: ((UIImageLoadResult) -> Void)? = nil) {
        
        loadAndDisplayImage(from: url, placeholder: placeholder, with: options) { [weak self] result in
            
            let notifyAboutCompletion = {
                completion?(result)
            }
            
            switch result {
                
            case .success(let image, let data, _):
                
                guard image.cgImage?.utType == kUTTypeGIF else {
                    notifyAboutCompletion()
                    return
                }
                
                if let imageData = data {
                    self?.displayGif(with: imageData, completion: notifyAboutCompletion)
                }
                else {
                    var extendedOptions = options ?? []
                    extendedOptions.append(.queryDataWhenInMemory)
                    self?.loadAndDisplayImageWithGifSupport(from: url, placeholder: placeholder, with: extendedOptions, completion: completion)
                }
                
            case .failure:
                notifyAboutCompletion()
            }
        }
    }
}

//
//  BankCardValidator.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 25.05.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Валидатор данных для банковских карт
protocol BankCardValidator {
    
    /// Поддерживаемые длины номеров карт
    var acceptableCardNumberLengths: CountableClosedRange<Int> { get }
    
    /// Провалидировать номер карты без форматирования
    func validate(cardNumber: String) -> Bool
    
    /// Провалидировать дату окончания срока действия карты без форматирования (формат - yyMM)
    func validate(expiry: String) -> Bool
    
    /// Провалидировать код безопасности карты без форматирования
    func validate(cvvc: String) -> Bool
}

//
//  BankCardValidatorImplementation.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 25.05.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

class BankCardValidatorImplementation: BankCardValidator {
    
    fileprivate static var expiryParser: DateFormatter = {
        
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.timeZone = TimeZone(abbreviation: "UTC")
        formatter.dateFormat = "yyMM"
        
        return formatter
    }()
    
    fileprivate static var calendar: Calendar = {
        
        var calendar = Calendar.autoupdatingCurrent
        calendar.locale = Locale(identifier: "ru_RU")
        
        if let timeZone = TimeZone(abbreviation: "UTC") {
            calendar.timeZone = timeZone
        }
        
        return calendar
    }()
    
    let acceptableCardNumberLengths = 13...19
    
    let cvvcLength = 3
    
    //MARK: - Приватные методы
    
    func obtainExpiryDate(from string: String) -> Date? {
        
        typealias Validator = BankCardValidatorImplementation
        
        guard let date = Validator.expiryParser.date(from: string) else { return nil }
        return Validator.calendar.date(byAdding: .month, value: 1, to: date)
    }
    
    func validateIsNumeric(string: String) -> Bool {
        return string.trimmingCharacters(in: .decimalDigits).isEmpty
    }
    
    func validateLuhnTest(cardNumber: String) -> Bool {
        
        var odd = true
        var sum = 0
        
        for character in cardNumber {
            
            /// Получили не число, значит номер карты не валидный
            guard var digit = Int(String(character)) else { return false }
            
            if odd {
                digit = digit * 2
            }
            if digit > 9 {
                digit = digit - 9
            }
            
            sum += digit
            odd = !odd
        }
        
        return sum % 10 == 0
    }
    
    //MARK: - BankCardValidator
    
    func validate(cardNumber: String) -> Bool {
        guard validateIsNumeric(string: cardNumber) else { return false }
        guard acceptableCardNumberLengths.contains(cardNumber.count) else { return false }
        return validateLuhnTest(cardNumber: cardNumber)
    }
    
    func validate(expiry: String) -> Bool {
        guard validateIsNumeric(string: expiry) else { return false }
        guard let date = obtainExpiryDate(from: expiry) else { return false }
        return date > Date()
    }
    
    func validate(cvvc: String) -> Bool {
        guard validateIsNumeric(string: cvvc) else { return false }
        return cvvc.count == cvvcLength
    }
}

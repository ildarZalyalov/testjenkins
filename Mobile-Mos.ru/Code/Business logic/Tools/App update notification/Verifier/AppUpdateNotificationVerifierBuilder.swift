//
//  AppUpdateNotificationVerifierBuilder.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 08.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

extension AppUpdateNotificationVerifier {
    
    static func getAppUpdateNotificationVerifier() -> AppUpdateNotificationVerifier {
        
        let appUpdateVerifier = AppUpdateNotificationVerifier()
        let serviceBuilder = UIApplication.shared.serviceBuilder
        
        appUpdateVerifier.lookupService = serviceBuilder.getiTunesSearchService()
        appUpdateVerifier.bundle = Bundle.main
        
        return appUpdateVerifier
    }
}

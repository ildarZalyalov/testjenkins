//
//  AppUpdateNotificationVerifier.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 08.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Результат запроса на получение информации об обновлении приложения
///
/// - success: успех, связанное значение - информация об обновлении (если оно есть) и флаг "является ли обновление свежим", то есть информация о нем поступила в первый раз (версия обновления больше закешированной)
/// - failure: провал, связанное значение - ошибка
enum ApplicationUpdateLookupResult {
    case success(state: iTunesVersionLookupState?, isRecentUpdate: Bool)
    case failure(error: Error)
}

/// Обработчик результата получения информации об обновлении приложения
typealias ApplicationUpdateLookupResultHandler = (ApplicationUpdateLookupResult) -> Void

final class AppUpdateNotificationVerifier {
    
    /// Интервал времени обновления кеша информации о версии приложения
    let lookupTimeInterval: TimeInterval = -60 * 60 * 24
    
    /// Идет ли сейчас проверка на наличие обновлений приложения
    fileprivate(set) var isCheckingForApplicationUpdates = false
    
    var lookupService: iTunesSearchService!
    var bundle: Bundle!
    
    //MARK: - Хелперы
    
    /// Сравнить информацию о версиях
    ///
    /// - Parameters:
    ///   - state: информация о версии
    ///   - anotherState: другая информация о версии
    /// - Returns: true, если state содержит информацию о более старшей версии, чем anotherState, иначе false
    fileprivate func compare(state: iTunesVersionLookupState?, isMoreRecentThan anotherState: iTunesVersionLookupState?) -> Bool {
        
        guard let stateVersion = state?.version else { return false }
        guard let anotherStateVersion = anotherState?.version else { return true }
        
        return Bundle.compare(version: anotherStateVersion, isLessThan: stateVersion)
    }
    
    /// Вызвать финальный обработчик результата получения информации о версии приложения
    ///
    /// - Parameters:
    ///   - completion: обработчик
    ///   - state: полученная информация о версии приложения
    fileprivate func call(completion: @escaping ApplicationUpdateLookupResultHandler, for state: iTunesVersionLookupState?, isRecentUpdate: Bool) {
        
        // если версия приложения меньше версии в iTunesVersionLookupState, то отдаем iTunesVersionLookupState, иначе отдаем nil (обновлений нет)
        var resultState: iTunesVersionLookupState? = nil
        if let version = state?.version, let appVersion = bundle.releaseVersionNumber, Bundle.compare(version: appVersion, isLessThan: version) {
            resultState = state
        }
        
        isCheckingForApplicationUpdates = false
        
        DispatchQueue.main.async {
            completion(.success(state: resultState, isRecentUpdate: isRecentUpdate))
        }
    }
    
    /// Вызвать финальный обработчик результата получения информации о версии приложения с ошибкой
    ///
    /// - Parameters:
    ///   - completion: обработчик
    ///   - error: ошибка
    fileprivate func call(completion: @escaping ApplicationUpdateLookupResultHandler, for error: Error) {
    
        isCheckingForApplicationUpdates = false
        
        DispatchQueue.main.async {
            completion(.failure(error: error))
        }
    }
    
    //MARK: - Открытые методы
    
    /// Проверить есть ли обновления приложения. Если обновления есть обработчик получит информацию о них, если нет, он получит nil
    ///
    /// - Parameter completion: обработчик получения информации об обновлениях
    func checkForApplicationUpdates(completion: @escaping ApplicationUpdateLookupResultHandler) {
        
        guard !isCheckingForApplicationUpdates else { return }
        isCheckingForApplicationUpdates = true
        
        lookupService.obtainSavedLookupState { [weak self] result in
            
            guard let strongSelf = self else { return }
            
            var cachedState: iTunesVersionLookupState? = nil
            
            switch result {
            case .success(let state):
                cachedState = state
            case .failure(let error):
                strongSelf.call(completion: completion, for: error)
                return
            }
            
            let sinceLastUpdate = cachedState?.date?.timeIntervalSinceNow
            let neverChecked = sinceLastUpdate == nil
            let timeForUpdate = sinceLastUpdate != nil && sinceLastUpdate! < strongSelf.lookupTimeInterval
            
            guard neverChecked || timeForUpdate else {
                strongSelf.call(completion: completion, for: cachedState, isRecentUpdate: false)
                return
            }
            
            strongSelf.lookupService.obtainApplicationAppStoreVersion(completion: { [weak self] result in
                
                guard let strongSelf = self else { return }
                
                switch result {
                case .success(let state):
                    strongSelf.call(completion: completion, for: state, isRecentUpdate: strongSelf.compare(state: state, isMoreRecentThan: cachedState))
                    guard state != nil else { return }
                    strongSelf.lookupService.saveLookupState(state: state!)
                case .failure(let error):
                    strongSelf.call(completion: completion, for: error)
                }
            })
        }
    }
}

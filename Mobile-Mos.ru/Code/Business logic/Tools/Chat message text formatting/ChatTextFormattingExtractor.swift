//
//  ChatTextFormattingExtractor.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 31.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Палитра шрифтов для расчета форматирования текста сообщения
struct ChatTextFormattingFontPalette {
    
    /// Расстояние между строками в тексте сообщения
    let lineSpacing: CGFloat
    
    /// Шрифт текста сообщения
    let normalFont: UIFont
    
    /// Шрифт текста сообщения среднего начертания
    let mediumFont: UIFont?
    
    /// Шрифт текста сообщения жирного начертания
    let boldFont: UIFont?
}

/// Парсер форматирования текста сообщения
protocol ChatTextFormattingExtractor {
    
    /// Распарсить форматирование текста сообщения
    ///
    /// - Parameter text: исходный текст с тегами
    /// - Returns: результат расчета форматирования текста сообщения (если были найдены теги)
    func extractFormatting(from text: String) -> ChatTextFormatting?
    
    /// Получить атрибутную строку для форматирования текста сообщения
    ///
    /// - Parameters:
    ///   - formatting: данные о форматировании текста сообщения
    ///   - fontPalette: палитра шрифтов
    ///   - text: исходный текст с тегами
    /// - Returns: атрибутная строка
    func attributedString(from formatting: ChatTextFormatting, with fontPalette: ChatTextFormattingFontPalette, for text: String) -> NSAttributedString
}

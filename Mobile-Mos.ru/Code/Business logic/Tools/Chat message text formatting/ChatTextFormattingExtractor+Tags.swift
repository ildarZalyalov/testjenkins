//
//  ChatTextFormattingExtractor+Tags.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 05.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Поддерживаемые атрибуты тегов
///
/// - backgroundColor: цвет фона
/// - fontSize: размер шрифта
/// - fontWeight: начертание шрифта
/// - textColor: цвет шрифта
/// - textDecoration: дополнительные элементы в тексте
/// - textDecorationColor: цвет дополнительных элементов в тексте
enum ChatTextFormattingTagAttributes: String {
    case backgroundColor = "background-color"
    case fontSize = "font-size"
    case fontWeight = "font-weight"
    case textColor = "text-color"
    case textDecoration = "text-decoration"
    case textDecorationColor = "text-decoration-color"
}

/// Поддерживаемые атрибуты тегов-параграфов
///
/// - paragraphPaddingTop: отступ перед параграфом
/// - paragraphPaddingBottom: отступ после параграфа
enum ChatTextFormattingParagraphAttributes: String {
    case paragraphPaddingTop = "padding-top"
    case paragraphPaddingBottom = "padding-bottom"
}

/// Данные о форматировании участка текста сообщения
struct ChatTextRangeFormatting {
    
    /// Тег форматирования, в котором было задано форматирование
    let tag: String
    
    /// Участок текста
    var range: NSRange
    
    /// Распарсенные атрибуты тегов и их значения
    var tagAttributes: [ChatTextFormattingTagAttributes : String]
    
    /// Распарсенные атрибуты тегов параграфов и их значения
    var paragraphAttributes: [ChatTextFormattingParagraphAttributes : String]
}

/// Данные о форматировании текста сообщения
struct ChatTextFormatting {
    
    /// Текст без тегов форматирования
    let trimmedText: String
    
    /// Распарсенные данные о форматировании участков текста сообщения
    let formattingRanges: [ChatTextRangeFormatting]
    
    /// Цвет фона всего сообщения (если был задан)
    let messageBackgroundColor: UIColor?
}

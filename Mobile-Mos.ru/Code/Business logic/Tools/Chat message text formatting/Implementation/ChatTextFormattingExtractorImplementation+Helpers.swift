//
//  ChatTextFormattingExtractorImplementation+Helpers.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 01.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

extension ChatTextFormattingExtractorImplementation {
    
    //MARK: - Хелперы
     
    /// Получить цвет фона для текста и удалить его из массива с информацией о форматировании
    ///
    /// - Parameters:
    ///   - range: range, для которого ищем атрибут цвета фона
    ///   - rangeFormatting: массив с информацией о форматировании текста
    /// - Returns: цвет фона для текста (если найден)
    func popBackgroundColor(for range: NSRange, from rangeFormatting: inout [ChatTextRangeFormatting]) -> UIColor? {
        
        guard let backgroundColorIndex = rangeFormatting.index(where: { NSEqualRanges($0.range, range) && $0.tagAttributes[TagAttributes.backgroundColor] != nil }) else { return nil }
        
        var formatting = rangeFormatting[backgroundColorIndex]
        
        guard let colorString = formatting.tagAttributes[TagAttributes.backgroundColor] else { return nil }
        
        formatting.tagAttributes[TagAttributes.backgroundColor] = nil
        rangeFormatting[backgroundColorIndex] = formatting
        
        return colorObject(from: colorString)
    }
    
    /// Получить цвет из строки
    ///
    /// - Parameter value: строка
    /// - Returns: цвет
    func colorObject(from value: String) -> UIColor? {
        
        if let supportedColorString = SupportedColorStringDescription(rawValue: value) {
            switch supportedColorString {
            case .transparent:
                return UIColor.clear
            }
        }
        else {
            return UIColor.fromHexString(hexString: value)
        }
    }
    
    //MARK: - Генерация атрибутов для строки
    
    /// Добавить атрибут со стилем параграфа к атрибутной строке
    ///
    /// - Parameters:
    ///   - attributedString: атрибутная строка
    ///   - range: range в строке, по которому добавляем атрибут
    ///   - lineSpacing: расстояние между строками в тексте сообщения
    ///   - spacingBefore: отступ до параграфа
    ///   - spacingAfter: отступ после параграфа
    func addParagraphStyle(to attributedString: NSMutableAttributedString,
                           at range: NSRange,
                           with lineSpacing: CGFloat,
                           spacingBefore: CGFloat,
                           spacingAfter: CGFloat) {
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .left
        paragraphStyle.lineSpacing = lineSpacing
        paragraphStyle.paragraphSpacingBefore = spacingBefore
        paragraphStyle.paragraphSpacing = spacingAfter
        
        attributedString.addAttribute(NSAttributedStringKey.paragraphStyle, value: paragraphStyle, range: range)
    }
    
    /// Добавить атрибуты из тегов к атрибутной строке
    ///
    /// - Parameters:
    ///   - attributedString: атрибутная строка
    ///   - attributes: набор поддерживаемых атрибутов и их значений
    ///   - range: range в строке, по которому добавляем атрибуты
    ///   - fontPalette: палитра используемых шрифтов
    func addAttributes(to attributedString: NSMutableAttributedString,
                       from attributes: [TagAttributes : String],
                       at range: NSRange,
                       with fontPalette: ChatTextFormattingFontPalette) {
        
        guard !attributes.isEmpty else { return }
        
        if let backgroundColorValue = attributes[.backgroundColor] {
            attributedString.addAttribute(NSAttributedStringKey.backgroundColor, value: colorObject(from: backgroundColorValue) as Any, range: range)
        }
        
        if let textColorValue = attributes[.textColor] {
            attributedString.addAttribute(NSAttributedStringKey.foregroundColor, value: colorObject(from: textColorValue) as Any, range: range)
        }
        
        var font: UIFont? = nil
        
        if let fontWeight = SupportedFontWeight(rawValueOptional: attributes[.fontWeight]) {
            switch fontWeight {
            case .normal:
                font = fontPalette.normalFont
            case .medium:
                font = fontPalette.mediumFont
            case .bold:
                font = fontPalette.boldFont
            }
        }
        
        if let fontSizeString = attributes[.fontSize], let fontSize = Double(fontSizeString) {
            font = (font ?? fontPalette.normalFont).withSize(CGFloat(fontSize))
        }
        
        if font != nil {
            attributedString.addAttribute(NSAttributedStringKey.font, value: font as Any, range: range)
        }
        
        var textDecorationColorKey: NSAttributedStringKey? = nil
        
        if let textDecoration = SupportedTextDecoration(rawValueOptional: attributes[.textDecoration]) {
            switch textDecoration {
            case .underline:
                attributedString.addAttribute(NSAttributedStringKey.underlineStyle, value: NSNumber(value: NSUnderlineStyle.styleSingle.rawValue), range: range)
                textDecorationColorKey = NSAttributedStringKey.underlineColor
            case .lineThrough:
                attributedString.addAttribute(NSAttributedStringKey.strikethroughStyle, value: NSNumber(value: NSUnderlineStyle.styleSingle.rawValue), range: range)
                textDecorationColorKey = NSAttributedStringKey.strikethroughColor
            }
        }
        
        if let textDecorationColorValue = attributes[.textDecorationColor], textDecorationColorKey != nil {
            attributedString.addAttribute(textDecorationColorKey!, value: colorObject(from: textDecorationColorValue) as Any, range: range)
        }
    }
    
    /// Добавить атрибуты из тегов-параграфов к атрибутной строке
    ///
    /// - Parameters:
    ///   - attributedString: атрибутная строка
    ///   - attributes: набор поддерживаемых атрибутов и их значений
    ///   - tag: название тега
    ///   - range: range в строке, по которому добавляем атрибуты
    ///   - string: исходная строка
    ///   - fontPalette: палитра используемых шрифтов
    func addParagraphAttributes(to attributedString: NSMutableAttributedString,
                                from attributes: [ParagraphTagAttributes : String],
                                for tag: String,
                                at range: NSRange,
                                in string: NSString,
                                with fontPalette: ChatTextFormattingFontPalette) {
        
        guard paragraphTagsSet.contains(tag) else { return }
        guard range.length > 0 else { return }
        
        var searchRange = range
        if string.character(at: searchRange.location) == newLineCharacter {
            searchRange = searchRange.insetOnStartLocation(by: 1)
        }
        if string.character(at: searchRange.endLocation) == newLineCharacter {
            searchRange = searchRange.insetOnEndLocation(by: 1)
        }
        
        var spacingBeforeRange = range
        let firstNewlineRange = string.rangeOfCharacter(from: .newlines,
                                                        options: [],
                                                        range: searchRange)
        
        if firstNewlineRange.location != NSNotFound {
            spacingBeforeRange = NSMakeRange(range.location, firstNewlineRange.location - range.location)
        }
        
        var spacingAfterRange = range
        let lastNewlineRange = string.rangeOfCharacter(from: .newlines,
                                                       options: .backwards,
                                                       range: searchRange)
        
        if lastNewlineRange.location != NSNotFound {
            spacingAfterRange = NSMakeRange(lastNewlineRange.location + 1, range.endLocation - lastNewlineRange.location)
        }
        
        var spacingBefore = defaultParagraphSpacing
        var spacingAfter = defaultParagraphSpacing
        
        if let spaceString = attributes[.paragraphPaddingTop], let spaceSize = Double(spaceString) {
            spacingBefore = CGFloat(spaceSize)
        }
        
        if let spaceString = attributes[.paragraphPaddingBottom], let spaceSize = Double(spaceString) {
            spacingAfter = CGFloat(spaceSize)
        }
        
        if spacingBeforeRange == spacingAfterRange {
            addParagraphStyle(to: attributedString, at: spacingBeforeRange, with: fontPalette.lineSpacing, spacingBefore: spacingBefore, spacingAfter: spacingAfter)
        }
        else {
            addParagraphStyle(to: attributedString, at: spacingBeforeRange, with: fontPalette.lineSpacing, spacingBefore: spacingBefore, spacingAfter: 0)
            addParagraphStyle(to: attributedString, at: spacingAfterRange, with: fontPalette.lineSpacing, spacingBefore: 0, spacingAfter: spacingAfter)
        }
    }
}

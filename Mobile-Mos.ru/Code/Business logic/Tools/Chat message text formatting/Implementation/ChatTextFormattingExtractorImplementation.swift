//
//  ChatTextFormattingExtractorImplementation.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 01.11.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class ChatTextFormattingExtractorImplementation: ChatTextFormattingExtractor {
    
    /// Поддерживаемые теги для блоков текста
    let supportedContainerTags: [String] = ["div", "span"]
    
    /// Поддерживаемые теги для параграфов
    let supportedParagraphTags: [String] = ["p"]
    
    /// Отступ для параграфов по умолчанию
    let defaultParagraphSpacing: CGFloat = 5
    
    /// Набор тегов для параграфов
    lazy var paragraphTagsSet: Set = Set(self.supportedParagraphTags)
    
    /// Регулярка для нахождения в тексте тегов <br/>
    /// реальная регулярка без escaping - <\s*?br\s*?\/\s*?>
    /// проверить ее можно здесь - https://regex101.com
    let breakRegexPattern = "<\\s*?br\\s*?\\/\\s*?>"
    
    /// Формат регулярки для нахождения в тексте определенного тега
    /// реальная регулярка без escaping - <\s*?div(?:\s+(.+?)\s*?|\s*?)>(.*?)<\s*?\/\s*?div\s*?>
    /// проверить ее можно здесь - https://regex101.com
    let tagRegexFormat = "<\\s*?%@(?:\\s+(.+?)\\s*?|\\s*?)>(.*?)<\\s*?\\/\\s*?%@\\s*?>"
    
    /// Кол-во ranges для регулярки тега при совпадении
    /// (само совпадение на весь тег, строка с атрибутами тега, строка с содержимым тега - смотри регулярку)
    let numberOfRangesInTagMatch = 3
    
    /// Индекс range строки с атрибутами в совпадении для тега
    let tagMatchAttributesIndex = 1
    
    /// Индекс range строки с содержимым тега в совпадении для тега
    let tagMatchContentIndex = 2
    
    /// Строка с "переносом строки"
    let newLineString = "\n"
    
    /// Код символа с "переносом строки"
    let newLineCharacter = "\n".utf16.first
    
    /// Регулярка для нахождения атрибутов в тегах
    /// реальная регулярка без escaping - ([A-Za-z-]+)\s*?=\s*?\"(.+?)\"\s*?,*?
    /// проверить ее можно здесь - https://regex101.com
    let attributeRegexPattern = "([A-Za-z-]+)\\s*?=\\s*?\"(.+?)\"\\s*?,*?"
    
    /// Кол-во ranges для регулярки атрибутов в тегах при совпадении
    /// (само совпадение на весь атрибут, строка с названием атрибута, строка с содержимым атрибута - смотри регулярку)
    let numberOfRangesInAttributesMatch = 3
    
    /// Индекс range названия атрибута в совпадении для атрибута
    let attributesMatchAttributeNameIndex = 1
    
    /// Индекс range значения атрибута в совпадении для атрибута
    let attributesMatchAttributeValueIndex = 2
    
    /// Кеш атрибутных строк форматирования текстов сообщений
    static var formattingCache = NSCache<NSString, NSAttributedString>()
    
    //MARK: - Приватные методы
    
    /// Добавить переносы строк (распарсить теги <br/>)
    ///
    /// - Parameter string: исходная строка с тегами
    /// - Returns: были ли найдены переносы строк
    @discardableResult
    func addNewLines(in string: NSMutableString) -> Bool {
        
        let breakRegex = try! NSRegularExpression(pattern: breakRegexPattern, options: [.dotMatchesLineSeparators])
        let textRange = NSMakeRange(0, string.length)
        
        return breakRegex.replaceMatches(in: string, options: [], range: textRange, withTemplate: newLineString) > 0
    }
    
    /// Обработать тег
    ///
    /// - Parameters:
    ///   - tag: название тега
    ///   - string: исходная строка с тегами
    ///   - formatting: массив с информацией о форматировании участков текста
    /// - Returns: цвет фона всего сообщения (если был обнаружен)
    func process(tag: String,
                 in string: NSMutableString,
                 formatting: inout [ChatTextRangeFormatting]) -> UIColor? {
        
        var messageBackgroundColor: UIColor? = nil
        
        let tagRegexPattern = String(format: tagRegexFormat, tag, tag)
        let tagRegex = try! NSRegularExpression(pattern: tagRegexPattern, options: [.dotMatchesLineSeparators])
        let textRange = NSMakeRange(0, string.length)
        let tagMathes = tagRegex.matches(in: string as String, options: [], range: textRange)
        
        guard !tagMathes.isEmpty else { return messageBackgroundColor }
        
        var rangeOffset = 0
        
        for match in tagMathes {
            
            guard match.numberOfRanges == numberOfRangesInTagMatch else { continue }
            
            let attributesRange = match.range(at: tagMatchAttributesIndex).offset(by: rangeOffset)
            let attributes = processAttributes(in: attributesRange, for: tag, in: string)
            
            guard let strippedRange = strip(tag: tag,
                                            with: tagRegex,
                                            for: match,
                                            in: string,
                                            matchOffset: &rangeOffset,
                                            existingFormatting: &formatting) else { continue }
            
            let rangeFormatting = ChatTextRangeFormatting(tag: tag, range: strippedRange, tagAttributes: attributes.tagAttributes, paragraphAttributes: attributes.paragraphTagAttributes)
            formatting.append(rangeFormatting)
            
            // range совпадения равен range строки - значит тег содержит весь текст сообщения
            if NSEqualRanges(match.range, textRange) {
                let strippedStringRange = NSMakeRange(0, string.length)
                messageBackgroundColor = popBackgroundColor(for: strippedStringRange, from: &formatting)
            }
        }
        
        return messageBackgroundColor
    }
    
    /// Обработать атрибуты тега
    ///
    /// - Parameters:
    ///   - range: range в исходной строке, в котором находятся атрибуты
    ///   - tag: обрабатываемый тег
    ///   - string: исходная строка с тегами
    /// - Returns: набор атрибутов и их значений
    func processAttributes(in range: NSRange,
                           for tag: String,
                           in string: NSMutableString) -> TagAttributesValues {
        
        // ведь атрибутов в теге может не быть
        guard range.location != NSNotFound else { return TagAttributesValues() }
        guard range.length > 0 else { return TagAttributesValues() }
        
        let attributesRegex = try! NSRegularExpression(pattern: attributeRegexPattern, options: [.dotMatchesLineSeparators])
        let attributeMatches = attributesRegex.matches(in: string as String, options: [], range: range)
        
        guard !attributeMatches.isEmpty else { return TagAttributesValues() }
        
        var supportedAttributes = [TagAttributes : String]()
        var supportedParagraphAttributes = [ParagraphTagAttributes : String]()
        
        for match in attributeMatches {
            
            guard match.numberOfRanges == numberOfRangesInAttributesMatch else { continue }
            
            let name = string.substring(with: match.range(at: attributesMatchAttributeNameIndex))
            let value = string.substring(with: match.range(at: attributesMatchAttributeValueIndex))
            
            if let attribute = TagAttributes(rawValue: name) {
                supportedAttributes[attribute] = value
            }
            
            if let paragraphAttribute = ParagraphTagAttributes(rawValue: name) {
                supportedParagraphAttributes[paragraphAttribute] = value
            }
        }
        
        return TagAttributesValues(tagAttributes: supportedAttributes, paragraphTagAttributes: supportedParagraphAttributes)
    }
    
    /// Вырезать тег из исходной строки
    ///
    /// - Parameters:
    ///   - tag: название тега
    ///   - tagRegex: регулярка для тега
    ///   - match: совпадение для тега в исходной строке (кусок строки с этим тегом)
    ///   - string: исходная строка с тегами
    ///   - matchOffset: сдвиг для совпадения (образуется из-за удаления тегов по мере парсинга)
    ///   - existingFormatting: массив с информацией о форматировании участков текста
    /// - Returns: range содержимого тега в обновленной исходной строке (после удаления переданного тега)
    @discardableResult
    func strip(tag: String,
               with tagRegex: NSRegularExpression,
               for match: NSTextCheckingResult,
               in string: NSMutableString,
               matchOffset: inout Int,
               existingFormatting: inout [ChatTextRangeFormatting]) -> NSRange? {
        
        guard match.numberOfRanges == numberOfRangesInTagMatch else { return nil }
        
        var replacement = tagRegex.replacementString(for: match,
                                                     in: string as String,
                                                     offset: matchOffset,
                                                     template: "$\(tagMatchContentIndex)")
        
        let matchRange = match.range.offset(by: matchOffset)
        
        addNewLinesIfNeeded(to: &replacement, for: tag, with: matchRange, in: string)
        string.replaceCharacters(in: matchRange, with: replacement)
        adjustRanges(in: &existingFormatting, for: match, of: tag, matchOffset: &matchOffset, replacement: replacement)
        
        return NSMakeRange(matchRange.location, (replacement as NSString).length)
    }
    
    /// Добавить переносы строк к строке, на которую будет заменен тег, если это нужно
    ///
    /// - Parameters:
    ///   - tagReplacement: строка, на которую будет заменен тег
    ///   - tag: название тега
    ///   - matchRange: range совпадения для тега в исходной строке
    ///   - string: исходная строка с тегами
    func addNewLinesIfNeeded(to tagReplacement: inout String,
                             for tag: String,
                             with matchRange: NSRange,
                             in string: NSString) {
        
        guard paragraphTagsSet.contains(tag) else { return }
        
        let currentLocation = matchRange.location
        let endLocation = matchRange.location + matchRange.length
        
        let lastBeforeLocation = currentLocation - 1
        let firstAfterLocation = endLocation + 1
        
        let shouldAddBefore = currentLocation > 0 && string.character(at: lastBeforeLocation) != newLineCharacter
        let shouldAddAfter = endLocation < string.length && string.character(at: firstAfterLocation) != newLineCharacter
        
        if shouldAddBefore {
            tagReplacement = newLineString + tagReplacement
        }
        
        if shouldAddAfter {
            tagReplacement = tagReplacement + newLineString
        }
    }
    
    /// Поправить range в наборе атрибутов после обработки тега
    ///
    /// - Parameters:
    ///   - formatting: набор данных о форматировании учатсков текста
    ///   - match: совпадение для тега в исходной строке (кусок строки с этим тегом)
    ///   - tag: название тега
    ///   - matchOffset: сдвиг для совпадения (образуется из-за удаления тегов по мере парсинга)
    ///   - replacement: строка, которой будет заменен тег
    func adjustRanges(in formatting: inout [ChatTextRangeFormatting],
                      for match: NSTextCheckingResult,
                      of tag: String,
                      matchOffset: inout Int,
                      replacement: String) {
        
        var matchRange = match.range
        matchRange.location += matchOffset
        
        let tagContentRange = match.range(at: tagMatchContentIndex)
        let replacementLength = (replacement as NSString).length
        let tagOpeningLength = tagContentRange.location - match.range.location
        
        let replacementAndTagLengthDifference = replacementLength - matchRange.length
        let replacementAndContentLengthDifference = replacementLength - tagContentRange.length
        
        matchOffset += replacementAndTagLengthDifference
        
        formatting.forEachMutate {
            // атрибуты располагаются после тега, значит просто сдвигаем их на разницу между длиной тега и строчкой, которую поставили вместо него
            if $0.range.location >= matchRange.location + matchRange.length {
                $0.range.location += replacementAndTagLengthDifference
            }
                // атрибуты располагаются внутри тега, значит сдвигаем их на длину открывающего тега
            else if $0.range.location >= matchRange.location {
                $0.range.location -= tagOpeningLength
                guard $0.tag != tag else { return }
                $0.range.length += replacementAndContentLengthDifference
            }
                // атрибуты относятся к другому тегу и, начинаясь до тега (предыдущие условия не выполнились), заканчиваются после его начала, значит им надо подправить длину
            else if $0.tag != tag && $0.range.location + $0.range.length > matchRange.location {
                $0.range.length += replacementAndTagLengthDifference
            }
        }
    }
    
    //MARK: - ChatTextFormattingExtractor
    
    func extractFormatting(from text: String) -> ChatTextFormatting? {
        
        guard !text.isEmpty else { return nil }
        
        let supportedTags = supportedContainerTags + supportedParagraphTags
        
        let trimmedText = NSMutableString(string: text)
        var textRangeFormatting = [ChatTextRangeFormatting]()
        var messageBackgroundColor: UIColor? = nil
        
        let foundNewLines = addNewLines(in: trimmedText)
        
        for tag in supportedTags {
            
            let possibleBackgroundColor = process(tag: tag,
                                                  in: trimmedText,
                                                  formatting: &textRangeFormatting)
            
            if messageBackgroundColor == nil {
                messageBackgroundColor = possibleBackgroundColor
            }
        }
        
        guard foundNewLines || !textRangeFormatting.isEmpty || messageBackgroundColor != nil else {
            return nil
        }
        
        return ChatTextFormatting(trimmedText: trimmedText as String, formattingRanges: textRangeFormatting, messageBackgroundColor: messageBackgroundColor)
    }
    
    func attributedString(from formatting: ChatTextFormatting, with fontPalette: ChatTextFormattingFontPalette, for text: String) -> NSAttributedString {
        
        typealias Extractor = ChatTextFormattingExtractorImplementation
        let key = text as NSString
        
        if let cachedString = Extractor.formattingCache.object(forKey: key) {
            return cachedString
        }
        
        let attributedString = NSMutableAttributedString(string: formatting.trimmedText)
        for rangeFormatting in formatting.formattingRanges {
            addAttributes(to: attributedString, from: rangeFormatting.tagAttributes, at: rangeFormatting.range, with: fontPalette)
            addParagraphAttributes(to: attributedString, from: rangeFormatting.paragraphAttributes, for: rangeFormatting.tag, at: rangeFormatting.range, in: formatting.trimmedText as NSString, with: fontPalette)
        }
        
        Extractor.formattingCache.setObject(attributedString, forKey: key)
        
        return attributedString
    }
}

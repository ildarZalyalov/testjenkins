//
//  ChatTextFormattingExtractorImplementation+Types.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 01.11.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

extension ChatTextFormattingExtractorImplementation {
    
    /// Поддерживаемые начертания шрифтов
    ///
    /// - normal: обычное
    /// - medium: среднее
    /// - bold: жирное
    enum SupportedFontWeight: String {
        case normal
        case medium
        case bold
    }
    
    /// Поддерживаемые дополнительные элементы в тексте
    ///
    /// - lineThrough: перечеркивание
    /// - underline: подчеркивание
    enum SupportedTextDecoration: String {
        case lineThrough = "line-through"
        case underline
    }
    
    /// Поддерживаемые текстовые описания цветов
    ///
    /// - transparent: прозрачный
    enum SupportedColorStringDescription: String {
        case transparent
    }
    
    typealias TagAttributes = ChatTextFormattingTagAttributes
    typealias ParagraphTagAttributes = ChatTextFormattingParagraphAttributes
    
    /// Набор атрибутов и их значений
    struct TagAttributesValues {
        
        /// Атрибуты тегов
        let tagAttributes: [TagAttributes : String]
        
        /// Атрибуты тегов-параграфов
        let paragraphTagAttributes: [ParagraphTagAttributes : String]
        
        init() {
            tagAttributes = [:]
            paragraphTagAttributes = [:]
        }
        
        init(tagAttributes: [TagAttributes : String], paragraphTagAttributes: [ParagraphTagAttributes : String]) {
            self.tagAttributes = tagAttributes
            self.paragraphTagAttributes = paragraphTagAttributes
        }
    }
}

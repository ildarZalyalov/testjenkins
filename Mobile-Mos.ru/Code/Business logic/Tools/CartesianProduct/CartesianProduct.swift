//
//  CartesianProduct.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 15.08.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Структура описывающая итерируемое декартово множество
struct CartesianProductIterator<X, Y>: IteratorProtocol where X: IteratorProtocol, Y: Collection {
    
    fileprivate var xIterator: X
    fileprivate let yCollection: Y
    
    fileprivate var x: X.Element?
    fileprivate var yIterator: Y.Iterator
    
    init(xs: X, ys: Y) {
        xIterator = xs
        yCollection = ys
        
        x = xIterator.next()
        yIterator = yCollection.makeIterator()
    }
    
    
    typealias Element = (X.Element, Y.Iterator.Element)
    
    mutating func next() -> Element? {
        guard !yCollection.isEmpty else {
            return nil
        }
        
        guard let someX = x else {
            return nil
        }
        
        guard let someY = yIterator.next() else {
            yIterator = yCollection.makeIterator()
            x = xIterator.next()
            return next()
        }
        
        return (someX, someY)
    }
}


/// Структура описывающая декартово множество
struct CartesianProductSequence<X, Y>: Sequence where X: Sequence, Y: Collection {
    typealias Iterator = CartesianProductIterator<X.Iterator, Y>
    
    fileprivate let xs: X
    fileprivate let ys: Y
    
    init(xs: X, ys: Y) {
        self.xs = xs
        self.ys = ys
    }
    
    func makeIterator() -> Iterator {
        return Iterator(xs: xs.makeIterator(), ys: ys)
    }
}

/// Декартово произведение двух множеств
///
/// - Parameters:
///   - xSequence: первое множество объектов
///   - ySequence: второе множество объектов (то, которое должно итерироваться внутри первого)
/// - Returns: коллекция полученная из двух множеств
func product<X, Y>(_ xSequence: X, _ ySequence: Y) -> CartesianProductSequence<X, Y> where X: Sequence, Y: Collection {
    return CartesianProductSequence(xs: xSequence, ys: ySequence)
}

//
//  DelayedSelectorPerformer.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 30.08.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Компонент для вызова селектора с задержкой без удержания strong ссылки на target
class DelayedSelectorPerformer: NSObject {
    
    /// Настоящий target
    fileprivate(set) weak var realPerformer: NSObject?
    
    let selector: Selector
    var argument: Any?
    
    init(target: NSObject, selector: Selector) {
        realPerformer = target
        self.selector = selector
    }
    
    /// Вызвать селектор с задержкой
    ///
    /// - Parameter delay: задержка
    func perform(afterDelay delay: TimeInterval) {
        perform(#selector(delayedSelector(argument:)), with: argument, afterDelay: delay)
    }
    
    /// Отменить вызов селектора
    func cancelDelayedPerform() {
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(delayedSelector(argument:)), object: argument)
    }
    
    @objc
    fileprivate func delayedSelector(argument: Any?) {
        guard let performer = realPerformer else { return }
        performer.perform(selector, with: argument)
    }
}

//
//  UserInfoAndSettingsServiceImplementation.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 13.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation
import UserNotifications

class UserInfoAndSettingsServiceImplementation: UserInfoAndSettingsService {
    
    /// Типы сообщений в сокете
    ///
    /// - getUserProfile: запросить данные о пользователе
    /// - getUserProfileResponse: получить данные о пользователе
    /// - setUserNotification: установить настройку уведомлений для чата
    /// - getUserNotificationSettings: запросить настройки уведомлений
    /// - getUserNotificationSettingsResponse: получить настройки уведомлений
    fileprivate enum Event: String {
        case getUserProfile
        case getUserProfileResponse
        case setUserNotification
        case getUserNotificationSettings
        case getUserNotificationSettingsResponse
    }
    
    //MARK: - Статические компоненты
    
    // Обработчики, заданные один раз в каждом экземпляре сервиса
    
    fileprivate static var userInfoHandlers = ThreadSafeDictionary<UUID, UserInfoObtainHandler>()
    
    // Обработчики, добавляемые экземплярами сервиса по вызову методов в неограниченном кол-ве
    
    fileprivate static var settingsRequestsQueue = ThreadSafeSerialEventQueue<NotificationsSettingObtainMultipleResult>()
    
    static var currentUserManager: CurrentUserManager!
    static var sharedJsonMapper: JsonResourceMapper!
    static var userDefaults: UserDefaults!
    
    static var sharedSocket: Socket! {
        
        didSet {
            
            // Ответ на попытку получить данные пользователя
            
            sharedSocket.onAsync(event: Event.getUserProfileResponse.rawValue) { socketData, _ in
                
                var result: UserInfoObtainResult
                
                defer {
                    
                    let handlers = userInfoHandlers.currentValues()
                    
                    DispatchQueue.main.async(execute: {
                        handlers.forEach { $0(result) }
                    })
                }
                
                guard let json = (socketData as? [NSDictionary])?.first else {
                    result = .failure(error: HTTPRequestError.jsonParsingNoResponseDataError)
                    return
                }
                
                let noErrorCode = UserInfoResponse.ErrorCode.noError.rawValue
                let response = sharedJsonMapper.mapJson(json, toResourceOfType: UserInfoResponse.self)
                
                if response.userInfo != nil && response.errorCode == noErrorCode {
                    currentUserManager.updateCurrentUserInfo(with: response.userInfo!)
                    updateUserInfoObtainDate()
                    result = .success(userInfo: response.userInfo!)
                }
                else {
                    result = .failure(error: UserInfoObtainResultErrors.userNotFound)
                }
            }
            
            // Получение настроек уведомлений
            
            sharedSocket.onAsync(event: Event.getUserNotificationSettingsResponse.rawValue) { socketData, _ in
                
                guard let handler = settingsRequestsQueue.dequeue()?.handler else { return }
                
                var result: NotificationsSettingObtainMultipleResult
                
                defer {
                    DispatchQueue.main.async(execute: {
                        handler(result)
                    })
                }
                
                guard let json = (socketData as? [NSDictionary])?.first else {
                    result = .failure(error: HTTPRequestError.jsonParsingNoResponseDataError)
                    return
                }
                
                let settings = notificationSettings(from: json)
                result = .success(settings: settings)
                
                currentUserManager.updateCurrentUserNotificationsSettings(with: settings)
            }
            
            // Восстановление соединения (перезапуск очередей)
            
            sharedSocket.onAsync(event: SocketEvent.connect.rawValue) { _,_  in
                settingsRequestsQueue.resume()
            }
            
            // Обработка обрыва соединения (остановка очередей)
            
            sharedSocket.onAsync(event: SocketEvent.disconnect.rawValue) { _,_  in
                settingsRequestsQueue.pause()
            }
        }
    }
    
    //MARK: - Компоненты экземпляра
    
    fileprivate typealias Service = UserInfoAndSettingsServiceImplementation
    
    fileprivate let userInfoHandlerId = UUID()
    fileprivate let changeNotificationSettingTimeout: Double = 10
    
    var notificationCenter: NotificationCenter!
    var application: UIApplication!
    
    deinit {
        Service.userInfoHandlers[userInfoHandlerId] = nil
    }
    
    //MARK: - Приватные методы
    
    /// Обновить дату последнего успешного обновления данных о пользователе
    fileprivate static func updateUserInfoObtainDate() {
        
        let key = ApplicationUserDefaultsKey.userInfoCacheUpdateDate.rawValue
        
        userDefaults.set(Date(), forKey: key)
        userDefaults.synchronize()
    }
    
    /// Распарсить список настроек уведомлений из json
    ///
    /// - Parameter json: полученный от сервера json
    /// - Returns: список настроек уведомлений
    fileprivate static func notificationSettings(from json: NSDictionary) -> [UserNotificationsSetting] {
        
        guard let keys = json.allKeys as? [String] else { return [] }
        
        var settings = [UserNotificationsSetting]()
        for key in keys {
            guard let notificationsEnabled = json[key] as? Bool else { continue }
            settings.append(UserNotificationsSetting(chatId: key, areNotificationsEnabled: notificationsEnabled))
        }
        
        return settings
    }
    
    /// Сохранить настройку уведомлений для чата
    ///
    /// - Parameters:
    ///   - conversation: чат
    ///   - value: значение настройки уведомлений (включены ли уведомления)
    fileprivate func saveNotificationSetting(for conversation: Conversation, to value: Bool) {
        
        var newSettings = currentUser.notificationsSettings.filter { $0.chatId != conversation.itemId }
        let newSetting = UserNotificationsSetting(chatId: conversation.itemId, areNotificationsEnabled: value)
        
        newSettings.append(newSetting)
        Service.currentUserManager.updateCurrentUserNotificationsSettings(with: newSettings)
    }
    
    //MARK: - UserInfoAndSettingsService
    
    var currentUser: User {
        return Service.currentUserManager.currentUser
    }
    
    var lastUserInfoObtainDate: Date? {
        let key = ApplicationUserDefaultsKey.userInfoCacheUpdateDate.rawValue
        return Service.userDefaults.object(forKey: key) as? Date
    }
    
    var userInfoHandler: UserInfoObtainHandler? {
        get { return Service.userInfoHandlers[userInfoHandlerId] }
        set { Service.userInfoHandlers[userInfoHandlerId] = newValue }
    }
    
    func obtainCurrentUserInfo() {
        
        guard currentUser.userId != nil else { return }
        
        let request = UserInfoRequest(sessionId: currentUser.sessionId, userId: currentUser.userId!)
        Service.sharedSocket.send(event: Event.getUserProfile.rawValue, with: [request.serializeToJson()])
    }
    
    func obtainNotificationsAuthorizationStatus(completion: @escaping NotificationsAuthorizationStatusObtainHandler) {
        
        guard !application.isRegisteredForRemoteNotifications else {
            completion(.authorized)
            return
        }
        
        if #available(iOS 10.0, *) {
            
            let center = UNUserNotificationCenter.current()
            
            center.getNotificationSettings { settings in
                
                var status: NotificationsAuthorizationStatus
                
                defer {
                    DispatchQueue.main.async {
                        completion(status)
                    }
                }
                
                switch settings.authorizationStatus {
                case .authorized:
                    status = .authorized
                case .denied:
                    status = .denied
                case .notDetermined:
                    status = .notDetermined
                }
            }
        }
        else {
            
            guard let settings = application.currentUserNotificationSettings else {
                completion(.notDetermined)
                return
            }
            
            let status: NotificationsAuthorizationStatus = settings.types.isEmpty ? .denied : .authorized
            completion(status)
        }
    }
    
    func refreshNotificationSettings(completion: NotificationsSettingObtainMultipleHandler?) {
        
        obtainNotificationsAuthorizationStatus { [weak self] status in
            
            guard let strongSelf = self else { return }
            
            guard status == .authorized else {
                completion?(.failure(error: NotificationsSettingObtainErrors.notificationsDisabledOnDevice))
                return
            }
            
            guard let userId = strongSelf.currentUser.userId else {
                completion?(.failure(error: NotificationsSettingObtainErrors.userNotAuthorized))
                return
            }
            
            let emitter = {
                Service.sharedSocket.send(event: Event.getUserNotificationSettings.rawValue, with: [["user_id" : userId]])
            }
            
            let handler: NotificationsSettingObtainMultipleHandler = { settingsResult in
                completion?(settingsResult)
            }
            
            let eventBlock = SerialEventBlock(emitter: emitter, handler: handler)
            Service.settingsRequestsQueue.enqueue(element: eventBlock)
        }
    }
    
    func changeNotificationSetting(for conversation: Conversation, to value: Bool, confirmationBlock: @escaping NotificationsSettingChangeHandler) {
        
        guard let userId = currentUser.userId else {
            confirmationBlock(false)
            return
        }
        
        let request = UserNotificationsSettingChangeRequest(sessionId: currentUser.sessionId, userId: userId, chatId: conversation.itemId, isEnabled: value)
        
        Service.sharedSocket.send(event: Event.setUserNotification.rawValue, with: [request.serializeToJson()], timingOutAfter: changeNotificationSettingTimeout) { [weak self] timedOut, parameters in
            
            guard let strongSelf = self else { return }
            
            var changeSuccess: Bool = false
            
            if !timedOut, let success = parameters.first as? Bool {
                changeSuccess = success
            }
            
            if changeSuccess {
                strongSelf.saveNotificationSetting(for: conversation, to: value)
            }
            
            confirmationBlock(changeSuccess)
        }
    }
}

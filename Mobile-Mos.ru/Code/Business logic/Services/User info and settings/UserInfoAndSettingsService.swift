//
//  UserInfoAndSettingsService.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 13.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Ошибки запроса данных о пользователе
///
/// - userNotAuthorized: пользователь не авторизован
/// - userNotFound: пользователь не найден
enum UserInfoObtainResultErrors: Error {
    case userNotAuthorized
    case userNotFound
}

/// Результат запроса данных о пользователе
///
/// - success: успех, связанное значение - данные о пользователе
/// - failure: провал, связанное значение - ошибка
enum UserInfoObtainResult {
    case success(userInfo: UserInfo)
    case failure(error: Error)
}

/// Ошибки получения настроек уведомлений
///
/// - notificationsDisabledOnDevice: уведомления отключены на устройстве
/// - userNotAuthorized: пользователь не авторизован
enum NotificationsSettingObtainErrors: Error {
    case notificationsDisabledOnDevice
    case userNotAuthorized
}

/// Статус авторизации уведомлений на устройстве
///
/// - notDetermined: не установлен
/// - authorized: авторизованы
/// - denied: запрещены
enum NotificationsAuthorizationStatus {
    case notDetermined
    case authorized
    case denied
}

/// Результат запроса на получение списка настроек уведомлений
///
/// - success: успех, связанное значение - список настроек уведомлений
/// - failure: провал, связанное значение - ошибка
enum NotificationsSettingObtainMultipleResult {
    case success(settings: [UserNotificationsSetting])
    case failure(error: Error)
}

/// Обработчик результата запроса данных о пользователе
typealias UserInfoObtainHandler = (UserInfoObtainResult) -> Void

/// Блок обработки получения статуса авторизации уведомлений на устройстве, единственный параметр - результат запроса
typealias NotificationsAuthorizationStatusObtainHandler = (NotificationsAuthorizationStatus) -> Void

/// Блок обработки получения списка настроек уведомлений, единственный параметр - результат запроса
typealias NotificationsSettingObtainMultipleHandler = (NotificationsSettingObtainMultipleResult) -> Void

/// Блок обработки изменения настройки уведомлений, единственный параметр - удалось ли изменить настройку
typealias NotificationsSettingChangeHandler = (Bool) -> Void

/// Сервис для работы с данными и настройками пользователя
protocol UserInfoAndSettingsService {
    
    /// Текущий пользователь
    var currentUser: User { get }
    
    /// Дата последнего успешного обновления данных о пользователе
    var lastUserInfoObtainDate: Date? { get }
    
    /// Блок обработки результата запроса данных о пользователе, вызывается при получении новых личных данных пользователя
    var userInfoHandler: UserInfoObtainHandler? { get set }
    
    /// Запросить обновление личных данных текущего пользователя
    func obtainCurrentUserInfo()
    
    /// Получить статус авторизации уведомлений на устройстве
    ///
    /// - Parameter completion: блок обработки получения статуса авторизации уведомлений на устройстве
    func obtainNotificationsAuthorizationStatus(completion: @escaping NotificationsAuthorizationStatusObtainHandler)
    
    /// Обновить с сервера кешированные настройки уведомлений
    ///
    /// - Parameter completion: блок обработки получения списка настроек уведомлений
    func refreshNotificationSettings(completion: NotificationsSettingObtainMultipleHandler?)
    
    /// Изменить настройку уведомлений
    ///
    /// - Parameters:
    ///   - conversation: диалог, для которого меняем настройку
    ///   - value: значение настройки (включены ли уведомления)
    ///   - confirmationBlock: блок обработки изменения настройки уведомлений
    func changeNotificationSetting(for conversation: Conversation, to value: Bool, confirmationBlock: @escaping NotificationsSettingChangeHandler)
}

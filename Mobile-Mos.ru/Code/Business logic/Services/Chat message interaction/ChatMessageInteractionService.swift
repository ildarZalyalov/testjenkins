//
//  ChatMessageInteractionService.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 27.06.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Блок обработки ответа на запрос на обратную связь при взаимодействии с сообщением, единственный параметр - ответ на запрос
typealias ChatMessageCallbackResponseHandler = (ChatMessageCallbackResponse) -> Void

/// Блок обработки получения уведомления для чата, единственный параметр - уведомление
typealias ChatNotificationHandler = (ChatNotification) -> Void

/// Блок обработки получения уведомления о том, что в чате "печатают"/"перестали печатать"
typealias ChatMessageTypingHandler = (ChatMessageTypingEvent) -> Void

/// Сервис для обработки взаимодействий с сообщениями в диалоге
protocol ChatMessageInteractionService {
    
    /// Блок обработки ответов на запрос на обратную связь, вызывается для каждого полученного ответа
    var callbackResponseHandler: ChatMessageCallbackResponseHandler? { get set }
    
    /// Блок обработки получения уведомления для чата, вызывается для каждого полученного уведомления
    var chatNotificationHandler: ChatNotificationHandler? { get set }
    
    /// Блок обработки получения уведомления о том, что в чате "печатают"
    var typingHandler: ChatMessageTypingHandler? { get set }
    
    /// Блок обработки получения уведомления о том, что в чате "перестали печатать"
    var typingStoppedHandler: ChatMessageTypingHandler? { get set }
    
    /// Отправить запрос на обратную связь при взаимодействии с сообщением
    ///
    /// - Parameters:
    ///   - data: данные запроса
    ///   - user: текущий пользователь
    ///   - message: сообщение, с которым взаимодействует пользователь, если есть
    ///   - embeddedMessage: вложенное сообщение, если сообщение является каруселью
    ///   - conversation: диалог, в котором находится пользователь (например, Привет, Москва!)
    func sendCallback(with data: String, by user: User, for message: ChatMessage?, embeddedMessage: ChatMessage?, in conversation: Conversation)
}

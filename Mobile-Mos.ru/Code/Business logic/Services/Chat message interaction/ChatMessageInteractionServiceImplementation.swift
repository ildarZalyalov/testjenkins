//
//  ChatMessageInteractionServiceImplementation.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 27.06.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class ChatMessageInteractionServiceImplementation: ChatMessageInteractionService {
    
    /// Типы сообщений в сокете
    ///
    /// - answerCallbackQuery: отправить запрос на обратную связь при взаимодействии с сообщением
    /// - answerCallbackQueryResponse: получить ответ на запрос на обратную связь при взаимодействии с сообщением
    /// - chatNotification: получить уведомление в чате
    /// - sendMessageTyping: отобразить событие "собеседник печатает"
    /// - sendMessageTypingStopped: скрыть событие "собеседник печатает"
    fileprivate enum Event: String {
        case answerCallbackQuery
        case answerCallbackQueryResponse
        case chatNotification
        case sendMessageTyping
        case sendMessageTypingStopped
    }
    
    //MARK: - Статические компоненты
    
    // Обработчики, заданные один раз в каждом экземпляре сервиса
    
    fileprivate static var callbackResponseHandlers = ThreadSafeDictionary<UUID, ChatMessageCallbackResponseHandler>()
    fileprivate static var chatNotificationHandlers = ThreadSafeDictionary<UUID, ChatNotificationHandler>()
    fileprivate static var typingHandlers = ThreadSafeDictionary<UUID, ChatMessageTypingHandler>()
    fileprivate static var typingStoppedHandlers = ThreadSafeDictionary<UUID, ChatMessageTypingHandler>()
    
    static var sharedJsonMapper: JsonResourceMapper!
    static var sharedSocket: Socket! {
        
        didSet {
            
            // Получение ответа на запрос на обратную связь
            
            sharedSocket.onAsync(event: Event.answerCallbackQueryResponse.rawValue) { socketData, _ in
                
                guard let json = (socketData as? [NSDictionary])?.first else {
                    return
                }
                
                let response = sharedJsonMapper.mapJson(json, toResourceOfType: ChatMessageCallbackResponse.self)

                let handlers = callbackResponseHandlers.currentValues()
                DispatchQueue.main.async(execute: {
                    handlers.forEach { $0(response) }
                })
            }
            
            // Получение уведомления в чате
            
            sharedSocket.onAsync(event: Event.chatNotification.rawValue) { socketData, _ in
                
                guard let json = (socketData as? [NSDictionary])?.first else {
                    return
                }
                
                let response = sharedJsonMapper.mapJson(json, toResourceOfType: ChatNotification.self)
                
                let handlers = chatNotificationHandlers.currentValues()
                DispatchQueue.main.async(execute: {
                    handlers.forEach { $0(response) }
                })
            }
            
            // Получение события "собеседник печатает"
            
            sharedSocket.onAsync(event: Event.sendMessageTyping.rawValue) { socketData, _ in
                
                guard let json = (socketData as? [NSDictionary])?.first else {
                    return
                }
                
                let event = sharedJsonMapper.mapJson(json, toResourceOfType: ChatMessageTypingEvent.self)
                
                let handlers = typingHandlers.currentValues()
                DispatchQueue.main.async(execute: {
                    handlers.forEach { $0(event) }
                })
            }
            
            // Получение события "собеседник перестал печатать"
            
            sharedSocket.onAsync(event: Event.sendMessageTypingStopped.rawValue) { socketData, _ in
                
                guard let json = (socketData as? [NSDictionary])?.first else {
                    return
                }
                
                let event = sharedJsonMapper.mapJson(json, toResourceOfType: ChatMessageTypingEvent.self)
                
                let handlers = typingStoppedHandlers.currentValues()
                DispatchQueue.main.async(execute: {
                    handlers.forEach { $0(event) }
                })
            }
        }
    }
    
    //MARK: - Локальные компоненты
    
    fileprivate typealias Service = ChatMessageInteractionServiceImplementation
    fileprivate let callbackResponseHandlerId = UUID()
    fileprivate let chatNotificationHandlerId = UUID()
    fileprivate let typingHandlerId = UUID()
    fileprivate let typingStoppedHandlerId = UUID()
    
    deinit {
        Service.callbackResponseHandlers[callbackResponseHandlerId] = nil
        Service.chatNotificationHandlers[chatNotificationHandlerId] = nil
        Service.typingHandlers[typingHandlerId] = nil
        Service.typingStoppedHandlers[typingStoppedHandlerId] = nil
    }
    
    //MARK: - ChatMessageInteractionService
    
    var callbackResponseHandler: ChatMessageCallbackResponseHandler? {
        get { return Service.callbackResponseHandlers[callbackResponseHandlerId] }
        set { Service.callbackResponseHandlers[callbackResponseHandlerId] = newValue }
    }
    
    var chatNotificationHandler: ChatNotificationHandler? {
        get { return Service.chatNotificationHandlers[chatNotificationHandlerId] }
        set { Service.chatNotificationHandlers[chatNotificationHandlerId] = newValue }
    }
    
    var typingHandler: ChatMessageTypingHandler? {
        get { return Service.typingHandlers[typingHandlerId] }
        set { Service.typingHandlers[typingHandlerId] = newValue }
    }
    
    var typingStoppedHandler: ChatMessageTypingHandler? {
        get { return Service.typingStoppedHandlers[typingStoppedHandlerId] }
        set { Service.typingStoppedHandlers[typingStoppedHandlerId] = newValue }
    }
    
    func sendCallback(with data: String, by user: User, for message: ChatMessage?, embeddedMessage: ChatMessage?, in conversation: Conversation) {
        let query = ChatMessageCallbackQuery(with: data, user: user, conversation: conversation, message: message, embeddedMessage: embeddedMessage)
        Service.sharedSocket.send(event: Event.answerCallbackQuery.rawValue, with: [query.serializeToJson()])
    }
}

//
//  AddressAutocompleteService.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 02.04.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Ошибки автокомплита адресов
///
/// - seachStringTooShort: слишком короткая строка поиска
enum AddressAutocompleteErrors: Error {
    case seachStringTooShort
}

/// Результат выполнения запроса на получение списка адресов
///
/// - success: успех, связанное значение - массив адресов
/// - failure: провал, связанное значение - ошибка
enum AddressAutocompleteMultipleObtainResult {
    case success(addresses: [AddressAutocompleteResponse])
    case failure(error: Error)
}

/// Обработчик получения результата запроса на получение списка адресов
typealias AddressAutocompleteMultipleObtainResultHandler = (AddressAutocompleteMultipleObtainResult) -> ()

/// Сервис для работы с автокомплитом адресов
protocol AddressAutocompleteService {
    
    /// Минимальная длина строки поиска адреса
    var searchAddressMinimumStringLength: Int { get }
    
    /// Найти адреса
    ///
    /// - Parameters:
    ///   - addressString: текст для поиска адреса
    ///   - completion: обработчик получения результата
    /// - Returns: объект запроса адресов
    func searchAddress(with addressString: String, completion: @escaping AddressAutocompleteMultipleObtainResultHandler) -> HTTPRequest?
    
    /// Сохранить ответ поиска адреса
    ///
    /// - Parameter addressSearchResponse: модель поиска
    func save(_ addressSearchResponse: AddressAutocompleteResponse)
    
    /// Получить адреса из истории
    ///
    /// - Parameter completion: обработчик получения результата
    func fetchAddressesFromHistory(completion: @escaping AddressAutocompleteMultipleObtainResultHandler)
}

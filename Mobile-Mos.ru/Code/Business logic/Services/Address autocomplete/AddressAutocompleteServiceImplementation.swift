//
//  AddressAutocompleteServiceImplementation.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 02.04.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

class AddressAutocompleteServiceImplementation: BaseNetworkService, AddressAutocompleteService {
    
    let maxAddressesFetchCount = 10
    let addressesHistorySize = 10
    
    var dataBaseManager: DataBaseManager!
    
    //MARK: - AddressAutocompleteService
    
    var searchAddressMinimumStringLength = 3
    
    func searchAddress(with addressString: String, completion: @escaping AddressAutocompleteMultipleObtainResultHandler) -> HTTPRequest? {
        
        guard addressString.count >= searchAddressMinimumStringLength else {
            completion(.failure(error: AddressAutocompleteErrors.seachStringTooShort))
            return nil
        }
        
        let urlString = "get_addresses"
        
        var configuration = HTTPRequestConfiguration(withMethod: HTTPMethod.GET.rawValue, andUrlString: urlString)
        configuration.parameters = ["address" : addressString, "limit" : maxAddressesFetchCount]
        
        let urlRequest = requestBuilder.request(with: configuration)
        
        return requestManager.perform(dataRequest: urlRequest)
            .validate(acceptableStatusCodes: [200])
            .validate(acceptableContentTypes: [HTTPRequestContentType.json.rawValue])
            .responseJson(responseHandler: { [weak self] (parsingResult) in
                
                guard let strongSelf = self else { return }
                
                var result: AddressAutocompleteMultipleObtainResult
                
                defer {
                    DispatchQueue.main.async {
                        completion(result)
                    }
                }
                
                switch parsingResult {
                    
                case .success(let json):
                    
                    guard let responseJson = json as? NSDictionary else {
                        result = .failure(error: HTTPRequestError.jsonMappingNotExpectedStructure)
                        return
                    }
                    
                    let response = strongSelf.responseMapper.mapJson(responseJson, toResourceOfType: AddressAutocompleteObtainResponse.self)
                    
                    let noErrorCode = AddressAutocompleteObtainResponse.ErrorCode.noError.rawValue
                    if response.errorCode == noErrorCode {
                        result = .success(addresses: response.addresses)
                    }
                    else {
                        let userInfo: [String : Any] = response.errorMessage != nil ? [NSLocalizedDescriptionKey: response.errorMessage!] : [:]
                        let error = NSError(domain: NSURLErrorDomain, code: response.errorCode, userInfo: userInfo)
                        result = .failure(error: error)
                    }
                    
                case .failure(let error):
                    result = .failure(error: error)
                }
            })
    }
    
    func save(_ addressSearchResponse: AddressAutocompleteResponse) {
        dataBaseManager.saveAsync(models: [addressSearchResponse], resultBlock: nil)
    }
    
    func fetchAddressesFromHistory(completion: @escaping AddressAutocompleteMultipleObtainResultHandler) {
        
        let fetchRequest = DataBaseFetchRequest(with: AddressAutocompleteResponse.self, limitedBy: addressesHistorySize)
        
        dataBaseManager.fetchAsync(with: fetchRequest) { fetchResult in
            
            var result: AddressAutocompleteMultipleObtainResult
            
            defer {
                completion(result)
            }
            
            switch fetchResult {
            case .success(let addresses):
                result = .success(addresses: addresses.reversed())
            case .error(let error):
                result = .failure(error: error)
            }
        }
    }
}

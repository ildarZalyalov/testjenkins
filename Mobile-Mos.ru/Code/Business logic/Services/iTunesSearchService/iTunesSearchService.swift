//
//  iTunesSearchService.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 07.02.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Результат запроса на получение списка приложений
///
/// - success: успех, связанное значение - список приложений
/// - failure: провал, связанное значение - ошибка
enum iTunesSearchAppMultipleResult {
    case success(items: [AppSearchModel])
    case failure(error: Error)
}

/// Результат запроса на получение информации о версии приложения
///
/// - success: успех, связанное значение - информация о версии приложения (если она есть)
/// - failure: провал, связанное значение - ошибка
enum iTunesVersionLookupResult {
    case success(state: iTunesVersionLookupState?)
    case failure(error: Error)
}

/// Обработчик результата получения списка приложений
typealias iTunesSearchAppMultipleResultHandler = (iTunesSearchAppMultipleResult) -> Void

/// Обработчик результата получения информации о версии приложения
typealias iTunesVersionLookupResultHandler = (iTunesVersionLookupResult) -> Void

/// Сервис для поиска по iTunes
protocol iTunesSearchService {
    
    /// Получить приложения правительства Москвы
    ///
    /// - Parameter completion: результат получения приложений
    /// - Returns: запрос
    @discardableResult
    func getMoscowGovermentApps(completion: @escaping iTunesSearchAppMultipleResultHandler) -> HTTPRequest
    
    /// Получить информацию о версии приложения в App Store
    ///
    /// - Parameter completion: обработчик результата получения информации о версии приложения
    func obtainApplicationAppStoreVersion(completion: @escaping iTunesVersionLookupResultHandler)
    
    /// Получить кешированную информацию о версии приложения
    ///
    /// - Parameter completion: обработчик результата получения информации о версии приложения
    func obtainSavedLookupState(completion: @escaping iTunesVersionLookupResultHandler)
    
    /// Закешировать информацию о версии приложения
    ///
    /// - Parameter state: информация о версии приложения
    func saveLookupState(state: iTunesVersionLookupState)
}

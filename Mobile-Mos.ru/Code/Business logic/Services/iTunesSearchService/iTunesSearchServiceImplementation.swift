//
//  iTunesSearchServiceImplementation.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 07.02.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

class iTunesSearchServiceImplementation: BaseNetworkService, iTunesSearchService {
    
    /// Уникальный идентификатор приложений Правительства Москвы
    fileprivate let moscowGovermentAppstoreId = "543871760"
    fileprivate let iPhoneDevicePrefix = "iPhone"
    fileprivate let gosuslugiAppId = 566254786
    
    fileprivate let lookupUrlString = "/lookup"
    
    fileprivate var currentAppVersionRequest: HTTPRequest?
    
    var dataBaseManager: DataBaseManager!
    
    func getMoscowGovermentApps(completion: @escaping iTunesSearchAppMultipleResultHandler) -> HTTPRequest {
        
        var configuration = HTTPRequestConfiguration(withMethod: HTTPMethod.GET.rawValue, andUrlString: lookupUrlString)
        configuration.parameters = ["id": moscowGovermentAppstoreId,
                                    "country": "ru",
                                    "entity": "software"]
        
        var urlRequest = requestBuilder.request(with: configuration)
        
        return requestManager.perform(dataRequest: urlRequest)
            .validate(acceptableStatusCodes: [200])
            .validate(acceptableContentTypes: [HTTPRequestContentType.json.rawValue])
            .responseJson(responseHandler: { [weak self] (parsingResult) in
                
                guard let strongSelf = self else {return}
                
                var result: iTunesSearchAppMultipleResult
                
                defer {
                    DispatchQueue.main.async {
                        completion(result)
                    }
                }
                
                switch parsingResult {
                    
                case .success(let json):
                    
                    guard let response = json as? NSDictionary else {
                        result = .failure(error: HTTPRequestError.jsonMappingNotExpectedStructure)
                        return
                    }
                    
                    guard let items = response["results"] as? [NSDictionary] else {
                        result = .failure(error:HTTPRequestError.jsonMappingNotExpectedStructure)
                        return
                    }
                    
                    var finalItems = items
                    
                    items.enumerated().forEach{
                        
                        // --- находим и удаляем приложение гос.услуги из массива данных 😈
                        if let id = $0.element["trackId"] as? Int, id == strongSelf.gosuslugiAppId {
                            finalItems.remove(at: $0.offset)
                        }
                        else if let supportedDevices = $0.element["supportedDevices"] as? [String] {
                            // --- находим и удаляем приложения, которые не поддерживают iPhone 😈
                            let isContainsIphone = supportedDevices.contains(where: { (string) -> Bool in
                                return string.contains(strongSelf.iPhoneDevicePrefix)
                            })
                            if !isContainsIphone {
                                finalItems.remove(at: $0.offset - 1)
                            }
                        }
                    }
                    
                    let appsItems = strongSelf.responseMapper.mapJsonAsArray(Array(finalItems.dropFirst()), ofResourcesOfType: AppSearchModel.self)
                    
                    result = .success(items: appsItems)
                    
                case .failure(let error):
                    result = .failure(error: error)
                }
            })
    }
    
    func obtainApplicationAppStoreVersion(completion: @escaping iTunesVersionLookupResultHandler) {
        
        currentAppVersionRequest?.cancel()
        
        var configuration = HTTPRequestConfiguration(withMethod: HTTPMethod.GET.rawValue, andUrlString: lookupUrlString)
        configuration.parameters = ["id" : applicationAppleId]
        
        var urlRequest = requestBuilder.request(with: configuration)
        
        currentAppVersionRequest = requestManager.perform(dataRequest: urlRequest)
            .validate(acceptableStatusCodes: [200])
            .validate(acceptableContentTypes: [HTTPRequestContentType.json.rawValue])
            .responseJson(responseHandler: { [weak self] (parsingResult) in
                
                guard let strongSelf = self else {return}
                
                var result: iTunesVersionLookupResult
                
                defer {
                    DispatchQueue.main.async {
                        completion(result)
                    }
                }
                
                switch parsingResult {
                    
                case .success(let json):
                    
                    guard let response = json as? NSDictionary else {
                        result = .failure(error: HTTPRequestError.jsonMappingNotExpectedStructure)
                        return
                    }
                    
                    guard let firstItem = (response["results"] as? [NSDictionary])?.first else {
                        result = .failure(error:HTTPRequestError.jsonMappingNotExpectedStructure)
                        return
                    }
                    
                    var state = strongSelf.responseMapper.mapJson(firstItem, toResourceOfType: iTunesVersionLookupState.self)
                    state.date = Date()
                    
                    result = .success(state: state)
                    
                case .failure(let error):
                    result = .failure(error: error)
                }
            })
    }
    
    func obtainSavedLookupState(completion: @escaping iTunesVersionLookupResultHandler) {
        let fetchRequest = DataBaseFetchRequest(with: iTunesVersionLookupState.self)
        dataBaseManager.fetchAsync(with: fetchRequest) { result in
            switch result {
            case .success(let states):
                completion(.success(state: states.first))
            case .error(let error):
                completion(.failure(error: error))
            }
        }
    }
    
    func saveLookupState(state: iTunesVersionLookupState) {
        dataBaseManager.deleteAllModelsAsync(of: iTunesVersionLookupState.self) { [weak self] result in
            switch result {
            case .success:
                self?.dataBaseManager.saveAsync(models: [state], resultBlock: nil)
            case .error:
                return
            }
        }
    }
}

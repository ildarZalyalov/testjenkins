//
//  CTMosRuService.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 05.07.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Результат запроса на отправку заявки на ремонт в "Чудо техники"
///
/// - success: успех, связанное значение - идентификатор заявки
/// - failure: провал, связанное значение - ошибка
enum CTMosRuSendRepairApplicationResult {
    case success(applicationId: Int)
    case failure(error: Error)
}

/// Обработчик результата отправки заявки на ремонт в "Чудо техники"
typealias CTMosRuSendRepairApplicationResultHandler = (CTMosRuSendRepairApplicationResult) -> Void

/// Заявка на ремонт в "Чудо техники"
struct CTMosRuRepairApplication {
    
    /// Тип устройства
    let deviceType: String
    
    /// Производитель
    let deviceManufacturer: String
    
    /// Модель устройства
    let deviceModel: String
    
    /// Описание проблемы
    let description: String
    
    /// ФИО заявителя
    let userName: String
    
    /// Телефон заявителя
    let phone: String
    
    /// Адрес заявителя
    let address: String
}

/// Сервис работы с сервисом "Чудо техники"
protocol CTMosRuService {
    
    /// Отправить заявку на ремонт в "Чудо техники"
    ///
    /// - Parameters:
    ///   - application: заявка
    ///   - completion: обработчик результата отправки заявки
    /// - Returns: запрос, который отправляется при вызове данного метода
    func sendRepairApplication(application: CTMosRuRepairApplication, completion: @escaping CTMosRuSendRepairApplicationResultHandler) -> HTTPRequest
}

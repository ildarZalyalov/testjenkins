//
//  CTMosRuServiceImplementation.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 05.07.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

class CTMosRuServiceImplementation: BaseNetworkService, CTMosRuService {
    
    let sendApplicationMethod = "481/2o6xu249eplwmima/crm.deal.add/"
    
    //MARK: - Хелперы
    
    /// Создать строку, вырезав из оригинальной строки теги HTML
    ///
    /// - Parameter string: оригинальная строка
    /// - Returns: новая строка
    func stringByRemovingHTMLTags(from string: String) -> String {
        return string.replacingOccurrences(of: "<[^>]+>", with: "", options: .regularExpression, range: nil)
    }
    
    //MARK: - CTMosRuService
    
    func sendRepairApplication(application: CTMosRuRepairApplication, completion: @escaping CTMosRuSendRepairApplicationResultHandler) -> HTTPRequest {
        
        let title = "Заявка из мобильного приложения Mos.ru_iOS \(application.userName) \(application.phone)"
        let applicationComments = "Устройство: \(application.deviceType)<br />Бренд: \(application.deviceManufacturer)<br />Модель: \(application.deviceModel)<br />Проблема: \(application.description)<br />ФИО: \(application.userName)<br />Телефон: \(application.phone)<br />Адрес: \(application.address)"
        
        var configuration = HTTPRequestConfiguration(withMethod: HTTPMethod.POST.rawValue, andUrlString: sendApplicationMethod)
        configuration.parametersEncoding = .httpBody
        configuration.parameters = [
            "fields[TITLE]" : title,
            "fields[TYPE_ID]" : "GOODS",
            "fields[CATEGORY_ID]" : "4",
            "fields[STAGE_ID]" : "C4:NEW",
            "fields[ASSIGNED_BY_ID]" : "1",
            "fields[COMMENTS]" : applicationComments,
            "params[REGISTER_SONET_EVENT]" : "Y"
        ]
        
        let urlRequest = requestBuilder.request(with: configuration)
        
        return requestManager.perform(dataRequest: urlRequest)
            .validate(acceptableStatusCodes: [200])
            .validate(acceptableContentTypes: [HTTPRequestContentType.json.rawValue])
            .responseJson(responseHandler: { [weak self] parsingResult in
                
                guard let strongSelf = self else {return}
                
                var result: CTMosRuSendRepairApplicationResult
                
                defer {
                    DispatchQueue.main.async {
                        completion(result)
                    }
                }
                
                switch parsingResult {
                    
                case .success(let json):
                    
                    guard let response = json as? NSDictionary else {
                        result = .failure(error:HTTPRequestError.jsonMappingNotExpectedStructure)
                        return
                    }
                    
                    let errorDescription = response["error_description"] as? String ?? String()
                    let errorMessage = strongSelf.stringByRemovingHTMLTags(from: errorDescription)
                    
                    if let applicationId = response["result"] as? Int {
                        result = .success(applicationId: applicationId)
                    }
                    else if !errorMessage.isEmpty {
                        let error = NSError(domain: NSURLErrorDomain, code: 0, userInfo: [NSLocalizedDescriptionKey: errorMessage as Any])
                        result = .failure(error: error)
                    }
                    else {
                        let error = NSError(domain: NSURLErrorDomain, code: NSURLErrorUnknown, userInfo: nil)
                        result = .failure(error: error)
                    }
                    
                case .failure(let error):
                    result = .failure(error: error)
                }
            })
    }
}

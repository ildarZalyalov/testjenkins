//
//  ServiceBuilderDebug.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 06.04.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class ServiceBuilderDebug: ServiceBuilderRelease {
    
    /// Базовый url для сервера
    override var baseUrlString: String {
        return baseUrlStringRelease
    }
    
    override func createChatSocketConnection() -> Socket {
        
        let url = URL(string: baseUrlString)!
        
        var configuration = SocketIOConfiguration(with: url)
        configuration.namespace = chatServerNamespace
        configuration.serverPath = chatServerPath
        configuration.connectionParametersObtainer = createChatSocketConnectionParametersObtainer()
        configuration.enableLogging = true
        
        let socket = SocketIOImplementation(with: configuration, and: NotificationCenter.default)
        
        socket.connect()
        
        return socket
    }
    
    override func createDatabaseManager() -> DataBaseManager {
        
        let manager = RealmDataBaseManager()
        manager.shouldLogAsyncOperationsErrors = true
        
        return manager
    }
    
    override func createProtectedDatabaseManager() -> DataBaseManager {
        
        let manager = RealmDataBaseManager(shouldUseEncryptedStorage: true)
        manager.shouldLogAsyncOperationsErrors = true
        
        return manager
    }
    
    override func getUserDataOptionsService() -> UserDataOptionsService {
        
        let userDataService = UserDataOptionsServiceImplementation()
        configureBaseNetworkService(with: userDataService, baseUrl: baseUrlString)
        
        return userDataService
    }
    
    override func getAddressAutocompleteService() -> AddressAutocompleteService {
        
        let addressAutocompleteService = AddressAutocompleteServiceImplementation()
        addressAutocompleteService.dataBaseManager = createDatabaseManager()
        
        configureBaseNetworkService(with: addressAutocompleteService, baseUrl: baseUrlString)
        
        return addressAutocompleteService
    }
    
    override func getOverlapsService() -> OverlapsService {
        
        let overlapsService = OverlapsServiceImplementation()
        configureBaseNetworkService(with: overlapsService, baseUrl: baseUrlString)
        
        return overlapsService
    }
    
    override func getWeatherService() -> WeatherService {
        
        let weatherService = WeatherServiceImplementation()
        configureBaseNetworkService(with: weatherService, baseUrl: baseUrlString)
        
        return weatherService
    }
    
    override func getPaymentService() -> PaymentService {
        
        let paymentService = PaymentServiceImplementation()
        
        paymentService.paymentManager = SDKDIT()
        paymentService.paymentManager.develEnvironment = false
        paymentService.paymentManager.debugMode = true
        
        paymentService.dataBaseManager = createProtectedDatabaseManager()
        
        configureBaseNetworkService(with: paymentService, baseUrl: baseUrlString)
        paymentService.urlSessionConfiguration.timeoutIntervalForRequest = networkingPaymentTimeoutIntervalForRequest
        paymentService.urlSessionConfiguration.timeoutIntervalForResource = networkingPaymentTimeoutIntervalForResource
        
        return paymentService
    }
}

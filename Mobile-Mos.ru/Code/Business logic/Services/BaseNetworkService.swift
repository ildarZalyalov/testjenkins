//
//  BaseNetworkService.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 25.02.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Базовый класс для сервиса, работающего с сетью
class BaseNetworkService: HTTPSessionDelegate {
    
    /// Строитель запросов
    var requestBuilder: HTTPRequestBuilder!
    
    /// Менеджер запросов
    var requestManager: HTTPRequestManager!
    
    /// Маппер JSON в объекты
    var responseMapper: JsonResourceMapper!
    
    /// Конфигурация работы с сетью
    var urlSessionConfiguration: URLSessionConfiguration!
}

//
//  ChatMessageService.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 26.06.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Блок обработки сообщения, единственный параметр - сообщение
typealias ChatMessageHandler = (ChatMessage) -> Void

/// Блок обработки запроса на отмену отправки всех сообщений
typealias ChatMessageMarkAllSendingAsFailedHandler = () -> Void

/// Блок обработки результата отправки сообщения, параметры - исходное сообщение, у которого установлен актуальный статус отправки, и флаг, который говорит были ли удалены все неотправленные сообщения
typealias ChatMessageSendingHandler = (ChatMessage, Bool) -> Void

/// Сервис для работы с сообщениями в диалоге
protocol ChatMessageService {
    
    /// Таймаут отправки сообщения (по его истечении без подтверждения отправки сообщение считается неотправленным)
    var sendMessageTimeout: Double { get }
    
    /// Блок обработки отправки сообщений, вызывается для каждого отправленного сообщения
    var sentMessageHandler: ChatMessageHandler? { get set }
    
    /// Блок обработки новых сообщений, вызывается для каждого полученного нового сообщения
    var newMessageHandler: ChatMessageHandler? { get set }
    
    /// Блок обработки редактирования сообщений, вызывается при запросе сервера на редактирование сообщения в истории
    var editMessageHandler: ChatMessageHandler? { get set }
    
    /// Блок обработки события отмены отправки всех отправляющихся сообщений
    var failSendingMessagesHandler: ChatMessageMarkAllSendingAsFailedHandler? { get set }
    
    /// Создать исходящее сообщение из текста
    ///
    /// - Parameters:
    ///   - text: текст
    ///   - messageContext: контекст сообщения (данные для переключения диалога из одного состояния в другое)
    ///   - conversation: диалог, в котором находится пользователь
    ///   - user: текущий пользователь
    /// - Returns: исходящее сообщение
    func createOutgoingMessage(from text: String, messageContext: String, for conversation: Conversation, and user: User) -> ChatMessage
    
    /// Отправить сообщение
    ///
    /// - Parameters:
    ///   - message: сообщение
    ///   - confirmationBlock: блок обработки результата отправки сообщения
    func send(message: ChatMessage, confirmationBlock: @escaping ChatMessageSendingHandler)
}

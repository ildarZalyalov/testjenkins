//
//  ChatMessageServiceImplementation.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 26.06.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class ChatMessageServiceImplementation: ChatMessageService {
    
    /// Типы сообщений в сокете
    ///
    /// - sendMessage: отправить сообщение
    /// - sendMessageResponse: получить сообщение
    /// - editMessage: отредактировать сообщение
    fileprivate enum Event: String {
        case sendMessage
        case sendMessageResponse
        case editMessage
    }
    
    //MARK: - Статические компоненты
    
    // Обработчики, заданные один раз в каждом экземпляре сервиса
    
    fileprivate static var sentMessageHandlers = ThreadSafeDictionary<UUID, ChatMessageHandler>()
    fileprivate static var newMessageHandlers = ThreadSafeDictionary<UUID, ChatMessageHandler>()
    fileprivate static var editMessageHandlers = ThreadSafeDictionary<UUID, ChatMessageHandler>()
    fileprivate static var failSendingMessagesHandlers = ThreadSafeDictionary<UUID, ChatMessageMarkAllSendingAsFailedHandler>()
    
    static var sharedMessageFactory: ChatMessageFactory!
    static var sharedDataBaseManager: DataBaseManager!
    static var currentUserManager: CurrentUserManager!
    
    static var sharedSocket: Socket! {
        
        didSet {
            
            // Получение сообщения
            
            sharedSocket.onAsync(event: Event.sendMessageResponse.rawValue) { socketData, _ in
                
                guard let messageJson = messageJson(from: socketData) else {
                    return
                }
                
                // при получении сообщения, меняем дату на текущую дату на устройстве
                // (бизнес-правило - у сообщений должны быть даты отправки/получения устройством, исключение - получение сообщений из истории)
                var message = sharedMessageFactory.buildMessage(from: messageJson, for: currentUserManager.currentUser)
                message.date = Date()
                
                try? sharedDataBaseManager.save(models: [message])
                
                let handlers = newMessageHandlers.currentValues()
                DispatchQueue.main.async(execute: {
                    handlers.forEach { $0(message) }
                })
            }
            
            // Редактирование сообщения
            
            sharedSocket.onAsync(event: Event.editMessage.rawValue) { socketData, _ in
                
                guard let messageJson = messageJson(from: socketData) else {
                    return
                }
                
                var message = sharedMessageFactory.buildMessage(from: messageJson, for: currentUserManager.currentUser)
                
                let predicate = predicateForCachedVersion(of: message)
                let request = DataBaseFetchRequest(with: ChatMessage.self, filteredWith: predicate)
                
                if let oldMessage = (try? sharedDataBaseManager.fetch(with: request))?.first {
                    message.uid = oldMessage.uid
                }
                try? sharedDataBaseManager.save(models: [message])
                
                let handlers = editMessageHandlers.currentValues()
                DispatchQueue.main.async(execute: {
                    handlers.forEach { $0(message) }
                })
            }
            
            // Потеря соединения
            
            sharedSocket.onAsync(event: SocketEvent.disconnect.rawValue) { _,_  in
                
                markAllSendingMessagesAsFailedAsync()
                
                let handlers = failSendingMessagesHandlers.currentValues()
                DispatchQueue.main.async(execute: {
                    handlers.forEach { $0() }
                })
            }
        }
    }
    
    //MARK: - Локальные компоненты
    
    fileprivate typealias Service = ChatMessageServiceImplementation
    
    fileprivate let sentMessageHandlerId = UUID()
    fileprivate let newMessageHandlerId = UUID()
    fileprivate let editMessageHandlerId = UUID()
    fileprivate let failSendingMessagesHandlerId = UUID()
    
    let sendMessageTimeout: Double = 10
    
    deinit {
        Service.sentMessageHandlers[sentMessageHandlerId] = nil
        Service.newMessageHandlers[newMessageHandlerId] = nil
        Service.editMessageHandlers[editMessageHandlerId] = nil
        Service.failSendingMessagesHandlers[failSendingMessagesHandlerId] = nil
    }
    
    //MARK: - Приватные методы
    
    /// Попытаться получить json сообщения из ответа сервера
    ///
    /// - Parameter socketData: данные из сокета
    /// - Returns: json сообщения или nil
    fileprivate static func messageJson(from socketData: [Any]) -> NSDictionary? {
        
        guard let json = (socketData as? [NSDictionary])?.first else {
            return nil
        }
        
        return json["message"] as? NSDictionary
    }
    
    /// Пометить все отправляющиеся сообщения в закешированной истории сообщений как отправленные с ошибкой
    fileprivate static func markAllSendingMessagesAsFailedAsync() {
        
        let keyPath = #keyPath(ChatMessageModel.statusRawValue)
        let configuration = NSPredicateConfiguration(with: "\(keyPath) = %d", and: [ChatMessage.Status.sending.rawValue])
        let predicate = NSPredicate(with: configuration)
        
        sharedDataBaseManager.updateAllModelsAsync(of: ChatMessage.self, where: predicate, with: ChatMessage.Status.failed.rawValue, for: keyPath, resultBlock: nil)
    }
    
    //MARK: - ChatMessageService
    
    var sentMessageHandler: ChatMessageHandler? {
        get { return Service.sentMessageHandlers[sentMessageHandlerId] }
        set { Service.sentMessageHandlers[sentMessageHandlerId] = newValue }
    }
    
    var newMessageHandler: ChatMessageHandler? {
        get { return Service.newMessageHandlers[newMessageHandlerId] }
        set { Service.newMessageHandlers[newMessageHandlerId] = newValue }
    }
    
    var editMessageHandler: ChatMessageHandler? {
        get { return Service.editMessageHandlers[editMessageHandlerId] }
        set { Service.editMessageHandlers[editMessageHandlerId] = newValue }
    }
    
    var failSendingMessagesHandler: ChatMessageMarkAllSendingAsFailedHandler? {
        get { return Service.failSendingMessagesHandlers[failSendingMessagesHandlerId] }
        set { Service.failSendingMessagesHandlers[failSendingMessagesHandlerId] = newValue }
    }
    
    func createOutgoingMessage(from text: String, messageContext: String, for conversation: Conversation, and user: User) -> ChatMessage {
        return Service.sharedMessageFactory.buildOutgoingMessage(from: text, messageContext: messageContext, for: conversation, and: user)
    }
    
    func send(message: ChatMessage, confirmationBlock: @escaping ChatMessageSendingHandler) {
        
        Service.sharedDataBaseManager.saveAsync(models: [message], resultBlock: nil)
        
        let event = Event.sendMessage.rawValue
        let messageJson = message.serializeToJson()
        
        Service.sharedSocket.send(event: event, with: [messageJson], timingOutAsyncAfter: sendMessageTimeout) { timedOut, parameters in
            
            var sentMessage = message
            sentMessage.status = timedOut ? .failed : .success
            
            if let messageId = parameters.first as? Int64 {
                sentMessage.messageId = messageId
            }
            
            let shouldDeleteUnsent = sentMessage.status == .success
            
            try? Service.sharedDataBaseManager.save(models: [sentMessage])
            if shouldDeleteUnsent {
                let predicate = Service.predicateForAllUnsent(afterSuccessfullySent: sentMessage)
                Service.sharedDataBaseManager.deleteAllModelsAsync(of: ChatMessage.self, where: predicate, resultBlock: nil)
            }
            
            let handlers = Service.sentMessageHandlers.currentValues()

            DispatchQueue.main.async(execute: {
                handlers.forEach { $0(sentMessage) }
                confirmationBlock(sentMessage, shouldDeleteUnsent)
            })
        }
    }
}

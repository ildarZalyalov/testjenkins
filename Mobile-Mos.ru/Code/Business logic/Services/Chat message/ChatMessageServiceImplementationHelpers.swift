//
//  ChatMessageServiceImplementationHelpers.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 26.06.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

extension ChatMessageServiceImplementation {
    
    /// Получение предиката для фильтрации выборки из БД неотправленных сообщений в чате (отправляющихся и отправленных с ошибкой)
    ///
    /// - Parameter message: успешно отправленное сообщение в чате
    /// - Returns: предикат для фильтрации
    static func predicateForAllUnsent(afterSuccessfullySent message: ChatMessage) -> NSPredicate {
        
        var configuration = NSPredicateConfiguration(with: "\(#keyPath(ChatMessageModel.chatId)) = %@", and: [message.chatId])
        configuration.and(condition: ("\(#keyPath(ChatMessageModel.isIncoming)) = false", []))
        configuration.and(keyPath: #keyPath(ChatMessageModel.statusRawValue), in: [ChatMessage.Status.sending.rawValue,
                                                                                   ChatMessage.Status.failed.rawValue])
        
        if let userId = message.userId {
            configuration.and(condition: ("\(#keyPath(ChatMessageModel.userId)) = %@", [userId]))
        }
        else {
            configuration.and(condition: ("\(#keyPath(ChatMessageModel.sessionId)) = %@", [message.sessionId]))
        }
        
        return NSPredicate(with: configuration)
    }
    
    /// Получение предиката для фильтрации выборки из БД определенного сообщения
    ///
    /// - Parameter message: сообщение, кешированную версию которого надо найти
    /// - Returns: предикат для фильтрации
    static func predicateForCachedVersion(of message: ChatMessage) -> NSPredicate {
        
        var configuration = NSPredicateConfiguration(with: "\(#keyPath(ChatMessageModel.chatId)) = %@", and: [message.chatId])
        configuration.and(condition: ("\(#keyPath(ChatMessageModel.messageId)) = %lld", [message.messageId]))
        
        if let userId = message.userId {
            configuration.and(condition: ("\(#keyPath(ChatMessageModel.userId)) = %@", [userId]))
        }
        else {
            configuration.and(condition: ("\(#keyPath(ChatMessageModel.sessionId)) = %@", [message.sessionId]))
        }
        
        return NSPredicate(with: configuration)
    }
}

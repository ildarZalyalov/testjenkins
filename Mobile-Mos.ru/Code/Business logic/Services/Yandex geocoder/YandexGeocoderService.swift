//
//  YandexGeocoderService.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 18.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Результат выполнения запроса на геокодинг
///
/// - success: успех
/// - failure: провал
enum YandexGeocodeRequestResult {
    case success(response: YandexGeocoderResponse)
    case failure(error: Error)
}

/// Наши ошибки для обработки поиска
///
/// - incorrectObjectsType: тип объектов не правильный
/// - seachStringTooShort: слишком короткая строка поиска
enum YandexAddressSearchErrors: Error {
    case incorrectObjectsType
    case seachStringTooShort
}

/// Результат выполнения запроса на поиск адресов
///
/// - success: успех, связанное значение - массив адресов
/// - failure: провал, связанное значение - ошибка
enum YandexAddressSearchRequestResult {
    case success(response: (objects: [YandexAddressSearchResponse], reversedNames: [String : String]))
    case failure(error: Error)
}

/// Обработчик получения результата запроса на геокодинг
typealias YandexGeocodeResultHandler = (YandexGeocodeRequestResult) -> ()

/// Обработчик получения результата запроса на поиск адреса
typealias YandexAddressSearchResultHandler = (YandexAddressSearchRequestResult) -> ()

/// Сервис геокодинга через API Yandex
protocol YandexGeocoderService {
    
    /// Минимальная длина строки поиска адреса
    var searchAdressMinimumStringLength: Int { get }
    
    /// Геокодировать координаты
    ///
    /// - Parameters:
    ///   - location: координаты
    ///   - completion: обработчик получения результата
    /// - Returns: запрос на геокодинг
    func geocodeCoordinate(_ location: Location, completion: @escaping YandexGeocodeResultHandler) -> HTTPRequest
    
    /// Геокодировать адрес
    ///
    /// - Parameters:
    ///   - addressString: адрес
    ///   - completion: обработчик получения результата
    /// - Returns: запрос на геокодинг
    func geocodeAddress(_ addressString: String, completion: @escaping YandexGeocodeResultHandler) -> HTTPRequest
    
    /// Найти адреса
    ///
    /// - Parameters:
    ///   - addressString: текст для поиска адреса
    ///   - completiong: обработчик получения результата
    func searchAddress(with addressString: String, completion: @escaping YandexAddressSearchResultHandler)
    
    /// Отменить текущий запрос на поиск адреса
    func cancelCurrentSearchAddress()
}

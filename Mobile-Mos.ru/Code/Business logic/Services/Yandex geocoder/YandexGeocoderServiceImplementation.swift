//
//  YandexGeocoderServiceImplementation.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 19.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

fileprivate extension YMKGeoObject {
    
    /// Не nil описание
    var descriptionString: String {
        get { return self.descriptionText ?? "" }
    }
    
    /// Не nil имя
    var nameString: String {
        get { return self.name ?? "" }
    }
}

class YandexGeocoderServiceImplementation: BaseNetworkService, YandexGeocoderService {
    
    var russiaCountryPrefix = "Россия, "
    let separatorCharacter = ","
    let stringShouldContainResult = "Мос"
    let replacingString = "Москва, Россия"
    let replacingCountry = "Россия"
    let maxResponseCount: NSNumber = 20
    let maxAddressesFetchCount = 20
    
    var yandexSearchManager: YMKSearchManager!
    var session: YMKSearchSession?
    
    fileprivate func removingRussiaCountryPrefix(from text: String) -> String {
        guard text.hasPrefix(russiaCountryPrefix) else { return text }
        return String(text[russiaCountryPrefix.endIndex...])
    }
    
    //MARK: - YandexGeocoderService
    
    var searchAdressMinimumStringLength = 4
    
    func geocodeCoordinate(_ location: Location, completion: @escaping YandexGeocodeResultHandler) -> HTTPRequest {
        
        var configuration = HTTPRequestConfiguration(withMethod: HTTPMethod.GET.rawValue, andUrlString: String())
        configuration.parameters = ["geocode" : "\(location.longitude),\(location.latitude)", "format" : "json"]
        
        let urlRequest = requestBuilder.request(with: configuration)
        
        return requestManager.perform(dataRequest: urlRequest)
            .validate(acceptableStatusCodes: [200])
            .validate(acceptableContentTypes: [HTTPRequestContentType.json.rawValue])
            .responseJson(responseHandler: { [weak self] (parsingResult) in
                
                guard let strongSelf = self else { return }
                
                var result: YandexGeocodeRequestResult
                
                defer {
                    DispatchQueue.main.async {
                        completion(result)
                    }
                }
                
                switch parsingResult {
                    
                case .success(let json):
                    
                    guard let responseJson = json as? NSDictionary else {
                        result = .failure(error: HTTPRequestError.jsonMappingNotExpectedStructure)
                        return
                    }
 
                    var response = strongSelf.responseMapper.mapJson(responseJson, toResourceOfType: YandexGeocoderResponse.self)
                    response.text = strongSelf.removingRussiaCountryPrefix(from: response.text)
                    
                    result = .success(response: response)
                    
                case .failure(let error):
                    result = .failure(error: error)
                }
            })
    }
    
    func geocodeAddress(_ addressString: String, completion: @escaping YandexGeocodeResultHandler) -> HTTPRequest {
        
        var configuration = HTTPRequestConfiguration(withMethod: HTTPMethod.GET.rawValue, andUrlString: String())
        configuration.parameters = ["geocode" : addressString, "format" : "json"]
        
        let urlRequest = requestBuilder.request(with: configuration)
        
        return requestManager.perform(dataRequest: urlRequest)
            .validate(acceptableStatusCodes: [200])
            .validate(acceptableContentTypes: [HTTPRequestContentType.json.rawValue])
            .responseJson(responseHandler: { [weak self] (parsingResult) in
                
                guard let strongSelf = self else { return }
                
                var result: YandexGeocodeRequestResult
                
                defer {
                    DispatchQueue.main.async {
                        completion(result)
                    }
                }
                
                switch parsingResult {
                    
                case .success(let json):
                    
                    guard let responseJson = json as? NSDictionary else {
                        result = .failure(error: HTTPRequestError.jsonMappingNotExpectedStructure)
                        return
                    }
                    
                    var response = strongSelf.responseMapper.mapJson(responseJson, toResourceOfType: YandexGeocoderResponse.self)
                    response.text = strongSelf.removingRussiaCountryPrefix(from: response.text)
                    
                    result = .success(response: response)
                    
                case .failure(let error):
                    result = .failure(error: error)
                }
            })
    }
    
    func searchAddress(with addressString: String, completion: @escaping YandexAddressSearchResultHandler) {
        
        guard addressString.count >= searchAdressMinimumStringLength else {
            completion(.failure(error: YandexAddressSearchErrors.seachStringTooShort))
            return
        }
        
        session?.cancel()
        
        session = yandexSearchManager.submit(withText: addressString,
                                             geometry: YMKGeometry.init(boundingBox: moscowBoundingBox),
                                             searchOptions: YMKSearchOptions(searchTypes: .geo,
                                                                             resultPageSize: maxResponseCount,
                                                                             snippets: .init(rawValue: 0),
                                                                             userPosition: moscowLocation.toYandexPoint(),
                                                                             origin: nil,
                                                                             directPageId: nil,
                                                                             directStatId: nil,
                                                                             appleCtx: nil,
                                                                             searchClosed: true,
                                                                             geometry: false,
                                                                             maxAdverts: nil,
                                                                             advertPageId: nil,
                                                                             suggestWords: true),
                                             responseHandler: { [weak self] (response, error) in
            
            guard let strongSelf = self else { return }
                                    
            var result: YandexAddressSearchRequestResult
            
            defer {
                DispatchQueue.main.async {
                    completion(result)
                }
            }
            
            guard error == nil else { result = .failure(error: error!); return }
            
            guard let objects = response?.collection.children else {
                
                result = .failure(error: YandexAddressSearchErrors.incorrectObjectsType)
                return
            }
            
            let moscowObjects = objects.filter({ $0.obj.descriptionString.contains(strongSelf.stringShouldContainResult) })
            
            var models = [YandexAddressSearchResponse]()
            var reversedNames = [String:String]()
            
            for object in moscowObjects {
                
                let name = object.obj.nameString + strongSelf.separatorCharacter
                let description = object.obj.descriptionText
                
                if var desc = description?.replacingOccurrences(of:strongSelf.replacingString , with: "") {
                    
                    desc = desc.replacingOccurrences(of: strongSelf.replacingCountry, with: "")
                    
                    var fullName = name + " " + desc
                    fullName = fullName.trimmingCharacters(in: .whitespacesAndNewlines)
                    fullName = String(fullName.dropLast())
                    
                    let geometry = object.obj.geometry
                    
                    if let objectPoint = geometry.first?.point {
                        
                        let model = YandexAddressSearchResponse(address: fullName, location: Location(latitude: objectPoint.latitude, longitude: objectPoint.longitude))
                        models.append(model)
                        reversedNames[model.address] = desc.firstLetterCapitalized() + " " + name.trimmingCharacters(in: .whitespacesAndNewlines).dropLast()
                    }
                }
            }
                                                
            result = .success(response: (models, reversedNames))
        })
    }
    
    func cancelCurrentSearchAddress() {
        session?.cancel()
    }
}

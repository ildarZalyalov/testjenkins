//
//  ConversationsServiceImplementationHelpers.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 29.08.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

extension ConversationsServiceImplementation {
    
    /// Получение предиката для фильтрации выборки из БД диалога с указанным идентификатором
    ///
    /// - Parameter conversationId: идентификатор диалога
    /// - Returns: предикат для фильтрации
    func predicateForConversation(with conversationId: String) -> NSPredicate {
        let configuration = NSPredicateConfiguration(with: "\(#keyPath(ConversationModel.itemId)) = %@", and: [conversationId])
        return NSPredicate(with: configuration)
    }
}

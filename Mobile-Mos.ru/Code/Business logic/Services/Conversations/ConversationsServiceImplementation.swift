//
//  ConversationsServiceImplementation.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 10.02.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class ConversationsServiceImplementation: ConversationsService {
   
    /// Типы сообщений в сокете
    ///
    /// - getChatList: запросить список диалогов
    /// - getChatListResponse: получить список диалогов
    /// - sendMessageResponse: получить сообщение
    fileprivate enum Event: String {
        case getChatList
        case getChatListResponse
        case sendMessageResponse
    }
    
    //MARK: - Статические компоненты
    
    // Обработчики, заданные один раз в каждом экземпляре сервиса
    
    fileprivate static var cacheObsoleteHandlers = ThreadSafeDictionary<UUID, ConversationCacheObsoleteHandler>()
    
    // Обработчики, добавляемые экземплярами сервиса по вызову методов в неограниченном кол-ве
    
    fileprivate static var chatListRequestsQueue = ThreadSafeSerialEventQueue<ConversationObtainMultipleResult>()
    
    fileprivate static var activeConversationsIds = Set<String>()
    
    static var sharedMessageFactory: ChatMessageFactory!
    static var sharedJsonMapper: JsonResourceMapper!
    static var sharedDataBaseManager: DataBaseManager!
    static var currentUserManager: CurrentUserManager!
    
    static var sharedSocket: Socket! {
        
        didSet {
            
            // Получение списка диалогов
            
            sharedSocket.onAsync(event: Event.getChatListResponse.rawValue) { socketData, _ in
                
                guard let handler = chatListRequestsQueue.dequeue()?.handler else { return }
                
                var result: ConversationObtainMultipleResult
                
                defer {
                    DispatchQueue.main.async(execute: {
                        handler(result)
                    })
                }
                
                guard let json = (socketData as? [NSDictionary])?.first else {
                    result = .failure(error: HTTPRequestError.jsonParsingNoResponseDataError)
                    return
                }
                
                guard let chatListJson = json["chat_list"] as? [NSDictionary] else {
                    result = .failure(error: HTTPRequestError.jsonMappingNotExpectedStructure)
                    return
                }
                
                var chatList = sharedJsonMapper.mapJsonAsArray(chatListJson, ofResourcesOfType: Conversation.self)
                chatList.assignDefaultValues()
                save(conversations: &chatList)
                
                for chatJson in chatListJson {
                    
                    guard let messageJson = chatJson["last_msg", "message"] as? NSDictionary else { continue }
                    
                    let lastMessage = sharedMessageFactory.buildMessage(from: messageJson, for: currentUserManager.currentUser)
                    save(lastMessage: lastMessage)
                }
                
                result = .success(conversations: chatList)
            }
            
            // Получение сообщения (для подсчета кол-ва непрочитанных)
            
            sharedSocket.onAsync(event: Event.sendMessageResponse.rawValue) { socketData, _ in
                
                guard let messageJson = messageJson(from: socketData) else {
                    return
                }
                
                guard let chatId = ChatMessage.chatIdFromJson(messageJson) else {
                    return
                }
                
                guard !activeConversationsIds.contains(chatId) else {
                    return
                }
                
                let fetchRequest = DataBaseFetchRequest(with: Conversation.self,
                                                        filteredWith: "\(#keyPath(ConversationModel.itemId)) = %@",
                                                        arguments: chatId)
                
                guard var conversation = (try? sharedDataBaseManager.fetch(with: fetchRequest))?.first else {
                    return
                }
                
                conversation.unreadMessagesCount += 1
                
                try? sharedDataBaseManager.save(models: [conversation])
            }
            
            // Восстановление соединения (перезапуск очередей + кэш мог устареть)
            
            sharedSocket.onAsync(event: SocketEvent.connect.rawValue) { _,_  in
                
                chatListRequestsQueue.resume()
                
                sharedDataBaseManager.updateAllModelsAsync(of: Conversation.self,
                                                           with: true,
                                                           for: #keyPath(ConversationModel.shouldUpdateHistory),
                                                           resultBlock: nil)
                
                let handlers = cacheObsoleteHandlers.currentValues()
                DispatchQueue.main.async(execute: {
                    handlers.forEach { $0() }
                })
            }
            
            // Обработка обрыва соединения (остановка очередей)
            
            sharedSocket.onAsync(event: SocketEvent.disconnect.rawValue) { _,_  in
                chatListRequestsQueue.pause()
            }
        }
    }
    
    //MARK: - Локальные компоненты
    
    fileprivate typealias Service = ConversationsServiceImplementation
    
    fileprivate let cacheObsoleteHandlerId = UUID()
    
    deinit {
        Service.cacheObsoleteHandlers[cacheObsoleteHandlerId] = nil
    }

    //MARK: - Приватные методы
    
    /// Попытаться получить json сообщения из ответа сервера
    ///
    /// - Parameter socketData: данные из сокета
    /// - Returns: json сообщения или nil
    fileprivate static func messageJson(from socketData: [Any]) -> NSDictionary? {
        
        guard let json = (socketData as? [NSDictionary])?.first else {
            return nil
        }
        
        return json["message"] as? NSDictionary
    }
    
    /// Сохранить список диалогов в кэш
    ///
    /// - warning: Метод использует синхронное сохранение в БД и будет блокировать вызывающий поток до завершения работы!
    /// - Parameter conversations: список диалогов
    fileprivate static func save(conversations: inout [Conversation]) {
        
        guard !conversations.isEmpty else { return }
        
        do {
            
            let request = DataBaseFetchRequest(with: Conversation.self)
            var cachedConversationsById = try sharedDataBaseManager.fetch(with: request).toDictionary { $0.itemId }
            
            conversations.forEachMutate {
                guard let cachedConversation = cachedConversationsById.removeValue(forKey: $0.itemId) else { return }
                $0.updatePersistentFields(from: cachedConversation)
            }
            
            if !cachedConversationsById.isEmpty {
                try sharedDataBaseManager.delete(models: Array(cachedConversationsById.values))
            }
            try sharedDataBaseManager.save(models: conversations)
        }
        catch {
            return
        }
    }
    
    /// Сохранить последнее сообщение в диалоге в кэш
    ///
    /// - Parameter lastMessage: последнее сообщение в диалоге
    fileprivate static func save(lastMessage: ChatMessage) {
        
        guard lastMessage.messageId < Int64.max else { return }
        
        let configuration = NSPredicateConfiguration(with: "\(#keyPath(ChatMessageModel.messageId)) = %@", and: [lastMessage.messageId])
        let sortDescriptors = [DataBaseSortDescriptor(sortKeyPath: #keyPath(ChatMessageModel.date))]
        var fetchRequest = DataBaseFetchRequest(with: ChatMessage.self, filteredWith: NSPredicate(with: configuration))
        fetchRequest.sortDescriptors = sortDescriptors
        fetchRequest.limit = 1
        
        if let cachedMessage = (try? sharedDataBaseManager.fetch(with: fetchRequest))?.first {
            
            guard cachedMessage.canBeUpdatedInHistory else { return }
            
            var messageToSave = lastMessage
            messageToSave.uid = cachedMessage.uid
            try? sharedDataBaseManager.save(models: [messageToSave])
        }
        else {
            try? sharedDataBaseManager.save(models: [lastMessage])
        }
    }
    
    //MARK: - ConversationsService
    
    var cacheObsoleteHandler: ConversationCacheObsoleteHandler? {
        get { return Service.cacheObsoleteHandlers[cacheObsoleteHandlerId] }
        set { Service.cacheObsoleteHandlers[cacheObsoleteHandlerId] = newValue }
    }
    
    var activeConversationsIds: Set<String> {
        return Service.activeConversationsIds
    }
    
    func activate(conversation: inout Conversation) {
        
        Service.activeConversationsIds.insert(conversation.itemId)
        
        guard conversation.unreadMessagesCount > 0 || conversation.unseenMessagesCount > 0 else {
            return
        }
        
        conversation.unreadMessagesCount = 0
        conversation.unseenMessagesCount = 0
        
        Service.sharedDataBaseManager.saveAsync(models: [conversation], resultBlock: nil)
    }
    
    func deactivate(conversation: inout Conversation) {
        Service.activeConversationsIds.remove(conversation.itemId)
    }
    
    func obtainConversations(for user: User, completion: @escaping ConversationObtainMultipleHandler) {
            
        guard Service.sharedSocket.isConnected else {
            
            let error = NSError(domain: NSURLErrorDomain, code: NSURLErrorCannotConnectToHost, userInfo: nil)
            let result = ConversationObtainMultipleResult.failure(error: error)
            completion(result)
            
            return
        }
    
        let userIdValue: Any = user.userId ?? NSNull()
        let request = [
            "user_id": userIdValue,
            "session_id": user.sessionId
        ]
        
        let emitter = {
            Service.sharedSocket.send(event: Event.getChatList.rawValue, with: [request])
        }
        
        let handler: ConversationObtainMultipleHandler = { result in
            completion(result)
        }
        
        let eventBlock = SerialEventBlock(emitter: emitter, handler: handler)
        Service.chatListRequestsQueue.enqueue(element: eventBlock)
    }
    
    func obtainCachedConversation(with conversationId: String, completion: @escaping ConversationObtainHandler) {
        
        let predicate = predicateForConversation(with: conversationId)
        let fetchRequest = DataBaseFetchRequest(with: Conversation.self, filteredWith: predicate)
        
        Service.sharedDataBaseManager.fetchAsync(with: fetchRequest) { result in
            
            var obtainResult: ConversationObtainResult
            
            switch result {
            case .success(let conversations):
                obtainResult = .success(conversation: conversations.first)
            case .error(let error):
                obtainResult = .failure(error: error)
            }
            
            completion(obtainResult)
        }
    }
    
    func obtainCachedConversations(for user: User, completion: @escaping ConversationObtainMultipleHandler) {
        
        let request = DataBaseFetchRequest(with: Conversation.self)
        
        Service.sharedDataBaseManager.fetchAsync(with: request) { result in
            
            var obtainResult: ConversationObtainMultipleResult
            
            switch result {
            case .success(let conversations):
                obtainResult = .success(conversations: conversations)
            case .error(let error):
                obtainResult = .failure(error: error)
            }
            
            completion(obtainResult)
        }
    }
    
    func obtainCachedConversationsController(completion: @escaping DataBaseControllerFetchResultBlock<Conversation>) {
        let fetchRequest = DataBaseFetchRequest(with: Conversation.self)
        Service.sharedDataBaseManager.fetchControllerAsync(with: fetchRequest, resultBlock: completion)
    }
    
    func markMessageHistoryRefreshed(for conversation: Conversation) {
        
        var conversationToSave = conversation
        conversationToSave.shouldUpdateHistory = false
        
        Service.sharedDataBaseManager.saveAsync(models: [conversationToSave], resultBlock: nil)
    }
    
    func clearCachedConversations() {
        Service.sharedDataBaseManager.deleteAllModelsAsync(of: Conversation.self, resultBlock: nil)
    }
}

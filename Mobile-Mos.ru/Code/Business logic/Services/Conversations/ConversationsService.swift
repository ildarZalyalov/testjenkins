//
//  ConversationsService.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 10.02.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Результат запроса на получение диалога
///
/// - success: успех, связанное значение - диалог или nil, если подходящий диалог найти не удалось
/// - failure: провал, связанное значение - ошибка
enum ConversationObtainResult {
    case success(conversation: Conversation?)
    case failure(error: Error)
}

/// Результат запроса на получение списка диалогов
///
/// - success: успех, связанное значение - список диалогов
/// - failure: провал, связанное значение - ошибка
enum ConversationObtainMultipleResult {
    case success(conversations: [Conversation])
    case failure(error: Error)
}

/// Блок обработки события - кэш диалогов устарел (возможно появились новые сообщения в диалогах пока пользователь был оффлайн)
typealias ConversationCacheObsoleteHandler = () -> Void

/// Блок обработки получения диалога, единственный параметр - результат запроса
typealias ConversationObtainHandler = (ConversationObtainResult) -> Void

/// Блок обработки получения диалогов, единственный параметр - результат запроса
typealias ConversationObtainMultipleHandler = (ConversationObtainMultipleResult) -> Void

/// Сервис для работы с диалогами
protocol ConversationsService {
    
    /// Блок обработки события - кэш диалогов устарел
    var cacheObsoleteHandler: ConversationCacheObsoleteHandler? { get set }
    
    /// Активные диалоги - для них не обновляется счетчик непрочитанных и пропущенных сообщений
    var activeConversationsIds: Set<String> { get }
    
    /// Сделать диалог активным
    ///
    /// - Parameter conversation: диалог
    func activate(conversation: inout Conversation)
    
    /// Сделать диалог неактивным
    ///
    /// - Parameter conversation: диалог
    func deactivate(conversation: inout Conversation)
    
    /// Запросить с сервера список текущих диалогов
    ///
    /// - Parameters:
    ///   - user: текущий пользователь
    ///   - completion: блок кода, который вызывается по окончании запроса, единственный параметр - результат запроса
    func obtainConversations(for user: User, completion: @escaping ConversationObtainMultipleHandler)
    
    /// Получить из кэша диалог по идентификатору
    ///
    /// - Parameters:
    ///   - conversationId: идентификатор диалога
    ///   - completion: блок обработки получения диалога
    func obtainCachedConversation(with conversationId: String, completion: @escaping ConversationObtainHandler)
    
    /// Получить из кэша список текущих диалогов
    ///
    /// - Parameters:
    ///   - user: текущий пользователь
    ///   - completion: блок кода, который вызывается по окончании запроса, единственный параметр - результат запроса
    func obtainCachedConversations(for user: User, completion: @escaping ConversationObtainMultipleHandler)
    
    /// Получить контроллер для отслеживания изменения набора кэшированных диалогов
    ///
    /// - Parameter completion: блок обработки получения контроллера
    func obtainCachedConversationsController(completion: @escaping DataBaseControllerFetchResultBlock<Conversation>)
    
    /// Пометить, что в диалоге обновлена история
    ///
    /// - Parameter conversation: диалог
    func markMessageHistoryRefreshed(for conversation: Conversation)
    
    /// Удалить кэш диалогов
    func clearCachedConversations()
}

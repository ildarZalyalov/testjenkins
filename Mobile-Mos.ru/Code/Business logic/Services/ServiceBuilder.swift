//
//  ServiceBuilder.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 13.02.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Строитель сервисов
protocol ServiceBuilder {
    
    var fileDownloadManager: FileDownloadManager { get }
    
    var defaultFileCacheManager: FileCacheManager { get }
    
    func getSocketConnectionObserver() -> SocketConnectionObserver
    
    func getChatMessageFactory() -> ChatMessageFactory
    
    func getUserService() -> UserService
    
    func getUserInfoAndSettingsService() -> UserInfoAndSettingsService
    
    func getUserDataOptionsService() -> UserDataOptionsService
    
    func getBannerService() -> BannerService
    
    func getConversationsService() -> ConversationsService
    
    func getMapItemsService() -> MapItemsService
    
    func getChatMessageService() -> ChatMessageService
    
    func getChatMessageHistoryService() -> ChatMessageHistoryService
    
    func getChatMessageInteractionService() -> ChatMessageInteractionService
    
    func getGlobalSearchService() -> GlobalSearchService

    func getPushTokensService() -> PushTokensService

    func getYandexGeocoderService() -> YandexGeocoderService
    
    func getAddressAutocompleteService() -> AddressAutocompleteService
    
    func getiTunesSearchService() -> iTunesSearchService
    
    func getChatKeywordsService() -> ChatKeywordsService
    
    func getFeedbackService() -> FeedbackService
    
    func getOverlapsService() -> OverlapsService
    
    func getWeatherService() -> WeatherService
    
    func getPaymentService() -> PaymentService
    
    func getCTMosRuService() -> CTMosRuService
}

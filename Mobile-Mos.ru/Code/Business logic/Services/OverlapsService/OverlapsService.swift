//
//  OverlapsService.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 22.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Результат запроса на получение перекрытий
///
/// - success: успех, связанное значение - список перекрытий
/// - failure: провал, связанное значение - ошибка
enum MapOverlapGeoObtainMultipleResult {
    case success(models: [OverlapGeo])
    case failure(error: Error)
}

/// Результат запроса на получение детальной инфы перекрытий
///
/// - success: успех, связанное значение - список перекрытий
/// - failure: провал, связанное значение - ошибка
enum MapOverlapObtainResult {
    case success(model: Overlap)
    case failure(error: Error)
}

protocol OverlapsService {
    
    /// Получение гео объектов перекрытий
    ///
    /// - Parameter completion: блок кода, который вызывается по окончании запроса, единственный параметр - результат запроса
    /// - Returns: запрос, который отправляется при вызове данного метода
    func obtainOverlapsGeo(completion: @escaping (MapOverlapGeoObtainMultipleResult) -> Void) -> HTTPRequest
    
    /// Получить объект перекрытия по id
    ///
    /// - Parameters:
    ///   - overlapId: id перекрытия
    ///   - completion: блок кода, который вызывается по окончании запроса, единственный параметр - результат запроса
    /// - Returns: запрос, который отправляется при вызове данного метода
    func obtainOverlap(with overlapId: String, completion: @escaping (MapOverlapObtainResult) -> Void) -> HTTPRequest
}

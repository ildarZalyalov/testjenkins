//
//  OverlapsServiceImplementation.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 22.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

class OverlapsServiceImplementation: BaseNetworkService, OverlapsService {
    
    let roadworksMethod = "roadworks"
    
    func obtainOverlapsGeo(completion: @escaping (MapOverlapGeoObtainMultipleResult) -> Void) -> HTTPRequest {
        
        let urlString = roadworksMethod
        let configuration = HTTPRequestConfiguration(withMethod: HTTPMethod.GET.rawValue, andUrlString: urlString)
        let urlRequest = requestBuilder.request(with: configuration)
        
        return requestManager.perform(dataRequest: urlRequest)
            .validate(acceptableStatusCodes: [200])
            .validate(acceptableContentTypes: [HTTPRequestContentType.json.rawValue])
            .responseJson(responseHandler: { [weak self] (parsingResult) in
                
                guard let strongSelf = self else {return}
                
                var result: MapOverlapGeoObtainMultipleResult
                
                defer {
                    DispatchQueue.main.async {
                        completion(result)
                    }
                }
                
                switch parsingResult {
                    
                case .success(let json):
                    
                    guard let items = json as? [NSDictionary] else {
                        result = .failure(error:HTTPRequestError.jsonMappingNotExpectedStructure)
                        return
                    }
                    
                    let overlapsGeo = strongSelf.responseMapper.mapJsonAsArray(items, ofResourcesOfType: OverlapGeo.self)
                    
                    result = .success(models: overlapsGeo)
                    
                case .failure(let error):
                    result = .failure(error: error)
                }
            })
    }
    
    func obtainOverlap(with overlapId: String, completion: @escaping (MapOverlapObtainResult) -> Void) -> HTTPRequest {
        
        let urlString = requestBuilder.pathFromComponents([roadworksMethod, overlapId])
        let configuration = HTTPRequestConfiguration(withMethod: HTTPMethod.GET.rawValue, andUrlString: urlString)
        let urlRequest = requestBuilder.request(with: configuration)
        
        return requestManager.perform(dataRequest: urlRequest)
            .validate(acceptableStatusCodes: [200])
            .validate(acceptableContentTypes: [HTTPRequestContentType.json.rawValue])
            .responseJson(responseHandler: { [weak self] (parsingResult) in
                
                guard let strongSelf = self else {return}
                
                var result: MapOverlapObtainResult
                
                defer {
                    DispatchQueue.main.async {
                        completion(result)
                    }
                }
                
                switch parsingResult {
                    
                case .success(let json):
                    
                    guard let item = json as? NSDictionary else {
                        result = .failure(error:HTTPRequestError.jsonMappingNotExpectedStructure)
                        return
                    }
                    
                    let overlap = strongSelf.responseMapper.mapJson(item, toResourceOfType: Overlap.self)
                    
                    result = .success(model: overlap)
                    
                case .failure(let error):
                    result = .failure(error: error)
                }
            })
    }
    
}

//
//  BannerService.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 17.02.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import UIKit

/// Информация о заднике баннера
struct BannerBackgroundConfiguration: Equatable {
    
    /// Картинка для фона
    /// Если фон не определен, то используются стандартные фоны для дня/ночи
    let background: UIImage
    
    /// Цвет для текста на фоне картинки
    /// Если цвет текста не определен, то используются стандартные цвета для дня/ночи
    let textColor: UIColor
    
    /// Стиль статус бара
    /// Если стиль не определен, то используются стандартные стили для дня/ночи
    let statusBarStyle: UIStatusBarStyle
    
    //MARK: - Equatable
    
    static func ==(lhs: BannerBackgroundConfiguration, rhs: BannerBackgroundConfiguration) -> Bool {
        
        let lhsValues = (lhs.background, lhs.textColor, lhs.statusBarStyle)
        let rhsValues = (rhs.background, rhs.textColor, rhs.statusBarStyle)
        
        return lhsValues == rhsValues
    }
}

/// Сервис для работы с баннером (область с фоном на главном экране над списком с чатами)
protocol BannerService {
    
    /// Получаем текущую информацию о заднике баннера
    func obtainСurrentBannerBackgroundConfiguration() -> BannerBackgroundConfiguration
    
    /// Перезапросить задник баннера
    ///
    /// - Parameter completion: блок кода, который вызывается по окончании запроса, единственный параметр - обновленная информация о заднике баннера
    func refreshCurrentBannerBackgroundConfiguration(completion: @escaping (BannerBackgroundConfiguration?) -> Void)
}

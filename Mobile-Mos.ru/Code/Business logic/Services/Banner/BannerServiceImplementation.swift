//
//  BannerServiceImplementation.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 17.02.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class BannerServiceImplementation: BannerService {
    
    var bannerBackgroundLoader: UIImageLoadingAndCacheManager!
    var fileManager: FileManager!
    var sunsetDetector: SunsetDetector!
    var userDefaults: UserDefaults!
    
    let lastBannerBackgroundFileName = "LastAppliedBannerBackground"
    
    let defaultBannerBackgroundNameForDay = "bannerBackgroundDay@2x"
    let defaultBannerBackgroundNameForNight = "bannerBackgroundNight@2x"
    let defaultFootballBannerBackgroundNameForDay = "bannerFootballBackgroundDay@2x"
    let defaultFootballBannerBackgroundNameForNight = "bannerFootballBackgroundNight@2x"
    let defaultBannerBackgroundType = "jpg"
    
    let dateToHideFootballBanner: Date? = {
        
        let components = DateComponents(year: 2018, month: 7, day: 16, hour: 1)
        var calendar = Calendar(identifier: .gregorian)

        if let timeZone = moscowTimezone {
            calendar.timeZone = timeZone
        }
        
        return calendar.date(from: components)
    }()
    
    var lastAppliedBannerBackgroundUrl: URL? {
        get {
            return userDefaults.url(forKey: ApplicationUserDefaultsKey.lastBannerBackgroundUrl.rawValue)
        }
        set {
            userDefaults.set(newValue, forKey: ApplicationUserDefaultsKey.lastBannerBackgroundUrl.rawValue)
            userDefaults.synchronize()
        }
    }
    
    var lastAppliedBannerTextColor: UIColor? {
        
        get {
            
            let key = ApplicationUserDefaultsKey.lastBannerTextColor.rawValue
            
            guard let data = userDefaults.object(forKey: key) as? Data else {
                return nil
            }
            
            return NSKeyedUnarchiver.unarchiveObject(with: data) as? UIColor
        }
        set {
            
            var colorData: Data?
            
            if let color = newValue {
                colorData = NSKeyedArchiver.archivedData(withRootObject: color)
            }
            
            userDefaults.set(colorData, forKey: ApplicationUserDefaultsKey.lastBannerTextColor.rawValue)
            userDefaults.synchronize()
        }
    }
    
    var lastAppliedBannerStatusBarStyle: UIStatusBarStyle? {
        
        get {
            let key = ApplicationUserDefaultsKey.lastBannerStatusBarStyle.rawValue
            return UIStatusBarStyle(rawValueOptional: userDefaults.integer(forKey: key))
        }
        set {
            userDefaults.set(newValue?.rawValue, forKey: ApplicationUserDefaultsKey.lastBannerBackgroundUrl.rawValue)
            userDefaults.synchronize()
        }
    }
    
    var documentsFolderUrl: URL? {
        return fileManager.urls(for: .documentDirectory, in: .userDomainMask).first
    }
    
    var defaultBannerBackground: UIImage {
        
        //здесь используется насильное разворачивание опционала, чтобы приложение падало, если фон по умолчанию не найден
        
        let useUsualBackground = Date() > (dateToHideFootballBanner ?? Date.distantPast)
        let dayBackground = useUsualBackground ? defaultBannerBackgroundNameForDay : defaultFootballBannerBackgroundNameForDay
        let nightBackground = useUsualBackground ? defaultBannerBackgroundNameForNight : defaultFootballBannerBackgroundNameForNight
        
        let backgroundImageName = sunsetDetector.isDaytime ? dayBackground : nightBackground
        
        let path = Bundle.main.path(forResource: backgroundImageName, ofType: defaultBannerBackgroundType)
        return UIImage(contentsOfFile: path!)!
    }
    
    var defaultBannerTextColor: UIColor {
        return sunsetDetector.isDaytime ? UIColorPalette.bannerTextColorForDay : UIColorPalette.bannerTextColorForNight
    }
    
    var defaultBannerStatusBarStyle: UIStatusBarStyle {
        return sunsetDetector.isDaytime ? .default : .lightContent
    }
    
    var defaultBannerBackgroundConfiguration: BannerBackgroundConfiguration {
        return BannerBackgroundConfiguration(background: defaultBannerBackground,
                                             textColor: defaultBannerTextColor,
                                             statusBarStyle: defaultBannerStatusBarStyle)
    }
    
    //MARK: - Приватные методы
    
    fileprivate func lastAppliedBannerBackgroundConfiguration() -> BannerBackgroundConfiguration? {
        
        let documentsUrl = documentsFolderUrl
        
        guard documentsUrl != nil else {
            return nil
        }
        
        let url = documentsFolderUrl!.appendingPathComponent(lastBannerBackgroundFileName)
        
        do {
            
            let imageData = try Data(contentsOf: url)
            
            guard let image = UIImage(data: imageData) else {
                return nil
            }
            
            let textColor = lastAppliedBannerTextColor ?? defaultBannerTextColor
            let statusBarStyle = lastAppliedBannerStatusBarStyle ?? defaultBannerStatusBarStyle
            
            return BannerBackgroundConfiguration(background: image,
                                                 textColor: textColor,
                                                 statusBarStyle: statusBarStyle)
        } catch {
            return nil
        }
    }
    
    fileprivate func saveLastAppliedBannerBackgroundConfiguration(configuration: BannerBackgroundConfiguration) {
        
        let documentsUrl = documentsFolderUrl
        
        guard documentsUrl != nil else {
            return
        }
        
        let url = documentsUrl!.appendingPathComponent(lastBannerBackgroundFileName)
        let imageData = UIImagePNGRepresentation(configuration.background)
        
        guard imageData != nil else {
            return
        }
        
        do {
            try imageData!.write(to: url)
            lastAppliedBannerTextColor = configuration.textColor
            lastAppliedBannerStatusBarStyle = configuration.statusBarStyle
        } catch {
            return
        }
    }
    
    fileprivate func deleteLastAppliedBannerBackgroundConfiguration() {
        
        let documentsUrl = documentsFolderUrl
        
        guard documentsUrl != nil else {
            return
        }
        
        let url = documentsUrl!.appendingPathComponent(lastBannerBackgroundFileName)
        
        do {
            try fileManager.removeItem(at: url)
            lastAppliedBannerTextColor = nil
            lastAppliedBannerStatusBarStyle = nil
        } catch {
            return
        }
    }
    
    fileprivate func obtainCurrentBannerBackgroundUrl(completion: @escaping (URL?, Error?) -> Void) {
        
        //TODO: - интегрировать API для подгрузки фонов
        //let url: URL? = URL(string: "https://stroi.mos.ru/uploads/user_files/images/gorod/city/Empire_1.jpg")
        let url: URL? = nil
        let error: Error? = nil
        
        completion(url, error)
    }
    
    //MARK: - BannerItemsService
    
    func obtainСurrentBannerBackgroundConfiguration() -> BannerBackgroundConfiguration {
        return lastAppliedBannerBackgroundConfiguration() ?? defaultBannerBackgroundConfiguration
    }
    
    func refreshCurrentBannerBackgroundConfiguration(completion: @escaping (BannerBackgroundConfiguration?) -> Void) {
        
        obtainCurrentBannerBackgroundUrl { [weak self] url, error in
            
            guard let strongSelf = self else {
                return
            }
            
            guard error == nil else {
                let configuration = strongSelf.lastAppliedBannerBackgroundConfiguration() ?? strongSelf.defaultBannerBackgroundConfiguration
                completion(configuration)
                return
            }
            
            if url != nil {
                
                guard url != strongSelf.lastAppliedBannerBackgroundUrl else {
                    completion(nil)
                    return
                }
                
                strongSelf.lastAppliedBannerBackgroundUrl = url
                
                strongSelf.bannerBackgroundLoader.loadImage(from: url, with: [.cacheMemoryOnly], completion: { [weak self] result in
                    
                    guard let strongSelf = self else {
                        return
                    }
                    
                    var background: UIImage? = nil
                    
                    defer {
                        
                        var backgroundConfiguration: BannerBackgroundConfiguration?
                        
                        if background != nil {
                            
                            let textColor = strongSelf.lastAppliedBannerTextColor ?? strongSelf.defaultBannerTextColor
                            let statusBarStyle = strongSelf.lastAppliedBannerStatusBarStyle ?? strongSelf.defaultBannerStatusBarStyle
                            backgroundConfiguration = BannerBackgroundConfiguration(background: background!,
                                                                                    textColor: textColor,
                                                                                    statusBarStyle: statusBarStyle)
                            
                            strongSelf.saveLastAppliedBannerBackgroundConfiguration(configuration: backgroundConfiguration!)
                        }
                        
                        completion(backgroundConfiguration)
                    }
                    
                    switch result {
                    case .success(let image, _, _):
                        background = image
                    default:
                        return
                    }
                })
            }
            else {
                strongSelf.deleteLastAppliedBannerBackgroundConfiguration()
                completion(strongSelf.defaultBannerBackgroundConfiguration)
            }
        }
    }
}

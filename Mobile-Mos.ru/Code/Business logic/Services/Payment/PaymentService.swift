//
//  PaymentService.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 16.05.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Результат запроса информации для квитанции платежа
///
/// - success: успех, связанное значение - информация для квитанции платежа
/// - failure: провал, связанное значение - ошибка
enum PaymentUserInfoObtainResult {
    case success(info: PaymentUserInfo?)
    case failure(error: Error)
}

/// Результат запроса на регистрацию пакета платежей
///
/// - success: успех, связанное значение - идентификатор пакета платежей
/// - failure: провал, связанное значение - ошибка
enum PaymentRegistrationResult {
    case success(batchId: String)
    case failure(error: Error)
}

/// Результат выполнения запроса на получение информации о пакете платежей
///
/// - success: успех, связанное значение - пакет платежей
/// - failure: провал, связанное значение - ошибка
enum PaymentRequestResult {
    case success(batch: PaymentBatch)
    case failure(error: PaymentError)
}

/// Результат выполнения запроса на получение списка карт
///
/// - success: успех, связанное значение - список карт
/// - failure: провал, связанное значение - ошибка
enum PaymentCardRequestResult {
    case success(cards: [PaymentCard])
    case failure(error: PaymentError)
}

/// Результат выполнения запроса на получение информации о банке, выпустившем карту
///
/// - success: успех, связанное значение - информация о карте и банке
/// - failure: провал, связанное значение - ошибка
enum PaymentCardInfoRequestResult {
    case success(cardInfo: PaymentCardExtendedInfo)
    case failure(error: PaymentError)
}

/// Результат выполнения запроса на выполнение операции
///
/// - success: успех, связанное значение - результат операции
/// - failure: провал, связанное значение - ошибка
enum PaymentOperationRequestResult {
    case success(result: PaymentOperationResult)
    case failure(error: PaymentError)
}

/// Обработчик результата запроса информации для квитанции платежа
typealias PaymentUserInfoObtainResultHandler = (PaymentUserInfoObtainResult) -> ()

/// Обработчик результата запроса на регистрацию пакета платежей
typealias PaymentRegistrationResultHandler = (PaymentRegistrationResult) -> ()

/// Обработчик редиректа при обработке оплаты (например, на страницу для 3DS авторизации платежа)
typealias PaymentRequestRedirectHandler = (PaymentRedirectInfo) -> ()

/// Обработчик получения информации о пакете платежей
typealias PaymentRequestResultHandler = (PaymentRequestResult) -> ()

/// Обработчик получения списка карт
typealias PaymentCardRequestResultHandler = (PaymentCardRequestResult) -> ()

/// Обработчик получения информации о банке, выпустившем карту
typealias PaymentCardInfoRequestResultHandler = (PaymentCardInfoRequestResult) -> ()

/// Обработчик выполнения операции
typealias PaymentOperationRequestResultHandler = (PaymentOperationRequestResult) -> ()

/// Сервис платежей
protocol PaymentService {
    
    /// Получить текущую информацию для квитанции платежа
    ///
    /// - Parameter completion: обработчик результата запроса информации для квитанции платежа
    func obtainCurrentUserPaymentInfo(completion: @escaping PaymentUserInfoObtainResultHandler)
    
    /// Удалить текущую информацию для квитанции платежа
    func deleteCurrentUserPaymentInfo()
    
    /// Сохранить информацию для квитанции платежа
    ///
    /// - Parameter userInfo: информация для квитанции платежа
    func saveUserPaymentInfo(_ userInfo: PaymentUserInfo)
    
    /// Зарегистрировать пакет платежей для его последующей обработки
    ///
    /// - Parameters:
    ///   - request: данные для регистрации
    ///   - completion: обработчик результата запроса на регистрацию пакета платежей
    /// - Returns: запрос на регистрацию
    func registerPayment(with request: ChatPaymentRequest, completion: @escaping PaymentRegistrationResultHandler) -> HTTPRequest
    
    /// Получить информациию о пакете платежей
    ///
    /// - Parameters:
    ///   - batchId: идентификатор пакета платежей
    ///   - completion: обработчик получения информации о пакете платежей
    func obtainPayment(for batchId: String, completion: @escaping PaymentRequestResultHandler)
    
    /// Получить статус обработки пакета платежей
    ///
    /// - Parameters:
    ///   - batchId: идентификатор пакета платежей
    ///   - redirect: обработчик редиректа при обработке оплаты
    ///   - completion: обработчик получения информации о пакете платежей
    func obtainPaymentStatus(for batchId: String, redirect: @escaping PaymentRequestRedirectHandler, completion: @escaping PaymentRequestResultHandler)
    
    /// Получить список карт для пакета платежей
    ///
    /// - Parameters:
    ///   - batchId: идентификатор пакета платежей
    ///   - completion: обработчик получения списка карт
    func obtainPaymentCards(for batchId: String, completion: @escaping PaymentCardRequestResultHandler)
    
    /// Получить информациию о банке, выпустившем карту
    ///
    /// - Parameters:
    ///   - bin: первые цифры номера карты
    ///   - completion: обработчик получения информации о банке, выпустившем карту
    func obtainPaymentCardInfo(for bin: String, completion: @escaping PaymentCardInfoRequestResultHandler)
    
    /// Запустить обработку пакета платежей
    ///
    /// - Parameters:
    ///   - batchId: идентификатор пакета платежей
    ///   - request: запрос на запуск обработки
    ///   - completion: обработчик получения информации о пакете платежей
    func beginPayment(for batchId: String, with request: PaymentRequest, completion: @escaping PaymentRequestResultHandler)
    
    /// Удалить сохраненную банковскую карту
    ///
    /// - Parameters:
    ///   - cardId: идентификатор карты
    ///   - batchId: идентификатор пакета платежей
    ///   - completion: обработчик выполнения операции
    func deletePaymentCard(with cardId: Int64, batchId: String, completion: @escaping PaymentOperationRequestResultHandler)
    
    /// Переименовать сохраненную банковскую карту
    ///
    /// - Parameters:
    ///   - cardId: идентификатор карты
    ///   - batchId: идентификатор пакета платежей
    ///   - request: запрос на переименование
    ///   - completion: обработчик выполнения операции
    func renamePaymentCard(with cardId: Int64, batchId: String, request: PaymentToolRenameRequest, completion: @escaping PaymentOperationRequestResultHandler)
}

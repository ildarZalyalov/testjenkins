//
//  PaymentServiceImplementation.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 17.05.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

class PaymentServiceImplementation: BaseNetworkService, PaymentService {
    
    /// HTTP коды успеха и редиректа
    let successStatusCode = 200
    let redirectStatusCode = 303
    
    /// Ссылка на метод мобильного бекэнда для регистрации платежа
    let registerPaymentUrlString = "register_payment"
    
    /// Типы ошибок запросов в банк по HTTP кодам
    lazy var errorTypesByStatusCode: [Int : PaymentError.ErrorType] = [
        400 : .invalidParameters,
        406 : .invalidParameters,
        403 : .operationForbidden,
        404 : .objectNotFound,
        429 : .serviceOverload,
        redirectStatusCode : .unexpectedRedirect
    ]
    
    /// Описания ошибок разбора ответов сервера
    let unknownErrorDescription = "Unknown payment error!"
    let requestErrorDescription = "Request json is not correct!"
    
    /// Префикс названия банка, который нужно удалять
    let cardInfoBankPrefixToRemove = "Bank "
    
    var paymentManager: SDKDIT!
    var dataBaseManager: DataBaseManager!
    
    //MARK: - Приватные функции
    
    /// Создаем ошибку неожиданной структуры ответа сервера
    func createUnexpectedResponseError() -> PaymentError {
        return PaymentError(type: .unexpectedResponse, code: nil, description: unknownErrorDescription)
    }
    
    /// Создаем ошибку некорректных параметров запроса
    func createRequestInvalidParametersError() -> PaymentError {
        return PaymentError(type: .invalidParameters, code: nil, description: requestErrorDescription)
    }
    
    /// Валидируем ответ сервера
    ///
    /// - Parameters:
    ///   - statusCode: HTTP код ответа
    ///   - json: json в ответе
    ///   - error: ошибка ответа
    /// - Returns: ошибка платежа (если есть)
    func validate(statusCode: Int,
                  json: Any?,
                  error: NSError?) -> PaymentError? {
        
        guard error == nil else { return PaymentError(error: error!) }
        guard statusCode != successStatusCode else { return nil }
        
        let errorType: PaymentError.ErrorType = errorTypesByStatusCode[statusCode] ?? .unknown
        var error = PaymentError(type: errorType, code: nil, description: unknownErrorDescription)
        
        if let errorJson = json as? NSDictionary {
            error.mapFromJson(errorJson as NSDictionary)
        }
        
        return error
    }
    
    //MARK: - Обработка ответов сервера
    
    func handlePaymentRequestResult(with handler: @escaping PaymentRequestResultHandler, statusCode: Int, json: Any?, error: Error?) {
    
        var result: PaymentRequestResult
        
        defer {
            DispatchQueue.main.async {
                handler(result)
            }
        }
        
        let paymentError = validate(statusCode: statusCode, json: json, error: error as NSError?)
        
        guard paymentError == nil else {
            result = .failure(error: paymentError!)
            return
        }
        guard let batchJson = json as? NSDictionary else {
            result = .failure(error: createUnexpectedResponseError())
            return
        }

        let batch = responseMapper.mapJson(batchJson, toResourceOfType: PaymentBatch.self)
        result = .success(batch: batch)
    }
    
    func handlePaymentRequestRedirect(with handler: @escaping PaymentRequestRedirectHandler, json: NSDictionary) {
        
        let info = responseMapper.mapJson(json, toResourceOfType: PaymentRedirectInfo.self)
        
        DispatchQueue.main.async {
            handler(info)
        }
    }
    
    func handlePaymentCardRequestResult(with handler: @escaping PaymentCardRequestResultHandler, statusCode: Int, json: Any?, error: Error?) {
        
        var result: PaymentCardRequestResult
        
        defer {
            DispatchQueue.main.async {
                handler(result)
            }
        }
        
        let paymentError = validate(statusCode: statusCode, json: json, error: error as NSError?)
        
        guard paymentError == nil else {
            result = .failure(error: paymentError!)
            return
        }
        guard let cardsJson = json as? [NSDictionary] else {
            result = .failure(error: createUnexpectedResponseError())
            return
        }
        
        var cards = responseMapper.mapJsonAsArray(cardsJson, ofResourcesOfType: PaymentCard.self)
        
        cards = cards.map({ card in
            
            guard let bankName = card.extendedCardInfo?.bankDescription else { return card }
            guard bankName.hasPrefix(cardInfoBankPrefixToRemove) else { return card }
            
            var updatedCard = card
            var cardInfo = updatedCard.extendedCardInfo
            cardInfo?.bankDescription = String(bankName.dropFirst(cardInfoBankPrefixToRemove.count))
            updatedCard.extendedCardInfo = cardInfo
            
            return updatedCard
        })
        
        result = .success(cards: cards)
    }
    
    func handlePaymentCardInfoRequestResult(with handler: @escaping PaymentCardInfoRequestResultHandler, statusCode: Int, json: Any?, error: Error?) {
        
        var result: PaymentCardInfoRequestResult
        
        defer {
            DispatchQueue.main.async {
                handler(result)
            }
        }
        
        let paymentError = validate(statusCode: statusCode, json: json, error: error as NSError?)
        
        guard paymentError == nil else {
            result = .failure(error: paymentError!)
            return
        }
        guard let cardInfoJson = json as? NSDictionary else {
            result = .failure(error: createUnexpectedResponseError())
            return
        }
        
        var cardInfo = responseMapper.mapJson(cardInfoJson, toResourceOfType: PaymentCardExtendedInfo.self)
        if let bankName = cardInfo.bankDescription, bankName.hasPrefix(cardInfoBankPrefixToRemove) {
            cardInfo.bankDescription = String(bankName.dropFirst(cardInfoBankPrefixToRemove.count))
        }
        
        result = .success(cardInfo: cardInfo)
    }
    
    func handlePaymentOperationRequestResult(with handler: @escaping PaymentOperationRequestResultHandler, statusCode: Int, json: Any?, error: Error?) {
        
        var result: PaymentOperationRequestResult
        
        defer {
            DispatchQueue.main.async {
                handler(result)
            }
        }
        
        let paymentError = validate(statusCode: statusCode, json: json, error: error as NSError?)
        
        guard paymentError == nil else {
            result = .failure(error: paymentError!)
            return
        }
        guard let resultJson = json as? NSDictionary else {
            result = .failure(error: createUnexpectedResponseError())
            return
        }
        
        let operationResult = responseMapper.mapJson(resultJson, toResourceOfType: PaymentOperationResult.self)
        result = .success(result: operationResult)
    }
    
    //MARK: - PaymentService
    
    func obtainCurrentUserPaymentInfo(completion: @escaping PaymentUserInfoObtainResultHandler) {
        let fetchRequest = DataBaseFetchRequest(with: PaymentUserInfo.self)
        dataBaseManager.fetchAsync(with: fetchRequest) { result in
            switch result {
            case .success(let infos):
                completion(.success(info: infos.first))
            case .error(let error):
                completion(.failure(error: error))
            }
        }
    }
    
    func deleteCurrentUserPaymentInfo() {
        dataBaseManager.deleteAllModelsAsync(of: PaymentUserInfo.self, resultBlock: nil)
    }
    
    func saveUserPaymentInfo(_ userInfo: PaymentUserInfo) {
        dataBaseManager.deleteAllModelsAsync(of: PaymentUserInfo.self) { [weak self] result in
            switch result {
            case .success:
                self?.dataBaseManager.saveAsync(models: [userInfo], resultBlock: nil)
            case .error:
                return
            }
        }
    }
    
    func registerPayment(with request: ChatPaymentRequest, completion: @escaping PaymentRegistrationResultHandler) -> HTTPRequest {
        
        var configuration = HTTPRequestConfiguration(withMethod: HTTPMethod.POST.rawValue, andUrlString: registerPaymentUrlString)
        configuration.parametersEncoding = .json
        
        configuration.parameters = [
            "first_name" : request.firstName,
            "last_name" : request.lastName,
            "middle_name" : request.middleName,
            "email" : request.email,
            "payment_params" : request.params,
            "payment_method" : request.method.rawValue
        ]
        
        if let userId = request.userId {
            configuration.parameters?["user_id"] = userId
        }
        
        let urlRequest = requestBuilder.request(with: configuration)
        
        return requestManager.perform(dataRequest: urlRequest)
            .validate(acceptableStatusCodes: [200])
            .validate(acceptableContentTypes: [HTTPRequestContentType.json.rawValue])
            .responseJson(responseHandler: { [weak self] parsingResult in
                
                guard let strongSelf = self else { return }
                
                var result: PaymentRegistrationResult
                
                defer {
                    DispatchQueue.main.async {
                        completion(result)
                    }
                }
                
                switch parsingResult {
                    
                case .success(let json):
                    
                    guard let responseJson = json as? NSDictionary else {
                        result = .failure(error: HTTPRequestError.jsonMappingNotExpectedStructure)
                        return
                    }
                    
                    let noErrorCode = ChatPaymentResponse.ErrorCode.noError.rawValue
                    let response = strongSelf.responseMapper.mapJson(responseJson, toResourceOfType: ChatPaymentResponse.self)
                    
                    if response.paymentId != nil && response.errorCode == noErrorCode {
                        result = .success(batchId: response.paymentId!)
                    }
                    else {
                        
                        let errorCode = response.errorCode ?? NSURLErrorUnknown
                        let error = NSError(domain: NSURLErrorDomain, code: errorCode, userInfo: [NSLocalizedDescriptionKey: response.errorMessage as Any])
                        
                        result = .failure(error: error)
                    }
                    
                case .failure(let error):
                    result = .failure(error: error)
                }
            })
    }
    
    func obtainPayment(for batchId: String, completion: @escaping PaymentRequestResultHandler) {
        paymentManager.getPayments(batchId) { [weak self] statusCode, json, error in
            self?.handlePaymentRequestResult(with: completion, statusCode: statusCode, json: json, error: error)
        }
    }
    
    func obtainPaymentStatus(for batchId: String, redirect: @escaping PaymentRequestRedirectHandler, completion: @escaping PaymentRequestResultHandler) {
        paymentManager.getPaymentStatus(batchId) { [weak self] statusCode, json, error in
            if let redirectJson = json as? NSDictionary, statusCode == self?.redirectStatusCode, error == nil {
                self?.handlePaymentRequestRedirect(with: redirect, json: redirectJson)
            }
            else {
                self?.handlePaymentRequestResult(with: completion, statusCode: statusCode, json: json, error: error)
            }
        }
    }
    
    func obtainPaymentCards(for batchId: String, completion: @escaping PaymentCardRequestResultHandler) {
        paymentManager.getCards(batchId) { [weak self] statusCode, json, error in
            self?.handlePaymentCardRequestResult(with: completion, statusCode: statusCode, json: json, error: error)
        }
    }
    
    func obtainPaymentCardInfo(for bin: String, completion: @escaping PaymentCardInfoRequestResultHandler) {
        paymentManager.getBank(bin) { [weak self] statusCode, json, error in
            self?.handlePaymentCardInfoRequestResult(with: completion, statusCode: statusCode, json: json, error: error)
        }
    }
    
    func beginPayment(for batchId: String, with request: PaymentRequest, completion: @escaping PaymentRequestResultHandler) {
        
        guard let requestJson = request.serializeToJson() as? [AnyHashable : Any] else {
            completion(.failure(error: createRequestInvalidParametersError()))
            return
        }
        
        paymentManager.startPayment(batchId, body: requestJson) { [weak self] statusCode, json, error in
            self?.handlePaymentRequestResult(with: completion, statusCode: statusCode, json: json, error: error)
        }
    }
    
    func deletePaymentCard(with cardId: Int64, batchId: String, completion: @escaping PaymentOperationRequestResultHandler) {
        paymentManager.removeCard(String(cardId), batch: batchId) { [weak self] statusCode, json, error in
            self?.handlePaymentOperationRequestResult(with: completion, statusCode: statusCode, json: json, error: error)
        }
    }
    
    func renamePaymentCard(with cardId: Int64, batchId: String, request: PaymentToolRenameRequest, completion: @escaping PaymentOperationRequestResultHandler) {
        
        guard let requestJson = request.serializeToJson() as? [AnyHashable : Any] else {
            completion(.failure(error: createRequestInvalidParametersError()))
            return
        }
        
        paymentManager.renameCard(String(cardId), batch: batchId, body: requestJson) { [weak self] statusCode, json, error in
            self?.handlePaymentOperationRequestResult(with: completion, statusCode: statusCode, json: json, error: error)
        }
    }
}

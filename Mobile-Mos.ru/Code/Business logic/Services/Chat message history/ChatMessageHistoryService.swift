//
//  ChatMessageHistoryService.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 26.06.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Результат запроса на получение сообщения
///
/// - success: успех, связанное значение - сообщение или nil, если подходящее сообщение не было найдено
/// - failure: провал, связанное значение - ошибка
enum ChatMessageObtainResult {
    case success(item: ChatMessage?)
    case failure(error: Error)
}

/// Результат запроса на получение списка сообщений
///
/// - success: успех, связанное значение - список сообщений
/// - failure: провал, связанное значение - ошибка
enum ChatMessageObtainMultipleResult {
    case success(items: [ChatMessage])
    case failure(error: Error)
}

/// Результат запроса на получение части истории сообщений
///
/// - success: успех, связанное значение - часть истории сообщений
/// - failure: провал, связанное значение - ошибка
enum ChatMessageHistoryPortionObtainResult {
    case success(history: ChatMessageHistoryPortion)
    case failure(error: Error)
}

/// Результат запроса на получение части закешированной истории сообщений
///
/// - success: успех, связанное значение - часть истории сообщений
/// - failure: провал, связанное значение - ошибка
enum ChatMessageCachedHistoryPortionObtainResult {
    case success(history: ChatMessageCachedHistoryPortion)
    case failure(error: Error)
}

/// Блок обработки получения сообщения, единственный параметр - результат запроса
typealias ChatMessageObtainHandler = (ChatMessageObtainResult) -> Void

/// Блок обработки получения списка сообщений, единственный параметр - результат запроса
typealias ChatMessageObtainMultipleHandler = (ChatMessageObtainMultipleResult) -> Void

/// Блок обработки получения части истории сообщений, единственный параметр - результат запроса
typealias ChatMessageHistoryPortionObtainHandler = (ChatMessageHistoryPortionObtainResult) -> Void

/// Блок обработки получения части закешированной истории сообщений, единственный параметр - результат запроса
typealias ChatMessageCachedHistoryPortionObtainHandler = (ChatMessageCachedHistoryPortionObtainResult) -> Void

/// Сервис для работы с историей сообщений
protocol ChatMessageHistoryService {
    
    /// Получить часть истории сообщений с сервера
    ///
    /// - Parameters:
    ///   - messageId: серверный идентификатор сообщения, относительно которого запрашивается история - идет выборка сообщений, которые были получены/отправлены до этого сообщения; если не передан, то идет запрос самых свежих сообщений в истории
    ///   - conversation: диалог, в котором находится пользователь (например, Привет, Москва!)
    ///   - user: текущий пользователь
    ///   - limit: максимальное кол-во сообщений, которое хотим получить
    ///   - completion: блок обработки получения части истории сообщений
    func obtainHistoryPortion(before messageId: Int64?, for conversation: Conversation, and user: User, withLimit limit: Int, completion: @escaping ChatMessageHistoryPortionObtainHandler)
    
    /// Получить часть закешированной истории сообщений до определенного сообщения
    ///
    /// - Parameters:
    ///   - message: сообщение, относительно которого запрашивается история - идет выборка сообщений, которые были получены/отправлены до этого сообщения (если не передано, выбираются сообщения с конца истории)
    ///   - conversation: диалог, в котором находится пользователь (например, Привет, Москва!)
    ///   - user: текущий пользователь
    ///   - limit: максимальное кол-во сообщений, которое хотим получить
    ///   - completion: блок обработки получения части закешированной истории сообщений
    func obtainCachedHistoryPortion(before message: ChatMessage?, for conversation: Conversation, and user: User, withLimit limit: Int, completion: @escaping ChatMessageCachedHistoryPortionObtainHandler)
    
    /// Получить часть закешированной истории сообщений после определенного сообщения
    ///
    /// - Parameters:
    ///   - message: сообщение, относительно которого запрашивается история - идет выборка сообщений, которые были получены/отправлены после этого сообщения (если не передано, выбираются сообщения с начала истории)
    ///   - conversation: диалог, в котором находится пользователь (например, Привет, Москва!)
    ///   - user: текущий пользователь
    ///   - limit: максимальное кол-во сообщений, которое хотим получить
    ///   - completion: блок обработки получения части закешированной истории сообщений
    func obtainCachedHistoryPortion(after message: ChatMessage?, for conversation: Conversation, and user: User, withLimit limit: Int, completion: @escaping ChatMessageCachedHistoryPortionObtainHandler)
    
    /// Получить часть закешированной истории сообщений, включающую определенное сообщение
    ///
    /// - Parameters:
    ///   - message: сообщение, относительно которого запрашивается история - идет выборка сообщений, которые были получены/отправлены до и после этого сообщения
    ///   - conversation: диалог, в котором находится пользователь (например, Привет, Москва!)
    ///   - user: текущий пользователь
    ///   - limit: максимальное кол-во сообщений, которое хотим получить, за исключением message (идет выборка limit / 2 до и после message)
    ///   - completion: блок обработки получения части закешированной истории сообщений
    func obtainCachedHistoryPortion(including message: ChatMessage, for conversation: Conversation, and user: User, withLimit limit: Int, completion: @escaping ChatMessageCachedHistoryPortionObtainHandler)
    
    /// Выполнить поиск сообщений в истории по их тексту
    ///
    /// - Parameters:
    ///   - searchString: строка поиска
    ///   - user: текущий пользователь
    ///   - limit: максимальное кол-во сообщений, которое хотим получить
    ///   - completion: блок обработки получения списка сообщений
    func searchCachedMessages(with searchString: String, for user: User, withLimit limit: Int, completion: @escaping ChatMessageObtainMultipleHandler)
    
    /// Получить сообщение по идентификатору
    ///
    /// - Parameters:
    ///   - messageUid: идентификатор сообщения
    ///   - completion: блок обработки получения сообщения
    func obtainCachedMessage(withUid messageUid: String, completion: @escaping ChatMessageObtainHandler)
    
    /// Получить последнее входящее сообщение в истории чата
    ///
    /// - Parameters:
    ///   - conversation: нужный чат
    ///   - user: текущий пользователь
    ///   - completion: блок обработки получения сообщения
    func obtainLastIncomingCachedMessage(for conversation: Conversation, and user: User, completion: @escaping ChatMessageObtainHandler)
    
    /// Получить последнее сообщение "группу" (содержащее вложенные сообщения, которые показываются друг за другом) в истории чата
    ///
    /// - Parameters:
    ///   - conversation: нужный чат
    ///   - user: текущий пользователь
    ///   - completion: блок обработки получения сообщения
    func obtainLastCachedMessageGroup(for conversation: Conversation, and user: User, completion: @escaping ChatMessageObtainHandler)
    
    /// Получить контроллер для отслеживания изменения последнего сообщения в чате
    ///
    /// - Parameters:
    ///   - conversation: нужный чат
    ///   - user: текущий пользователь
    ///   - completion: блок обработки получения контроллера
    func obtainLastCachedMessageController(for conversation: Conversation, and user: User, completion: @escaping DataBaseControllerFetchResultBlock<ChatMessage>)
    
    /// Обновить сообщение в кэше
    ///
    /// - Parameter message: сообщение
    func updateInCachedHistory(message: ChatMessage)
    
    /// Удалить сообщение из закешированной истории сообщений
    ///
    /// - Parameter message: удаляемое сообщение
    func deleteFromCachedHistory(message: ChatMessage)
    
    /// Удалить закешированную историю сообщений для пользователя
    ///
    /// - Parameter user: пользователь
    func clearCachedHistory(for user: User)
}

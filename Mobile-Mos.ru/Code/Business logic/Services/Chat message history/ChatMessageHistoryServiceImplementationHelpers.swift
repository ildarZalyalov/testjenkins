//
//  ChatMessageHistoryServiceImplementationHelpers.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 26.06.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

extension ChatMessageHistoryServiceImplementation {
    
    /// Получение предиката для фильтрации выборки из БД части закешированной истории сообщений до определенного сообщения
    ///
    /// - Parameters:
    ///   - message: сообщение, относительно которого запрашивается история - идет выборка сообщений, которые были получены/отправлены до этого сообщения
    ///   - conversation: диалог, в котором находится пользователь (например, Привет, Москва!)
    ///   - user: текущий пользователь
    /// - Returns: предикат для фильтрации
    static func predicateForCachedHistoryPortion(before message: ChatMessage?, for conversation: Conversation, and user: User) -> NSPredicate {
        
        var configuration = NSPredicateConfiguration(with: "\(#keyPath(ChatMessageModel.chatId)) = %@", and: [conversation.itemId])
        configuration.and(condition: ("\(#keyPath(ChatMessageModel.parentMessageUid)) = nil", []))
        
        if let userId = user.userId {
            configuration.and(condition: ("\(#keyPath(ChatMessageModel.userId)) = %@", [userId]))
        }
        else {
            configuration.and(condition: ("\(#keyPath(ChatMessageModel.sessionId)) = %@", [user.sessionId]))
        }
        
        if let beforeMessage = message {
            configuration.and(condition: ("\(#keyPath(ChatMessageModel.messageId)) < %@", [beforeMessage.messageId]))
        }
        
        return NSPredicate(with: configuration)
    }
    
    /// Получение предиката для фильтрации выборки из БД части закешированной истории сообщений после определенного сообщения
    ///
    /// - Parameters:
    ///   - message: сообщение, относительно которого запрашивается история - идет выборка сообщений, которые были получены/отправлены после этого сообщения
    ///   - conversation: диалог, в котором находится пользователь (например, Привет, Москва!)
    ///   - user: текущий пользователь
    /// - Returns: предикат для фильтрации
    static func predicateForCachedHistoryPortion(after message: ChatMessage?, for conversation: Conversation, and user: User) -> NSPredicate {
        
        var configuration = NSPredicateConfiguration(with: "\(#keyPath(ChatMessageModel.chatId)) = %@", and: [conversation.itemId])
        configuration.and(condition: ("\(#keyPath(ChatMessageModel.parentMessageUid)) = nil", []))
        
        if let userId = user.userId {
            configuration.and(condition: ("\(#keyPath(ChatMessageModel.userId)) = %@", [userId]))
        }
        else {
            configuration.and(condition: ("\(#keyPath(ChatMessageModel.sessionId)) = %@", [user.sessionId]))
        }
        
        if let beforeMessage = message {
            configuration.and(condition: ("\(#keyPath(ChatMessageModel.messageId)) > %@", [beforeMessage.messageId]))
        }
        
        return NSPredicate(with: configuration)
    }
    
    /// Получение предиката для фильтрации выборки из БД сообщений, в которых содержится строка поиска
    ///
    /// - Parameters:
    ///   - searchString: строка поиска
    ///   - user: текущий пользователь
    /// - Returns: предикат для фильтрации
    func predicateForSearchingCachedMessages(with searchString: String, and user: User) -> NSPredicate {
        
        var configuration = NSPredicateConfiguration(with: "\(#keyPath(ChatMessageModel.contentModel.previewString)) contains[cd] %@", and: [searchString])
        
        if let userId = user.userId {
            configuration.and(condition: ("\(#keyPath(ChatMessageModel.userId)) = %@", [userId]))
        }
        else {
            configuration.and(condition: ("\(#keyPath(ChatMessageModel.sessionId)) = %@", [user.sessionId]))
        }
        
        return NSPredicate(with: configuration)
    }
    
    /// Получение предиката для фильтрации выборки из БД последнего сообщения в диалоге
    ///
    /// - Parameters:
    ///   - conversation: диалог, в котором находится пользователь (например, Привет, Москва!)
    ///   - user: текущий пользователь
    /// - Returns: предикат для фильтрации
    func predicateForLastCachedMessage(in conversation: Conversation, for user: User) -> NSPredicate {
        
        var configuration = NSPredicateConfiguration(with: "\(#keyPath(ChatMessageModel.chatId)) = %@", and: [conversation.itemId])
        configuration.and(condition: ("\(#keyPath(ChatMessageModel.parentMessageUid)) = nil", []))
        
        if let userId = user.userId {
            configuration.and(condition: ("\(#keyPath(ChatMessageModel.userId)) = %@", [userId]))
        }
        else {
            configuration.and(condition: ("\(#keyPath(ChatMessageModel.sessionId)) = %@", [user.sessionId]))
        }
        
        return NSPredicate(with: configuration)
    }
    
    /// Получение предиката для фильтрации выборки из БД последнего входящего сообщения в диалоге
    ///
    /// - Parameters:
    ///   - conversation: диалог, в котором находится пользователь (например, Привет, Москва!)
    ///   - user: текущий пользователь
    /// - Returns: предикат для фильтрации
    func predicateForLastIncomingCachedMessage(in conversation: Conversation, for user: User) -> NSPredicate {
        
        var configuration = NSPredicateConfiguration(with: "\(#keyPath(ChatMessageModel.chatId)) = %@", and: [conversation.itemId])
        configuration.and(condition: ("\(#keyPath(ChatMessageModel.parentMessageUid)) = nil", []))
        configuration.and(condition: ("\(#keyPath(ChatMessageModel.isIncoming)) = true", []))
        
        if let userId = user.userId {
            configuration.and(condition: ("\(#keyPath(ChatMessageModel.userId)) = %@", [userId]))
        }
        else {
            configuration.and(condition: ("\(#keyPath(ChatMessageModel.sessionId)) = %@", [user.sessionId]))
        }
        
        return NSPredicate(with: configuration)
    }
    
    /// Получение предиката для фильтрации выборки из БД последнего сообщения "группы" в диалоге
    ///
    /// - Parameters:
    ///   - conversation: диалог, в котором находится пользователь (например, Привет, Москва!)
    ///   - user: текущий пользователь
    /// - Returns: предикат для фильтрации
    func predicateForLastCachedMessageGroup(in conversation: Conversation, for user: User) -> NSPredicate {
        
        var configuration = NSPredicateConfiguration(with: "\(#keyPath(ChatMessageModel.chatId)) = %@", and: [conversation.itemId])
        configuration.and(condition: ("\(#keyPath(ChatMessageModel.contentModel.typeRawValue)) = %@", [ChatMessageContent.ContentType.messageGroup.rawValue]))
        configuration.and(condition: ("\(#keyPath(ChatMessageModel.parentMessageUid)) = nil", []))
        
        if let userId = user.userId {
            configuration.and(condition: ("\(#keyPath(ChatMessageModel.userId)) = %@", [userId]))
        }
        else {
            configuration.and(condition: ("\(#keyPath(ChatMessageModel.sessionId)) = %@", [user.sessionId]))
        }
        
        return NSPredicate(with: configuration)
    }
    
    /// Получение предиката для фильтрации выборки из БД сообщений, идентификаторы которых находятся среди переданных
    ///
    /// - Parameter messageIds: идентификаторы интересующих сообщений
    /// - Returns: предикат для фильтрации
    static func predicateForCachedMessages(withIdsIn messageIds: [Int64]) -> NSPredicate {
        
        var configuration = NSPredicateConfiguration(with: #keyPath(ChatMessageModel.messageId), in: messageIds)
        configuration.and(condition: ("\(#keyPath(ChatMessageModel.parentMessageUid)) = nil", []))
        
        return NSPredicate(with: configuration)
    }
}

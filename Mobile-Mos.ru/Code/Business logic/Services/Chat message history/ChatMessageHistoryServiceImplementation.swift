//
//  ChatMessageHistoryServiceImplementation.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 26.06.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class ChatMessageHistoryServiceImplementation: ChatMessageHistoryService {
    
    /// Типы сообщений в сокете
    ///
    /// - getMessageHistory: запросить часть истории сообщений
    /// - getMessageHistoryResponse: получить часть истории сообщений
    fileprivate enum Event: String {
        case getMessageHistory
        case getMessageHistoryResponse
    }
    
    //MARK: - Статические компоненты
    
    // Обработчики, добавляемые экземплярами сервиса по вызову методов в неограниченном кол-ве
    
    fileprivate static var historyRequestsQueue = ThreadSafeSerialEventQueue<ChatMessageHistoryPortionObtainResult>()
    
    static var sharedMessageFactory: ChatMessageFactory!
    static var sharedDataBaseManager: DataBaseManager!
    static var currentUserManager: CurrentUserManager!
    
    static var sharedSocket: Socket! {
        
        didSet {
            
            // Получение части истории сообщений
            
            sharedSocket.onAsync(event: Event.getMessageHistoryResponse.rawValue) { socketData, _ in
                
                guard let handler = historyRequestsQueue.dequeue()?.handler else { return }
                
                var result: ChatMessageHistoryPortionObtainResult
                
                defer {
                    DispatchQueue.main.async(execute: {
                        handler(result)
                    })
                }
                
                guard let json = (socketData as? [NSDictionary])?.first else {
                    result = .failure(error: HTTPRequestError.jsonParsingNoResponseDataError)
                    return
                }
                
                var historyPortion = sharedMessageFactory.buildMessageHistoryPortion(from: json, for: currentUserManager.currentUser)
                save(historyPortion: &historyPortion)
                
                result = .success(history: historyPortion)
            }
            
            // Восстановление соединения (перезапуск очередей)
            
            sharedSocket.onAsync(event: SocketEvent.connect.rawValue) { _,_  in
                historyRequestsQueue.resume()
            }
            
            // Обработка обрыва соединения (остановка очередей)
            
            sharedSocket.onAsync(event: SocketEvent.disconnect.rawValue) { _,_  in
                historyRequestsQueue.pause()
            }
        }
    }
    
    //MARK: - Приватные методы
    
    fileprivate typealias Service = ChatMessageHistoryServiceImplementation
    
    /// Сохранить часть истории сообщений в кэш. Если сообщение не может быть обновлено в кэше (ChatMessage.canBeUpdatedInHistory), то
    /// в переданном куске истории оно заменяется на эквивалентное сообщение из кэша
    ///
    /// - warning: Метод использует синхронное сохранение в БД и будет блокировать вызывающий поток до завершения работы!
    /// - Parameter historyPortion: часть истории сообщений
    fileprivate static func save(historyPortion: inout ChatMessageHistoryPortion) {
        
        guard !historyPortion.messages.isEmpty else { return }
        
        let predicate = predicateForCachedMessages(withIdsIn: historyPortion.messages.map { $0.messageId })
        let request = DataBaseFetchRequest(with: ChatMessage.self, filteredWith: predicate)
        
        guard let cachedMessages = try? sharedDataBaseManager.fetch(with: request) else { return }
        
        let cachedMessagesGroupedById = cachedMessages.group { $0.messageId }
        var cachedMessageById = [Int64 : ChatMessage]()
        
        // в медиа каналах в перерывах между получением новостей можно переписываться с ботом.
        // эта переписка не сохраняется на сервере, messageId всех таких сообщений будет равен
        // messageId последней новости.
        // поэтому, если messageId совпадает у нескольких сообщений, то выбираем самое старое,
        // ведь остальные не хранятся в истории на сервере.
        for (messageId, cachedMessagesWithSameId) in cachedMessagesGroupedById {
            cachedMessageById[messageId] = cachedMessagesWithSameId.sorted(by: { $0.date < $1.date }).first
        }
        
        var messagesToSave = [ChatMessage]()
        
        for index in 0 ..< historyPortion.messages.count {
            
            var message = historyPortion.messages[index]
            
            if let cachedMessage = cachedMessageById[message.messageId] {

                if cachedMessage.canBeUpdatedInHistory {
                    message.uid = cachedMessage.uid
                    historyPortion.messages[index] = message
                }
                else {
                    historyPortion.messages[index] = cachedMessage
                    continue
                }
            }
            
            messagesToSave.append(message)
        }
        
        try? sharedDataBaseManager.save(models: messagesToSave)
    }
    
    /// Сделать выборку сообщений из закешированной истории относительно другого сообщения
    ///
    /// - warning: Метод использует синхронные запросы в БД и будет блокировать вызывающий поток до завершения работы!
    /// - Parameters:
    ///   - message: сообщение, относительно которого идет выборка - выбираются сообщения, которые были получены/отосланы до/после данного сообщения (если не передано, то выборка идет с конца/начала истории)
    ///   - before: нужны ли нам сообщения до message (если false, выборка идет после message)
    ///   - conversation: текущий диалог
    ///   - user: текущий пользователь
    ///   - limit: максимальное кол-во сообщений в выборке
    /// - Returns: результат выборки
    fileprivate static func obtainCachedMessagesHistoryPortion(around message: ChatMessage?, before: Bool, for conversation: Conversation, and user: User, withLimit limit: Int) -> ChatMessageCachedHistoryPortionObtainResult {
        
        let predicateBefore = predicateForCachedHistoryPortion(before: message, for: conversation, and: user)
        let predicateAfter = predicateForCachedHistoryPortion(after: message, for: conversation, and: user)
        
        let limitBefore = before ? limit + 1 : 1
        let limitAfter = before ? 1 : limit + 1
        
        let sortDescriptors = [DataBaseSortDescriptor(sortKeyPath: #keyPath(ChatMessageModel.messageId)),
                               DataBaseSortDescriptor(sortKeyPath: #keyPath(ChatMessageModel.date))]
        
        var requestBefore = DataBaseFetchRequest(with: ChatMessage.self, limitedBy: limitBefore, filteredWith: predicateBefore)
        requestBefore.sortDescriptors = sortDescriptors
        requestBefore.limitType = .suffix
        
        var requestAfter = DataBaseFetchRequest(with: ChatMessage.self, limitedBy: limitAfter, filteredWith: predicateAfter)
        requestAfter.sortDescriptors = sortDescriptors
        requestAfter.limitType = .prefix
        
        var messagesBefore: [ChatMessage] = []
        var messagesAfter: [ChatMessage] = []
        var result: ChatMessageCachedHistoryPortionObtainResult
        
        let obtainingFromBeginning = !before && message == nil
        let obtainingFromEnd = before && message == nil
        
        if !obtainingFromBeginning {
            
            do {
                messagesBefore = try sharedDataBaseManager.fetch(with: requestBefore)
            } catch let error {
                result = .failure(error: error)
            }
        }
        
        if !obtainingFromEnd {
            
            do {
                messagesAfter = try sharedDataBaseManager.fetch(with: requestAfter)
            } catch let error {
                result = .failure(error: error)
            }
        }
        
        let hasPrevious = messagesBefore.count == limitBefore
        let hasNext = messagesAfter.count == limitAfter
        var messages = before ? messagesBefore : messagesAfter
        
        if before && hasPrevious {
            messages.removeFirst()
        }
        else if !before && hasNext {
            messages.removeLast()
        }
        
        messages.forEach {
            sharedMessageFactory.updateDocumentDownloadStateIfNeeded(in: $0)
        }
        
        let portion = ChatMessageCachedHistoryPortion(hasMorePrevious: hasPrevious,
                                                      hasMoreNext: hasNext,
                                                      messages: messages)
        
        result = .success(history: portion)
        
        return result
    }
    
    //MARK: - ChatMessageHistoryService
    
    func obtainHistoryPortion(before messageId: Int64?, for conversation: Conversation, and user: User, withLimit limit: Int, completion: @escaping ChatMessageHistoryPortionObtainHandler) {
        
        let request = ChatMessageHistoryPortionRequest(messageId: messageId,
                                                       chatId: conversation.itemId,
                                                       userId: user.userId,
                                                       sessionId: user.sessionId,
                                                       limit: limit)
        
        let emitter = {
            Service.sharedSocket.send(event: Event.getMessageHistory.rawValue, with: [request.serializeToJson()])
        }
        
        let handler: ChatMessageHistoryPortionObtainHandler = { historyResult in
            
            var result = historyResult
            
            defer {
                completion(result)
            }
            
            switch result {
                
            case .success(let historyPortion):
                
                guard historyPortion.chatId == conversation.itemId else {
                    result = .failure(error: HTTPRequestError.jsonMappingNotExpectedStructure)
                    return
                }
                
            default:
                return
            }
        }
        
        let eventBlock = SerialEventBlock(emitter: emitter, handler: handler)
        Service.historyRequestsQueue.enqueue(element: eventBlock)
    }
    
    func obtainCachedHistoryPortion(before message: ChatMessage?, for conversation: Conversation, and user: User, withLimit limit: Int, completion: @escaping ChatMessageCachedHistoryPortionObtainHandler) {
        Service.sharedDataBaseManager.performAsyncAfterAllSavesFinished {
            let result = Service.obtainCachedMessagesHistoryPortion(around: message, before: true, for: conversation, and: user, withLimit: limit)
            DispatchQueue.main.async {
                completion(result)
            }
        }
    }
    
    func obtainCachedHistoryPortion(after message: ChatMessage?, for conversation: Conversation, and user: User, withLimit limit: Int, completion: @escaping ChatMessageCachedHistoryPortionObtainHandler) {
        Service.sharedDataBaseManager.performAsyncAfterAllSavesFinished {
            let result = Service.obtainCachedMessagesHistoryPortion(around: message, before: false, for: conversation, and: user, withLimit: limit)
            DispatchQueue.main.async {
                completion(result)
            }
        }
    }
    
    func obtainCachedHistoryPortion(including message: ChatMessage, for conversation: Conversation, and user: User, withLimit limit: Int, completion: @escaping ChatMessageCachedHistoryPortionObtainHandler) {
        
        Service.sharedDataBaseManager.performAsyncAfterAllSavesFinished {
            
            let halfLimit = limit / 2
            
            let resultBefore = Service.obtainCachedMessagesHistoryPortion(around: message, before: true, for: conversation, and: user, withLimit: halfLimit)
            let resultAfter = Service.obtainCachedMessagesHistoryPortion(around: message, before: false, for: conversation, and: user, withLimit: halfLimit)
            
            var historyBefore: ChatMessageCachedHistoryPortion
            var historyAfter: ChatMessageCachedHistoryPortion
            var result: ChatMessageCachedHistoryPortionObtainResult
            
            defer {
                DispatchQueue.main.async {
                    completion(result)
                }
            }
            
            switch resultBefore {
            case .success(let history):
                historyBefore = history
            case .failure(_):
                result = resultBefore
                return
            }
            
            switch resultAfter {
            case .success(let history):
                historyAfter = history
            case .failure(_):
                result = resultAfter
                return
            }
            
            var messages = historyBefore.messages
            if message.parentMessageUid == nil {
                messages += [message]
            }
            messages += historyAfter.messages
            
            let portion = ChatMessageCachedHistoryPortion(hasMorePrevious: historyBefore.hasMorePrevious,
                                                          hasMoreNext: historyAfter.hasMoreNext,
                                                          messages: messages)
            
            result = .success(history: portion)
        }
    }
    
    func searchCachedMessages(with searchString: String, for user: User, withLimit limit: Int, completion: @escaping ChatMessageObtainMultipleHandler) {
        
        let predicate = predicateForSearchingCachedMessages(with: searchString, and: user)
        let request = DataBaseFetchRequest(with: ChatMessage.self, limitedBy: limit, filteredWith: predicate)
        
        Service.sharedDataBaseManager.performAsyncAfterAllSavesFinished {
            
            var result: ChatMessageObtainMultipleResult
            
            do {
                let messages = try Service.sharedDataBaseManager.fetch(with: request)
                result = .success(items: messages)
            } catch let error {
                result = .failure(error: error)
            }
            
            DispatchQueue.main.async {
                completion(result)
            }
        }
    }
    
    func obtainCachedMessage(withUid messageUid: String, completion: @escaping ChatMessageObtainHandler) {
        
        let configuration = NSPredicateConfiguration(with: "\(#keyPath(ChatMessageModel.uid)) = %@", and: [messageUid])
        let request = DataBaseFetchRequest(with: ChatMessage.self, limitedBy: 1, filteredWith: NSPredicate(with: configuration))
        
        Service.sharedDataBaseManager.performAsyncAfterAllSavesFinished {
            
            var result: ChatMessageObtainResult
            
            do {
                let messages = try Service.sharedDataBaseManager.fetch(with: request)
                result = .success(item: messages.last)
            } catch let error {
                result = .failure(error: error)
            }
            
            DispatchQueue.main.async {
                completion(result)
            }
        }
    }
    
    func obtainLastIncomingCachedMessage(for conversation: Conversation, and user: User, completion: @escaping ChatMessageObtainHandler) {
        
        let predicate = predicateForLastIncomingCachedMessage(in: conversation, for: user)
        var fetchRequest = DataBaseFetchRequest(with: ChatMessage.self, limitedBy: 1, filteredWith: predicate)
        fetchRequest.sortDescriptors = [DataBaseSortDescriptor(sortKeyPath: #keyPath(ChatMessageModel.messageId)),
                                        DataBaseSortDescriptor(sortKeyPath: #keyPath(ChatMessageModel.date))]
        fetchRequest.limitType = .suffix
        
        Service.sharedDataBaseManager.performAsyncAfterAllSavesFinished {
            
            var result: ChatMessageObtainResult
            
            do {
                let messages = try Service.sharedDataBaseManager.fetch(with: fetchRequest)
                result = .success(item: messages.last)
            } catch let error {
                result = .failure(error: error)
            }
            
            DispatchQueue.main.async {
                completion(result)
            }
        }
    }
    
    func obtainLastCachedMessageGroup(for conversation: Conversation, and user: User, completion: @escaping ChatMessageObtainHandler) {
        
        let predicate = predicateForLastCachedMessageGroup(in: conversation, for: user)
        var fetchRequest = DataBaseFetchRequest(with: ChatMessage.self, limitedBy: 1, filteredWith: predicate)
        fetchRequest.sortDescriptors = [DataBaseSortDescriptor(sortKeyPath: #keyPath(ChatMessageModel.messageId)),
                                        DataBaseSortDescriptor(sortKeyPath: #keyPath(ChatMessageModel.date))]
        fetchRequest.limitType = .suffix
        
        Service.sharedDataBaseManager.performAsyncAfterAllSavesFinished {
            
            var result: ChatMessageObtainResult
            
            do {
                let messages = try Service.sharedDataBaseManager.fetch(with: fetchRequest)
                result = .success(item: messages.last)
            } catch let error {
                result = .failure(error: error)
            }
            
            DispatchQueue.main.async {
                completion(result)
            }
        }
    }
    
    func obtainLastCachedMessageController(for conversation: Conversation, and user: User, completion: @escaping DataBaseControllerFetchResultBlock<ChatMessage>) {
        
        let predicate = predicateForLastCachedMessage(in: conversation, for: user)
        var fetchRequest = DataBaseFetchRequest(with: ChatMessage.self, limitedBy: 1, filteredWith: predicate)
        fetchRequest.sortDescriptors = [DataBaseSortDescriptor(sortKeyPath: #keyPath(ChatMessageModel.messageId)),
                                        DataBaseSortDescriptor(sortKeyPath: #keyPath(ChatMessageModel.date))]
        fetchRequest.limitType = .suffix
        
        Service.sharedDataBaseManager.fetchControllerAsync(with: fetchRequest, resultBlock: completion)
    }
    
    func updateInCachedHistory(message: ChatMessage) {
        Service.sharedDataBaseManager.saveAsync(models: [message], resultBlock: nil)
    }
    
    func deleteFromCachedHistory(message: ChatMessage) {
        Service.sharedDataBaseManager.deleteAsync(models: [message], resultBlock: nil)
    }
    
    func clearCachedHistory(for user: User) {
        
        var configuration: NSPredicateConfiguration
        
        if let userId = user.userId {
            configuration = NSPredicateConfiguration(with: "\(#keyPath(ChatMessageModel.userId)) = %@", and: [userId])
        }
        else {
            configuration = NSPredicateConfiguration(with: "\(#keyPath(ChatMessageModel.sessionId)) = %@", and: [user.sessionId])
        }

        Service.sharedDataBaseManager.deleteAllModelsAsync(of: ChatMessage.self, where: NSPredicate(with: configuration), resultBlock: nil)
    }
}

//
//  UserDataOptionsService.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 20.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Ошибки запроса опций данных пользователя
///
/// - userNotAuthorized: пользователь не авторизован
enum UserDataOptionsErrors: Error {
    case userNotAuthorized
}

/// Результат запроса на получение опций данных пользователя
///
/// - success: успех, связанное значение - список опций данных пользователя
/// - failure: провал, связанное значение - ошибка
enum UserDataOptionsObtainResult {
    case success(options: UserOptionsData)
    case failure(error: Error)
}

/// Обработчик результата запроса опций данных пользователя
typealias UserDataOptionsObtainHandler = (UserDataOptionsObtainResult) -> Void

/// Результат операции с опциями данных пользователя
///
/// - success: успех
/// - failure: провал, связанное значение - ошибка
enum UserDataOptionsOperationResult {
    case success
    case failure(error: Error)
}

/// Обработчик результата операции с опциями данных пользователя
typealias UserDataOptionsOperationHandler = (UserDataOptionsOperationResult) -> Void

/// Результат запроса значений автокомплита для опции
///
/// - success: успех, связанное значение - значения автокомплита
/// - failure: провал, связанное значение - ошибка
enum UserDataOptionsAutocompleteResult {
    case success(values: [UserOptionsAutocompleteValue])
    case failure(error: Error)
}

/// Обработчик результата запроса значений автокомплита для опции
typealias UserDataOptionsAutocompleteHandler = (UserDataOptionsAutocompleteResult) -> Void

/// Сервис для работы с опциями данных пользователя
/// Опция в данных пользователя - типизированная единица данных пользователя, содержит номер документа/счетчика воды/другое
protocol UserDataOptionsService {
    
    /// Есть ли соединение с сервером
    var hasConnectionToServer: Bool { get }
    
    /// Текущий пользователь
    var currentUser: User { get }
    
    /// Получить опции данных текущего пользователя
    ///
    /// - Parameters:
    ///   - slug: Slug группы данных пользователя
    ///   - completion: обработчик результата запроса списка опций данных пользователя
    func obtainUserDataOptions(for slug: String, completion: @escaping UserDataOptionsObtainHandler)
    
    /// Добавить опцию данных текущего пользователя
    ///
    /// - Parameters:
    ///   - type: тип опции
    ///   - value: значение опции
    ///   - completion: обработчик результата операции с опциями данных пользователя
    func addUserDataOption(with type: String, and value: String, completion: @escaping UserDataOptionsOperationHandler)
    
    /// Добавить группу опций данных текущего пользователя
    ///
    /// - Parameters:
    ///   - type: тип группы
    ///   - alias: название (алиас) группы (если есть)
    ///   - values: значения опций группы
    ///   - completion: обработчик результата операции с опциями данных пользователя
    func addUserDataOptionGroup(with type: String, alias: String?, and values: [UserOption], completion: @escaping UserDataOptionsOperationHandler)
    
    /// Изменить опцию данных текущего пользователя
    ///
    /// - Parameters:
    ///   - optionId: идентификатор опции
    ///   - optionType: тип опции
    ///   - value: значение опции
    ///   - completion: обработчик результата операции с опциями данных пользователя
    func editUserDataOption(with optionId: String, optionType: String, bySetting value: String, completion: @escaping UserDataOptionsOperationHandler)
    
    /// Изменить группу опций данных текущего пользователя
    ///
    /// - Parameters:
    ///   - groupId: идентификатор группы опций
    ///   - groupType: тип группы опций
    ///   - alias: изменение названия (алиаса) группы опций (если есть)
    ///   - options: изменения опций в группе
    ///   - completion: обработчик результата операции с опциями данных пользователя
    func editUserDataOptionGroup(with groupId: String, groupType: String, byChanging alias: UserOptionChange?, and options: [UserOptionChange], completion: @escaping UserDataOptionsOperationHandler)
    
    /// Удалить опцию данных текущего пользователя
    ///
    /// - Parameters:
    ///   - optionId: идентификатор опции
    ///   - optionType: тип опции
    ///   - completion: обработчик результата операции с опциями данных пользователя
    func deleteUserDataOption(with optionId: String, optionType: String, completion: @escaping UserDataOptionsOperationHandler)
    
    /// Удалить группу опций данных текущего пользователя
    ///
    /// - Parameters:
    ///   - groupId: идентификатор группы опций
    ///   - groupType: тип группы опций
    ///   - completion: обработчик результата операции с опциями данных пользователя
    func deleteUserDataOptionGroup(with groupId: String, groupType: String, completion: @escaping UserDataOptionsOperationHandler)
    
    /// Запросить автокомплит для опции данных текущего пользователя
    ///
    /// - Parameters:
    ///   - optionType: тип опции
    ///   - searchString: строка поиска
    ///   - completion: обработчик результата запроса значений автокомплита для опции
    /// - Returns: запрос, который отправляется при вызове данного метода
    func obtainUserDataOptionAutocomplete(for optionType: String, and searchString: String, completion: @escaping UserDataOptionsAutocompleteHandler) -> HTTPRequest
}

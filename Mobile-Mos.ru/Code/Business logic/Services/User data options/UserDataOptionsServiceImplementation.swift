//
//  UserDataOptionsServiceImplementation.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 20.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

class UserDataOptionsServiceImplementation: BaseNetworkService, UserDataOptionsService {
    
    /// Типы сообщений в сокете
    ///
    /// - getUserProfileGroup: запросить список опций данных текущего пользователя
    /// - getUserProfileGroupResponse: получить список опций данных текущего пользователя
    /// - addUserProfileOption: добавить опцию данных текущего пользователя
    /// - addUserProfileOptionResponse: получить результат добавления опции
    /// - addUserProfileOptionsGroup: добавить группу опций данных текущего пользователя
    /// - addUserProfileOptionsGroupResponse: получить результат добавления группы опций
    /// - editUserProfileOption: изменить опцию данных текущего пользователя
    /// - editUserProfileOptionResponse: получить результат изменения опции
    /// - editUserProfileOptionsGroup: изменить группу опций данных текущего пользователя
    /// - editUserProfileOptionsGroupResponse: получить результат изменения группы опций
    /// - deleteUserProfileOption: удалить опцию данных текущего пользователя
    /// - deleteUserProfileOptionResponse: получить результат удаления опции
    /// - deleteUserProfileOptionsGroup: удалить группу опций данных текущего пользователя
    /// - deleteUserProfileOptionsGroupResponse: получить результат удаления группы опций данных текущего пользователя
    fileprivate enum Event: String {
        case getUserProfileGroup
        case getUserProfileGroupResponse
        case addUserProfileOption
        case addUserProfileOptionResponse
        case addUserProfileOptionsGroup
        case addUserProfileOptionsGroupResponse
        case editUserProfileOption
        case editUserProfileOptionResponse
        case editUserProfileOptionsGroup
        case editUserProfileOptionsGroupResponse
        case deleteUserProfileOption
        case deleteUserProfileOptionResponse
        case deleteUserProfileOptionsGroup
        case deleteUserProfileOptionsGroupResponse
    }
    
    //MARK: - Статические компоненты
    
    // Обработчики, добавляемые экземплярами сервиса по вызову методов в неограниченном кол-ве
    
    fileprivate static var userOptionsRequestsQueue = ThreadSafeSerialEventQueue<UserDataOptionsObtainResult>()
    fileprivate static var userOptionAddRequestsQueue = ThreadSafeSerialEventQueue<UserDataOptionsOperationResult>()
    fileprivate static var userOptionsGroupAddRequestsQueue = ThreadSafeSerialEventQueue<UserDataOptionsOperationResult>()
    fileprivate static var userOptionEditRequestsQueue = ThreadSafeSerialEventQueue<UserDataOptionsOperationResult>()
    fileprivate static var userOptionsGroupEditRequestsQueue = ThreadSafeSerialEventQueue<UserDataOptionsOperationResult>()
    fileprivate static var userOptionDeleteRequestsQueue = ThreadSafeSerialEventQueue<UserDataOptionsOperationResult>()
    fileprivate static var userOptionsGroupDeleteRequestsQueue = ThreadSafeSerialEventQueue<UserDataOptionsOperationResult>()
    
    static var currentUserManager: CurrentUserManager!
    static var sharedJsonMapper: JsonResourceMapper!
    
    static var sharedSocket: Socket! {
        
        didSet {
            
            // Получение опций данных пользователя
            
            sharedSocket.onAsync(event: Event.getUserProfileGroupResponse.rawValue) { socketData, _ in
                
                guard let handler = userOptionsRequestsQueue.dequeue()?.handler else { return }
                
                var result: UserDataOptionsObtainResult
                
                defer {
                    DispatchQueue.main.async(execute: {
                        handler(result)
                    })
                }
                
                guard let json = (socketData as? [NSDictionary])?.first else {
                    result = .failure(error: HTTPRequestError.jsonParsingNoResponseDataError)
                    return
                }
                
                let noErrorCode = UserOptionsObtainResponse.ErrorCode.noError.rawValue
                let response = sharedJsonMapper.mapJson(json, toResourceOfType: UserOptionsObtainResponse.self)
                
                if response.optionsData != nil && response.errorCode == noErrorCode {
                    result = .success(options: response.optionsData!)
                }
                else {
                    let userInfo: [String : Any] = response.errorMessage != nil ? [NSLocalizedDescriptionKey: response.errorMessage!] : [:]
                    let error = NSError(domain: NSURLErrorDomain, code: response.errorCode, userInfo: userInfo)
                    result = .failure(error: error)
                }
            }
            
            // Получение результата добавления опции данных пользователя
            
            sharedSocket.onAsync(event: Event.addUserProfileOptionResponse.rawValue) { socketData, _ in
                handleUserDataOptionsOperationResult(from: userOptionAddRequestsQueue, with: socketData)
            }
            
            // Получение результата добавления группы опций данных пользователя
            
            sharedSocket.onAsync(event: Event.addUserProfileOptionsGroupResponse.rawValue) { socketData, _ in
                handleUserDataOptionsOperationResult(from: userOptionsGroupAddRequestsQueue, with: socketData)
            }
            
            // Получение результата изменения опции данных пользователя
            
            sharedSocket.onAsync(event: Event.editUserProfileOptionResponse.rawValue) { socketData, _ in
                handleUserDataOptionsOperationResult(from: userOptionEditRequestsQueue, with: socketData)
            }
            
            // Получение результата изменения группы опций данных пользователя
            
            sharedSocket.onAsync(event: Event.editUserProfileOptionsGroupResponse.rawValue) { socketData, _ in
                handleUserDataOptionsOperationResult(from: userOptionsGroupEditRequestsQueue, with: socketData)
            }
            
            // Получение результата удаления опции данных пользователя
            
            sharedSocket.onAsync(event: Event.deleteUserProfileOptionResponse.rawValue) { socketData, _ in
                handleUserDataOptionsOperationResult(from: userOptionDeleteRequestsQueue, with: socketData)
            }
            
            // Получение результата удаления группы опций данных пользователя
            
            sharedSocket.onAsync(event: Event.deleteUserProfileOptionsGroupResponse.rawValue) { socketData, _ in
                handleUserDataOptionsOperationResult(from: userOptionsGroupDeleteRequestsQueue, with: socketData)
            }
            
            // Восстановление соединения (перезапуск очередей)
            
            sharedSocket.onAsync(event: SocketEvent.connect.rawValue) { _,_  in
                userOptionsRequestsQueue.resume()
                userOptionAddRequestsQueue.resume()
                userOptionsGroupAddRequestsQueue.resume()
                userOptionEditRequestsQueue.resume()
                userOptionsGroupEditRequestsQueue.resume()
                userOptionDeleteRequestsQueue.resume()
                userOptionsGroupDeleteRequestsQueue.resume()
            }
            
            // Обработка обрыва соединения (остановка очередей)
            
            sharedSocket.onAsync(event: SocketEvent.disconnect.rawValue) { _,_  in
                userOptionsRequestsQueue.pause()
                userOptionAddRequestsQueue.pause()
                userOptionsGroupAddRequestsQueue.pause()
                userOptionEditRequestsQueue.pause()
                userOptionsGroupEditRequestsQueue.pause()
                userOptionDeleteRequestsQueue.pause()
                userOptionsGroupDeleteRequestsQueue.pause()
            }
        }
    }
    
    //MARK: - Компоненты экземпляра
    
    fileprivate typealias Service = UserDataOptionsServiceImplementation
    fileprivate let autocompleteLimit = 10
    
    /// Удалить элемент (опцию/группу опций) из данных пользователя
    ///
    /// - Parameters:
    ///   - itemId: идентификатор элемента
    ///   - itemType: тип элемента
    ///   - event: событие запроса на удаление
    ///   - queue: очередь обработчиков
    ///   - completion: обработчик ответа на запрос удаления
    fileprivate func deleteUserDataItem(with itemId: String, itemType: String, event: Event, queue: ThreadSafeSerialEventQueue<UserDataOptionsOperationResult>, completion: @escaping UserDataOptionsOperationHandler) {
        
        guard let userId = currentUser.userId else {
            completion(.failure(error: UserDataOptionsErrors.userNotAuthorized))
            return
        }
        
        let request = UserOptionDeleteRequest(sessionId: currentUser.sessionId, userId: userId, itemId: itemId, itemType: itemType)
        
        let emitter = {
            Service.sharedSocket.send(event: event.rawValue, with: [request.serializeToJson()])
        }
        
        let eventBlock = SerialEventBlock(emitter: emitter, handler: completion)
        queue.enqueue(element: eventBlock)
    }
    
    /// Обработать результат операции с опциями пользователя
    ///
    /// - Parameters:
    ///   - queue: очередь обработчиков
    ///   - socketData: данные из сокета
    fileprivate static func handleUserDataOptionsOperationResult(from queue: ThreadSafeSerialEventQueue<UserDataOptionsOperationResult>, with socketData: [Any]) {
        
        guard let handler = queue.dequeue()?.handler else { return }
        
        var result: UserDataOptionsOperationResult
        
        defer {
            DispatchQueue.main.async(execute: {
                handler(result)
            })
        }
        
        guard let json = (socketData as? [NSDictionary])?.first else {
            result = .failure(error: HTTPRequestError.jsonParsingNoResponseDataError)
            return
        }
        
        let noErrorCode = UserOptionsObtainResponse.ErrorCode.noError.rawValue
        let response = sharedJsonMapper.mapJson(json, toResourceOfType: UserOptionsOperationResponse.self)
        
        if response.errorCode == noErrorCode {
            result = .success
        }
        else {
            let userInfo: [String : Any] = response.errorMessage != nil ? [NSLocalizedDescriptionKey: response.errorMessage!] : [:]
            let error = NSError(domain: NSURLErrorDomain, code: response.errorCode, userInfo: userInfo)
            result = .failure(error: error)
        }
    }
    
    //MARK: - UserInfoAndSettingsService
    
    var hasConnectionToServer: Bool {
        return Service.sharedSocket.isConnected
    }
    
    var currentUser: User {
        return Service.currentUserManager.currentUser
    }
    
    func obtainUserDataOptions(for slug: String, completion: @escaping UserDataOptionsObtainHandler) {
        
        guard let userId = currentUser.userId else {
            completion(.failure(error: UserDataOptionsErrors.userNotAuthorized))
            return
        }
        
        let request = UserOptionsObtainRequest(sessionId: currentUser.sessionId, userId: userId, slug: slug)
        
        let emitter = {
            Service.sharedSocket.send(event: Event.getUserProfileGroup.rawValue, with: [request.serializeToJson()])
        }
        
        let eventBlock = SerialEventBlock(emitter: emitter, handler: completion)
        Service.userOptionsRequestsQueue.enqueue(element: eventBlock)
    }
    
    func addUserDataOption(with type: String, and value: String, completion: @escaping UserDataOptionsOperationHandler) {
        
        guard let userId = currentUser.userId else {
            completion(.failure(error: UserDataOptionsErrors.userNotAuthorized))
            return
        }
        
        let request = UserOptionAddRequest(sessionId: currentUser.sessionId, userId: userId, optionType: type, optionValue: value)
        
        let emitter = {
            Service.sharedSocket.send(event: Event.addUserProfileOption.rawValue, with: [request.serializeToJson()])
        }
        
        let eventBlock = SerialEventBlock(emitter: emitter, handler: completion)
        Service.userOptionAddRequestsQueue.enqueue(element: eventBlock)
    }
    
    func addUserDataOptionGroup(with type: String, alias: String?, and values: [UserOption], completion: @escaping UserDataOptionsOperationHandler) {
        
        guard let userId = currentUser.userId else {
            completion(.failure(error: UserDataOptionsErrors.userNotAuthorized))
            return
        }
        
        let request = UserOptionGroupAddRequest(sessionId: currentUser.sessionId, userId: userId, groupType: type, groupAlias: alias, options: values)
        
        let emitter = {
            Service.sharedSocket.send(event: Event.addUserProfileOptionsGroup.rawValue, with: [request.serializeToJson()])
        }
        
        let eventBlock = SerialEventBlock(emitter: emitter, handler: completion)
        Service.userOptionsGroupAddRequestsQueue.enqueue(element: eventBlock)
    }
    
    func editUserDataOption(with optionId: String, optionType: String, bySetting value: String, completion: @escaping UserDataOptionsOperationHandler) {
        
        guard let userId = currentUser.userId else {
            completion(.failure(error: UserDataOptionsErrors.userNotAuthorized))
            return
        }
        
        let request = UserOptionEditRequest(sessionId: currentUser.sessionId, userId: userId, optionId: optionId, optionType: optionType, optionValue: value)
        
        let emitter = {
            Service.sharedSocket.send(event: Event.editUserProfileOption.rawValue, with: [request.serializeToJson()])
        }
        
        let eventBlock = SerialEventBlock(emitter: emitter, handler: completion)
        Service.userOptionEditRequestsQueue.enqueue(element: eventBlock)
    }
    
    func editUserDataOptionGroup(with groupId: String, groupType: String, byChanging alias: UserOptionChange?, and options: [UserOptionChange], completion: @escaping UserDataOptionsOperationHandler) {
        
        guard let userId = currentUser.userId else {
            completion(.failure(error: UserDataOptionsErrors.userNotAuthorized))
            return
        }
        
        let request = UserOptionGroupEditRequest(sessionId: currentUser.sessionId, userId: userId, groupId: groupId, groupType: groupType, groupAlias: alias, optionChanges: options)
        
        let emitter = {
            Service.sharedSocket.send(event: Event.editUserProfileOptionsGroup.rawValue, with: [request.serializeToJson()])
        }
        
        let eventBlock = SerialEventBlock(emitter: emitter, handler: completion)
        Service.userOptionsGroupEditRequestsQueue.enqueue(element: eventBlock)
    }
    
    func deleteUserDataOption(with optionId: String, optionType: String, completion: @escaping UserDataOptionsOperationHandler) {
        deleteUserDataItem(with: optionId, itemType: optionType, event: Event.deleteUserProfileOption, queue: Service.userOptionDeleteRequestsQueue, completion: completion)
    }
    
    func deleteUserDataOptionGroup(with groupId: String, groupType: String, completion: @escaping UserDataOptionsOperationHandler) {
        deleteUserDataItem(with: groupId, itemType: groupType, event: Event.deleteUserProfileOptionsGroup, queue: Service.userOptionsGroupDeleteRequestsQueue, completion: completion)
    }
    
    func obtainUserDataOptionAutocomplete(for optionType: String, and searchString: String, completion: @escaping UserDataOptionsAutocompleteHandler) -> HTTPRequest {
        
        let urlString = "get_profile_autocomplete"
        
        var configuration = HTTPRequestConfiguration(withMethod: HTTPMethod.GET.rawValue, andUrlString: urlString)
        configuration.parameters = ["search_string" : searchString,
                                    "option_type" : optionType,
                                    "limit" : autocompleteLimit]
        var urlRequest = requestBuilder.request(with: configuration)
        
        return requestManager.perform(dataRequest: urlRequest)
            .validate(acceptableStatusCodes: [200])
            .validate(acceptableContentTypes: [HTTPRequestContentType.json.rawValue])
            .responseJson(responseHandler: {[weak self] (parsingResult) in
                
                guard let strongSelf = self else {return}
                
                var result: UserDataOptionsAutocompleteResult
                
                defer {
                    DispatchQueue.main.async {
                        completion(result)
                    }
                }
                
                switch parsingResult {
                    
                case .success(let json):
                    
                    guard let responseJson = json as? NSDictionary else {
                        result = .failure(error:HTTPRequestError.jsonMappingNotExpectedStructure)
                        return
                    }
                    
                    let response = strongSelf.responseMapper.mapJson(responseJson, toResourceOfType: UserOptionsAutocompleteResponse.self)
                    
                    typealias ErrorCode = UserOptionsAutocompleteResponse.ErrorCode
                    guard response.errorCode == ErrorCode.noError.rawValue else {
                        
                        let userInfo: [String : Any] = response.errorMessage != nil ? [NSLocalizedDescriptionKey: response.errorMessage!] : [:]
                        let error = NSError(domain: NSURLErrorDomain, code: response.errorCode, userInfo: userInfo)
                        
                        result = .failure(error: error)
                        return
                    }
                    
                    result = .success(values: response.values)
                    
                case .failure(let error):
                    result = .failure(error: error)
                }
            })
    }
}

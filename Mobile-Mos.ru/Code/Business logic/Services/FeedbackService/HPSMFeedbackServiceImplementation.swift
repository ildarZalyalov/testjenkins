//
//  HPSMFeedbackServiceImplementation.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 13.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

class HPSMFeedbackServiceImplementation: BaseNetworkService, FeedbackService {
    
    var xmlStringsBuilder: XmlStringsBuilder!
    
    func sendFeedback(with feedbackInfo: FeedbackInfo, completion: @escaping FeedbackResultHandler) -> HTTPRequest {
        
        var configuration = HTTPRequestConfiguration(withMethod: HTTPMethod.POST.rawValue, andUrlString: requestBuilder.baseURLString)
        
        var urlRequest = requestBuilder.request(with: configuration)
        
        let xmlString  =  xmlStringsBuilder.buildHPSMCreateRequestXmlString(from: feedbackInfo)
        
        urlRequest.httpBody = xmlString.data(using: .utf8, allowLossyConversion: false)
        
        urlRequest.addValue("text/xml; charset=utf-8", forHTTPHeaderField: "Content-Type")
        urlRequest.addValue("\(xmlString.count)", forHTTPHeaderField: "Content-Length")
        urlRequest.addValue("Create", forHTTPHeaderField: "SOAPAction")
        
        addBaseAuthorization(to: &urlRequest)
        
        return requestManager.perform(dataRequest: urlRequest)
            .validate(acceptableStatusCodes: [200])
            .response(responseHandler: { (response) in
                
                var result: FeedbackMultipleResult
                
                defer {
                    DispatchQueue.main.async {
                        completion(result)
                    }
                }
                
                if let code = response.httpUrlResponse?.statusCode, code == 200 {
                    result = .success()
                }
                else {
                    
                    result = .failure(error: response.error ?? HTTPRequestError.statusCodeNotValid)
                }
                
            })
    }
    
    fileprivate func addBaseAuthorization(to urlRequest: inout URLRequest) {
        
        let username = "MyMoscowMobile_SOAP"
        let password = "R5tqvExP"
        let loginString = String(format: "%@:%@", username, password)
        let loginData = loginString.data(using: .utf8)!
        let base64LoginString = loginData.base64EncodedString()
        
        urlRequest.setValue("Basic \(base64LoginString)", forHTTPHeaderField: "Authorization")
    }
}

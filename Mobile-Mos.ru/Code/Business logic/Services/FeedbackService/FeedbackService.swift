//
//  FeedbackService.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 13.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Результат запроса на отправление обращения
///
/// - success: успех
/// - failure: провал с ошибкой
enum FeedbackMultipleResult {
    case success()
    case failure(error: Error)
}

/// Обработчик результата запроса
typealias FeedbackResultHandler = (FeedbackMultipleResult) -> Void

/// Сервис для отправки обращений
protocol FeedbackService {
    
    /// Отправить обращение (фид-бэк)
    ///
    /// - Parameters:
    ///   - email: email отправителя
    ///   - feedBackDescription: текст обращения
    ///   - completion: блок - обработчик запроса
    /// - Returns: http запрос
    @discardableResult
    func sendFeedback(with feedbackInfo: FeedbackInfo, completion: @escaping FeedbackResultHandler) -> HTTPRequest
}

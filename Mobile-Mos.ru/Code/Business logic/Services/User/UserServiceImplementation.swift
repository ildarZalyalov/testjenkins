//
//  UserServiceImplementation.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 30.06.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class UserServiceImplementation: UserService {
    
    fileprivate enum Event: String {
        case loginUser
        case loginUserResponse
        case logoutUser
        case getRegisterCode
        case getRegisterCodeResponse
        case registerNewUser
        case registerNewUserResponse
        case sendRecoveryCode
        case sendRecoveryCodeResponse
        case setNewPassword
        case setNewPasswordResponse
    }
    
    //MARK: - Статические компоненты
    
    // Обработчики, добавляемые экземплярами сервиса по вызову методов в неограниченном кол-ве
    
    fileprivate static var loginRequestsQueue = ThreadSafeSerialEventQueue<UserLoginResult>()
    fileprivate static var setNewPasswordRequestsQueue = ThreadSafeSerialEventQueue<UserNewPasswordResult>()
    fileprivate static var sendRecoveryCodeRequestsQueue = ThreadSafeSerialEventQueue<UserObtainSmsCodeResult>()
    fileprivate static var registerRequestQueue = ThreadSafeSerialEventQueue<UserRegistrationResult>()
    fileprivate static var getRegisterCodeQueue = ThreadSafeSerialEventQueue<UserObtainSmsCodeResult>()
    
    static var currentUserManager: CurrentUserManager!
    static var sharedJsonMapper: JsonResourceMapper!
    static var userDefaults: UserDefaults!
    
    static var sharedSocket: Socket! {
        
        didSet {
            
            // Ответ на попытку логина
            
            sharedSocket.onAsync(event: Event.loginUserResponse.rawValue) { socketData, _ in
                
                guard let handler = loginRequestsQueue.dequeue()?.handler else { return }
                
                var result: UserLoginResult
                
                defer {
                    DispatchQueue.main.async(execute: {
                        handler(result)
                    })
                }
                
                guard let json = (socketData as? [NSDictionary])?.first else {
                    result = .failure(error: HTTPRequestError.jsonParsingNoResponseDataError)
                    return
                }
                
                let noErrorCode = UserLoginResponse.ErrorCode.noError.rawValue
                let response = sharedJsonMapper.mapJson(json, toResourceOfType: UserLoginResponse.self)
                
                if response.userId != nil && response.errorCode == noErrorCode {
                    
                    var user = User()
                    user.sessionId = currentUserManager.currentUser.sessionId
                    user.userId = response.userId
                    
                    result = .success(user: user)
                }
                else {
                    let error = NSError(domain: NSURLErrorDomain, code: response.errorCode, userInfo: [NSLocalizedDescriptionKey: response.errorMessage])
                    result = .failure(error: error)
                }
            }
            
            // Ответ на попытку регистрации
            
            sharedSocket.onAsync(event: Event.registerNewUserResponse.rawValue) { (socketData, _) in

                guard let handler = registerRequestQueue.dequeue()?.handler else { return }

                var result: UserRegistrationResult

                defer {
                    DispatchQueue.main.async(execute: {
                        handler(result)
                    })
                }

                guard let json = (socketData as? [NSDictionary])?.first else {
                    result = .failure(error: HTTPRequestError.jsonParsingNoResponseDataError)
                    return
                }
                
                let noErrorCode = UserLoginResponse.ErrorCode.noError.rawValue
                let response = sharedJsonMapper.mapJson(json, toResourceOfType: UserRegistrationResponse.self)
            
                if response.userId != nil && response.errorCode == noErrorCode {
                    
                    var user = User()
                    user.sessionId = currentUserManager.currentUser.sessionId
                    user.userId = response.userId
                    
                    result = .success(user: user)
                }
                else {
                    let errorCode = response.errorCode ?? 0
                    let errorMessage = response.errorMessage ?? ""
                    
                    let error = NSError(domain: NSURLErrorDomain, code: errorCode, userInfo: [NSLocalizedDescriptionKey: errorMessage])
                    
                    result = .failure(error: error)
                }
            }
            
            // Ответ на попытку смена пароля пользователя
            
            sharedSocket.onAsync(event: Event.setNewPasswordResponse.rawValue) { socketData, _ in
                
                guard let handler = setNewPasswordRequestsQueue.dequeue()?.handler else { return }
                
                var result: UserNewPasswordResult
                
                defer {
                    DispatchQueue.main.async(execute: {
                        handler(result)
                    })
                }
                
                guard let json = (socketData as? [NSDictionary])?.first else {
                    result = .failure(error: HTTPRequestError.jsonParsingNoResponseDataError)
                    return
                }
                
                let noErrorCode = UserLoginResponse.ErrorCode.noError.rawValue
                let response = sharedJsonMapper.mapJson(json, toResourceOfType: UserSetNewPasswordResponse.self)
                
                if response.errorCode == noErrorCode {
                    result = .success()
                }
                else {
                    let error = NSError(domain: NSURLErrorDomain, code: response.errorCode, userInfo: [NSLocalizedDescriptionKey: response.errorMessage])
                    result = .failure(error: error)
                }
            }
            
            // Ответ на попытку получения SMS кода
            
            sharedSocket.onAsync(event: Event.sendRecoveryCodeResponse.rawValue) { socketData, _ in
                
                guard let handler = sendRecoveryCodeRequestsQueue.dequeue()?.handler else { return }
                
                var result: UserObtainSmsCodeResult
                
                defer {
                    DispatchQueue.main.async(execute: {
                        handler(result)
                    })
                }
                
                guard let json = (socketData as? [NSDictionary])?.first else {
                    result = .failure(error: HTTPRequestError.jsonParsingNoResponseDataError)
                    return
                }
                
                let noErrorCode = UserLoginResponse.ErrorCode.noError.rawValue
                let response = sharedJsonMapper.mapJson(json, toResourceOfType: UserSetNewPasswordResponse.self)
                
                if response.errorCode == noErrorCode {
                    result = .success(timeOut: response.timeout)
                }
                else {
                    let error = NSError(domain: NSURLErrorDomain, code: response.errorCode, userInfo: [NSLocalizedDescriptionKey: response.errorMessage])
                    result = .failure(error: error)
                }
            }
            
            // Ответ на попытку получения SMS кода при регистрации
            
            sharedSocket.onAsync(event: Event.getRegisterCodeResponse.rawValue) { socketData, _ in
                
                guard let handler = getRegisterCodeQueue.dequeue()?.handler else { return }
                
                var result: UserObtainSmsCodeResult
                
                defer {
                    DispatchQueue.main.async(execute: {
                        handler(result)
                    })
                }
                
                guard let json = (socketData as? [NSDictionary])?.first else {
                    result = .failure(error: HTTPRequestError.jsonParsingNoResponseDataError)
                    return
                }
                
                let noErrorCode = UserLoginResponse.ErrorCode.noError.rawValue
                let response = sharedJsonMapper.mapJson(json, toResourceOfType: UserPasswordForgotResponse.self)
                
                if response.errorCode == noErrorCode {
                    result = .success(timeOut: response.timeout)
                }
                else {
                    let error = NSError(domain: NSURLErrorDomain, code: response.errorCode, userInfo: [NSLocalizedDescriptionKey: response.errorMessage])
                    result = .failure(error: error)
                }
            }
            
            // Восстановление соединения (перезапуск очередей)
            
            sharedSocket.onAsync(event: SocketEvent.connect.rawValue) { _,_  in
                loginRequestsQueue.resume()
                setNewPasswordRequestsQueue.resume()
                sendRecoveryCodeRequestsQueue.resume()
                registerRequestQueue.resume()
                getRegisterCodeQueue.resume()
            }
            
            // Обработка обрыва соединения (остановка очередей)
            
            sharedSocket.onAsync(event: SocketEvent.disconnect.rawValue) { _,_  in
                loginRequestsQueue.pause()
                setNewPasswordRequestsQueue.pause()
                sendRecoveryCodeRequestsQueue.pause()
                registerRequestQueue.pause()
                getRegisterCodeQueue.pause()
            }
        }
    }
    
    //MARK: - Компоненты экземпляра
    
    fileprivate typealias Service = UserServiceImplementation
    
    let logoutTimeout: Double = 15
    let getRegisterCodeTimeout: Double = 10
    let registerUserTimeout: Double = 15
    
    var notificationCenter: NotificationCenter!
    
    //MARK: - UserService
    
    var hasConnectionToServer: Bool {
        return Service.sharedSocket.isConnected
    }
    
    var currentUser: User {
        return Service.currentUserManager.currentUser
    }

    func login(with login: String,
               and password: String,
               beforeSuccess: UserLoginOrLogoutBeforeSuccessHandler?,
               completion: @escaping UserLoginResultHandler) {
        
        let request = UserLoginRequest(sessionId: currentUser.sessionId,
                                       phone: login,
                                       password: password)
        
        let emitter = {
            Service.sharedSocket.send(event: Event.loginUser.rawValue, with: [request.serializeToJson()])
        }
        
        let handler: UserLoginResultHandler = { [weak self] loginResult in
            
            guard let strongSelf = self else { return }
            
            defer {
                completion(loginResult)
            }
            
            switch loginResult {
                
            case .success(var user):
                
                user.login = login
                user.password = password
                
                beforeSuccess?(user)
                
                Service.currentUserManager.switchCurrentUser(to: user)
                strongSelf.notificationCenter.post(name: .UserLoginStateDidChange, object: nil)
                
            default:
                return
            }
        }
        
        let eventBlock = SerialEventBlock(emitter: emitter, handler: handler)
        Service.loginRequestsQueue.enqueue(element: eventBlock)
    }
    
    func logout(beforeSuccess: UserLoginOrLogoutBeforeSuccessHandler?,
                completion: @escaping UserLogoutResultHandler) {
        
        guard let userId = currentUser.userId else {
            completion(false)
            return
        }
        
        let unauthorizedUser = User()
        let event = Event.logoutUser.rawValue
        let requestJson = UserLogoutRequest(sessionId: currentUser.sessionId,
                                            newSessionId: unauthorizedUser.sessionId,
                                            userId: userId).serializeToJson()
        
        Service.sharedSocket.send(event: event, with: [requestJson], timingOutAfter: logoutTimeout) {
            [weak self] timedOut, parameters in
            
            guard let strongSelf = self else { return }
            
            var logoutSuccess: Bool = false
            
            if !timedOut, let success = parameters.first as? Bool {
                logoutSuccess = success
            }
            
            if logoutSuccess {
                beforeSuccess?(unauthorizedUser)
                Service.currentUserManager.switchCurrentUser(to: unauthorizedUser)
                strongSelf.notificationCenter.post(name: .UserLoginStateDidChange, object: nil)
            }
        
            completion(logoutSuccess)
        }
    }
    
    func registerNewUser(with phone: String,
                         password: String,
                         code: Int,
                         beforeSuccess: UserLoginOrLogoutBeforeSuccessHandler?,
                         completion: @escaping UserRegistrationResultHandler) {
        
        let request = UserRegisterRequest(sessionId: currentUser.sessionId, phone: phone, password: password, code: code)
        
        let emitter = {
            Service.sharedSocket.send(event: Event.registerNewUser.rawValue, with: [request.serializeToJson()])
        }
        
        let handler: UserRegistrationResultHandler = { [weak self] registrationResult in
            
            guard let strongSelf = self else { return }
            
            defer {
                DispatchQueue.main.async(execute: {
                    completion(registrationResult)
                })
            }
            
            switch registrationResult {
                
            case .success(var user):
                
                user.login = phone
                user.password = password
                
                beforeSuccess?(user)
                
                Service.currentUserManager.switchCurrentUser(to: user)
                strongSelf.notificationCenter.post(name: .UserLoginStateDidChange, object: nil)
                
            default:
                return
            }
        }
    
        let eventBlock = SerialEventBlock(emitter: emitter, handler: handler)
        Service.registerRequestQueue.enqueue(element: eventBlock)
    }
    
    func obtainUserRegisterCode(with phoneNumber: String, completion: @escaping UserGetCodeResultHandler) {
        
        let request = UserGetPhoneCode(sessionId: currentUser.sessionId, phone: phoneNumber)
        
        let emitter = {
            Service.sharedSocket.send(event: Event.getRegisterCode.rawValue, with: [request.serializeToJson()])
        }
        
        let handler: UserGetCodeResultHandler = { result in
            
            completion(result)
        }
        
        let eventBlock = SerialEventBlock(emitter: emitter, handler: handler)
        Service.getRegisterCodeQueue.enqueue(element: eventBlock)
    }
    
    func obtainUserRecoveryCode(with phoneNumber: String, completion: @escaping UserGetCodeResultHandler) {
        
        let request = UserGetPhoneCode(sessionId: currentUser.sessionId, phone: phoneNumber)
        
        let emitter = {
            Service.sharedSocket.send(event: Event.sendRecoveryCode.rawValue, with: [request.serializeToJson()])
        }
        
        let handler: UserGetCodeResultHandler = { result in
            
            completion(result)
        }
        
        let eventBlock = SerialEventBlock(emitter: emitter, handler: handler)
        Service.sendRecoveryCodeRequestsQueue.enqueue(element: eventBlock)
    }
    
    func setNewPassword(with code: String, newPassword: String, completion: @escaping UserSetNewPasswordHandler) {
        
        let request = UserNewPasswordRequest(sessionId: currentUser.sessionId, code: code, newPassword: newPassword)
        
        let emitter = {
            Service.sharedSocket.send(event: Event.setNewPassword.rawValue, with: [request.serializeToJson()])
        }
        
        let handler: UserSetNewPasswordHandler = { result in
            
            DispatchQueue.main.async(execute: {
                completion(result)
            })
        }
       
        let eventBlock = SerialEventBlock(emitter: emitter, handler: handler)
        Service.setNewPasswordRequestsQueue.enqueue(element: eventBlock)
    }
}

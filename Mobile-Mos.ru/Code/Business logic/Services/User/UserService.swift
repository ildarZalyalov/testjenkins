//
//  UserService.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 30.06.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

extension NSNotification.Name {
    
    /// Нотификация об изменении статуса авторизации пользователя, вызывается на главном потоке
    static let UserLoginStateDidChange = NSNotification.Name("UserLoginStateDidChange")
}

/// Результат запроса на логин
///
/// - success: успех, связанное значение - авторизовавшийся пользователь
/// - failure: провал, связанное значение - ошибка
enum UserLoginResult {
    case success(user: User)
    case failure(error: Error)
}

/// Ошибки логина
///
/// - wrongLoginOrPassword: неправильный логин или пароль
enum UserLoginResultErrors: Error {
    case wrongLoginOrPassword
}

/// Ошибка запроса смс кода
///
/// - recoveryPasswordTimeNotCompleted: время жизни предыдущей смс еще не истекло
/// - limitForSmsCode: ограничение кол-во запросов на смс
enum UserPasswordRecoverySmsCodeErrors: Error {
    
    case incorrectCode
    case recoveryPasswordTimeNotCompleted
    case limitForSmsCode
    
    var code: Int {
        switch self {
        case .incorrectCode: return 1
        case .recoveryPasswordTimeNotCompleted: return 1003
        case .limitForSmsCode: return 1002
        }
    }
}

/// Результат запроса на регистрацию
///
/// - success: успех, связанное значение - авторизовавшийся пользователь
/// - failure: провал, связанное значение - ошибка
enum UserRegistrationResult {
    case success(user: User)
    case failure(error: Error)
}

/// Результат запроса изменения пароля
///
/// - success: успех
/// - failure: провал, связанное значение - ошибка
enum UserNewPasswordResult {
    case success()
    case failure(error: Error)
}

/// Результат запроса на смс
///
/// - success: успех
/// - failure: провал, связанное значение - ошибка
enum UserObtainSmsCodeResult {
    case success(timeOut: Int)
    case failure(error: Error)
}

/// Обработчик для вызова перед получением результата успешной попытки авторизации/деавторизации, параметр - пользователь, который станет текущим после завершения авторизации/деавторизации
typealias UserLoginOrLogoutBeforeSuccessHandler = (User) -> Void

/// Обработчик результата попытки авторизации
typealias UserLoginResultHandler = (UserLoginResult) -> Void

/// Обработчик результата попытки деавторизации
typealias UserLogoutResultHandler = (Bool) -> Void

/// Обработчик результата попытки регистрации
typealias UserRegistrationResultHandler = (UserRegistrationResult) -> Void

/// Обработчик результата запроса сменя пароля пользоватея
typealias UserSetNewPasswordHandler = (UserNewPasswordResult) -> Void

/// Обработчик результата запроса смс кода пользователя
typealias UserGetCodeResultHandler = (UserObtainSmsCodeResult) -> Void

/// Сервис для работы с сущностью пользователя
protocol UserService {
    
    /// Есть ли соединение с сервером
    var hasConnectionToServer: Bool { get }
    
    /// Текущий пользователь
    var currentUser: User { get }
    
    /// Запустить попытку авторизации
    ///
    /// - Parameters:
    ///   - login: логин
    ///   - password: пароль
    ///   - beforeSuccess: обработчик для вызова перед получением результата успешной попытки авторизации
    ///   - completion: обработчик результата попытки авторизации
    func login(with login: String,
               and password: String,
               beforeSuccess: UserLoginOrLogoutBeforeSuccessHandler?,
               completion: @escaping UserLoginResultHandler)
    
    /// Запустить попытку деавторизации
    ///
    /// - Parameters:
    ///   - beforeSuccess: обработчик для вызова перед получением результата успешной попытки деавторизации
    ///   - completion: обработчик результата попытки деавторизации
    func logout(beforeSuccess: UserLoginOrLogoutBeforeSuccessHandler?,
                completion: @escaping UserLogoutResultHandler)
    
    /// Запустить попытку регистрации
    ///
    /// - Parameters:
    ///   - phone: телефон пользователя
    ///   - password: пароль пользователя
    ///   - code: код пользователя
    ///   - beforeSuccess: обработчик для вызова перед получением результата успешной попытки регистрации
    ///   - completion: обработчик результата попытки авторизации
    func registerNewUser(with phone: String,
                         password: String,
                         code: Int,
                         beforeSuccess: UserLoginOrLogoutBeforeSuccessHandler?,
                         completion: @escaping UserRegistrationResultHandler)
    
    /// Запросить код для регистрации по номеру телефона пользователя
    ///
    /// - Parameter phoneNumber: номер телефона пользователя
    ///   - completion: обработчик результата получения кода
    func obtainUserRegisterCode(with phoneNumber: String, completion: @escaping UserGetCodeResultHandler)
    
    /// Запросить код для восстановления пароля по номеру телефона пользователя
    ///
    /// - Parameters:
    ///   - phoneNumber: номер телефона пользователя
    ///   - completion: обработчик результата получения кода
    func obtainUserRecoveryCode(with phoneNumber: String, completion: @escaping UserGetCodeResultHandler)
    
    /// Задать новый пароль пользователю
    ///
    /// - Parameters:
    ///   - code: код - смс пользователя
    ///   - newPassword: новый пароль пользователя
    ///   - completion: блок - обработчик результата
    func setNewPassword(with code: String, newPassword: String, completion: @escaping UserSetNewPasswordHandler)
}

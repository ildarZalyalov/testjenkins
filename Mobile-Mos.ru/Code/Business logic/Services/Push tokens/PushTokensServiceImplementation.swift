//
//  PushTokensServiceImplementation.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 18.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class PushTokensServiceImplementation: PushTokensService {
    
    /// Типы сообщений в сокете
    ///
    /// - sendDeviceToken: отправить токен пуш-уведомлений
    fileprivate enum Event: String {
        case sendDeviceToken
    }
    
    /// Таймаут ожидания результата запроса отправки на сервер токена пуш-уведомлений
    let sendTokenTimeout: Double = 10
    
    var socket: Socket!
    var userDefaults: UserDefaults!
    
    //MARK: - PushTokensService
    
    var passedToServerToken: String? {
        
        get {
            let key = ApplicationUserDefaultsKey.remoteNotificationsPassedToServerToken.rawValue
            return userDefaults.string(forKey: key)
        }
        
        set {
            
            let key = ApplicationUserDefaultsKey.remoteNotificationsPassedToServerToken.rawValue
            
            userDefaults.set(newValue, forKey: key)
            userDefaults.synchronize()
        }
    }
    
    func sendToServer(pushToken: String, for user: User, completion: @escaping PushTokenSentToServerHandler) {
        
        let event = Event.sendDeviceToken.rawValue
        let sendTokenJson: NSDictionary = [
            "session_id": user.sessionId,
            "token": pushToken,
            "platform": "ios",
        ]
        
        socket.send(event: event, with: [sendTokenJson], timingOutAfter: sendTokenTimeout) { [weak self] timedOut, parameters in
            
            var successful = !timedOut
            
            if !timedOut {
                successful = (parameters.first as? Bool) ?? false
            }
            
            if successful {
                self?.passedToServerToken = pushToken
            }
            
            let result: PushTokenSentToServerResult = successful ? .success : .failure
            completion(result)
        }
    }
}

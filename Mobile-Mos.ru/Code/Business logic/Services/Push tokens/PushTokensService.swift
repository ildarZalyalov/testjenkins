//
//  PushTokensService.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 18.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Результат отправки токена пуш-уведомлений на сервер
///
/// - success: успех
/// - failure: облом 😕
enum PushTokenSentToServerResult {
    case success
    case failure
}

/// Обработчик отправки токена пуш-уведомлений на сервер, единственный параметр - результат отправки
typealias PushTokenSentToServerHandler = (PushTokenSentToServerResult) -> ()

/// Сервис для работы с токенами пуш-уведомлений
protocol PushTokensService {
    
    /// Токен, который уже был успешно отправлен на сервер
    var passedToServerToken: String? { get }
    
    /// Отправить токен на сервер
    ///
    /// - Parameters:
    ///   - pushToken: токен пуш-уведомлений
    ///   - user: текущий пользователь
    ///   - completion: обработчик отправки
    func sendToServer(pushToken: String, for user: User, completion: @escaping PushTokenSentToServerHandler)
}

//
//  GlobalSearchServiceImplementation.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 07.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class GlobalSearchServiceImplementation: BaseNetworkService, GlobalSearchService {
    
    let maxCountOfSearchStringsResult = 3
    var dataBaseManager: DataBaseManager!
    
    func saveSearchString(_ searchStringModel: SearchTopWords) {
        
        let fetchRequest = DataBaseFetchRequest(with: SearchTopWords.self, filteredWith: "title=%@", arguments: searchStringModel.title)
        
        do {
            let sameModels = try dataBaseManager.fetch(with: fetchRequest)
            
            try dataBaseManager.delete(models: sameModels)
            try dataBaseManager.save(models: [searchStringModel])
        }
        catch let error {
            print("Ошибка при сохранении поисковой строки: \(error.localizedDescription)")
        }
    }
    
    func obtainCashedSearchStrings(completion: @escaping (SearchTopWordsMultipleResult) -> Void) {
        
        let fetchRequest = DataBaseFetchRequest(with: SearchTopWords.self)
        
        dataBaseManager.fetchAsync(with: fetchRequest) { (fetchResult) in
            
            var result: SearchTopWordsMultipleResult
            
            defer {
                DispatchQueue.main.async {
                    completion(result)
                }
            }
            
            switch fetchResult {
            case .success(let models):
                
                var finalArray = [SearchTopWords]()
                models.reversed().enumerated().forEach{ if ($0.offset + 1) <= self.maxCountOfSearchStringsResult { finalArray.append($0.element) }}
                
                result = .success(models: finalArray)
            case .error(let error):
                result = .failure(error: error)
                print("Ошибка получения поисковых строк: \(error.localizedDescription)")
            }
        }
    }
    
    func clearCachedSearchStrings() {
        dataBaseManager.deleteAllModelsAsync(of: SearchTopWords.self, resultBlock: nil)
    }
    
    func getSearchTopWords(completion: @escaping (SearchTopWordsMultipleResult) -> Void) -> HTTPRequest {
        
        var urlString = pathToAiSuggest + topWordsEndpoint
        let finalUrlString = requestBuilder.pathFromComponents([urlString])
        
        var configuration = HTTPRequestConfiguration(withMethod: HTTPMethod.GET.rawValue, andUrlString: finalUrlString)
        
        var urlRequest = requestBuilder.request(with: configuration)
        
        return requestManager.perform(dataRequest: urlRequest)
            .validate(acceptableStatusCodes: [200])
            .validate(acceptableContentTypes: [HTTPRequestContentType.json.rawValue])
            .responseJson(responseHandler: {[weak self] (parsingResult) in
                
                guard let strongSelf = self else {return}
                
                var result: SearchTopWordsMultipleResult
                
                defer {
                    DispatchQueue.main.async {
                        completion(result)
                    }
                }
                
                switch parsingResult{
                    
                case .success(let json):
                    
                    guard let response = json as? NSDictionary else {
                        result = .failure(error: HTTPRequestError.jsonMappingNotExpectedStructure)
                        return
                    }
                    
                    guard let items = response["data"] as? [NSArray] else {
                        result = .failure(error:HTTPRequestError.jsonMappingNotExpectedStructure)
                        return
                    }
                    
                    var dictionaries:[NSDictionary] = []
                    
                    items.forEach{ dictionaries.append(["name":$0.lastObject ?? ""])}
                    
                    let newsItems = strongSelf.responseMapper.mapJsonAsArray(dictionaries, ofResourcesOfType: SearchTopWords.self)
                    
                    result = .success(models: newsItems)
                    
                case .failure(let error):
                    result = .failure(error: error)
                }
            })
        
    }
    
    func searchSuggests(with searchString: String, completion: @escaping (SearchSuggestsMultipleResult) -> Void) -> HTTPRequest {
        
        var urlString = pathToAiSuggest + suggestsEndpoint
        
        let finalUrlString = requestBuilder.pathFromComponents([urlString])
    
        var configuration = HTTPRequestConfiguration(withMethod: HTTPMethod.GET.rawValue, andUrlString: finalUrlString)
        
        var urlRequest = requestBuilder.request(with: configuration)
        urlRequest.url?.appendPathComponent(searchString)
        
        return requestManager.perform(dataRequest: urlRequest)
            .validate(acceptableStatusCodes: [200])
            .validate(acceptableContentTypes: [HTTPRequestContentType.json.rawValue])
            .responseJson(responseHandler: {[weak self] (parsingResult) in
                
                guard let strongSelf = self else {return}
                
                var result: SearchSuggestsMultipleResult
                
                defer {
                    DispatchQueue.main.async {
                        completion(result)
                    }
                }
                
                switch parsingResult{
                    
                case .success(let json):
                    
                    guard let response = json as? NSDictionary else {
                        result = .failure(error: HTTPRequestError.jsonMappingNotExpectedStructure)
                        return
                    }
                    
                    guard let items = response["data"] as? [NSDictionary] else {
                        result = .failure(error:HTTPRequestError.jsonMappingNotExpectedStructure)
                        return
                    }
                    
                    let newsItems = strongSelf.responseMapper.mapJsonAsArray(items, ofResourcesOfType: SearchSuggestsModel.self)
                    
                    result = .success(models: newsItems)
                    
                case .failure(let error):
                    result = .failure(error: error)
                }
            })
    }
    
    func searchNews(with searchString: String, completion: @escaping (SearchNewsMulitpleResult) -> Void) -> HTTPRequest {
        
        var urlString = pathToAiSearch
        
        var configuration = HTTPRequestConfiguration(withMethod: HTTPMethod.GET.rawValue, andUrlString: urlString)

        configuration.queryStringParameters = ["page" : "1", "q":searchString, "pagesize":"10", "skip_stat":"6"]

        var urlRequest = requestBuilder.request(with: configuration)
        
        return requestManager.perform(dataRequest: urlRequest)
            .validate(acceptableStatusCodes: [200])
            .validate(acceptableContentTypes: [HTTPRequestContentType.json.rawValue])
            .responseJson(responseHandler: {[weak self] (parsingResult) in

                guard let strongSelf = self else {return}

                var result: SearchNewsMulitpleResult

                defer {
                    DispatchQueue.main.async {
                        completion(result)
                    }
                }

                switch parsingResult{

                case .success(let json):

                    guard let response = json as? NSDictionary else {
                        result = .failure(error: HTTPRequestError.jsonMappingNotExpectedStructure)
                        return
                    }

                    guard let items = response["results"] as? [NSDictionary] else {
                        result = .failure(error:HTTPRequestError.jsonMappingNotExpectedStructure)
                        return
                    }

                    let newsItems = strongSelf.responseMapper.mapJsonAsArray(items, ofResourcesOfType: NewsSearchModel.self)

                    
                    result = .success(model: newsItems)

                case .failure(let error):
                    result = .failure(error: error)
                }
            })
    }
    
}

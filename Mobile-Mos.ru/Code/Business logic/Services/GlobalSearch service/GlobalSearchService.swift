//
//  GlobalSearchService.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 07.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Результат запроса на получение фичеред моделей
///
/// - success: успех, связанное значение - фичеред модель
/// - failure: провал, связанное значение - ошибка
enum FeaturedObtainMultipleResult {
    case success(model: GlobalSearchModel)
    case failure(error: Error)
}

/// Результат запроса на получение результата поиска новостей
///
/// - success: успех, связанное значение - массив новостей
/// - failure: провал, связанное значение - ошибка
enum SearchNewsMulitpleResult {
    case success(model: [NewsSearchModel])
    case failure(error: Error)
}

/// Результат запроса на получение результата топовых поисковых слов
///
/// - success: успех, связанное значение - массив поисковых слов
/// - failure: провал, связанное значение - ошибка
enum SearchTopWordsMultipleResult {
    case success(models: [SearchTopWords])
    case failure(error: Error)
}

/// Результат запроса на получение саджестов
///
/// - success: успех, связанное значение - массив саджестов
/// - failure: провал, связанное значение - ошибка
enum SearchSuggestsMultipleResult {
    case success(models: [SearchSuggestsModel])
    case failure(error: Error)
}

/// Сервис для глобального поиска
protocol GlobalSearchService {
    
    /// Получить топ поисковых слов
    ///
    /// - Parameter completion: блок - обработчик результата
    /// - Returns: http запрос
    func getSearchTopWords(completion: @escaping (SearchTopWordsMultipleResult) -> Void) -> HTTPRequest
    
    /// Получить кэш поисковых строк
    ///
    /// - Parameter completion: блок - обработчик результата
    func obtainCashedSearchStrings(completion: @escaping (SearchTopWordsMultipleResult) -> Void)
    
    /// Сохранить поисковую строку в кэш
    ///
    /// - Parameter searchStringModel: поисковая строка
    func saveSearchString(_ searchStringModel: SearchTopWords)
    
    /// Очистить кэш поисковых строк
    ///
    func clearCachedSearchStrings()
    
    /// Найти саджесты на портале mos.ru
    ///
    /// - Parameters:
    ///   - searchString: строка для поиска
    ///   - completion: блок - обработчик результата
    /// - Returns: http запрос
    func searchSuggests(with searchString: String, completion: @escaping (SearchSuggestsMultipleResult) -> Void) -> HTTPRequest
    
    /// Найти новости на портале mos.ru
    ///
    /// - Parameters:
    ///   - searchString: строка для поиска
    ///   - completion: блок - обработчик результата
    /// - Returns: http запрос
    func searchNews(with searchString: String, completion: @escaping (SearchNewsMulitpleResult) -> Void) -> HTTPRequest
}

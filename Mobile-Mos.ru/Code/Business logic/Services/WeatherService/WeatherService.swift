//
//  WeatherService.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 10.04.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Результат запроса на погоду
///
/// - success: успех - связанное значение модель погоды
/// - failure: провал - связанное значение ошибка
enum WeatherObtainMultipleResult {
    case success(weather: Weather)
    case failure(error: Error)
}

/// Позволяет получить погоду в определенном городе
protocol WeatherService {
    
    
    /// Получить текущую погоду в Москве
    ///
    /// - Parameter completion: результат запроса
    /// - Returns: http запрос
    @discardableResult
    func getCurrentWeatherInMoscow(completion: @escaping (WeatherObtainMultipleResult) -> Void) -> HTTPRequest?
}

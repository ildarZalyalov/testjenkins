//
//  WeatherServiceImplementation.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 10.04.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

class WeatherServiceImplementation: BaseNetworkService, WeatherService {
    
    func getCurrentWeatherInMoscow(completion: @escaping (WeatherObtainMultipleResult) -> Void) -> HTTPRequest? {
        
        var urlString = weatherEndpoint

        var configuration = HTTPRequestConfiguration(withMethod: HTTPMethod.GET.rawValue, andUrlString: urlString)
        
        var urlRequest = requestBuilder.request(with: configuration)
        
        return requestManager.perform(dataRequest: urlRequest)
            .validate(acceptableStatusCodes: [200])
            .validate(acceptableContentTypes: [HTTPRequestContentType.json.rawValue])
            .responseJson(responseHandler: { [weak self] (parsingResult) in
                
                guard let strongSelf = self else {return}
                
                var result: WeatherObtainMultipleResult
                
                defer {
                    DispatchQueue.main.async {
                        completion(result)
                    }
                }
                
                switch parsingResult{
                    
                case .success(let json):
                    
                    guard let item = (json as? [NSDictionary])?.first else {
                        result = .failure(error:HTTPRequestError.jsonMappingNotExpectedStructure)
                        return
                    }
                    
                    let weatherModel = strongSelf.responseMapper.mapJson(item, toResourceOfType: Weather.self)
                    
                    result = .success(weather: weatherModel)
                    
                case .failure(let error):
                    result = .failure(error: error)
                }
            })
    }
}

//
//  ServiceBuilderRelease.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 13.02.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class ServiceBuilderRelease: ServiceBuilder {
    
    /// Базовый url для сервера
    var baseUrlString: String {
        return baseUrlStringRelease
    }
    
    fileprivate(set) lazy var chatSocketConnection: Socket = self.createChatSocketConnection()
    
    init() {
        configureSharedSocketComponents()
    }
    
    //MARK: - Создаем компоненты
    
    func createChatSocketConnection() -> Socket {
        
        let url = URL(string: baseUrlString)!
        
        var configuration = SocketIOConfiguration(with: url)
        configuration.namespace = chatServerNamespace
        configuration.serverPath = chatServerPath
        configuration.connectionParametersObtainer = createChatSocketConnectionParametersObtainer()
        
        let socket = SocketIOImplementation(with: configuration, and: NotificationCenter.default)
        
        socket.connect()
        
        return socket
    }
    
    func createChatSocketConnectionParametersObtainer() -> SocketIOConnectionParametersObtainer {
        
        let currentUserManager = createCurrentUserManager()
        
        return {
            
            let currentUser = currentUserManager.currentUser
            let userIdValue: Any = currentUser.userId ?? serverNilStringLiteral
            
            return ["user_id" : userIdValue,
                    "session_id" : currentUser.sessionId]
        }
    }
    
    func createCurrentUserManager() -> CurrentUserManager {
        
        typealias UserManager = CurrentUserManagerImplementation
        
        if UserManager.dataBaseManager == nil {
            UserManager.dataBaseManager = createProtectedDatabaseManager()
        }

        return UserManager()
    }
    
    func createDatabaseManager() -> DataBaseManager {
        return RealmDataBaseManager()
    }
    
    func createProtectedDatabaseManager() -> DataBaseManager {
        return RealmDataBaseManager(shouldUseEncryptedStorage: true)
    }
    
    func configureBaseNetworkService(with service: BaseNetworkService, baseUrl: String = baseUrlStringDefault, sessionDelegate: HTTPSessionDelegate = HTTPSessionDelegate()) {
        
        service.requestBuilder = HTTPRequestBuilderDefault()
        service.requestBuilder.baseURLString = baseUrl
        service.requestBuilder.parametersEncoder = HTTPRequestParameterEncoderDefault()
        
        let urlSessionConfiguration = URLSessionConfiguration.default
        urlSessionConfiguration.timeoutIntervalForRequest = networkingTimeoutIntervalForRequest
        urlSessionConfiguration.timeoutIntervalForResource = networkingTimeoutIntervalForResource
        
        service.responseMapper = JsonResourceMapperImplementation()
        service.urlSessionConfiguration = urlSessionConfiguration
        
        service.requestManager = HTTPRequestManagerDefault(with: service.urlSessionConfiguration, and: sessionDelegate)
    }
    
    func configureSharedSocketComponents() {
        
        let messageFactory = getChatMessageFactory()
        
        typealias UserService = UserServiceImplementation
        
        UserService.sharedSocket = chatSocketConnection
        UserService.currentUserManager = createCurrentUserManager()
        UserService.sharedJsonMapper = JsonResourceMapperImplementation()
        UserService.userDefaults = UserDefaults.standard
        
        typealias UserInfoAndSettingsService = UserInfoAndSettingsServiceImplementation
        
        UserInfoAndSettingsService.sharedSocket = chatSocketConnection
        UserInfoAndSettingsService.currentUserManager = createCurrentUserManager()
        UserInfoAndSettingsService.sharedJsonMapper = JsonResourceMapperImplementation()
        UserInfoAndSettingsService.userDefaults = UserDefaults.standard
        
        typealias UserDataOptionsService = UserDataOptionsServiceImplementation
        
        UserDataOptionsService.sharedSocket = chatSocketConnection
        UserDataOptionsService.currentUserManager = createCurrentUserManager()
        UserDataOptionsService.sharedJsonMapper = JsonResourceMapperImplementation()
        
        typealias ConversationsService = ConversationsServiceImplementation
        
        ConversationsService.sharedSocket = chatSocketConnection
        ConversationsService.sharedDataBaseManager = createDatabaseManager()
        ConversationsService.sharedMessageFactory = messageFactory
        ConversationsService.sharedJsonMapper = JsonResourceMapperImplementation()
        ConversationsService.currentUserManager = createCurrentUserManager()
        
        typealias MessageService = ChatMessageServiceImplementation
        
        MessageService.sharedSocket = chatSocketConnection
        MessageService.sharedDataBaseManager = createDatabaseManager()
        MessageService.sharedMessageFactory = messageFactory
        MessageService.currentUserManager = createCurrentUserManager()
        
        typealias MessageHistoryService = ChatMessageHistoryServiceImplementation
        
        MessageHistoryService.sharedSocket = chatSocketConnection
        MessageHistoryService.sharedDataBaseManager = createDatabaseManager()
        MessageHistoryService.sharedMessageFactory = messageFactory
        MessageHistoryService.currentUserManager = createCurrentUserManager()
        
        typealias MessageInteractionService = ChatMessageInteractionServiceImplementation
        
        MessageInteractionService.sharedSocket = chatSocketConnection
        MessageInteractionService.sharedJsonMapper = JsonResourceMapperImplementation()
        
        typealias ChatKeywordsService = ChatKeywordsServiceImplementation
        
        ChatKeywordsService.sharedSocket = chatSocketConnection
        ChatKeywordsService.sharedJsonMapper = JsonResourceMapperImplementation()
        ChatKeywordsService.sharedDataBaseManager = createDatabaseManager()
        ChatKeywordsService.currentUserManager = createCurrentUserManager()
    }
    
    //MARK: - ServiceBuilder
    
    lazy var fileDownloadManager: FileDownloadManager = {
        
        let manager = FileDownloadManagerImplementation()
        manager.urlSessionConfiguration = URLSessionConfiguration.default
        manager.requestManager = HTTPRequestManagerDefault(with: manager.urlSessionConfiguration, and: manager)
        
        return manager
    }()
    
    lazy var defaultFileCacheManager: FileCacheManager = {
        
        let configuration = FileCacheConfiguration()
        let manager = FileCacheManagerImplementation(with: configuration)
        
        return manager
    }()
    
    func getSocketConnectionObserver() -> SocketConnectionObserver {
        return SocketConnectionObserver(with: chatSocketConnection)
    }
    
    func getChatMessageFactory() -> ChatMessageFactory {
        
        let messageFactory = ChatMessageFactoryImplementation()
        messageFactory.jsonMapper = JsonResourceMapperImplementation()
        messageFactory.formattingExtractor = ChatTextFormattingExtractorImplementation()
        messageFactory.fileDonwloadManager = fileDownloadManager
        messageFactory.fileCacheManager = defaultFileCacheManager
        messageFactory.imagesPrefetcher = UIImageLoadingAndCacheManager()
        
        return messageFactory
    }
    
    func getUserService() -> UserService {
        
        let service = UserServiceImplementation()
        service.notificationCenter = NotificationCenter.default
        
        return service
    }
    
    func getUserInfoAndSettingsService() -> UserInfoAndSettingsService {
        
        let service = UserInfoAndSettingsServiceImplementation()
        service.notificationCenter = NotificationCenter.default
        service.application = UIApplication.shared
        
        return service
    }
    
    func getUserDataOptionsService() -> UserDataOptionsService {
        
        let userDataService = UserDataOptionsServiceImplementation()
        configureBaseNetworkService(with: userDataService, baseUrl: baseUrlString)
        
        return userDataService
    }
    
    func getBannerService() -> BannerService {
        
        let service = BannerServiceImplementation()
        
        service.bannerBackgroundLoader = UIImageLoadingAndCacheManager()
        service.fileManager = FileManager()
        service.sunsetDetector = SunsetDetectorImplementation()
        service.userDefaults = UserDefaults.standard
        
        return service
    }
    
    func getConversationsService() -> ConversationsService {
        return ConversationsServiceImplementation()
    }
    
    func getMapItemsService() -> MapItemsService {
        
        let mapItemService = MapItemsServiceImplementation()
        mapItemService.dataBaseManager = createDatabaseManager()
        
        configureBaseNetworkService(with: mapItemService, baseUrl: baseUrlStringDataMosAPI)
        
        return mapItemService
    }
    
    func getChatMessageService() -> ChatMessageService {
        return ChatMessageServiceImplementation()
    }
    
    func getChatMessageHistoryService() -> ChatMessageHistoryService {
        return ChatMessageHistoryServiceImplementation()
    }
    
    func getChatMessageInteractionService() -> ChatMessageInteractionService {
        return ChatMessageInteractionServiceImplementation()
    }
    
    func getGlobalSearchService() -> GlobalSearchService {
        
        let globalSearchService = GlobalSearchServiceImplementation()
        globalSearchService.dataBaseManager = createDatabaseManager()

        configureBaseNetworkService(with: globalSearchService, baseUrl: baseUrlStringDataMosAPI)
        
        return globalSearchService
    }
    
    func getPushTokensService() -> PushTokensService {
        
        let pushTokenService = PushTokensServiceImplementation()
        pushTokenService.socket = chatSocketConnection
        pushTokenService.userDefaults = UserDefaults.standard
        
        return pushTokenService
    }

    func getYandexGeocoderService() -> YandexGeocoderService {
        
        let yandexGeocoderService = YandexGeocoderServiceImplementation()
        configureBaseNetworkService(with: yandexGeocoderService, baseUrl: baseUrlStringYandexGeocoder)
        yandexGeocoderService.yandexSearchManager = YMKMapKit.sharedInstance().createSearchManager(with: .online)
        
        return yandexGeocoderService
    }
    
    func getAddressAutocompleteService() -> AddressAutocompleteService {
        
        let addressAutocompleteService = AddressAutocompleteServiceImplementation()
        addressAutocompleteService.dataBaseManager = createDatabaseManager()

        configureBaseNetworkService(with: addressAutocompleteService, baseUrl: baseUrlString)
        
        return addressAutocompleteService
    }
    
    func getiTunesSearchService() -> iTunesSearchService {
        
        let iTunesSearchService = iTunesSearchServiceImplementation()
        configureBaseNetworkService(with: iTunesSearchService, baseUrl: baseUrlStringItunesLookup)
        iTunesSearchService.dataBaseManager = createDatabaseManager()
        
        return iTunesSearchService
    }
    
    func getChatKeywordsService() -> ChatKeywordsService {
        
        let chatKeywordsService = ChatKeywordsServiceImplementation()
        return chatKeywordsService
    }
    
    func getFeedbackService() -> FeedbackService {
        
        let feedbackService = HPSMFeedbackServiceImplementation()
        configureBaseNetworkService(with: feedbackService, baseUrl: hpsmServiceUrlString)
        
        feedbackService.xmlStringsBuilder = XmlStringsBuilder.getXmlStringsBuilder()
        return feedbackService
    }
    
    func getOverlapsService() -> OverlapsService {
        
        let overlapsService = OverlapsServiceImplementation()
        configureBaseNetworkService(with: overlapsService, baseUrl: baseUrlString)
        
        return overlapsService
    }
    
    func getWeatherService() -> WeatherService {
        
        let weatherService = WeatherServiceImplementation()
        configureBaseNetworkService(with: weatherService, baseUrl: baseUrlString)
        
        return weatherService
    }
    
    func getPaymentService() -> PaymentService {
        
        let paymentService = PaymentServiceImplementation()
        
        paymentService.paymentManager = SDKDIT()
        paymentService.paymentManager.develEnvironment = false
        paymentService.paymentManager.debugMode = false
        
        paymentService.dataBaseManager = createProtectedDatabaseManager()
        
        configureBaseNetworkService(with: paymentService, baseUrl: baseUrlString)
        paymentService.urlSessionConfiguration.timeoutIntervalForRequest = networkingPaymentTimeoutIntervalForRequest
        paymentService.urlSessionConfiguration.timeoutIntervalForResource = networkingPaymentTimeoutIntervalForResource
        
        return paymentService
    }
    
    func getCTMosRuService() -> CTMosRuService {
        
        let ctMosRuService = CTMosRuServiceImplementation()
        configureBaseNetworkService(with: ctMosRuService, baseUrl: baseUrlStringCTMosRu)
        
        return ctMosRuService
    }
}

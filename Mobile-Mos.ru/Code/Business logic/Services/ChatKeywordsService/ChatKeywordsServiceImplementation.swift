//
//  ChatKeywordsServiceImplementation.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 14.02.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

class ChatKeywordsServiceImplementation: ChatKeywordsService {
    
    fileprivate typealias Service = ChatKeywordsServiceImplementation
    fileprivate static var keywordsRequestsQueue = ThreadSafeSerialEventQueue<ChatKeywordsObtainMultipleResult>()
    
    var currentUser: User {
        return Service.currentUserManager.currentUser
    }
    
    /// Типы сообщений в сокете
    fileprivate enum Event: String {
        case getKeyWords
        case getKeyWordsResponse
    }
    
    static var sharedDataBaseManager: DataBaseManager!
    static var currentUserManager: CurrentUserManager!
    static var sharedJsonMapper: JsonResourceMapper!
    static var sharedSocket: Socket! {
        
        didSet {
            
            sharedSocket.onAsync(event: Event.getKeyWordsResponse.rawValue) { socketData, _ in
                
                guard let handler = keywordsRequestsQueue.dequeue()?.handler else { return }
                
                var result: ChatKeywordsObtainMultipleResult
                
                defer {
                    DispatchQueue.main.async(execute: {
                        handler(result)
                    })
                }
                
                guard let json = (socketData as? [NSDictionary])?.first else {
                    result = .failure(error: HTTPRequestError.jsonParsingNoResponseDataError)
                    return
                }
                
                guard let keywordsJson = json["keywords"] as? [NSDictionary] else {
                    result = .failure(error: HTTPRequestError.jsonMappingNotExpectedStructure)
                    return
                }
                
                var chatKeywords = sharedJsonMapper.mapJsonAsArray(keywordsJson, ofResourcesOfType: ChatKeywords.self)
                save(chatKeywords: &chatKeywords)
                
                result = .success(conversations: chatKeywords)
                
            }
            
            // Восстановление соединения (перезапуск очередей)
            
            sharedSocket.onAsync(event: SocketEvent.connect.rawValue) { _,_  in
                keywordsRequestsQueue.resume()
            }
            
            // Обработка обрыва соединения (остановка очередей)
            
            sharedSocket.onAsync(event: SocketEvent.disconnect.rawValue) { _,_  in
                keywordsRequestsQueue.pause()
            }
        }
    }
    
    func obtainKeyWords(completion: ChatKeywordsObtainMultipleHandler?) {
        
        guard Service.sharedSocket.isConnected else {
            
            let error = NSError(domain: NSURLErrorDomain, code: NSURLErrorCannotConnectToHost, userInfo: nil)
            let result = ChatKeywordsObtainMultipleResult.failure(error: error)
            
            guard let completion = completion else { return }
            
            completion(result)
            
            return
        }
        
        let userIdValue: Any = currentUser.userId ?? NSNull()
        let request = [
            "user_id": userIdValue,
            "session_id": currentUser.sessionId
        ]

        let emitter = {
            Service.sharedSocket.send(event: Event.getKeyWords.rawValue, with: [request])
        }

        let handler: ChatKeywordsObtainMultipleHandler = { result in
            guard let completion = completion else { return }
            completion(result)
        }

        let eventBlock = SerialEventBlock(emitter: emitter, handler: handler)
        Service.keywordsRequestsQueue.enqueue(element: eventBlock)
    }
    
    func getChatIds(with string: String) -> [String] {

        var chatids = [String]()
        
        let cachedKeywords = Service.obtainCachedKeywords()
        cachedKeywords.forEach{ if $0.keywords.lowercased().contains(string) { chatids.append($0.chatId) } }
        
        return chatids
    }
    
    func getChatKeywords(for conversationType: ConversationIdentificator) -> String {
    
        let cachedKeywords = Service.obtainCachedKeywords()
        
        return (cachedKeywords.filter({ $0.chatId == conversationType.rawValue }).first)?.keywords ?? ""
    }
    
    //MARK: - Private methods
    
    /// Сохранить список ключевых слов в кэш
    ///
    /// - Parameter chatKeywords: массив ключевых слов
    fileprivate static func save(chatKeywords: inout [ChatKeywords]) {
        
        guard !chatKeywords.isEmpty else { return }
        
        do {
            try sharedDataBaseManager.save(models: chatKeywords)
        }
        catch let error {
            print("Ошибка при сохранении ключевых слов: \(error.localizedDescription)")
        }
    }
    
    /// Получить кэш ключевых слов
    ///
    /// - Returns: массив ключевых слов
    fileprivate static func obtainCachedKeywords() -> [ChatKeywords] {
        
        let fetchRequest = DataBaseFetchRequest(with: ChatKeywords.self)
        do {
           return try sharedDataBaseManager.fetch(with: fetchRequest)
        }
        catch let error {
            print("Ошибка при получении ключевых слов: \(error.localizedDescription)")
            return []
        }
    }
    
    /// Очистить кэш в БД
    fileprivate func removeAllCachedKeywords() {
        Service.sharedDataBaseManager.deleteAllModelsAsync(of: ChatKeywords.self, resultBlock: nil)
    }
}

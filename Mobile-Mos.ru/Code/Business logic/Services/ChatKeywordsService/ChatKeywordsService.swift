//
//  ChatKeywordsService.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 14.02.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Результат запроса на получение списка ключевых слов
///
/// - success: успех, связанное значение - список диалогов
/// - failure: провал, связанное значение - ошибка
enum ChatKeywordsObtainMultipleResult {
    case success(conversations: [ChatKeywords])
    case failure(error: Error)
}

enum ChatByKeywordSearchMultipleResult {
    case success(chatId: Int)
    case failure(error: Error)
}

/// Блок обработки получения ключевых слов, единственный параметр - результат запроса
typealias  ChatKeywordsObtainMultipleHandler = (ChatKeywordsObtainMultipleResult) -> Void

protocol ChatKeywordsService {
    
    /// Получить кейворды
    ///
    /// - Parameter completion: обработчик запроса
    func obtainKeyWords(completion: ChatKeywordsObtainMultipleHandler?)
    
    /// Получить список id чатов, которые содержат поисковое слово
    ///
    /// - Parameter string: поисковый запрос
    func getChatIds(with string: String) -> [String]

    /// Получить кейворды чата
    ///
    /// - Parameter conversationType: тип чата
    /// - Returns: строка с кейвордами через \n
    func getChatKeywords(for conversationType: ConversationIdentificator) -> String
}

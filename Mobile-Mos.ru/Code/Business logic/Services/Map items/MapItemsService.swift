//
//  MapItemsService.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 21.02.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Результат запроса на получения списка объектов выбранного датасета
///
/// - success: успех, связанное значение - список элементов категории
/// - failure: провал, связанное значение - ошибка
enum MapItemObtainMultipleResult {
    case success(items: [MapItem])
    case failure(error: Error)
}

/// Результат запроса на получение списка объектов категории
///
/// - success: успех, связанное значение - список категорий
/// - failure: провал, связаное значение - ошибка
enum MapItemDatasetObtainMultipleResult {
    case success(items: [MapItemDataset])
    case failure(error: Error)
}

/// Результат запроса на получение списка категорий
///
/// - success: успех, связанное значение - список категорий
/// - failure: провал, связаное значение - ошибка
enum MapItemCategoryObtainMultipleResult {
    case success(categories: [MapItemCategory])
    case failure(error: Error)
}

/// Блок обработки получения категорий
typealias CategoryObtainMultipleHandler = (MapItemCategoryObtainMultipleResult) -> Void

/// Сервис для работы с объектами карты
protocol MapItemsService {
    
     /// Получение категорий
     ///
     /// - Parameter completion: блок кода, который вызывается по окончании запроса, единственный параметр - результат запроса
     func obtainMapItemsCategories(completion: @escaping (MapItemCategoryObtainMultipleResult) -> Void) -> HTTPRequest
    
    
    /// Получение объектов категории
    ///
    /// - Parameters:
    ///   - dataSetId: строковое значение идентификатора категории
    ///   - boundingBox: видимый квадрат карты, передаваемый для получения точек по нему
    ///   - completion: блок кода, который вызывается по окончании запроса, единственный параметр - результат запроса
    /// - Returns: запрос, который отправляется при вызове данного метода
    @discardableResult
    func obtainMapItems(with dataSetId:String, completion: @escaping (MapItemObtainMultipleResult) -> Void) -> HTTPRequest
    
    /// Поиск датасета по содержания подстроки в строке из БД
    ///
    /// - Parameter searchString: строка для фильтрации
    /// - Returns: [MapItemDataset] - отфильтрованный массив
     func searchDatasetFromBD(with searchString: String, completion: @escaping (MapItemDatasetObtainMultipleResult) -> Void)
    
    /// Получение детальной информации про объект датасета
    ///
    /// - Parameter itemId: id объекта датасета
    func obtainMapItemDetail(with itemId: String)
    
    /// Сохранение датасетов в базу данных
    ///
    /// - Parameter mapItemsDataset: датасет для сохранения
    func save(mapItemsDataset: MapItemDataset, resultBlock: @escaping DataBaseOperationResultBlock)
    
    /// Сохранение датасета с точкой карты в базу данных как избранное
    ///
    /// - Parameter mapItem: точка карты
    func saveToFavorites(_ dataset: MapItemDataset)
    
    ///  Проверить существует ли в базе данных текущий item
    ///
    /// - Parameter mapItem: точка карты
    /// - Returns: Bool - обновилась точка или нет
    func mapItemIsFavorite(mapItem: MapItem, completion:@escaping (Bool) -> Void)
    
    /// Удаление конкретной точки карты из базы данных
    ///
    /// - Parameter mapItem: точка карты
    func remove(mapItem: MapItem)
    
    /// Получение сохраненных датасетов
    ///
    /// - Returns: массив категорий
    func obtainSavedMapItemsDatasets() -> [MapItemDataset]
    
    /// Получение избранных датасетов с точеками карты
    ///
    /// - Returns: массив датасетов
    func obtainFavoritedMapItemsWithDataset() -> [FavoritedDataset]
    
    /// Получение сохраненных/избранных датасетов
    ///
    /// - Returns: массив точек карты
    func obtainFavoritedMapItems() -> [MapItem]
}

//
//  MapItemsServiceImplementation.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 21.02.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class MapItemsServiceImplementation: BaseNetworkService, MapItemsService {
    
    let apiKey = "3a52f3a64008f7bc9225df5f2e3d6314"
    let maxCountOfDatasetsInHistory = 30
    let maxCountOfDatasetSearchResult = 3
    let offsetForEqualLocation = 0.0000000001
    
    var dataBaseManager: DataBaseManager!
    var datasetsCache = [Dataset]()
    
    func obtainMapItemsCategories(completion: @escaping (MapItemCategoryObtainMultipleResult) -> Void) -> HTTPRequest{
        
        let urlString = pathToOpendata + "categories"
        var configuration = HTTPRequestConfiguration(withMethod: HTTPMethod.GET.rawValue, andUrlString: urlString)
        
        configuration.queryStringParameters = ["fields":"id,name, datasets_app_count","filter":"{\"datasets.show_in_app\":1}","expand":"datasets"]
        
        var urlRequest = requestBuilder.request(with: configuration)
        
        return requestManager.perform(dataRequest: urlRequest)
            .validate(acceptableStatusCodes: [200])
            .validate(acceptableContentTypes: [HTTPRequestContentType.json.rawValue])
            .responseJson(responseHandler: {[weak self] (parsingResult) in
                
                guard let strongSelf = self else { return }
                
                var result: MapItemCategoryObtainMultipleResult
                
                defer {
                    DispatchQueue.main.async {
                        completion(result)
                    }
                }
                
                switch parsingResult{
                    
                case .success(let json):
                    
                    guard let response = json as? NSDictionary else {
                        result = .failure(error: HTTPRequestError.jsonMappingNotExpectedStructure)
                        return
                    }
                    
                    guard let items = response["items"] as? [NSDictionary] else {
                        result = .failure(error: HTTPRequestError.jsonMappingNotExpectedStructure)
                        return
                    }
                    
                    guard !items.isEmpty else {
                        result = .failure(error:
                            HTTPRequestError.emptyContentError)
                        return
                    }
                    
                    let categories = strongSelf.responseMapper.mapJsonAsArray(items, ofResourcesOfType: MapItemCategory.self)
                    
                    result = .success(categories: categories)
                    
                    strongSelf.save(categories: categories)
                    
                case .failure(let error):
                    result = .failure(error: error)
                    
                    strongSelf.obtainCashedCategories { (cachedResult) in
                        completion(cachedResult)
                    }
                }
            })
        
    }
    
    func obtainMapItems(with dataSetId:String, completion: @escaping (MapItemObtainMultipleResult) -> Void) -> HTTPRequest{
        
        let urlString = pathToOpendata + "datasets/\(dataSetId)"
        var configuration = HTTPRequestConfiguration(withMethod: HTTPMethod.GET.rawValue, andUrlString: urlString)
        
        configuration.queryStringParameters = ["expand":"objects","fields":"\"objects\""]
        
        var urlRequest = requestBuilder.request(with: configuration)
        
        return requestManager.perform(dataRequest: urlRequest)
            .validate(acceptableStatusCodes: [200])
            .validate(acceptableContentTypes: [HTTPRequestContentType.json.rawValue])
            .responseJson(responseHandler: { [weak self] (parsingResult) in

                guard let strongSelf = self else { return }
                
                var result: MapItemObtainMultipleResult
                
                defer {
                    DispatchQueue.main.async {
                        completion(result)
                    }
                }
                
                switch parsingResult{
                    
                case .success(let json):
                    
                    guard let response = json as? NSDictionary else {
                        result = .failure(error: HTTPRequestError.jsonMappingNotExpectedStructure)
                        return
                    }
                    
                    guard let features = response["objects"] as? [NSDictionary] else {
                        result = .failure(error: HTTPRequestError.jsonMappingNotExpectedStructure)
                        return
                    }
                    
                    var places: [MapItem] = []
                    
                    for json in features {
                        let place = strongSelf.responseMapper.mapJson(json, toResourceOfType: MapItem.self)
                        places.append(place)
                    }
                   
                    places = Array(Set(places))
                    
                    let favoritedItems = strongSelf.obtainFavoritedMapItems()
        
                    places.forEachMutate({
                        $0.location += (strongSelf.offsetForEqualLocation / drand48())
                    })
                    
                    places.forEachMutate({ (mapItem) in
                        favoritedItems.forEach{
                            if mapItem.globalId == $0.globalId { mapItem.isFavorited = true }
                        }
                    })
                    
                    result = .success(items: places)
                    
                case .failure(let error):
                    result = .failure(error: error)
                }
            })
    }
    
    func searchDatasetFromBD(with searchString: String, completion: @escaping (MapItemDatasetObtainMultipleResult) -> Void) {
        
        let predicate = NSPredicate(format: "\(#keyPath(MapItemDatasetModel.name)) contains[cd] %@", searchString)
        let request = DataBaseFetchRequest(with: Dataset.self, limitedBy: maxCountOfDatasetSearchResult, filteredWith: predicate)
        
        dataBaseManager.fetchAsync(with: request) { (parsingResult) in
            
            var result: MapItemDatasetObtainMultipleResult
            
            defer {
                DispatchQueue.main.async {
                    completion(result)
                }
            }
            
            switch parsingResult {
                
            case .success(let models):
                
                result = .success(items: models)
            
            case .error(let error):
                
                result = .failure(error: error)
            }
        }
    }
    
    func obtainMapItemDetail(with itemId: String) {
        //будет запрос для получние детальной информации про точку на карте
    }
    
    func save(mapItemsDataset: MapItemDataset, resultBlock: @escaping DataBaseOperationResultBlock) {
        
        guard let model = mapItemsDataset as? Dataset else { return }
        
        var mutableModel = model
        mutableModel.saveDate = Date()
       
        dataBaseManager.saveAsync(models: [mutableModel]) { (result) in
            resultBlock(result)
        }
    }
    
    func saveToFavorites(_ dataset: MapItemDataset) {
        
        guard var model = dataset as? FavoritedDataset else { return }
        
        let fetchRequest = DataBaseFetchRequest(with: FavoritedDataset.self, filteredWith: "itemId = %d", arguments: model.itemId)
        
        var favoritesItems: [MapItem] = model.favoritedItems
        
        do {
            if let savedModel = try dataBaseManager.fetch(with: fetchRequest).first {
                favoritesItems.append(contentsOf: savedModel.favoritedItems)
            }
        }
        catch let error {
            print("Ошибка получения избранного датасета:\(error.localizedDescription)")
        }
        
        favoritesItems.forEachMutate({ $0.isFavorited = true })
        
        model.favoritedItems = favoritesItems
        dataBaseManager.saveAsync(models: [model], resultBlock: nil)
    }
    
    func mapItemIsFavorite(mapItem: MapItem, completion:@escaping (Bool) -> Void) {
        
        let request = DataBaseFetchRequest(with: MapItem.self,
                                           filteredWith: "globalId = %d AND isFavorited = true",
                                           arguments: mapItem.globalId)
        
        dataBaseManager.fetchAsync(with: request) { result in
            switch result {
            case .success(let models):
                completion(!models.isEmpty)
            default:
                completion(false)
            }
        }
    }
    
    
    func remove(mapItem: MapItem) {
        do {
            try dataBaseManager.delete(models: [mapItem])
        }
        catch let error {
            print("Ошибка при удалении: \(error)")
        }
    }
    
    func obtainFavoritedMapItemsWithDataset() -> [FavoritedDataset] {
        
        let request = DataBaseFetchRequest(with: FavoritedDataset.self,
                       filteredWith: "favoritedItems.@count > 0")
        do {
            return try dataBaseManager.fetch(with: request)
        }
        catch let error {
            print("Ошибка при получении сохраненных точек категорий: \(error)")
            return []
        }
    }
    
    func obtainFavoritedMapItems() -> [MapItem] {
        let request = DataBaseFetchRequest(with: MapItem.self)
        
        do{
            return try dataBaseManager.fetch(with: request)
        }
        catch let error {
            print("Ошибка при получении сохраненных точек категорий: \(error)")
            return []
        }
    }
    
    func obtainSavedMapItemsDatasets() -> [MapItemDataset] {
        do {
            var request = DataBaseFetchRequest(with: Dataset.self,
                                               limitedBy: maxCountOfDatasetsInHistory,
                                               sortedBy: #keyPath(MapItemDatasetModel.saveDate),
                                               ascending: false)
            
            request.filterPredicate = NSPredicate(format: "\(#keyPath(MapItemDatasetModel.saveDate)) != nil", [])
            
            
            let models = try dataBaseManager.fetch(with: request).sorted(by: {$0.saveDate! > $1.saveDate!})
            
            datasetsCache = models
            
            return models
        }
        catch let error {
            print("Ошибка при получении сохраненных категорий: \(error)")
            return []
        }
    }
    
    //MARK: Helpers
    
    //MARK: Private methods 
    
    fileprivate func updateDatasetsCache(with models: [Dataset]) {
        
        dataBaseManager.saveAsync(models: models, resultBlock: nil)
    }
    
    /// Сохранить категории в кэш
    ///
    /// - Parameter categories: категории
    fileprivate func save(categories: [MapItemCategory]) {
        
        var mutableModelsArray = [MapItemCategory]()
        
        for var model in categories {
            model.isSaved = true
            mutableModelsArray.append(model)
        }
        
        dataBaseManager.saveAsync(models: mutableModelsArray) { [weak self] (result) in
            guard let strongSelf = self else { return }
            
            switch result {
            case .success:
                
                strongSelf.updateDatasetsCache(with: strongSelf.datasetsCache)
            case .error(let error) :
                print("Ошибка при сохранении в кэш категорий: \(error)")
            }
        }
    }
    
    /// Получить объекты категоий из кэша
    ///
    /// - Parameter completion: результат запроса на получение кэша
    fileprivate func obtainCashedCategories(completion:
        @escaping CategoryObtainMultipleHandler) {
        
        let request = DataBaseFetchRequest(with: MapItemCategory.self, filteredWith: NSPredicate(format: "\(#keyPath(MapItemCategoryModel.isSaved)) = true", []))
        
        
        dataBaseManager.fetchAsync(with: request) { (fetchResult) in
            
            var obtainResult: MapItemCategoryObtainMultipleResult
            
            switch fetchResult {
                
            case .success(let models):
                
                obtainResult = .success(categories: models)
            case .error(let error):
                
                obtainResult = .failure(error: error)
            }
            
            completion(obtainResult)
        }
        
    }
}


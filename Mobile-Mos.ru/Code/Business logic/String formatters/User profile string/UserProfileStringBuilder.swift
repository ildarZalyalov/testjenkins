//
//  UserProfileStringBuilder.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 21.02.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Форматтер строк для ЛК
protocol UserProfileStringBuilder {
    
    /// Получить полное имя пользователя, например "Тестовицкий Людвиг Аристархович"
    ///
    /// - Parameter user: пользователь
    /// - Returns: полное имя пользователя
    func fullName(of user: User) -> String
    
    /// Получить базовую информацию о пользователе (почту и телефон)
    ///
    /// - Parameter user: пользователь
    /// - Returns: базовая информация о пользователе
    func basicUserInfo(for user: User) -> String
}

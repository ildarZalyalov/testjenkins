//
//  UserProfileStringBuilderImplementation.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 21.02.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

class UserProfileStringBuilderImplementation: UserProfileStringBuilder {
    
    func fullName(of user: User) -> String {
        
        let spacesCharacterSet = CharacterSet.whitespacesAndNewlines
        
        var lastNameString = user.userInfo?.lastName ?? String()
        var firstNameString = user.userInfo?.firstName ?? String()
        var patronymicNameString = user.userInfo?.patronymicName ?? String()
        
        lastNameString = lastNameString.trimmingCharacters(in: spacesCharacterSet)
        firstNameString = firstNameString.trimmingCharacters(in: spacesCharacterSet)
        patronymicNameString = patronymicNameString.trimmingCharacters(in: spacesCharacterSet)
        
        var name = lastNameString
        if !firstNameString.isEmpty {
            name += " " + firstNameString
        }
        if !patronymicNameString.isEmpty {
            name += " " + patronymicNameString
        }
        
        return name
    }
    
    func basicUserInfo(for user: User) -> String {
        
        var userInfo = String()
        
        if let phone = user.userInfo?.phone, !phone.isEmpty {
            userInfo = phone
        }
        
        if let email = user.userInfo?.email, !email.isEmpty {
            
            if !userInfo.isEmpty {
                userInfo += "\n"
            }
            
            userInfo += email
        }
        
        return userInfo
    }
}

//
//  DistanceStringBuilderImplementation.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 12.10.16.
//  Copyright © 2016 mos.ru. All rights reserved.
//

import Foundation

class DistanceStringBuilderImplementation: DistanceStringBuilder {
    
    fileprivate static var cachedMetresFormatter: NumberFormatter!
    fileprivate static var cachedKilometresFormatter: NumberFormatter!
    
    lazy var metresFormatter: NumberFormatter = {
        
        if DistanceStringBuilderImplementation.cachedMetresFormatter == nil {
            
            let numberFormatter = NumberFormatter()
            numberFormatter.numberStyle = .decimal
            numberFormatter.minimumFractionDigits = 0
            numberFormatter.maximumFractionDigits = 0
            
            DistanceStringBuilderImplementation.cachedMetresFormatter = numberFormatter
        }
        
        return DistanceStringBuilderImplementation.cachedMetresFormatter
    }()
    
    lazy var kilometresFormatter: NumberFormatter = {
        
        if DistanceStringBuilderImplementation.cachedKilometresFormatter == nil {
            
            let numberFormatter = NumberFormatter()
            numberFormatter.numberStyle = .decimal
            numberFormatter.minimumFractionDigits = 0
            numberFormatter.maximumFractionDigits = 1
            
            DistanceStringBuilderImplementation.cachedKilometresFormatter = numberFormatter
        }
        return DistanceStringBuilderImplementation.cachedKilometresFormatter
    }()
    
    let unknownDistanceStrings: FormattedDistanceStrings = ("-", "м")
    
    func distanceStrings(forDistance distance: Double) -> FormattedDistanceStrings? {
        
        var distanceToDisplay = distance
        
        let hasKilometres = Int(distance) >= 1000
        if hasKilometres {
            distanceToDisplay = distance / 1000
        }
        
        let numberFormatter = hasKilometres ? self.kilometresFormatter : self.metresFormatter
        let number = NSNumber(value: distanceToDisplay)
        
        guard let distanceString = numberFormatter.string(from: number) else {
            return nil
        }
        
        let distanceUnitString = hasKilometres ? "км" : "м"
        
        return (distanceString, distanceUnitString)
    }
}

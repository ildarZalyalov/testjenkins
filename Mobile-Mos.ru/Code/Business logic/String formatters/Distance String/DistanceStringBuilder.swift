//
//  DistanceStringBuilder.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 12.10.16.
//  Copyright © 2016 mos.ru. All rights reserved.
//

import Foundation

/// Строки, описывающие расстояние - строка с расстоянием и строка с единицей измерения расстояния
typealias FormattedDistanceStrings = (distanceString: String, distanceUnitString: String)

/// Строитель строк, описывающих расстояние
protocol DistanceStringBuilder {
    
    /// Строки для описания неизвестного расстояния
    var unknownDistanceStrings: FormattedDistanceStrings { get }
    
    /// Получить строки для описания расстояния
    ///
    /// - Parameter distance: расстояние
    /// - Returns: строки для описания расстояния или nil, если не получилось сгенерировать строки
    func distanceStrings(forDistance distance: Double) -> FormattedDistanceStrings?
}

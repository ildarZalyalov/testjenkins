//
//  DateStringFormatterImplementation.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 20.06.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

class DateStringFormatterImplementation: DateStringFormatter {
    
    //MARK: - Форматтеры дат
    
    var timeZone: TimeZone?
    
    lazy var dateFormatter: DateFormatter = {
        
        let russianLocale = Locale(identifier: "ru_RU")
        let timeZoneAbbreviation = "UTC"
        
        let formatter = DateFormatter()
        formatter.dateFormat = "d MMMM"
        formatter.locale = russianLocale
        formatter.timeZone = timeZone ?? TimeZone(abbreviation: timeZoneAbbreviation)
        return formatter
    }()
    
    lazy var dateFormatterWithYear: DateFormatter = {
        
        let russianLocale = Locale(identifier: "ru_RU")
        let timeZoneAbbreviation = "UTC"
        
        let formatter = DateFormatter()
        formatter.dateFormat = "d MMMM, yyyy"
        formatter.locale = russianLocale
        formatter.timeZone = timeZone ?? TimeZone(abbreviation: timeZoneAbbreviation)
        
        return formatter
    }()
    
    lazy var dateFormattedForCalendar: DateFormatter = {
        
        let russianLocale = Locale(identifier: "ru_RU")
        let timeZoneAbbreviation = "UTC"
        
        let formatter = DateFormatter()
        formatter.dateFormat = "d MMMM yyyy"
        formatter.locale = russianLocale
        formatter.timeZone = timeZone ?? TimeZone(abbreviation: timeZoneAbbreviation)
        
        return formatter
    }()
    
    lazy var dateFormatterWithTime: DateFormatter = {
        
        let russianLocale = Locale(identifier: "ru_RU")
        let timeZoneAbbreviation = "UTC"
        
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm"
        formatter.locale = russianLocale
        formatter.timeZone = timeZone ?? TimeZone(abbreviation: timeZoneAbbreviation)
        
        return formatter
    }()
    
    //MARK: - DateStringFormatter
    
    func relativeDateString(from date: Date) -> String {
        
        var dateString = ""
        var yestedayDateComponents: DateComponents = DateComponents()
        
        if let yesterdayDate = Calendar.current.date(byAdding: .day, value: -1, to: Date()) {
            yestedayDateComponents = yesterdayDate.getDateComponents(components: .year, .month, .day)
        }
        
        let todayDateComponents = Date().getDateComponents(components: .year, .month, .day)
        let dateComponents = date.getDateComponents(components: .year, .month, .day)
        
        guard let newsYear = dateComponents.year, let todayYear = todayDateComponents.year else {
            return dateFormatter.string(from: date)
        }
        
        if todayYear > newsYear {
            dateString = dateFormatterWithYear.string(from: date)
        }
        else if newsYear == todayYear {
            dateString = dateFormatter.string(from: date)
        }
        
        if todayDateComponents == dateComponents {
            dateString = StringsHelper.todayStringPrefix + dateFormatterWithTime.string(from: date)
        }
        else if yestedayDateComponents == dateComponents {
            dateString = StringsHelper.yesterdayStringPrefix + dateFormatterWithTime.string(from: date)
        }
        
        return dateString
    }
    
    func relativeDateIntervalString(from dateFrom: Date, to dateTo: Date) -> String {
        
        let dateToComponents = dateTo.getDateComponents(components: .year, .month, .day)
        let dateFromComponents = dateFrom.getDateComponents(components: .year, .month, .day)
        let todayDateComponents = Date().getDateComponents(components: .year, .month, .day)
        
        if dateToComponents == dateFromComponents && todayDateComponents == dateToComponents {
            return StringsHelper.todayStringPrefix + dateFormatterWithTime.string(from: dateFrom) + " - " + dateFormatterWithTime.string(from: dateFrom)
        }
        else if dateToComponents == dateFromComponents {
            return dateFormatter.string(from: dateTo)
        }
        
        guard let toYear = dateToComponents.year, let fromYear = dateFromComponents.year
            else { return dateFormatter.string(from: dateTo) }
        
        var dateToString = ""
        var dateFromString = ""
        
        if fromYear != toYear {
            dateToString = dateFormattedForCalendar.string(from: dateTo)
            dateFromString = dateFormattedForCalendar.string(from: dateFrom)
        }
        else {
            dateToString = dateFormatter.string(from: dateTo)
            dateFromString = dateFormatter.string(from: dateFrom)
        }
        
        return dateFromString + " - " + dateToString
    }
}

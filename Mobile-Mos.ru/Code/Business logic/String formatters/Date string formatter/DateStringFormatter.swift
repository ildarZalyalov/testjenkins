//
//  DateStringFormatter.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 20.06.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Форматтер строк с датами
protocol DateStringFormatter {
    
    /// Получить относительное строковое представление даты
    ///
    /// - Parameter date: дата
    /// - Returns: относительное строковое представление даты
    func relativeDateString(from date: Date) -> String
    
    /// Получить относительное строковое представление интервала дат
    ///
    /// - Parameters:
    ///   - dateFrom: дата от
    ///   - dateTo: дата до
    /// - Returns: относительное строковое представление интервала дат
    func relativeDateIntervalString(from dateFrom: Date, to dateTo: Date) -> String
}

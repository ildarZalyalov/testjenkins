//
//  NounCountStringFormatter.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 26.01.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Форматтер позволяет возвращать нужные окончания для числа
protocol NounCountStringFormatter {
    
    /// Получить окончание для минут
    ///
    /// - Parameter count: кол-во минут
    /// - Returns: окончание
    func getMinuteWordForCount(count: UInt) -> String
    
    /// Получить окончание для часов
    ///
    /// - Parameter count: кол-во часов
    /// - Returns: окончание
    func getHourWordForCount(count: UInt) -> String
   
    /// Получить окончание для дня
    ///
    /// - Parameter count: кол-во дней
    /// - Returns: окончание
    func getDayWordForCOunt(count: UInt) -> String
    
    /// Получить окончание для кол-ва мест
    ///
    /// - Parameter count: кол-во мест
    /// - Returns: окончание
    func getPlaceWordForCount(count: UInt) -> String
}

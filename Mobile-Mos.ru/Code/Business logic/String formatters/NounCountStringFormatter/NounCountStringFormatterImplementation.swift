//
//  NounCountStringFormatterImplementation.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 26.01.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

class NounCountStringFormatterImplementation: NounCountStringFormatter {
    
    
    func getMinuteWordForCount(count: UInt) -> String {
        return getNounForCount(count: count, noun1: StringsHelper.minutesFew, noun2: StringsHelper.minuteOne, noun3: StringsHelper.minutes)
    }
    
    func getHourWordForCount(count: UInt) -> String {
        return getNounForCount(count: count, noun1: StringsHelper.hours, noun2: StringsHelper.hour, noun3: StringsHelper.hoursFew)
    }
    
    func getDayWordForCOunt(count: UInt) -> String {
        return getNounForCount(count: count, noun1: StringsHelper.daysFew, noun2: StringsHelper.day, noun3: StringsHelper.daysFew)
    }
    
    func getPlaceWordForCount(count: UInt) -> String {
        return getNounForCount(count: count, noun1: StringsHelper.placesSubtitle, noun2: StringsHelper.placeSubtitle, noun3: StringsHelper.placesFewSubtitle)
    }
    
   /// Получить окончание для числа
   ///
   /// - Parameters:
   ///   - count: число
   ///   - noun1: окончание типа "объявлений"
   ///   - noun2: окончание типа "объявление"
   ///   - noun3: окончание типа "объявления"
   /// - Returns: окончание для числа
   fileprivate func getNounForCount(count: UInt, noun1: String, noun2: String, noun3: String) -> String {
        
        let hundredRemnant = count % 100
        let tenRemnant = count % 10
        
        if hundredRemnant > 10 && hundredRemnant < 21 {
            return noun1
        }
        else if tenRemnant == 1 {
            return noun2
        }
        else if tenRemnant > 1 && tenRemnant < 5 {
            return noun3
        }
        
        return noun1
    }
}

//
//  BankCardFormatManagerImplementation.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 25.05.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

class BankCardFormatManagerImplementation: NSObject, BankCardFormatManager {
    
    let maximumCardNumberPartLengthToDetermineType = 6
    
    fileprivate var updatingCardTypes = false
    fileprivate weak var currentTextField: UITextField?
    
    //MARK: - BankCardFormatManager
    
    var separator: String = " "
    
    var acceptableCardNumberLengths: CountableClosedRange<Int>? {
        didSet {
            filterAcceptableCardTypes()
        }
    }
    
    var cardTypes: [BankCardType] = BankCardType.obtainDefaultTypes() {
        didSet {
            guard !updatingCardTypes else { return }
            filterAcceptableCardTypes()
        }
    }
    
    var cardNumber: String? {
        guard let text = currentTextField?.text else { return nil }
        return unformat(cardNumber: text)
    }
    
    weak var proxyDelegate: UITextFieldDelegate?
    
    func configure(with textField: UITextField) {
        currentTextField = textField
    }
    
    func unformat(cardNumber: String) -> String {
        return cardNumber.replacingOccurrences(of: separator, with: String())
    }
    
    func format(cardNumber: String) -> BankCardNumberFormatResult {
        
        guard let cardType = cardType(for: cardNumber) else { return (cardNumber, nil) }
        
        var pattern = String()
        var first = true
        
        for group in cardType.numberGrouping {
            pattern += "(\\d{1,\(group)})"
            if !first { pattern += "?" }
            first = false
        }
        
        guard let regex = try? NSRegularExpression(pattern: pattern, options: []) else {
            return (cardNumber, cardType)
        }
        
        let formattedCardNumber = split(string: cardNumber, with: regex).joined(separator: separator)
        return (formattedCardNumber, cardType)
    }
    
    //MARK: - Приватные методы
    
    func filterAcceptableCardTypes() {
        
        guard let lengths = acceptableCardNumberLengths else { return }
        
        updatingCardTypes = true
        cardTypes = cardTypes.filter { lengths.contains($0.maxLength) }
        updatingCardTypes = false
    }
    
    func cardType(for cardNumber: String) -> BankCardType? {
        
        let numberPartLength = min(cardNumber.count, maximumCardNumberPartLengthToDetermineType)
        let checkRange = (0...numberPartLength).reversed()
        
        for i in checkRange {
            
            guard let substringAsInt = Int(cardNumber.prefix(i)) else { continue }
            guard let firstMatchingCardType = cardTypes.filter({ $0.identifyingDigits.contains(substringAsInt) }).first else { continue }
            
            return firstMatchingCardType
        }
        
        return nil
    }
    
    func split(string: String, with regex: NSRegularExpression) -> [String] {
        
        let matches = regex.matches(in: string, options: [], range: NSMakeRange(0, string.count))
        var result = [String]()
        
        for match in matches {
            for i in 1 ..< match.numberOfRanges {
                let range = match.range(at: i)
                guard range.length > 0 else { continue }
                result.append(NSString(string: string).substring(with: range))
            }
        }
        
        return result
    }
    
    func indexInUnformattedString(for indexInFormattedString: Int, formattedString: String) -> Int {
        
        var componentWithIndex = 0
        var charCount = 0
        
        for component in formattedString.components(separatedBy: separator) {
            charCount += component.count
            guard charCount < indexInFormattedString else { break }
            componentWithIndex += 1
            charCount += separator.count
        }
        
        return indexInFormattedString - componentWithIndex * separator.count
    }
    
    func indexInFormattedString(for index: Int, unformattedString: String) -> Int {
        
        var charIdx = 0
        let formattedString = format(cardNumber: unformattedString).formattedNumber
        let groups = formattedString.components(separatedBy: separator)
        
        for i in 0 ..< groups.count {
            let groupChars = groups[i].count
            charIdx += groupChars
            if charIdx >= index {
                return min(index + i * separator.count, formattedString.count)
            }
        }
        
        return 0
    }
    
    //MARK: - UITextFieldDelegate
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        return proxyDelegate?.textFieldShouldClear?(textField) ?? true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return proxyDelegate?.textFieldShouldReturn?(textField) ?? true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return proxyDelegate?.textFieldShouldBeginEditing?(textField) ?? true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        proxyDelegate?.textFieldDidBeginEditing?(textField)
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return proxyDelegate?.textFieldShouldBeginEditing?(textField) ?? true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        proxyDelegate?.textFieldDidEndEditing?(textField)
    }
    
    @available(iOS 10.0, *)
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextFieldDidEndEditingReason) {
        proxyDelegate?.textFieldDidEndEditing?(textField, reason: reason)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let text = textField.text ?? String()
        let newValueUnformatted = unformat(cardNumber: NSString(string: text).replacingCharacters(in: range, with: string))
        let oldValueUnformatted = unformat(cardNumber: text)
        let formattingResult = format(cardNumber: newValueUnformatted)
        
        if let maxLength = acceptableCardNumberLengths?.last {
            
            var cardNumberMaxLength = maxLength
            if let cardType = formattingResult.cardType {
                cardNumberMaxLength = min(cardNumberMaxLength, cardType.maxLength)
            }
            
            guard newValueUnformatted.count <= cardNumberMaxLength else { return false }
        }
        
        var position: UITextPosition?
        
        if let start = textField.selectedTextRange?.start {
            
            let oldCursorPosition = textField.offset(from: textField.beginningOfDocument, to: start)
            let oldCursorPositionUnformatted = indexInUnformattedString(for: oldCursorPosition, formattedString: text)
            let newCursorPositionUnformatted = oldCursorPositionUnformatted + (newValueUnformatted.count - oldValueUnformatted.count)
            let newCursorPositionFormatted = indexInFormattedString(for: newCursorPositionUnformatted, unformattedString: newValueUnformatted)
            
            position = textField.position(from: textField.beginningOfDocument, offset: newCursorPositionFormatted)
        }
        
        textField.text = formattingResult.formattedNumber
        
        if let position = position {
            textField.selectedTextRange = textField.textRange(from: position, to: position)
        }
        
        textField.sendActions(for: .editingChanged)
        
        return false
    }
}

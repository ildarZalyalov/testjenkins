//
//  BankCardFormatManager.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 25.05.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Тип банковской карты
struct BankCardType {
    
    /// Название типа, например MasterCard
    let name: String
    
    /// Группировка чисел в номере карты, например [4, 4, 4, 4] означает 4 группы по 4 цифры
    let numberGrouping: [Int]
    
    /// Префиксы групп номеров, принадлежащих различным типам карт
    let identifyingDigits: Set<Int>
    
    /// Максимальная длина номера карты
    var maxLength: Int {
        return numberGrouping.reduce(0) { $0 + $1 }
    }
    
    /// Типы карт по умполчанию
    static func obtainDefaultTypes() -> [BankCardType] {
        return [
            BankCardType(name: "American Express", numberGrouping: [4, 6, 5], identifyingDigits: Set([34, 37])),
            BankCardType(name: "China UnionPay", numberGrouping: [4, 4, 4, 4], identifyingDigits: Set([62])),
            BankCardType(name: "Diners Club", numberGrouping: [4, 6, 4], identifyingDigits: Set(300...305).union(Set([36, 38, 39, 309, 2014, 2149]))),
            BankCardType(name: "Discover", numberGrouping: [4, 4, 4, 4], identifyingDigits: Set(622126...622925).union(Set(644...649)).union(Set([6011])).union(Set([65]))),
            BankCardType(name: "JCB", numberGrouping: [4, 4, 4, 4], identifyingDigits: Set(3528...3589).union(Set([3088, 3096, 3112, 3158, 3337]))),
            BankCardType(name: "MasterCard", numberGrouping: [4, 4, 4, 4], identifyingDigits: Set(51...55).union(2221...2720)),
            BankCardType(name: "Visa", numberGrouping: [4, 4, 4, 4], identifyingDigits: Set([4])),
            BankCardType(name: "МИР", numberGrouping: [4, 4, 4, 4], identifyingDigits: Set([22]))
        ]
    }
}

/// Результат форматирования номера карты - форматированная строка и тип карты
typealias BankCardNumberFormatResult = (formattedNumber: String, cardType: BankCardType?)

/// Форматтер номеров банковскиих карт
protocol BankCardFormatManager: UITextFieldDelegate {
    
    /// Разделитель между группами цифр в номере карты, по умолчанию пробел
    var separator: String { get set }
    
    /// Поддерживаемые длины номеров карт, по умолчанию nil
    var acceptableCardNumberLengths: CountableClosedRange<Int>? { get set }
    
    /// Поддерживаемые типы карт, по умолчанию получаются из метода obtainDefaultTypes() типа BankCardType
    var cardTypes: [BankCardType] { get set }
    
    /// Номер карты без форматирования в текущем поле
    var cardNumber: String? { get }
    
    /// Делегат, которому будут проксировать обращения к делегату поля
    /// (кроме textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool)
    var proxyDelegate: UITextFieldDelegate? { get set }
    
    /// Сконфигурировать для поля
    ///
    /// - Parameter textField: текстовое поле
    func configure(with textField: UITextField)
    
    /// Получить строку без форматирования
    ///
    /// - Parameter cardNumber: строка с форматированием
    /// - Returns: строка без форматирования
    func unformat(cardNumber: String) -> String
    
    /// Получить результат форматирования строки
    ///
    /// - Parameter cardNumber: строка без форматирования
    /// - Returns: результат форматирования номера карты
    func format(cardNumber: String) -> BankCardNumberFormatResult
}

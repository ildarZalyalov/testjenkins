//
//  ChatMessageStringBuilder.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 01.08.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import ChattoAdditions

/// Форматтер строк для чата
protocol ChatMessageStringBuilder {
    
    /// Получить строку с представлением даты для сепаратора с датой
    ///
    /// - Parameter date: дата
    /// - Returns: строка
    func relativeDateString(for date: Date) -> String?
    
    /// Получить строку с представлением статуса сообщения
    ///
    /// - Parameter status: статус
    /// - Returns: строка
    func statusString(for status: MessageStatus) -> String?
}

//
//  ChatMessageStringBuilderImplementation.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 01.08.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import ChattoAdditions

class ChatMessageStringBuilderImplementation: ChatMessageStringBuilder {
    
    fileprivate typealias Class = ChatMessageStringBuilderImplementation
    
    fileprivate static var calendar = Calendar.autoupdatingCurrent
    fileprivate static var timeZone = TimeZone.autoupdatingCurrent
    
    fileprivate static let localeIdentifier = "ru_RU"
    fileprivate static var formatter: DateFormatter = {
        
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: localeIdentifier)
        formatter.timeZone = timeZone
        
        return formatter
    }()
    
    func relativeDateString(for date: Date) -> String? {
        
        guard !Class.calendar.isDateInToday(date) else {
            Class.formatter.dateFormat = "HH:mm"
            return Class.formatter.string(from: date)
        }
        
        guard !Class.calendar.isDateInYesterday(date) else {
            Class.formatter.dateFormat = "вчера в HH:mm"
            return Class.formatter.string(from: date)
        }
        
        let currentYear = Class.calendar.component(.year, from: Date())
        let year = Class.calendar.component(.year, from: date)
        
        guard year < currentYear else {
            Class.formatter.dateFormat = "d MMMM"
            return Class.formatter.string(from: date)
        }
        
        Class.formatter.dateFormat = "dd.MM.yy"
        return Class.formatter.string(from: date)
    }
    
    func statusString(for status: MessageStatus) -> String? {
        switch status {
        case .failed:
            return "Не отправлено"
        case .sending:
            return "Отправляется..."
        default:
            return nil
        }
    }
}

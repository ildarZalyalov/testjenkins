//
//  PhoneTextFieldFormatManager.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 25.12.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Возможные паттерны вывода номера телефона
///
/// - defaultPhone: дефолтный со скобками и +7 впереди
/// - defaultWithBrackets: дефолтный со скобками
/// - phoneWithoutBrackets: без скобок
/// - bankCardDate: дата окончания срока банковской карты
/// - bankCvv: код безопасности банковской карты
enum TextFieldFormatPattern: String {
    case defaultPhone = "+7 (***) ***-**-**"
    case defaultPhoneWithBrackets = "(***) ***-**-**"
    case phoneWithoutBrackets = "*** ***-**-**"
    case bankCardDate = "** / **"
    case bankCvv = "***"
}

/// Префикс, который не будет изменяться
///
/// - none: нет
/// - defaultPhone: префикс телефона - +7
enum TextFieldFormatPrefix: String {
    case none = ""
    case defaultPhone = "+7 "
}

/// Протокол, который умеет форматировать текст из текущего
protocol TextFieldFormatManager: UITextFieldDelegate {
    
    /// Кол-во символов в строке помимо плейсхолдера, которые нельзя удалить, по умолчанию 0
    var stopCharacterRemovingCount: Int { get set }
    
    /// Префикс, который не будет изменяться, по умолчанию .none
    /// Для паттерна defaultPhone автоматически выставляется в defaultPhone
    var staticPrefix: TextFieldFormatPrefix { get set }
    
    /// Делегат, которому будут проксировать обращения к делегату поля
    /// (кроме textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool)
    var proxyDelegate: UITextFieldDelegate? { get set }
    
    /// Сконфигурировать для поля
    ///
    /// - Parameters:
    ///   - textField: текстовое поле
    ///   - mask: паттерн
    func configure(with textField: UITextField, mask: TextFieldFormatPattern?)
    
    /// Обновить текущее поле по маске
    func reloadCurrentTextFieldByMask()
}

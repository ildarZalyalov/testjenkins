//
//  PhoneTextFieldFormatManager.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 25.12.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class TextFieldFormatManagerImplementation: NSObject, TextFieldFormatManager {
    
    /// паттерн к которому будут подстраиваться слова/числа Пример: (***)**-**-**
    fileprivate var maskPattern: TextFieldFormatPattern = .defaultPhoneWithBrackets
    
    /// символ, который будет заменяться в паттерне
    fileprivate var replacementChar: String = "*"
    
    /// разрешить ввод текста
    fileprivate var allowText: Bool = false
    
    /// разрешить ввод цифр
    fileprivate var allowNumbers: Bool = true
    
    fileprivate weak var currentTextField: UITextField!
    
    //MARK: - TextFieldFormatManager
    
    var stopCharacterRemovingCount = 0
    
    var staticPrefix: TextFieldFormatPrefix = .none
    
    weak var proxyDelegate: UITextFieldDelegate?
    
    func configure(with textField: UITextField, mask: TextFieldFormatPattern?) {
        
        maskPattern = mask ?? .defaultPhoneWithBrackets
        currentTextField = textField
        currentTextField.addTarget(self, action: #selector(textDidChange), for: .editingChanged)
        
        if maskPattern == .defaultPhone {
            staticPrefix = .defaultPhone
        }
        else if maskPattern == .defaultPhoneWithBrackets {
            staticPrefix = .none
        }
    }
    
    func reloadCurrentTextFieldByMask() {
        
        guard currentTextField != nil else { return }
        textDidChange()
    }
    
    //MARK: - Приватные методы
    
    private func prepareString(_ string: String) -> String {
        
        var charSet: CharacterSet?
        
        if allowText && allowNumbers {
            charSet = CharacterSet.alphanumerics.inverted
        }
        else if allowText {
            charSet = CharacterSet.letters.inverted
        }
        else if allowNumbers {
            charSet = CharacterSet.decimalDigits.inverted
        }
        
        guard let currentCharSet = charSet else { return string }
        
        var tempString = string
        
        if !staticPrefix.rawValue.isEmpty && tempString.contains(staticPrefix.rawValue) {
            tempString = String(tempString[staticPrefix.rawValue.endIndex...])
        }
        
        let result = tempString.components(separatedBy: currentCharSet)
        
        return staticPrefix.rawValue + result.joined(separator: "")
    }
    
    //MARK: - UITextFieldDelegate
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        return proxyDelegate?.textFieldShouldClear?(textField) ?? true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return proxyDelegate?.textFieldShouldReturn?(textField) ?? true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return proxyDelegate?.textFieldShouldBeginEditing?(textField) ?? true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        proxyDelegate?.textFieldDidBeginEditing?(textField)
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return proxyDelegate?.textFieldShouldBeginEditing?(textField) ?? true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        proxyDelegate?.textFieldDidEndEditing?(textField)
    }
    
    @available(iOS 10.0, *)
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextFieldDidEndEditingReason) {
        proxyDelegate?.textFieldDidEndEditing?(textField, reason: reason)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if string.isEmpty, let text = textField.text, text == staticPrefix.rawValue {
            return false
        }
        
        if string.isEmpty, let text = textField.text, text.count == staticPrefix.rawValue.count + stopCharacterRemovingCount {
            return false
        }
        
        return true
    }
    
    @objc func textDidChange() {
        
        guard let text = currentTextField.text else {
            return
        }
        
        let pattern = self.maskPattern.rawValue
        
        guard !text.isEmpty, !pattern.isEmpty else { return }
        
        var finalText   = ""
        var stop        = false
        let tempString  = prepareString(text)
        
        var formatIndex = pattern.startIndex
        var tempIndex   = staticPrefix.rawValue.isEmpty ? tempString.startIndex : staticPrefix.rawValue.endIndex
        
        DispatchQueue.global(qos: .userInteractive).async { [weak self] in
            
            guard let strongSelf = self else { return }
            
            while !stop {
                
                let formattingPatternRange = formatIndex ..< pattern.index(formatIndex, offsetBy: 1)
                
                if pattern[formattingPatternRange.lowerBound..<formattingPatternRange.upperBound] != String(strongSelf.replacementChar) {
                    
                    finalText += pattern[formattingPatternRange.lowerBound..<formattingPatternRange.upperBound]
                    
                } else if tempString.count > 0 {
                    
                    let pureStringRange = tempIndex ..< tempString.index(tempIndex, offsetBy: 1)
                    finalText += tempString[pureStringRange.lowerBound..<pureStringRange.upperBound]
                    tempIndex = tempString.index(tempIndex, offsetBy: 1)
                }
                
                formatIndex = pattern.index(formatIndex, offsetBy: 1)
                
                stop = formatIndex >= pattern.endIndex || tempIndex >= tempString.endIndex
                
                DispatchQueue.main.async { [weak self] in
                    self?.currentTextField.text = finalText
                }
            }
        }
    }
}

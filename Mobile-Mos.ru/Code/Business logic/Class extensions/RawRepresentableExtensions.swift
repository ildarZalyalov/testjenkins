//
//  RawRepresentableExtensions.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 10.07.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

extension RawRepresentable {
    
    /// Создаем экземпляр enum из опционального rawValue
    init?(rawValueOptional: Self.RawValue?) {
        
        guard let rawValue = rawValueOptional else {
            return nil
        }
        
        self.init(rawValue: rawValue)
    }
}

//
//  DoubleExtensions.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 09.02.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

extension Double {
    
    func roundToDecimal(_ fractionDigits: Int) -> Double {
        let multiplier = pow(10, Double(fractionDigits))
        return Darwin.round(self * multiplier) / multiplier
    }
}

//
//  YMKPointExtension.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 09.11.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

extension YMKPoint: MKAnnotation {
    
    public var coordinate: CLLocationCoordinate2D {
        return CLLocationCoordinate2DMake(self.latitude, self.longitude)
    }
}


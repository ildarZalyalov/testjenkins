//
//  UIDeviceExtensions.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 04.08.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/**
 Тип экрана устройства (различаем классы устройств по размеру экрана)
 
 - iPhone4:             телефон с шириной экрана 320pt и диагональю от 3,5 до 4 дюймов
 - iPhone5:             телефон с шириной экрана 320pt и диагональю от 4 до 4,7 дюймов
 - iPhone6:             телефон с шириной экрана 375pt и диагональю от 4,7 до 5,5 дюймов
 - iPhoneX:             телефон с шириной экрана 375pt и диагональю 5,8 дюймов
 - iPhone6Plus:         телефон с шириной экрана 414pt и диагональю от 5,5 и больше дюймов
 - iPad:                планшет с диагональю от 9,7 и больше дюймов
 - Unknown:             Неизвестно
 */
enum DeviceScreenType {
    case iPhone4
    case iPhone5
    case iPhone6
    case iPhoneX
    case iPhone6Plus
    case iPad
    case Unknown
}

extension UIDevice {
    
    /// Определяем класс размера экрана устройства
    var deviceScreenType: DeviceScreenType {
        
        guard self.userInterfaceIdiom == .phone else {
            return .iPad
        }
        
        let screenSize = UIScreen.main.orientationIndependentSize
        
        let compactScreenWidth: CGFloat = 320
        let regularScreenWidth: CGFloat = 375
        let largeScreenWidth: CGFloat = 414
        
        let iPhone5ScreenHeight: CGFloat = 568
        let iPhone6ScreenHeight: CGFloat = 667
        let iPhone6PlusScreenHeight: CGFloat = 736
        let iPhoneXScreenHeight: CGFloat = 812
        
        switch screenSize.width {
        case compactScreenWidth ..< regularScreenWidth :
            switch screenSize.height {
            case ..<iPhone5ScreenHeight:
                return .iPhone4
            case iPhone5ScreenHeight...:
                return .iPhone5
            default:
                return .Unknown
            }
        case regularScreenWidth ..< largeScreenWidth :
            switch screenSize.height {
            case iPhone6ScreenHeight ..< iPhoneXScreenHeight:
                return .iPhone6
            case iPhoneXScreenHeight...:
                return .iPhoneX
            default:
                return .Unknown
            }
        case largeScreenWidth...:
            switch screenSize.height {
            case iPhone6PlusScreenHeight...:
                return .iPhone6Plus
            default:
                return .Unknown
            }
        default:
            return .Unknown
        }
    }
    
    /// Определяем обладает ли устройство компактным дисплеем (экран 4 дюйма и меньше)
    var hasCompactScreen: Bool {
        return deviceScreenType == .iPhone4
    }
    
    /// Определяем обладает ли устройство маленьким размером
    var hasConfinedScreen: Bool {
        return deviceScreenType == .iPhone4 ||
               deviceScreenType == .iPhone5
    }
    
    /// Инсталяционный идентификатор устройства (уникален, меняется при переустановке приложения)
    var deviceInstallationIdentificator: String {
        
        let userDefaults = UserDefaults.standard
        let key = ApplicationUserDefaultsKey.deviceInstallationIdentificator.rawValue
        var identificator = userDefaults.string(forKey: key)
        
        if identificator == nil {
            
            identificator = UUID().uuidString.lowercased()
            userDefaults.set(identificator, forKey: key)
            userDefaults.synchronize()
        }
        
        return identificator!
    }
    
}

//
//  YMKMapViewExtension.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 17.03.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Длительность анимированного перемещения камеры карты
fileprivate let cameraMoveDuration: Float = 1.0

/// Длительность анимаций пинов на карте
fileprivate let pinAnimationDuration: Float = 0.2

extension YMKMapView{
    
    /// Карта
    var map: YMKMap {
        get {
            return self.mapWindow.map!
        }
    }
    
    /// Текущая позиция камеры
    var cameraPosition: YMKCameraPosition {
        get { return self.map.cameraPosition }
    }
    
    /// Объекты карты
    var mapObjects: YMKMapObjectCollection {
        get { return self.map.mapObjects! }
    }
    
    /// Добавить точку на карту
    ///
    /// - Parameter point: YMKPoint точка, которую нужно добавить на карту(в массив с объектами mapObjects)
    func addMapObject(with point: YMKPoint, andObjectData userData: Any?) -> YMKPlacemarkMapObject {
        
        let mapPlaceMarkObject = mapObjects.addPlacemark(with: point) 
       
        mapPlaceMarkObject?.setVisibleWithVisible(false, animation: YMKAnimation(type: .linear, duration: 0), callback: nil)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + Double(pinAnimationDuration)) {
            
            guard let placemark = mapPlaceMarkObject, placemark.isValid else { return }
            
            mapPlaceMarkObject?.setVisibleWithVisible(true, animation: YMKAnimation(type: .smooth, duration: pinAnimationDuration), callback: nil)
        }
        
        mapPlaceMarkObject!.userData = userData
        
        //Форсим, потому что если YMKPlacemarkMapObject nil после добавления на карту - у нас проблемы.
        return mapPlaceMarkObject!
    }
    
    /// Удаляет конкретную точку YMKMapObject из массива объектов mapObjects у карты
    ///
    /// - Parameter mapObject: YMKMapObject - конкретный объект из массива
    func remove(mapObject: YMKMapObject){
        mapObjects.remove(with: mapObject)
    }
    
    /// Очищает карту от всех mapObjects объектов
    func clearMapObjects(){
        mapObjects.clear()
    }
    
    /// Очистить карту от точек из массива
    /// Позволяет тригерить метод делегата YMKMapObjectCollectionListener чтобы узнать о удалении точки с карты
    ///
    /// - Parameter objects: массив точек карты добавленных на карту
    func clearMapObjects(with objects: [YMKPlacemarkMapObject]) {
        objects.forEach{ mapObjects.remove(with: $0)}
    }
    
    
    /// Передвежение карты на указанную latitute, longitute
    ///
    /// - Parameters:
    ///   - latitute: Double долгота
    ///   - longitute: Double широта
    ///   - zoom: Float зуминг камеры, где 0 - самая высокая точка обозреваемая весь мир
    ///   - azimuth: Float азимут камеры, угол между серверным вектором и текущим направлением камеры на карте, в градусах [0, 360)
    ///   - tilt: Угол наклона в градусах, 0 означает вертикальное прямое
    ///   - animated: Применять ли анимацию при смене камеры
    ///   - callBack: блок завершения анимации передвижения
    ///   - isItemInfoCardOpen: открыта ли карточка в данный момент (если открыта, то центруется на свободной от карточки места)
    ///   - viewportBottomInset: отступ снизу для центрирования камеры на открытом участке карты
    func moveCameraPosition(to latitute: Double,
                            longitute: Double,
                            zoom: Float,
                            azimuth: Float = 0,
                            tilt: Float = 0,
                            animated: Bool = true,
                            isItemInfoCardOpen: Bool = false,
                            viewportBottomInset: CGFloat = 0,
                            callBack: YMKMapCameraCallback? = nil) {
        
        var targetPosition = YMKPoint(latitude: latitute, longitude: longitute)
        
        if isItemInfoCardOpen {
            guard let toWorldPosition = mapWindow.worldToScreen(withWorldPoint: targetPosition),
                let finalPosition = mapWindow.screenToWorld(with: YMKScreenPoint(x: toWorldPosition.x, y: toWorldPosition.y + Float(viewportBottomInset/2)))
                else {
                    return
            }
            targetPosition = finalPosition
        }
        
        let cameraPosition = YMKCameraPosition(target:targetPosition,
                                               zoom: zoom,
                                               azimuth: azimuth,
                                               tilt: tilt)
        
        let animationType = animated ? YMKAnimation(type: .smooth, duration: cameraMoveDuration) : YMKAnimation(type: .linear, duration: 0)
    
        map.move(with: cameraPosition, animationType: animationType, cameraCallback: callBack)
    }
    
    /// Обновление место положения камеры по BoundingBox элементов (points)
    ///
    /// - Parameters:
    ///   - points: [YMKPoint] массив точек для отображения на карте
    ///   - cameraCallBack: YMKMapCameraCallBack блок возвращающий true/false после передвижения камеры
    func updateCameraPosition(with points: [YMKPoint], cameraCallBack: YMKMapCameraCallback? = nil) {
        
        var latMax  = Double.leastNormalMagnitude
        var latMin  = Double.greatestFiniteMagnitude
        var longMax = Double.leastNormalMagnitude
        var longMin = Double.greatestFiniteMagnitude
        
        for point in points {
            
            latMax  = max(latMax,  point.latitude)
            latMin  = min(latMin,  point.latitude)

            longMax = max(longMax, point.longitude)
            longMin = min(longMin, point.longitude)
        }
        
        
        let southWestPoint = YMKPoint(latitude: Double(latMax), longitude: Double(longMax))
        let northWestPoint = YMKPoint(latitude: Double(latMin), longitude: Double(longMin))
        
        let boundingBox = YMKBoundingBox(southWest: southWestPoint,
                                         northEast: northWestPoint)
        
        var cameraPosition = map.cameraPosition(with: boundingBox)
        
        cameraPosition = YMKCameraPosition(target: cameraPosition.target,
                                           zoom: cameraPosition.zoom - 1,
                                           azimuth: cameraPosition.azimuth,
                                           tilt: cameraPosition.tilt)
        
        
        map.move(with: cameraPosition, animationType: YMKAnimation(type: .smooth, duration: cameraMoveDuration)) { (isFinished) in
            
            guard isFinished else { return }
            
            if let completionHandler = cameraCallBack {
                
               completionHandler(isFinished)
            }
        }
    }
    
    /// Обновить настройку отображения ночного режима
    func updateMapNightMode() {
        map.isNightModeEnabled = UserDefaults.standard.bool(forKey: ApplicationUserDefaultsKey.isMapDark.rawValue)
    }
    
    /// Обновить настройку отображения информации о пробках
    func updateMapTrafficJam() {
        map.trafficLayer?.setTrafficVisibleWithOn(UserDefaults.standard.bool(forKey: ApplicationUserDefaultsKey.isTrafficJamEnabled.rawValue))
    }
    
}

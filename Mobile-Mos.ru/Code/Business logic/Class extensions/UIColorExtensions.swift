//
//  UIColorExtensions.swift
//  Portable
//
//  Created by Ivan Erasov on 25.01.17.
//  Copyright © 2017. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    
    //MARK: Из rgb
    
    /// Получить цвет из значений RGB, выраженных числами от 0 до 255
    ///
    /// - Parameters:
    ///   - red: красный компонент
    ///   - green: зеленый компонент
    ///   - blue: синий компонент
    ///   - alpha: альфа
    /// - Returns: полученный цвет
    static func fromRGB(red: CGFloat, green: CGFloat, blue: CGFloat, alpha: CGFloat = 1) -> UIColor {
        return UIColor(red: red / 255.0, green: green / 255.0, blue: blue / 255.0, alpha: alpha)
    }
    
    //MARK: Из строки
    
    /// Получить цвет из шестнадцетиричного строчного представления
    ///
    /// - Parameter hexString: строка с hex записью цвета
    /// - Returns: полученный цвет
    static func fromHexString(hexString: String) -> UIColor? {
        
        var colorString = hexString
        
        if (colorString.hasPrefix("#")) {
            colorString.remove(at: colorString.startIndex)
        }
        
        guard colorString.count == 6 else {
            return nil
        }
        
        var rgbValue: UInt32 = 0
        Scanner(string: colorString).scanHexInt32(&rgbValue)
        
        return fromHex(hex: Int(rgbValue))
    }
    
    //MARK: Из числа
    
    /// Получить цвет из шестнадцетиричного числа
    ///
    /// - Parameters:
    ///   - hex: hex запись цвета
    ///   - decodingAlpha: декодировать ли alpha компонент
    /// - Returns: полученный цвет
    static func fromHex(hex: Int, decodingAlpha: Bool = false) -> UIColor {
        
        let components = (
            alpha: CGFloat((hex >> 24) & 0xff),
            red: CGFloat((hex >> 16) & 0xff),
            green: CGFloat((hex >> 08) & 0xff),
            blue: CGFloat((hex >> 00) & 0xff)
        )
        
        return fromRGB(red: components.red,
                       green: components.green,
                       blue: components.blue,
                       alpha: decodingAlpha ? components.alpha : 1)
    }
    
    /// Получить шестнадцетиричное число из цвета
    ///
    /// - Parameter encodingAlpha: кодировать ли alpha компонент
    /// - Returns: шестнадцетиричное число
    func toHex(encodingAlpha: Bool = false) -> Int {
        
        var alphaComponent: CGFloat = 0
        var redComponent: CGFloat = 0
        var greenComponent: CGFloat = 0
        var blueComponent: CGFloat = 0

        getRed(&redComponent, green: &greenComponent, blue: &blueComponent, alpha: &alphaComponent)
        
        var hex: Int = (Int)(redComponent * 255.0) << 16 | (Int)(greenComponent * 255.0) << 08 | (Int)(blueComponent * 255.0) << 00
        
        if encodingAlpha {
            hex |= (Int)(alphaComponent * 255.0) << 24
        }
        
        return hex
    }
}

//
//  UITableViewExtensions.swift
//  Portable
//
//  Created by Ivan Erasov on 14.02.17.
//  Copyright © 2017. All rights reserved.
//

import Foundation
import UIKit

extension UITableView {
    
    /// Применить обновления источника данных к таблице
    ///
    /// - Parameter updates: обновления источника данных
    func apply(updates: [TableViewDataSourceUpdate]) {
        
        guard !updates.isEmpty else { return }
        
        let updates = { [weak self] in
            
            guard let strongSelf = self else { return }
            
            for update in updates {
                switch update {
                case .insertRows(let indexPaths, let animation):
                    strongSelf.insertRows(at: indexPaths, with: animation)
                case .insertSections(let sections, let animation):
                    strongSelf.insertSections(sections, with: animation)
                case .deleteRows(let indexPaths, let animation):
                    strongSelf.deleteRows(at: indexPaths, with: animation)
                case .deleteSections(let sections, let animation):
                    strongSelf.deleteSections(sections, with: animation)
                case .reloadRows(let indexPaths, let animation):
                    strongSelf.reloadRows(at: indexPaths, with: animation)
                case .reloadSections(let sections, let animation):
                    strongSelf.reloadSections(sections, with: animation)
                case .moveRow(let indexPathFrom, let indexPathTo):
                    strongSelf.moveRow(at: indexPathFrom, to: indexPathTo)
                case .moveSection(let sectionFrom, let sectionTo):
                    strongSelf.moveSection(sectionFrom, toSection: sectionTo)
                }
            }
        }
        
        if #available(iOS 11.0, *) {
            performBatchUpdates(updates, completion: nil)
        }
        else {
            beginUpdates()
            updates()
            endUpdates()
        }
    }
    
    /// Присваиваем таблице пустой заголовок, чтобы в начале не отрисовывался отступ у группированной таблице
    func assignEmptyTableHeaderToHideGroupedTableTopOffset() {
        tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.leastNormalMagnitude))
    }
    
    /// Присваиваем таблице пустой футер, чтобы в конце не отрисовывались бесконечные сепараторы
    func assignEmptyTableFooterToHideEmptyCellsSeparators() {
        tableFooterView = UIView()
    }
    
    /// Регистрируем nib ячейки. Имя nib берется из метода cellNibName cell object ячейки.
    ///
    /// - Parameter cellObjectType: тип cell object ячейки
    func registerCellNib(for cellObjectType: CellObject.Type) {
        let nib = UINib(nibName: cellObjectType.cellNibName(), bundle: nil)
        register(nib, forCellReuseIdentifier: cellObjectType.cellReuseIdentifier())
    }
    
    /// Регистрируем пустой заголовок
    func registerEmptyHeader() {
        register(EmptyTableHeaderView.self, forHeaderFooterViewReuseIdentifier: EmptyTableHeaderObject.headerReuseIdentifier())
    }
    
    /// Регистрируем nib заголовка. Имя nib берется из метода headerNibName header object заголовка.
    ///
    /// - Parameter headerObjectType: тип header object заголовка
    func registerHeaderNib(for headerObjectType: HeaderObject.Type) {
        let nib = UINib(nibName: headerObjectType.headerNibName(), bundle: nil)
        register(nib, forHeaderFooterViewReuseIdentifier: headerObjectType.headerReuseIdentifier())
    }
    
    /// Регистрируем пустой футер
    func registerEmptyFooter() {
        register(EmptyTableFooterView.self, forHeaderFooterViewReuseIdentifier: EmptyTableFooterObject.footerReuseIdentifier())
    }
    
    /// Регистрируем nib футера. Имя nib берется из метода footerNibName footer object футера.
    ///
    /// - Parameter footerObjectType: тип footer object футера
    func registerFooterNib(for footerObjectType: FooterObject.Type) {
        let nib = UINib(nibName: footerObjectType.footerNibName(), bundle: nil)
        register(nib, forHeaderFooterViewReuseIdentifier: footerObjectType.footerReuseIdentifier())
    }
}

//
//  NSRangeExtensions.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 01.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

extension NSRange {
    
    /// Индекс последнего элемента в интервале
    var endLocation: Int {
        return location + length - 1
    }
    
    /// Сдвинуть интервал
    ///
    /// - Parameter offset: сдвиг
    /// - Returns: сдвинутый интервал
    func offset(by offset: Int) -> NSRange {
        return NSMakeRange(location + offset, length)
    }
    
    /// Ужать интервал
    ///
    /// - Parameter inset: на сколько ужать
    /// - Returns: ужатый интервал
    func inset(by inset: Int) -> NSRange {
        return NSMakeRange(location + inset, length - 2 * inset)
    }
    
    /// Ужать интервал в начале
    ///
    /// - Parameter inset: на сколько ужать
    /// - Returns: ужатый интервал
    func insetOnStartLocation(by inset: Int) -> NSRange {
        return NSMakeRange(location + inset, length - inset)
    }
    
    /// Ужать интервал в конце
    ///
    /// - Parameter inset: на сколько ужать
    /// - Returns: ужатый интервал
    func insetOnEndLocation(by inset: Int) -> NSRange {
        return NSMakeRange(location, length - inset)
    }
}

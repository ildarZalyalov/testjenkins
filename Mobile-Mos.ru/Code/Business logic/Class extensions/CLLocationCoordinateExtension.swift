//
//  CLLocationCoordinateExtension.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 06.02.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

extension CLLocationCoordinate2D: Equatable {
    
    public static func ==(lhs: CLLocationCoordinate2D, rhs: CLLocationCoordinate2D) -> Bool {
        return lhs.latitude == rhs.latitude &&
               lhs.longitude == rhs.longitude
    }
    
    /// Проверить эквивалентность с округлением до n чисел после запятой
    ///
    /// - Parameters:
    ///   - fractionDigits: кол-во знаков после запятой
    ///   - rhs: координата для проверки
    /// - Returns: тру/фалс
    public func roundedCheckEqual(withDigitsAfterZero fractionDigits:Int, rhs: CLLocationCoordinate2D) -> Bool {
        
        let lhsLatitudeDouble = Double(self.latitude).roundToDecimal(fractionDigits)
        let lhsLongitudeDouble = Double(self.longitude).roundToDecimal(fractionDigits)
        let rhsLatitudeDouble = Double(rhs.latitude).roundToDecimal(fractionDigits)
        let rhsLongitudeDouble = Double(rhs.longitude).roundToDecimal(fractionDigits)
        
        return lhsLatitudeDouble == rhsLatitudeDouble &&
               lhsLongitudeDouble == rhsLongitudeDouble
    }
}


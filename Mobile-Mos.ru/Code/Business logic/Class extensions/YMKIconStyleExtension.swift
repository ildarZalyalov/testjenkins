//
//  YMKIconStyleExtension.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 21.04.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Перечесление, описывающае доступны ли повороты
///
/// - noRotation: не доступны
/// - rotate: доступны
enum RotationType: NSNumber {
    case noRotation = 0
    case rotate     = 1
}

/// Тип отображения
///
/// - onEarth: относительно земли
/// - onScreen: относительно экрана (по дефолту)
enum DisplayedSurface: NSNumber {
    case onEarth    = 0
    case onScreen   = 1
}

/// Отображется ли иконка объекта карты в данный момент
///
/// - unvisible: не видна
/// - visible: видна (по дефолту)
enum IconVisibility: NSNumber {
    case unvisible  = 0
    case visible    = 1
}

extension YMKIconStyle{
   
    /// Инициализрует YMKIconStyle
    ///
    /// - Parameters:
    ///   - anchor: угол поворота относительно земли
    ///   - rotationType: RotationType тип вращения иконки, по умолчанию стоит nil
    ///   - zIndex: NSNumber если значок распологается относитьельно оси Z
    ///   - flat: flat: DisplayedSurface
    ///                .onEarth, значок отображается относительно поверхности Земли.
    ///                .onScreen, значок отображается отностиельно экрана.(дефолт)
    ///   - visible: IconVisibility, Видна ли иконка. По дефолту .visible
    ///   - scale: Double, Масштаб иконки. По дефолту 1.0
    convenience init(with anchor: NSValue? = nil, rotationType: RotationType? = nil, zIndex: NSNumber? = nil, flat: DisplayedSurface? = .onScreen, visible: IconVisibility? = .visible, scale: Double? = 1.0) {
        self.init()
        self.anchor = anchor
        self.rotationType = rotationType?.rawValue
        self.zIndex = zIndex
        self.flat = flat?.rawValue
        self.visible = visible?.rawValue
        self.scale = scale as NSNumber?
    }
    
}


//
//  ErrorExtensions.swift
//  Portable
//
//  Created by Ivan Erasov on 10.12.15.
//  Copyright © 2015. All rights reserved.
//

import Foundation

extension Error {
    
    /// Является ли ошибка ошибкой отсутствия соединения с интернетом
    func isNoInternetConnectionError() -> Bool {
        let error = self as NSError
        return error.domain == NSURLErrorDomain && error.code == NSURLErrorNotConnectedToInternet
    }
    
    /// Появилась ли ошибка в результате отмены запроса
    func isNetworkRequestCancelled() -> Bool {
        let error = self as NSError
        return error.domain == NSURLErrorDomain && error.code == NSURLErrorCancelled
    }
}

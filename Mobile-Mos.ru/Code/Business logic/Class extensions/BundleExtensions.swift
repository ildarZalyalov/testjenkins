//
//  BundleExtensions.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 08.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

extension Bundle {
    
    /// Отображаемое имя приложения
    var appDisplayName: String? {
        return infoDictionary?["CFBundleDisplayName"] as? String
    }
    
    /// Короткая "релизная" версия приложения (без номера билда)
    var releaseVersionNumber: String? {
        return infoDictionary?["CFBundleShortVersionString"] as? String
    }
    
    /// Номер билда
    var buildVersionNumber: String? {
        return infoDictionary?["CFBundleVersion"] as? String
    }
    
    /// Проверяем, что версия меньше другой версии
    ///
    /// - Parameters:
    ///   - version: версия
    ///   - anotherVersion: версия, с которой сравниваем
    /// - Returns: true, если версия меньше
    static func compare(version: String, isLessThan anotherVersion: String) -> Bool {
        let compareResult = version.compare(anotherVersion, options: .numeric, range: nil, locale: nil)
        return compareResult == .orderedAscending
    }
    
    /// Проверяем, что версия больше или равна другой версии
    ///
    /// - Parameters:
    ///   - version: версия
    ///   - anotherVersion: версия, с которой сравниваем
    /// - Returns: true, если версия больше или равна версии, с которой сравнивали
    static func compare(version: String, isGreaterThanOrEqual anotherVersion: String) -> Bool {
        let compareResult = version.compare(anotherVersion, options: .numeric, range: nil, locale: nil)
        return compareResult != .orderedAscending
    }
}

//
//  StringExtensions.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 26.07.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

extension String {
    
    /// Размер строки при рендеринге с определенным шрифтом
    ///
    /// - Parameter font: шрифт
    /// - Parameter width: максимальная ширина строки
    /// - Returns: размер строки
    func sizeOfString(with font: UIFont, constrainedTo width: CGFloat = 0) -> CGSize {
        if width > 0 {
            let boundingSize = CGSize(width: width, height: CGFloat.greatestFiniteMagnitude)
            return (self as NSString).boundingRect(with: boundingSize,
                                                   options: .usesLineFragmentOrigin,
                                                   attributes: [NSAttributedStringKey.font: font],
                                                   context: nil).size
        }
        else {
            return (self as NSString).size(withAttributes: [NSAttributedStringKey.font: font])
        }
    }
    
    /// Сделать первую букву в строке заглавной
    ///
    /// - Returns: строку с первой заглавной буквой
    func firstLetterCapitalized() -> String {
        
        guard !isEmpty else { return self }
        
        let firstIndex = index(startIndex, offsetBy: 1)
        return self[..<firstIndex].capitalized + self[firstIndex...].lowercased()
    }
    
    /// Удалить все пробелы из строки
    ///
    /// - Returns: строка без пробелов
    func trimWhiteSpaces() -> String {
        return components(separatedBy: .whitespaces).joined()
    }
    
    /// Удалить все символы кроме определенного типа (прим. оставить только числа в номере телефона)
    ///
    /// - Parameter characterSet: Тип символов, который должен остаться
    /// - Returns: строка без лишних символов
    func trimAll(exeptFor characterSet: CharacterSet) -> String {
        return components(separatedBy: characterSet).joined(separator: String())
    }
}

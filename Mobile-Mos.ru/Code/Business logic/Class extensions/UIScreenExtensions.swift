//
//  UIScreenExtensions.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 27.07.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

extension UIScreen {
    
    /// Проверка на retina экран
    var isRetina: Bool {
        return scale > 1
    }
    
    var orientationIndependentSize: CGSize {
        
        var size = nativeBounds.size
        
        size.width /= nativeScale
        size.height /= nativeScale
        
        return size
    }
}

//
//  CLLocationManagerExtensions.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 05.09.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import CoreLocation

extension CLLocationManager {
    
    /// Доступны ли для использования сервисы геолокации - включены ли вобще + авторизованы для приложения
    /// Если статус авторизации еще не определен, вернется nil
    static var areLocationServicesAvailable: Bool? {
        
        if CLLocationManager.locationServicesEnabled() {
            
            switch(CLLocationManager.authorizationStatus()) {
            case .notDetermined:
                return nil
            case .restricted, .denied:
                return false
            case .authorizedAlways, .authorizedWhenInUse:
                return true
            }
            
        } else {
            return false
        }
    }
}

//
//  NSAttributedStringExtension.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 11.12.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

extension NSMutableAttributedString {
    
    /// Создать строку с выделенной подстрокой
    ///
    /// - Parameters:
    ///   - fullString: полная строка
    ///   - subString: подстрока
    ///   - subStringColor: цвет подстроки
    convenience init(with fullString: String,
                     subString: String,
                     subStringColor : UIColor) {
        
        self.init(string: fullString)
        
        let range = mutableString.range(of: subString, options:NSString.CompareOptions.caseInsensitive)
        if range.location != NSNotFound {
            addAttribute(NSAttributedStringKey.foregroundColor, value: subStringColor, range: range);
        }
    }
    
}


//
//  LAErrorExtension.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 10.07.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import LocalAuthentication

extension LAError {
    
    var localizedErrorDescription: String {
        get{
            var errorText = ""
            
            switch(self.code) {
            case LAError.authenticationFailed:
                errorText = "Трудности с индетификацией личности."
            case LAError.userCancel:
                errorText = "Авторизация отменена пользователем"
                
            case LAError.userFallback:
                errorText = "Пользователь нажал кнопку отмена"
            case LAError.systemCancel:
                errorText = "Авторизация отменена системой"
            case LAError.passcodeNotSet:
                errorText = "Кода Touch ID пользователя нет в системе"
            case LAError.touchIDNotAvailable:
                errorText = "Touch ID не поддерживается этим устройством"
            case LAError.touchIDNotEnrolled:
                errorText = "Touch ID не распознал палец"
            case LAError.touchIDLockout:
                errorText = "Слишком много попыток входа по Touch ID. Вход по Touch ID временно заблокирован."
            case LAError.appCancel:
                errorText = "Авторизация отменена приложением."
            case LAError.invalidContext:
                errorText = "Авторизация уже успешно пройдена."
                
            default:
                errorText = "Touch ID скорее всего не сконфигурирован."
                break
            }
            
            return errorText
        }
    }
}

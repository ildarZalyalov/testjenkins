//
//  UIImageExtensions.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 31.07.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import UIKit

extension UIImage {
    
    /// Создать картинку из файла с учетом разрешения устройства
    ///
    /// - Parameter name: название файла
    /// - Parameter type: расширение файла
    /// - Parameter bundle: bundle, в котором расположен файл
    /// - Returns: картинка или nil, если не удалось найти файл
    convenience init?(contentsOfFileWithName name: String, ofType type: String, in bundle: Bundle = Bundle.main) {
        
        var fileName = name
        let scale = UIScreen.main.scale
        
        if scale >= 2 {
            fileName += "@2x"
        }
        else if scale >= 3 {
            fileName += "@3x"
        }
        
        guard let path = bundle.path(forResource: fileName, ofType: type) else {
            return nil
        }
        
        self.init(contentsOfFile: path)
    }
    
    /// Загрузить набор кадров для анимации
    ///
    /// - Parameters:
    ///   - name: базовое имя каждого кадра, имена файлов с кадрами должны быть сформированы по типу "name" + номер кадра
    ///   - framesCount: количество кадров
    /// - Returns: набор кадров для анимации
    static func imageSequence(with name: String, framesCount: Int) -> [UIImage] {
        
        var frames = [UIImage?]()
        
        for frameIndex in 0 ..< framesCount {
            frames.append(UIImage(named: name + "\(frameIndex)"))
        }
        
        return frames.compactMap { $0 }
    }
}

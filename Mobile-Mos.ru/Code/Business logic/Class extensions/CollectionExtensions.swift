//
//  CollectionExtensions.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 28.07.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

extension Sequence {
    
    /// Преобразовать в словарь
    ///
    /// - Parameter keyObtainer: блок получения ключа для элемента
    /// - Returns: словарь
    func toDictionary<Key: Hashable>(with keyObtainer: (Iterator.Element) -> Key) -> [Key : Iterator.Element] {
        var dictionary: [Key : Iterator.Element] = [:]
        self.forEach { dictionary[keyObtainer($0)] = $0 }
        return dictionary
    }
    
    /// Сгруппировать элементы последовательности по совпадению ключа группы. Если ключ группы для элемента получить не удалось, то элемент не входит ни в одну из полученных групп.
    ///
    /// - Parameter groupKeyObtainer: блок получения ключа группы
    /// - Returns: словарь с группами
    func group<Key: Hashable>(by groupKeyObtainer: (Iterator.Element) -> Key?) -> [Key : [Iterator.Element]] {
        
        var groups: [Key : [Iterator.Element]] = [:]
        
        for element in self {
            
            guard let key = groupKeyObtainer(element) else { continue }
            
            if groups[key] == nil {
                groups[key] = []
            }
            
            groups[key]?.append(element)
        }
        
        return groups
    }
}

extension Collection {
    
    /// Безопасный доступ к элементу по индексу
    ///
    /// - Parameter index: индекс элемента
    subscript(safe index: Index) -> Iterator.Element? {
        return indices.contains(index) ? self[index] : nil
    }
}

extension Collection where Index: Hashable {
    
    /// Преобразовать в словарь, в котором элементы коллекции лежат по индексам
    ///
    /// - Returns: словарь с элементами по индексу
    func toIndicesDictionary() -> [Index : Element] {
        return Dictionary(uniqueKeysWithValues: zip(indices, self))
    }
}

extension Array {
    
    /// Выбрать первый с конца элемент, удовлетворяющий условию
    ///
    /// - Parameter shouldReturn: условие выборки
    /// - Returns: подходящий элемент массива
    func last(where shouldReturn: (Element) -> Bool) -> Element? {
        
        // чтобы вызвался вариант метода, который не выделяет память, а то в протоколах определены еще реализации
        let reversedSelf: ReversedCollection<Array<Element>> = reversed()
        
        for element in reversedSelf {
            if shouldReturn(element) {
                return element
            }
        }
        
        return nil
    }
    
    /// Tuple из индекса элемента и самого элемента
    typealias ElementAndIndex = (index: Index, element: Element)
    
    /// Выбрать первый с конца элемент и его индекс, удовлетворяющий условию
    ///
    /// - Parameter shouldReturn: условие выборки
    /// - Returns: подходящий элемент массива и его индекс
    func lastElementAndIndex(where shouldReturn: (Element) -> Bool) -> ElementAndIndex? {
        
        // чтобы вызвался вариант метода, который не выделяет память, а то в протоколах определены еще реализации
        let reversedSelf: ReversedCollection<Array<Element>> = reversed()
        
        // enumerated() собирает новую коллекцию с индексами, в нашем случае индекс проще посчитать
        var currentIndex = count - 1
        
        for element in reversedSelf {
            
            if shouldReturn(element) {
                return (currentIndex, element)
            }
            
            currentIndex -= 1
        }
        
        return nil
    }
    
    /// Итерируем по массиву, изменяя элементы
    ///
    /// - Parameter mutation: обработчик изменения, принимает ссылку на элемент массива
    /// - Throws: перебрасывает исключение, которое может сгенерировать обработчик
    mutating func forEachMutate(_ mutation: (inout Element) throws -> Void) rethrows {
        try self = self.map {
            var mutatingElement = $0
            try mutation(&mutatingElement)
            return mutatingElement
        }
    }
}

extension Dictionary {
    
    /// Удалить значения по ключам
    ///
    /// - Parameter keys: набор ключей
    mutating func removeValues(for keys: [Key]) {
        keys.forEach { self.removeValue(forKey: $0) }
    }
    
    /// Итерируем по словарю, изменяя элементы
    ///
    /// - Parameter mutation: обработчик изменения, принимает ссылку на элемент словаря
    /// - Throws: перебрасывает исключение, которое может сгенерировать обработчик
    mutating func forEachMutate(_ mutation: (inout Value) throws -> Void) rethrows {
        try self = self.reduce([Key : Value](), { result, element in
            var mutatingElement = element.value
            try mutation(&mutatingElement)
            return result + [element.key : mutatingElement]
        })
    }
}


//
//  UIApplicationExtensions.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 11.07.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

extension UIApplication {
    
    /// Открыть ссылку (метод для обратной поддержки deprecated метода)
    ///
    /// - Parameters:
    ///   - url: ссылка
    ///   - completion: блок обработки результата открытия ссылки
    func open(url: URL, completionHandler completion: ((Bool) -> ())? = nil) {
        if #available(iOS 10.0, *) {
            open(url, completionHandler: completion)
        } else {
            let success = openURL(url)
            completion?(success)
        }
    }
    
    /// Попытаться совершить телефонный звонок
    ///
    /// - Parameter phoneNumber: телефонный номер
    func attempt(calling phoneNumber: String) {
        if !attempt(calling: phoneNumber, withPrompt: true) {
            attempt(calling: phoneNumber, withPrompt: false)
        }
    }
    
    /// Попытаться совершить телефонный звонок
    ///
    /// - Parameters:
    ///   - phoneNumber: телефонный номер
    ///   - withPrompt: надо ли спрашивать подтверждение пользователя
    /// - Returns: получилось ли совершить звонок
    @discardableResult fileprivate func attempt(calling phoneNumber: String, withPrompt: Bool) -> Bool {
        
        let urlPrefix = withPrompt ? "telprompt://" : "tel://"
        
        guard let url = URL(string: urlPrefix + phoneNumber) else { return false }
        guard canOpenURL(url) else { return false }
        
        open(url: url)
        
        return true
    }
}

//
//  DateExtensions.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 26.01.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

extension Date {
    
    
    /// Получить компоненты даты
    ///
    /// - Returns: компоненты даты
    func getDateComponents() -> DateComponents {
        
        let components = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute], from: self)
        
        return components
    }
    
    /// Получить определенные компоненты даты
    ///
    /// - Parameter components: запрашиваемые компоненты даты
    /// - Returns: компоненты даты
    func getDateComponents(components: Calendar.Component...) -> DateComponents {
        
        let finalComponents = Calendar.current.dateComponents(Set(components), from: self)
        
        return finalComponents
    }
    
    /// Получить название месяца
    ///
    /// - Returns: название месяца
    func getCurrentMonthName(with dateFormatter: DateFormatter) -> String {
        
        let formatter = dateFormatter
        let components = self.getDateComponents()
        formatter.dateFormat = "MM"
        
        guard let month = components.month else { return "" }
        
        let monthName = formatter.monthSymbols[month]
        
        return monthName
    }
}

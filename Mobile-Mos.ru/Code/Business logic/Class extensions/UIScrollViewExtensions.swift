//
//  UIScrollViewExtensions.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 16.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

extension UIScrollView {
    
    /// Проскроллить UIScrollView до конца
    ///
    /// - Parameter animated: применять ли анимацию
    func scrollToBottom(animated: Bool) {
        
        var difference = contentSize.height - bounds.height
        if #available(iOS 11.0, *) {
            difference += safeAreaInsets.top + safeAreaInsets.bottom
        }
        
        guard difference > 0 else { return }
        
        let newOffset = CGPoint(x: contentOffset.x, y: difference)
        setContentOffset(newOffset, animated: animated)
    }
    
    /// Проскроллить UIScrollView до верха
    ///
    /// - Parameter animated: применять ли анимацию
    func scrollToTop(animated: Bool) {
        
        if #available(iOS 11.0, *) {
            let offset = CGPoint(x: -adjustedContentInset.left, y: -adjustedContentInset.top)
            setContentOffset(offset, animated: animated)
            
        } else {
            let offset = CGPoint(x: -contentInset.left, y: -contentInset.top)
            setContentOffset(offset, animated: animated)
        }
    }
}

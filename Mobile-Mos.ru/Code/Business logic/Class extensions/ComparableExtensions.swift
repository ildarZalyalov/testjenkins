//
//  ComparableExtensions.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 07.08.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

extension Comparable {
    
    /// "Обрезать" текущее значение в соответствии с переданным интервалом возможных значений
    ///
    /// - Parameter limits: интервал возможных значений
    /// - Returns: "обрезанное" значение
    func clamped(to limits: ClosedRange<Self>) -> Self {
        return min(max(self, limits.lowerBound), limits.upperBound)
    }
}

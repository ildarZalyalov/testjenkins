//
//  EKEventStoreExtensions.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 08.12.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import EventKit

extension EKEventStore {
    
    fileprivate(set) static var sharedStore = EKEventStore()
    
    /// Запросить авторизацию к календарю на устройстве
    ///
    /// - Parameters:
    ///   - entityType: тип сущности, к которым хотим иметь доступ
    ///   - completion: обработчик получения разрешения
    /// - Returns: запросили ли разрешение (появлися ли системный алерт)
    static func requestIfNeededAuthorizationAccess(for entityType: EKEntityType, completion: EventKit.EKEventStoreRequestAccessCompletionHandler?) -> Bool {
        
        let status = EKEventStore.authorizationStatus(for: entityType)
        guard status == .notDetermined else { return false }
        
        sharedStore.requestAccess(to: entityType) { granted, error in
            guard completion != nil else { return }
            DispatchQueue.main.async {
                completion?(granted, error)
            }
        }
        
        return true
    }
    
    /// Разрешен ли доступ к календарю на устройстве
    ///
    /// - Parameter entityType: тип сущности, доступ к которым проверяем
    /// - Returns: есть ли доступ
    static func isAuthorizationGranted(for entityType: EKEntityType) -> Bool {
        let status = EKEventStore.authorizationStatus(for: entityType)
        return status == .authorized
    }
}

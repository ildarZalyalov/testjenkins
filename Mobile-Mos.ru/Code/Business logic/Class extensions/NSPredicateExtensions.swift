//
//  NSPredicateExtensions.swift
//  Portable
//
//  Created by Ivan Erasov on 16.06.17.
//  Copyright © 2017. All rights reserved.
//

import Foundation

/// Конфигурация для NSPredicate
struct NSPredicateConfiguration {
    
    /// Условие предиката, содержит форматную строку и аргументы к ней
    typealias PredicateFormatCondition = (format: String, arguments: [Any])
    
    /// Форматная строка предиката
    fileprivate(set) var predicateFormat: String
    
    /// Аргументы форматной строки предиката
    fileprivate(set) var predicateArguments: [Any]
    
    init(with format: String, and arguments: [Any]) {
        predicateFormat = format
        predicateArguments = arguments
    }
    
    init(with keyPath: String, in values: [CustomStringConvertible]) {
        predicateFormat = String()
        predicateArguments = []
        add(keyPath: keyPath, in: values)
    }
    
    /// Добавить условие к предикату
    ///
    /// - Parameter condition: условие
    mutating func and(condition: PredicateFormatCondition) {
        predicateFormat += " AND " + condition.format
        predicateArguments += condition.arguments
    }
    
    /// Добавить одно из возможных условий к предикату
    ///
    /// - Parameter condition: условие
    mutating func or(condition: PredicateFormatCondition) {
        predicateFormat += " OR " + condition.format
        predicateArguments += condition.arguments
    }
    
    /// Добавить набор возможных условий к предикату
    ///
    /// - Parameter conditions: условия
    mutating func and(mutuallyExclusive conditions: PredicateFormatCondition...) {
        
        predicateFormat += " AND ("
        
        for (index, condition) in conditions.enumerated() {
            
            predicateFormat += condition.format
            predicateArguments += condition.arguments
            
            if index < conditions.count - 1 {
                predicateFormat += " OR "
            }
        }
        
        predicateFormat += ")"
    }
    
    /// Добавить условие нахождения значения свойства в списке
    ///
    /// - Parameters:
    ///   - keyPath: свойство
    ///   - values: список возможных значений свойства
    mutating func and(keyPath: String, in values: [CustomStringConvertible]) {
        predicateFormat += " AND "
        add(keyPath: keyPath, in: values)
    }
    
    /// Добавить возможное условие нахождения значения свойства в списке
    ///
    /// - Parameters:
    ///   - keyPath: свойство
    ///   - values: список возможных значений свойства
    mutating func or(keyPath: String, in values: [CustomStringConvertible]) {
        predicateFormat += " OR "
        add(keyPath: keyPath, in: values)
    }
    
    /// Добавить к форматной строке предиката условие нахождения значения свойства в списке
    ///
    /// - Parameters:
    ///   - keyPath: свойство
    ///   - values: список возможных значений свойства
    fileprivate mutating func add(keyPath: String, in values: [CustomStringConvertible]) {
        let valuesArray = values is [String] ? values.map { "‘\($0)’" } : values.map { "\($0)" }
        let valuesList = valuesArray.joined(separator: ",")
        predicateFormat += "(\(keyPath) IN {\(valuesList)})"
    }
}

extension NSPredicate {
    
    convenience init(with configuration: NSPredicateConfiguration) {
        self.init(format: configuration.predicateFormat, argumentArray: configuration.predicateArguments)
    }
}

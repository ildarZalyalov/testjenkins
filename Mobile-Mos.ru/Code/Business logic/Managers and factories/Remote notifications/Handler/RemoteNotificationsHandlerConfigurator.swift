//
//  RemoteNotificationsHandlerConfigurator.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 01.02.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Конфигурация зависимостей для RemoteNotificationsHandler
extension RemoteNotificationsHandler {
    
    static func getNotificationsHandler() -> RemoteNotificationsHandler {
        
        let applicationRouter = ApplicationRouterBuilder.getApplicationRouter()
        
        let handler = RemoteNotificationsHandler()
        handler.applicationRouter = applicationRouter
        handler.conversationsService = UIApplication.shared.serviceBuilder.getConversationsService()
        handler.analyticsManager = AnalyticsManagerBuilder.getAnalyticsManager()
        
        return handler
    }
}

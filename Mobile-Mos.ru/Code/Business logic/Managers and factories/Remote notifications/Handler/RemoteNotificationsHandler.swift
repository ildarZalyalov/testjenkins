//
//  RemoteNotificationsHandler.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 18.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import UserNotifications

/// Обработчик пуш-уведомлений
class RemoteNotificationsHandler: NSObject, UNUserNotificationCenterDelegate {
    
    var applicationRouter: ApplicationRouter!
    var conversationsService: ConversationsService!
    var analyticsManager: AnalyticsManager!
    
    fileprivate let apsPayloadKey = "aps"
    fileprivate let alertPayloadKey = "alert"
    fileprivate let titlePayloadKey = "title"
    fileprivate let bodyPayloadKey = "body"
    
    fileprivate let notificationTypePayloadKey = "push_msg_type"
    fileprivate let chatIdPayloadKey = "chat_id"
    fileprivate let statsPushTypeId = "push_type_id"
    
    fileprivate enum NotificationType: String {
        case newMessage = "chat_msg"
    }
    
    override init() {
        
        super.init()
        
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate = self
        }
    }
    
    /// Payload пуш-уведомления, реакция на которое запустила приложение (если есть)
    var applicationLaunchRemoteNotificationPayload: [NSObject : AnyObject]? = nil
    
    /// Обработать пуш-уведомление, реакция на которое запустила приложение (если оно было)
    func handleApplicationDidReceiveRemoteNotificationOnLaunchIfNeeded() {
        if applicationLaunchRemoteNotificationPayload != nil {
            handleRemoteNotificationWithPayload(applicationLaunchRemoteNotificationPayload!)
            applicationLaunchRemoteNotificationPayload = nil
        }
    }
    
    /// Обработать получение пуш-уведомления
    ///
    /// - Parameter userInfo: payload пуш-уведомления
    func handleApplicationDidReceiveRemoteNotification(_ userInfo: [AnyHashable : Any]) {
        handleRemoteNotificationWithPayload(userInfo)
    }
    
    /// Обработать получение пуш-уведомления в фоне
    ///
    /// - Parameters:
    ///   - userInfo: payload пуш-уведомления
    ///   - handler: обработчик загрузки данных в фоне
    func handleApplicationDidReceiveRemoteNotification(_ userInfo: [AnyHashable : Any],
                                                       fetchCompletionHandler handler: (UIBackgroundFetchResult) -> Void) {
        handleRemoteNotificationWithPayload(userInfo)
        handler(UIBackgroundFetchResult.noData)
    }
    
    /// Обработать payload пуш-уведомления
    ///
    /// - Parameter userInfo: payload пуш-уведомления
    fileprivate func handleRemoteNotificationWithPayload(_ userInfo: [AnyHashable : Any]) {
        
        guard UIApplication.shared.applicationState != .active else { return }
        
        let typeString = userInfo[notificationTypePayloadKey] as? String
        guard let notificationType = NotificationType(rawValueOptional: typeString) else { return }
        
        let conversationId = userInfo[chatIdPayloadKey] as? String
        let pushTypeId = userInfo[statsPushTypeId] as? NSNumber
        let alertName = userInfo[titlePayloadKey] as? String
        let alertText = userInfo[alertPayloadKey] as? String
        
        switch notificationType {
        case .newMessage:
            guard conversationId != nil else { return }
            analyticsManager.sendPushNotificationEvent(by: conversationId!, pushTypeId: pushTypeId?.int64Value, pushName: alertName, pushDescription: alertText)
            showChatModule(for: conversationId!)
        }
        
    }
    
    //MARK: - Обработчики уведомлений
    
    fileprivate func showChatModule(for conversationId: String) {
        
        guard !conversationsService.activeConversationsIds.contains(conversationId) else { return }
        
        conversationsService.obtainCachedConversation(with: conversationId, completion: { [weak self] result in
            switch result {
            case .success(let conversation):
                guard conversation != nil else { return }
                self?.applicationRouter.showChatModule(for: conversation!)
            case .failure(_):
                return
            }
        })
    }
    
    //MARK: - UNUserNotificationCenterDelegate
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        handleRemoteNotificationWithPayload(notification.request.content.userInfo)
        completionHandler([])
    }
}

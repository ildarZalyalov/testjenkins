//
//  RemoteNotificationsConfigurator+AppSpecific.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 18.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Реализация методов RemoteNotificationsConfigurator, специфичных для приложения
extension RemoteNotificationsConfigurator {
    
    fileprivate func createPushTokenService() -> PushTokensService {
        return UIApplication.shared.serviceBuilder.getPushTokensService()
    }
    
    fileprivate func createUserService() -> UserService {
        return UIApplication.shared.serviceBuilder.getUserService()
    }
    
    /// Задержка перед повторной попыткой получения токена пуш-уведомлений
    fileprivate var registrationRetryDelay: TimeInterval {
        return 5
    }
    
    /// Отправить токен пуш-уведомлений на сервер
    ///
    /// - Parameter token: токен
    fileprivate func sendToServer(token: String) {
        
        let tokenService = createPushTokenService()
        let userService = createUserService()
        
        guard token != tokenService.passedToServerToken else { return }
        
        let user = userService.currentUser
        let retryDelay = registrationRetryDelay
        
        tokenService.sendToServer(pushToken: token, for: user) { result in
            switch result {
            case .failure:
                DispatchQueue.main.asyncAfter(deadline: .now() + retryDelay) { [weak self] in
                    self?.sendToServer(token: token)
                }
            default:
                return
            }
        }
    }
    
    //MARK: - RemoteNotificationsConfigurator
    
    func postRemoteNotificationsStateDidChangeIfNeeded() {
        
        let userDefaults = UserDefaults.standard
        let userDefaultsKey = ApplicationUserDefaultsKey.remoteNotificationsAreAuthorized.rawValue
        let lastNotificationsAuthorizedState = userDefaults.object(forKey: userDefaultsKey) as? NSNumber
        
        if notificationsAreAuthorized != lastNotificationsAuthorizedState?.boolValue {
            
            DispatchQueue.main.async {
                let notificationName = Notification.Name.RemoteNotificationsStateDidChange
                NotificationCenter.default.post(name: notificationName, object: nil)
            }
            
            var value: Any? = nil
            if let notificationsAuthorized = notificationsAreAuthorized {
                value = NSNumber(value: notificationsAuthorized)
            }
            
            userDefaults.set(value, forKey: userDefaultsKey)
            userDefaults.synchronize()
        }
    }
    
    func handleDidRegister(with token: Data) {
        let tokenString = token.map { String(format: "%02.2hhx", $0) }.joined()
        sendToServer(token: tokenString)
    }
    
    func handleDidFailToRegister(with error: Error) {
        DispatchQueue.main.asyncAfter(deadline: .now() + registrationRetryDelay) { [weak self] in
            self?.registerForRemoteNotifications()
        }
    }
}

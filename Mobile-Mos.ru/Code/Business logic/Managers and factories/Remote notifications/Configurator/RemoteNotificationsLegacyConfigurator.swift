//
//  RemoteNotificationsLegacyConfigurator.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 18.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Конфигуратор пуш-уведомлений - легаси реализация для iOS 9
class RemoteNotificationsLegacyConfigurator: RemoteNotificationsConfigurator {
    
    /// Попросили ли мы пользователя разрешить уведомления
    fileprivate var didAskToAuthorizeUserNotificationSettings = false
    
    /// Зарегистрировали ли мы настройки уведомлений
    fileprivate var didRegisterUserNotificationSettings = false
    
    /// Получить текущие настройки уведомлений
    fileprivate func refreshUserNotificationSettings() {
        
        let isRegisteredForRemoteNotifications = UIApplication.shared.isRegisteredForRemoteNotifications
        
        guard didAskToAuthorizeUserNotificationSettings || isRegisteredForRemoteNotifications else { return }
        guard let settings = UIApplication.shared.currentUserNotificationSettings else { return }
        
        notificationsAreAuthorized = !settings.types.isEmpty
    }
    
    //MARK: - RemoteNotificationsConfigurator
    
    var notificationsAreAuthorized: Bool? = nil {
        didSet {
            postRemoteNotificationsStateDidChangeIfNeeded()
        }
    }
    
    func registerForRemoteNotifications() {
        
        refreshUserNotificationSettings()
        
        let application = UIApplication.shared
        
        guard !application.isRegisteredForRemoteNotifications else {
            notificationsAreAuthorized = true
            application.registerForRemoteNotifications()
            return
        }
        
        guard !didRegisterUserNotificationSettings else {
            notificationsAreAuthorized = true
            application.registerForRemoteNotifications()
            return
        }
        
        guard didAskToAuthorizeUserNotificationSettings else {
            
            didAskToAuthorizeUserNotificationSettings = true
            
            let settings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
            
            return
        }
        
        didRegisterUserNotificationSettings = true
        application.registerForRemoteNotifications()
    }
}

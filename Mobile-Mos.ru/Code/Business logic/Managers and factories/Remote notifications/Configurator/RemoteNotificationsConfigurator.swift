//
//  RemoteNotificationsConfigurator.swift
//  Portable
//
//  Created by Ivan Erasov on 18.10.2017.
//  Copyright © 2017. All rights reserved.
//

import Foundation

extension NSNotification.Name {
    
    /// Нотификация об изменении разрешения пользователя на получение уведомлений
    static let RemoteNotificationsStateDidChange = NSNotification.Name("RemoteNotificationsStateDidChange")
}

/// Конфигуратор пуш-уведомлений
protocol RemoteNotificationsConfigurator: class {
    
    /// Авторизованы ли пуш-уведомления пользователем
    var notificationsAreAuthorized: Bool? { get }
    
    /// Зарегистрироваться для использования пуш-уведомлений
    func registerForRemoteNotifications()
    
    /// Отправить нотификацию об изменении разрешения пользователя на получение уведомлений, если она изменилась
    func postRemoteNotificationsStateDidChangeIfNeeded()
    
    /// Обработать получение токена для пуш-уведомлений
    ///
    /// - Parameter token: токен
    func handleDidRegister(with token: Data)
    
    /// Обработать ошибку регистрации для использования пуш-уведомлений
    ///
    /// - Parameter error: ошибка
    func handleDidFailToRegister(with error: Error)
}

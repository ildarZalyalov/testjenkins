//
//  RemoteNotificationsDefaultConfigurator.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 18.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import UserNotifications

/// Конфигуратор пуш-уведомлений - актуальная реализация
@available(iOS 10.0, *)
class RemoteNotificationsDefaultConfigurator: RemoteNotificationsConfigurator {
    
    /// Получить текущие настройки уведомлений
    fileprivate func getNotificationSettings() {
        
        let center = UNUserNotificationCenter.current()
        
        center.getNotificationSettings { [weak self] (settings) in
            
            switch settings.authorizationStatus {
            case .authorized:
                self?.notificationsAreAuthorized = true
            case .denied:
                self?.notificationsAreAuthorized = false
            case .notDetermined:
                return
            }
            
            guard self?.notificationsAreAuthorized ?? false else { return }
            DispatchQueue.main.async {
                UIApplication.shared.registerForRemoteNotifications()
            }
        }
    }
    
    //MARK: - RemoteNotificationsConfigurator
    
    var notificationsAreAuthorized: Bool? = nil {
        didSet {
            postRemoteNotificationsStateDidChangeIfNeeded()
        }
    }
    
    func registerForRemoteNotifications() {
        
        guard !UIApplication.shared.isRegisteredForRemoteNotifications else {
            notificationsAreAuthorized = true
            UIApplication.shared.registerForRemoteNotifications()
            return
        }
    
        let center = UNUserNotificationCenter.current()
        let options: UNAuthorizationOptions = [.alert, .sound, .badge]
        
        center.requestAuthorization(options: options) { [weak self] granted, error in
            
            guard granted else {
                self?.notificationsAreAuthorized = false
                return
            }
            
            self?.getNotificationSettings()
        }
    }
}

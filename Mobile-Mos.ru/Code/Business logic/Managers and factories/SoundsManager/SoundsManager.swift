//
//  SoundsManager.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 21.11.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Системные звуки, можно ID можно смотреть тут: https://github.com/TUNER88/iOSSystemSoundsLibrary
///
/// - touchSound1: звук нажатия на телефонной клавиатуре
/// - mailSent: звук отправки почты
/// - smsSent: звук отправки смс
enum SystemSounds: Int {
    case touchSound1 = 1200
    case mailSent    = 1001
    case smsSent     = 1016
}

/// Кастомные звуки (их название)
///
/// - touchSound1: звук плента бомбы из CS (народ решил что это она)
/// - remoteNotification: звук пуш уведомления
/// - messageIn: звук входящего сообщения
/// - messageInOther: звук входящего сообщения в неактивном чате
/// - messageOut: звук отправленного сообщения
/// - messageError: звук ошибки отправки сообщения
enum CustomSounds: String {
    case touchSound1 = "landing0"
    case remoteNotification = "app"
    case messageIn = "message-in"
    case messageInOther = "channel-other"
    case messageError = "channel-error"
}

protocol SoundsManager {
    
    /// Воспроизвести системный звук
    ///
    /// - Parameter systemSound: выбор системного звука
    func playSystemSound(_ systemSound: SystemSounds)
    
    /// Воспроизвести кастомный звук
    ///
    /// - Parameter customSound: выбор кастомного звука
    func playCustomSound(_ customSound: CustomSounds)
}

//
//  SoundsManagerImplementation.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 21.11.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import AudioToolbox

class SoundsManagerImplementation: SoundsManager {
    
    fileprivate let musicType = "wav"
    fileprivate var soundID: UInt32 = 0
    
    
    func playSystemSound(_ systemSound: SystemSounds) {
        AudioServicesPlaySystemSound(SystemSoundID(systemSound.rawValue))
    }
    
    func playCustomSound(_ customSound: CustomSounds) {
        
        guard let audioPath = Bundle.main.path(forResource: customSound.rawValue, ofType: musicType) else { return }
        
        AudioServicesCreateSystemSoundID(URL(fileURLWithPath: audioPath) as CFURL, &soundID)
        
        AudioServicesPlaySystemSound(soundID)
    }
}


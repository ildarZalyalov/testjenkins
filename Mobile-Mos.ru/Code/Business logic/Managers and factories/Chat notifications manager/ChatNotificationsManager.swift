//
//  ChatNotificationsManager.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 08.09.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Менеджер управления показа нотификаций из чатов в приложении
class ChatNotificationsManager {
    
    var presenter: ChatNotificationsPresenter!
    
    var applicationRouter: ApplicationRouter!
    var hapticFeedManager: HapticFeedbackManager?
    var soundsManager: SoundsManager!
    var analyticsManager: AnalyticsManager!
    var conversationsService: ConversationsService!
    var userSettingsService: UserInfoAndSettingsService!
    
    var messagesService: ChatMessageService! {
        
        didSet {
            
            guard messagesService != nil else { return }
            
            // настраиваем нотификации для новых сообщений
            
            messagesService.newMessageHandler = { [weak self] message in
                
                guard let strongSelf = self else { return }
                guard message.isIncoming else { return }
                guard !strongSelf.conversationsService.activeConversationsIds.contains(message.chatId) else { return }
                
                strongSelf.userSettingsService.obtainNotificationsAuthorizationStatus(completion: { [weak self] status in
                    
                    guard let strongSelf = self else { return }
                    
                    switch status {
                        
                    case .authorized:
                        
                        let user = strongSelf.userSettingsService.currentUser
                        
                        if user.isLoggedIn {
                            let notificationsEnabled = user.notificationsSettings.first(where: { $0.chatId == message.chatId })?.areNotificationsEnabled ?? true
                            guard notificationsEnabled else { return }
                        }
                        
                        strongSelf.displayNotification(for: message)
                        
                    case .denied, .notDetermined:
                        return
                    }
                })
            }
        }
    }
    
    var messageInteractionService: ChatMessageInteractionService! {
        
        didSet {
            
            guard messageInteractionService != nil else { return }
            
            // настраиваем нотификации для ответов на коллбэк запрос
            
            messageInteractionService.callbackResponseHandler = { [weak self] response in
                
                guard let strongSelf = self else { return }
                
                if strongSelf.conversationsService.activeConversationsIds.contains(response.chatId) {
                    strongSelf.soundsManager.playCustomSound(.messageIn)
                    strongSelf.hapticFeedManager?.triggerImpactFeedback(with: .light)
                }
                else {
                    strongSelf.soundsManager.playCustomSound(.messageInOther)
                    strongSelf.hapticFeedManager?.triggerNotificationFeedback(with: .warning)
                }
                
                strongSelf.conversationsService.obtainCachedConversation(with: response.chatId, completion: { [weak self] result in
                    switch result {
                    case .success(let conversation):
                        guard let messageConversation = conversation else { return }
                        self?.presenter.showNotification(with: response.text, in: messageConversation, tapHandler: { [weak self] in
                            self?.applicationRouter.showChatModule(for: messageConversation)
                        })
                    case .failure(_):
                        return
                    }
                })
            }
            
            // настраиваем нотификации для уведомлений в чате
            
            messageInteractionService.chatNotificationHandler = { [weak self] response in
                
                guard let strongSelf = self else { return }
                
                if response.type == .error {
                    strongSelf.soundsManager.playCustomSound(.messageError)
                    strongSelf.hapticFeedManager?.triggerNotificationFeedback(with: .error)
                }
                else if strongSelf.conversationsService.activeConversationsIds.contains(response.chatId) {
                    strongSelf.soundsManager.playCustomSound(.messageIn)
                    strongSelf.hapticFeedManager?.triggerImpactFeedback(with: .light)
                }
                else {
                    strongSelf.soundsManager.playCustomSound(.messageInOther)
                    strongSelf.hapticFeedManager?.triggerNotificationFeedback(with: .warning)
                }
                
                strongSelf.conversationsService.obtainCachedConversation(with: response.chatId, completion: { [weak self] result in
                    switch result {
                    case .success(let conversation):
                        guard let notificationConversation = conversation else { return }
                        self?.presenter.showNotification(with: response.text, in: notificationConversation, tapHandler: { [weak self] in
                            self?.applicationRouter.showChatModule(for: notificationConversation)
                        })
                    case .failure(_):
                        return
                    }
                })
            }
        }
    }
    
    //MARK: - Приватные методы
    
    /// Показать уведомление для сообщения
    ///
    /// - Parameter message: сообщения
    fileprivate func displayNotification(for message: ChatMessage) {
        
        soundsManager.playCustomSound(.messageInOther)
        hapticFeedManager?.triggerNotificationFeedback(with: .warning)
        
        conversationsService.obtainCachedConversation(with: message.chatId, completion: { [weak self] result in
            switch result {
            case .success(let conversation):
                guard let messageConversation = conversation else { return }
                self?.presenter.showNotification(for: message, in: messageConversation, tapHandler: { [weak self] in
                    
                    if let statsData = message.statsData {
                        self?.analyticsManager.sendSomeEvent(with: statsData.eventName, eventJson: statsData.eventJson, errorText: nil)
                    }
                    self?.applicationRouter.showChatModule(for: messageConversation)
                })
            case .failure(_):
                return
            }
        })
    }
}

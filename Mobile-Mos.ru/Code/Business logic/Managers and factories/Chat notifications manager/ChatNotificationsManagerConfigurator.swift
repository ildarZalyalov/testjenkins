//
//  ChatNotificationsManagerConfigurator.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 11.09.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Конфигурация зависимостей для ChatNotificationsManager
extension ChatNotificationsManager {
    
    static func getNotificationsManager() -> ChatNotificationsManager {
        
        let manager = ChatNotificationsManager()
        
        let applicationRouter = ApplicationRouterBuilder.getApplicationRouter()
        let soundsManager = SoundsManagerImplementation()
        let presenter = ChatNotificationsPresenter()
        let analyticsManager = AnalyticsManagerBuilder.getAnalyticsManager()
        
        manager.presenter = presenter
        manager.applicationRouter = applicationRouter
        manager.soundsManager = soundsManager
        
        if #available(iOS 10.0, *) {
            let hapticFeedManager = HapticFeedbackManagerImplementation()
            manager.hapticFeedManager = hapticFeedManager
        }
        
        manager.messagesService = UIApplication.shared.serviceBuilder.getChatMessageService()
        manager.messageInteractionService = UIApplication.shared.serviceBuilder.getChatMessageInteractionService()
        manager.conversationsService = UIApplication.shared.serviceBuilder.getConversationsService()
        manager.userSettingsService = UIApplication.shared.serviceBuilder.getUserInfoAndSettingsService()
        manager.analyticsManager = analyticsManager
        
        return manager
    }
}

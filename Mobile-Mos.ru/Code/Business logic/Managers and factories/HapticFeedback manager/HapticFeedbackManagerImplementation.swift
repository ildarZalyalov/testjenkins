//
//  HapticFeedbackManagerImplementation.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 21.11.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

@available(iOS 10.0, *)
class HapticFeedbackManagerImplementation: HapticFeedbackManager {
    
    fileprivate var selectionFeedbackGenerator: UISelectionFeedbackGenerator?
    fileprivate var impactFeedbackGenerator: UIImpactFeedbackGenerator?
    fileprivate var notificationFeedbackGenerator: UINotificationFeedbackGenerator?
    
    func triggerImpactFeedback(with style: UIImpactFeedbackStyle) {
        
        impactFeedbackGenerator = UIImpactFeedbackGenerator(style: style)
        impactFeedbackGenerator?.impactOccurred()
        
        DispatchQueue.main.async { [weak self] in
            self?.impactFeedbackGenerator = nil
        }
    }
    
    func triggerSelectionFeedback() {
        
        selectionFeedbackGenerator = UISelectionFeedbackGenerator()
        selectionFeedbackGenerator?.selectionChanged()
        
        DispatchQueue.main.async { [weak self] in
            self?.selectionFeedbackGenerator = nil
        }
    }
    
    func triggerNotificationFeedback(with style: UINotificationFeedbackType) {
        
        notificationFeedbackGenerator = UINotificationFeedbackGenerator()
        notificationFeedbackGenerator?.notificationOccurred(style)
        
        DispatchQueue.main.async { [weak self] in
            self?.notificationFeedbackGenerator = nil
        }
    }
}

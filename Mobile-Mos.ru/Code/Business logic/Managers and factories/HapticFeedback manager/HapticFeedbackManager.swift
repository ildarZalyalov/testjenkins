//
//  HapticFeedbackManager.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 21.11.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Менеджер, который умеет воспроизводить haptic feedback
/// Документация по видам haptic feedback: https://developer.apple.com/documentation/uikit/uifeedbackgenerator
protocol HapticFeedbackManager {
    
    /// Воспроизвести haptic для экшена
    func triggerSelectionFeedback()
    
    /// Воспроизвести haptic на нажатие с определенным стилем
    ///
    /// - Parameter style: сила отзыва на нажатие (.light, .medium, .heavy)
    func triggerImpactFeedback(with style: UIImpactFeedbackStyle)

    /// Воспроизвести отзыв на нажатие со стилем нотификации
    ///
    /// - Parameter style: стили отзыва на разные события (success, warning, error)
    func triggerNotificationFeedback(with style: UINotificationFeedbackType)
}

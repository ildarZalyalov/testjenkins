//
//  YandexClusterManagerImplementation.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 08.11.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class YandexClusterManagerImplementation: NSObject, YandexClusterManager, YMKMapViewDataSource, YMKMapCameraListener, YMKMapObjectTapListener, YMKMapObjectCollectionListener {
    
    fileprivate let clusterdImage = #imageLiteral(resourceName: "pin-clustered")
    fileprivate let iconForFavoritePlacemarkOnMap = #imageLiteral(resourceName: "pin-stared")
    fileprivate let iconForPlacemarkOnMap = #imageLiteral(resourceName: "pinIcon")
    fileprivate let iconScaleForPlacemark = 1.0
    fileprivate let thousandsCountSuffix = "k"
    fileprivate let clusterFontSize: CGFloat = 15
    fileprivate let offsetToUncluster = 0.0003
    fileprivate var maxZoom: Float = 17.7
    fileprivate let digitsAfterZero = 2
    
    fileprivate weak var currentMapView: YMKMapView!
    fileprivate var  mapPinIconConfigurator: MapPinIconConfiguratorProtocol!
    fileprivate weak var selectedCluster: CKCluster?
    fileprivate var  lastSelectedClusterPosition: CLLocationCoordinate2D?
    fileprivate var  clusterPoints: [YMKPlacemarkMapObject] = []
    
    var dataByPoint: [Double : PointData] = [:]
    
    func configure(with mapView: YMKMapView) {
        
        currentMapView = mapView
        
        mapPinIconConfigurator = MapPinIconConfigurator()
        
        let algorithm = CKNonHierarchicalDistanceBasedAlgorithm()
        algorithm.cellSize = 200
        
        currentMapView.clusterManager.algorithm = algorithm
        currentMapView.clusterManager.marginFactor = 1
        currentMapView.dataSource = self
        
        setupMapDelegates()
    }
    
    func clearUnclusteredObjects() {
        clearMap()
    }
    
    //MARK: - YMKMapViewDataSource
    
    func mapView(_ mapView: YMKMapView, markerFor cluster: CKCluster) -> YMKPlacemarkMapObject {
        
        let position = YMKPoint(latitude: cluster.coordinate.latitude, longitude: cluster.coordinate.longitude)
        
        let uniqHash = position.latitude + position.longitude
        let data = dataByPoint[uniqHash]
        
        let marker = currentMapView.addMapObject(with: position, andObjectData: data)
    
        if cluster.count > 1 {
            
            let clusterText = getNumbersOfItems(with: cluster)
            let image = textToImage(drawText: clusterText, inImage: clusterdImage)
            configureImage(of: marker, with: image)
            
            if let position = lastSelectedClusterPosition,
                position.roundedCheckEqual(
                withDigitsAfterZero: digitsAfterZero,
                rhs: cluster.coordinate) {
                
                clearMap()
            }
        }
        else {
            configureImage(of: marker, with: iconForPlacemarkOnMap)
            
            guard let mapItem = data as? MapItem else { return marker }
   
            setPlacemarkFavoriteIfNeeded(mapItem, placemark: marker)
        }
        
        return marker
    }
    
    //MARK: - YMKMapObjectCollectionListener
    
    func onMapObjectAdded(with mapObject: YMKMapObject?) {
        mapObject?.addTapListener(with: self)
    }
    
    func onMapObjectRemoved(with mapObject: YMKMapObject?) {
        mapObject?.removeTapListener(with: self)
    }
    
    func onMapObjectUpdated(with mapObject: YMKMapObject?) {
        
    }
    
    func onCameraPositionChanged(with map: YMKMap?, cameraPosition: YMKCameraPosition, cameraUpdateSource: YMKCameraUpdateSource, finished: Bool) {
        
        if finished && cameraPosition.zoom < maxZoom {
            currentMapView.clusterManager.updateClustersIfNeeded()
        }
    }
    
    func onMapObjectTap(with mapObject: YMKMapObject?, point: YMKPoint) -> Bool {
        
        guard let mapPlacemarkObject = mapObject as? YMKPlacemarkMapObject
            else {
                return false
        }
        
        if let cluster = mapPlacemarkObject.cluster, cluster.count > 1 {
            
            if let points = cluster.annotations as? [YMKPoint] {
                
                if selectedCluster == cluster {
                    
                    forceUncluster(cluster)
                    lastSelectedClusterPosition = cluster.coordinate
                    
                    return false
                }
                selectedCluster = cluster
                currentMapView.updateCameraPosition(with: points, cameraCallBack: nil)
            }
            
            return true
        }
        return false
    }
    
    //MARK: - Helpers
    
    fileprivate func setupMapDelegates() {
        
        currentMapView.map.addCameraListener(with: self)
        currentMapView.map.mapObjects?.addListener(with: self)
    }
    
    fileprivate func configureImage(of placemarkMapObject: YMKPlacemarkMapObject, with image: UIImage) {
    
        let iconStyle = YMKIconStyle(scale: iconScaleForPlacemark)
        mapPinIconConfigurator.configure(with: placemarkMapObject, iconStyle: iconStyle, icon: image)
    }
    
    fileprivate func setPlacemarkFavoriteIfNeeded(_ mapItem: MapItem, placemark: YMKPlacemarkMapObject) {
        
        if mapItem.isFavorited && placemark.isValid {
            
            configureImage(of: placemark, with: iconForFavoritePlacemarkOnMap)
        }
    }
    
    fileprivate func clearMap() {
        
        if !clusterPoints.isEmpty {
            clusterPoints.forEach{ currentMapView.remove(mapObject: $0) }
            clusterPoints.removeAll()
            lastSelectedClusterPosition = nil 
        }
    }
    
    fileprivate func textToImage(drawText text: String, inImage image: UIImage) -> UIImage {
        
        let textColor = UIColor.white
        let textFont = UIFont.boldSystemFont(ofSize: clusterFontSize)
        let textSize = text.sizeOfString(with: textFont)
        
        let scale = UIScreen.main.scale
        UIGraphicsBeginImageContextWithOptions(image.size, false, scale)
        
        let textFontAttributes = [
            NSAttributedStringKey.font: textFont,
            NSAttributedStringKey.foregroundColor: textColor,
            ]
        image.draw(in: CGRect(origin: CGPoint.zero, size: image.size))
        
        let xPos = (image.size.width / 2) - textSize.width / 2
        let yPos = (image.size.height / 2) - textSize.height / 2
        
        let point = CGPoint(x: xPos, y: yPos)
        let rect = CGRect(origin: point, size: image.size)
        text.draw(in: rect, withAttributes: textFontAttributes)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    fileprivate func getNumbersOfItems(with cluster: CKCluster) -> String {
        
        let objectsCount = cluster.count
        
        return objectsCount <= 999 ? String(objectsCount) : String(objectsCount / 1000) + thousandsCountSuffix
    }
    
    fileprivate func forceUncluster(_ cluster: CKCluster) {
        
        DispatchQueue.global(qos: .userInteractive).async { [weak self] in
            
            guard let strongSelf = self else { return }
            
            let newPoints = strongSelf.currentMapView.clusterManager.forceExpand(cluster, withOffset: strongSelf.offsetToUncluster)
            var previousData = [PointData]()
            cluster.annotations.forEach{ previousData.append(strongSelf.dataByPoint[$0.coordinate.latitude + $0.coordinate.longitude]!)}
            
            let mapPoints = newPoints.map({ YMKPoint(latitude: $0.coordinate.longitude, longitude: $0.coordinate.latitude)})
            
            DispatchQueue.main.async {
                
                mapPoints.enumerated().forEach {
                    
                    let mapObject = strongSelf.currentMapView.addMapObject(with: $0.element, andObjectData: previousData[$0.offset])
                   
                    strongSelf.configureImage(of: mapObject, with: strongSelf.iconForPlacemarkOnMap)
                    strongSelf.setPlacemarkFavoriteIfNeeded(mapObject.userData as! MapItem, placemark: mapObject)
                    strongSelf.clusterPoints.append(mapObject)
                }
                
                strongSelf.currentMapView.remove([cluster])
                strongSelf.selectedCluster = nil
            }
        }
    }
}

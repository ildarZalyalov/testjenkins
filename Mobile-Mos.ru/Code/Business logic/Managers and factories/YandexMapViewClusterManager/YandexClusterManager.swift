//
//  YandexClusterManager.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 08.11.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Данные точки
typealias PointData = Any

/// Протокол менджера который умеет кластаризовывать
protocol YandexClusterManager: class {
    
    /// данные конкретной точки
    var dataByPoint: [Double : PointData] { get set }
    
    /// Сконфигурировать менеджер кластаризации с текущей вьюхой карты
    ///
    /// - Parameter mapView: текущая карта
    func configure(with mapView: YMKMapView)
    
    /// Удалить модели, которые были насильно раскластаризованы
    func clearUnclusteredObjects()
}

//
//  OverlapsManagerConfigurator.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 20.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

// MARK: - Конфигуратор менеджера перекрытий
extension OverlapsManagerImplementation {
    
    static func getOverlapsManager() -> OverlapsManager {
        
        let manager = OverlapsManagerImplementation()
        let pinConfigurator = MapPinIconConfigurator()
        
        manager.mapPinIconConfigurator = pinConfigurator
        
        return manager
    }
}

//
//  OverlapsManagerDelegate.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 23.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Делегат менеджера перекрытий
protocol OverlapsManagerDelegate: class {
    
    /// Выбран пин относящийся к перекрытию
    ///
    /// - Parameter selectedPinData: информация выбранной точки
    func didSelectOverlapPin(with selectedPinData: Any)
    
    /// Камера Перекрытий изменились
    func didOverlapCameraChanged()
    
    /// Перекрытия спрятаны
    func didHideOverlaps()
}

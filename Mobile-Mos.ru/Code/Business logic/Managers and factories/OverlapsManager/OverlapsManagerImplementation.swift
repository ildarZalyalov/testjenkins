
//
//  OverlapsManagerImplementation.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 19.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

class OverlapsManagerImplementation: NSObject, OverlapsManager, YMKMapObjectTapListener {
    
    /// Перечисление со стейтами конфигурации картинок точки карты
    ///
    /// - normal: нормальная
    /// - selected: "выбранная"
    enum ImagePinConfigureState {
        case normal
        case selected
    }
    
    /// Модель информации точки
    struct PinData {
        var point: YMKPoint
        var userData: Any?
    }
    
    fileprivate let selectedOverlapIcon = #imageLiteral(resourceName: "overlap_selected")
    fileprivate let overlapIcon = #imageLiteral(resourceName: "pin_selected_black")
    
    fileprivate let iconScale = 1.0
    fileprivate let maxZoom: Float = 13.0
    
    /// Цвет основной линии
    fileprivate let lineStrokeColor = UIColor.black
    
    /// Цвет линии подложки
    fileprivate let sublineStrokeColor = UIColor.white
    
    /// ширина линии для минимально допустимого приближения
    fileprivate let lineStrokeMinWidth: Float = 3.0
    
    /// ширина линии подложки для минимально допустимого приближения
    fileprivate let sublineStrokeMinWidth: Float = 7.0
    
    /// ширина линии для приближения > минимально допустимого
    fileprivate let lineStrokeMaxWidth: Float = 2.0
    
    /// ширина линии подложки для приближения > минимально допустимого
    fileprivate let sublineStrokeMaxWidth: Float = 4.0
    
    /// Какая то константа чтобы зумить чуть ближе константы макса зума
    fileprivate let zoomOnPointMax: Float = 3.0
    
    fileprivate lazy var overpalsOperationQueue: OperationQueue = {
        let operationQueue = OperationQueue()
        operationQueue.name = "Overlaps operation queue"
        operationQueue.qualityOfService = .userInteractive
        
        return operationQueue
    }()
    
    fileprivate static var operationQueueMain = OperationQueue.main
    
    fileprivate var overlaps = [OverlapGeo]()
    
    fileprivate var overlapPinsDataArray = [PinData]()
    
    fileprivate var linesMapObjects: [YMKPolylineMapObject?] = [] {
        willSet {
            if !hasOverlapsVisible {
                newValue.forEach{ if let object = $0, object.isValid { mapView.remove(mapObject: object) } }
            }
        }
    }
    
    fileprivate var markMapObjects: [YMKMapObject?] = [] {
        willSet {
            if !hasOverlapsVisible {
                newValue.forEach{ if let object = $0, object.isValid { mapView.remove(mapObject: object) } }
            }
        }
    }
    
    fileprivate var selectedMapObject: YMKPlacemarkMapObject? {
        willSet {
            if hasOverlapsVisible, let object = selectedMapObject {
                configure(mark: object)
            }
        }
    }
    
    fileprivate var isCameraСentralizedOnPin = false
    fileprivate var hasOverlapsVisible = false
    fileprivate var isOverlapPinShowing = false
    fileprivate var isMapObjectsHidden = false
    
    typealias Class = OverlapsManagerImplementation
    
    weak var mapView: YMKMapView!
    weak var overlapsManagerDelegate: OverlapsManagerDelegate?
    var mapPinIconConfigurator: MapPinIconConfiguratorProtocol!
    
    var viewportBottomInset: CGFloat = 262
    
    var hasOverlaps: Bool {
        return !overlaps.isEmpty
    }
    
    var hasOverlapsObjectsVisible: Bool {
        return hasOverlapsVisible
    }
    
    //MARK: - Open methods
    
    func configure(with map: YMKMapView, delegate: OverlapsManagerDelegate?) {
        mapView = map
        overlapsManagerDelegate = delegate
    }
    
    func showOverlapsOnMap(with currentCameraZoom: Float, overlaps:[OverlapGeo]) {
        
        if self.overlaps.isEmpty {
            self.overlaps = overlaps
        }
        
        hasOverlapsVisible = true
        
        overpalsOperationQueue.addOperation { [weak self] in
            self?.addOverlapsToMap(withCameraZoom: currentCameraZoom)
        }
    }
    
    func hideOverlaps() {
        
        guard hasOverlapsVisible else { return }
        
        hasOverlapsVisible = false
        isOverlapPinShowing = false
        
        overlapsManagerDelegate?.didHideOverlaps()
        
        mapView.clearMapObjects()
        
        linesMapObjects.removeAll()
        markMapObjects.removeAll()
        overlapPinsDataArray.removeAll()
    }
    
    func changeOverlaps(withZoom currentCameraZoom: Float) {
        
        guard hasOverlapsVisible else { return }
        
        if !isCameraСentralizedOnPin {
            
            overlapsManagerDelegate?.didOverlapCameraChanged()
            selectedMapObject = nil
        }
        
        if currentCameraZoom > maxZoom {

            changeLinesStrokeWidth(with: linesMapObjects,
                                   lineStrokeWidth: lineStrokeMinWidth,
                                   sublineStrokeWidht: sublineStrokeMinWidth)
            
            if !isCameraСentralizedOnPin && !isOverlapPinShowing {
                isOverlapPinShowing = true
                overlapPinsDataArray.forEach{ addOverlapPin(with: $0) }
            }
            
            if isMapObjectsHidden && !isCameraСentralizedOnPin {
                
                markMapObjects.forEach{ $0?.isVisible = true }
                isMapObjectsHidden = false
            }
           
        }
        else {

            changeLinesStrokeWidth(with: linesMapObjects,
                                   lineStrokeWidth: lineStrokeMaxWidth,
                                   sublineStrokeWidht: sublineStrokeMaxWidth)

            guard !markMapObjects.isEmpty else { return }

            markMapObjects.forEach{ if let object = $0 { mapView.remove(mapObject: object)} }
            
            markMapObjects.removeAll()
            
            isOverlapPinShowing = false
        }
        
    }
    
    func hideSelectedOverlap() {
        
        guard hasOverlapsVisible else { return }
        
        selectedMapObject = nil
        markMapObjects.forEach{ $0?.isVisible = true }
        isMapObjectsHidden = false
    }
    
    //MARK: - YMKMapObjectTapListener
    
    func onMapObjectTap(with mapObject: YMKMapObject?, point: YMKPoint) -> Bool {
        
        if let polyline = mapObject as? YMKPolylineMapObject, let firstPoint = polyline.geometry.points.first {
            
            mapView.moveCameraPosition(to: firstPoint.latitude, longitute: firstPoint.longitude, zoom: maxZoom + zoomOnPointMax, callBack:{ [weak self] (isFinished) in
                
                if isFinished {
                    self?.isCameraСentralizedOnPin = false
                }
            })
        }
        
        guard let mapObject = mapObject, let mapPlacemarkModel = mapObject as? YMKPlacemarkMapObject, let overlapId = mapObject.userData as? String else { return false }
        
        overlapsManagerDelegate?.didSelectOverlapPin(with: overlapId)
        
        isCameraСentralizedOnPin = true
        
        if !isMapObjectsHidden {
            isMapObjectsHidden = true
            markMapObjects.forEach{ $0?.isVisible = false }
        }
        
        mapObject.isVisible = true
        configure(mark: mapPlacemarkModel, with: .selected)
        
        selectedMapObject = mapPlacemarkModel
        
        mapView.moveCameraPosition(to: mapPlacemarkModel.geometry.latitude, longitute: mapPlacemarkModel.geometry.longitude, zoom: mapView.cameraPosition.zoom, isItemInfoCardOpen: true, viewportBottomInset: viewportBottomInset, callBack:{ [weak self] (isFinished) in
            
            if isFinished {
                self?.isCameraСentralizedOnPin = false
            }
        })
        
        return true
    }
    
    //MARK: - Helper methods
    
    fileprivate func addOverlapsToMap(withCameraZoom zoom: Float) {
        
        for overlap in overlaps {
            
            var mapPoints: [YMKPoint] = []
            var handler: () -> ()
            
            overlap.geometry.forEach{
                
                let point = YMKPoint(latitude: $0.latitude, longitude: $0.longitude)
                mapPoints.append(point)
            }
            
            defer {
                Class.operationQueueMain.addOperation {
                    handler()
                }
            }
            
            switch overlap.type {
            case .point:
                
                handler = { [weak self] in self?.setupPointOverlaps(with: mapPoints, overlapId: overlap.id) }
            case .lineString:
                
                handler = { [weak self] in
                    guard let strongSelf = self else { return }
                    
                    strongSelf.setupLineStringOverlaps(with: mapPoints,
                                                  color: strongSelf.sublineStrokeColor,
                                                  strokeWidth: strongSelf.sublineStrokeMinWidth)
                    
                    strongSelf.setupLineStringOverlaps(with: mapPoints,
                                                  color: strongSelf.lineStrokeColor,
                                                  strokeWidth: strongSelf.lineStrokeMaxWidth)
                    
                    
                    if  let beginPoint = mapPoints.first, let endPoint = mapPoints.last {
                        
                        strongSelf.overlapPinsDataArray.append(PinData(point: beginPoint, userData: overlap.id))
                        strongSelf.overlapPinsDataArray.append(PinData(point: endPoint, userData: overlap.id))
                    }
                }
                
            default:
                handler = {}
                return
            }
        }
        
        Class.operationQueueMain.addOperation { [weak self] in
            self?.changeOverlaps(withZoom: zoom)
        }
    }
    
    fileprivate func setupPointOverlaps(with points: [YMKPoint], overlapId: String) {
        
        points.forEach{
            
            let placemark = mapView.mapObjects.addPlacemark(with: $0)
            placemark?.userData = overlapId
            placemark?.addTapListener(with: self)
            
            configure(mark: placemark)
            markMapObjects.append(placemark)
        }
    }
    
    fileprivate func addOverlapPin(with pinData: PinData) {
        
        let placemark = mapView.addMapObject(with: pinData.point, andObjectData: pinData.userData)
        configure(mark: placemark)
        placemark.addTapListener(with: self)
        
        markMapObjects.append(placemark)
    }
    
    fileprivate func setupLineStringOverlaps(with points: [YMKPoint], color: UIColor, strokeWidth: Float) {
        
        let polyline = mapView.mapObjects.addPolyline(with: YMKPolyline(points: points))
        polyline?.strokeWidth = strokeWidth
        polyline?.strokeColor = color
        
        polyline?.addTapListener(with: self)
        
        linesMapObjects.append(polyline)
    }
    
    fileprivate func configure(mark placemark: YMKPlacemarkMapObject?, with state: ImagePinConfigureState = .normal) {
        
        guard let placemark = placemark, placemark.isValid else { return }
        
        let iconStyle = YMKIconStyle(scale: iconScale)
        
        switch state {
        case .normal    :
            mapPinIconConfigurator.configure(with: placemark,
                                            iconStyle: iconStyle,
                                            icon: overlapIcon)
        case .selected  :
            mapPinIconConfigurator.configure(with: placemark,
                                             iconStyle: iconStyle,
                                             icon: selectedOverlapIcon)
        }
    }
    
    fileprivate func changeLinesStrokeWidth(with lines: [YMKPolylineMapObject?], lineStrokeWidth: Float, sublineStrokeWidht: Float) {
        
        for line in lines {
            
            if line?.strokeColor == lineStrokeColor {
                line?.strokeWidth = lineStrokeWidth
            }
            else if line?.strokeColor == sublineStrokeColor {
                line?.strokeWidth = sublineStrokeWidht
            }
        }
    }
    
}

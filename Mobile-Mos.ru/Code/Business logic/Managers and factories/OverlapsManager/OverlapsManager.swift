//
//  OverlapsManager.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 19.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Менеджер по работе с перекрытиями
protocol OverlapsManager {
    
    /// Имеются ли перекрытия в менеджере
    var hasOverlaps: Bool { get }
    
    /// Видны ли объекты перекрытий в данный момент
    var hasOverlapsObjectsVisible: Bool { get }
    
    /// Сконфигурировать менеджер
    ///
    /// - Parameters:
    ///   - map: карта для отображения перекрытий
    ///   - overlaps: массив перекрытий
    func configure(with map: YMKMapView, delegate: OverlapsManagerDelegate?)
    
    /// Отобразить перекрытия на карте
    func showOverlapsOnMap(with currentCameraZoom: Float, overlaps:[OverlapGeo])
    
    /// Спрятать перекрытия на карте
    func hideOverlaps()
    
    /// Спрятать выбранное перекрытие
    func hideSelectedOverlap()
    
    /// Изменить перекрытия в зависимости от позиции камеры
    ///
    /// - Parameter cameraPosition: позиция камеры
    func changeOverlaps(withZoom currentCameraZoom: Float)
}

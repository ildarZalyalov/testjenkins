//
//  CurrentUserManagerImplementation.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 23.08.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class CurrentUserManagerImplementation: CurrentUserManager {
    
    static var dataBaseManager: DataBaseManager!
    
    fileprivate typealias Class = CurrentUserManagerImplementation
    
    /// Текущий пользователь, закешированный в памяти
    fileprivate static var currentUser: User = obtainCurrentUser()
    
    /// Получить текущего пользователя из БД
    ///
    /// - Returns: текущий пользователь
    fileprivate static func obtainCurrentUser() -> User {
        
        let request = DataBaseFetchRequest(with: User.self)
        let users = try? dataBaseManager.fetch(with: request)
        
        if let savedUser = users?.first {
            return savedUser
        }
        
        let unauthorizedUser = User()
        saveAsync(user: unauthorizedUser)
        
        return unauthorizedUser
    }
    
    /// Асинхронно сохранить данные о пользователе
    ///
    /// - Parameters:
    ///   - user: пользователь
    ///   - deletingCurrent: удалить ли текущего пользователя перед сохранением данных
    fileprivate static func saveAsync(user: User, deletingCurrent: Bool = false) {
        
        if deletingCurrent {
            
            dataBaseManager.deleteAllModelsAsync(of: User.self, resultBlock: { result in
                
                switch result {
                case .success:
                    dataBaseManager.saveAsync(models: [user], resultBlock: nil)
                default:
                    return
                }
            })
        }
        else {
            dataBaseManager.saveAsync(models: [user], resultBlock: nil)
        }
    }
    
    //MARK: - CurrentUserManager
    
    var currentUser: User {
        return Class.currentUser
    }
    
    func switchCurrentUser(to user: User) {
        Class.currentUser = user
        Class.saveAsync(user: user, deletingCurrent: true)
    }
    
    func updateCurrentUserInfo(with userInfo: UserInfo) {
        Class.currentUser.userInfo = userInfo
        Class.saveAsync(user: Class.currentUser)
    }
    
    func updateCurrentUserNotificationsSettings(with settings: [UserNotificationsSetting]) {
        Class.currentUser.notificationsSettings = settings
        Class.saveAsync(user: Class.currentUser)
    }
}

//
//  CurrentUserManager.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 23.08.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Менеджер для работы с текущим пользователем
protocol CurrentUserManager {
    
    /// Текущий пользователь
    var currentUser: User { get }
    
    /// Сменить текущего пользователя на другого
    ///
    /// - Parameter user: другой пользователь
    func switchCurrentUser(to user: User)
    
    /// Обновить инфорацию о пользователе
    ///
    /// - Parameter userInfo: информация о пользователе
    func updateCurrentUserInfo(with userInfo: UserInfo)
    
    /// Обновить настройки уведомлений для текущего пользователя
    ///
    /// - Parameter settings: настройки уведомлений
    func updateCurrentUserNotificationsSettings(with settings: [UserNotificationsSetting])
}

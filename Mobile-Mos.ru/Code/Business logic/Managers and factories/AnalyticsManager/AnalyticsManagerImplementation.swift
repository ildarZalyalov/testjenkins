//
//  AnalyticsManagerImplementation.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 21.05.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation
import YandexMobileMetrica

/// Типы возможных Событий
enum CategoryType: String {
    case none = "Пусто"
    case authorization = "Авторизация - Вход"
    case forgotPassword = "Авторизация - Забыли пароль"
    case newPassword = "Авторизация - Новый пароль"
    case registration = "Авторизация - Регистрация"
    case addSmsCode = "Авторизация - Введите код"
    case feedback = "Обратная связь"
    case session = "Сессия"
    case agreements = "Пользовательское соглашение"
    case appsAndServices = "Наши приложения и сервисы"
    case aboutApp = "ЛК_О приложении"
    case childrenService = "ЛК_Дневник, проход и питание"
    case transportService = "ЛК_Транспортные средства и ВУ"
    case myHomeService = "ЛК_Коды плательщика ЖКУ"
    case medecineInsuranceService = "ЛК_Полисы ОМС"
    case notificationService = "ЛК_Уведомления"
    case personalInfo = "ЛК_Личные данные"
    case userAccount = "ЛК"
    case globalSearch = "Поиск"
    case mainScreen = "Главный экран"
    case safariBrawser = "Браузер в приложении"
    case healthConversation = "Здоровье"
    case transportConversation = "Транспорт"
    case childConversation = "Дети в школе"
    case myHomeConversation = "Мой дом"
    case consultationConversation = "Консультации"
    case cityNewsConversation = "Новости города"
    case mapSearchConversation = "Поиск по карте"
    case newsPush = "Push-событие_Новости города"
    case consultationPush = "Push-событие_Консультации"
    case childInSchoolPush = "Push-событие_Дети в школе"
    case transportPush = "Push-событие_Транспорт"
    case myHomePush = "Push-событие_Мой дом"
}

class AnalyticsManagerImplementation: AnalyticsManager {
    
    var currentUserManager: CurrentUserManager!
    var userDefaults:UserDefaults!
    
    //MARK: - Публичные переменные
    
    var categoryName: String {
        return Class.mapObjectsCategoryName
    }
    
    var datasetName: String {
        return Class.mapObjectsDatasetName
    }
    
    var selectedNews:NewsSearchModel? {
        return Class.selectedNewsItem
    }
    
    var chatIdentifier: String? {
        return Class.chatIdentifier
    }
    
    var selectedUrl: URL? {
        return Class.selectedUrl
    }
    
    var currentStatsData: StatsData? {
        return Class.statsData
    }
    
    //MARK: - Статик данные
    
    static var mapObjectsCategoryName: String = AnalyticsConstants.emptyValueName
    static var mapObjectsDatasetName: String = AnalyticsConstants.emptyValueName
    static var selectedNewsItem: NewsSearchModel?
    static var chatIdentifier: String  = AnalyticsConstants.emptyValueName
    static var selectedUrl: URL?
    static var statsData: StatsData?
    
    typealias Class = AnalyticsManagerImplementation
    
    //MARK: - Setters
    
    func setMapObjectCategoryName(with name: String) {
        Class.mapObjectsCategoryName = name
    }
    
    func setMapObjectDatasetName(with name: String) {
        Class.mapObjectsDatasetName = name
    }
    
    func setSelectedNewsItem(with item: NewsSearchModel) {
        Class.selectedNewsItem = item
    }
    
    func setChatIdetifier(with chatId: String) {
        Class.chatIdentifier = chatId
    }
    
    func setSelectedUrl(with url: URL) {
        Class.selectedUrl = url
    }
    
    func setStatsData(with statsData: StatsData?) {
        Class.statsData = statsData
    }
    
    //MARK: - AnalyticsManager
    
    func sendSessionEvent(with event: SessionEvents) {
        
        var analyticsEvent: Event
        
        if event.functionName == AnalyticsConstants.firstSessionEventName {
            
            let key = ApplicationUserDefaultsKey.firstUserSession.rawValue
            var isFirstLaunchOfApp = userDefaults.bool(forKey: key)
            
            if userDefaults.object(forKey: key) == nil {
                
                userDefaults.set(false, forKey: key)
                userDefaults.synchronize()
                
                isFirstLaunchOfApp = true
            }
            
            let authorizationStatus = currentUserManager.currentUser.isLoggedIn ? AnalyticsConstants.AuthorizationStatus.authorized.rawValue : AnalyticsConstants.AuthorizationStatus.nonAuthorized.rawValue
            let sessionType:String = isFirstLaunchOfApp ? SessionType.firtSession.rawValue : SessionType.notFirstSession.rawValue
            let multipleParameter = event.getMultipleValueFirstParameter(from: (sessionType, authorizationStatus))
            
            analyticsEvent = Event(type: .session,
                                   functionName: event.functionName,
                                   multipleValueFirstParameter: multipleParameter)
        }
        else {
            
            analyticsEvent = Event(type: .session,
                                   functionName: event.functionName,
                                   firstParameterName: event.parameterName,
                                   firstParameterValue: event.parameterValue,
                                   actionValue: event.actionValue)
        }
        
        sendEvent(with: analyticsEvent)
    }
    
    
    func sendAuthorizationSignInEvent(with event: AuthorizationSignInEvents, errorText: String?) {
        
        let analyticsEvent = Event(type: .authorization,
                          functionName: event.functionName,
                          firstParameterName: event.firstParameterName,
                          firstParameterValue: errorText ?? event.firstParameterValue,
                          actionValue: event.actionValue)
       
        sendEvent(with: analyticsEvent)
    }
    
    func sendForgotPasswordEvent(with event: AuthorizationForgotPasswordEvents, errorText: String?) {
        
        let analyticsEvent = Event(type: .forgotPassword,
                                   functionName: event.functionName,
                                   firstParameterName: event.firstParameterName,
                                   firstParameterValue: errorText ?? event.firstParameterValue,
                                   actionValue: event.actionValue)
        
        sendEvent(with: analyticsEvent)
    }
    
    func sendNewPasswordEvent(with event: AuthorizationNewPasswordEvents, errorText: String?) {
        
        let analyticsEvent = Event(type: .newPassword,
                                   functionName: event.functionName,
                                   firstParameterName: event.firstParameterName,
                                   firstParameterValue: errorText ?? event.firstParameterValue,
                                   actionValue: event.actionValue)
        
        sendEvent(with: analyticsEvent)
    }
    
    func sendSignUpEvent(with event: AuthorizationSignUpEvents, errorText: String?) {
        
        let analyticsEvent = Event(type: .registration,
                                   functionName: event.functionName,
                                   firstParameterName:  event.firstParameterName,
                                   firstParameterValue: errorText ?? event.firstParameterValue,
                                   actionValue: event.actionValue)
        
        sendEvent(with: analyticsEvent)
    }
    
    func sendAddSmsCodeEvent(with event: AuthorizationAddSmsCodeEvents, errorText: String?) {
        
        let analyticsEvent = Event(type: .addSmsCode,
                                   functionName: event.functionName,
                                   firstParameterName:  event.firstParameterName,
                                   firstParameterValue: errorText ?? event.firstParameterValue,
                                   actionValue: event.actionValue)
        
        sendEvent(with: analyticsEvent)
    }
    
    func sendFeedbackEvent(with event: FeedbackEvents, categoryName: String?, errorText: String?) {
        
        var analyticsEvent: Event
        
        if event.functionName == AnalyticsConstants.feedbackSentEventName {
            
            let categoryName = categoryName ?? ""
            let authorizationStatus = currentUserManager.currentUser.isLoggedIn ? AnalyticsConstants.AuthorizationStatus.authorized.rawValue : AnalyticsConstants.AuthorizationStatus.nonAuthorized.rawValue
            let error = errorText ?? event.firstParameterValue
            
            let multipleValueFirstParameter = event.getMultipleValueFirstParameter(from: (categoryName,authorizationStatus, error))
            
            analyticsEvent = Event(type: .feedback,
                          functionName: event.functionName,
                          multipleValueFirstParameter: multipleValueFirstParameter)
        }
        else {
            analyticsEvent = Event(type: .feedback,
                          functionName: event.functionName,
                          firstParameterName: event.firstParameterName,
                          firstParameterValue: event.firstParameterValue,
                          actionValue: event.actionValue)
        }
       
        sendEvent(with: analyticsEvent)
    }
    
    func sendAgreementsEvent(with event: AgreementsEvents) {
        
        let analyticsEvent = Event(type: .agreements,
                                   functionName: event.functionName,
                                   firstParameterName: event.firstParameterName,
                                   firstParameterValue: AnalyticsDefaultsParams.authorizationStatus.rawValue,
                                   actionValue: event.actionValue)
        
        sendEvent(with: analyticsEvent)
    }
    
    func sendAppsAndServicesEvent(with event: AppsAndServecesEvents, appName: String?, error: String?) {
        
        let optionalValue = appName ?? error
        
        let analyticsEvent = Event(type: .appsAndServices,
                                   functionName: event.functionName,
                                   firstParameterName: event.firstParameterName,
                                   firstParameterValue: optionalValue ?? event.firstParameterValue,
                                   actionValue: event.actionValue)
        
        sendEvent(with: analyticsEvent)
    }
    
    func sendAboutAppEvent(with event: AboutAppEvents, sharingData: SharingEventData?) {
        
        var analyticsEvent: Event
        
        if sharingData != nil {
            
            let multipleValueFirstParameter = event.getMultipleValueFirstParameter(from: sharingData!)
            
            analyticsEvent = Event(type: .aboutApp,
                                   functionName: event.functionName,
                                   multipleValueFirstParameter: multipleValueFirstParameter)
        }
        else {
            analyticsEvent = Event(type: .aboutApp,
                                   functionName: event.functionName,
                                   firstParameterName: event.firstParameterName,
                                   firstParameterValue: event.firstParameterValue,
                                   actionValue: event.actionValue)
        }
        
        sendEvent(with: analyticsEvent)
    }
    
    func sendServiceEvent(with eventType: CategoryType, event: ServicesEvents, errorText: String?) {
        
        let analyticsEvent = Event(type: eventType,
                                   functionName: event.functionName,
                                   firstParameterName: event.firstParameterName,
                                   firstParameterValue: errorText ?? event.firstParameterValue,
                                   actionValue: event.actionValue)
        
        sendEvent(with: analyticsEvent)
    }
    
    func sendNotificationServiceEvent(with event: NotificationServiceEvents, data: NotificationEventData) {
        
        let multipleValueFirstParameter = event.getLoadedScreenMultipleParameter(from: data)
        
        let analyticsEvent = Event(type: .notificationService,
                               functionName: event.functionName,
                               multipleValueFirstParameter: multipleValueFirstParameter)
        
        sendEvent(with: analyticsEvent)
    }
    
    func sendUserPersonalInfoEvent(with event: UserPersonalInfoEvents) {
        
        let analyticsEvent = Event(type: .personalInfo,
                                   functionName: event.functionName,
                                   firstParameterName: event.firstParameterName,
                                   firstParameterValue: event.firstParameterValue,
                                   actionValue: event.actionValue)
        
        sendEvent(with: analyticsEvent)
    }
    
    func sendUserAccountEvent(with event: UserAccountEvents, selectedServiceName: String?, errorText: String?) {
        
        let analyticsEvent = Event(type: .userAccount,
                                   functionName: event.functionName,
                                   firstParameterName: selectedServiceName ?? event.firstParameterName,
                                   firstParameterValue: errorText ?? event.firstParameterValue,
                                   actionValue: event.actionValue)
        
        sendEvent(with: analyticsEvent)
    }
    
    func sendGlobalSearchEvent(with event: GlobalSearchEvents, requestData: GlobalSearchEventData?, selectedChatData: GlobalSearchSelectChatEventData?, selectedSearchResultData: GlobalSearchSelectResultEventData?, scrolledToLastNews: Bool?) {
        
        var analyticsEvent: Event
        
        if selectedChatData != nil ||  selectedSearchResultData != nil || requestData != nil {
            
            let authorizationStatus = currentUserManager.currentUser.isLoggedIn ? AnalyticsConstants.AuthorizationStatus.authorized.rawValue : AnalyticsConstants.AuthorizationStatus.nonAuthorized.rawValue
            
            var requestData: GlobalSearchEventData? = requestData
            var chatData: GlobalSearchSelectChatEventData? = selectedChatData
            var resultData: GlobalSearchSelectResultEventData? = selectedSearchResultData
            
            if requestData != nil {
                requestData!.authorizationStatus = authorizationStatus
            }
            
            if selectedChatData != nil {
                chatData!.authorizationStatus = authorizationStatus
            }
            
            if selectedSearchResultData != nil {
                resultData!.authorizationStatus = authorizationStatus
            }
            
            let multipleValueFirstParameter = event.getMultipleValueFirstParameter(from: requestData, chatData: chatData, selectedResultData: resultData)
            
            analyticsEvent = Event(type: .globalSearch,
                                   functionName: event.functionName,
                                   multipleValueFirstParameter: multipleValueFirstParameter)
        }
        else {
            var firstParameterValue = event.firstParameterValue
            
            if let scrolled = scrolledToLastNews {
                firstParameterValue = scrolled ? "Да" : "Нет"
            }
            
            analyticsEvent = Event(type: .globalSearch,
                                   functionName: event.functionName,
                                   firstParameterName: event.firstParameterName,
                                   firstParameterValue: firstParameterValue,
                                   actionValue: event.actionValue)
        }
        
        sendEvent(with: analyticsEvent)
    }
    
    func sendMainScreenEvent(with event: ConversationsScreenEvents, chatName: String?) {
        
        let analyticsEvent = Event(type: .mainScreen,
                                   functionName: event.functionName,
                                   firstParameterName: chatName ?? event.firstParameterName,
                                   firstParameterValue: event.firstParameterValue,
                                   actionValue: event.actionValue)
        
        sendEvent(with: analyticsEvent)
    }
    
    func sendMedecineEvent(with event: MedecineConversationEvents, aboutAppData: SharingConversationEventData?, appSharingSource:String?)
    {
        var analyticsEvent: Event
        
        if let aboutAppData = aboutAppData {
            
            let authorizationStatus = currentUserManager.currentUser.isLoggedIn ? AnalyticsConstants.AuthorizationStatus.authorized.rawValue : AnalyticsConstants.AuthorizationStatus.nonAuthorized.rawValue
            
            var sharingEventData: SharingConversationEventData = aboutAppData
            sharingEventData.authorizationStatus = authorizationStatus
            
            let multipleValueFirstParameter = event.getMultipleValueAboutChat(from: sharingEventData)
            
            analyticsEvent = Event(type: .healthConversation,
                                   functionName: event.functionName,
                                   multipleValueFirstParameter: multipleValueFirstParameter)
        }
        else {
            
            analyticsEvent = Event(type: .healthConversation,
                                   functionName: event.functionName,
                                   firstParameterName: event.firstParameterName,
                                   firstParameterValue: appSharingSource ?? event.firstParameterValue,
                                   actionValue: event.actionValue)
        }
        
        sendEvent(with: analyticsEvent)
    }
    
    func sendChildsConversationEvent(with event: ChildsConversationEvents, aboutAppData: SharingConversationEventData?, appSharingSource:String?) {
        
        var analyticsEvent: Event
        
        if let aboutAppData = aboutAppData {
            
            let authorizationStatus = currentUserManager.currentUser.isLoggedIn ? AnalyticsConstants.AuthorizationStatus.authorized.rawValue : AnalyticsConstants.AuthorizationStatus.nonAuthorized.rawValue
            
            var sharingEventData: SharingConversationEventData = aboutAppData
            sharingEventData.authorizationStatus = authorizationStatus
            
            let multipleValueFirstParameter = event.getMultipleValueAboutChat(from: sharingEventData)
            
            analyticsEvent = Event(type: .childConversation,
                                   functionName: event.functionName,
                                   multipleValueFirstParameter: multipleValueFirstParameter)
        }
        else {
            
            analyticsEvent = Event(type: .childConversation,
                                   functionName: event.functionName,
                                   firstParameterName: event.firstParameterName,
                                   firstParameterValue: appSharingSource ?? event.firstParameterValue,
                                   actionValue: event.actionValue)
        }
        
        sendEvent(with: analyticsEvent)
    }
    
    func sendTransportConversationEvent(with event: TransportConversationEvents, aboutAppData: SharingConversationEventData?, appSharingSource:String?) {
        
        var analyticsEvent: Event
        
        if let aboutAppData = aboutAppData {
            
            let authorizationStatus = currentUserManager.currentUser.isLoggedIn ? AnalyticsConstants.AuthorizationStatus.authorized.rawValue : AnalyticsConstants.AuthorizationStatus.nonAuthorized.rawValue
            
            var sharingEventData: SharingConversationEventData = aboutAppData
            sharingEventData.authorizationStatus = authorizationStatus
            
            let multipleValueFirstParameter = event.getMultipleValueAboutChat(from: sharingEventData)
            
            analyticsEvent = Event(type: .transportConversation,
                                   functionName: event.functionName,
                                   multipleValueFirstParameter: multipleValueFirstParameter)
        }
        else {
            
            analyticsEvent = Event(type: .transportConversation,
                                   functionName: event.functionName,
                                   firstParameterName: event.firstParameterName,
                                   firstParameterValue: appSharingSource ?? event.firstParameterValue,
                                   actionValue: event.actionValue)
        }
        
        sendEvent(with: analyticsEvent)
    }
    
    func sendConsultationConversationEvent(with event: ConsultationConversationEvents, aboutAppData: SharingConversationEventData?, appSharingSource:String?) {
        
        var analyticsEvent: Event
        
        if let aboutAppData = aboutAppData {
            
            let authorizationStatus = currentUserManager.currentUser.isLoggedIn ? AnalyticsConstants.AuthorizationStatus.authorized.rawValue : AnalyticsConstants.AuthorizationStatus.nonAuthorized.rawValue
            
            var sharingEventData: SharingConversationEventData = aboutAppData
            sharingEventData.authorizationStatus = authorizationStatus
            
            let multipleValueFirstParameter = event.getMultipleValueAboutChat(from: sharingEventData)
            
            analyticsEvent = Event(type: .consultationConversation,
                                   functionName: event.functionName,
                                   multipleValueFirstParameter: multipleValueFirstParameter)
        }
        else {
            
            analyticsEvent = Event(type: .consultationConversation,
                                   functionName: event.functionName,
                                   firstParameterName: event.firstParameterName,
                                   firstParameterValue: appSharingSource ?? event.firstParameterValue,
                                   actionValue: event.actionValue)
        }
        
        sendEvent(with: analyticsEvent)
    }
    
    func sendNewsConversationEvent(with event: NewsConversationEvents, aboutAppData: SharingConversationEventData?, appSharingSource:String?) {
        
        var analyticsEvent: Event
        
        if let aboutAppData = aboutAppData {
            
            let authorizationStatus = currentUserManager.currentUser.isLoggedIn ? AnalyticsConstants.AuthorizationStatus.authorized.rawValue : AnalyticsConstants.AuthorizationStatus.nonAuthorized.rawValue
            
            var sharingEventData: SharingConversationEventData = aboutAppData
            sharingEventData.authorizationStatus = authorizationStatus
            
            let multipleValueFirstParameter = event.getMultipleValueAboutChat(from: sharingEventData)
            
            analyticsEvent = Event(type: .cityNewsConversation,
                                   functionName: event.functionName,
                                   multipleValueFirstParameter: multipleValueFirstParameter)
        }
        else {
            
            analyticsEvent = Event(type: .cityNewsConversation,
                                   functionName: event.functionName,
                                   firstParameterName: event.firstParameterName,
                                   firstParameterValue: appSharingSource ?? event.firstParameterValue,
                                   actionValue: event.actionValue)
        }
        
        sendEvent(with: analyticsEvent)
    }
    
    func sendMyHomeConversationEvent(with event: MyHomeConversationEvents, aboutAppData: SharingConversationEventData?, appSharingSource:String?) {
        
        var analyticsEvent: Event
        
        if let aboutAppData = aboutAppData {
            
            let authorizationStatus = currentUserManager.currentUser.isLoggedIn ? AnalyticsConstants.AuthorizationStatus.authorized.rawValue : AnalyticsConstants.AuthorizationStatus.nonAuthorized.rawValue
            
            var sharingEventData: SharingConversationEventData = aboutAppData
            sharingEventData.authorizationStatus = authorizationStatus
            
            let multipleValueFirstParameter = event.getMultipleValueAboutChat(from: sharingEventData)
            
            analyticsEvent = Event(type: .myHomeConversation,
                                   functionName: event.functionName,
                                   multipleValueFirstParameter: multipleValueFirstParameter)
        }
        else {
            
            analyticsEvent = Event(type: .myHomeConversation,
                                   functionName: event.functionName,
                                   firstParameterName: event.firstParameterName,
                                   firstParameterValue: appSharingSource ?? event.firstParameterValue,
                                   actionValue: event.actionValue)
        }
        
        sendEvent(with: analyticsEvent)
    }
    
    func sendMapConversationEvent(with event: MapSearchEvents, aboutAppData: SharingConversationEventData?, appSharingSource:String?, datasetData: DatasetEventData?, mapObjectSharingData: MapObjectInfoEventData?, overlapData: OverlapObjectInfoEventData?) {
        
        var analyticsEvent: Event
        
        if aboutAppData != nil || datasetData != nil ||
            mapObjectSharingData != nil || overlapData != nil {
            
            let authorizationStatus = currentUserManager.currentUser.isLoggedIn ? AnalyticsConstants.AuthorizationStatus.authorized.rawValue : AnalyticsConstants.AuthorizationStatus.nonAuthorized.rawValue
            
            var aboutAppData: SharingConversationEventData? = aboutAppData
            var datasetData: DatasetEventData? = datasetData
            var mapObjectSharingData:MapObjectInfoEventData? = mapObjectSharingData
            var overlapData: OverlapObjectInfoEventData? = overlapData
            
            if aboutAppData != nil {
                aboutAppData!.authorizationStatus = authorizationStatus
            }
            
            if datasetData != nil {
                datasetData!.authorizationStatus = authorizationStatus
            }
            
            if mapObjectSharingData != nil {
                mapObjectSharingData!.authorizationStatus = authorizationStatus
            }
            
            if overlapData != nil {
                overlapData!.authorizationStatus = authorizationStatus
            }
            
            let multipleValueFirstParameter = event.getMultipleValueFirstParameter(from: datasetData, mapObjectSharingData: mapObjectSharingData, overlapData: overlapData, aboutChatSharing: aboutAppData)
            
            analyticsEvent = Event(type: .mapSearchConversation,
                                   functionName: event.functionName,
                                   multipleValueFirstParameter: multipleValueFirstParameter)
        }
        else {
            analyticsEvent = Event(type: .mapSearchConversation,
                                   functionName: event.functionName,
                                   firstParameterName: event.firstParameterName,
                                   firstParameterValue: event.firstParameterValue,
                                   actionValue: event.actionValue)
        }
        
        sendEvent(with: analyticsEvent)
    }
    
    func sendPushNotificationEvent(by chatId: String, pushTypeId: Int64?, pushName: String?, pushDescription: String?) {
        
        typealias ChatIds = ConversationIdentificator
        let emptyValue = AnalyticsConstants.emptyValueName
        
        var analyticsEvent: Event
        
        switch chatId {
        case ChatIds.cityNews.rawValue:
            
            let newsPush = NewsPushNotificationEvents.self
            let event = newsPush.opened(withString: AnalyticsConstants.notificationOpenedEventName)
            let authorizationStatus = currentUserManager.currentUser.isLoggedIn ? AnalyticsConstants.AuthorizationStatus.authorized.rawValue : AnalyticsConstants.AuthorizationStatus.nonAuthorized.rawValue
            let data: PushNotificationEventData = (pushName ?? emptyValue, pushDescription ?? emptyValue, authorizationStatus)
            let mupltipleValue = event.getMultipleValue(from: data)
            
            analyticsEvent = Event(type: .newsPush,
                                   functionName: event.functionName,
                                   multipleValueFirstParameter: mupltipleValue)
        case ChatIds.children.rawValue:
            
            guard let statsPushId = pushTypeId else { return }
            
            let childrenPush = ChildrenPushNotificationEvents.self
            let event = childrenPush.opened(withString: AnalyticsConstants.notificationOpenedEventName)
            
            let parameterName = event.getFirstParameterName(from: statsPushId)
            
            analyticsEvent = Event(type: .childInSchoolPush,
                                   functionName: event.functionName,
                                   firstParameterName: parameterName,
                                   firstParameterValue: "\(statsPushId)",
                                   actionValue: event.actionValue)
        case ChatIds.transport.rawValue:
            guard let statsPushId = pushTypeId else { return }
            
            let transportPush = TransportPushNotificationEvents.self
            let event = transportPush.opened(withString: AnalyticsConstants.notificationOpenedEventName)
            
            let parameterName = event.getFirstParameterName(from: statsPushId)
            
            analyticsEvent = Event(type: .transportPush,
                                   functionName: event.functionName,
                                   firstParameterName: parameterName,
                                   firstParameterValue: "\(statsPushId)",
                                   actionValue: event.actionValue)
        case ChatIds.myHome.rawValue:
            guard let statsPushId = pushTypeId else { return }
            
            let myHomePush = MyHomePushNotificationEvents.self
            let event = myHomePush.opened(withString: AnalyticsConstants.notificationOpenedEventName)
            
            let parameterName = event.getFirstParameterName(from: statsPushId)
            
            analyticsEvent = Event(type: .myHomePush,
                                   functionName: event.functionName,
                                   firstParameterName: parameterName,
                                   firstParameterValue: "\(statsPushId)",
                                   actionValue: event.actionValue)
        default:
            return
        }
        
        sendEvent(with: analyticsEvent)
    }
    
    func sendSomeEvent(with eventName: String, eventJson: String, errorText: String?) {
        
        var parameters: [AnyHashable : Any]?
        
        if let data = eventJson.data(using: .utf8), var jsonParams = try? JSONSerialization.jsonObject(with: data, options: []) as? Dictionary<String, Dictionary<String, Any>> {
            
            let error = errorText ?? AnalyticsConstants.emptyValueName
            let authorizationStatus = currentUserManager.currentUser.isLoggedIn ? AnalyticsConstants.AuthorizationStatus.authorized.rawValue : AnalyticsConstants.AuthorizationStatus.nonAuthorized.rawValue
            
            let errorEvent = [
                error:[
                    AnalyticsConstants.emptyValueName : AnalyticsConstants.Actions.click.rawValue
                ]
            ]
  
            let authEvent = [
                authorizationStatus:[
                    AnalyticsConstants.emptyValueName : AnalyticsConstants.Actions.click.rawValue
                ]
            ]
            
            let errorKey = AnalyticsConstants.errorParameterName
            let authorizationKey = AnalyticsConstants.authorizationStatusParameterName
            
            parameters = jsonParams
            
            if let parameter = jsonParams, let firstKey = parameter.keys.first, let values = parameter[firstKey], let errorValue = values[errorKey] as? String, errorValue.isEmpty {
               
                jsonParams![jsonParams!.keys.first!]![errorKey] = errorEvent
                parameters = jsonParams
            }
            
            if let parameter = jsonParams, let firstKey = parameter.keys.first, let values = parameter[firstKey], let authValue = values[authorizationKey] as? String, authValue.isEmpty {
                
                jsonParams![jsonParams!.keys.first!]![authorizationKey] = authEvent
                parameters = jsonParams
            }
        }
        
        guard let params = parameters else { return }
        
        sendEvent(with: eventName, eventParameters: params)
    }
    
    func sendSomeSharingEvent(with eventName: String, eventFunction: String, sharingSource: String) {
        
        let authorizationStatus = currentUserManager.currentUser.isLoggedIn ? AnalyticsConstants.AuthorizationStatus.authorized.rawValue : AnalyticsConstants.AuthorizationStatus.nonAuthorized.rawValue
        
        let sharingEvent = [
            AnalyticsConstants.shareAppParameterName:[
                sharingSource:[
                    AnalyticsConstants.emptyValueName : AnalyticsConstants.Actions.click.rawValue
                ]
            ]
        ]
        
        let authorizationEvent = [
            AnalyticsConstants.authorizationStatusParameterName:[
                authorizationStatus:[
                    AnalyticsConstants.emptyValueName : AnalyticsConstants.Actions.click.rawValue
                ]
            ]
        ]
        
        let parameters:[AnyHashable : Any] = [
            eventFunction:[sharingEvent, authorizationEvent]
        ]
        sendEvent(with: eventName, eventParameters: parameters)
    }
    
    //MARK: - Helpers
    
    /// Отправить событие (на любую метрику по вкусу)
    ///
    /// - Parameter event: событие
    fileprivate func sendEvent(with event: Event) {
        reportEventToYandexMetric(with: event)
    }
    
    fileprivate func sendEvent(with eventName: String, eventParameters: [AnyHashable : Any]) {
        
        YMMYandexMetrica.reportEvent(eventName, parameters: eventParameters) { (error) in
            print("Error when report event: \(error.localizedDescription)")
        }
    }
        
    /// Отправить событие на YandexMetric
    ///
    /// - Parameter event: событие для отправки
    fileprivate func reportEventToYandexMetric(with event: Event) {
        
        let parameters: [AnyHashable : Any]
        
        if !event.multipleValueFirstParameter.isEmpty {
            parameters = getMultipleValuedEventParameters(from: event)
        }
        else {
            parameters = getEventParameters(from: event)
        }
        
        YMMYandexMetrica.reportEvent(event.type.rawValue, parameters: parameters) { (error) in
            print("Error when report event: \(error.localizedDescription)")
        }
    }
    
    /// Получить параметры события в виде словаря из самого события
    ///
    /// - Parameter event: событие для отправки
    /// - Returns: словарь с параметрами события
    fileprivate func getEventParameters(from event: Event) -> [AnyHashable : Any] {
        
        var firstValue = event.firstParameterValue
        var firstParameter = event.firstParameterName
        
        if firstValue == AnalyticsDefaultsParams.authorizationStatus.rawValue {
            firstValue = currentUserManager.currentUser.isLoggedIn ? AnalyticsConstants.AuthorizationStatus.authorized.rawValue : AnalyticsConstants.AuthorizationStatus.nonAuthorized.rawValue
        }
        
        if firstParameter == AnalyticsConstants.errorParameterName && firstValue == AnalyticsConstants.emptyValueName {
            firstParameter = AnalyticsConstants.emptyValueName
        }
            
        let eventDictionary:[AnyHashable : Any] = [
            event.functionName:[
                firstParameter:[
                    firstValue:event.actionValue
                ]
            ]
        ]
        
        return eventDictionary
    }
    
    /// Получить мульти параметры события(когда в первом параметре несколько значений на одном уровне) в виде словаря из самого события
    ///
    /// - Parameter event: событие для отправки
    /// - Returns: словарь с параметрами события
    fileprivate func getMultipleValuedEventParameters(from event: Event) -> [AnyHashable : Any] {
        
        guard !event.multipleValueFirstParameter.isEmpty else { return [:] }
        
        let eventDictionary:[AnyHashable : Any] = [event.functionName : event.multipleValueFirstParameter]
        return eventDictionary
    }
}

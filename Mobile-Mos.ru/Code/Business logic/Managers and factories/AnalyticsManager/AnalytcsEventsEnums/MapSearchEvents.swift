//
//  MapSearchEvents.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 02.07.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Данные для события с датасетами
typealias DatasetEventData = (datasetName: String, categoryName: String, authorizationStatus: String, error: String)

/// Данные для события с объектом карты
typealias MapObjectInfoEventData = (sharingAppName: String, datasetName: String, categoryName: String, authorizationStatus: String, error: String)

/// Данные для события с перекрытиями
typealias OverlapObjectInfoEventData = (authorizationStatus: String, address: String, okrug: String, error: String)

/// События экрана Карты
///
/// - screenLoaded: экран загрузился
/// - selectDataset: выбран датасет
/// - searchDataset: осуществлен поиск датасета
/// - viewMapObjectInfo: просмотрена карточка объекта
/// - overlapMapObjectInfo: просмотрета карточка перекрытия
/// - shareMapObjectInfo: карточка объекта пошарина
/// - aboutConversation: просмотрено "О чате" в карте
/// - shareApp: чат пошарен куда то
/// - addMapObjectToFavorite: объект добавлен в избранное
enum MapSearchEvents {
    
    case screenLoaded(withString: String)
    case selectDataset(withString: String)
    case searchDataset(withString: String)
    case viewMapObjectInfo(withString: String)
    case overlapMapObjectInfo(withString: String)
    case shareMapObjectInfo(withString: String)
    case aboutConversation(withString: String)
    case shareApp(withString: String)
    case addMapObjectToFavorite(withString: String)
    
    var functionName: String {
        switch self {
        case let .screenLoaded(functionStringName)          : return functionStringName
        case let .selectDataset(functionStringName)         : return functionStringName
        case let .searchDataset(functionStringName)         : return functionStringName
        case let .viewMapObjectInfo(functionStringName)     : return functionStringName
        case let .shareMapObjectInfo(functionStringName)    : return functionStringName
        case let .aboutConversation(functionStringName)     : return functionStringName
        case let .shareApp(functionStringName)              : return functionStringName
        case let .overlapMapObjectInfo(functionStringName)  : return functionStringName
        case let .addMapObjectToFavorite(functionStringName): return functionStringName
        }
    }
    
    var firstParameterName: String {
        
        switch self {
        case .shareApp: return AnalyticsConstants.shareAppParameterName
        default:
            return AnalyticsConstants.authorizationStatusParameterName
        }
    }
    
    var firstParameterValue: String {
        
        switch self {
        default:
            return AnalyticsDefaultsParams.authorizationStatus.rawValue
        }
    }
    
    var actionValue: String {
        
        switch self {
        case .screenLoaded          :return AnalyticsConstants.Actions.view.rawValue
        case .selectDataset         :return AnalyticsConstants.Actions.click.rawValue
        case .searchDataset         :return AnalyticsConstants.Actions.click.rawValue
        case .viewMapObjectInfo     :return AnalyticsConstants.Actions.click.rawValue
        case .shareMapObjectInfo    :return AnalyticsConstants.Actions.click.rawValue
        case .aboutConversation     :return AnalyticsConstants.Actions.click.rawValue
        case .shareApp              :return AnalyticsConstants.Actions.click.rawValue
        case .overlapMapObjectInfo  :return AnalyticsConstants.Actions.click.rawValue
        case .addMapObjectToFavorite:return AnalyticsConstants.Actions.click.rawValue
        }
    }
    
    func getMultipleValueFirstParameter(from datasetData: DatasetEventData?, mapObjectSharingData: MapObjectInfoEventData?, overlapData: OverlapObjectInfoEventData?, aboutChatSharing: SharingConversationEventData?) -> [[AnyHashable: Any]] {
        
        switch self {
        case .selectDataset         :return getMultipleValueParametersForDatasetEvent(from: datasetData)
        case .searchDataset         :return getMultipleValueParametersForDatasetEvent(from: datasetData)
        case .viewMapObjectInfo     :return getMultipleValueParametersForDatasetEvent(from: datasetData)
        case .shareMapObjectInfo    :return getMultipleValueParametersForMapObjectSharingEvent(from: mapObjectSharingData)
        case .aboutConversation     :return getMultipleValueAboutChat(from: aboutChatSharing)
        case .overlapMapObjectInfo  :return getMultipleValueParametersForOverlapObjectEvent(from: overlapData)
        case .addMapObjectToFavorite:return getMultipleValueParametersForDatasetEvent(from: datasetData)
        default:
            return []
        }
    }
    
    fileprivate func getMultipleValueFirstParametersForSharing(from data: SharingEventData?) -> [[AnyHashable: Any]] {
        
        guard let data = data else { return [] }
        
        var dataDictionary: [[AnyHashable: Any]] = []
        
        let sharingParameter:[AnyHashable : Any] = [
            AnalyticsConstants.appNameParameter:[
                data.appName:actionValue
            ]
        ]
        
        dataDictionary.append(sharingParameter)
        
        if data.error != AnalyticsConstants.emptyValueName {
            let errorParameter:[AnyHashable : Any] = [
                AnalyticsConstants.errorParameterName:[
                    data.error:actionValue
                ]
            ]
            dataDictionary.append(errorParameter)
        }
        
        return dataDictionary
    }
    
    fileprivate func getMultipleValueParametersForDatasetEvent(from data: DatasetEventData?) -> [[AnyHashable: Any]] {
        
        guard let data = data else { return [] }
        
        var dataDictionary: [[AnyHashable: Any]] = []
        
        let datasetNameParameter:[AnyHashable : Any] = [
            AnalyticsConstants.datasetNameParameterName:[
                data.datasetName:actionValue
            ]
        ]
        
        let categoryNameParameter:[AnyHashable : Any] = [
            AnalyticsConstants.categoryParameterName:[
                data.categoryName:actionValue
            ]
        ]
        
        let authStatusParameter:[AnyHashable : Any] = [
            AnalyticsConstants.authorizationStatusParameterName:[
                data.authorizationStatus:actionValue
            ]
        ]
        
        dataDictionary.append(datasetNameParameter)
        dataDictionary.append(categoryNameParameter)
        dataDictionary.append(authStatusParameter)
        
        if data.error != AnalyticsConstants.emptyValueName {
            let errorParameter:[AnyHashable : Any] = [
                AnalyticsConstants.errorParameterName:[
                    data.error:actionValue
                ]
            ]
            dataDictionary.append(errorParameter)
        }
        
        return dataDictionary
    }
    
    fileprivate func getMultipleValueParametersForMapObjectSharingEvent(from data: MapObjectInfoEventData?) -> [[AnyHashable: Any]] {
        
        guard let data = data else { return [] }
        
        var dataDictionary: [[AnyHashable: Any]] = []
        
        let appNameParameter:[AnyHashable : Any] = [
            AnalyticsConstants.appNameParameter:[
                data.sharingAppName:actionValue
            ]
        ]
        
        let datasetNameParameter:[AnyHashable : Any] = [
            AnalyticsConstants.datasetNameParameterName:[
                data.datasetName:actionValue
            ]
        ]
        
        let categoryNameParameter:[AnyHashable : Any] = [
            AnalyticsConstants.categoryParameterName:[
                data.categoryName:actionValue
            ]
        ]
        
        let authStatusParameter:[AnyHashable : Any] = [
            AnalyticsConstants.authorizationStatusParameterName:[
                data.authorizationStatus:actionValue
            ]
        ]
        
        dataDictionary.append(appNameParameter)
        dataDictionary.append(datasetNameParameter)
        dataDictionary.append(categoryNameParameter)
        dataDictionary.append(authStatusParameter)
        
        if data.error != AnalyticsConstants.emptyValueName {
            let errorParameter:[AnyHashable : Any] = [
                AnalyticsConstants.errorParameterName:[
                    data.error:actionValue
                ]
            ]
            dataDictionary.append(errorParameter)
        }
        
        return dataDictionary
    }
    
    fileprivate func getMultipleValueParametersForOverlapObjectEvent(from data: OverlapObjectInfoEventData?) -> [[AnyHashable: Any]] {
        
        guard let data = data else { return [] }
        
        var dataDictionary: [[AnyHashable: Any]] = []
        
        let addressNameParameter:[AnyHashable : Any] = [
            AnalyticsConstants.overlapAddressParameterName:[
                data.address:actionValue
            ]
        ]
        
        let okrugNameParameter:[AnyHashable : Any] = [
            AnalyticsConstants.overlapOkgurParameterName:[
                data.okrug:actionValue
            ]
        ]
        
        let authStatusParameter:[AnyHashable : Any] = [
            AnalyticsConstants.authorizationStatusParameterName:[
                data.authorizationStatus:actionValue
            ]
        ]
        
        dataDictionary.append(addressNameParameter)
        dataDictionary.append(okrugNameParameter)
        dataDictionary.append(authStatusParameter)
        
        if data.error != AnalyticsConstants.emptyValueName {
            let errorParameter:[AnyHashable : Any] = [
                AnalyticsConstants.errorParameterName:[
                    data.error:actionValue
                ]
            ]
            dataDictionary.append(errorParameter)
        }
        
        return dataDictionary
    }
    
   fileprivate func getMultipleValueAboutChat(from data: SharingConversationEventData?) -> [[AnyHashable: Any]] {
        
        guard let data = data else { return [] }
        
       let notificationEnabledString = data.notificationsEnabled ? AnalyticsConstants.notificationEnabled : AnalyticsConstants.notificationDisabled
        
        let notificationStatusParameter:[AnyHashable : Any] = [
            AnalyticsConstants.notificationStatusParameterName:[
                notificationEnabledString:actionValue
            ]
        ]
        
        let authorizationStatus:[AnyHashable : Any] = [
            AnalyticsConstants.authorizationStatusParameterName:[
                data.authorizationStatus:actionValue
            ]
        ]
        
        return [notificationStatusParameter,authorizationStatus]
    }
}

//
//  PushNotificationsEvents.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 09.07.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Данные, для открытия пуш уведомлений
typealias PushNotificationEventData = (notificationName: String, notificationDescription: String, authorizationStatus: String)

/// События пуш нотификации новостей
///
/// - opened: пушка открыта
enum NewsPushNotificationEvents {
    
    case opened(withString: String)
    
    var functionName: String {
        switch self {
        case let .opened(functionStringName) :return functionStringName
        }
    }
    
    var firstParameterName: String {
        
        switch self {
        case .opened: return AnalyticsConstants.notificationOpenedEventName
        }
    }
    
    var firstParameterValue: String {
        
        switch self {
        default:
            return AnalyticsConstants.emptyValueName
        }
    }
    
    var actionValue: String {
        
        switch self {
        case .opened   :return AnalyticsConstants.Actions.click.rawValue
        }
    }
    
    func getMultipleValue(from data: PushNotificationEventData) -> [[AnyHashable: Any]] {
        
        let notificationStatusParameter:[AnyHashable : Any] = [
            data.notificationName:[
                data.notificationDescription:actionValue
            ]
        ]
        
        let authorizationStatus:[AnyHashable : Any] = [
            AnalyticsConstants.authorizationStatusParameterName:[
                data.authorizationStatus:actionValue
            ]
        ]
        
        return [notificationStatusParameter,authorizationStatus]
    }
}

/// События пуш нотификаций консультаций
///
/// - opened: пушка открыта
enum ConsultationPushNotificationEvents {
    
    case opened(withString: String)
    
    var functionName: String {
        
        switch self {
        case let .opened(functionStringName) :return functionStringName
        }
    }
    
    var secondParameterName: String {
        
        switch self {
        default:
            return AnalyticsConstants.emptyValueName
        }
    }
    
    var actionValue: String {

        switch self {
        case .opened   :return AnalyticsConstants.Actions.click.rawValue
        }
    }
}

/// События пуш нотификации детей в школе
///
/// - opened: пушка открыта
enum ChildrenPushNotificationEvents {
    
    case opened(withString: String)
    
    var functionName: String {
        
        switch self {
        case let .opened(functionStringName) :return functionStringName
        }
    }
    
    var actionValue: String {
        
        switch self {
        case .opened   :return AnalyticsConstants.Actions.click.rawValue
        }
    }
    
    func getFirstParameterName(from pushTypeId: Int64) -> String {
        
        switch pushTypeId {
        case 901240001: return "Вход в школу"
        case 901240002: return "Выход из школы"
        case 901240003: return "Вход в школу с представителем"
        case 901240004: return "Выход из школы с представителем"
        case 901240005: return "Зачисление на л/с"
        case 901240007: return "Списание с л/с (Оплата буфета)"
        case 901240008: return "Платное горячее питание"
        case 901240009: return "Льготное питание"
        case 901240015: return "Критический остаток"
        case 901240010: return "Итоги дня"
        case 901240011: return "Итоги недели"
        case 901240012: return "Оповещение о снижении баланса"
        default:
            return AnalyticsConstants.emptyValueName
        }
    }
}

/// События пуш нотификации транспорта
///
/// - opened: пушка открыта
enum TransportPushNotificationEvents {
    
    case opened(withString: String)
    
    var functionName: String {
        
        switch self {
        case let .opened(functionStringName) :return functionStringName
        }
    }
    
    var actionValue: String {
        
        switch self {
        case .opened   :return AnalyticsConstants.Actions.click.rawValue
        }
    }
    
    func getFirstParameterName(from pushTypeId: Int64) -> String {
        
        switch pushTypeId {
        case 901190002: return "Эвакуация ТС на спецстоянку"
        case 901190003: return "Содержится ТС на спецстоянке"
        case 901190005: return "ТС выдано, нарушение закрыто"
        case 1: return "Информирование о новых штрафах по СТС и ВУ"
        default:
            return AnalyticsConstants.emptyValueName
        }
    }
}

/// События пуш нотификации моего дома
///
/// - opened: пушка открыта
enum MyHomePushNotificationEvents {
    
    case opened(withString: String)
    
    var functionName: String {
        
        switch self {
        case let .opened(functionStringName) :return functionStringName
        }
    }
    
    var actionValue: String {
        
        switch self {
        case .opened   :return AnalyticsConstants.Actions.click.rawValue
        }
    }
    
    func getFirstParameterName(from pushTypeId: Int64) -> String {
        
        switch pushTypeId {
        case 801270106: return "Оповещение о появлении нового ЕПД"
        case 801270105: return "Оповещение о появлении нового долгового ЕПД"
        case 800180005: return "Оповещение о появлении нового начисления"
        case 800180006: return "Оповещение о появлении нового долгового начисления"
        default:
            return AnalyticsConstants.emptyValueName
        }
    }
}

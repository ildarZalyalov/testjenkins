//
//  FeedbackEvents.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 28.06.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Данные, нужные для формирования Sent События
typealias FeedbackEventSentData = (categoryName: String, authorizationStatus: String, error: String)

/// События фидбэка пользователя
///
/// - screenLoaded: экран загрузился
/// - sendPressed: нажали отправить
/// - agreementsPressed: нажали на пользовательское соглашение
enum FeedbackEvents {
    
    case screenLoaded(withString: String)
    case sendPressed(withString: String)
    case agreementsPressed(withString: String)
    
    var functionName: String {
        
        switch self {
        case let .screenLoaded(functionStringName)      :return functionStringName
        case let .sendPressed(functionStringName)       :return functionStringName
        case let .agreementsPressed(functionStringName) :return functionStringName
        }
    }
    
    var firstParameterName: String {
        
        switch self {
        case .screenLoaded      :return AnalyticsConstants.authorizationStatusParameterName
        case .agreementsPressed :return AnalyticsConstants.authorizationStatusParameterName
        default :
            return AnalyticsConstants.emptyValueName
        }
    }
    
    var firstParameterValue: String {
        
        switch self {
        case .screenLoaded      :return AnalyticsDefaultsParams.authorizationStatus.rawValue
        case .agreementsPressed :return AnalyticsDefaultsParams.authorizationStatus.rawValue
        default:
            return AnalyticsConstants.emptyValueName
        }
    }
    
    var actionValue: String {
        
        switch self {
        case .screenLoaded      :return AnalyticsConstants.Actions.view.rawValue
        case .sendPressed       :return AnalyticsConstants.Actions.sent.rawValue
        case .agreementsPressed :return AnalyticsConstants.Actions.click.rawValue
        }
    }
    
    func getMultipleValueFirstParameter(from data: FeedbackEventSentData) -> [[AnyHashable: Any]] {
        
        var dataDictionary: [[AnyHashable: Any]] = []
        
        let categoryNameParameter:[AnyHashable : Any] = [
            AnalyticsConstants.categoryParameterName:[
                data.categoryName:actionValue
            ]
        ]
        
        let authStatusParameter:[AnyHashable : Any] = [
            AnalyticsConstants.authorizationStatusParameterName:[
                data.authorizationStatus:actionValue
            ]
        ]
        
        dataDictionary.append(categoryNameParameter)
        dataDictionary.append(authStatusParameter)
        
        if data.error != AnalyticsConstants.emptyValueName {
            let errorParameter:[AnyHashable : Any] = [
                AnalyticsConstants.errorParameterName:[
                    data.error:actionValue
                ]
            ]
            dataDictionary.append(errorParameter)
        }
      
        return dataDictionary
    }
}

//
//  SessionEvents.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 28.06.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Данные, нужные для формирования Sent События
typealias SessionEventData = (newUser: String, authorizationStatus: String)

/// События сессии
///
/// - firstSession: первый ли запуск приложения
/// - sessionBegin: сессия началась
/// - sessionEnd: сессия закончилась
enum SessionEvents {
    
    case firstSession(withString: String)
    case sessionBegin(withString: String)
    case sessionEnd(withString: String)
    
    var functionName: String {
        
        switch self {
        case let .firstSession(functionStringName) :return functionStringName
        case let .sessionBegin(functionStringName) :return functionStringName
        case let .sessionEnd(functionStringName)   :return functionStringName
        }
    }
    
    var parameterName: String {

        switch self {
        case .sessionBegin :return AnalyticsConstants.authorizationStatusParameterName
        case .sessionEnd   :return AnalyticsConstants.authorizationStatusParameterName
        default:
            return AnalyticsConstants.emptyValueName
        }
    }
    
    var parameterValue: String {

        switch self {
        case .firstSession :return AnalyticsConstants.emptyValueName
        case .sessionBegin :return AnalyticsDefaultsParams.authorizationStatus.rawValue
        case .sessionEnd   :return AnalyticsDefaultsParams.authorizationStatus.rawValue
        }
    }
    
    var actionValue: String {
        
        switch self {
        case .firstSession  :return AnalyticsConstants.Actions.start.rawValue
        case .sessionBegin  :return AnalyticsConstants.Actions.start.rawValue
        case .sessionEnd    :return AnalyticsConstants.Actions.pause.rawValue
        }
    }
    
    func getMultipleValueFirstParameter(from data: SessionEventData) -> [[AnyHashable: Any]] {
        
        let authStatusParameter:[AnyHashable : Any] = [
            AnalyticsConstants.authorizationStatusParameterName:[
                data.authorizationStatus:actionValue
            ]
        ]
        
        let newUserParameter:[AnyHashable : Any] = [
            AnalyticsConstants.newUserParameterName:[
                data.newUser:actionValue
            ]
        ]
        
        return [authStatusParameter,newUserParameter]
    }
}

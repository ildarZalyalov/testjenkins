//
//  AboutAppEventsEnums.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 28.06.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Данные, для шаринга из приложения
typealias SharingEventData = (appName: String, error: String)

/// Данные для события нотификация
typealias NotificationEventData = (consultationNotificationEnabled: Bool,
                                    childrenNotificationEnabled: Bool,
                                    transportNotificationEnabled: Bool,
                                    healthNotificationEnabled: Bool,
                                    newsNotificationEnabled: Bool,
                                    myHomeNotificationEnabled: Bool)

/// События "О приложении"
///
/// - screenLoaded: экран загрузился
/// - sharing: шаринг
/// - more: нажали на подробнее
/// - openInAppstore: открыть в сторе
enum AboutAppEvents {
    
    case screenLoaded(withString: String)
    case sharing(withString: String)
    case more(withString: String)
    case openInAppstore(withString: String)
    
    var functionName: String {
        switch self {
        case let .screenLoaded(functionStringName)  : return functionStringName
        case let .sharing(functionStringName)       : return functionStringName
        case let .more(functionStringName)          : return functionStringName
        case let .openInAppstore(functionStringName): return functionStringName
        }
    }
    
    var firstParameterName: String {
        
        switch self {
        
        default:
            return AnalyticsConstants.emptyValueName
        }
    }
    
    var firstParameterValue: String {
        
        switch self {
        default:
            return AnalyticsConstants.emptyValueName
        }
    }
    
    var actionValue: String {
        
        switch self {
        case .screenLoaded  :return AnalyticsConstants.Actions.view.rawValue
        case .sharing       :return AnalyticsConstants.Actions.click.rawValue
        case .more          :return AnalyticsConstants.Actions.click.rawValue
        case .openInAppstore:return AnalyticsConstants.Actions.click.rawValue
        }
    }
    
    func getMultipleValueFirstParameter(from data: SharingEventData) -> [[AnyHashable: Any]] {
        
        var dataDictionary: [[AnyHashable: Any]] = []
        
        let sharingParameter:[AnyHashable : Any] = [
            AnalyticsConstants.appNameParameter:[
                data.appName:actionValue
            ]
        ]
        
        dataDictionary.append(sharingParameter)
    
        if data.error != AnalyticsConstants.emptyValueName {
            let errorParameter:[AnyHashable : Any] = [
                AnalyticsConstants.errorParameterName:[
                    data.error:actionValue
                ]
            ]
            dataDictionary.append(errorParameter)
        }
        
        return dataDictionary
    }
}



/// События раздела уведомлений
///
/// - screenLoaded: экран загрузился
enum NotificationServiceEvents {
    
    case screenLoaded(withString: String)
    
    var functionName: String {
        switch self {
        case let .screenLoaded(functionStringName) :return functionStringName
        }
    }
    
    var firstParameterName: String {
        
        switch self {
        case .screenLoaded: return AnalyticsConstants.emptyValueName
        }
    }
    
    var firstParameterValue: String {
        
        switch self {
        default:
            return AnalyticsConstants.emptyValueName
        }
    }
    
    var actionValue: String {
        
        switch self {
        case .screenLoaded  :return AnalyticsConstants.Actions.view.rawValue
        }
    }
    
    func getLoadedScreenMultipleParameter(from data: NotificationEventData) -> [[AnyHashable: Any]] {
        
        let consState = convertNotificationStateToString(boolStateValue: data.consultationNotificationEnabled)
        let childState = convertNotificationStateToString(boolStateValue: data.childrenNotificationEnabled)
        let transportState = convertNotificationStateToString(boolStateValue: data.transportNotificationEnabled)
        let healthState = convertNotificationStateToString(boolStateValue: data.healthNotificationEnabled)
        let newsState = convertNotificationStateToString(boolStateValue: data.newsNotificationEnabled)
        let myHomeState = convertNotificationStateToString(boolStateValue: data.myHomeNotificationEnabled)
        
        let consultationNameParameter:[AnyHashable : Any] = [
            AnalyticsConstants.consultationNotification:[
                consState:actionValue
            ]
        ]
        
        let childNameParameter:[AnyHashable : Any] = [
            AnalyticsConstants.childNotification:[
                childState:actionValue
            ]
        ]
        
        let transportNameParameter:[AnyHashable : Any] = [
            AnalyticsConstants.transportNotification:[
                transportState:actionValue
            ]
        ]
        
        let healthNameParameter:[AnyHashable : Any] = [
            AnalyticsConstants.healthNotification:[
                healthState:actionValue
            ]
        ]
        
        let newsNameParameter:[AnyHashable : Any] = [
            AnalyticsConstants.newsNotification:[
                newsState:actionValue
            ]
        ]
        
        let myHomeNameParameter:[AnyHashable : Any] = [
            AnalyticsConstants.myHomeNotification:[
                myHomeState:actionValue
            ]
        ]
        
        return [consultationNameParameter,childNameParameter, transportNameParameter, healthNameParameter, newsNameParameter, myHomeNameParameter]
    }
    
    fileprivate func convertNotificationStateToString(boolStateValue: Bool) -> String {
        return boolStateValue == true ? "Вкл" : "Выкл"
    }
}

/// События редактирование
///
/// - screenLoaded: экран загрузился
enum UserPersonalInfoEvents {
    
    case screenLoaded(withString: String)
    
    var functionName: String {
        switch self {
        case let .screenLoaded(functionStringName) :return functionStringName
        }
    }
    
    var firstParameterName: String {
        
        switch self {
        case .screenLoaded: return AnalyticsConstants.emptyValueName
        }
    }
    
    var firstParameterValue: String {
        
        switch self {
        default:
            return AnalyticsConstants.emptyValueName
        }
    }
    
    var actionValue: String {
        
        switch self {
        case .screenLoaded      :return AnalyticsConstants.Actions.view.rawValue
        }
    }
}

/// События сервисов
///
/// - screenLoaded: экран загрузился
/// - edit: выбрано редактирование
/// - add: выбрано добавление
/// - openLinkInstrusction: выбрано открытие ссылки 
enum ServicesEvents {
    
    case screenLoaded(withString: String)
    case edit(withString: String)
    case add(withString: String)
    case openLinkInstrusction(withString: String)
    
    var functionName: String {
        switch self {
        case let .screenLoaded(functionStringName)         :return functionStringName
        case let .edit(functionStringName)                 :return functionStringName
        case let .add(functionStringName)                  :return functionStringName
        case let .openLinkInstrusction(functionStringName) :return functionStringName
        }
    }
    
    var firstParameterName: String {
        
        switch self {
            default:
            return AnalyticsConstants.emptyValueName
        }
    }
    
    var firstParameterValue: String {
        
        switch self {
        case .openLinkInstrusction:
            return AnalyticsConstants.errorParameterName
        default:
            return AnalyticsConstants.emptyValueName
        }
    }
    
    var actionValue: String {
        
        switch self {
        case .screenLoaded         :return AnalyticsConstants.Actions.view.rawValue
        case .edit                 :return AnalyticsConstants.Actions.click.rawValue
        case .add                  :return AnalyticsConstants.Actions.click.rawValue
        case .openLinkInstrusction :return AnalyticsConstants.Actions.click.rawValue
        }
    }
}

/// Событие аккаунта пользователя
///
/// - screenLoaded: экран загрузился
/// - exit: экшен выхода
/// - selectedService: выбор любого сервиса
enum UserAccountEvents {
    
    case screenLoaded(withString: String)
    case exit(withString: String)
    case selectedService(withString: String)
    
    var functionName: String {
        switch self {
        case let .screenLoaded(functionStringName)   :return functionStringName
        case let .exit(functionStringName)           :return functionStringName
        case let .selectedService(functionStringName):return functionStringName
        }
    }
    
    var firstParameterName: String {
        
        switch self {
        case .exit: return AnalyticsConstants.errorParameterName
        default:
            return AnalyticsConstants.emptyValueName
        }
    }
    
    var firstParameterValue: String {
        
        switch self {
        default:
            return AnalyticsConstants.emptyValueName
        }
    }
    
    var actionValue: String {
        
        switch self {
        case .screenLoaded      :return AnalyticsConstants.Actions.view.rawValue
        case .exit              :return AnalyticsConstants.Actions.logout.rawValue
        case .selectedService   :return AnalyticsConstants.Actions.click.rawValue
        }
    }
}

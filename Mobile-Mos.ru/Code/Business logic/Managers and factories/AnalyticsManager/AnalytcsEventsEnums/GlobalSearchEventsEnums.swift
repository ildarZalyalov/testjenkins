//
//  GlobalSearchEventsEnums.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 28.06.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Данные, нужные для формирования Sent События
typealias GlobalSearchEventData = (searchText: String, authorizationStatus: String, error: String)

/// Данные, нужные для формирования выбора чата
typealias GlobalSearchSelectChatEventData = (searchText: String, authorizationStatus: String, chatName: String)

/// Данные, нужные для формирования выбора результата
typealias GlobalSearchSelectResultEventData = (title: String, urlString: String, type:String, authorizationStatus: String, error: String)

/// События по глобальному поиска
///
/// - screenLoaded: экран поиска
/// - request: осуществлен запрос текст запрос
/// - selectedRequest: выбран запрос из топа
/// - selectChat: выбран чат из результатов запроса
/// - selectSearchResult: выбран результат поиска
enum GlobalSearchEvents {
    
    case screenLoaded(withString: String)
    case request(withString: String)
    case selectedRequest(withString: String)
    case selectChat(withString: String)
    case selectSearchResult(withString: String)
    case scrolledToLastNews(withString: String)
    
    var functionName: String {
        switch self {
        case let .screenLoaded(functionStringName)      :return functionStringName
        case let .request(functionStringName)           :return functionStringName
        case let .selectedRequest(functionStringName)   :return functionStringName
        case let .selectChat(functionStringName)        :return functionStringName
        case let .selectSearchResult(functionStringName):return functionStringName
        case let .scrolledToLastNews(functionStringName):return functionStringName
        }
    }
    
    var firstParameterName: String {
        
        switch self {
        case .screenLoaded       :return AnalyticsConstants.authorizationStatusParameterName
        case .scrolledToLastNews :return AnalyticsConstants.scrolledToLastNewsParameterName
        default:
            return AnalyticsConstants.emptyValueName
        }
    }
    
    var firstParameterValue: String {
        
        switch self {
        case .screenLoaded: return AnalyticsDefaultsParams.authorizationStatus.rawValue
        default:
            return AnalyticsConstants.emptyValueName
        }
    }
    
    var actionValue: String {
        
        switch self {
        case .screenLoaded      : return AnalyticsConstants.Actions.view.rawValue
        case .request           : return AnalyticsConstants.Actions.click.rawValue
        case .selectedRequest   : return AnalyticsConstants.Actions.click.rawValue
        case .selectChat        : return AnalyticsConstants.Actions.click.rawValue
        case .selectSearchResult: return AnalyticsConstants.Actions.click.rawValue
        case .scrolledToLastNews: return AnalyticsConstants.emptyValueName
        }
    }
    
    func getMultipleValueFirstParameter(from requestData: GlobalSearchEventData?, chatData: GlobalSearchSelectChatEventData?, selectedResultData: GlobalSearchSelectResultEventData?) -> [[AnyHashable: Any]] {
        
        switch self {
        case .selectedRequest   : return getMultipleValueParameterForSearchRequest(from: requestData)
        case .request           : return getMultipleValueParameterForSearchRequest(from: requestData)
        case .selectChat        : return getMultipleValueParametersForChat(from: chatData)
        case .selectSearchResult: return getMultipleValueParametersForResult(from: selectedResultData)
        default:
            return []
        }
    }
    
    fileprivate func getMultipleValueParameterForSearchRequest(from data: GlobalSearchEventData?) -> [[AnyHashable: Any]] {
        
        guard let data = data else { return [] }
        
        var dataDictionary: [[AnyHashable: Any]] = []
        
        let searchTextNameParameter:[AnyHashable : Any] = [
            AnalyticsConstants.requestParameterName:[
                data.searchText:actionValue
            ]
        ]
        
        let authStatusParameter:[AnyHashable : Any] = [
            AnalyticsConstants.authorizationStatusParameterName:[
                data.authorizationStatus:actionValue
            ]
        ]
        
        dataDictionary.append(searchTextNameParameter)
        dataDictionary.append(authStatusParameter)
        
        if data.error != AnalyticsConstants.emptyValueName {
            let errorParameter:[AnyHashable : Any] = [
                AnalyticsConstants.errorParameterName:[
                    data.error:actionValue
                ]
            ]
            dataDictionary.append(errorParameter)
        }
        
        
        return dataDictionary
    }
    
    fileprivate func getMultipleValueParametersForChat(from data: GlobalSearchSelectChatEventData?) -> [[AnyHashable: Any]] {
        
        guard let data = data else { return [] }
        
        let searchTextNameParameter:[AnyHashable : Any] = [
            AnalyticsConstants.requestParameterName:[
                data.searchText:actionValue
            ]
        ]
        
        let authStatusParameter:[AnyHashable : Any] = [
            AnalyticsConstants.authorizationStatusParameterName:[
                data.authorizationStatus:actionValue
            ]
        ]
        
        let chatNameParameter:[AnyHashable : Any] = [
            AnalyticsConstants.chatNameParameterName:[
                data.chatName:actionValue
            ]
        ]
        
        return [searchTextNameParameter,chatNameParameter, authStatusParameter]
    }
    
    fileprivate func getMultipleValueParametersForResult(from data: GlobalSearchSelectResultEventData?) -> [[AnyHashable: Any]] {
        
        guard let data = data else { return [] }
        
        var dataDictionary: [[AnyHashable: Any]] = []
        
        let titleNameParameter:[AnyHashable : Any] = [
            AnalyticsConstants.titleParameterName:[
                data.title:actionValue
            ]
        ]
        
        let urlParameter:[AnyHashable : Any] = [
            AnalyticsConstants.urlParameterName:[
                data.urlString:actionValue
            ]
        ]
        
        let typeParameter:[AnyHashable : Any] = [
            AnalyticsConstants.typeParameterName:[
                data.type:actionValue
            ]
        ]
        
        let authStatusParameter:[AnyHashable : Any] = [
            AnalyticsConstants.authorizationStatusParameterName:[
                data.authorizationStatus:actionValue
            ]
        ]
        
        dataDictionary.append(titleNameParameter)
        dataDictionary.append(urlParameter)
        dataDictionary.append(typeParameter)
        dataDictionary.append(authStatusParameter)
        
        if data.error != AnalyticsConstants.emptyValueName {
            let errorParameter:[AnyHashable : Any] = [
                AnalyticsConstants.errorParameterName:[
                    data.error:actionValue
                ]
            ]
            dataDictionary.append(errorParameter)
        }
        
        
        return dataDictionary
    }
}


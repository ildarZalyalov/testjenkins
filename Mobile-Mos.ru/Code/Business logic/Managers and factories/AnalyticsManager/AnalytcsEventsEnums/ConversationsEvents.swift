//
//  ConversationsEvents.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 03.07.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Данные, для шаринга из приложения
typealias SharingConversationEventData = (notificationsEnabled: Bool, authorizationStatus: String)

/// Данные, нужные для формирования выбора результата
typealias NewsSelectEventData = (title: String, urlString: String, type:String, authorizationStatus: String, error: String)

/// События чата здоровье
///
/// - screenLoaded: экран загрузился
/// - aboutChat: выбрана о чате
/// - shareApp: пошарили чат
enum MedecineConversationEvents {
    
    case screenLoaded(withString: String)
    case shareApp(withString: String)
    case aboutChat(withString: String)
    
    var functionName: String {
        switch self {
        case let .screenLoaded(functionStringName) :return functionStringName
        case let .shareApp(functionStringName)     :return functionStringName
        case let .aboutChat(functionStringName)    :return functionStringName
        }
    }
    
    var firstParameterName: String {
        
        switch self {
        case .shareApp: return AnalyticsConstants.shareAppParameterName
        default:
            return AnalyticsConstants.authorizationStatusParameterName
        }
    }
    
    var firstParameterValue: String {
        
        switch self {
        case .screenLoaded: return AnalyticsDefaultsParams.authorizationStatus.rawValue
        default:
            return AnalyticsConstants.emptyValueName
        }
    }
    
    var actionValue: String {
        
        switch self {
        case .screenLoaded   :return AnalyticsConstants.Actions.view.rawValue
        case .aboutChat      :return AnalyticsConstants.Actions.click.rawValue
        case .shareApp       :return AnalyticsConstants.Actions.click.rawValue
        }
    }
    
    func getMultipleValueAboutChat(from data: SharingConversationEventData) -> [[AnyHashable: Any]] {
        
        let notificationEnabledString = data.notificationsEnabled ? AnalyticsConstants.notificationEnabled : AnalyticsConstants.notificationDisabled
        
        let notificationStatusParameter:[AnyHashable : Any] = [
            AnalyticsConstants.notificationStatusParameterName:[
                notificationEnabledString:actionValue
            ]
        ]
        
        let authorizationStatus:[AnyHashable : Any] = [
            AnalyticsConstants.authorizationStatusParameterName:[
                data.authorizationStatus:actionValue
            ]
        ]
        
        return [notificationStatusParameter,authorizationStatus]
    }
}

/// События чата транспорт
///
/// - screenLoaded: экран загрузился
/// - aboutChat: выбрана о чате
/// - shareApp: пошарили чат
enum TransportConversationEvents {
    
    case screenLoaded(withString: String)
    case aboutChat(withString: String)
    case shareApp(withString: String)
    
    var functionName: String {
        switch self {
        case let .screenLoaded(functionStringName)    :return functionStringName
        case let .aboutChat(functionStringName)       :return functionStringName
        case let .shareApp(functionStringName)        :return functionStringName
        }
    }
    
    var firstParameterName: String {
        
        switch self {
        case .shareApp: return AnalyticsConstants.shareAppParameterName
        default:
            return AnalyticsConstants.authorizationStatusParameterName
        }
    }
    
    var firstParameterValue: String {
        
        switch self {
        default:
            return AnalyticsDefaultsParams.authorizationStatus.rawValue
        }
    }
    
    var actionValue: String {
        
        switch self {
        case .screenLoaded    :return AnalyticsConstants.Actions.view.rawValue
        case .aboutChat       :return AnalyticsConstants.Actions.click.rawValue
        case .shareApp        :return AnalyticsConstants.Actions.click.rawValue
        }
    }
    
    func getMultipleValueAboutChat(from data: SharingConversationEventData) -> [[AnyHashable: Any]] {
        
        let notificationEnabledString = data.notificationsEnabled ? AnalyticsConstants.notificationEnabled : AnalyticsConstants.notificationDisabled
        
        let notificationStatusParameter:[AnyHashable : Any] = [
            AnalyticsConstants.notificationStatusParameterName:[
                notificationEnabledString:actionValue
            ]
        ]
        
        let authorizationStatus:[AnyHashable : Any] = [
            AnalyticsConstants.authorizationStatusParameterName:[
                data.authorizationStatus:actionValue
            ]
        ]
        
        return [notificationStatusParameter,authorizationStatus]
    }
}


/// События чата дети в школе
///
///  - screenLoaded: экран загрузился
/// - foodLinkSelected: выбрана ссылка испп
/// - diaryLinkSelected: выбрана ссылка эжд
/// - aboutChat: выбрана о чате
/// - shareApp: пошарили чат
enum ChildsConversationEvents {
    
    case screenLoaded(withString: String)
    case foodLinkSelected(withString: String)
    case diaryLinkSelected(withString: String)
    case aboutChat(withString: String)
    case shareApp(withString: String)
    
    var functionName: String {
        switch self {
        case let .screenLoaded(functionStringName)     :return functionStringName
        case let .foodLinkSelected(functionStringName) :return functionStringName
        case let .diaryLinkSelected(functionStringName):return functionStringName
        case let .shareApp(functionStringName)            :return functionStringName
        case let .aboutChat(functionStringName)            :return functionStringName
        }
    }
    
    var firstParameterName: String {
        
        switch self {
        case .shareApp: return AnalyticsConstants.shareAppParameterName
        default:
            return AnalyticsConstants.emptyValueName
        }
    }
    
    var firstParameterValue: String {
        
        switch self {
        default:
            return AnalyticsConstants.emptyValueName
        }
    }
    
    var actionValue: String {
        
        switch self {
        case .screenLoaded      :return AnalyticsConstants.Actions.view.rawValue
        case .foodLinkSelected  :return AnalyticsConstants.Actions.click.rawValue
        case .diaryLinkSelected :return AnalyticsConstants.Actions.click.rawValue
        case .shareApp          :return AnalyticsConstants.Actions.click.rawValue
        case .aboutChat         :return AnalyticsConstants.Actions.click.rawValue
        }
    }
    
    func getMultipleValueAboutChat(from data: SharingConversationEventData) -> [[AnyHashable: Any]] {
        
        let notificationEnabledString = data.notificationsEnabled ? AnalyticsConstants.notificationEnabled : AnalyticsConstants.notificationDisabled
        
        let notificationStatusParameter:[AnyHashable : Any] = [
            AnalyticsConstants.notificationStatusParameterName:[
                notificationEnabledString:actionValue
            ]
        ]
        
        let authorizationStatus:[AnyHashable : Any] = [
            AnalyticsConstants.authorizationStatusParameterName:[
                data.authorizationStatus:actionValue
            ]
        ]
        
        return [notificationStatusParameter,authorizationStatus]
    }
}

/// События чата "мой дом"
///
/// - screenLoaded: экран загрузился
/// - aboutChat: выбран о чате
/// - shareApp: чат пошарен
enum MyHomeConversationEvents {
    
    case screenLoaded(withString: String)
    case aboutChat(withString: String)
    case shareApp(withString: String)
    
    var functionName: String {
        switch self {
        case let .screenLoaded(functionStringName)  :return functionStringName
        case let .shareApp(functionStringName)         :return functionStringName
        case let .aboutChat(functionStringName)     :return functionStringName
        }
    }
    
    var firstParameterName: String {
        
        switch self {
            
        case .shareApp: return AnalyticsConstants.shareAppParameterName
        default:
            return AnalyticsConstants.emptyValueName
        }
    }
    
    var firstParameterValue: String {
        
        switch self {
        default:
            return AnalyticsConstants.emptyValueName
        }
    }
    
    var actionValue: String {
        
        switch self {
        case .screenLoaded  :return AnalyticsConstants.Actions.view.rawValue
        case .shareApp      :return AnalyticsConstants.Actions.click.rawValue
        case .aboutChat     :return AnalyticsConstants.Actions.click.rawValue
        }
    }
    
    func getMultipleValueAboutChat(from data: SharingConversationEventData) -> [[AnyHashable: Any]] {
        
        let notificationEnabledString = data.notificationsEnabled ? AnalyticsConstants.notificationEnabled : AnalyticsConstants.notificationDisabled
        
        let notificationStatusParameter:[AnyHashable : Any] = [
            AnalyticsConstants.notificationStatusParameterName:[
                notificationEnabledString:actionValue
            ]
        ]
        
        let authorizationStatus:[AnyHashable : Any] = [
            AnalyticsConstants.authorizationStatusParameterName:[
                data.authorizationStatus:actionValue
            ]
        ]
        
        return [notificationStatusParameter,authorizationStatus]
    }
}

/// События чата консультации
///
/// - screenLoaded: экран загрузился
/// - aboutChat: выбран о чате
/// - shareApp: чат пошарен
enum ConsultationConversationEvents {
    
    case screenLoaded(withString: String)
    case aboutChat(withString: String)
    case shareApp(withString: String)
    
    var functionName: String {
        switch self {
        case let .screenLoaded(functionStringName)  :return functionStringName
        case let .shareApp(functionStringName)         :return functionStringName
        case let .aboutChat(functionStringName)     :return functionStringName
        }
    }
    
    var firstParameterName: String {
        
        switch self {
        case .shareApp: return AnalyticsConstants.shareAppParameterName
        default:
            return AnalyticsConstants.emptyValueName
        }
    }
    
    var firstParameterValue: String {
        
        switch self {
        default:
            return AnalyticsConstants.emptyValueName
        }
    }
    
    var actionValue: String {
        
        switch self {
        case .screenLoaded  :return AnalyticsConstants.Actions.view.rawValue
        case .shareApp         :return AnalyticsConstants.Actions.click.rawValue
        case .aboutChat     :return AnalyticsConstants.Actions.click.rawValue
        }
    }
    
    func getMultipleValueAboutChat(from data: SharingConversationEventData) -> [[AnyHashable: Any]] {
        
        let notificationEnabledString = data.notificationsEnabled ? AnalyticsConstants.notificationEnabled : AnalyticsConstants.notificationDisabled
        
        let notificationStatusParameter:[AnyHashable : Any] = [
            AnalyticsConstants.notificationStatusParameterName:[
                notificationEnabledString:actionValue
            ]
        ]
        
        let authorizationStatus:[AnyHashable : Any] = [
            AnalyticsConstants.authorizationStatusParameterName:[
                data.authorizationStatus:actionValue
            ]
        ]
        
        return [notificationStatusParameter,authorizationStatus]
    }
}

/// События чата новости
///
/// - screenLoaded: экран загрузился
/// - aboutChat: выбрана о чате
/// - shareApp: пошарили чат
enum NewsConversationEvents {
    
    case screenLoaded(withString: String)
    case aboutChat(withString: String)
    case shareApp(withString: String)
    
    var functionName: String {
        switch self {
        case let .screenLoaded(functionStringName) :return functionStringName
        case let .shareApp(functionStringName)        :return functionStringName
        case let .aboutChat(functionStringName)    :return functionStringName
        }
    }
    
    var firstParameterName: String {
        
        switch self {
        default:
            return AnalyticsConstants.authorizationStatusParameterName
        }
    }
    
    var firstParameterValue: String {
        
        switch self {
        case .screenLoaded: return AnalyticsDefaultsParams.authorizationStatus.rawValue
        default:
            return AnalyticsConstants.emptyValueName
        }
    }
    
    var actionValue: String {
        
        switch self {
        case .screenLoaded  :return AnalyticsConstants.Actions.view.rawValue
        case .shareApp         :return AnalyticsConstants.Actions.click.rawValue
        case .aboutChat     :return AnalyticsConstants.Actions.click.rawValue
        }
    }
    
    func getMultipleValueAboutChat(from data: SharingConversationEventData) -> [[AnyHashable: Any]] {
        
        let notificationEnabledString = data.notificationsEnabled ? AnalyticsConstants.notificationEnabled : AnalyticsConstants.notificationDisabled
        
        let notificationStatusParameter:[AnyHashable : Any] = [
            AnalyticsConstants.notificationStatusParameterName:[
                notificationEnabledString:actionValue
            ]
        ]
        
        let authorizationStatus:[AnyHashable : Any] = [
            AnalyticsConstants.authorizationStatusParameterName:[
                data.authorizationStatus:actionValue
            ]
        ]
        
        return [notificationStatusParameter,authorizationStatus]
    }
}

//
//  AuthorizationEventsEnums.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 26.06.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// События Авторизации - Вход
///
/// - screenLoaded: открытие экрана
/// - createProfile: нажал на создать профиль (регистрацию)
/// - logInPressed: действие авторизации
/// - gosUslugiPressed: нажал на ссылку гос.услуг
enum AuthorizationSignInEvents {
    case screenLoaded(withString: String)
    case createProfile(withString: String)
    case forgotPassword(withString: String)
    case logInPressed(withString: String)
    case gosUslugiPressed(withString: String)
    
    var functionName: String {
        
        switch self {
        case let .screenLoaded(functionStringName)     :return functionStringName
        case let .createProfile(functionStringName)    :return functionStringName
        case let .logInPressed(functionStringName)     :return functionStringName
        case let .gosUslugiPressed(functionStringName) :return functionStringName
        case let .forgotPassword(functionStringName) :return functionStringName
        }
    }
    
    var firstParameterName: String {
        
        switch self {
        case .logInPressed :return AnalyticsConstants.errorParameterName
        default:
            return AnalyticsConstants.emptyValueName
        }
    }
    
    var firstParameterValue: String {
        
        switch self {
        default:
            return AnalyticsConstants.emptyValueName
        }
    }
    
    var actionValue: String {
        
        switch self {
        case .screenLoaded     :return AnalyticsConstants.Actions.view.rawValue
        case .createProfile    :return AnalyticsConstants.Actions.click.rawValue
        case .logInPressed     :return AnalyticsConstants.Actions.login.rawValue
        case .gosUslugiPressed :return AnalyticsConstants.Actions.click.rawValue
        case .forgotPassword:return
            AnalyticsConstants.Actions.click.rawValue
        }
    }
}

/// События забыли пароль
///
/// - screenLoaded: экран загрузился
/// - continuePressed: нажали продолжить
enum AuthorizationForgotPasswordEvents {
    
    case screenLoaded(withString: String)
    case continuePressed(withString: String)
    
    var functionName: String {
        
        switch self {
        case let .screenLoaded(functionStringName)      :return functionStringName
        case let .continuePressed(functionStringName)   :return functionStringName
        }
    }
    
    var firstParameterName: String {
        
        switch self {
        case .continuePressed   :return AnalyticsConstants.errorParameterName
        default:
            return AnalyticsConstants.emptyValueName
        }
    }
    
    var firstParameterValue: String {
        
        switch self {
        default:
            return AnalyticsConstants.emptyValueName
        }
    }
    
    var actionValue: String {
        
        switch self {
        case .screenLoaded       :return AnalyticsConstants.Actions.view.rawValue
        case .continuePressed    :return AnalyticsConstants.Actions.click.rawValue
        }
    }
    
}

/// События создания нового пароля
///
/// - screenLoaded: экран загрузился
/// - donePressed: нажали готово
/// - repeatAgain: нажали повтор
enum AuthorizationNewPasswordEvents {
    
    case screenLoaded(withString: String)
    case donePressed(withString: String)
    case repeatAgain(withString: String)
    
    var functionName: String {
        
        switch self {
        case let .screenLoaded(functionStringName)  :return functionStringName
        case let .donePressed(functionStringName)   :return functionStringName
        case let .repeatAgain(functionStringName)   :return functionStringName
        }
    }
    
    var firstParameterName: String {
        
        switch self {
        case .donePressed :return AnalyticsConstants.errorParameterName
        default:
            return AnalyticsConstants.emptyValueName
        }
    }
    
    var firstParameterValue: String {
        
        switch self {
        default:
            return AnalyticsConstants.emptyValueName
        }
    }
    
    var actionValue: String {
        
        switch self {
        case .screenLoaded  :return AnalyticsConstants.Actions.view.rawValue
        case .donePressed   :return AnalyticsConstants.Actions.sent.rawValue
        case .repeatAgain   :return AnalyticsConstants.Actions.click.rawValue
        }
    }
}

/// События регистрации
///
/// - screenLoaded: экран загружен
/// - signInLinkPressed: нажал на авторизацию
/// - continuePressed: нажал продолжить
/// - agreementsPressed: нажал на соглашения пользователя
enum AuthorizationSignUpEvents {
    
    case screenLoaded(withString: String)
    case signInLinkPressed(withString: String)
    case continuePressed(withString: String)
    case agreementsPressed(withString: String)
    
    var functionName: String {
        
        switch self {
        case let .screenLoaded(functionStringName)      :return functionStringName
        case let .signInLinkPressed(functionStringName) :return functionStringName
        case let .continuePressed(functionStringName)   :return functionStringName
        case let .agreementsPressed(functionStringName) :return functionStringName
        }
    }
    
    var firstParameterName: String {
        
        switch self {
        case .continuePressed :return AnalyticsConstants.errorParameterName
        default:
            return AnalyticsConstants.emptyValueName
        }
    }
    
    var firstParameterValue: String {
        
        switch self {
        default:
            return AnalyticsConstants.emptyValueName
        }
    }
    
    var actionValue: String {
        
        switch self {
        default:
            return AnalyticsConstants.emptyValueName
        }
    }
}

/// События добавления кода из смс
///
/// - screenLoaded: экран загружен
/// - donePressed: нажал на готово
/// - sendSmsAgain: нажал на переотправку смс 
enum AuthorizationAddSmsCodeEvents {
    
    case screenLoaded(withString: String)
    case donePressed(withString: String)
    case sendSmsAgain(withString: String)
    
    var functionName: String {
        switch self {
        case let .screenLoaded(functionStringName)  :return functionStringName
        case let .donePressed(functionStringName)   :return functionStringName
        case let .sendSmsAgain(functionStringName)  :return functionStringName
        }
    }
    
    var firstParameterName: String {
        
        switch self {
        case .donePressed :return AnalyticsConstants.errorParameterName
        default:
            return AnalyticsConstants.emptyValueName
        }
    }
    
    var firstParameterValue: String {
        
        switch self {
        default:
            return AnalyticsConstants.emptyValueName
        }
    }
    
    var actionValue: String {
        
        switch self {
        case .screenLoaded :return AnalyticsConstants.Actions.view.rawValue
        case .donePressed  :return AnalyticsConstants.Actions.sent.rawValue
        case .sendSmsAgain :return AnalyticsConstants.Actions.click.rawValue
        }
    }
}

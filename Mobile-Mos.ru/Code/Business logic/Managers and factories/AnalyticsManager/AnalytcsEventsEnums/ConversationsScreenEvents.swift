//
//  ConversationsScreenEvents.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 29.06.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// События главного экрана
///
/// - screenLoaded: экран загрузился
/// - login: выбрана авторизация через нижнюю часть экрана
/// - selectedChat: выбрана экран чата
/// - authorizationOnBanner: выбрана авторизация через верхнюю часть экрана
/// - search: выбран поиск
/// - userAccount: выбран ЛК пользователя (если авторизован)
enum ConversationsScreenEvents {
    
    case screenLoaded(withString: String)
    case login(withString: String)
    case selectedChat(withString: String)
    case authorizationOnBanner(withString: String)
    case search(withString: String)
    case userAccount(withString: String)
    
    var functionName: String {
        switch self {
        case let .screenLoaded(functionStringName)         :return functionStringName
        case let .login(functionStringName)                :return functionStringName
        case let .selectedChat(functionStringName)         :return functionStringName
        case let .authorizationOnBanner(functionStringName):return functionStringName
        case let .search(functionStringName)               :return functionStringName
        case let .userAccount(functionStringName)          :return functionStringName
        }
    }
    
    var firstParameterName: String {
        
        switch self {
        default:
            return AnalyticsConstants.authorizationStatusParameterName
        }
    }
    
    var firstParameterValue: String {
        
        switch self {
        case .selectedChat: return AnalyticsConstants.authorizationStatusParameterName
        default:
            return AnalyticsDefaultsParams.authorizationStatus.rawValue
        }
    }
    
    var actionValue: String {
        
        switch self {
        case .screenLoaded          :return AnalyticsConstants.Actions.view.rawValue
        case .login                 :return AnalyticsConstants.Actions.click.rawValue
        case .selectedChat          :return AnalyticsConstants.Actions.click.rawValue
        case .authorizationOnBanner :return AnalyticsConstants.Actions.click.rawValue
        case .search                :return AnalyticsConstants.Actions.click.rawValue
        case .userAccount           :return AnalyticsConstants.Actions.click.rawValue
        }
    }
}

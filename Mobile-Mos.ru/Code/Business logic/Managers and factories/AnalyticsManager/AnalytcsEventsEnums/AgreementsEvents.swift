//
//  AgreementsEvents.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 28.06.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// События для соглашения
///
/// - screenLoaded: экран загрузился
/// - scrolledToBottom: доскроллил до конца экрана
enum AgreementsEvents {
    
    case screenLoaded(withString: String)
    case scrolledToBottom(withString: String)
    
    var functionName: String {
        switch self {
        case let .screenLoaded(functionStringName)     :return functionStringName
        case let .scrolledToBottom(functionStringName) :return functionStringName
        }
    }
    
    var firstParameterName: String {
        
        switch self {
        case .screenLoaded      :return AnalyticsConstants.authorizationStatusParameterName
        case .scrolledToBottom  :return AnalyticsConstants.authorizationStatusParameterName
        }
    }
    
    var actionValue: String {
        
        switch self {
        case .screenLoaded      :return AnalyticsConstants.Actions.view.rawValue
        case .scrolledToBottom  :return AnalyticsConstants.Actions.click.rawValue
        }
    }
}

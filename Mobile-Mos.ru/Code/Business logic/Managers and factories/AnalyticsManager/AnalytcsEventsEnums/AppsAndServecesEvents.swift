//
//  AppsAndServecesEvents.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 28.06.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// События для приложения и сервесы
///
/// - screenLoaded: экран загрузился
/// - selectedApp: выбрано приложение
enum AppsAndServecesEvents {
    
    case screenLoaded(withString: String)
    case selectedApp(withString: String)
    case selectedCTForm(withString: String)
    case sendCTForm(withString: String)
    
    var functionName: String {
        switch self {
        case let .screenLoaded(functionStringName) :return functionStringName
        case let .selectedApp(appName)             :return appName
        case let .selectedCTForm(appName)          :return appName
        case let .sendCTForm(appName)              :return appName
        }
    }
    
    var firstParameterName: String {
        switch self {
        case .selectedApp   : return AnalyticsConstants.appNameParameter
        case .selectedCTForm: return AnalyticsConstants.emptyValueName
        case .sendCTForm    : return AnalyticsConstants.errorParameterName
        default:
            return AnalyticsConstants.authorizationStatusParameterName
        }
    }
    
    var firstParameterValue: String {
        switch self {
        case .selectedCTForm: return AnalyticsConstants.emptyValueName
        case .sendCTForm    : return AnalyticsConstants.emptyValueName
        default:
            return AnalyticsDefaultsParams.authorizationStatus.rawValue
        }
    }

    var actionValue: String {

        switch self {
        case .screenLoaded  :return AnalyticsConstants.Actions.view.rawValue
        case .selectedApp   :return AnalyticsConstants.Actions.click.rawValue
        case .selectedCTForm:return AnalyticsConstants.Actions.click.rawValue
        case .sendCTForm    :return AnalyticsConstants.Actions.click.rawValue
        }
    }
}

//
//  AnalyticsManagerConfigurator.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 27.06.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

final class AnalyticsManagerBuilder {
    
    /// Получить сконфигурированный менеджер аналитики
    ///
    /// - Returns: менеджер аналитики
    static func getAnalyticsManager() -> AnalyticsManager {
        
        let analyticsManager = AnalyticsManagerImplementation()
        analyticsManager.currentUserManager = CurrentUserManagerImplementation()
        analyticsManager.userDefaults = UserDefaults.standard
        return analyticsManager
    }
}

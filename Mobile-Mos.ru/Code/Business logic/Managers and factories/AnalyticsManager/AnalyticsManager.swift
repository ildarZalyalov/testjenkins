//
//  AnalyticsManager.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 21.05.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

protocol AnalyticsManager {
    
    //MARK: - Геттер/сеттер к статическим переменным
    
    /// Название категории
    var categoryName: String { get }
    
    /// Название датасета
    var datasetName: String { get }
    
    /// Выбранная новость
    var selectedNews: NewsSearchModel? { get }
    
    /// Идентификатор чата
    var chatIdentifier: String? { get }
    
    /// Выбранный урл
    var selectedUrl: URL? { get }
    
    /// Текущая инфа для статистики
    var currentStatsData: StatsData? { get }
    
    /// Задать название категории
    ///
    /// - Parameter name: имя категории
    func setMapObjectCategoryName(with name: String)
    
    /// Задать название датасета
    ///
    /// - Parameter name: имя датасета
    func setMapObjectDatasetName(with name: String)
    
    /// Задать выбранную новость
    ///
    /// - Parameter item: новость
    func setSelectedNewsItem(with item: NewsSearchModel)
    
    /// Задать идентификатор чата
    ///
    /// - Parameter chatId: чат id
    func setChatIdetifier(with chatId: String)
    
    /// Задать урл
    ///
    /// - Parameter url: урл
    func setSelectedUrl(with url: URL)
    
    /// Задать инфу статистики
    ///
    /// - Parameter statsData: инфа статистики
    func setStatsData(with statsData: StatsData?)
    
    //MARK: - Основные методы
    
    /// Отправить событие сессии
    ///
    /// - Parameters:
    ///   - event: событие
    ///   - firstParameter: параметр события, если nil - подставляется автоматически по типу события
    ///   - value: значение параметра. Может использоваться rawValue из AnalyticsDefaultsParams
    func sendSessionEvent(with event: SessionEvents)
    
    /// Отправить событие авторизации
    ///
    /// - Parameters:
    ///   - event: событие
    ///   - parameter: параметр события, если nil - подставляется автоматически по типу события
    ///   - value: значение параметра, если nil - подставляется автоматически по типу события
    func sendAuthorizationSignInEvent(with event: AuthorizationSignInEvents, errorText: String?)
    
    /// Отправить событие восстановления пароля
    ///
    /// - Parameters:
    ///   - event: событие
    ///   - parameter: параметр события, если nil - подставляется автоматически по типу события
    ///   - value: значение параметра. Может использоваться rawValue из AnalyticsDefaultsParams
    func sendForgotPasswordEvent(with event: AuthorizationForgotPasswordEvents, errorText: String?)
    
    /// Отправить событие нового пароля
    ///
    /// - Parameters:
    ///   - event: событие
    ///   - parameter: параметр события, если nil - подставляется автоматически по типу события
    ///   - value: значение параметра. Может использоваться rawValue из AnalyticsDefaultsParams
    func sendNewPasswordEvent(with event: AuthorizationNewPasswordEvents, errorText: String?)
    
    /// Отправить событие регистрации
    ///
    /// - Parameters:
    ///   - event: событие
    ///   - parameter: параметр события, если nil - подставляется автоматически по типу события
    ///   - value: значение параметра. Может использоваться rawValue из AnalyticsDefaultsParams
    func sendSignUpEvent(with event: AuthorizationSignUpEvents, errorText: String?)
    
    /// Отправить событие ввода смс кода
    ///
    /// - Parameters:
    ///   - event: событие
    ///   - parameter: параметр события, если nil - подставляется автоматически по типу события
    ///   - value: значение параметра. Может использоваться rawValue из AnalyticsDefaultsParams
    func sendAddSmsCodeEvent(with event: AuthorizationAddSmsCodeEvents, errorText: String?)
    
    /// Отправить событие обратной связи
    ///
    /// - Parameters:
    ///   - event: событие
    ///   - categoryName: название выбранной категории, если нужно
    ///   - parameter: параметр события, если nil - подставляется автоматически по типу события
    ///   - value: значение параметра. Может использоваться rawValue из AnalyticsDefaultsParams
    func sendFeedbackEvent(with event: FeedbackEvents, categoryName: String?, errorText: String?)
    
    /// Отправить событие пользовательского соглашения
    ///
    /// - Parameter event: событие
    func sendAgreementsEvent(with event: AgreementsEvents)
    
    
    /// Отправить событие о разделе приложения и сервисы
    ///
    /// - Parameter event: событие
    ///   - appName: имя выбранного приложения
    ///   - error: ошибка
    func sendAppsAndServicesEvent(with event: AppsAndServecesEvents, appName: String?, error: String?)
    
    /// Отправить событие о разделе "О приложении"
    ///
    /// - Parameters:
    ///   - event: событие
    ///   - sharedToAppName: название приложения, куда был совершен шаринг, если нужно
    ///   - value: значение параметра. Может использоваться rawValue из AnalyticsDefaultsParams
    func sendAboutAppEvent(with event: AboutAppEvents, sharingData: SharingEventData?)
    
    func sendServiceEvent(with eventType: CategoryType, event: ServicesEvents, errorText: String?)
    
    /// Отправить событие о сервисе Уведомлений
    ///
    /// - Parameter event: событие
    func sendNotificationServiceEvent(with event: NotificationServiceEvents, data: NotificationEventData)
    
    /// Отправить событие о разделе Личные данные
    ///
    /// - Parameter event: событие
    func sendUserPersonalInfoEvent(with event: UserPersonalInfoEvents)
    
    /// Отправить событие о главном экране Личного Кабинета
    ///
    /// - Parameter event: событие
    func sendUserAccountEvent(with event: UserAccountEvents, selectedServiceName: String?, errorText: String?)
    
    
    /// Отправить событие о разделе Глобального поиска
    ///
    /// - Parameter event: событие
    ///   - requestData: данные для запроса
    ///   - selectedChatData: данные при выборе чата
    ///   - selectedSearchResultData: данные при выборе результата выдачи
    ///   - scrolledToLastNews: до скроллил ли до последней новости
    func sendGlobalSearchEvent(with event: GlobalSearchEvents, requestData: GlobalSearchEventData?, selectedChatData: GlobalSearchSelectChatEventData?, selectedSearchResultData: GlobalSearchSelectResultEventData?, scrolledToLastNews: Bool?)
    
    /// Отправить событие о главном(стартовом) Экране
    ///
    /// - Parameter
    ///    - event: событие
    ///    - chatName: имя чата
    func sendMainScreenEvent(with event: ConversationsScreenEvents, chatName: String?)
    
    /// Отправить событие здоровья
    ///
    /// - Parameters:
    ///   - event: событие
    ///   - aboutAppData: данные для о чате
    ///   - appSharingSource: данные для шаринга
    func sendMedecineEvent(with event: MedecineConversationEvents, aboutAppData: SharingConversationEventData?, appSharingSource:String?)
    
    /// Отправить событие детей из чата
    ///
    /// - Parameters:
    ///   - event: событие
    ///   - aboutAppData: данные для о чате
    ///   - appSharingSource: данные для шаринга
    func sendChildsConversationEvent(with event: ChildsConversationEvents, aboutAppData: SharingConversationEventData?, appSharingSource:String?)
    
    /// Отправить событие транспорта из чата
    ///
    /// - Parameters:
    ///   - event: событие
    ///   - aboutAppData: данные для о чате
    ///   - appSharingSource: данные для шаринга
    func sendTransportConversationEvent(with event: TransportConversationEvents, aboutAppData: SharingConversationEventData?, appSharingSource:String?)
    
    /// Отправить событие конслультации из чата
    ///
    /// - Parameters:
    ///   - event: событие
    ///   - aboutAppData: данные для о чате
    ///   - appSharingSource: данные для шаринга
    func sendConsultationConversationEvent(with event: ConsultationConversationEvents, aboutAppData: SharingConversationEventData?, appSharingSource:String?)
    
    /// Отправить событие новостей из чата
    ///
    /// - Parameters:
    ///   - event: событие
    ///   - aboutAppData: данные для о чате
    ///   - appSharingSource: данные для шаринга
    func sendNewsConversationEvent(with event: NewsConversationEvents, aboutAppData: SharingConversationEventData?, appSharingSource:String?)
    
    /// Отправить событие мой дом из чата
    ///
    /// - Parameters:
    ///   - event: событие
    ///   - aboutAppData: данные для о чате
    ///   - appSharingSource: данные для шаринга
    func sendMyHomeConversationEvent(with event: MyHomeConversationEvents, aboutAppData: SharingConversationEventData?, appSharingSource:String?)
    
    /// Отправить событие карты из чата
    ///
    /// - Parameters:
    ///   - event: событие
    ///   - aboutAppData: данные для о чате
    ///   - appSharingSource: данные для шаринга
    ///   - datasetData: данные датасета
    ///   - mapObjectSharingData: данные объекта карты для шаринга
    ///   - overlapData: данные перекрытия
    func sendMapConversationEvent(with event: MapSearchEvents, aboutAppData: SharingConversationEventData?, appSharingSource:String?, datasetData: DatasetEventData?, mapObjectSharingData: MapObjectInfoEventData?, overlapData: OverlapObjectInfoEventData?)
    
    
    /// Отправить событие нотификаций
    ///
    /// - Parameters:
    ///   - chatId: id чата для которого пришла пушка
    ///   - pushTypeId: уникальный идентификатор для статистики
    func sendPushNotificationEvent(by chatId: String, pushTypeId: Int64?, pushName: String?, pushDescription: String?)
    
    //MARK: - Отправка асбтрактных событий
    
    /// Отправка абстрактного события
    ///
    /// - Parameters:
    ///   - eventName: имя события
    ///   - eventJson: json строка события
    ///   - errorText: ошибка, если есть
    func sendSomeEvent(with eventName: String, eventJson: String, errorText: String?)
    
    /// Отправка абстрактного события шаринга
    ///
    /// - Parameters:
    ///   - eventName: имя события
    ///   - eventFunction: название функции события
    ///   - sharingSource: приложение, куда пошарили
    func sendSomeSharingEvent(with eventName: String, eventFunction: String, sharingSource: String)
}

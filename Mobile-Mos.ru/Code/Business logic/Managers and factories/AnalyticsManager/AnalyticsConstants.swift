//
//  AnalyticsConstants.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 28.06.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Дефолтные кейсы для параметров (то, что может использоваться во многих событиях и может отлавливаться внутри менеджера)
///
/// - authorizationStatus: статус авторизации
enum AnalyticsDefaultsParams: String {
    case authorizationStatus
}

/// Тип сессии, первая ли сессия(запуск приложения)
///
/// - firtSession: да, сессия первая
/// - notFirstSession: нет, сессия не первая
enum SessionType: String {
    case firtSession = "Да"
    case notFirstSession = "Нет"
}


/// Константы аналитики
struct AnalyticsConstants {
    
    //MARK: - Общие константы
    
    /// Перечисление с названиями статусов авторизации пользователя
    enum AuthorizationStatus: String {
        case authorized = "Авторизован"
        case nonAuthorized = "Не авторизован"
    }
    
    /// Перечисление действий во время события
    ///
    /// - view: показ экрана или чата
    /// - click: переходы и клики по ссылка кнопкам
    /// - login: авторизация пользователя
    /// - sent: отправка форм
    /// - logout: выход пользователя из учетки
    /// - start: запуск приложения, активация приложения
    /// - pause: остановка, пауза приложения
    enum Actions: String {
        case view
        case click
        case login
        case sent
        case logout
        case start
        case pause
    }
    
    /// Название пустого параметра
    static let emptyValueName: String = "Пусто"
    
    /// Название параметра ошибки
    static let errorParameterName: String = "Ошибка"
    
    /// Название параметра статуса авторизации
    static let authorizationStatusParameterName = "Статус авторизации"
    
    /// Название события загрузки экрана
    static let screenLoadedEventName = "Открытие экрана"
    
    /// Название события действия продолжения
    static let continueEventName = "Продолжить"
    
    /// Название события повторной отправки
    static let repeatAgainEventName = "Отправить повторно"
    
    /// Название события "Готово"
    static let doneEventName = "Готово"
    
    /// Название параметра источника шаринга
    static let shareAppParameterName = "Источник шаринга"
    
    /// Назване события о чате
    static let aboutChatEventName = "О чате"
    
    /// Назване ошибки, когда не удалось загрузить страницу
    static let notLoadedErrorValue = "Нет загрузки страницы"
    
    //MARK: - Константы Сессии
    
    /// Название события факта первой сессии
    static let firstSessionEventName = "Сессия пользователя"
    
    static let newUserParameterName = "Новый пользователь"
    
    /// Название события начала сессии
    static let beginSessionEventName = "Начало"
    
    /// Название события завершения сессии
    static let endSessionEventName = "Завершение"
    
    //MARK: - Константы Авторизации
    
    /// Название события ссылки на Госулуги Москвы
    static let gosuslugiLinkEventName = "Ссылка на МПГУ"
    
    /// Название события ссылки забыли пароль
    static let forgotPasswordEventName = "Забыли пароль"
    
    /// Название события регистрации
    static let createProfileEventName = "Создать профиль"
    
    /// Название события авторизации
    static let loginInEventName = "Вход"
    
    /// Название события ссылки на условия использования
    static let agreementsEventName = "Условия соглашения"
    
    /// Название события ссылки на вход с экрана регистрации
    static let linkToLogInEventName = "Ссылка войти"
    
    //MARK: - Константы Обратной связи
    
    /// Название функции отправленного сообщения
    static let feedbackSentEventName = "Сообщение отправлено"
    
    /// Название параметра категории обращения
    static let categoryParameterName = "Категория обращения"
    
    //MARK: - Константы Пользовательского соглашения
    
    static let scrollToBottomEventName = "Долистал до конца"
    
    //MARK: - Константы раздела Наши приложения и сервисы
    
    /// Название параметра выбора приложения из списка
    static let selectedAppNameEventName = "Выбор приложения"
    
    /// Название параметра выбора формы Чудо техники
    static let selectCTForm = "Переход на форму_ЧТ"
    
    /// Название параметра отправки формы Чудо техники
    static let sendCTForm = "Отправка заявки_ЧТ"
    
    //MARK: - Константы раздела Уведомлений
    
    /// Название параметра уведомления Консультации
    static let consultationNotification = "Уведомления Консультации"
    
    /// Название параметра уведомления детей
    static let childNotification = "Уведомления Дети в школе"
    
    /// Название параметра уведомления здоровья
    static let healthNotification = "Уведомления Здоровье"
    
    /// Название параметра уведомления новостей
    static let newsNotification = "Уведомления Новости города"
    
    /// Название параметра уведомления транспорта
    static let transportNotification = "Уведомления Транспорт"
    
    /// Название параметра уведомления моего дома
    static let myHomeNotification = "Уведомления Мой дом"
    
    /// Уведомления включены
    static let notificationEnabled = "Включены"
    
    /// Уведомления выключены
    static let notificationDisabled = "Выключены"
    
    //MARK: - Константы раздела Личного кабинета
    
    /// Название события шаринга приложения
    static let sharingToAppEventName = "Шеринг приложения"
    
    /// Название события "Еще"
    static let moreEventName = "Еще"
    
    /// Названеи события рекомендации другу
    static let recommentToFriendEventName = "Рекомендовать другу"
    
    /// Название события открытие в сторе
    static let openInAppStoreEventName = "Открыть в App Store"
    
    /// Название параметра имя приложения
    static let appNameParameter = "Название приложения"
    
    /// Название события выхода
    static let exitFromAppEventName = "Выход"
    
    /// Название события выбора сервиса
    static let selectedServiceNameEventName = "Переход в раздел"
    
    /// Название событие открытия ссылки на проверку омс
    static let checkMedicalInsuranceLinkEventName = "ОМС_проверка"
    
    /// Название события открытия ссылки на инструкцию ЭЖД
    static let childESHDLinkEventName = "ЭЖД_инструкция на mos"
    
    /// Название события открытия ссылки на инструкцию ИСПП
    static let childIsppLinkEventName = "ИСПП_инструкция на mos"
    
    //MARK: - Константы Глобального поиска
    
    /// Название события запроса
    static let requestEventName = "Запрос"
    
    /// Название события выбора запроса из выдачи(топа)
    static let selectFromTopEventName = "Выбор из топ"
    
    /// Название события перехода в чат из поиска
    static let selectChatEventName = "Переход в чат"
    
    /// Название события перехода на материал из поиска
    static let selectSearchResultEventName = "Переход на материал"
    
    /// Название события докручивания до последней новости
    static let scrolledToLastNewsEventName = "Прокрутка списка_конец"
    
    /// Название параметра докручивания до последней новости
    static let scrolledToLastNewsParameterName = "Прокрутил до конца"
    
    /// Название параметра поискового запроса
    static let requestParameterName = "Текст запроса"
    
    /// Название параметра имени чата
    static let chatNameParameterName = "Название чата"
    
    /// Название параметра заголовка
    static let titleParameterName = "Заголовок"
    
    /// Название параметра ссылки на материал
    static let urlParameterName = "URL материала"
    
    /// Название параметра типа материала
    static let typeParameterName = "Тип материала"
    
    //MARK: - Константы Главного экана
    
    /// Название события открытия входа через вернюю иконку на главной странице
    static let authorizationIconEventName = "Вход_icon"
    
    /// Название события открытия входа через нижнюю кнопку на главной странице
    static let authorizationButtonEventName = "Вход_button"
    
    /// Название события поиска через главную страницу
    static let searchEventName = "Поиск"
    
    /// Название события личного кабинета (если пользователь авторизован) на главной странице
    static let userAccountEventName = "ЛК"
    
    
    //MARK: - Константы Поиска по карте
    
    /// Название события выбора датасета
    static let selectDatasetEventName = "Выбор датасета"
    
    /// Название события поиска датасета
    static let searchDatasetEventName = "Поиск датасета"
    
    /// Название события просмотра карточки объекта
    static let viewMapObjectEventName = "Просмотр карточки объекта"
    
    /// Название события просмотра карточки перекрытия датасета
    static let viewOverlapObjectEventName = "Просмотр карточки перекрытия"
    
    /// Название события добавления карточи в избранное
    static let addMapObjectToFavoriteEventName = "Добавление карточки в избранное"
    
    /// Название события шаринга карточки объекта
    static let sharingMapObjectInfoEventName = "Шеринг карточки объекта"
    
    /// Название параметра имени датасета
    static let datasetNameParameterName = "Название датасета"
    
    /// Название параметра адреса перекрытия
    static let overlapAddressParameterName = "Адрес перекрытия"
    
    /// Название параметра округа перекрытия
    static let overlapOkgurParameterName = "Округ перекрытия"
    
    //MARK: - Константы чатов
    
    /// Название параметра статуса уведомлений
    static let notificationStatusParameterName = "Статус уведомлений"
    
    /// Название события перехода на инструкцию ЭЖД
    static let childDiaryViewingEventName = "Переход на инструкцию ЭЖД"
    
    /// Название события перехода на заявление ИСПП
    static let foodInstructionViewingEventName = "Просмотр заявления ИСПП"
    
    //MARK: - Push Notifications
    
    /// Пуш нотификация открыта
    static let notificationOpenedEventName = "Открыто"
}


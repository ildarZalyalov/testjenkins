//
//  RateAppNotificationManager.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 04.04.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation
import StoreKit

final class RateAppNotificationManager {
    
    var userDefaults: UserDefaults!
    var notificationCenter: NotificationCenter! {
        didSet{
            notificationCenter.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
            notificationCenter.addObserver(self, selector: #selector(keyboardWillHide), name: .UIKeyboardWillHide, object: nil)
        }
    }
    
    var timer: Timer = Timer()
    
    var showAlertDelay: Int = 120
    var delayForKeyboardAppear = 30
    
    deinit {
        notificationCenter.removeObserver(self)
    }
    
    fileprivate var onboardingWasCompleted: Bool {
        let key = ApplicationUserDefaultsKey.onboardingWasCompleted.rawValue
        return userDefaults.bool(forKey: key)
    }
    
    func setupAppReviewRequest() {
        
        guard onboardingWasCompleted else { return }
        
        setupTimer(with: showAlertDelay)
    }
    
    
    //MARK: Helpers
    
    @objc
    fileprivate func keyboardWillShow() {
        showAlertDelay += delayForKeyboardAppear
    }
    
    @objc
    fileprivate func keyboardWillHide() {
        showAlertDelay -= delayForKeyboardAppear
    }
    
    fileprivate func setupTimer(with timeCount: Int) {
        
        showAlertDelay = timeCount
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: true)
    }
    
    @objc
    fileprivate func updateTimer() {
        
        if showAlertDelay < 0 {
            timer.invalidate()
            
            guard #available(iOS 10.3, *) else { return }
            SKStoreReviewController.requestReview()
        }
        else {
            showAlertDelay -= 1
        }
    }
}

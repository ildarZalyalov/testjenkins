//
//  RateAppNotificationManagerBuilder.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 12.04.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Конфигурация зависимостей для RateAppNotificationManager
extension RateAppNotificationManager {
    
    static func getRateAppNotificationManager() -> RateAppNotificationManager {
        
        let rateManager = RateAppNotificationManager()
        let userDefaults = UserDefaults.standard
        let notificationCenter = NotificationCenter.default
        
        rateManager.userDefaults = userDefaults
        rateManager.notificationCenter = notificationCenter
        
        return rateManager
    }
}

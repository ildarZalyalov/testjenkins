//
//  ChatMessageTypingManagerImplementation.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 26.04.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

class ChatMessageTypingManagerImplementation: ChatMessageTypingManager {
    
    //MARK: - Статические компоненты
    
    /// Статус "вам печатают" по идентификаторам чатов
    fileprivate static var typingStatusByChatId = [String : Bool]()
    
    /// Таймеры завершения события "вам печатают" по идентификаторам чатов
    fileprivate static var hidePerformersByChatId = [String : DelayedWorkPerformer]()
    
    /// Подписавшиеся обработчики событий
    fileprivate static var typingHandlers = ThreadSafeDictionary<UUID, ChatMessageTypingChangedHandler>()
    fileprivate static var typingStoppedHandlers = ThreadSafeDictionary<UUID, ChatMessageTypingChangedHandler>()
    
    /// Задержка завершения события "вам печатают" по умолчанию
    static let hideTypingDefaultDelay: TimeInterval = 10
    
    //MARK: - Локальные компоненты
    
    fileprivate typealias Manager = ChatMessageTypingManagerImplementation
    fileprivate let typingHandlerId = UUID()
    fileprivate let typingStoppedHandlerId = UUID()
    
    var messageService: ChatMessageService! {
        
        didSet {
            
            guard messageService != nil else {
                return
            }
            
            messageService.sentMessageHandler = { message in
                guard message.status == .success else { return }
                guard !message.replyMarkup.hideTypingIndicator else { return }
                Manager.beganTypingInConversation(with: message.chatId,
                                                  eventSource: .messageSent,
                                                  sender: self)
            }
            
            messageService.newMessageHandler = { message in
                Manager.stoppedTypingInConversation(with: message.chatId,
                                                    eventSource: .messageObtained,
                                                    sender: self)
            }
        }
    }
    
    var interactionService: ChatMessageInteractionService! {
        
        didSet {
            
            guard interactionService != nil else {
                return
            }
            
            interactionService.typingHandler = { event in
                Manager.beganTypingInConversation(with: event.chatId,
                                                  eventSource: .server,
                                                  sender: self,
                                                  timeout: event.timeout)
            }
            
            interactionService.typingStoppedHandler = { event in
                Manager.stoppedTypingInConversation(with: event.chatId,
                                                    eventSource: .server,
                                                    sender: self)
            }
        }
    }
    
    deinit {
        Manager.typingHandlers[typingHandlerId] = nil
        Manager.typingStoppedHandlers[typingStoppedHandlerId] = nil
    }
    
    //MARK: - Приватные методы
    
    /// Запустить обработку получения события "вам печатают"
    ///
    /// - Parameters:
    ///   - conversationId: идентификатор чата
    ///   - eventSource: источник события
    ///   - sender: сущность, отправившая событие
    ///   - timeout: задержка завершения события (если есть)
    fileprivate static func beganTypingInConversation(with conversationId: String, eventSource: ChatMessageTypingEventSource, sender: AnyObject?, timeout: Int? = nil) {
        
        let delay: TimeInterval = timeout != nil ? TimeInterval(timeout!) : hideTypingDefaultDelay
        
        let isTyping = typingStatusByChatId[conversationId] ?? false
        guard !isTyping else {
            
            hidePerformersByChatId[conversationId]?.cancelDelayedPerform()
            hidePerformersByChatId[conversationId]?.performOnMain(afterDelay: delay)
            
            return
        }
        
        let hideTypingPerformer = DelayedWorkPerformer {
            stoppedTypingInConversation(with: conversationId, eventSource: .timedOut, sender: nil)
        }
        hideTypingPerformer.performOnMain(afterDelay: delay)
        
        typingStatusByChatId[conversationId] = true
        hidePerformersByChatId[conversationId] = hideTypingPerformer
        
        let info = ChatMessageTypingEventInfo(conversationId: conversationId,
                                              source: eventSource,
                                              sender: sender)
        
        let handlers = typingHandlers.currentValues()
        DispatchQueue.main.async(execute: {
            handlers.forEach { $0(info) }
        })
    }
    
    /// Запустить обработку завершения события "вам печатают"
    ///
    /// - Parameters:
    ///   - conversationId: идентификатор чата
    ///   - eventSource: источник события
    ///   - sender: сущность, отправившая событие
    fileprivate static func stoppedTypingInConversation(with conversationId: String, eventSource: ChatMessageTypingEventSource, sender: AnyObject?) {
        
        let isTyping = typingStatusByChatId[conversationId] ?? false
        guard isTyping else { return }
        
        typingStatusByChatId[conversationId] = false
        
        hidePerformersByChatId[conversationId]?.cancelDelayedPerform()
        hidePerformersByChatId[conversationId] = nil
        
        let info = ChatMessageTypingEventInfo(conversationId: conversationId,
                                              source: eventSource,
                                              sender: sender)
        
        let handlers = typingStoppedHandlers.currentValues()
        DispatchQueue.main.async(execute: {
            handlers.forEach { $0(info) }
        })
    }
    
    //MARK: - ChatMessageTypingManager
    
    var typingHandler: ChatMessageTypingChangedHandler? {
        get { return Manager.typingHandlers[typingHandlerId] }
        set { Manager.typingHandlers[typingHandlerId] = newValue }
    }
    
    var typingStoppedHandler: ChatMessageTypingChangedHandler? {
        get { return Manager.typingStoppedHandlers[typingStoppedHandlerId] }
        set { Manager.typingStoppedHandlers[typingStoppedHandlerId] = newValue }
    }
    
    func isTypingInConversation(with conversationId: String) -> Bool {
        return Manager.typingStatusByChatId[conversationId] ?? false
    }

    func setIsTypingInConversation(with conversationId: String, sender: AnyObject?) {
        Manager.beganTypingInConversation(with: conversationId, eventSource: .local, sender: sender)
    }
    
    func setStoppedTypingInConversation(with conversationId: String, sender: AnyObject?) {
        Manager.stoppedTypingInConversation(with: conversationId, eventSource: .local, sender: sender)
    }
}

//
//  ChatMessageTypingManager.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 26.04.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Источник события "вам печатают" или его завершения
///
/// - local: изменение состояния запросило приложение
/// - server: изменение состояния запросил сервер
/// - messageSent: событие "вам печатают" было получено в связи с отправкой сообщения
/// - messageObtained: событие "вам печатают" завершилось в связи с получением сообщения
/// - timedOut: событие "вам печатают" завершилось по таймауту
enum ChatMessageTypingEventSource {
    case local
    case server
    case messageSent
    case messageObtained
    case timedOut
}

/// Параметры события "вам печатают"
struct ChatMessageTypingEventInfo {
    
    /// Идентификатор чата
    let conversationId: String
    
    /// Данные об источнике
    let source: ChatMessageTypingEventSource
    
    /// Сущность, отправившая событие
    /// (чтобы клиенты менеджера могли отбрасывать события, отправленные ими самими)
    let sender: AnyObject?
}

/// Блок обработки получения уведомления о том, что в чате "печатают"/"перестали печатать", единственный аргумент - параметры события
typealias ChatMessageTypingChangedHandler = (ChatMessageTypingEventInfo) -> ()

/// Менеджер обработки события "вам печатают"
protocol ChatMessageTypingManager {
    
    /// Блок обработки получения уведомления о том, что в чате "печатают"
    var typingHandler: ChatMessageTypingChangedHandler? { get set }
    
    /// Блок обработки получения уведомления о том, что в чате "перестали печатать"
    var typingStoppedHandler: ChatMessageTypingChangedHandler? { get set }
    
    /// "Печатают" ли в конкретном чате
    ///
    /// - Parameter conversationId: идентификатор чата
    /// - Returns: "печатают" ли в чате
    func isTypingInConversation(with conversationId: String) -> Bool
    
    /// Отправить событие "вам печатают" в конкретный чат
    ///
    /// - Parameters:
    ///   - conversationId: идентификатор чата
    ///   - sender: сущность, отправившая событие
    func setIsTypingInConversation(with conversationId: String, sender: AnyObject?)
    
    /// Зарегистрировать завершение события "вам печатают" в конкретном чате
    ///
    /// - Parameters:
    ///   - conversationId: идентификатор чата
    ///   - sender: сущность, отправившая событие
    func setStoppedTypingInConversation(with conversationId: String, sender: AnyObject?)
}

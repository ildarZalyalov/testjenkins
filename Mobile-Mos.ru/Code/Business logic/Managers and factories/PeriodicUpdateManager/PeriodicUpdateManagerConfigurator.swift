//
//  PeriodicUpdateManagerConfigurator.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 16.02.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Конфигурация зависимостей для PeriodicUpdateManager
extension PeriodicUpdateManager {
    
    static func getPeriodicUpdateManager() -> PeriodicUpdateManager {
        
        let periodicUpdateManager = PeriodicUpdateManager()
        let serviceBuilder = UIApplication.shared.serviceBuilder
        
        periodicUpdateManager.chatKeywordsService = serviceBuilder.getChatKeywordsService()
        periodicUpdateManager.userInfoService = serviceBuilder.getUserInfoAndSettingsService()
        
        periodicUpdateManager.connectionObserver = serviceBuilder.getSocketConnectionObserver()
        periodicUpdateManager.userDefaults = UserDefaults.standard
    
        return periodicUpdateManager
    }
}

//
//  PeriodicUpdateManager.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 16.02.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

final class PeriodicUpdateManager: SocketConnectionObserverDelegate {
    
    /// Ключ для сохранения последней даты кэширования кейвордов
    fileprivate let chatKeywordsCacheDateKey = "lastChatKeywordsRequestDate"
    
    fileprivate let oneDayTimeInterval: TimeInterval = -60 * 60 * 24
    fileprivate let halfDayTimeInterval: TimeInterval = -60 * 60 * 12
    
    var chatKeywordsService: ChatKeywordsService!
    var userInfoService: UserInfoAndSettingsService!
    
    var userDefaults: UserDefaults!
    var connectionObserver: SocketConnectionObserver! {
        didSet {
            guard connectionObserver != nil else { return }
            connectionObserver.delegate = self
        }
    }
    
    //MARK: - SocketConnectionObserverDelegate
    
    func socketObserver(observer: SocketConnectionObserver, observedConnectionChangeTo connected: Bool) {
        guard connected else { return }
        updateDataIfNeeded()
    }
    
    //MARK: - Открытые методы
    
    /// Обновить данные, если нужно
    func updateDataIfNeeded() {
        
        updateKeywordsCacheIfNeeded()
        updateUserInfoCacheIfNeeded()
        updateUserNotificationSettingsIfNeeded()
        updateForgotSmsCodeTimesIfNeeded()
    }
    
    //MARK: - Обновление ключевых слов для поиска чатов
    
    fileprivate func updateKeywordsCacheIfNeeded() {
        
        guard let date = userDefaults.object(forKey: chatKeywordsCacheDateKey) as? Date else {
            updateKeywordsCache()
            return
        }
        
        guard date.timeIntervalSinceNow < oneDayTimeInterval else { return }
        updateKeywordsCache()
    }
    
    fileprivate func updateKeywordsCacheDate() {
        userDefaults.set(Date(), forKey: chatKeywordsCacheDateKey)
        userDefaults.synchronize()
    }
    
    fileprivate func updateKeywordsCache() {
        chatKeywordsService.obtainKeyWords(completion: { [weak self] result in
            switch result {
            case .success:
                self?.updateKeywordsCacheDate()
            default:
                return
            }
        })
    }
    
    //MARK: - Обновление личных данных пользователя
    
    fileprivate func updateUserInfoCacheIfNeeded() {
        
        guard let date = userInfoService.lastUserInfoObtainDate else {
            updateUserInfoCache()
            return
        }
        
        guard date.timeIntervalSinceNow < halfDayTimeInterval else { return }
        updateUserInfoCache()
    }
    
    fileprivate func updateUserInfoCache() {
        userInfoService.obtainCurrentUserInfo()
    }
    
    //MARK: - Обновление настроек уведомлений пользователя
    
    fileprivate func updateUserNotificationSettingsIfNeeded() {
        guard userInfoService.currentUser.isLoggedIn else { return }
        userInfoService.refreshNotificationSettings(completion: nil)
    }
    
    //MARK: - Сброс значений в UserDefaults
    
    fileprivate func updateForgotSmsCodeTimesIfNeeded() {
        
        guard let date = userDefaults.object(forKey: ApplicationUserDefaultsKey.forgotPasswordCacheDate.rawValue) as? Date else {
            return
        }
        
        guard date.timeIntervalSinceNow < oneDayTimeInterval else { return }
        userDefaults.set(0, forKey: ApplicationUserDefaultsKey.forgotPasswordCodeRequestTimes.rawValue)
        userDefaults.synchronize()
    }
}

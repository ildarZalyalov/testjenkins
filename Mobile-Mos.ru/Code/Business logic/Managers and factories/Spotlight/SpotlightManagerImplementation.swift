//
//  SpotlightManager.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 26.12.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import CoreSpotlight
import MobileCoreServices

/// Менеджер, который умеет настраивать Spotlight
final class SpotlightManager {
    
    var conversationsService: ConversationsService!
    var currentUserManager: CurrentUserManager!
    var spotlightItemsStrings: SpotlightItemsStrings!
    
    fileprivate let conversationsSpotlightImage = #imageLiteral(resourceName: "appIcon")
    fileprivate let conversationsDomainIdentifier = "conversations"
    fileprivate let fakeConversationIdetifier = "fake_conversation"
    
    func configureSpotlightForConversations() {
        
        conversationsService.obtainCachedConversations(for: currentUserManager.currentUser) { [weak self] (result) in
            
            guard let strongSelf = self else { return }
            
            switch result {
            case .success(let conversations):

                var searchableItems = [CSSearchableItem]()

                for conversation in conversations {

                    let item = strongSelf.getSearchableItem(with: conversation.itemId, title: conversation.title, description: conversation.descriptionText)
                    searchableItems.append(item)
                }
                
                var keyWordsOfNonExistsConversations = [String]()
                
                for conversationId in ConversationIdentificator.allCasesRawValues {
                    
                    guard searchableItems.filter({ $0.uniqueIdentifier == conversationId }).isEmpty else { continue }
                    
                    let keyWords = strongSelf.spotlightItemsStrings.getKeywords(for: conversationId)
                    keyWordsOfNonExistsConversations.append(contentsOf: keyWords)
                }
                
                if !keyWordsOfNonExistsConversations.isEmpty {
                    let item = strongSelf.getFakeConversationSearchableItem(with: keyWordsOfNonExistsConversations)
                    searchableItems.append(item)
                }

                strongSelf.addSearchableItemsToIndex(items: searchableItems)
                
            case .failure(let error):
                print("Ошибка получения кэша чатов: \(error.localizedDescription)")
            }
        }
    }
    
    /// Получить фейковый чат для поиска со всеми поисковыми словами не доступных на данный момент чатов
    ///
    /// - Parameter keywords: поисковые слова
    fileprivate func getFakeConversationSearchableItem(with keywords: [String]) -> CSSearchableItem {
        
        let searchableItemAttributeSet = getSearchableItemAttributeSet(with: nil, description: nil, names: keywords)
        let searchableItem = CSSearchableItem(uniqueIdentifier: fakeConversationIdetifier, domainIdentifier: conversationsDomainIdentifier, attributeSet: searchableItemAttributeSet)
        searchableItem.expirationDate = Date.distantFuture
        return searchableItem
    }
    
    /// Получить объект спотлайта
    ///
    /// - Parameters:
    ///   - conversationId: id чата
    ///   - title: заголовок чата
    ///   - description: описание чата
    /// - Returns: объект спотлайта
    fileprivate func getSearchableItem(with conversationId: String,
                                       title: String? = nil,
                                       description: String? = nil) -> CSSearchableItem {
        
        let searchNames: [String] = spotlightItemsStrings.getKeywords(for: conversationId)
        let searchableItemAttributeSet = getSearchableItemAttributeSet(with: title, description: description, names: searchNames)
        let searchableItem = CSSearchableItem(uniqueIdentifier: "\(conversationId)", domainIdentifier: conversationsDomainIdentifier, attributeSet: searchableItemAttributeSet)
        searchableItem.expirationDate = Date.distantFuture
        return searchableItem
    }
    
    /// Получить объект для индексации в Spotlight
    ///
    /// - Parameters:
    ///   - title: заголовок объекта
    ///   - image: картинка объекта
    ///   - description: описание объекта
    ///   - names: дополнительные имена объекта для поиска
    /// - Returns: объекта для индексации в Spotlight
    fileprivate func getSearchableItemAttributeSet(with title: String? = nil,
                                                   image: UIImage = #imageLiteral(resourceName: "appIcon"),
                                                   description: String? = nil,
                                                   names: [String]) -> CSSearchableItemAttributeSet {
        
        let searchableItemAttributeSet = CSSearchableItemAttributeSet(itemContentType: kUTTypeText as String)
        
        searchableItemAttributeSet.title = title ?? Bundle.main.appDisplayName
        searchableItemAttributeSet.thumbnailData = UIImagePNGRepresentation(image)
        searchableItemAttributeSet.contentDescription = description ?? StringsHelper.appDescription
        searchableItemAttributeSet.keywords = names
        
        return searchableItemAttributeSet
    }
    
    /// Добавить объекты в индекс Spotlight
    ///
    /// - Parameter items: объекты
    fileprivate func addSearchableItemsToIndex(items: [CSSearchableItem]) {
        
        CSSearchableIndex.default().deleteAllSearchableItems { (error) in
            
            guard error == nil else {
                
                print("Ошибка удаления индексов из Spotlight-а: \(String(describing: error?.localizedDescription))")
                return
            }
            
            CSSearchableIndex.default().indexSearchableItems(items) { (error) in
                
                if error != nil {
                    print("Ошибка индексирования Spotlight: \(String(describing: error?.localizedDescription))")
                }
            }
        }
    }
    
}

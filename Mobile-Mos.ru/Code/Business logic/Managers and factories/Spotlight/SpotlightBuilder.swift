//
//  SpotlightBuilder.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 27.12.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class SpotlightBuilder {
    
    static func getSpotlightManager() -> SpotlightManager {
        
        let spotlightManager = SpotlightManager()
        let currentUserManager = CurrentUserManagerImplementation()
        
        spotlightManager.spotlightItemsStrings = SpotlightItemsStrings.getSpotlightItemsStrings()
        spotlightManager.conversationsService = UIApplication.shared.serviceBuilder.getConversationsService()
        spotlightManager.currentUserManager = currentUserManager
        
        return spotlightManager
    }
    
    static func getSpotlightHandler() -> SpotlightHandler {
        
        let spotlightHandler = SpotlightHandler()
        spotlightHandler.applicationRouter = ApplicationRouterBuilder.getApplicationRouter()
        spotlightHandler.conversationsService = UIApplication.shared.serviceBuilder.getConversationsService()
        
        return spotlightHandler
    }
}

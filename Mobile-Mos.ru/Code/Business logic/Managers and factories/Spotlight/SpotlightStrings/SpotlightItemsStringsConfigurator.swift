//
//  SpotlightItemsStringsConfigurator.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 18.02.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

extension SpotlightItemsStrings {
    
    static func getSpotlightItemsStrings() -> SpotlightItemsStrings {
        
        let spotlightItemsStrings = SpotlightItemsStrings()
        spotlightItemsStrings.chatKeywordsService = UIApplication.shared.serviceBuilder.getChatKeywordsService()
        
        return spotlightItemsStrings
    }
}

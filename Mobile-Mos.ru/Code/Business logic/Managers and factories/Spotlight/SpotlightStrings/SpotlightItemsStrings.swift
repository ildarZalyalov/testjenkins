//
//  SpotlightItemsStrings.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 28.12.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// позволяет доставать альтернативные имена для чатов
class SpotlightItemsStrings {
    
   var chatKeywordsService: ChatKeywordsService!
    
    func getKeywords(for conversationId: String) -> [String] {
        
        guard let conversationType = ConversationIdentificator(rawValue: conversationId) else { return [] }
        
        switch conversationType {
        case .cityNews      :return getArrayOfNames(with: .cityNews)
        case .children      :return getArrayOfNames(with: .children)
        case .transport     :return getArrayOfNames(with: .transport)
        case .myHome        :return getArrayOfNames(with: .myHome)
        case .health        :return getArrayOfNames(with: .health)
        case .qa            :return getArrayOfNames(with: .qa)
        case .testChat      :return getArrayOfNames(with: .testChat)
        case .activeCitizen :return getArrayOfNames(with: .activeCitizen)
        case .ourTown       :return getArrayOfNames(with: .ourTown)
        }
    }
    
    fileprivate func getArrayOfNames(with type: ConversationIdentificator) -> [String] {
        return chatKeywordsService.getChatKeywords(for: type).components(separatedBy: CharacterSet.newlines)
    }
}

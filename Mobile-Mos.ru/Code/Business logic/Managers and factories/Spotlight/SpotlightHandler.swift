//
//  SpotlightHandler.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 27.12.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import CoreSpotlight

/// Хендлер, который умеет ловить нажатие на объект Spotlight-а
final class SpotlightHandler {
    
   var conversationsService: ConversationsService!
   var applicationRouter: ApplicationRouter!
    
    /// Отловить нажатие на найденный чат через Spotlight
    ///
    /// - Parameter userActivity: объект, найденный в Spotlight
    func handleTapForConversationItem(with userActivity: NSUserActivity) {
        
        guard userActivity.activityType == CSSearchableItemActionType else { return }
        
        guard let identifier = userActivity.userInfo?[CSSearchableItemActivityIdentifier] as? String else { return }
        
        guard !conversationsService.activeConversationsIds.contains(identifier) else { return }
        
        if identifier == mapConversation.itemId {
            
            applicationRouter.showChatModule(for: mapConversation)
        }
        else {
            
            getConversation(with: identifier) { [weak self] (conversation) in
                
                guard let conversation = conversation else { return }
                
                self?.applicationRouter.showChatModule(for: conversation)
            }
        }
    }
    
    fileprivate func getConversation(with itemId: String, completion:@escaping (Conversation?) -> () ) {
        
        conversationsService.obtainCachedConversation(with: itemId) { (result) in
            
            switch result {
                
            case .success(let conversation):
                
                completion(conversation)
                
            case .failure(let error):
                completion(nil)
                print("Ошибка получения чата: \(error)")
            }
        }
    }
}

//
//  3DTouchManager.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 15.12.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Менеджер по работе с 3д тачем
class ThreeDTouchManager {
    
    var conversationsService: ConversationsService!
    var shortcutModels:[ApplicationShortcutItemModel]!
    var currentUserManager: CurrentUserManager = CurrentUserManagerImplementation()
    
    func updateShortcutItems() {
        
        conversationsService.obtainCachedConversations(for: currentUserManager.currentUser) {[weak self] (result) in
            
            switch result {
            case .success(let conversations):
                
                if !conversations.isEmpty {
                    self?.generateShorcuts(for: conversations)
                }
            case .failure(let error):
                print("Ошибка получения чатов: \(error)")
            }
        }
    }
    
    fileprivate func generateShorcuts(for conversations: [Conversation]) {
        
        UIApplication.shared.shortcutItems?.removeAll()
        
        for conversation in conversations {
            
            for model in shortcutModels {
                
                if conversation.itemId == model.itemId {
                    
                    DispatchQueue.main.async(execute: {
                        
                        UIApplication.shared.shortcutItems?.append(UIApplicationShortcutItem(
                            type: model.typeName,
                            localizedTitle: model.name,
                            localizedSubtitle: nil,
                            icon: model.iconName,
                            userInfo: nil))
                    })
                }
            }
        }
    }
}

//
//  3DTouchBuilder.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 15.12.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class ThreeDTouchBuilder {
    
    fileprivate static var shortcutModels:[ApplicationShortcutItemModel] = {
        
        let homeModel = ApplicationShortcutItemModel(name: "Показания счетчиков",
                                                     typeName: Shortcut.counters.rawValue,
                                                     itemId: "ed7230ca-93c6-416f-a39f-1667632e937d",
                                                     iconName:  UIApplicationShortcutIcon.init(templateImageName: "touch_home"))
        let medicineModel = ApplicationShortcutItemModel(name: "Записаться к врачу",
                                                         typeName: Shortcut.medicine.rawValue,
                                                         itemId: "8d04e7f3-d15d-4f45-b449-5a986f4883d4" ,
                                                         iconName: UIApplicationShortcutIcon.init(templateImageName: "touch_health"))
        let finesModel = ApplicationShortcutItemModel(name: "Проверить штрафы",
                                                      typeName: Shortcut.fines.rawValue,
                                                      itemId: "acc160e7-79b1-4eb2-9b09-7c5f58bdfc87",
                                                      iconName: UIApplicationShortcutIcon.init(templateImageName: "touch_transport"))
        
        return [homeModel, medicineModel, finesModel]
    }()
    
   static func get3DTouchHandler() -> ThreeDTouchHandler {
        
        let handler = ThreeDTouchHandler()
        handler.applicationRouter = ApplicationRouterBuilder.getApplicationRouter()
        handler.conversationsService = UIApplication.shared.serviceBuilder.getConversationsService()
        handler.shortcutModels = shortcutModels
        
        return handler
    }
    
   static func get3DTouchManager() -> ThreeDTouchManager {
        
        let manager = ThreeDTouchManager()
        manager.conversationsService = UIApplication.shared.serviceBuilder.getConversationsService()
        manager.shortcutModels = shortcutModels
        
        return manager 
    }
}

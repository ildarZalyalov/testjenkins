//
//  3DTouchHandler.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 15.12.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Перечисление идентификаторов элементов 3д тача
///
/// - counters: идентификатор счетиков дома
/// - medicine: идентификатор медицины
/// - fines: идентификатор штрафов
/// - map: идентификотор карты
enum Shortcut: String {
    
    case counters = "ru.mos.mobile.app.Counters"
    case medicine = "ru.mos.mobile.app.Medicine"
    case fines    = "ru.mos.mobile.app.Fines"
    case map      = "ru.mos.mobile.app.Map"
}

/// Хэндлер для отлова нажатия на элемент 3д тача
class ThreeDTouchHandler {
    
    var applicationRouter: ApplicationRouter!
    var conversationsService: ConversationsService!
    var shortcutModels:[ApplicationShortcutItemModel]!
    
    func handleQuickAction(shortcutItem: UIApplicationShortcutItem) -> Bool {
        
        var quickActionHandled = false
        
        if let shortcutType = Shortcut.init(rawValue: shortcutItem.type) {
            
            switch shortcutType {
            case .counters:
                
                 getConversation(for: shortcutModels[0], completion: { [weak self] (conversation) in
                    
                    self?.applicationRouter.showChatModule(for: conversation)
                })
                quickActionHandled = true
            case .medicine:
                
                 getConversation(for: shortcutModels[1], completion: { [weak self] (conversation) in
                    self?.applicationRouter.showChatModule(for: conversation)
                })
                
                quickActionHandled = true
            case .fines:
                
                 getConversation(for: shortcutModels[2], completion: { [weak self] (conversation) in
                    self?.applicationRouter.showChatModule(for: conversation)
                })
                
                quickActionHandled = true
            case .map:
                applicationRouter.showChatModule(for: mapConversation)
                quickActionHandled = true
            }
        }
        
        return quickActionHandled
    }
    
    fileprivate func getConversation(for shortCut: ApplicationShortcutItemModel, completion:@escaping (Conversation) -> () ) {
        
        conversationsService.obtainCachedConversation(with: shortCut.itemId) { (result) in
            
            switch result {
                
            case .success(let conversation):
                
                if let conversation = conversation {
                    completion(conversation)
                }
                
            case .failure(let error):
                print("Ошибка получения чата: \(error)")
            }
        }
        
    }
    
}

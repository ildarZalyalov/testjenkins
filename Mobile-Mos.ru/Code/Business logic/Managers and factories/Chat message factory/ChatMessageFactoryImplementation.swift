//
//  ChatMessageFactoryImplementation.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 15.05.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class ChatMessageFactoryImplementation: ChatMessageFactory {
    
    var jsonMapper: JsonResourceMapper!
    
    var formattingExtractor: ChatTextFormattingExtractor!
    var fileDonwloadManager: FileDownloadManager!
    var fileCacheManager: FileCacheManager!
    var imagesPrefetcher: UIImageLoadingAndCacheManager!
    
    let emptyTextPlaceholder = "    "
    
    //MARK: - ChatMessageFactory
    
    func buildMessage(from messageJson: NSDictionary, for user: User) -> ChatMessage {
        
        var message = jsonMapper.mapJson(messageJson, toResourceOfType: ChatMessage.self)
        
        processServerMessage(message: &message)
        processMessage(message: &message, with: user)
        
        return message
    }
    
    func buildFakeIncomingMessage(from text: String, for conversation: Conversation, and user: User) -> ChatMessage {
        
        var message = ChatMessage()
        message.content.text = text
        message.isIncoming = true
        message.status = .success
        
        processLocalMessage(message: &message, with: conversation)
        processMessage(message: &message, with: user)
        
        return message
    }
    
    func buildOutgoingMessage(from text: String, messageContext: String, for conversation: Conversation, and user: User) -> ChatMessage {
        
        var message = ChatMessage()
        message.content.text = text
        message.messageContext = messageContext
        
        processLocalMessage(message: &message, with: conversation)
        processMessage(message: &message, with: user)
        
        return message
    }
    
    func buildMessageHistoryPortion(from historyJson: NSDictionary, for user: User) -> ChatMessageHistoryPortion {
        
        var historyPortion = jsonMapper.mapJson(historyJson, toResourceOfType: ChatMessageHistoryPortion.self)
        historyPortion.messages = historyPortion.messages.map { message in
            
            var processedMessage = message
            
            processServerMessage(message: &processedMessage)
            processMessage(message: &processedMessage, with: user)
            
            return processedMessage
        }
        
        return historyPortion
    }
    
    func cacheKeyForDocument(in message: ChatMessage) -> String? {
        guard let url = message.content.documentObject?.fileUrl else { return nil }
        return String(url.hashValue) + url.lastPathComponent
    }
    
    func updateDocumentDownloadStateIfNeeded(in message: ChatMessage) {
        
        guard let url = message.content.documentObject?.fileUrl else { return }
        guard let state = message.content.documentObject?.downloadState else { return }
        
        if let cacheKey = cacheKeyForDocument(in: message),
           let fileCacheUrl = fileCacheManager.cacheUrl(for: cacheKey) {
            state.status.value = .downloadFinished
            state.progress.value = nil
            state.fileCacheUrl = fileCacheUrl
        }
        else if let progress = fileDonwloadManager.downloadProgressForFile(at: url) {
            state.status.value = .downloading
            state.progress.value = progress
            state.fileCacheUrl = nil
        }
        else {
            state.status.value = .notDownloaded
            state.progress.value = nil
            state.fileCacheUrl = nil
        }
    }
    
    //MARK: - Приватные методы
    
    fileprivate func correctIfEmptyTextMessage(message: inout ChatMessage) {
        guard message.content.type == .text, message.content.text.isEmpty else { return }
        message.content.text = emptyTextPlaceholder
    }
    
    fileprivate func processMessageTextFormatting(in message: inout ChatMessage) {
        guard let formatting = formattingExtractor.extractFormatting(from: message.content.text) else { return }
        message.content.textFormatting = ChatMessageTextFormatting(formatting: formatting)
    }
    
    fileprivate func processContentMessages(in message: inout ChatMessage, with mutation: (inout ChatMessage) -> Void) {
        message.content.messageScrollObject?.messages.forEachMutate(mutation)
        message.content.messageGroupObject?.messageGroups.forEachMutate { $0.forEachMutate(mutation) }
    }
    
    fileprivate func processServerMessage(message: inout ChatMessage) {
        
        let messageUid = message.uid
        
        message.status = .success
        processContentMessages(in: &message, with: { $0.status = .success })
        processContentMessages(in: &message, with: { $0.parentMessageUid = messageUid })
        processContentMessages(in: &message, with: { processMessageTextFormatting(in: &$0) })
        
        correctIfEmptyTextMessage(message: &message)
        processMessageTextFormatting(in: &message)
        updateDocumentDownloadStateIfNeeded(in: message)
        
        let imagesToPrefetch = message.content.imageUrlsToPrefetch
        guard !imagesToPrefetch.isEmpty else { return }
        imagesPrefetcher.prefetchImages(from: imagesToPrefetch, completion: nil)
    }
    
    fileprivate func processLocalMessage(message: inout ChatMessage, with conversation: Conversation) {
        message.chatId = conversation.itemId
        correctIfEmptyTextMessage(message: &message)
    }
    
    fileprivate func processMessage(message: inout ChatMessage, with user: User) {
        
        message.sessionId = user.sessionId
        message.userId = user.userId
        
        processContentMessages(in: &message, with: { $0.sessionId = user.sessionId })
        processContentMessages(in: &message, with: { $0.userId = user.userId })
    }
}

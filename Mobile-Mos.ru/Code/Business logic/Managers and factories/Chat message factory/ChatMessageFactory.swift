//
//  ChatMessageFactory.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 15.05.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Фабрика моделей сообщения из чата
protocol ChatMessageFactory: class {
    
    /// Построить сообщение из json
    ///
    /// - Parameters:
    ///   - messageJson: json сообщения
    ///   - user: текущий пользователь
    /// - Returns: модель входящего сообщения
    func buildMessage(from messageJson: NSDictionary, for user: User) -> ChatMessage
    
    /// Построить фейковое (не с сервера) входящее сообщение из текста
    ///
    /// - Parameters:
    ///   - text: текст сообщения
    ///   - conversation: текущий диалог
    ///   - user: текущий пользователь
    /// - Returns: модель входящего сообщения
    func buildFakeIncomingMessage(from text: String, for conversation: Conversation, and user: User) -> ChatMessage
    
    /// Построить исходящее сообщение из текста
    ///
    /// - Parameters:
    ///   - text: текст сообщения
    ///   - messageContext: контекст сообщения (данные для переключения диалога из одного состояния в другое)
    ///   - conversation: текущий диалог
    ///   - user: текущий пользователь
    /// - Returns: модель исходящего сообщения
    func buildOutgoingMessage(from text: String, messageContext: String, for conversation: Conversation, and user: User) -> ChatMessage
    
    /// Построить часть истории сообщений с сервера
    ///
    /// - Parameters:
    ///   - historyJson: json части истории сообщений
    ///   - user: текущий пользователь
    /// - Returns: часть истории сообщений с сервера
    func buildMessageHistoryPortion(from historyJson: NSDictionary, for user: User) -> ChatMessageHistoryPortion
    
    /// Получить ключ кэширования для документа
    ///
    /// - Parameter message: сообщение с документом
    /// - Returns: ключ кэширования или nil, если сообщение не содержало документа
    func cacheKeyForDocument(in message: ChatMessage) -> String?
    
    /// Обновить состояние загрузки документа в сообщении (если требуется)
    ///
    /// - Parameter message: сообщение с документом
    func updateDocumentDownloadStateIfNeeded(in message: ChatMessage)
}

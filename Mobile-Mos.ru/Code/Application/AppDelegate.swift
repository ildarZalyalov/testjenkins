//
//  AppDelegate.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 09.02.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    let thirdPartiesConfigurator = ThirdPartiesConfigurator.getThirdPartiesConfigurator()
    
    let notificationsManager: ChatNotificationsManager = ChatNotificationsManager.getNotificationsManager()
    let analyticsManager: AnalyticsManager = AnalyticsManagerBuilder.getAnalyticsManager()
    
    var threeDTouchHandler: ThreeDTouchHandler = ThreeDTouchBuilder.get3DTouchHandler()
    var threeDTouchManager: ThreeDTouchManager = ThreeDTouchBuilder.get3DTouchManager()
    
    var spotlightHandler: SpotlightHandler = SpotlightBuilder.getSpotlightHandler()
    var spotlightManager: SpotlightManager = SpotlightBuilder.getSpotlightManager()
    
    var periodicUpdateManager: PeriodicUpdateManager = PeriodicUpdateManager.getPeriodicUpdateManager()
    
    let remoteNotificationsHandler = RemoteNotificationsHandler.getNotificationsHandler()
    let remoteNotificationsConfigurator: RemoteNotificationsConfigurator = {
        if #available(iOS 10.0, *) {
            return RemoteNotificationsDefaultConfigurator()
        } else {
            return RemoteNotificationsLegacyConfigurator()
        }
    }()
    
    let applicationRouter = ApplicationRouterBuilder.getApplicationRouter()
    let appUpdateVerifier = AppUpdateNotificationVerifier.getAppUpdateNotificationVerifier()
    let rateAppNotificationManager = RateAppNotificationManager.getRateAppNotificationManager()
    let onboardingBuilder = OnboardingBuilder.getOnboardingBuilder()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        // Override point for customization after application launch.
        thirdPartiesConfigurator.startThirdParties()
        
        let notificationKey = UIApplicationLaunchOptionsKey.remoteNotification
        remoteNotificationsHandler.applicationLaunchRemoteNotificationPayload = launchOptions?[notificationKey] as? [NSObject : AnyObject]
        
        application.serviceBuilder.fileDownloadManager.clearTemporaryFileCache()
        
        rateAppNotificationManager.setupAppReviewRequest()
        
        let onboardingPresented = onboardingBuilder.presentOnboardingIfNeeded(above: window, configuringNotificationsWith: remoteNotificationsConfigurator)
        if !onboardingPresented {
            remoteNotificationsConfigurator.registerForRemoteNotifications()
        }
        
        analyticsManager.sendSessionEvent(with: .firstSession(withString: AnalyticsConstants.firstSessionEventName))
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        
        threeDTouchManager.updateShortcutItems()
        spotlightManager.configureSpotlightForConversations()
        
        analyticsManager.sendSessionEvent(with: .sessionEnd(withString: AnalyticsConstants.endSessionEventName))
    }
    
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([Any]?) -> Void) -> Bool {
        spotlightHandler.handleTapForConversationItem(with: userActivity)
        return true
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
        let notificationsAuthorized = remoteNotificationsConfigurator.notificationsAreAuthorized ?? false
        if notificationsAuthorized {
            application.applicationIconBadgeNumber = 0
        }
        
        remoteNotificationsHandler.handleApplicationDidReceiveRemoteNotificationOnLaunchIfNeeded()
        remoteNotificationsConfigurator.postRemoteNotificationsStateDidChangeIfNeeded()
        
        periodicUpdateManager.updateDataIfNeeded()
        
        appUpdateVerifier.checkForApplicationUpdates { [weak self] result in
            switch result {
            case .success(let state, let isRecentUpdate):
                guard isRecentUpdate else { return }
                guard let versionInfo = state else { return }
                self?.applicationRouter.showAppNewVersionModule(with: versionInfo)
            case .failure:
                return
            }
        }
        
        analyticsManager.sendSessionEvent(with: .sessionBegin(withString: AnalyticsConstants.beginSessionEventName))
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    //MARK: - 3D Touch shortcut action
    
    func application(_ application: UIApplication, performActionFor shortcutItem: UIApplicationShortcutItem, completionHandler: @escaping (Bool) -> Void) {
        completionHandler(threeDTouchHandler.handleQuickAction(shortcutItem: shortcutItem))
    }
    
    //MARK: - Remote notifications
    
    func application(_ application: UIApplication, didRegister notificationSettings: UIUserNotificationSettings) {
        remoteNotificationsConfigurator.registerForRemoteNotifications()
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        remoteNotificationsConfigurator.handleDidRegister(with: deviceToken)
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        remoteNotificationsConfigurator.handleDidFailToRegister(with: error)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        remoteNotificationsHandler.handleApplicationDidReceiveRemoteNotification(userInfo)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        remoteNotificationsHandler.handleApplicationDidReceiveRemoteNotification(userInfo, fetchCompletionHandler: completionHandler)
    }

}


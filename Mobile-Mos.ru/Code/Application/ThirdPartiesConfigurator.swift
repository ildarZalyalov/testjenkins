//
//  ThirdPartiesConfigurator.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 23.03.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import Fabric
import Crashlytics
import SDWebImage
import YandexMobileMetrica
import Flurry_iOS_SDK

final class ThirdPartiesConfigurator {
    
    /// Менеджер определения местоположения пользователя
    var locationManager: CLLocationManager!
    
    /// Менеджер текущего пользователя
    var currentUserManager: CurrentUserManager!
    
    /// Центр уведомлений для отлова переключения стейта логин/лоаут пользователя
    var notificationCenter: NotificationCenter! {
        didSet {
            notificationCenter.addObserver(self, selector: #selector(userLoginStateDidChange), name: .UserLoginStateDidChange, object: nil)
        }
    }
    
    deinit {
        notificationCenter.removeObserver(self)
    }
    
    func startThirdParties() {
        
        configureYMKMapKit()
        configureSDWebImage()
        configureAnalytics()
    }
    
    func configureYMKMapKit() {
        YMKMapKit.setApiKey("5f8776b7-3b94-4001-8c47-5031a516a411")
    }
    
    func configureSDWebImage() {
        
        class SDWebImageImageCoder: SDWebImageImageIOCoder {
            
            override func decompressedImage(with image: UIImage?, data: AutoreleasingUnsafeMutablePointer<NSData?>, options optionsDict: [String : NSObject]? = nil) -> UIImage? {
                guard NSData.sd_imageFormat(forImageData: data.pointee as Data?) != .GIF else { return image }
                return super.decompressedImage(with: image, data: data, options: optionsDict)
            }
        }
        
        SDWebImageCodersManager.sharedInstance().addCoder(SDWebImageImageCoder())
    }
    
    func configureAnalytics() {
        
        let userLocation = locationManager.location
        let buildConfifuration = UIApplication.shared.buildConfiguration
        
        //YandexMetrica
        let yandexMetricConfiguration = YMMYandexMetricaConfiguration(apiKey: "b0a8a309-bcea-4b76-84eb-d62aa536ca06")!
        YMMYandexMetrica.activate(with: yandexMetricConfiguration)
        YMMYandexMetrica.setLocation(userLocation)
        YMMYandexMetrica.setLocationTracking(userLocation != nil)
        YMMYandexMetrica.setUserProfileID(currentUserManager.currentUser.sessionId)
        
        //Flurry
        let logLevel: FlurryLogLevel = buildConfifuration == .release ? FlurryLogLevelNone : FlurryLogLevelAll
        Flurry.trackPreciseLocation(false)
        Flurry.startSession("BRFG8ZRKK68MJQP9V53C", with: FlurrySessionBuilder
            .init()
            .withLogLevel(logLevel))
        
        //Crashlytics
        Fabric.with([Crashlytics.self])
    }
    
    @objc
    fileprivate func userLoginStateDidChange() {
        YMMYandexMetrica.setUserProfileID(currentUserManager.currentUser.sessionId)
    }
}

extension ThirdPartiesConfigurator {
    
    /// Конфигурация зависимостей для ThirdPartiesConfigurator
    static func getThirdPartiesConfigurator() -> ThirdPartiesConfigurator {
        
        let configurator = ThirdPartiesConfigurator()
        let locationManager = CLLocationManager()
        let currentUserManager = CurrentUserManagerImplementation()
        let notificationCenter = NotificationCenter.default
        
        configurator.locationManager = locationManager
        configurator.currentUserManager = currentUserManager
        configurator.notificationCenter = notificationCenter
        
        return configurator
    }
}

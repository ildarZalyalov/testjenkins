//
//  ApplicationConfiguration.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 13.02.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import UIKit

extension UIApplication {
    
    /// Конфигурация сборки
    ///
    /// - debug: отладка
    /// - release: боевая
    public enum BuildConfiguration: String {
        case debug
        case release
    }
    
    /// Текущая конфигурация сборки
    #if RELEASE
        var buildConfiguration: BuildConfiguration { return .release }
    #else
        var buildConfiguration: BuildConfiguration { return .debug }
    #endif
    
    /// Текущая конфигурация сервисов
    var serviceConfiguration: BuildConfiguration {
        
        var configuration = buildConfiguration
        
        if configuration == .debug {
            let useReleaseAPI = UserDefaults.standard.bool(forKey: ApplicationUserDefaultsKey.debugModeUseReleaseAPI.rawValue)
            configuration = useReleaseAPI ? .release : .debug
        }
        
        return configuration
    }
    
    //MARK: - ServiceBuilder
    
    /// Строитель сервисов, для получения экземпляров сервисов в любом месте приложения надо обращаться к нему
    var serviceBuilder: ServiceBuilder {
        recreateServiceBuilderIfNeeded()
        return UIApplication.currentServiceBuilder
    }
    
    /// Текущий закэшированный строитель сервисов
    fileprivate static var currentServiceBuilder: ServiceBuilder!
    
    /// Создаем строитель сервисов согласно текущей конфигурации сервисов
    fileprivate func createServiceBuilder() -> ServiceBuilder {
        return serviceConfiguration == .release ? ServiceBuilderRelease() : ServiceBuilderDebug()
    }
    
    /// Пересоздаем строитель сервисов согласно текущей конфигурации сервисов, если это необходимо
    fileprivate func recreateServiceBuilderIfNeeded() {
        
        // если строитель еще не определен, то просто создаем его
        guard UIApplication.currentServiceBuilder != nil else {
            UIApplication.currentServiceBuilder = createServiceBuilder()
            return
        }
        
        var shouldRecreateServiceBuilder = false
        let usingDebugServiceBuilder = UIApplication.currentServiceBuilder is ServiceBuilderDebug
        
        switch serviceConfiguration {
        case .debug:
            shouldRecreateServiceBuilder = !usingDebugServiceBuilder
        case .release:
            shouldRecreateServiceBuilder = usingDebugServiceBuilder
        }
        
        if shouldRecreateServiceBuilder {
            UIApplication.currentServiceBuilder = createServiceBuilder()
        }
    }
}

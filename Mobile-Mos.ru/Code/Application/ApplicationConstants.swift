//
//  ApplicationConstants.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 13.02.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

//MARK: Global Constants

/// Идентификатор русского языка согласно ISO 639 (https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes)
let localeIdentifierRussian = "ru"

/// Идентификатор английского языка согласно ISO 639 (https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes)
let localeIdentifierEnglish = "en"

/// Base urls

/// Url по умолчанию, используется сервисами, если url не указан явно
let baseUrlStringDefault = baseUrlStringDataMosAPI

/// Базовый url для сервера Developer
let baseUrlStringDeveloper = "https://mobile-dev.tech.mos.ru/"

/// Базовый url для сервера Beta
let baseUrlStringBeta = "https://mobile-beta.tech.mos.ru/"

/// Базовый url для сервера Production
let baseUrlStringRelease = "https://m.mos.ru/"

/// Базовый url на API mos.ru
let baseUrlStringDataMosAPI = "https://www.mos.ru"

/// Базовый url на поиск в itunes по id
let baseUrlStringItunesLookup = "https://itunes.apple.com"

/// Базовый url для сервиса статических Яндекс карт
let baseUrlStringYandexMapsStatic = "https://static-maps.yandex.ru/1.x/"

/// Базовый url для сервиса геокодинга Яндекс
let baseUrlStringYandexGeocoder = "https://geocode-maps.yandex.ru/1.x/"

/// Базовый url для сервиса "Чудо техники"
let baseUrlStringCTMosRu = "https://crm.ctech.moscow/rest/"

/// Url paths

/// Путь до сервера Opendata
let pathToOpendata = "api/opendata/v1/frontend/json/ru/"

/// Путь до интеллектуального поиска mos.ru
let pathToAiSearch = "aisearch/frontend/api/v1/search/mobile_news/"

/// Путь до саджестов mos.ru
let pathToAiSuggest = "aisearch/suggest/ais/api/"

/// Путь до проксированных методов ЕХД
let pathToEhd = "api/ehd/"

/// Url end points

/// Путь до топовых поисковых слов
let topWordsEndpoint = "topw/serp/"

/// Путь до поиска по саджестам
let suggestsEndpoint = "suggests/serp/"

/// Неймспейс для сервера обмена сообщениями
let chatServerNamespace = "/chat_v1"

/// Путь для сервера обмена сообщениями
let chatServerPath = "/socket.io"

/// Путь до погоды на нашем сервере
let weatherEndpoint = "weather"

/// Other

/// Координаты центра Москвы, всегда пригодятся :)
let moscowLocation = Location(with: [37.617635, 55.755814])

/// Таймзона Москвы
let moscowTimezone = TimeZone(identifier: "Europe/Moscow")

/// BoundingBox области москвы, централизован на Москве
let moscowBoundingBox: YMKBoundingBox = {
    return YMKBoundingBox(southWest: YMKPoint(latitude: 37.3193, longitude: 55.4907), northEast: YMKPoint(latitude: 37.9676, longitude: 55.9576))
}()

/// Строковое представление null на сервере
let serverNilStringLiteral = "None"

/// Ссылка на приложение
let applicationUrl = URL(string: "https://itunes.apple.com/ru/app/id1390518510")!

/// Ссылка на приложение для его оценки
let applicationReviewInAppStoreUrl = URL(string: "https://itunes.apple.com/ru/app/id1390518510?action=write-review")!

/// Apple Id приложения
let applicationAppleId = "1390518510"

/// Ссылка на приложение Госуслуги
let gosuslugiApplicationUrl = URL(string: "https://itunes.apple.com/ru/app/id566254786")!

/// Ссылка на приложение Активный Гражданин
let agApplicationUrl = URL(string: "https://itunes.apple.com/ru/app/id873648765")!

/// Название для приложения Яндекс.Карты
let yandexMapsAppName = "Яндекс.Карты"

/// Ссылка на пользовательское соглашение
let legalRulesUrl = URL(string: "https://www.mos.ru/legal/rules/")!

/// Ссылка на пользовательское соглашение API Яндекс.Карт
let legalYandexAPIRulesUrl = URL(string: "https://yandex.ru/legal/maps_api/")!

/// Ссылка на пользовательское соглашение оплаты
let paymentRulesUrl = URL(string: "https://pay.mos.ru/mospayportal/assets/offer/offer.pdf")!

/// Ссылка на пользовательское соглашение сервиса Чудо Техники
let ctMosRuRulesUrl = URL(string: "https://ct.mos.ru/offer/")!

/// Ссылка на сервер hpsm для запросов
let hpsmServiceUrlString = "http://sm.mos.ru:8090/SM/7/ws"

/// Идентификатор в платежной системе
let paymentSystemIdentifier = "AC05D3316C388017BF08E754BA3B424A"

/// View model сервиса Чудо Техники для отображения информации о нем в приложении
let ctMosRuServiceViewModel = ServicesAndAppsModel(icon: #imageLiteral(resourceName: "ctMosAvatar"),
                                                   title: "Чудо Техники",
                                                   subtitle: "городской онлайн-сервис",
                                                   descriptionHeader: "Починим телефон, планшет, ноутбук и не только.",
                                                   description: "Приедем и заберем неисправное устройство\nПроведем диагностику\nСделаем так, чтобы всё работало!",
                                                   linkFromApp: "https://ct.mos.ru",
                                                   buttonTitle: "Подать заявку")

/// Конструктор ссылки построения маршрута в приложении Яндекс.Карты
///
/// - Parameters:
///   - startPoint: начальная точка маршрута
///   - endPoint: конечная точка маршрута
/// - Returns: ссылка построения маршрута
func yandexMapsAppRouteUrl(from startPoint: Location, to endPoint: Location) -> URL? {
    return URL(string: "yandexmaps://maps.yandex.ru/?rtext=\(startPoint.latitude),\(startPoint.longitude)~\(endPoint.latitude),\(endPoint.longitude)&rtt=auto")
}

/// Ссылка на приложение Яндекс.Карты
let yandexMapsAppStoreUrl = URL(string: "https://itunes.apple.com/ru/app/yandex.maps/id313877526?mt=8")

/// Название для приложения Яндекс.Навигатор
let yandexNavigatorAppName = "Яндекс.Навигатор"

/// Конструктор ссылки построения маршрута в приложении Яндекс.Навигатор
///
/// - Parameters:
///   - startPoint: начальная точка маршрута
///   - endPoint: конечная точка маршрута
/// - Returns: ссылка построения маршрута
func yandexNavigatorAppRouteUrl(from startPoint: Location, to endPoint: Location) -> URL? {
    return URL(string: "yandexnavi://build_route_on_map?lat_from=\(startPoint.latitude)&lon_from=\(startPoint.longitude)&lat_to=\(endPoint.latitude)&lon_to=\(endPoint.longitude)")
}

/// Ссылка на приложение Яндекс.Навигатор
let yandexNavigatorAppStoreUrl = URL(string: "https://itunes.apple.com/ru/app/yandex.navigator/id474500851")

//MARK: - User defaults constants


/// Ключи в UserDefaults
///
/// - deviceInstallationIdentificator: ключ, по которому хранится идентификатор установки приложения
/// - debugModeUseReleaseAPI: ключ, по которому хранится флаг об использовании боевого API
/// - onboardingWasCompleted: ключ, по которому хранится флаг о gрохождении онбординга
/// - remoteNotificationsAreAuthorized: ключ, по которому хранится флаг о статусе авторизации пуш-уведомлений
/// - remoteNotificationsPassedToServerToken: ключ, по которому хранится последний успешно отправленный на сервер токен пуш-уведомлений
/// - lastBannerBackgroundUrl: ключ, по которому хранится последний url задника баннера
/// - lastBannerTextColor: ключ, по которому хранится цвет текста фактоида для баннера
/// - lastBannerStatusBarStyle: ключ, по которому хранится стиль статус бара для баннера
/// - isMapDark: ключ, по которому хранится включен ли "темный режим" для карты
/// - isTrafficJamEnabled: ключ, по которому хранится включено ли отображение пробок на карте
/// - isTrafficJamEnabled: ключ, по которому хранится дата последнего обновления личных данных авторизованного пользователя
/// - forgotPasswordCodeRequestTimes: ключ, по которому хранится кол-во отправок смс для восстановления пароля пользователя
/// - forgotPasswordCacheDate: ключ, по которому хранится последняя дата отправки смс
/// - updateWeatherCacheDate: ключ, по которому хранится последняя дата обновления погоды
/// - firstUserSession: ключ, по которому хранится первая ли сессия пользователя
public enum ApplicationUserDefaultsKey: String {
    case deviceInstallationIdentificator
    case debugModeUseReleaseAPI
    case onboardingWasCompleted
    case remoteNotificationsAreAuthorized
    case remoteNotificationsPassedToServerToken
    case lastBannerBackgroundUrl
    case lastBannerTextColor
    case lastBannerStatusBarStyle
    case isMapDark
    case isTrafficJamEnabled
    case userInfoCacheUpdateDate
    case forgotPasswordCodeRequestTimes
    case forgotPasswordCacheDate
    case updateWeatherCacheDate
    case firstUserSession
}

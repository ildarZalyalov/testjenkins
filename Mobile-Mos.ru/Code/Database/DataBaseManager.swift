//
//  DataBaseManager.swift
//  Portable
//
//  Created by Ildar Zalyalov on 27.04.17.
//  Copyright © 2017. All rights reserved.
//

import Foundation

/// Результат выполнения операции в БД
///
/// - success: успех
/// - error: ошибка, связанное значение - объект ошибки
enum DataBaseOperationResult {
    case success
    case error(Error)
}

/// Результат выполнения выборки из БД
///
/// - success: успех, связанное значение - набор моделей
/// - error: ошибка, связанное значение - объект ошибки
enum DataBaseFetchResult<ModelType: Persistable> {
    case success([ModelType])
    case error(Error)
}

/// Результат получения контроллера выборки из БД
///
/// - success: успех, связанное значение - контроллер выборки из БД
/// - error: ошибка, связанное значение - объект ошибки
enum DataBaseControllerFetchResult<ModelType: Persistable> {
    case success(DataBaseFetchController<ModelType>)
    case error(Error)
}

/// Обработчик выполнения операции в БД
typealias DataBaseOperationResultBlock = (DataBaseOperationResult) -> Void

/// Обработчик выполнения выборки из БД
typealias DataBaseFetchResultBlock<ModelType: Persistable> = (DataBaseFetchResult<ModelType>) -> Void

/// Обработчик получения контроллера выборки из БД
typealias DataBaseControllerFetchResultBlock<ModelType: Persistable> = (DataBaseControllerFetchResult<ModelType>) -> Void

/// Менеджер по работе с БД
protocol DataBaseManager: class {
    
    //MARK: - Методы сохранения моделей -
    
    /// Логика определения нужно ли сохранить или обновить модель задается реализацией
    /// Обычно у моделей есть поле (или метод), позволяющее уникально идентифицировать данный экземпляр модели в базе данных
    /// Для каждой переданной модели при помощи данного поля (метода) определяется сохранена ли она уже в базе данных
    /// Если да, то она обновляется. Если нет - сохраняется в базе данных.
    
    /// Сохранить или обновить модели в базе данных
    ///
    /// - Parameter models: модели
    /// - Throws: объект ошибки (Error), описывающий что пошло не так
    func save<ModelType: Persistable>(models: [ModelType]) throws
    
    /// Асинхронно сохранить или обновить модель в базе данных
    ///
    /// - Parameters:
    ///   - models: модели
    ///   - resultBlock: обработчик выполнения операции, вызывается по завершении операции
    func saveAsync<ModelType: Persistable>(models: [ModelType], resultBlock: DataBaseOperationResultBlock?)
    
    //MARK: - Методы получение моделей -
    
    /// Сделать выборку из базы данных
    ///
    /// - Parameter request: запрос на выборку
    /// - Returns: массив моделей, подходящих по критериям выборки
    /// - Throws: объект ошибки (Error), описывающий что пошло не так
    func fetch<ModelType>(with request: DataBaseFetchRequest<ModelType>) throws -> [ModelType]
    
    /// Асинхронно сделать выборку из базы данных
    ///
    /// - Parameters:
    ///   - request: запрос на выборку
    ///   - resultBlock: обработчик выполнения выборки, вызывается по завершении выборки
    func fetchAsync<ModelType>(with request: DataBaseFetchRequest<ModelType>, resultBlock: DataBaseFetchResultBlock<ModelType>?)
    
    /// Получить контроллер выборки из БД
    ///
    /// - Parameter request: запрос на выборку
    /// - Returns: контроллер выборки из БД
    /// - Throws: объект ошибки (Error), описывающий что пошло не так
    func fetchController<ModelType>(with request: DataBaseFetchRequest<ModelType>) throws -> DataBaseFetchController<ModelType>
    
    /// Асинхронно получить контроллер выборки из БД
    ///
    /// - Parameters:
    ///   - request: request: запрос на выборку
    ///   - resultBlock: обработчик получения контроллера выборки из БД, вызывается по завершении получения контроллера
    func fetchControllerAsync<ModelType>(with request: DataBaseFetchRequest<ModelType>, resultBlock: DataBaseControllerFetchResultBlock<ModelType>?)
    
    //MARK: - Методы удаления моделей -
    
    /// Логика определения сохранена ли модель в базе данных задается реализацией
    /// Обычно у моделей есть поле (или метод), позволяющее уникально идентифицировать данный экземпляр модели в базе данных
    /// Для каждой переданной модели при помощи данного поля (метода) определяется сохранена ли она уже в базе данных
    /// Если в массиве попадается модель, которая не сохранена в базе данных, то операция удаления завершается с ошибкой
    
    /// Удалить модели из базы данных
    ///
    /// - Parameter models: модели
    /// - Throws: объект ошибки (Error), описывающий что пошло не так
    func delete<ModelType: Persistable>(models: [ModelType]) throws
    
    /// Асинхронно удалить модели из базы данных
    ///
    /// - Parameters:
    ///   - models: модели
    ///   - resultBlock: обработчик выполнения операции, вызывается по завершении операции
    func deleteAsync<ModelType: Persistable>(models: [ModelType], resultBlock: DataBaseOperationResultBlock?)
    
    //MARK: - Методы обновления моделей -
    
    /// Асинхронно обновить модели в базе данных
    ///
    /// - Parameters:
    ///   - modelType: тип моделей, которые нужно обновить
    ///   - value: значение для поля, которое нужно обновить
    ///   - key: key path для поля, которое нужно обновить
    ///   - resultBlock: обработчик выполнения операции, вызывается по завершении операции
    func updateAllModelsAsync<ModelType: Persistable>(of modelType: ModelType.Type, with value: Any, for key: String, resultBlock: DataBaseOperationResultBlock?)
    
    /// Асинхронно обновить модели в базе данных
    ///
    /// - Parameters:
    ///   - modelType: тип моделей, которые нужно обновить
    ///   - filterPredicate: предикат для фильтрации моделей
    ///   - value: значение для поля, которое нужно обновить
    ///   - key: key path для поля, которое нужно обновить
    ///   - resultBlock: обработчик выполнения операции, вызывается по завершении операции
    func updateAllModelsAsync<ModelType: Persistable>(of modelType: ModelType.Type, where filterPredicate: NSPredicate, with value: Any, for key: String, resultBlock: DataBaseOperationResultBlock?)
    
    /// Асинхронно удалить модели в базе данных
    ///
    /// - Parameters:
    ///   - modelType: тип моделей, которые нужно удалить
    ///   - resultBlock: обработчик выполнения операции, вызывается по завершении операции
    func deleteAllModelsAsync<ModelType: Persistable>(of modelType: ModelType.Type, resultBlock: DataBaseOperationResultBlock?)

    /// Асинхронно удалить модели в базе данных
    ///
    /// - Parameters:
    ///   - modelType: тип моделей, которые нужно удалить
    ///   - filterPredicate: предикат для фильтрации моделей
    ///   - resultBlock: обработчик выполнения операции, вызывается по завершении операции
    func deleteAllModelsAsync<ModelType: Persistable>(of modelType: ModelType.Type, where filterPredicate: NSPredicate, resultBlock: DataBaseOperationResultBlock?)
    
    /// Выполнить асинхронно блок кода после того, как выполнятся все текущие
    /// асинхронные операции записи в БД
    /// (блок ожидает выполнения операций записи, которые выполнялись/ожидали
    /// выполнения на момент вызова метода)
    ///
    /// - Parameter work: блок кода
    func performAsyncAfterAllSavesFinished(work: @escaping () -> ())
}

//
//  DataBaseFetchRequest.swift
//  Portable
//
//  Created by Ivan Erasov on 08.06.17.
//  Copyright © 2017. All rights reserved.
//

import Foundation

/// Дескриптор сортировки для выборки из БД
struct DataBaseSortDescriptor {
    
    /// Key path поля модели, по которому будет идти сортировка
    var sortKeyPath: String
    
    /// Должна ли сортировка быть по возрастанию
    var sortAscending: Bool = true
    
    init(sortKeyPath: String) {
        self.sortKeyPath = sortKeyPath
    }
    
    init(sortKeyPath: String, sortAscending: Bool) {
        self.sortKeyPath = sortKeyPath
        self.sortAscending = sortAscending
    }
}

/// Структура, описывающая запрос на выборку из БД
struct DataBaseFetchRequest<ModelType: Persistable> {
    
    /// Тип ограничения выборки на кол-во объектов
    ///
    /// - prefix: выбирать объекты с начала
    /// - suffix: выбирать объекты с конца
    enum LimitType {
        case prefix
        case suffix
    }
    
    /// Тип моделей
    var modelType: ModelType.Type
    
    /// Предикат для фильтрации моделей
    var filterPredicate: NSPredicate?
    
    /// Дескрипторы сортировки
    var sortDescriptors: [DataBaseSortDescriptor] = []
    
    /// Ограничение на кол-во объектов в выборке
    var limit: Int?
    
    /// Тип ограничения выборки на кол-во объектов
    var limitType: LimitType = .prefix
    
    init(with type: ModelType.Type) {
        modelType = type
    }
    
    init(with type: ModelType.Type, limitedBy count: Int) {
        self.init(with: type)
        limit = count
    }
    
    init(with type: ModelType.Type, filteredWith predicateFormat: String, arguments: Any...) {
        self.init(with: type)
        filterPredicate = NSPredicate(format: predicateFormat, argumentArray: arguments)
    }
    
    init(with type: ModelType.Type, limitedBy count: Int, filteredWith predicateFormat: String, arguments: Any...) {
        self.init(with: type)
        limit = count
        filterPredicate = NSPredicate(format: predicateFormat, argumentArray: arguments)
    }
    
    init(with type: ModelType.Type, filteredWith predicate: NSPredicate) {
        self.init(with: type)
        filterPredicate = predicate
    }
    
    init(with type: ModelType.Type, limitedBy count: Int, filteredWith predicate: NSPredicate) {
        self.init(with: type)
        limit = count
        filterPredicate = predicate
    }
    
    init(with type: ModelType.Type, sortedBy keyPath: String, ascending: Bool = true) {
        self.init(with: type)
        sortDescriptors.append(DataBaseSortDescriptor(sortKeyPath: keyPath, sortAscending: ascending))
    }
    
    init(with type: ModelType.Type, limitedBy count: Int, sortedBy keyPath: String, ascending: Bool = true) {
        self.init(with: type)
        limit = count
        sortDescriptors.append(DataBaseSortDescriptor(sortKeyPath: keyPath, sortAscending: ascending))
    }
}

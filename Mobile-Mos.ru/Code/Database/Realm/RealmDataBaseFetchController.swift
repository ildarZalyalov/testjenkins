//
//  RealmDataBaseFetchController.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 28.08.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import RealmSwift

class RealmDataBaseFetchController<ModelType: Persistable>: DataBaseFetchControllerProtocol {
    
    typealias Model = ModelType

    let fetchRequest: DataBaseFetchRequest<ModelType>
    let fetchResults: Results<Object>
    fileprivate(set) var fetchResultsUpdateNotificationToken: NotificationToken!
    
    var results: [ModelType] {
        
        let mapper: (Object) -> ModelType = { object in
            return ModelType(with: object as! ModelType.PersistableObjectType)
        }
        
        return fetchResults.toPonsoArray(limitedBy: fetchRequest.limit, fromEnd: fetchRequest.limitType == .suffix, mapper: mapper)
    }
    
    var onUpdate: DataBaseFetchUpdateBlock?
    
    init(with fetchRequest: DataBaseFetchRequest<ModelType>, and fetchResults: Results<Object>) {
        
        self.fetchRequest = fetchRequest
        self.fetchResults = fetchResults
        
        fetchResultsUpdateNotificationToken = fetchResults.observe({ [weak self] change in
            switch change {
            case .update(_, let deletions, let insertions, let modifications):
                let update = DataBaseFetchUpdate(deletions: deletions, insertions: insertions, modifications: modifications)
                self?.onUpdate?(update)
            default:
                return
            }
        })
    }
    
    deinit {
        fetchResultsUpdateNotificationToken.invalidate()
    }
}

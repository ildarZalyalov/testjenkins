//
//  RealmExtensions.swift
//  Portable
//
//  Created by Ivan Erasov on 07.06.17.
//  Copyright © 2017. All rights reserved.
//

import Foundation
import RealmSwift

extension Results {
    
    /// Создать массив ponso
    ///
    /// - Parameters:
    ///   - count: ограничение по количеству (если есть)
    ///   - fromEnd: применять ли ограничение по количеству с конца набора
    /// - Returns: массив ponso
    func toPonsoArray<ModelType: Persistable>(limitedBy count: Int? = nil, fromEnd: Bool = false) -> [ModelType] where ModelType.PersistableObjectType == Element {
        return toPonsoArray(limitedBy: count, fromEnd: fromEnd, mapper: ModelType.init(with:))
    }
    
    /// Создать массив ponso
    ///
    /// - Parameters:
    ///   - count: ограничение по количеству (если есть)
    ///   - fromEnd: применять ли ограничение по количеству с конца набора
    ///   - mapper: маппер типа в Results в тип модели
    /// - Returns: массив ponso
    func toPonsoArray<ModelType: Persistable>(limitedBy count: Int? = nil, fromEnd: Bool = false, mapper: (Element) -> ModelType) -> [ModelType] {
        
        // Зачем такой изврат? Просто у Results нет prefix и suffix
        
        if let limit = count {
            if fromEnd {
                return self.lazy.suffix(limit).map(mapper)
            }
            else {
                return self.lazy.prefix(limit).map(mapper)
            }
        }
        else {
            return self.map(mapper)
        }
    }
}

extension List {
    
    /// Создать массив ponso
    ///
    /// - Returns: массив ponso
    func toPonsoArray<ModelType: Persistable>() -> [ModelType] where ModelType.PersistableObjectType == Element {
        return map { ModelType(with: $0) }
    }
}

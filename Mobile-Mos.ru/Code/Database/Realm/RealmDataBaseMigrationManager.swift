//
//  RealmDataBaseMigrationManager.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 03.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import RealmSwift

/// Менеджер миграций Realm, здесь определены методы для миграций между разными версиями файлов БД
final class RealmDataBaseMigrationManager {
    
    fileprivate let realmEncryptionService = "ru.mos.realm.encryption"
    fileprivate let realmEncryptionKey = "ru.mos.realm.encryption.key"
    fileprivate let realmEncryptionKeyLength = 64
    fileprivate let realmEncryptedFilename = "encrypted.realm"
    
    fileprivate var keychainStorage: KeychainStorage!
    
    /// Текущая версия схемы Realm
    fileprivate let currentRealmSchemaVersion: UInt64 = 0
    
    /// Конфигурация, используемая всеми экземплярами Realm
    func obtainSharedConfiguration() -> Realm.Configuration {
        return Realm.Configuration(schemaVersion: currentRealmSchemaVersion,
                                   deleteRealmIfMigrationNeeded: true)
    }
    
    /// Конфигурация, используемая всеми зашифрованными экземплярами Realm
    func obtainEncryptedConfiguration() -> Realm.Configuration {
        
        var config = Realm.Configuration(encryptionKey: obtainRealmEncryptionKey(),
                                         schemaVersion: currentRealmSchemaVersion,
                                         deleteRealmIfMigrationNeeded: true)
        config.fileURL = config.fileURL?.deletingLastPathComponent().appendingPathComponent(realmEncryptedFilename)
        
        return config
    }
    
    //MARK: - Приватные методы
    
    /// Получить ключ шифрования зашифрованных экземпляров Realm
    fileprivate func obtainRealmEncryptionKey() -> Data {
        
        let context = KeychainEntryContext(with: realmEncryptionService, and: realmEncryptionKey)
        let cachedKey: Data? = (try? keychainStorage.fetchData(with: context)) ?? nil
        
        guard cachedKey == nil else { return cachedKey! }
        
        var encryptionKey = Data(count: realmEncryptionKeyLength)
        _ = encryptionKey.withUnsafeMutableBytes { bytes in
            SecRandomCopyBytes(kSecRandomDefault, realmEncryptionKeyLength, bytes)
        }
        
        var saveContext = KeychainNewEntryContext(with: realmEncryptionService, and: encryptionKey)
        saveContext.label = realmEncryptionService
        saveContext.comment = realmEncryptionKey
        saveContext.synchronizable = false
        saveContext.accessibility = .afterFirstUnlockThisDeviceOnly
        
        try? keychainStorage.saveEntry(with: saveContext, for: realmEncryptionKey)

        return encryptionKey
    }
    
    //MARK: - Открытые методы
    
    /// Инициализация менеджера
    static func getMigrationManager() -> RealmDataBaseMigrationManager {
        
        let manager = RealmDataBaseMigrationManager()
        manager.keychainStorage = KeychainAccessStorage()
        
        return manager
    }
}

//
//  DataBaseManager.swift
//  Portable
//
//  Created by Ildar Zalyalov on 27.04.17.
//  Copyright © 2017. All rights reserved.
//

import Foundation
import RealmSwift

/// Перечисление, для выбора ошибки которой кидаемся 💩
///
/// - typeCantBePersistedInRealm: Используем для проверки, что persistable object является наследником Object
/// - objectCantBeResolved: Используем для проверки primaryKey у объекта
/// - referenceToFetchResultCannotBeResolved: Используем при невозможности "перекинуть" на главный поток ссылку с результатами выборки для создания контроллера выборки из БД
enum RealmDataManagerErrors: Error {
    case typeCantBePersistedInRealm(type: Any.Type)
    case objectCantBeResolved(objectType: Object.Type)
    case referenceToFetchResultCannotBeResolved
}

/// Результат выполнения выборки из БД для создания контроллера выборки из БД
///
/// - success: success: успех, связанное значение - потокобезопасная ссылка на результаты выборки из БД
/// - error: error: ошибка, связанное значение - объект ошибки
enum RealmDataBaseControllerFetchResult {
    case success(ThreadSafeReference<Results<Object>>)
    case error(Error)
}

/// Класс реализующий протокол DataBaseManager для хранения объектов в Realm
class RealmDataBaseManager: DataBaseManager {
    
    /// Очередь для чтения из Realm не на главном потоке
    fileprivate static var backGroundReadQueue: OperationQueue = {
        var queue = OperationQueue()
        queue.name = "backGroundReadQueue"
        return queue
    }()
    
    /// Очередь для записи в Realm не на главном потоке
    fileprivate static var backGroundWriteQueue: OperationQueue = {
        var queue = OperationQueue()
        queue.name = "backGroundWriteQueue"
        return queue
    }()
    
    /// Очередь для удаления из Realm не на главном потоке
    fileprivate static var backGroundDeleteQueue: OperationQueue = {
        var queue = OperationQueue()
        queue.name = "backGroundDeleteQueue"
        return queue
    }()
    
    /// Очередь для выполнения операций не на главном потоке, операции
    /// в которой ждут окончания всех операций записи, которые были
    /// на момент их создания
    fileprivate static var backGroundWriteSyncedWorkQueue: OperationQueue = {
        var queue = OperationQueue()
        queue.name = "backGroundWriteSyncedWorkQueue"
        return queue
    }()
    
    /// Использовать ли зашифрованный файл для хранения данных в БД
    fileprivate(set) var shouldUseEncryptedStorage = false
    
    /// Логировать ошибки в асинхронных операция с БД
    var shouldLogAsyncOperationsErrors = false
    
    convenience init(shouldUseEncryptedStorage: Bool) {
        self.init()
        self.shouldUseEncryptedStorage = shouldUseEncryptedStorage
    }
    
    //MARK: - DataBaseManager
    
    func save<ModelType: Persistable>(models: [ModelType]) throws {
        
        guard !models.isEmpty else { return }
        
        try checkModelCanBePersistedInRealm(modelType: ModelType.self)
        
        try autoreleasepool {
            
            let db = try createRealm()
            let realmModels = models.toPersistableObjectsArray() as! [Object]
            let canUpdate = (try? checkModelCanBePersistedInRealmAndResolved(modelType: ModelType.self)) ?? false
            
            try write(models: realmModels, to: db, canUpdate: canUpdate)
        }
    }
    
    func saveAsync<ModelType: Persistable>(models: [ModelType], resultBlock: DataBaseOperationResultBlock?) {
        
        guard !models.isEmpty else { return }
        
        do {
            try checkModelCanBePersistedInRealm(modelType: ModelType.self)
        }
        catch {
            call(completion: resultBlock, with: .error(error))
        }
        
        //Создаем persistable objects на том же потоке, чтобы не захватывать на другой поток models
        let realmModels = models.toPersistableObjectsArray() as! [Object]
        let canUpdate = (try? checkModelCanBePersistedInRealmAndResolved(modelType: ModelType.self)) ?? false
        
        let backGroundWriteQueue = RealmDataBaseManager.backGroundWriteQueue
        backGroundWriteQueue.addOperation { [weak self] in
            
            guard let strongSelf = self else { return }
            
            var result: DataBaseOperationResult
            
            defer {
                strongSelf.call(completion: resultBlock, with: result)
            }
            
            do {
                
                try autoreleasepool {
                    let db = try strongSelf.createRealm()
                    try strongSelf.write(models: realmModels, to: db, canUpdate: canUpdate)
                }
                
                result = .success
            }
            catch {
                result = .error(error)
            }
        }
    }
    
    func fetch<ModelType>(with request: DataBaseFetchRequest<ModelType>) throws -> [ModelType] {
        
        let models = try fetchResults(with: request)
        let mapper: (Object) -> ModelType = { object in
            return ModelType(with: object as! ModelType.PersistableObjectType)
        }
        
        return models.toPonsoArray(limitedBy: request.limit, fromEnd: request.limitType == .suffix, mapper: mapper)
    }
    
    func fetchAsync<ModelType>(with request: DataBaseFetchRequest<ModelType>, resultBlock: DataBaseFetchResultBlock<ModelType>?) {
        
        let backGroundReadQueue = RealmDataBaseManager.backGroundReadQueue
        backGroundReadQueue.addOperation { [weak self] in
            
            guard let strongSelf = self else { return }
            
            var result: DataBaseFetchResult<ModelType>
            
            defer {
                strongSelf.call(completion: resultBlock, with: result)
            }
            
            do {
                let models = try strongSelf.fetch(with: request)
                result = .success(models)
            }
            catch {
                result = .error(error)
            }
        }
    }
    
    func fetchController<ModelType>(with request: DataBaseFetchRequest<ModelType>) throws -> DataBaseFetchController<ModelType> {
        
        let models = try fetchResults(with: request)
        let controller = RealmDataBaseFetchController(with: request, and: models)
        
        return DataBaseFetchController<ModelType>(fetchController: controller)
    }
    
    func fetchControllerAsync<ModelType>(with request: DataBaseFetchRequest<ModelType>, resultBlock: DataBaseControllerFetchResultBlock<ModelType>?) {
        
        let backGroundReadQueue = RealmDataBaseManager.backGroundReadQueue
        backGroundReadQueue.addOperation { [weak self] in
            
            guard let strongSelf = self else { return }
            
            var result: RealmDataBaseControllerFetchResult
            
            defer {
                strongSelf.call(completion: resultBlock, with: request, and: result)
            }
            
            do {
                let models = try strongSelf.fetchResults(with: request)
                result = .success(ThreadSafeReference(to: models))
            }
            catch {
                result = .error(error)
            }
        }
    }
    
    fileprivate func fetchResults<ModelType>(with request: DataBaseFetchRequest<ModelType>) throws -> Results<Object> {
        
        try checkModelCanBePersistedInRealm(modelType: ModelType.self)
        
        return try autoreleasepool {
            
            let db = try createRealm()
            let realmType = request.modelType.PersistableObjectType.self as! Object.Type
            
            db.refresh()
            var models = db.objects(realmType)
            
            if let predicate = request.filterPredicate {
                models = models.filter(predicate)
            }
            
            if !request.sortDescriptors.isEmpty {
                models = models.sorted(by: request.sortDescriptors.map { SortDescriptor(keyPath: $0.sortKeyPath, ascending: $0.sortAscending) })
            }
            
            return models
        }
    }
    
    func delete<ModelType: Persistable>(models: [ModelType]) throws {
        
        guard !models.isEmpty else { return }
        
        try checkModelCanBePersistedInRealmAndResolved(modelType: ModelType.self)
        
        try autoreleasepool {
            
            let db = try createRealm()
            let realmModels = models.toPersistableObjectsArray() as! [Object]
            
            try remove(models: realmModels, from: db)
        }
    }
    
    func deleteAsync<ModelType: Persistable>(models: [ModelType], resultBlock: DataBaseOperationResultBlock?) {
        
        guard !models.isEmpty else { return }
        
        do {
            try checkModelCanBePersistedInRealmAndResolved(modelType: ModelType.self)
        }
        catch {
            call(completion: resultBlock, with: .error(error))
        }
        
        //Создаем persistable objects на том же потоке, чтобы не захватывать на другой поток models
        let realmModels = models.toPersistableObjectsArray() as! [Object]

        let backGroundDeleteQueue = RealmDataBaseManager.backGroundDeleteQueue
        backGroundDeleteQueue.addOperation { [weak self] in
            
            guard let strongSelf = self else { return }
            
            var result: DataBaseOperationResult
            
            defer {
                strongSelf.call(completion: resultBlock, with: result)
            }
            
            do {
                
                try autoreleasepool {
                    let db = try strongSelf.createRealm()
                    try strongSelf.remove(models: realmModels, from: db)
                }
                
                result = .success
            }
            catch {
                result = .error(error)
            }
        }
    }
    
    func updateAllModelsAsync<ModelType: Persistable>(of modelType: ModelType.Type,
                              with value: Any,
                              for key: String,
                              resultBlock: DataBaseOperationResultBlock?) {
        updateAllModelsAsync(of: modelType, filteredWith: nil, with: value, for: key, resultBlock: resultBlock)
    }
    
    func updateAllModelsAsync<ModelType: Persistable>(of modelType: ModelType.Type,
                              where filterPredicate: NSPredicate,
                              with value: Any,
                              for key: String,
                              resultBlock: DataBaseOperationResultBlock?) {
        updateAllModelsAsync(of: modelType, filteredWith: filterPredicate, with: value, for: key, resultBlock: resultBlock)
    }
    
    fileprivate func updateAllModelsAsync<ModelType: Persistable>(of modelType: ModelType.Type,
                                          filteredWith filterPredicate: NSPredicate?,
                                          with value: Any,
                                          for key: String,
                                          resultBlock: DataBaseOperationResultBlock?) {
        
        let backGroundWriteQueue = RealmDataBaseManager.backGroundWriteQueue
        backGroundWriteQueue.addOperation { [weak self] in
            
            guard let strongSelf = self else { return }
            
            var result: DataBaseOperationResult
            
            defer {
                strongSelf.call(completion: resultBlock, with: result)
            }
            
            do {
                
                try strongSelf.checkModelCanBePersistedInRealm(modelType: ModelType.self)
                
                try autoreleasepool {
                    
                    let db = try strongSelf.createRealm()
                    let realmType = ModelType.PersistableObjectType.self as! Object.Type
                    var realmModels = db.objects(realmType)
                    if let predicate = filterPredicate {
                        realmModels = realmModels.filter(predicate)
                    }
                    
                    try db.write({
                        realmModels.forEach{ strongSelf.cascadeDelete(valueForKey: key, for: $0, in: db) }
                        realmModels.setValue(value, forKey: key)
                    })
                }
                
                result = .success
            }
            catch {
                result = .error(error)
            }
        }
    }
    
    func deleteAllModelsAsync<ModelType: Persistable>(of modelType: ModelType.Type,
                              resultBlock: DataBaseOperationResultBlock?) {
        deleteAllModelsAsync(of: modelType, filteredWith: nil, resultBlock: resultBlock)
    }
    
    func deleteAllModelsAsync<ModelType: Persistable>(of modelType: ModelType.Type,
                              where filterPredicate: NSPredicate,
                              resultBlock: DataBaseOperationResultBlock?) {
        deleteAllModelsAsync(of: modelType, filteredWith: filterPredicate, resultBlock: resultBlock)
    }
    
    fileprivate func deleteAllModelsAsync<ModelType: Persistable>(of modelType: ModelType.Type,
                                          filteredWith filterPredicate: NSPredicate?,
                                          resultBlock: DataBaseOperationResultBlock?) {
        
        let backGroundDeleteQueue = RealmDataBaseManager.backGroundDeleteQueue
        backGroundDeleteQueue.addOperation { [weak self] in
            
            guard let strongSelf = self else { return }
            
            var result: DataBaseOperationResult
            
            defer {
                strongSelf.call(completion: resultBlock, with: result)
            }
            
            do {
                
                try strongSelf.checkModelCanBePersistedInRealm(modelType: ModelType.self)
                
                try autoreleasepool {
                    
                    let db = try strongSelf.createRealm()
                    let realmType = ModelType.PersistableObjectType.self as! Object.Type
                    var realmModels = db.objects(realmType)
                    if let predicate = filterPredicate {
                        realmModels = realmModels.filter(predicate)
                    }
                    
                    try db.write({
                        realmModels.forEach{ strongSelf.cascadeDelete($0, with: db) }
                    })
                }
                
                result = .success
            }
            catch {
                result = .error(error)
            }
        }
    }
    
    func performAsyncAfterAllSavesFinished(work: @escaping () -> ()) {
        
        let backGroundWriteQueue = RealmDataBaseManager.backGroundWriteQueue
        let backGroundWriteSyncedWorkQueue = RealmDataBaseManager.backGroundWriteSyncedWorkQueue
        
        let operation = BlockOperation(block: work)
        backGroundWriteQueue.operations.forEach { operation.addDependency($0) }
        
        backGroundWriteSyncedWorkQueue.addOperation(operation)
    }
    
    //MARK: - Helper методы
    
    /// Логировать результат выполнения операции в БД
    ///
    /// - Parameter dataBaseOperationResult: результат выполнения операции в БД
    fileprivate func log(dataBaseOperationResult: DataBaseOperationResult) {
        
        guard shouldLogAsyncOperationsErrors else { return }
        
        switch dataBaseOperationResult {
        case .error(let error):
            print(error)
        default:
            return
        }
    }
    
    /// Логировать результат выполнения выборки из БД
    ///
    /// - Parameter dataBaseFetchResult: результат выполнения выборки из БД
    fileprivate func log<ModelType>(dataBaseFetchResult: DataBaseFetchResult<ModelType>) {
        
        guard shouldLogAsyncOperationsErrors else { return }
        
        switch dataBaseFetchResult {
        case .error(let error):
            print(error)
        default:
            return
        }
    }
    
    /// Логировать результат создания контроллера выборки из БД
    ///
    /// - Parameter dataBaseControllerFetchResult: результат создания контроллера выборки из БД
    fileprivate func log<ModelType>(dataBaseControllerFetchResult: DataBaseControllerFetchResult<ModelType>) {
        
        guard shouldLogAsyncOperationsErrors else { return }
        
        switch dataBaseControllerFetchResult {
        case .error(let error):
            print(error)
        default:
            return
        }
    }
    
    /// Вызвать обработчик завершения операции в БД
    ///
    /// - Parameters:
    ///   - completion: обработчик завершения операции в БД
    ///   - result: результат выполнения операции в БД
    fileprivate func call(completion: DataBaseOperationResultBlock?, with result: DataBaseOperationResult) {
        
        log(dataBaseOperationResult: result)
        
        guard let resultBlock = completion else { return }
        DispatchQueue.main.async(execute: {
            resultBlock(result)
        })
    }
    
    /// Вызвать обработчик завершения выборки из БД
    ///
    /// - Parameters:
    ///   - completion: обработчик завершения выборки из БД
    ///   - result: результат выполнения выборки из БД
    fileprivate func call<ModelType>(completion: DataBaseFetchResultBlock<ModelType>?, with result: DataBaseFetchResult<ModelType>) {
        
        log(dataBaseFetchResult: result)
        
        guard let resultBlock = completion else { return }
        DispatchQueue.main.async(execute: {
            resultBlock(result)
        })
    }
    
    /// Вызвать обработчик получения контроллера выборки из БД
    ///
    /// - Parameters:
    ///   - completion: обработчик получения контроллера выборки из БД
    ///   - request: исходный запрос на выборку
    ///   - result: результат выполнения выборки из БД для создания контроллера выборки из БД
    fileprivate func call<ModelType>(completion: DataBaseControllerFetchResultBlock<ModelType>?,
                          with request: DataBaseFetchRequest<ModelType>,
                          and result: RealmDataBaseControllerFetchResult) {
        
        guard let resultBlock = completion else { return }
        
        DispatchQueue.main.async(execute: { [weak self] in
            
            var dataBaseResult: DataBaseControllerFetchResult<ModelType>
            
            // RealmDataBaseFetchController создается на главном потоке, чтобы блок обновления вызывался тоже на нем
            // для асинхронного вызова мы это гарантируем т.к. гарантируем вызов всех обработчиков на главном потоке
            
            switch result {
                
            case .success(let modelsReference):
                
                do {
                    
                    guard let strongSelf = self else {
                        throw RealmDataManagerErrors.referenceToFetchResultCannotBeResolved
                    }
                    
                    let realm = try strongSelf.createRealm()
                    
                    guard let models = realm.resolve(modelsReference) else {
                        throw RealmDataManagerErrors.referenceToFetchResultCannotBeResolved
                    }
                    
                    let controller = RealmDataBaseFetchController(with: request, and: models)
                    dataBaseResult = .success(DataBaseFetchController<ModelType>(fetchController: controller))
                }
                catch {
                    dataBaseResult = .error(RealmDataManagerErrors.referenceToFetchResultCannotBeResolved)
                }
                
            case .error(let error):
                dataBaseResult = .error(error)
            }
            
            resultBlock(dataBaseResult)
        })
    }
    
    /// Проверяем, что данный тип моделей может быть сохранен в Realm
    ///
    /// - Parameter modelType: тип модели
    /// - Throws: возвращает ошибку Error
    @discardableResult fileprivate func checkModelCanBePersistedInRealm<ModelType: Persistable>(modelType: ModelType.Type) throws -> Bool {
        
        guard modelType.PersistableObjectType.self is Object.Type else {
            throw RealmDataManagerErrors.typeCantBePersistedInRealm(type: modelType.PersistableObjectType.self)
        }
        
        return true
    }
    
    /// Проверяем, что данный тип моделей может быть сохранен в Realm и имеет первичный ключ
    ///
    /// - Parameter modelType: тип модели
    /// - Throws: возвращает ошибку Error
    @discardableResult fileprivate func checkModelCanBePersistedInRealmAndResolved<ModelType: Persistable>(modelType: ModelType.Type) throws -> Bool {
        
        guard let modelObjectType = modelType.PersistableObjectType.self as? Object.Type else {
            throw RealmDataManagerErrors.typeCantBePersistedInRealm(type: modelType.PersistableObjectType.self)
        }
        
        guard modelObjectType.primaryKey() != nil else {
            throw RealmDataManagerErrors.objectCantBeResolved(objectType: modelObjectType)
        }
        
        return true
    }
    
    /// Получение экземпляра(слепка) базы данных
    ///
    /// - Returns: экземпляр(слепок) базы данных Realm
    /// - Throws: возвращает ошибку Error
    fileprivate func createRealm() throws -> Realm {
        
        let manager = RealmDataBaseMigrationManager.getMigrationManager()
        let configuration = shouldUseEncryptedStorage ? manager.obtainEncryptedConfiguration() : manager.obtainSharedConfiguration()
        let realm = try Realm(configuration: configuration)
        
        return realm
    }
    
    /// Получение моделей из экземпляра базы данных Realm, эквивалентных переданным моделям
    /// Эквивалентность определяется совпадением типа модели и значения в primaryKey
    ///
    /// - Parameters:
    ///   - models: набор исходных моделей
    ///   - realm: экземпляр(слепок) базы данных Realm
    /// - Returns: эквивалентные модели
    fileprivate func obtainModels(equivalentTo models: [Object], in realm: Realm) -> [Object] {
        
        var objectsFromThisRealm: [Object] = []
        
        for model in models {
            
            let modelType = type(of: model)
            
            guard modelType.primaryKey() != nil else { continue }
            
            let primaryKeyValue = model.value(forKeyPath: modelType.primaryKey()!)
            
            if let objectFromThisRealm = realm.object(ofType: modelType, forPrimaryKey: primaryKeyValue) {
                objectsFromThisRealm.append(objectFromThisRealm)
            }
        }
        
        return objectsFromThisRealm
    }
    
    /// Запись моделей в базу данных Realm
    ///
    /// - Parameters:
    ///   - models: модели, которые нужно записать в Realm
    ///   - realm: экземпляр(слепок) базы данных Realm
    ///   - canUpdate: можно ли попытаться обновить модели, а не сохранять как новые
    /// - Throws: возвращает ошибку Error
    fileprivate func write(models: [Object], to realm: Realm, canUpdate: Bool) throws {
        
        // если модели обновляемы, то пытаемся найти текущие значения
        // если нашли, то удаляем их свойства перед обновлением
        let objectsFromThisRealm = canUpdate ? obtainModels(equivalentTo: models, in: realm) : []
        
        try realm.write({
            objectsFromThisRealm.forEach{ cascadeDelete($0, with: realm, onlyProperties: true) }
            realm.add(models, update: canUpdate)
        })
    }
    
    /// Удаление моделей из базы данных Realm
    ///
    /// - Parameters:
    ///   - models: массив моделей, которые нужно удалить из Realm
    ///   - realm: экземпляр(слепок) базы данных Realm
    /// - Throws: возвращает ошибку Error
    fileprivate func remove(models: [Object], from realm: Realm) throws {
        
        let objectsFromThisRealm = obtainModels(equivalentTo: models, in: realm)
        
        try realm.write({
            objectsFromThisRealm.forEach{ cascadeDelete($0, with: realm) }
        })
    }
    
    /// Каскадное удаление моделей из Realm
    ///
    /// - warning: Вызывать только во write транзакции
    /// - Parameters:
    ///   - object: объект, который нужно удалить из Realm
    ///   - realm: экземпляр(слепок) базы данных Realm
    ///   - onlyProperties: удалить только свойства модели (например, перед ее перезаписью)
    fileprivate func cascadeDelete(_ object: Object, with realm: Realm, onlyProperties: Bool = false) {
        
        guard !object.isInvalidated else { return }
        
        for property in object.objectSchema.properties {
            cascadeDelete(valueForKey: property.name, for: object, in: realm)
        }
        
        if !onlyProperties {
            realm.delete(object)
        }
    }
    
    /// Каскадное удаление свойства по key path в модели Realm
    ///
    /// - warning: Вызывать только во write транзакции
    /// - Parameters:
    ///   - key: key path свойства модели
    ///   - object: модель
    ///   - realm: экземпляр(слепок) базы данных Realm
    fileprivate func cascadeDelete(valueForKey key: String, for object: Object, in realm: Realm) {
        
        guard let value = object.value(forKey: key) else { return }
        
        if let object = value as? Object {
            cascadeDelete(object, with: realm)
        }
        
        if let list = value as? List<Object> {
            list.forEach { cascadeDelete($0, with: realm) }
        }
    }
}

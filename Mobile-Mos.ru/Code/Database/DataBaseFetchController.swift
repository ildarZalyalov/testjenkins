//
//  DataBaseFetchController.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 25.08.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Информация об изменении выборки из БД
struct DataBaseFetchUpdate {
    
    /// Изначальное обновление (выборка не была изменена, идет обработка текущих значений)
    static let initalUpdate = DataBaseFetchUpdate(deletions: [], insertions: [], modifications: [])
    
    /// Индексы моделей, которые были удалены
    var deletions: [Int]
    
    /// Индексы моделей, которые были добавлены
    var insertions: [Int]
    
    /// Индексы моделей, которые были изменены
    var modifications: [Int]
    
    /// Является ли обновление изначальным обновлением
    var isInitialUpdate: Bool {
        return deletions.isEmpty && insertions.isEmpty && modifications.isEmpty
    }
 }

/// Обработчик изменения выборки из БД - принимает информацию об изменении выборки из БД
typealias DataBaseFetchUpdateBlock = (DataBaseFetchUpdate) -> ()

/// Протокол конкретной реализации контроллера выборки из БД
/// Реализуется для каждой конкретной БД
protocol DataBaseFetchControllerProtocol: class {
    
    /// Тип моделей
    associatedtype Model: Persistable
    
    /// Запрос на выборку из БД
    var fetchRequest: DataBaseFetchRequest<Model> { get }
    
    /// Результаты выборки из БД (обновляемые)
    var results: [Model] { get }
    
    /// Обработчик, который вызывается при изменении выборки из БД
    var onUpdate: DataBaseFetchUpdateBlock? { get set }
}

/// Общий интерфейс контроллера выборки из БД
/// Позволяет отслеживать изменения выборки из БД - появление новых элементов, подходящих под условия выборки, удаление старых, изменение моделей из выборки
/// Представляет из себя "оболочку" для конкретной реализации контроллера выборки из БД
class DataBaseFetchController<ModelType: Persistable>: DataBaseFetchControllerProtocol {
    
    fileprivate let controller: Any
    
    fileprivate let obtainOriginalFetchRequest: () -> DataBaseFetchRequest<ModelType>
    fileprivate let obtainResults: () -> [ModelType]
    
    //MARK: - DataBaseFetchControllerProtocol
    
    typealias Model = ModelType
    
    var fetchRequest: DataBaseFetchRequest<ModelType> {
        return obtainOriginalFetchRequest()
    }
    
    var results: [ModelType] {
        return obtainResults()
    }
    
    var onUpdate: DataBaseFetchUpdateBlock?

    //MARK: - Конструктор
    
    init<AnyDataBaseFetchController: DataBaseFetchControllerProtocol>(fetchController: AnyDataBaseFetchController)
        where AnyDataBaseFetchController.Model == ModelType {
        
        self.controller = fetchController
        
        obtainOriginalFetchRequest = { return fetchController.fetchRequest }
        obtainResults = { return fetchController.results }
        fetchController.onUpdate = { [weak self] update in self?.onUpdate?(update) }
    }
}

//
//  Persistable.swift
//  Portable
//
//  Created by Ivan Erasov on 07.06.17.
//  Copyright © 2017. All rights reserved.
//

import Foundation

/// Общий протокол для всех моделей, которые можно сохранить в БД
protocol Persistable: BaseModel {
    
    /// Связанный тип объектов, которые могут быть сохранены в БД (например наследники NSManagedObject для Core Data)
    associatedtype PersistableObjectType
    
    init()
    
    /// Создать объект для сохранения в БД
    func toPersistableObject() -> PersistableObjectType
    
    /// Обновить значения полей из сохраненного в БД объекта
    ///
    /// - Parameter object: объект из БД
    mutating func updateFromPersistableObject(object: PersistableObjectType)
}

extension Persistable {
    
    /// Создание сохраняемой модели из сохраненного в БД объекта
    ///
    /// - Parameter object: объект из БД
    init(with object: PersistableObjectType) {
        self.init()
        updateFromPersistableObject(object: object)
    }
}

extension Array where Element: Persistable {
    
    /// Создать набор объектов для сохранения в БД
    func toPersistableObjectsArray() -> [Element.PersistableObjectType] {
        return map { $0.toPersistableObject() }
    }
}

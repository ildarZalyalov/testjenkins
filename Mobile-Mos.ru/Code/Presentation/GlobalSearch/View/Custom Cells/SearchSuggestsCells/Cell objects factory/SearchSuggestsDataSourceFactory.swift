//
//  SearchSuggestsDataSourceFactory.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 01.02.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

struct SearchSuggestsDataSourceConfiguration {
    
    var dataSource: TableViewDataSource
    
    var selectionItems: [String : SearchSuggest]
}

protocol SearchSuggestsDataSourceFactory {
    
    func buildDataSourceConfiguration(from models: [SearchSuggest], cachedSearchStrings: [SearchTopWords], isTopWords: Bool, searchString: String, headerDelegate: GlobalSearchSectionHeaderDelegate?) -> SearchSuggestsDataSourceConfiguration
    
    func clearSuggestsHistory(in configuration: inout SearchSuggestsDataSourceConfiguration) -> [TableViewDataSourceUpdate]
}

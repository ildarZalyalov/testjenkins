//
//  SearchSuggestsDataSourceFactoryImplementation.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 01.02.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

class SearchSuggestsDataSourceFactoryImplementation: SearchSuggestsDataSourceFactory {
    
    fileprivate let historySectionIndexPath = IndexPath(row: 0, section: 0)
    fileprivate let historySectionIdentifier = "-1"
    
    //MARK: - Приватные методы
    
    fileprivate func addHistorySection(from historyArray: [SearchTopWords],
                                       with selectionItemsSuggests: inout [String: SearchSuggest],
                                       to dataStructure: TableViewDataSourceStructure,
                                       delegate: GlobalSearchSectionHeaderDelegate?) {
        
        let headerCellObject = GlobalSearchSectionHeaderCellObject(headerTitle: StringsHelper.historySectionName, cellDelegate: delegate, isHistorySection: true)
        dataStructure.appendCellObject(headerCellObject)
        
        for (i,item) in historyArray.enumerated() {
            
            let cellObject = SearchSuggestCellObject(itemId: String("\(i)" + historySectionIdentifier + item.title), title: item.title)
            selectionItemsSuggests[cellObject.itemId] = item
            
            dataStructure.appendCellObject(cellObject)
        }
    }
    
    //MARK: - SearchSuggestsDataSourceFactory
    
    func buildDataSourceConfiguration(from models: [SearchSuggest], cachedSearchStrings: [SearchTopWords], isTopWords: Bool, searchString: String, headerDelegate: GlobalSearchSectionHeaderDelegate?) -> SearchSuggestsDataSourceConfiguration {
        
        let dataStructure = TableViewDataSourceStructure()
        var selectionItemsSuggests = [String: SearchSuggest]()
        
        if !cachedSearchStrings.isEmpty {
            addHistorySection(from: cachedSearchStrings, with: &selectionItemsSuggests, to: dataStructure, delegate: headerDelegate)
        }
        
        var headerTitle = ""
        var cellObjects = [CellObject]()
        
        if isTopWords {
            headerTitle = StringsHelper.popularWords
            let headerCellObject = GlobalSearchSectionHeaderCellObject(headerTitle: headerTitle, cellDelegate: nil, isHistorySection: false)
            cellObjects.append(headerCellObject)
        }
        
        for (i,item) in models.enumerated() {
            
            let cellObject = SearchSuggestCellObject(itemId: String("\(i)" + item.title), title: item.title)
            selectionItemsSuggests[cellObject.itemId] = item
            
            cellObjects.append(cellObject)
        }
        
        dataStructure.appendSection(with: cellObjects)
        
        let dataSource = TableViewDataSource(with: dataStructure)
 
        return SearchSuggestsDataSourceConfiguration(dataSource: dataSource, selectionItems: selectionItemsSuggests)
    }
    
    func clearSuggestsHistory(in configuration: inout SearchSuggestsDataSourceConfiguration) -> [TableViewDataSourceUpdate] {
        
        configuration.dataSource.dataStructure.enumerateCellObjectsUsing { cellObject, indexPath in
            guard indexPath.section == historySectionIndexPath.section else { return }
            guard let suggestCellObject = cellObject as? SearchSuggestCellObject else { return }
            configuration.selectionItems[suggestCellObject.itemId] = nil
        }
        configuration.dataSource.dataStructure.removeSections(at: [historySectionIndexPath])
        
        return [.deleteSections(sections: [historySectionIndexPath.section], animation: .fade)]
    }
}

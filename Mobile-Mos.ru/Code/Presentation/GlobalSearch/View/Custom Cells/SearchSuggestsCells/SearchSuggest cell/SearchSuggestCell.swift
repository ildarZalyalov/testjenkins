//
//  SearchSuggestCell.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 01.02.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import UIKit

class SearchSuggestCell: UITableViewCell, ConfigurableView {
    
    //MARK: - ConfigurableView
    
    @IBOutlet weak var titleLabel: UILabel!
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        contentView.alpha = highlighted ? 0.5 : 1
    }
    
    func configure(with object: Any) {
        
        guard let cellObject = object as? SearchSuggestCellObject else {
            return
        }
        
        titleLabel.text = cellObject.title
        titleLabel.textColor = titleLabel.tintColor
    }
}

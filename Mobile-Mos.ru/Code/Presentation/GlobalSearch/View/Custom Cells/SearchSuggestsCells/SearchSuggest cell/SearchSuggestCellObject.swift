//
//  SearchSuggestCellObject.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 01.02.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

struct SearchSuggestCellObject: CellObjectWithId {
    
    var itemId: String
    
    /// Заголовок
    var title: String
}

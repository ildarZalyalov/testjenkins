//
//  ConversationsSearchCellObject.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 19.02.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

struct ConversationsSearchCellObject: CellObjectWithId {
    
    var itemId: String
    var imageUrl: URL?
    var imagePlaceholderName: String?
    var title: String
    var subtitle: String
    var isLastObject: Bool
}

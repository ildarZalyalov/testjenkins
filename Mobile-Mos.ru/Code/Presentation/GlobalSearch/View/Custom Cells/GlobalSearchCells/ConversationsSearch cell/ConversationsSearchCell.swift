//
//  ConversationsSearchCell.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 19.02.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import UIKit

class ConversationsSearchCell: UITableViewCell, ConfigurableView {
    
    //MARK: - ConfigurableView
    
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var bottomSeparatorView: UIView!
    
    func configure(with object: Any) {
        
        guard let cellObject = object as? ConversationsSearchCellObject else {
            return
        }
        
        titleLabel.text = cellObject.title
        subtitleLabel.text = cellObject.subtitle
        
        var placeholderImage: UIImage?
        if let placeholderName = cellObject.imagePlaceholderName {
            placeholderImage = UIImage(named: placeholderName)
        }
        
        if placeholderImage == nil {
            avatarImageView.startLoadingAnimation()
        }
        else {
            avatarImageView.stopLoadingAnimation()
        }
        
        bottomSeparatorView.isHidden = !cellObject.isLastObject
        
        avatarImageView.loadAndDisplayImage(from: cellObject.imageUrl,
                                            placeholder: placeholderImage,
                                            with: nil) { [weak self] result in
                                                
                                                switch result {
                                                case .success:
                                                    self?.avatarImageView.stopLoadingAnimation()
                                                case .failure:
                                                    return
                                                }
        }
        
    }
}

//
//  GlobalSearchDataSourceFactory.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 02.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Перечисление с id секций фабрики
///
/// - globalSearchServicesTagId: id ячейки с тэгами сервисов
enum GlobalSearchDataSourceFactorySectionsId: String {
    case globalSearchServicesTagId = "0"
}

/// Значения в результатах поиска по идентификатору
struct GlobalSearchSelectionItems {
    
    /// Найденные новости по идентификатору
    let newsSearch: [String : NewsSearchModel]
    
    /// Найденные диалоги по идентификатору
    let convesations: [String : Conversation]
}

typealias GlobalSearchDataSourceConfiguration = (dataSource: TableViewDataSource, selectionItems: GlobalSearchSelectionItems)

protocol GlobalSearchDataSourceFactory {
    
    func buildDataSourceConfiguration(from model: GlobalSearchModel, conversations: [Conversation]) -> GlobalSearchDataSourceConfiguration
}

//
//  GlobalSearchDataSourceFactoryImplementation.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 02.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class GlobalSearchDataSourceFactoryImplementation: GlobalSearchDataSourceFactory {
    
    var nounCountStringFormatter: NounCountStringFormatter!
    var dateFormatter: DateStringFormatter!
    
    //MARK: - GlobalSearchDataSourceFactory
    
    func buildDataSourceConfiguration(from model: GlobalSearchModel, conversations: [Conversation]) -> GlobalSearchDataSourceConfiguration {
        
        typealias SectionIds = GlobalSearchDataSourceFactorySectionsId
        
        let dataStructure = TableViewDataSourceStructure()
        
        var selectionItemsConversations = [String : Conversation]()
        var selectionItemsNews = [String : NewsSearchModel]()
        
        if !model.conversations.isEmpty {
            appendConversationsSection(from: model, conversations: conversations, to: dataStructure, selectionItemsConversations: &selectionItemsConversations)
        }
        
        if !model.news.isEmpty {
            appendNewsSection(from: model, news: model.news, to: dataStructure, selectionItemsConversations: &selectionItemsNews)
        }
        
        let dataSource = TableViewDataSource(with: dataStructure)
        let selectionItems = GlobalSearchSelectionItems(newsSearch: selectionItemsNews, convesations: selectionItemsConversations)
          
        return (dataSource, selectionItems)
    }
    
    //MARK: - Приватные методы
    
    fileprivate func appendNewsSection(from model: GlobalSearchModel, news: [NewsSearchModel], to dataStructure: TableViewDataSourceStructure, selectionItemsConversations: inout [String : NewsSearchModel]) {
        
        for (i, news) in model.news.enumerated() {
            
            var dateString: String = ""
            
            switch news.type {
            case .calendar:
                dateString = dateFormatter.relativeDateIntervalString(from: news.dateFrom, to: news.date)
            case .news:
                dateString = dateFormatter.relativeDateString(from: news.date)
            }
            
            let cellObject = NewsSearchCellObject(itemId: "\(i)", imageUrlString: news.imageStringUrl, title: news.title, dateString: dateString, isFirstCell: i == 0)
            
            dataStructure.appendCellObject(cellObject)
            selectionItemsConversations[cellObject.itemId] = news
        }
    }
    
    fileprivate func appendConversationsSection(from model: GlobalSearchModel, conversations: [Conversation], to dataStructure: TableViewDataSourceStructure, selectionItemsConversations: inout [String : Conversation]) {

        
        for (i,conversation) in model.conversations.enumerated() {
            
            let cellObject = ConversationsSearchCellObject(itemId: conversation.itemId,
                                                           imageUrl: conversation.avatarIconUrl,
                                                           imagePlaceholderName: conversation.avatarPlaceholderIconName,
                                                           title: conversation.title,
                                                           subtitle: conversation.descriptionText,
                                                           isLastObject: i == (conversations.count - 1))
            
            dataStructure.appendCellObject(cellObject)
            selectionItemsConversations[cellObject.itemId] = conversation
        }
    }
}

//
//  NewsSearchCell.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 26.01.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import UIKit

class NewsSearchCell: UITableViewCell, ConfigurableView {
    
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!

    @IBOutlet weak var innerContentViewTopConstraint: NSLayoutConstraint!
    
    let defaultTopConstant: CGFloat = 8
    let firstCellTopConstant: CGFloat = 40
    let titleLabelLineHeightMultiple: CGFloat = 1.33
    
    override func prepareForReuse() {
        
        super.prepareForReuse()
        
        avatarImageView.image = nil
        titleLabel.text = nil
        dateLabel.text = nil
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        contentView.alpha = highlighted ? 0.5 : 1
    }
    
    //MARK: - ConfigurableView
    
    func configure(with object: Any) {
        
        guard let cellObject = object as? NewsSearchCellObject else {
            return
        }
        
        avatarImageView.image = nil
        avatarImageView.loadAndDisplayImage(from: URL(string: cellObject.imageUrlString))
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineHeightMultiple = titleLabelLineHeightMultiple
        paragraphStyle.alignment = .left
        paragraphStyle.lineBreakMode = .byTruncatingTail
        
        let attributes: [NSAttributedStringKey : Any] = [NSAttributedStringKey.font : titleLabel.font, NSAttributedStringKey.paragraphStyle : paragraphStyle]
        let titleString = NSAttributedString(string: cellObject.title, attributes: attributes)
        
        titleLabel.attributedText = titleString
        dateLabel.text = cellObject.dateString
        
        if cellObject.isFirstCell {
            innerContentViewTopConstraint.constant = firstCellTopConstant
        }
        else {
            innerContentViewTopConstraint.constant = defaultTopConstant
        }
    }
}

//
//  NewsSearchCellObject.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 26.01.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

struct NewsSearchCellObject: CellObjectWithId {
    
    var itemId: String
    var imageUrlString: String
    var title: String
    var dateString: String
    var isFirstCell: Bool 
}

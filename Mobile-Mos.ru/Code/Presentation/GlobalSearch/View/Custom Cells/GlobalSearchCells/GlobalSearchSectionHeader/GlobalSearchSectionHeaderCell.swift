//
//  GlobalSearchSectionHeaderCell.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 05.12.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol GlobalSearchSectionHeaderDelegate: class {
    func didPressClear()
}

class GlobalSearchSectionHeaderCell: UITableViewCell, ConfigurableView {
    
    @IBOutlet weak var headerTitleLabel: UILabel!
    @IBOutlet weak var clearButton: UIButton!
    weak var delegate: GlobalSearchSectionHeaderDelegate?
    
    //MARK: - ConfigurableView
    
    func configure(with object: Any) {
        
        guard let headerObject = object as? GlobalSearchSectionHeaderCellObject else {
            return
        }
        
        headerTitleLabel.text = headerObject.headerTitle
        delegate = headerObject.cellDelegate
        clearButton.isHidden = !headerObject.isHistorySection
    }
    
    @IBAction func clearButtonPressed(_ sender: Any) {
        delegate?.didPressClear()
    }
    
}

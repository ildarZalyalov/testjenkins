//
//  GlobalSearchViewInput.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 29.09.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol GlobalSearchViewInput: class {
    
    var currentSearchString: String? { get }
    
    func showActivityIndicator()
    
    func hideActivityIndicator()
    
    func displaySuggests(with dataSource: TableViewDataSource, and updates: [TableViewDataSourceUpdate])
    
    func displayGlobalSearchResult(with dataSource: TableViewDataSource)
    
    func displayTextInTextField(_ text: String)
    
    func displayEmptyContentView()
}

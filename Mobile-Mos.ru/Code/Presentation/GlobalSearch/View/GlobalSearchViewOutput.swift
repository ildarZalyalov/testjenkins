//
//  GlobalSearchViewOutput.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 29.09.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol GlobalSearchViewOutput: class {
    
    func setupInitialState()
    
    func didRequestSearch(with searchString: String)
    
    func didRequestSearchSuggests(with searchString: String)
    
    func didClearSearchString()
    
    func didSelectSearchResult(with itemId: String)
    
    func didScrollToLastNews()
    
    func didNotScrollToLastNews()
}

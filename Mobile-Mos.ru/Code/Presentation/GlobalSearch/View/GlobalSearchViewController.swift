//
//  GlobalSearchViewController.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 29.09.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import UIKit

class GlobalSearchViewController: BaseViewController, GlobalSearchViewInput, NavigationBarCustomizingController, UITableViewDelegate, UITextFieldDelegate {
    
    var output: GlobalSearchViewOutput!
    var notificationCenter: NotificationCenter!
    
    var tableViewDataDisplayManager: TableViewDataSource!
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchField: UITextField!
    @IBOutlet weak var emptyContentView: UIView!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    let searchRequestTimeout: TimeInterval = 0.5
    lazy var searchRequestPerformer: DelayedSelectorPerformer = {
        return DelayedSelectorPerformer(target: self, selector: #selector(beginSearching(with:)))
    }()
    
    let suggestsResultsEstimatedCellHeight: CGFloat = 55
    let newsResultsEstimatedCellHeight: CGFloat = 200
    var didScrollToEnd = true
    var lastNewsRowIndex = 0
    
    //MARK: - Методы
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        output.setupInitialState()
        
        hideActivityIndicator()
        setupTableView()
        
        searchField.addTarget(self, action: #selector(searchFieldTextDidChange(_:)), for: .editingChanged)
        searchField.addTarget(self, action: #selector(searchFieldTextDidPressSearch(_:)), for: .editingDidEndOnExit)
        
        notificationCenter.addObserver(self,
                                       selector: #selector(keyboardWillShow(with:)),
                                       name: NSNotification.Name.UIKeyboardWillShow,
                                       object: nil)
        
        notificationCenter.addObserver(self,
                                       selector: #selector(keyboardWillHide(with:)),
                                       name: NSNotification.Name.UIKeyboardWillHide,
                                       object: nil)
    }
    
    override func viewSafeAreaInsetsDidChange() {
        resetTableViewInsets()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        super.viewWillDisappear(animated)
        
        guard searchField.isFirstResponder else { return }
        searchField.resignFirstResponder()
    }
    
    deinit {
        notificationCenter.removeObserver(self)
        output?.didNotScrollToLastNews()
    }
    
    override var shouldAutorotate: Bool {
        // для дочерних контроллеров, которые могут поменять ориентацию
        return true
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return .portrait
    }
    
    //MARK: - Хелперы
    
    func setupTableView() {
        
        tableView.estimatedRowHeight = 100
        tableView.assignEmptyTableFooterToHideEmptyCellsSeparators()

        tableView.registerCellNib(for: ConversationsSearchCellObject.self)
        tableView.registerCellNib(for: NewsSearchCellObject.self)
        tableView.registerCellNib(for: SearchSuggestCellObject.self)
        
        let backgroundView = UIView()
        backgroundView.backgroundColor = view.backgroundColor
        tableView.backgroundView = backgroundView
    }
    
    func scrollTableViewToTop() {
        
        tableView.scrollToTop(animated: false)
        
        tableView.setNeedsLayout()
        tableView.layoutIfNeeded()
    }
    
    func resetTableViewInsets() {
        
        var insets = UIEdgeInsets.zero
        if #available(iOS 11.0, *) {
            insets.bottom = view.safeAreaInsets.bottom
        }
        
        tableView.contentInset = insets
        tableView.scrollIndicatorInsets = insets
    }
    
    @objc
    func beginSearching(with text: String) {
        output.didRequestSearchSuggests(with: text)
    }
    
    //MARK: - NavigationBarCustomizingController
    
    var navigationBarState: NavigationBarState? {
        
        var state = NavigationBarState()
        
        state.isHidden = false
        state.isTranslucent = false
        state.backgroundState = NavigationBarBackgroundState.regular
        state.tintColor = UIColor.black
        state.isShadowHidden = true
        
        let titleTextAttributes = [
            NSAttributedStringKey.font : UIFont(customName: .graphikLCGBold, size: 15) as Any,
            NSAttributedStringKey.foregroundColor : UIColor.black
        ]
        
        let largeTitleTextAttributes = [
            NSAttributedStringKey.font : UIFont(customName: .graphikLCGBold, size: 25) as Any,
            NSAttributedStringKey.foregroundColor : UIColor.black
        ]
        
        state.titleTextAttributes = titleTextAttributes
        state.largeTitleTextAttributes = largeTitleTextAttributes
        
        return state
    }
    
    //MARK: - Нотификации
    
    @objc func keyboardWillShow(with notification: Notification) {
        
        guard let info = notification.userInfo,
            let keyboardEndSize = (info[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size
            else { return }
        
        let insets = UIEdgeInsets(top: 0, left: 0, bottom: keyboardEndSize.height, right: 0)
        tableView.contentInset = insets
        tableView.scrollIndicatorInsets = insets
    }
    
    @objc func keyboardWillHide(with notification: Notification) {
        resetTableViewInsets()
    }
    
    //MARK: - GlobalSearchViewInput
    
    var currentSearchString: String? {
        return searchField.text
    }
    
    func showActivityIndicator() {
        
        tableView.isHidden = true
        
        activityIndicator.isHidden = false
        activityIndicator.startAnimating()
    }
    
    func hideActivityIndicator() {
        
        activityIndicator.stopAnimating()
        activityIndicator.isHidden = true
        
        tableView.isHidden = false
    }
    
    func displaySuggests(with dataSource: TableViewDataSource, and updates: [TableViewDataSourceUpdate]) {
        
        emptyContentView.isHidden = true
        hideActivityIndicator()
        
        if updates.isEmpty {
            scrollTableViewToTop()
        }
        
        tableView.estimatedRowHeight = suggestsResultsEstimatedCellHeight
        
        tableViewDataDisplayManager = dataSource
        tableViewDataDisplayManager.delegate = self
        tableViewDataDisplayManager.assign(to:tableView)
        
        if updates.isEmpty {
            tableView.reloadData()
        }
        else {
            tableView.apply(updates: updates)
        }
    }
    
    func displayGlobalSearchResult(with dataSource: TableViewDataSource) {
        
        emptyContentView.isHidden = true
        hideActivityIndicator()
        scrollTableViewToTop()
        
        tableView.estimatedRowHeight = newsResultsEstimatedCellHeight
        
        tableViewDataDisplayManager = dataSource
        tableViewDataDisplayManager.delegate = self
        tableViewDataDisplayManager.assign(to:tableView)
        
        didScrollToEnd = false
        lastNewsRowIndex = dataSource.dataStructure.numberOfObjects(at: 0) - 1
        
        tableView.reloadData()
    }
    
    func displayEmptyContentView() {
        emptyContentView.isHidden = false
    }
    
    func displayTextInTextField(_ text: String) {
        searchField.text = text
    }
    
    //MARK: - UITextFieldDelegate
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if range.location == 0 && string == " " {
            return false
        }
        
        if range.location == 0 && string.isEmpty {
            output.didClearSearchString()
        }
        
        return true
    }
    
    @objc
    func searchFieldTextDidPressSearch(_ textField: UITextField) {
        
        if let searchText = textField.text, !searchText.isEmpty {
            searchRequestPerformer.cancelDelayedPerform()
            output.didRequestSearch(with: searchText)
        }
        
        textField.resignFirstResponder()
    }
    
    @objc
    func searchFieldTextDidChange(_ textField: UITextField) {
        
        searchRequestPerformer.cancelDelayedPerform()
        
        guard let searchText = textField.text else { return }
        
        if !searchText.isEmpty {
            searchRequestPerformer.argument = searchText
            searchRequestPerformer.perform(afterDelay: searchRequestTimeout)
        }
        else {
            output.didClearSearchString()
            hideActivityIndicator()
        }
    }
    
    //MARK: - UITableViewDelegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        searchField.resignFirstResponder()
        tableView.deselectRow(at: indexPath, animated: false)
        
        let dataStructure = tableViewDataDisplayManager.dataStructure
        guard let cellObject = dataStructure.cellObject(at: indexPath) as? CellObjectWithId else {
            return
        }
        
        output.didSelectSearchResult(with: cellObject.itemId)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        guard !didScrollToEnd else { return }
        
        if indexPath.row == lastNewsRowIndex {
            output.didScrollToLastNews()
            didScrollToEnd = true
        }
    }
}



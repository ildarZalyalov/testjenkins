//
//  GlobalSearchRouter.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 29.09.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import SafariServices

class GlobalSearchRouter: NSObject, GlobalSearchRouterInput, SFSafariViewControllerDelegate {
    
    weak var transitionHandler: DataTransferModuleController!
    weak var transitionController: UIViewController!
    
    let childSegueIdentifier = "showChat"
    let childMapsSegueIdentifier = "showChatMap"
    var analyticsManager: AnalyticsManager!
    
    //MARK: - GlobalSearchRouterInput
    
    func dismissCurrentModule() {
    }
    
    func showMapChat(with configurationObject: Any) {
        transitionHandler.performSegue(with: childMapsSegueIdentifier, sender: nil) { (configurableController) in
            configurableController.configureModule(with: configurationObject)
        }
    }
    
    func showMapChat() {
        transitionHandler.performSegue(with: childMapsSegueIdentifier, sender: nil, configurationClosure: nil)
    }
    
    func showChat(for conversation: Conversation) {
        transitionHandler.performSegue(with: childSegueIdentifier, sender: nil) { configurableController in
            let configuration = ChatModuleConfiguration(conversation: conversation, message: nil)
            configurableController.configureModule(with: configuration)
        }
    }
    
    func showChat(for conversation: Conversation, with message: ChatMessage) {
        transitionHandler.performSegue(with: childSegueIdentifier, sender: nil) { configurableController in
            let configuration = ChatModuleConfiguration(conversation: conversation, message: message)
            configurableController.configureModule(with: configuration)
        }
    }
    
    func showSafariViewController(with websiteString: String) {
        
        guard let websiteUrl = URL(string: websiteString) else {
            return
        }
        
        let safariController = SFSafariViewController(url: websiteUrl, entersReaderIfAvailable: true)
        safariController.delegate = self
        
        if #available(iOS 10.0, *) {
            safariController.preferredControlTintColor = UIColorPalette.browserControlsDefaultTintColor
        }
        
        transitionController.present(safariController, animated: true, completion: nil)
    }
    
    //MARK: - SFSafariViewControllerDelegate
    
    func safariViewController(_ controller: SFSafariViewController, didCompleteInitialLoad didLoadSuccessfully: Bool) {
        
        guard let news = analyticsManager.selectedNews else { return }
        
        let errorText = didLoadSuccessfully ? AnalyticsConstants.emptyValueName : AnalyticsConstants.notLoadedErrorValue
        let data:GlobalSearchSelectResultEventData = (news.title, news.newsStringUrl, news.type.localizedName, AnalyticsConstants.emptyValueName, errorText)
        
        analyticsManager.sendGlobalSearchEvent(with: .selectSearchResult(withString: AnalyticsConstants.selectSearchResultEventName), requestData: nil, selectedChatData: nil, selectedSearchResultData: data, scrolledToLastNews: nil)
    }
}

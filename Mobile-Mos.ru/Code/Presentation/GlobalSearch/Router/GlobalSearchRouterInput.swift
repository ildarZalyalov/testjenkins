//
//  GlobalSearchRouterInput.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 29.09.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol GlobalSearchRouterInput: class {
    
    func showMapChat(with configurationObject: Any)
    
    func showMapChat()
    
    func showChat(for conversation: Conversation)
    
    func showChat(for conversation: Conversation, with message: ChatMessage)
    
    func showSafariViewController(with websiteString: String)
    
    func dismissCurrentModule()
}

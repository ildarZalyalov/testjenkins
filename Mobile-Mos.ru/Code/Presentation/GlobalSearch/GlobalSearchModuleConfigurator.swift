//
//  GlobalSearchModuleConfigurator.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 29.09.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import UIKit

class GlobalSearchModuleConfigurator: BaseModuleConfigurator {
    
    override func configureModule(for view: UIViewController) {
        
        let viewController = view as! GlobalSearchViewController
        let presenter = GlobalSearchPresenter()
        let interactor = GlobalSearchInteractor()
        let router = GlobalSearchRouter()
        
        let nounCountStringFormatter = NounCountStringFormatterImplementation()
        let globalSearchFactory = GlobalSearchDataSourceFactoryImplementation()
        let searchSuggestsFactory = SearchSuggestsDataSourceFactoryImplementation()
        let dateFormatter = DateStringFormatterImplementation()
        
        dateFormatter.timeZone = moscowTimezone
        
        globalSearchFactory.nounCountStringFormatter = nounCountStringFormatter
        globalSearchFactory.dateFormatter = dateFormatter
        
        let userService = UIApplication.shared.serviceBuilder.getUserService()
        let mapItemsService = UIApplication.shared.serviceBuilder.getMapItemsService()
        let conversationsService = UIApplication.shared.serviceBuilder.getConversationsService()
        let messageHistoryService = UIApplication.shared.serviceBuilder.getChatMessageHistoryService()
        let globalSearchService = UIApplication.shared.serviceBuilder.getGlobalSearchService()
        let chatKeywordsService = UIApplication.shared.serviceBuilder.getChatKeywordsService()
        
        let notificationCenter = NotificationCenter.default
        let analyticsManager = AnalyticsManagerBuilder.getAnalyticsManager()
        
        viewController.output = presenter
        viewController.notificationCenter = notificationCenter
        
        presenter.view = viewController
        presenter.interactor = interactor
        presenter.router = router
        presenter.searchFactory = globalSearchFactory
        presenter.searchSuggestsFactory = searchSuggestsFactory
        
        interactor.output = presenter
        interactor.userService = userService
        interactor.mapItemsService = mapItemsService
        interactor.conversationsService = conversationsService
        interactor.messageHistoryService = messageHistoryService
        interactor.globalSearchService = globalSearchService
        interactor.chatKeywordsService = chatKeywordsService
        interactor.analyticsManager = analyticsManager
        
        router.transitionHandler = viewController
        router.transitionController = viewController
        router.analyticsManager = analyticsManager
    }
}

//
//  GlobalSearchPresenter.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 29.09.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class GlobalSearchPresenter: GlobalSearchViewOutput, GlobalSearchInteractorOutput, GlobalSearchSectionHeaderDelegate {
    
    weak var view: GlobalSearchViewInput!
    var router: GlobalSearchRouterInput!
    var interactor: GlobalSearchInteractorInput!
    
    var searchFactory: GlobalSearchDataSourceFactory!
    var searchSuggestsFactory: SearchSuggestsDataSourceFactory!
    
    var selectionItems: GlobalSearchSelectionItems!
    var suggestsDataSourceConfiguration: SearchSuggestsDataSourceConfiguration!
    
    var globalSearchModel = GlobalSearchModel()
    var lastSearchRequest = ""
    var didScrollToEnd = false
    
    //MARK: - Приватные методы
    
    func showSearchResult(with searchModel: GlobalSearchModel) {
        
        didScrollToEnd = false
        
        let configuration = searchFactory.buildDataSourceConfiguration(from: searchModel, conversations: interactor.conversationsCache)
        selectionItems = configuration.selectionItems
        globalSearchModel = searchModel
        view.displayGlobalSearchResult(with: configuration.dataSource)
    }
    
    func showSuggestsResult(with searchModels: [SearchSuggest], cachedSearchStrings:[SearchTopWords] = [], isTopWords: Bool = false, searchString: String = "") {
        
        suggestsDataSourceConfiguration = searchSuggestsFactory.buildDataSourceConfiguration(from: searchModels, cachedSearchStrings: cachedSearchStrings, isTopWords: isTopWords, searchString: searchString, headerDelegate: self)
        
        view.displaySuggests(with: suggestsDataSourceConfiguration.dataSource, and: [])
    }
    
    //MARK: - GlobalSearchViewOutput
    
    func setupInitialState() {
        
        interactor.obtainSearchTopWords()
        interactor.obtainSearchStringsCache()
        
        interactor.sendEvent(with: .screenLoaded(withString: AnalyticsConstants.screenLoadedEventName), data: nil, selectedChatdata: nil, selectedResultData: nil, scrolledToLastNews: nil)
    }
    
    func didRequestSearch(with searchString: String) {
        
        view.showActivityIndicator()
        
        let searchText = searchString.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        interactor.performSearchRequest(with: searchText)
        
        lastSearchRequest = searchString
        interactor.sendEvent(with: .request(withString: AnalyticsConstants.requestEventName), data: (searchString, AnalyticsConstants.emptyValueName, AnalyticsConstants.emptyValueName), selectedChatdata: nil, selectedResultData: nil, scrolledToLastNews: nil)
        didNotScrollToLastNews()
    }
    
    func didRequestSearchSuggests(with searchString: String) {
        
        let searchText = searchString.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        interactor.performSearchSuggests(with: searchText)
        didNotScrollToLastNews()
    }
    
    func didClearSearchString() {

        interactor.cancelRequest()
        interactor.obtainSearchStringsCache()
    }
    
    func didSelectSearchResult(with itemId: String) {
    
        if let suggest = suggestsDataSourceConfiguration.selectionItems[itemId] {
            
            view.displayTextInTextField(suggest.title)
            view.showActivityIndicator()
            
            interactor.performSearchRequest(with: suggest.title)
            
            lastSearchRequest = suggest.title
            interactor.sendEvent(with: .selectedRequest(withString: AnalyticsConstants.selectFromTopEventName), data: (suggest.title, AnalyticsConstants.emptyValueName, AnalyticsConstants.emptyValueName), selectedChatdata: nil, selectedResultData: nil, scrolledToLastNews: nil)
            didNotScrollToLastNews()
        }
        else if let conversation = selectionItems.convesations[itemId] {
            router.showChat(for: conversation)
            interactor.sendEvent(with: .selectChat(withString: AnalyticsConstants.selectChatEventName), data: nil, selectedChatdata: (lastSearchRequest, conversation.title, AnalyticsConstants.emptyValueName), selectedResultData: nil, scrolledToLastNews: nil)
        }
        else if let news = selectionItems.newsSearch[itemId] {
            interactor.addNewsToAnalytics(news)
            router.showSafariViewController(with: news.newsStringUrl)
        }
    }
    
    func didScrollToLastNews() {
        
        guard !globalSearchModel.isEmpty else { return }
        interactor.sendEvent(with: .scrolledToLastNews(withString: AnalyticsConstants.scrolledToLastNewsEventName), data: nil, selectedChatdata: nil, selectedResultData: nil, scrolledToLastNews: true)
        didScrollToEnd = true
    }
    
    func didNotScrollToLastNews() {
        
        guard !globalSearchModel.isEmpty else { return }
        if !didScrollToEnd {
        interactor.sendEvent(with: .scrolledToLastNews(withString: AnalyticsConstants.scrolledToLastNewsEventName), data: nil, selectedChatdata: nil, selectedResultData: nil, scrolledToLastNews: false)
        }
    }
    
    //MARK: - GlobalSearchInteractorOutput

    func didFinishSearching(with model: GlobalSearchModel) {
        
        showSearchResult(with: model)
        
        if model.isEmpty {
            view.displayEmptyContentView()
        }
    }
    
    func didFinishObtainingSearchSuggests(for searchString: String, with result: SearchSuggestsMultipleResult) {
        switch result {
        case .success(let models):
            showSuggestsResult(with: models, searchString: searchString)
        case .failure(let error):
            print("Error: \(error.localizedDescription)")
        }
    }
    
    func didFinishObtainingSearchTopWords(with result: SearchTopWordsMultipleResult) {
        
        switch result {
        case .success(let models):
            
            let cachedModels = interactor.searchStringsCache
            interactor.searchTopWordsCache = models
            
            showSuggestsResult(with: models, cachedSearchStrings: cachedModels, isTopWords: true)
            
        case .failure(let error):
            print("Error: \(error.localizedDescription)")
        }
    }
    
    func didFinishObtainingCachedSearchTopWords(with result: SearchTopWordsMultipleResult) {
        
        switch result {
        case .success(let models):
            
            let cachedModels = interactor.searchTopWordsCache
            interactor.searchStringsCache = models
            
            showSuggestsResult(with: cachedModels, cachedSearchStrings: models, isTopWords: true)
            
        case .failure(let error):
            print("Error: \(error.localizedDescription)")
        }
        
    }
    
    //MARK: - GlobalSearchSectionHeaderDelegate
    
    func didPressClear() {
        
        let updates = searchSuggestsFactory.clearSuggestsHistory(in: &suggestsDataSourceConfiguration!)
        
        view.displaySuggests(with: suggestsDataSourceConfiguration.dataSource, and: updates)
        interactor.clearSearchStringsCache()
    }
}

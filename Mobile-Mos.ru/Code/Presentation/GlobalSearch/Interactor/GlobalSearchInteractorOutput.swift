//
//  GlobalSearchInteractorOutput.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 29.09.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol GlobalSearchInteractorOutput: class {

    /// Получение топовых поисковых слов
    ///
    /// - Parameter result: результат получения топовых поисковых слов
    func didFinishObtainingSearchTopWords(with result: SearchTopWordsMultipleResult)
    
    /// Получение кэша поисковых запросов
    ///
    /// - Parameter result: результат получения топовых поисковых слов
    func didFinishObtainingCachedSearchTopWords(with result: SearchTopWordsMultipleResult)
    
    /// Получение саджестов
    ///
    /// - Parameter result: результат получения саджестов
    func didFinishObtainingSearchSuggests(for searchString: String, with result: SearchSuggestsMultipleResult)
    
    /// Получение модели поиска после поиска
    ///
    /// - Parameter searchString: строка поиска, по котрой запустили поиск
    /// - Parameter model: результат поиска
    func didFinishSearching(with model: GlobalSearchModel)
}

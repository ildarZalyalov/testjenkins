//
//  GlobalSearchInteractorInput.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 29.09.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol GlobalSearchInteractorInput: class {
    
    /// Кеш диалогов
    var conversationsCache: [Conversation] { get }
    
    /// Кэш топ поисковых запросов
    var searchTopWordsCache: [SearchTopWords] { get set }
    
    /// Кэш поисковых запросов
    var searchStringsCache: [SearchTopWords] { get set }
    
    /// Получить топ поисковых слов
    func obtainSearchTopWords()
    
    /// Почистить кэш поисковых слов
    func clearSearchStringsCache()
    
    /// Выполнить поиск
    ///
    /// - Parameters:
    ///   - searchString: строка для поиска
    func performSearchRequest(with searchString: String)
    
    /// Выполнить поиск по саджестам
    ///
    /// - Parameter searchString: строка для поиска
    func performSearchSuggests(with searchString: String)
    
    /// Сохранить поисковый запрос в кэш
    ///
    /// - Parameter searchString: поисковый запрос
    func saveSearchStringToCache(_ searchString: String)
    
    /// Получить кэш поисковых запросов
    func obtainSearchStringsCache()
    
    /// Отменить запрос
    func cancelRequest()
    
    /// Отправить событие поиска
    ///
    /// - Parameters:
    ///   - event: событие
    ///   - data: информация поиска
    func sendEvent(with event: GlobalSearchEvents, data: GlobalSearchEventData?, selectedChatdata: GlobalSearchSelectChatEventData?, selectedResultData: GlobalSearchSelectResultEventData?, scrolledToLastNews: Bool?)
    
    func addNewsToAnalytics(_ news: NewsSearchModel)
}

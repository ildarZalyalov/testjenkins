//
//  GlobalSearchInteractor.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 29.09.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class GlobalSearchInteractor: GlobalSearchInteractorInput {
    
    weak var output: GlobalSearchInteractorOutput!
    
    var userService: UserService!
    var globalSearchService: GlobalSearchService!
    var mapItemsService: MapItemsService!
    var conversationsService: ConversationsService!
    var messageHistoryService: ChatMessageHistoryService!
    var chatKeywordsService: ChatKeywordsService!
    var analyticsManager: AnalyticsManager!
    
    var request: HTTPRequest?
    var conversations: [Conversation]?
    
    //MARK: - Приватные методы
    
    func obtainConversationsCache(with completion: @escaping () -> ()) {
        
        conversationsService.obtainCachedConversations(for: userService.currentUser) { [weak self] (result) in
            
            switch result {
            case .success(let cachedConversations):
                self?.conversations = cachedConversations
            default:
                self?.conversations = []
            }
            
            completion()
        }
    }
    
    //MARK: - GlobalSearchInteractorInput
    
    var conversationsCache: [Conversation] {
        return conversations ?? []
    }
    
    var searchTopWordsCache: [SearchTopWords] = []
    
    var searchStringsCache: [SearchTopWords] = []
    
    func cancelRequest() {
        request?.cancel()
    }
    
    func obtainSearchTopWords() {
        request = globalSearchService.getSearchTopWords {[weak self] (result) in
            self?.output.didFinishObtainingSearchTopWords(with: result)
        }
    }
    
    func clearSearchStringsCache() {
        searchStringsCache.removeAll()
        globalSearchService.clearCachedSearchStrings()
    }
    
    func performSearchSuggests(with searchString: String) {
        request?.cancel()
        request = globalSearchService.searchSuggests(with: searchString, completion: { [weak self] (result) in
            self?.output.didFinishObtainingSearchSuggests(for: searchString, with: result)
        })
    }
    
    func saveSearchStringToCache(_ searchString: String) {
        let searchWordModel = SearchTopWords(title: searchString)
        globalSearchService.saveSearchString(searchWordModel)
    }
    
    func obtainSearchStringsCache() {
        globalSearchService.obtainCashedSearchStrings {[weak self] (result) in
            self?.output.didFinishObtainingCachedSearchTopWords(with: result)
        }
    }
    
    func performSearchRequest(with searchString: String) {
        
        request?.cancel()
        saveSearchStringToCache(searchString)
        
        guard conversations != nil else {
            obtainConversationsCache { [weak self] in
                self?.performSearchRequest(with: searchString)
            }
            return
        }
        
        let filterString = searchString.lowercased()
        var searchModel = GlobalSearchModel()
        
        request = globalSearchService.searchNews(with: searchString) { [weak self] (result) in
            
            guard let strongSelf = self else { return }
            
            switch result {
            case .success(let models):
                searchModel.news = models
            case .failure(let error):
                print("Request error: \(error.localizedDescription)")
            }
            
            let chatIds = strongSelf.chatKeywordsService.getChatIds(with: filterString)
            
            searchModel.conversations = strongSelf.conversationsCache.filter { chatIds.contains($0.itemId) }
            
            strongSelf.output.didFinishSearching(with: searchModel)
        }
    }
    
    func sendEvent(with event: GlobalSearchEvents, data: GlobalSearchEventData?, selectedChatdata: GlobalSearchSelectChatEventData?, selectedResultData: GlobalSearchSelectResultEventData?, scrolledToLastNews: Bool?) {
        analyticsManager.sendGlobalSearchEvent(with: event, requestData: data, selectedChatData: selectedChatdata, selectedSearchResultData: selectedResultData, scrolledToLastNews: scrolledToLastNews)
    }
    
    func addNewsToAnalytics(_ news: NewsSearchModel) {
        analyticsManager.setSelectedNewsItem(with: news)
    }
}

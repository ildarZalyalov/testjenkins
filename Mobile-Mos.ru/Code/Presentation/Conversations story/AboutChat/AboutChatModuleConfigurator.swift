//
//  AboutChatModuleConfigurator.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 08.08.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import UIKit

class AboutChatModuleConfigurator: BaseModuleConfigurator {
    
    override func configureModule(for view: UIViewController) {
        
        let viewController = view as! AboutChatViewController
        let presenter = AboutChatPresenter()
        let interactor = AboutChatInteractor()
        let router = AboutChatRouter()
        
        let userSettingsService = UIApplication.shared.serviceBuilder.getUserInfoAndSettingsService()
        let notificationCenter = NotificationCenter.default
        let alertsFactory = CommonAlertsFactoryImplementation()
        let analyticsManager = AnalyticsManagerBuilder.getAnalyticsManager()
        
        viewController.output = presenter
        
        presenter.view = viewController
        presenter.interactor = interactor
        presenter.router = router
        presenter.notificationCenter = notificationCenter
        
        interactor.output = presenter
        interactor.userSettingsService = userSettingsService
        interactor.analyticsManager = analyticsManager
        
        router.transitionHandler = viewController
        router.alertsFactory = alertsFactory
        router.analyticsManager = analyticsManager
    }
}

//
//  AboutChatViewOutput.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 08.08.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol AboutChatViewOutput: class {
    
    func setupInitialState()
    
    func configure(with conversation: Conversation)
    
    func didPressShareButton()
    
    func didPressAgreementsButton()
    
    func didChangeNotificationSetting(to value: Bool)
}

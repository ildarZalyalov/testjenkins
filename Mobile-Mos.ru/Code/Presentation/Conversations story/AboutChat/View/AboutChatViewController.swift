//
//  AboutChatViewController.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 08.08.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import UIKit

class AboutChatViewController: BaseViewController, AboutChatViewInput, NavigationBarCustomizingController, ConfigurableModuleController {
    
    var output: AboutChatViewOutput!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var yandexAgreementsLabel: UILabel!
    
    @IBOutlet weak var avatarView: UIImageView!
    
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var notificationButton: UIButton!
    @IBOutlet weak var notificationSwitch: UISwitch!
    @IBOutlet weak var notificationActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var separatorView: UIView!
    
    @IBOutlet var regularConstraints: [NSLayoutConstraint]!
    @IBOutlet var compactConstraints: [NSLayoutConstraint]!
    
    @IBOutlet weak var shareToSettingsConstraint: NSLayoutConstraint!
    @IBOutlet weak var shareToBottomConstraint: NSLayoutConstraint!
    
    let deviceScreenWidthForCompactConstraints: CGFloat = 320
    let descriptionMinimumFontScale: CGFloat = 0.75
    
    let descriptionTextParagraphStyle: NSParagraphStyle = {
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .left
        paragraphStyle.lineSpacing = 9
        paragraphStyle.lineBreakMode = .byTruncatingTail
        
        return paragraphStyle
    }()
    
    //MARK: - Методы
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        let deviceScreenWidth = UIScreen.main.orientationIndependentSize.width
        
        if deviceScreenWidth <= deviceScreenWidthForCompactConstraints {
            
            NSLayoutConstraint.deactivate(regularConstraints)
            NSLayoutConstraint.activate(compactConstraints)
            
            avatarView.isHidden = true
        }
        
        output.setupInitialState()
    }
    
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    @IBAction func pressedShareButton() {
        output.didPressShareButton()
    }
    
    @IBAction func changedNotificationSetting() {
        output.didChangeNotificationSetting(to: notificationSwitch.isOn)
    }
    
    //MARK: - ConfigurableModuleController
    
    func configureModule(with object: Any) {
        if let conversation = object as? Conversation {
            output.configure(with: conversation)
        }
    }
    
    //MARK: - NavigationBarCustomizingController
    
    var navigationBarState: NavigationBarState? {
        
        var state = NavigationBarState()
        
        state.isHidden = false
        state.isShadowHidden = true
        state.isTranslucent = false
        state.barTintColor = UIColor.white
        state.backgroundState = NavigationBarBackgroundState.regular
        state.tintColor = UIColor.black
        state.titleTextAttributes = [
            NSAttributedStringKey.font : UIFont(customName: .graphikLCGBold, size: 15) as Any,
            NSAttributedStringKey.foregroundColor : UIColor.black
        ]
        
        return state
    }
    
    //MARK: - AboutChatViewInput
    
    func displayInfo(about conversation: Conversation) {
        
        titleLabel.text = conversation.title
        
        let attributes: [NSAttributedStringKey : Any] = [
            NSAttributedStringKey.font : descriptionLabel.font,
            NSAttributedStringKey.foregroundColor : descriptionLabel.textColor,
            NSAttributedStringKey.paragraphStyle : descriptionTextParagraphStyle
        ]
        
        descriptionLabel.attributedText = NSAttributedString(string: conversation.descriptionText, attributes: attributes)
        
        descriptionLabel.adjustsFontSizeToFitWidth = true
        descriptionLabel.minimumScaleFactor = descriptionMinimumFontScale
        
        var placeholder: UIImage?
        if let placeholderName = conversation.avatarPlaceholderIconName {
            placeholder = UIImage(named: placeholderName)
        }
        
        avatarView.loadAndDisplayImage(from: conversation.avatarIconUrl, placeholder: placeholder, with: nil, completion: nil)
        
        shareButton.tintColor = conversation.style.controlsTextColor
        shareButton.setTitleColor(conversation.style.controlsTextColor, for: .highlighted)
        
        notificationButton.tintColor = conversation.style.controlsTextColor
        notificationButton.setTitleColor(conversation.style.controlsTextColor, for: .highlighted)
        
        guard conversation.title == mapConversation.title else { return }
        
        yandexAgreementsLabel.isHidden = false 
    }
    
    func displayNotificationsSetting(with notificationsEnabled: Bool, animated: Bool) {
        
        let title = notificationsEnabled ? StringsHelper.aboutConversationNotificationsEnabledTitle : StringsHelper.aboutConversationNotificationsDisabledTitle
        
        notificationButton.setTitle(title, for: .normal)
        notificationSwitch.setOn(notificationsEnabled, animated: animated)
    }
    
    func hideUserNotificationsSettings() {
        
        notificationButton.isHidden = true
        notificationSwitch.isHidden = true
        separatorView.isHidden = true
        
        shareToSettingsConstraint.priority = .defaultLow
        shareToBottomConstraint.priority = .defaultHigh
        
        view.setNeedsLayout()
        view.layoutIfNeeded()
    }
    
    func showNotificationsSettingsLoadingStatus() {
        notificationSwitch.isHidden = true
        notificationActivityIndicator.startAnimating()
    }
    
    func hideNotificationsSettingsLoadingStatus() {
        notificationActivityIndicator.stopAnimating()
        notificationSwitch.isHidden = false
    }
    
    @IBAction func didPressYandexMapAgreements(_ sender: Any) {
        output.didPressAgreementsButton()
    }
    
}

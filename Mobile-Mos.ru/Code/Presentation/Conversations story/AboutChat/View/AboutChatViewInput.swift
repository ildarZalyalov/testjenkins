//
//  AboutChatViewInput.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 08.08.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol AboutChatViewInput: class {
    
    func displayInfo(about conversation: Conversation)
    
    func displayNotificationsSetting(with notificationsEnabled: Bool, animated: Bool)
    
    func hideUserNotificationsSettings()
    
    func showNotificationsSettingsLoadingStatus()
    
    func hideNotificationsSettingsLoadingStatus()
}

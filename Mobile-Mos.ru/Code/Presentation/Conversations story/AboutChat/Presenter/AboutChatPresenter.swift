//
//  AboutChatPresenter.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 08.08.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class AboutChatPresenter: AboutChatViewOutput, AboutChatInteractorOutput {
    
    weak var view: AboutChatViewInput!
    var router: AboutChatRouterInput!
    var interactor: AboutChatInteractorInput!
    
    var notificationCenter: NotificationCenter!
    
    var conversation: Conversation!
    
    var areNotificationsAuthorized: Bool!
    var isNotificationsSettingObtained: Bool = false
    
    var currentNotificationsSetting: UserNotificationsSetting?
    
    let retryObtainSettingsDelay: TimeInterval = 3
    lazy var retryObtainSettingsPerformer = DelayedWorkPerformer { [weak self] in
        guard let strongSelf = self else { return }
        strongSelf.interactor.obtainNotificationsSettings(for: strongSelf.conversation)
    }
    
    func redisplayNotificationsSetting() {
        if let setting = currentNotificationsSetting {
            view.hideNotificationsSettingsLoadingStatus()
            view.displayNotificationsSetting(with: setting.areNotificationsEnabled, animated: false)
        }
        else {
            view.hideNotificationsSettingsLoadingStatus()
            view.hideUserNotificationsSettings()
        }
    }
    
    //MARK: - AboutChatViewOutput
    
    func setupInitialState() {
        
        view.displayInfo(about: conversation)
        
        guard conversation.itemId != mapConversation.itemId else {
            view.hideUserNotificationsSettings()
            return
        }
        
        view.showNotificationsSettingsLoadingStatus()
        interactor.obtainCurrentUser()
    }
    
    func configure(with conversation: Conversation) {
        self.conversation = conversation
    }
    
    func didPressShareButton() {
        router.showSharingModule(with: interactor.itemsForSharingConversation(conversation: conversation), conversationId: conversation.itemId)
    }
    
    func didPressAgreementsButton() {
        router.showAgreementsModule()
    }
    
    func didChangeNotificationSetting(to value: Bool) {
        
        guard areNotificationsAuthorized else {
            
            view.displayNotificationsSetting(with: false, animated: true)
            router.showNotificationsDisabledModule()
            
            return
        }
        
        view.showNotificationsSettingsLoadingStatus()
        interactor.changeNotificationsSetting(for: conversation, to: value)
    }
    
    //MARK: - AboutChatInteractorOutput
    
    func didFinishObtainingCurrentUser(with user: User) {
        
        if user.isLoggedIn {
            interactor.obtainNotificationsAuthorized()
            notificationCenter.addObserver(self, selector: #selector(applicationDidBecomeActive), name: .UIApplicationDidBecomeActive, object: nil)
        }
        else {
            interactor.sendOpenEvent(with: conversation.itemId, areNotificationsEnabled: true)
            view.hideNotificationsSettingsLoadingStatus()
            view.hideUserNotificationsSettings()
        }
    }
    
    func didFinishObtainingNotificationsAuthorized(with authorized: Bool) {
        
        areNotificationsAuthorized = authorized
        
        guard authorized else {
            
            view.hideNotificationsSettingsLoadingStatus()
            view.displayNotificationsSetting(with: false, animated: false)
            interactor.sendOpenEvent(with: conversation.itemId, areNotificationsEnabled: false)
            return
        }
        
        if isNotificationsSettingObtained {
            redisplayNotificationsSetting()
        }
        else {
            interactor.obtainNotificationsSettings(for: conversation)
        }
    }
    
    func didFinishObtainingNotificationsSettings(for conversation: Conversation, with setting: UserNotificationsSetting?) {

        isNotificationsSettingObtained = true
        currentNotificationsSetting = setting
        
        redisplayNotificationsSetting()
        
        if let settings = setting {
            interactor.sendOpenEvent(with: conversation.itemId, areNotificationsEnabled: settings.areNotificationsEnabled)
        }
    }
    
    func didFinishObtainingNotificationsSettings(with error: Error) {
        switch error {
        case NotificationsSettingObtainErrors.notificationsDisabledOnDevice:
            areNotificationsAuthorized = false
            view.hideUserNotificationsSettings()
            view.displayNotificationsSetting(with: false, animated: false)
        default:
            retryObtainSettingsPerformer.performOnMain(afterDelay: retryObtainSettingsDelay)
        }
    }
    
    func didFinishChangingNotificationsSetting(for conversation: Conversation, to value: Bool, successfully: Bool) {
        
        view.hideNotificationsSettingsLoadingStatus()
        
        let newValue = successfully ? value : !value
        view.displayNotificationsSetting(with: newValue, animated: false)
        
        if !successfully {
            router.showChangingNotificationSettingErrorModule()
        }
    }
    
    //MARK: - Нотификации
    
    @objc
    func applicationDidBecomeActive() {
        view.showNotificationsSettingsLoadingStatus()
        interactor.obtainNotificationsAuthorized()
    }
}

//
//  AboutChatRouter.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 08.08.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import SafariServices

class AboutChatRouter: AboutChatRouterInput {
    
    weak var transitionHandler: UIViewController!
    var alertsFactory: CommonAlertsFactory!
    var analyticsManager: AnalyticsManager!
    
    //MARK: - AboutChatRouterInput
    
    func showSharingModule(with data: [Any], conversationId: String) {
        
        let activityController = UIActivityViewController(activityItems: data, applicationActivities: nil)
        activityController.completionWithItemsHandler = { [weak self] activityType, completed, items, error  in
            
            if completed {
                self?.sendShareEvent(with: conversationId, appSharingSource: activityType?.rawValue)
            }
        }
        
        transitionHandler.present(activityController, animated: true, completion: nil)
    }
    
    func showAgreementsModule() {
        
        let safariController = SFSafariViewController(url: legalYandexAPIRulesUrl, entersReaderIfAvailable: true)
        if #available(iOS 10.0, *) {
            safariController.preferredControlTintColor = mapConversation.style.controlsTextColor
        }
        
        transitionHandler.present(safariController, animated: true, completion: nil)
    }
    
    func showChangingNotificationSettingErrorModule() {
        let alert = alertsFactory.getErrorAlert(with: StringsHelper.changingNotificationSettingsErrorAlertText)
        transitionHandler.present(alert, animated: true, completion: nil)
    }
    
    func showNotificationsDisabledModule() {
        let alert = alertsFactory.getUserNotificationsNotAuthorizedAlert()
        transitionHandler.present(alert, animated: true, completion: nil)
    }
    
   fileprivate func sendShareEvent(with conversationId: String, appSharingSource: String?) {
        
        guard let chat = ConversationIdentificator.init(rawValue: conversationId) else { return }
        
        let eventName = AnalyticsConstants.sharingToAppEventName
        let sharingAppSource = appSharingSource ?? AnalyticsConstants.emptyValueName
    
        guard conversationId != mapConversation.itemId else {
            analyticsManager.sendMapConversationEvent(with: .shareApp(withString: eventName), aboutAppData: nil, appSharingSource: appSharingSource, datasetData: nil, mapObjectSharingData: nil, overlapData: nil)
            return
        }
        
        switch chat {
        case .children:
            analyticsManager.sendChildsConversationEvent(with: .shareApp(withString: eventName), aboutAppData: nil, appSharingSource: sharingAppSource)
        case .transport:
            analyticsManager.sendTransportConversationEvent(with: .shareApp(withString: eventName), aboutAppData: nil, appSharingSource: sharingAppSource)
        case .myHome:
            analyticsManager.sendMyHomeConversationEvent(with: .shareApp(withString: eventName), aboutAppData: nil, appSharingSource: sharingAppSource)
        case .health:
            analyticsManager.sendMedecineEvent(with: .shareApp(withString: eventName), aboutAppData: nil, appSharingSource: sharingAppSource)
        case .cityNews:
            analyticsManager.sendNewsConversationEvent(with: .shareApp(withString: eventName), aboutAppData: nil, appSharingSource: sharingAppSource)
        case .qa:
            analyticsManager.sendConsultationConversationEvent(with: .shareApp(withString: eventName), aboutAppData: nil, appSharingSource: sharingAppSource)
        default:
            return
        }
    }
}

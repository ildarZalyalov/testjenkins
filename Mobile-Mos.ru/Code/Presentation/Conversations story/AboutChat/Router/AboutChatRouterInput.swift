//
//  AboutChatRouterInput.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 08.08.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol AboutChatRouterInput: class {

    func showSharingModule(with data: [Any], conversationId: String)
    
    func showAgreementsModule()
    
    func showChangingNotificationSettingErrorModule()
    
    func showNotificationsDisabledModule()
}

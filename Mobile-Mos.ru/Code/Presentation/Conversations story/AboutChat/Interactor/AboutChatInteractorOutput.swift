//
//  AboutChatInteractorOutput.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 08.08.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol AboutChatInteractorOutput: class {
    
    func didFinishObtainingCurrentUser(with user: User)
    
    func didFinishObtainingNotificationsAuthorized(with authorized: Bool)
    
    func didFinishObtainingNotificationsSettings(for conversation: Conversation, with setting: UserNotificationsSetting?)
    
    func didFinishObtainingNotificationsSettings(with error: Error)
    
    func didFinishChangingNotificationsSetting(for conversation: Conversation, to value: Bool, successfully: Bool)
}

//
//  AboutChatInteractor.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 08.08.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class AboutChatInteractor: AboutChatInteractorInput {
    
    weak var output: AboutChatInteractorOutput!
    
    var userSettingsService: UserInfoAndSettingsService!
    var analyticsManager: AnalyticsManager!
    
    //MARK: - AboutChatInteractorInput
    
    func itemsForSharingConversation(conversation: Conversation) -> [Any] {
        return [conversation.title, conversation.descriptionText, #imageLiteral(resourceName: "appIcon") as Any, applicationUrl]
    }
    
    func obtainCurrentUser() {
        output.didFinishObtainingCurrentUser(with: userSettingsService.currentUser)
    }
    
    func obtainNotificationsAuthorized() {
        userSettingsService.obtainNotificationsAuthorizationStatus { [weak self] status in
            switch status {
            case .denied, .notDetermined:
                self?.output.didFinishObtainingNotificationsAuthorized(with: false)
            case .authorized:
                self?.output.didFinishObtainingNotificationsAuthorized(with: true)
            }
        }
    }
    
    func obtainNotificationsSettings(for conversation: Conversation) {
        
        userSettingsService.refreshNotificationSettings(completion: { [weak self] settingsResult in
            
            guard let strongSelf = self else { return }
            
            switch settingsResult {
            case .success(let settings):
                let setting = settings.filter({ $0.chatId == conversation.itemId }).first
                strongSelf.output.didFinishObtainingNotificationsSettings(for: conversation, with: setting)
            case .failure(let error):
                strongSelf.output.didFinishObtainingNotificationsSettings(with: error)
            }
        })
    }
    
    func changeNotificationsSetting(for conversation: Conversation, to value: Bool) {
        userSettingsService.changeNotificationSetting(for: conversation, to: value) { [weak self] result in
            self?.output.didFinishChangingNotificationsSetting(for: conversation, to: value, successfully: result)
        }
    }
    
    func sendOpenEvent(with conversationId: String, areNotificationsEnabled: Bool) {
        
        guard let chat = ConversationIdentificator.init(rawValue: conversationId) else { return }
        
        let sharingConversationEvent: SharingConversationEventData = (areNotificationsEnabled, AnalyticsDefaultsParams.authorizationStatus.rawValue)
        
        let eventName = AnalyticsConstants.aboutChatEventName
        
        guard conversationId != mapConversation.itemId else {
            analyticsManager.sendMapConversationEvent(with: .aboutConversation(withString: eventName), aboutAppData: sharingConversationEvent, appSharingSource: nil, datasetData: nil, mapObjectSharingData: nil, overlapData: nil)
            return
        }
    
        switch chat {
        case .children:
            analyticsManager.sendChildsConversationEvent(with: .aboutChat(withString: eventName), aboutAppData: sharingConversationEvent, appSharingSource: nil)
        case .transport:
            analyticsManager.sendTransportConversationEvent(with: .aboutChat(withString: eventName), aboutAppData: sharingConversationEvent, appSharingSource: nil)
        case .myHome:
            analyticsManager.sendMyHomeConversationEvent(with: .aboutChat(withString: eventName), aboutAppData: sharingConversationEvent, appSharingSource: nil)
        case .health:
            analyticsManager.sendMedecineEvent(with: .aboutChat(withString: eventName), aboutAppData: sharingConversationEvent, appSharingSource: nil)
        case .cityNews:
            analyticsManager.sendNewsConversationEvent(with: .aboutChat(withString: eventName), aboutAppData: sharingConversationEvent, appSharingSource: nil)
        case .qa:
            analyticsManager.sendConsultationConversationEvent(with: .aboutChat(withString: eventName), aboutAppData: sharingConversationEvent, appSharingSource: nil)
        default:
            return
        }
    }
}

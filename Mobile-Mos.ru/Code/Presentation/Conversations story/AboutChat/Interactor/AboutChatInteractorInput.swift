//
//  AboutChatInteractorInput.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 08.08.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol AboutChatInteractorInput: class {
    
    func itemsForSharingConversation(conversation: Conversation) -> [Any]
    
    func obtainCurrentUser()
    
    func obtainNotificationsAuthorized()
    
    func obtainNotificationsSettings(for conversation: Conversation)
    
    func changeNotificationsSetting(for conversation: Conversation, to value: Bool)
    
    func sendOpenEvent(with conversationId: String, areNotificationsEnabled: Bool)
}

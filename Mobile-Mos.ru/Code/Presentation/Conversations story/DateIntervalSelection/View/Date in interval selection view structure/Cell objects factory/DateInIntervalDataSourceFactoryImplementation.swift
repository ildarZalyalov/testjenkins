//
//  DateInIntervalDataSourceFactoryImplementation.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 23.11.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class DateInIntervalDataSourceFactoryImplementation: DateInIntervalDataSourceFactory {
    
    lazy var dateFormatter: DateFormatter = {
        
        let localeIdentifier = "en_US_POSIX"
        let timeZoneAbbreviation = "UTC"
        
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: localeIdentifier)
        formatter.timeZone = TimeZone(abbreviation: timeZoneAbbreviation)
        formatter.dateFormat = "dd.MM.yyyy"
        
        return formatter
    }()
    
    func refreshDateFormat() {
        
        var format = "dd.MM.yyyy"
        
        if disableSelectYear && disableSelectDays {
            format = "MMMM"
        }
        else if disableSelectYear {
            format = "dd.MM"
        }
        else if disableSelectDays {
            format = "MM.yyyy"
        }
        
        dateFormatter.dateFormat = format
    }
    
    func cellObject(from selectionValue: DateInIntervalSelectionValue, selectionColor: UIColor) -> DateInIntervalSelectionCellObject {
        
        let title = selectionValue.type == .dateFrom ? StringsHelper.chatDateIntervalControlDateFromTitle : StringsHelper.chatDateIntervalControlDateToTitle
        var value: String?
        if let date = selectionValue.selectedDate {
            value = dateFormatter.string(from: date)
        }
        
        return DateInIntervalSelectionCellObject(itemId: UUID().uuidString,
                                                 isSelected: selectionValue.isSelected,
                                                 selectionColor: selectionColor,
                                                 title: title,
                                                 value: value)
    }
    
    //MARK: - DateInIntervalDataSourceFactory
    
    var disableSelectYear: Bool = false {
        didSet {
            refreshDateFormat()
        }
    }
    
    var disableSelectDays: Bool = false {
        didSet {
            refreshDateFormat()
        }
    }
    
    func buildDataSourceConfiguration(with currentDateFrom: DateInIntervalSelectionValue, and currentDateTo: DateInIntervalSelectionValue, selectionColor: UIColor) -> DateInIntervalDataSourceConfiguration {
        
        let dataStructure = CollectionViewDataSourceStructure()
        var selectionItems = [String : DateInIntervalSelectionValue]()
        
        dataStructure.appendSection()
        
        let cellObjectFrom = cellObject(from: currentDateFrom, selectionColor: selectionColor)
        dataStructure.appendCellObject(cellObjectFrom)
        selectionItems[cellObjectFrom.itemId] = currentDateFrom
        
        let cellObjectTo = cellObject(from: currentDateTo, selectionColor: selectionColor)
        dataStructure.appendCellObject(cellObjectTo)
        selectionItems[cellObjectTo.itemId] = currentDateTo
        
        let dataSource = CollectionViewDataSource(with: dataStructure)
        
        return DateInIntervalDataSourceConfiguration(dataSource: dataSource, selectionItems: selectionItems)
    }
}

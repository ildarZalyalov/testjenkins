//
//  DateInIntervalDataSourceFactory.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 23.11.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Структура данных, описывающая состояние выбора дат от/до
struct DateInIntervalSelectionValue {
    
    /// Тип даты
    ///
    /// - dateFrom: дата от
    /// - dateTo: дата до
    enum DateType {
        case dateFrom
        case dateTo
    }
    
    /// Тип даты
    let type: DateType
    
    /// Осуществляется ли сейчас выбор этой даты
    var isSelected: Bool = false
    
    /// Значение даты (если есть)
    var selectedDate: Date?
    
    init(with type: DateType) {
        self.type = type
    }
}

/// Конфигурация источника данных для выбора дат от/до
typealias DateInIntervalDataSourceConfiguration = (dataSource: CollectionViewDataSource, selectionItems: [String : DateInIntervalSelectionValue])

/// Фабрика источника данных для выбора дат от/до
protocol DateInIntervalDataSourceFactory {
    
    /// Заблокирован ли выбор года
    var disableSelectYear: Bool { get set }
    
    /// Заблокирован ли выбор дня
    var disableSelectDays: Bool { get set }
    
    /// Построить источник данных для выбора дат от/до
    ///
    /// - Parameters:
    ///   - currentDateFrom: текущее состояние выбора даты от
    ///   - currentDateTo: текущее состояние выбора даты до
    ///   - selectionColor: цвет для выбранной даты
    /// - Returns: источник данных
    func buildDataSourceConfiguration(with currentDateFrom: DateInIntervalSelectionValue, and currentDateTo: DateInIntervalSelectionValue, selectionColor: UIColor) -> DateInIntervalDataSourceConfiguration
}

//
//  DateInIntervalSelectionCellObject.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 23.11.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Cell оbject ячейки выбора дат от/до
struct DateInIntervalSelectionCellObject: CellObjectWithId {
    
    /// Идентификатор состояния выбора даты
    var itemId: String
    
    /// Осуществляется ли сейчас выбор этой даты
    let isSelected: Bool
    
    /// Цвет для выбранной даты
    let selectionColor: UIColor
    
    /// Заголовок над выбранной датой ("От" или "До")
    let title: String
    
    /// Значение выбранной даты в текстовом виде (если есть)
    let value: String?
}

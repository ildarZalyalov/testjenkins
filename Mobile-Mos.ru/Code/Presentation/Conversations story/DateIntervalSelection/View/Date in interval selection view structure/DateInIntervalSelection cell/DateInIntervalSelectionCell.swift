//
//  DateInIntervalSelectionCell.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 23.11.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import UIKit

class DateInIntervalSelectionCell: UICollectionViewCell, ConfigurableView {
    
    @IBOutlet weak var calendarIconView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var placeholderLabel: UILabel!
    
    @IBOutlet var regularConstraints: [NSLayoutConstraint]!
    @IBOutlet var compactConstraints: [NSLayoutConstraint]!
    
    fileprivate var selectedStateBackgroundColor: UIColor?
    
    override var isSelected: Bool {
        didSet {
            refreshUI()
        }
    }
    
    override func awakeFromNib() {
        
        calendarIconView.image = calendarIconView.image?.withRenderingMode(.alwaysTemplate)
        selectedBackgroundView = UIView(frame: contentView.bounds)
        
        if UIDevice.current.hasCompactScreen {
            NSLayoutConstraint.deactivate(regularConstraints)
            NSLayoutConstraint.activate(compactConstraints)
        }
    }
    
    //MARK: - Приватные методы
    
    fileprivate func refreshUI() {
        
        let regularBackgroundColor = UIColorPalette.dateInIntervalDefaultBackgroundColor
        let selectedBackgroundColor = selectedStateBackgroundColor ?? UIColorPalette.dateInIntervalDefaultSelectedBackgroundColor
        
        calendarIconView.tintColor = isSelected ? regularBackgroundColor : selectedBackgroundColor
        titleLabel.highlightedTextColor = regularBackgroundColor
        valueLabel.highlightedTextColor = regularBackgroundColor
        placeholderLabel.highlightedTextColor = regularBackgroundColor
    }
    
    //MARK: - ConfigurableView
    
    func configure(with object: Any) {
        
        guard let cellObject = object as? DateInIntervalSelectionCellObject else {
            return
        }
        
        titleLabel.text = cellObject.title
        valueLabel.text = cellObject.value
        placeholderLabel.text = cellObject.title
        
        let hasNoValue = cellObject.value?.isEmpty ?? true
        
        titleLabel.isHidden = hasNoValue
        valueLabel.isHidden = hasNoValue
        placeholderLabel.isHidden = !hasNoValue
        
        selectedStateBackgroundColor = cellObject.selectionColor
        selectedBackgroundView?.backgroundColor = cellObject.selectionColor
        
        isSelected = cellObject.isSelected
        refreshUI()
    }
}

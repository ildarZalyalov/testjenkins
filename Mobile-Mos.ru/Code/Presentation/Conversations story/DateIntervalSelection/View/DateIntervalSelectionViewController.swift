//
//  DateIntervalSelectionViewController.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 22.11.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import UIKit
import JTAppleCalendar

class DateIntervalSelectionViewController: BaseViewController, DateIntervalSelectionViewInput, ConfigurableModuleController, HorizontalPickerViewDelegate, JTAppleCalendarViewDataSource, JTAppleCalendarViewDelegate, CommandKeyboardViewDelegate, UICollectionViewDelegate {
    
    var output: DateIntervalSelectionViewOutput!
    
    @IBOutlet weak var cancelBarButtonItem: UIBarButtonItem!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var scrollViewTopShadowView: UIImageView!
    @IBOutlet weak var contentStackView: UIStackView!
    @IBOutlet weak var rootStackView: UIStackView!
    @IBOutlet weak var topContainer: UIView!
    
    var dateInIntervalDataDisplayManager: CollectionViewDataSource!
    @IBOutlet weak var dateInIntervalSelectionView: UICollectionView!
    @IBOutlet weak var dateInIntervalSelectionViewLayout: UICollectionViewFlowLayout!
    
    @IBOutlet weak var monthsSelectionContainerView: UIView!
    lazy var monthsPickerView = HorizontalPickerView.loadNib()
    
    @IBOutlet weak var yearsSelectionContainerView: UIView!
    lazy var yearsPickerView = HorizontalPickerView.loadNib()
    
    @IBOutlet weak var calendarContainerView: UIView!
    @IBOutlet weak var calendarView: JTAppleCalendarView!
    
    @IBOutlet weak var submitContainerView: UIView!
    let submitKeyboard = CommandKeyboardView(frame: CGRect.zero)
    
    @IBOutlet var regularConstraints: [NSLayoutConstraint]!
    @IBOutlet var compactConstraints: [NSLayoutConstraint]!
    
    let navigationItemSpaceWidth: CGFloat = 9
    let navigationItemsFontSize: CGFloat = 15
    let numberOfRowsInCalendar = 6
    
    let calendarWeekDaysHeaderTopOffset: CGFloat = 2
    let calendarWeekDaysHeaderHeight: CGFloat = 22
    
    var lastDateInIntervalSelectionViewWidth: CGFloat = 0
    
    var statusBarStyle = UIStatusBarStyle.default
    
    var calendarParameters: ConfigurationParameters!
    var calendarSelectionColor = UIColor.red
    
    //MARK: - Методы
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // ограничение языка на #available
        if #available(iOS 10.0, *) {}
        else {
            
            if var rightBarButtonItems = navigationItem.rightBarButtonItems {
                
                let spaceItem = UIBarButtonItem(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
                spaceItem.width = navigationItemSpaceWidth
                
                rightBarButtonItems = [spaceItem] + rightBarButtonItems + [spaceItem]
                navigationItem.rightBarButtonItems = rightBarButtonItems
            }
            
            let falseSpaceOnLeft = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
            navigationItem.leftBarButtonItems = [falseSpaceOnLeft]
            
            view.setNeedsLayout()
            view.layoutIfNeeded()
        }
        
        scrollViewTopShadowView.image = #imageLiteral(resourceName: "customControlHeaderShadow").resizableImage(withCapInsets: UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5))
        
        yearsPickerView.delegate = self
        yearsPickerView.translatesAutoresizingMaskIntoConstraints = false
        yearsSelectionContainerView.insertSubview(yearsPickerView, at: 0)
        yearsSelectionContainerView.addConstraints(NSLayoutConstraint.edgesConstraints(for: yearsPickerView))
        
        monthsPickerView.delegate = self
        monthsPickerView.translatesAutoresizingMaskIntoConstraints = false
        monthsSelectionContainerView.insertSubview(monthsPickerView, at: 0)
        monthsSelectionContainerView.addConstraints(NSLayoutConstraint.edgesConstraints(for: monthsPickerView))
        
        calendarView.minimumLineSpacing = 0
        calendarView.minimumInteritemSpacing = 0
        calendarView.sectionInset = UIEdgeInsets(top: calendarWeekDaysHeaderTopOffset, left: 0, bottom: 0, right: 0)
        
        submitKeyboard.delegate = self
        submitKeyboard.translatesAutoresizingMaskIntoConstraints = false
        submitContainerView.insertSubview(submitKeyboard, at: 0)
        submitContainerView.addConstraints(NSLayoutConstraint.edgesConstraints(for: submitKeyboard))
        
        if UIDevice.current.hasCompactScreen {
            
            yearsPickerView.controlSizing = .compact
            monthsPickerView.controlSizing = .compact
            
            NSLayoutConstraint.deactivate(regularConstraints)
            NSLayoutConstraint.activate(compactConstraints)
        }
        
        output.setupInitialState()
        
        refreshPreferredContentSize()
    }
    
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return statusBarStyle
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override func viewDidLayoutSubviews() {
        
        guard lastDateInIntervalSelectionViewWidth != dateInIntervalSelectionView.width else { return }
        
        lastDateInIntervalSelectionViewWidth = dateInIntervalSelectionView.width
        
        let width = lastDateInIntervalSelectionViewWidth / 2
        let height = dateInIntervalSelectionView.height
        
        dateInIntervalSelectionViewLayout.itemSize = CGSize(width: width, height: height)
        dateInIntervalSelectionViewLayout.invalidateLayout()
    }
    
    @IBAction func pressedCancel() {
        output.didPressCancel()
    }
    
    //MARK: - Приватные методы
    
    func refreshPreferredContentSize() {
        
        var size = rootStackView.systemLayoutSizeFitting(UILayoutFittingCompressedSize)
        size.height += topContainer.systemLayoutSizeFitting(UILayoutFittingCompressedSize).height
        
        if #available(iOS 11.0, *), submitContainerView.isHidden {
            size.height += view.safeAreaInsets.bottom
        }
        
        preferredContentSize = size
    }
    
    func configure(cell: JTAppleCell?, with cellState: CellState) {
        handleSelection(for: cell, with: cellState)
        handleTextColor(for: cell, with: cellState)
    }
    
    func handleSelection(for cell: JTAppleCell?, with cellState: CellState) {
        guard let calendarCell = cell as? CalendarViewCell else { return }
        calendarCell.handleSelection(isSelected: output.isInIntervalDate(date: cellState.date))
    }
    
    func handleTextColor(for cell: JTAppleCell?, with cellState: CellState) {
        
        guard let calendarCell = cell as? CalendarViewCell else { return }
        
        if output.isInIntervalDate(date: cellState.date) {
            calendarCell.dateLabel.textColor = UIColorPalette.calendarSelectedDayTextColor
        }
        else {
            if output.isEnabledDate(date: cellState.date) {
                calendarCell.dateLabel.textColor = UIColorPalette.calendarDayTextColor
            }
            else {
                calendarCell.dateLabel.textColor = UIColorPalette.calendarDisabledDayTextColor
            }
        }
    }
    
    //MARK: - ConfigurableModuleController
    
    func configureModule(with object: Any) {
        if let configuration = object as? DateIntervalSelectionModuleConfiguration {
            output.configure(with: configuration)
        }
    }
    
    //MARK: - DateIntervalSelectionViewInput
    
    var isSubmitButtonDisplayed: Bool {
        return !submitContainerView.isHidden
    }
    
    func setup(with style: ConversationStyle) {
        
        calendarSelectionColor = style.controlsTextColor
        
        statusBarStyle = style.isStatusBarDark ? .default : .lightContent
        setNeedsStatusBarAppearanceUpdate()
        
        let attributes: [NSAttributedStringKey : Any] = [
            NSAttributedStringKey.font : UIFont(customName: .graphikLCGRegular, size: navigationItemsFontSize) as Any,
            NSAttributedStringKey.foregroundColor : style.controlsTextColor
        ]
        
        cancelBarButtonItem.setTitleTextAttributes(attributes, for: .normal)
        cancelBarButtonItem.setTitleTextAttributes(attributes, for: .highlighted)
        
        submitKeyboard.buttonsTitleColor = style.controlsTextColor
        submitKeyboard.buttonsShadowColor = style.controlsShadowColor
    }
    
    func displayTitle(_ title: String) {
        navigationItem.title = title
    }
    
    func selectDateInInterval(with itemId: String) {
        
        guard let indexPath = dateInIntervalDataDisplayManager.dataStructure.indexPathOfFirstObjectPassing({
            guard let cellObject = $0 as? DateInIntervalSelectionCellObject else { return false }
            return cellObject.itemId == itemId
        }) else { return }
        
        dateInIntervalSelectionView.selectItem(at: indexPath, animated: false, scrollPosition: .left)
        output.didSelectDateInInterval(with: itemId)
    }
    
    func dispayDateInIntervalSelection(with dataSource: CollectionViewDataSource) {
        
        dateInIntervalDataDisplayManager = dataSource
        dateInIntervalDataDisplayManager.delegate = self
        dateInIntervalDataDisplayManager.assign(to: dateInIntervalSelectionView)
        
        dateInIntervalSelectionView.reloadData()
    }
    
    func displayYears(with dataSource: [HorizontalPickerElementCellObject]) {
        yearsPickerView.display(elements: dataSource)
    }
    
    func scrollToYear(with yearId: String, animated: Bool) {
        yearsPickerView.selectElement(with: yearId, triggerDelegate: false, animated: animated)
    }
    
    func displayMonths(with dataSource: [HorizontalPickerElementCellObject]) {
        monthsPickerView.display(elements: dataSource)
    }
    
    func scrollToMonth(with monthId: String, animated: Bool) {
        monthsPickerView.selectElement(with: monthId, triggerDelegate: false, animated: animated)
    }
    
    func displayCalendar(with calendar: Calendar, minimumDate: Date, maximumDate: Date) {
        
        calendarParameters = ConfigurationParameters(startDate: minimumDate,
                                                     endDate: maximumDate,
                                                     numberOfRows: numberOfRowsInCalendar,
                                                     calendar: calendar,
                                                     generateInDates: .forAllMonths,
                                                     generateOutDates: .tillEndOfGrid,
                                                     firstDayOfWeek: .monday,
                                                     hasStrictBoundaries: false)
        
        if calendarView.calendarDataSource !== self {
            calendarView.calendarDataSource = self
        }
        
        calendarView.deselectAllDates()
        calendarView.reloadData()
    }
    
    func displaySubmitButton(with title: String) {
        
        let submitButton = CommandKeyboardView.KeyboardButtonViewModel(itemId: UUID().uuidString, title: title)
        
        submitKeyboard.buttons = [[submitButton]]
        submitContainerView.isHidden = false
        
        refreshPreferredContentSize()
    }
    
    func hideSubmitButton() {
        submitContainerView.isHidden = true
        refreshPreferredContentSize()
    }
    
    func scrollCalendar(to date: Date, animated: Bool) {
        calendarView.scrollToDate(date, triggerScrollToDateDelegate: false, animateScroll: animated)
    }
    
    func selectDateInCalendar(date: Date?) {
        
        if let dateToSelect = date {
            calendarView.selectDates([dateToSelect])
        }
        else {
            calendarView.deselectAllDates()
            calendarView.reloadData()
        }
    }
    
    func setYearSelection(hidden: Bool) {
        yearsSelectionContainerView.isHidden = hidden
    }
    
    func setCalendarSelection(hidden: Bool) {
        calendarContainerView.isHidden = hidden
    }
    
    //MARK: - UICollectionViewDelegate
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let cellObject = dateInIntervalDataDisplayManager.dataStructure.cellObject(at: indexPath) as? DateInIntervalSelectionCellObject else { return }
        output.didSelectDateInInterval(with: cellObject.itemId)
    }
    
    //MARK: - HorizontalPickerViewDelegate
    
    func pickerView(_ pickerView: HorizontalPickerView, didSelectElementWithId elementId: String) {
        
        if pickerView == yearsPickerView {
            output.didScrollYear(to: elementId)
        }
        else if pickerView == monthsPickerView {
            output.didScrollMonth(to: elementId)
        }
    }
    
    //MARK: - JTAppleCalendarViewDataSource
    
    func configureCalendar(_ calendar: JTAppleCalendarView) -> ConfigurationParameters {
        return calendarParameters
    }
    
    //MARK: - JTAppleCalendarViewDelegate
    
    func calendar(_ calendar: JTAppleCalendarView, cellForItemAt date: Date, cellState: CellState, indexPath: IndexPath) -> JTAppleCell {
        
        let identifier = String(describing: CalendarViewCell.self)
        let cell = calendar.dequeueReusableJTAppleCell(withReuseIdentifier: identifier, for: indexPath)
        
        if let calendarCell = cell as? CalendarViewCell {
            
            calendarCell.dateLabel.isHidden = cellState.dateBelongsTo != .thisMonth
            calendarCell.dateLabel.text = cellState.text
            calendarCell.selectionView.backgroundColor = calendarSelectionColor
            
            configure(cell: cell, with: cellState)
        }
        
        return cell
    }
    
    func calendar(_ calendar: JTAppleCalendarView, willDisplay cell: JTAppleCell, forItemAt date: Date, cellState: CellState, indexPath: IndexPath) {
        configure(cell: cell, with: cellState)
    }
    
    func calendar(_ calendar: JTAppleCalendarView, shouldSelectDate date: Date, cell: JTAppleCell?, cellState: CellState) -> Bool {
        return output.isEnabledDate(date: cellState.date)
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didSelectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        configure(cell: cell, with: cellState)
        output.didSelectInCalendar(date: cellState.date)
        cell?.performBounceAnimation()
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didDeselectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        configure(cell: cell, with: cellState)
    }
    
    func calendar(_ calendar: JTAppleCalendarView, headerViewForDateRange range: (start: Date, end: Date), at indexPath: IndexPath) -> JTAppleCollectionReusableView {
        let identifier = String(describing: CalendarViewWeekDaysHeader.self)
        return calendar.dequeueReusableJTAppleSupplementaryView(withReuseIdentifier: identifier, for: indexPath)
    }
    
    func calendarSizeForMonths(_ calendar: JTAppleCalendarView?) -> MonthSize? {
        return MonthSize(defaultSize: calendarWeekDaysHeaderHeight)
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didScrollToDateSegmentWith visibleDates: DateSegmentInfo) {
        guard let date = visibleDates.monthDates.first?.date else { return }
        output.didScrollCalendar(to: date)
    }
    
    //MARK: - CommandKeyboardViewDelegate
    
    func didPressCommand(with itemId: String) {
        output.didPressSubmit()
    }
}

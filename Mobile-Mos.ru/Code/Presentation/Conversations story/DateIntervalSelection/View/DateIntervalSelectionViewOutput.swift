//
//  DateIntervalSelectionViewOutput.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 22.11.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol DateIntervalSelectionViewOutput: class {
    
    func setupInitialState()
    
    func configure(with configuration: DateIntervalSelectionModuleConfiguration)
    
    func isEnabledDate(date: Date) -> Bool
    
    func isInIntervalDate(date: Date) -> Bool
    
    func didSelectDateInInterval(with itemId: String)
    
    func didScrollYear(to yearId: String)
    
    func didScrollMonth(to monthId: String)
    
    func didScrollCalendar(to date: Date)
    
    func didSelectInCalendar(date: Date)
    
    func didPressCancel()
    
    func didPressSubmit()
}

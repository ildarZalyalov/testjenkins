//
//  DateIntervalSelectionPresenter.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 22.11.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class DateIntervalSelectionPresenter: DateIntervalSelectionViewOutput, DateIntervalSelectionInteractorOutput {
    
    weak var view: DateIntervalSelectionViewInput!
    var router: DateIntervalSelectionRouterInput!
    var interactor: DateIntervalSelectionInteractorInput!
    
    var dateInIntervalDataSourceFactory: DateInIntervalDataSourceFactory!
    var dateInIntervalSelectionItems = [String : DateInIntervalSelectionValue]()
    
    var dateComponentsDataSourceFactory: DateSelectionDataSourceFactory!
    var yearSelectionItems = [String : DateComponents]()
    var monthSelectionItems = [String : DateComponents]()
    
    var moduleConfiguration: DateIntervalSelectionModuleConfiguration!
    var control: ChatMessageDateIntervalControl {
        return moduleConfiguration.control
    }
    
    var dateFromSelection: DateInIntervalSelectionValue!
    var dateToSelection: DateInIntervalSelectionValue!
    
    var displayedYear: Int? {
        didSet {
            guard displayedYear != oldValue else { return }
            refreshMonths()
        }
    }
    
    var calendar: Calendar {
        return dateComponentsDataSourceFactory.calendar
    }
    
    var initialDate = Date()
    var minimumDate: Date!
    var maximumDate: Date!
    
    var selectedDate: Date? {
        
        get {
            
            if dateFromSelection.isSelected {
                return dateFromSelection.selectedDate
            }
            else {
                return dateToSelection.selectedDate
            }
        }
        set {
        
            let oldValue = selectedDate
            
            if dateFromSelection.isSelected {
                dateFromSelection.selectedDate = newValue
            }
            else {
                dateToSelection.selectedDate = newValue
            }
            
            if oldValue != nil && newValue == nil {
                view.selectDateInCalendar(date: nil)
                refreshDateInIntervalSelection()
            }
            
            if dateFromSelection.selectedDate != nil && dateToSelection.selectedDate != nil {
                view.displaySubmitButton(with: control.submitTitle)
            }
            else if view.isSubmitButtonDisplayed {
                view.hideSubmitButton()
            }
        }
    }
    
    //MARK: - Приватные методы
    
    func initializeDateSelection() {
        
        view.displayTitle(control.title)
        view.setup(with: moduleConfiguration.style)
        
        if control.disableSelectYear {
            view.setYearSelection(hidden: true)
        }
        else {
            
            let yearConfiguration = dateComponentsDataSourceFactory.buildDataSourceConfigurationForYears(with: minimumDate, and: maximumDate)
            yearSelectionItems = yearConfiguration.selectionItems
            
            view.displayYears(with: yearConfiguration.dataSource)
        }
        
        refreshMonths()
        
        if control.disableSelectDays {
            view.setCalendarSelection(hidden: true)
        }
        else {
            view.displayCalendar(with: calendar, minimumDate: minimumDate, maximumDate: maximumDate)
        }
        
        selectDateInPickers(date: initialDate, animated: false)
        
        if control.disableSelectDays {
            selectedDate = initialDate
            refreshDateInIntervalSelection()
        }
        else {
            view.scrollCalendar(to: initialDate, animated: false)
            if let date = selectedDate {
                view.selectDateInCalendar(date: date)
            }
        }
        
        if dateFromSelection.selectedDate != nil && dateToSelection.selectedDate != nil && !view.isSubmitButtonDisplayed {
            view.displaySubmitButton(with: control.submitTitle)
        }
    }
    
    func didFinishComputingDateSelectionParameters(parameters: DateSelectionParameters) {

        minimumDate = parameters.minimumDate
        maximumDate = parameters.maximumDate
        initialDate = parameters.initialDate
        displayedYear = parameters.displayedYear
        
        initializeDateSelection()
    }
    
    func selectDateInPickers(date: Date, animated: Bool) {
        
        let dateComponents = calendar.dateComponents([.year, .month], from: date)
        
        if !control.disableSelectYear, let year = yearSelectionItems.filter({ $0.value.year == dateComponents.year }).first {
            displayedYear = year.value.year
            view.scrollToYear(with: year.key, animated: animated)
        }
        
        if let month = monthSelectionItems.filter({ $0.value.month == dateComponents.month }).first {
            view.scrollToMonth(with: month.key, animated: animated)
        }
    }
    
    func refreshDateInIntervalSelection() {
        
        let selectionConfiguration = dateInIntervalDataSourceFactory.buildDataSourceConfiguration(with: dateFromSelection, and: dateToSelection, selectionColor: moduleConfiguration.style.controlsTextColor)
        dateInIntervalSelectionItems = selectionConfiguration.selectionItems
        
        view.dispayDateInIntervalSelection(with: selectionConfiguration.dataSource)
    }
    
    func refreshMonths() {
        
        let limit = DateSelectionDataSourceMonthsLimit(displayedYear: displayedYear, minimumDate: minimumDate, maximumDate: maximumDate)
        let monthConfiguration = dateComponentsDataSourceFactory.buildDataSourceConfigurationForMonths(with: limit)
        monthSelectionItems = monthConfiguration.selectionItems
        
        view.displayMonths(with: monthConfiguration.dataSource)
    }
    
    fileprivate func dropSelectedDateIfNeeded(displayedMonthDate: Date) {
        
        guard let selected = selectedDate else { return }
        
        if calendar.compare(displayedMonthDate, to: selected, toGranularity: .month) != .orderedSame {
            selectedDate = nil
        }
    }
    
    //MARK: - DateIntervalSelectionViewOutput
    
    func setupInitialState() {
        
        dateFromSelection = DateInIntervalSelectionValue(with: .dateFrom)
        dateFromSelection.isSelected = control.initialDateFrom == nil && control.initialFloatingDateFrom == nil
        dateFromSelection.selectedDate = control.initialDateFrom ?? interactor.date(for: control.initialFloatingDateFrom, with: calendar)
        
        dateToSelection = DateInIntervalSelectionValue(with: .dateTo)
        dateToSelection.isSelected = !dateFromSelection.isSelected
        dateToSelection.selectedDate = control.initialDateTo ?? interactor.date(for: control.initialFloatingDateTo, with: calendar)
        
        dateInIntervalDataSourceFactory.disableSelectYear = control.disableSelectYear
        dateInIntervalDataSourceFactory.disableSelectDays = control.disableSelectDays
        
        refreshDateInIntervalSelection()
        if dateFromSelection.isSelected {
            interactor.computeFromDateSelectionParameters(for: control, with: calendar, selectedFromDate: dateFromSelection.selectedDate, selectedToDate: dateToSelection.selectedDate)
        }
        else {
            interactor.computeToDateSelectionParameters(for: control, with: calendar, selectedFromDate: dateFromSelection.selectedDate, selectedToDate: dateToSelection.selectedDate)
        }
    }
    
    func configure(with configuration: DateIntervalSelectionModuleConfiguration) {
        self.moduleConfiguration = configuration
    }
    
    func isEnabledDate(date: Date) -> Bool {
        
        let minimumResult = calendar.compare(date, to: minimumDate, toGranularity: .day)
        let maximumResult = calendar.compare(date, to: maximumDate, toGranularity: .day)
        
        return (minimumResult == .orderedSame || minimumResult == .orderedDescending) &&
            (maximumResult == .orderedSame || maximumResult == .orderedAscending)
    }
    
    func isInIntervalDate(date: Date) -> Bool {
        
        guard dateFromSelection.selectedDate != nil else { return false }
        
        let dateFrom = dateFromSelection.selectedDate!
        let minimumResult = calendar.compare(date, to: dateFrom, toGranularity: .day)
        
        guard dateToSelection.selectedDate != nil else { return minimumResult == .orderedSame }
        let dateTo = dateToSelection.selectedDate!
        let maximumResult = calendar.compare(date, to: dateTo, toGranularity: .day)
        
        return (minimumResult == .orderedSame || minimumResult == .orderedDescending) &&
            (maximumResult == .orderedSame || maximumResult == .orderedAscending)
    }
    
    func didSelectDateInInterval(with itemId: String) {
        
        guard let selection = dateInIntervalSelectionItems[itemId] else { return }
        
        switch selection.type {
        case .dateFrom:
            guard !dateFromSelection.isSelected else { return }
            dateFromSelection.isSelected = true
            dateToSelection.isSelected = false
            interactor.computeFromDateSelectionParameters(for: control, with: calendar, selectedFromDate: dateFromSelection.selectedDate, selectedToDate: dateToSelection.selectedDate)
        case .dateTo:
            guard !dateToSelection.isSelected else { return }
            dateFromSelection.isSelected = false
            dateToSelection.isSelected = true
            interactor.computeToDateSelectionParameters(for: control, with: calendar, selectedFromDate: dateFromSelection.selectedDate, selectedToDate: dateToSelection.selectedDate)
        }
        
        refreshDateInIntervalSelection()
    }
    
    func didScrollYear(to yearId: String) {
        
        guard let year = yearSelectionItems[yearId] else { return }
        guard year.year != displayedYear else { return }
        guard var yearDate = calendar.date(from: year) else { return }
        
        var scrolledToPreviousYear = false
        if displayedYear != nil && year.year != nil {
            scrolledToPreviousYear = displayedYear! > year.year!
        }
        
        displayedYear = year.year
        
        if scrolledToPreviousYear {
            yearDate = calendar.date(byAdding: .year, value: 1, to: yearDate) ?? yearDate
            yearDate = calendar.date(byAdding: .month, value: -1, to: yearDate) ?? yearDate
        }
        
        // скроллинг года вперед перелистывает месяцы на начало, скроллинг назад - на последний месяц, но, если год в прошлом есть, значит как минимум последний месяц доступен, иначе имеем "дыру"
        // поэтому проверяем только минимальную дату
        if calendar.compare(yearDate, to: minimumDate, toGranularity: .month) == .orderedAscending {
            yearDate = minimumDate
        }
        
        if scrolledToPreviousYear, let lastMonthComponent = calendar.dateComponents([.month], from: yearDate).month, let lastMonthItemId = monthSelectionItems.first(where: { $0.value.month == lastMonthComponent })?.key {
            view.scrollToMonth(with: lastMonthItemId, animated: false)
        }
        
        guard !control.disableSelectDays else {
            selectedDate = yearDate
            refreshDateInIntervalSelection()
            return
        }
        
        dropSelectedDateIfNeeded(displayedMonthDate: yearDate)
        view.scrollCalendar(to: yearDate, animated: false)
    }
    
    func didScrollMonth(to monthId: String) {
        
        guard var month = monthSelectionItems[monthId] else { return }
        month.year = displayedYear
        guard let monthDate = calendar.date(from: month) else { return }
        
        if control.disableSelectDays {
            didSelectInCalendar(date: monthDate)
        }
        else {
            view.scrollCalendar(to: monthDate, animated: true)
            dropSelectedDateIfNeeded(displayedMonthDate: monthDate)
        }
    }
    
    func didScrollCalendar(to date: Date) {
        selectDateInPickers(date: date, animated: true)
        dropSelectedDateIfNeeded(displayedMonthDate: date)
    }
    
    func didSelectInCalendar(date: Date) {
        
        guard date != selectedDate else { return }
        
        selectedDate = date
        refreshDateInIntervalSelection()
        
        guard !control.disableSelectDays else { return }
        
        if dateFromSelection.isSelected {
            guard let itemId = dateInIntervalSelectionItems.first(where: { $0.value.type == .dateTo })?.key else { return }
            view.selectDateInInterval(with: itemId)
        }
        else {
            view.displayCalendar(with: calendar, minimumDate: minimumDate, maximumDate: maximumDate)
        }
    }
    
    func didPressCancel() {
        router.closeModule(completion: nil)
    }
    
    func didPressSubmit() {
        
        guard let fromDate = dateFromSelection.selectedDate else { return }
        guard let toDate = dateToSelection.selectedDate else { return }
        
        let handler = moduleConfiguration.resultHandler
        
        let messageText = interactor.controlValue(for: fromDate, and: toDate, with: control.dateOutputFormat, and: control.dateSeparatorString)
        let messageContext = moduleConfiguration.messageContext
        
        let callBackData = interactor.controlCallbackValue(for: fromDate, and: toDate, callbackData: moduleConfiguration.callbackData, format: control.dateOutputFormat, separatorString: control.dateSeparatorString)
        let message = moduleConfiguration.message
        let embeddedMessage = moduleConfiguration.embeddedMessage
        
        router.closeModule {
            
            guard handler != nil else { return }
            
            if messageText != nil && messageContext != nil {
                handler!.handleSendMessage(with: messageText!, context: messageContext!)
            }
            
            if callBackData != nil {
                handler!.handleSendCallback(with: callBackData!, for: message, embeddedMessage: embeddedMessage)
            }
        }
    }
    
    //MARK: - DateIntervalSelectionInteractorOutput
    
    func didFinishComputingFromDateSelectionParameters(parameters: DateSelectionParameters) {
        didFinishComputingDateSelectionParameters(parameters: parameters)
    }
    
    func didFinishComputingToDateSelectionParameters(parameters: DateSelectionParameters) {
        didFinishComputingDateSelectionParameters(parameters: parameters)
    }
}

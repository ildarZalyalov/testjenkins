//
//  DateIntervalSelectionModuleConfiguration.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 22.11.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Конфигурация модуля календаря с выбором периода времени
struct DateIntervalSelectionModuleConfiguration {
    
    /// Информация о контроле календаря
    var control: ChatMessageDateIntervalControl
    
    /// Стиль текущего чата
    var style: ConversationStyle
    
    /// Контекст для отправки сообщения (если есть)
    var messageContext: String?
    
    // Данные для коллбэк запроса (если есть)
    var callbackData: String?
    
    /// Сообщение, с которым связан контрол (если есть)
    var message: ChatMessage?
    
    /// Вложенное сообщение, если сообщение является каруселью
    var embeddedMessage: ChatMessage?
    
    /// Обработчик результата работы контрола
    weak var resultHandler: ChatCustomControlResultHandler?
    
    init(control: ChatMessageDateIntervalControl, style: ConversationStyle) {
        self.control = control
        self.style = style
    }
}

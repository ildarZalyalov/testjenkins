//
//  DateIntervalSelectionRouter.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 22.11.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class DateIntervalSelectionRouter: DateIntervalSelectionRouterInput {
    
    weak var transitionHandler: UIViewController!
    
    let unwindToChatSegueIdentifier = "unwindToChat"
    
    //MARK: - DateIntervalSelectionRouterInput
    
    func closeModule(completion: (() -> ())?) {
        transitionHandler.performSegue(withIdentifier: unwindToChatSegueIdentifier, sender: self)
        transitionHandler.transitionCoordinator?.animate(alongsideTransition: nil, completion: { _ in
            completion?()
        })
    }
}

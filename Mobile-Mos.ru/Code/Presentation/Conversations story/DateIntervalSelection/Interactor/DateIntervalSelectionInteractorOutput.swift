//
//  DateIntervalSelectionInteractorOutput.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 22.11.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol DateIntervalSelectionInteractorOutput: class {
    
    func didFinishComputingFromDateSelectionParameters(parameters: DateSelectionParameters)
    
    func didFinishComputingToDateSelectionParameters(parameters: DateSelectionParameters)
}

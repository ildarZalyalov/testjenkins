//
//  DateIntervalSelectionInteractorInput.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 22.11.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol DateIntervalSelectionInteractorInput: class {
    
    func date(for floatingDate: ChatMessageCalendarFloatingDate?,
              with calendar: Calendar) -> Date?
    
    func computeFromDateSelectionParameters(for control: ChatMessageDateIntervalControl,
                                            with calendar: Calendar,
                                            selectedFromDate: Date?,
                                            selectedToDate: Date?)
    
    func computeToDateSelectionParameters(for control: ChatMessageDateIntervalControl,
                                          with calendar: Calendar,
                                          selectedFromDate: Date?,
                                          selectedToDate: Date?)
    
    func controlValue(for fromDate: Date,
                      and toDate: Date,
                      with format: String,
                      and separatorString: String) -> String?
    
    func controlCallbackValue(for fromDate: Date,
                              and toDate: Date,
                              callbackData: String?,
                              format: String?,
                              separatorString: String) -> String?
}

//
//  DateIntervalSelectionInteractor.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 22.11.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class DateIntervalSelectionInteractor: DateIntervalSelectionInteractorInput {
    
    weak var output: DateIntervalSelectionInteractorOutput!
    
    let yearPlusDayAmountOfSeconds: TimeInterval = 60 * 60 * 24 * 366
    
    lazy var dateFormatter: DateFormatter = {
        
        let localeIdentifier = "en_US_POSIX"
        let timeZoneAbbreviation = "UTC"
        
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: localeIdentifier)
        formatter.timeZone = TimeZone(abbreviation: timeZoneAbbreviation)
        formatter.dateStyle = .none
        formatter.timeStyle = .short
        return formatter
    }()
    
    //MARK: - Приватные методы
    
    func constrain(initialDate: inout Date,
                   minimumDate: inout Date,
                   maximumDate: inout Date,
                   displayedYear: inout Int?,
                   for control: ChatMessageDateIntervalControl,
                   with calendar: Calendar) {
        
        let componentsForTomorrow = DateComponents(day: 1)
        let componentsForOneMinuteBefore = DateComponents(minute: -1)
        if let oneDayAfterMaximumDate = calendar.date(byAdding: componentsForTomorrow, to: maximumDate), let lastMinuteOfMaximumDate = calendar.date(byAdding: componentsForOneMinuteBefore, to: oneDayAfterMaximumDate) {
            maximumDate = lastMinuteOfMaximumDate
        }
        
        if maximumDate < minimumDate {
            maximumDate = Date(timeInterval: 2 * yearPlusDayAmountOfSeconds, since: minimumDate)
        }
        
        if initialDate < minimumDate || initialDate > maximumDate {
            initialDate = maximumDate
        }
        
        let dateComponents = calendar.dateComponents([.year], from: initialDate)
        displayedYear = dateComponents.year
        
        guard control.disableSelectYear else { return }
        
        let minimumComponents = calendar.dateComponents([.year], from: initialDate)
        guard let firstDayDate = calendar.date(from: minimumComponents) else { return }
        
        let lastMonthComponents = DateComponents(calendar: calendar, year: displayedYear, month: calendar.monthSymbols.count)
        let componentsToSubtract = DateComponents(month: 1, day: -1)
        
        guard let lastMonthDate = calendar.date(from: lastMonthComponents) else { return }
        guard let lastDayDate = calendar.date(byAdding: componentsToSubtract, to: lastMonthDate) else { return }
        
        minimumDate = firstDayDate
        maximumDate = lastDayDate
    }
    
    //MARK: - DateIntervalSelectionInteractorInput
    
    func date(for floatingDate: ChatMessageCalendarFloatingDate?,
              with calendar: Calendar) -> Date? {
        
        guard let date = floatingDate else { return nil }
        
        switch date {
        case .today:
            return Date()
        case .previousMonth:
            return calendar.date(byAdding: .month, value: -1, to: Date())
        case .nextMonth:
            return calendar.date(byAdding: .month, value: 1, to: Date())
        }
    }
    
    func computeFromDateSelectionParameters(for control: ChatMessageDateIntervalControl,
                                            with calendar: Calendar,
                                            selectedFromDate: Date?,
                                            selectedToDate: Date?) {
        
        var initialDate: Date
        var minimumDate: Date
        var maximumDate: Date
        var displayedYear: Int?
        
        defer {
            
            let parameters = DateSelectionParameters(initialDate: initialDate,
                                                     minimumDate: minimumDate,
                                                     maximumDate: maximumDate,
                                                     displayedYear: displayedYear)
            
            output.didFinishComputingFromDateSelectionParameters(parameters: parameters)
        }
        
        let initialFloatingDate = date(for: control.initialFloatingDateFrom, with: calendar)
        let minimumFloatingDate = date(for: control.minimumFloatingDate, with: calendar)
        let maximumFloatingDate = date(for: control.maximumFloatingDate, with: calendar)
        
        initialDate = selectedFromDate ?? control.initialDateFrom ?? initialFloatingDate ?? Date()
        minimumDate = control.minimumDate ?? minimumFloatingDate ?? Date(timeInterval: -yearPlusDayAmountOfSeconds, since: initialDate)
        maximumDate = selectedToDate ?? control.maximumDate ?? maximumFloatingDate ?? Date(timeInterval: yearPlusDayAmountOfSeconds, since: initialDate)
        
        constrain(initialDate: &initialDate, minimumDate: &minimumDate, maximumDate: &maximumDate, displayedYear: &displayedYear, for: control, with: calendar)
    }
    
    func computeToDateSelectionParameters(for control: ChatMessageDateIntervalControl,
                                          with calendar: Calendar,
                                          selectedFromDate: Date?,
                                          selectedToDate: Date?) {
        
        var initialDate: Date
        var minimumDate: Date
        var maximumDate: Date
        var displayedYear: Int?
        
        defer {
            
            let parameters = DateSelectionParameters(initialDate: initialDate,
                                                     minimumDate: minimumDate,
                                                     maximumDate: maximumDate,
                                                     displayedYear: displayedYear)
            
            output.didFinishComputingToDateSelectionParameters(parameters: parameters)
        }
        
        let initialFloatingDate = date(for: control.initialFloatingDateTo, with: calendar)
        let minimumFloatingDate = date(for: control.minimumFloatingDate, with: calendar)
        let maximumFloatingDate = date(for: control.maximumFloatingDate, with: calendar)
        
        initialDate = selectedToDate ?? control.initialDateTo ?? initialFloatingDate ?? Date()
        minimumDate = selectedFromDate ?? control.minimumDate ?? minimumFloatingDate ?? Date(timeInterval: -yearPlusDayAmountOfSeconds, since: initialDate)
        maximumDate = control.maximumDate ?? maximumFloatingDate ?? Date(timeInterval: yearPlusDayAmountOfSeconds, since: initialDate)
        
        constrain(initialDate: &initialDate, minimumDate: &minimumDate, maximumDate: &maximumDate, displayedYear: &displayedYear, for: control, with: calendar)
        
        let today = Date()
        if today >= minimumDate && today <= maximumDate && initialDate > today {
            initialDate = today
        }
    }
    
    func controlValue(for fromDate: Date,
                      and toDate: Date,
                      with format: String,
                      and separatorString: String) -> String? {
        
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: fromDate) + separatorString + dateFormatter.string(from: toDate)
    }
    
    func controlCallbackValue(for fromDate: Date,
                              and toDate: Date,
                              callbackData: String?,
                              format: String?,
                              separatorString: String) -> String? {
        
        guard let data = callbackData else { return nil }
        
        dateFormatter.dateFormat = format
        let controlValue = dateFormatter.string(from: fromDate) + separatorString + dateFormatter.string(from: toDate)
        
        let jsonDictionary: [String : Any] = [
            "callback" : data,
            "control_value" : controlValue
        ]
        
        guard let jsonData = try? JSONSerialization.data(withJSONObject: jsonDictionary, options: []) else { return nil }
        
        return String(data: jsonData, encoding: .utf8)
    }
}

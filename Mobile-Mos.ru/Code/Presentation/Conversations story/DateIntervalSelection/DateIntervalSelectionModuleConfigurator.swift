//
//  DateIntervalSelectionModuleConfigurator.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 22.11.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import UIKit

class DateIntervalSelectionModuleConfigurator: BaseModuleConfigurator {
    
    override func configureModule(for view: UIViewController) {
        
        let viewController = view as! DateIntervalSelectionViewController
        let presenter = DateIntervalSelectionPresenter()
        let interactor = DateIntervalSelectionInteractor()
        let router = DateIntervalSelectionRouter()
        
        let dateInIntervalDataSourceFactory = DateInIntervalDataSourceFactoryImplementation()
        let dateDataSourceFactory = DateSelectionDataSourceFactoryImplementation()
        
        viewController.output = presenter
        
        presenter.view = viewController
        presenter.interactor = interactor
        presenter.router = router
        presenter.dateInIntervalDataSourceFactory = dateInIntervalDataSourceFactory
        presenter.dateComponentsDataSourceFactory = dateDataSourceFactory
        
        interactor.output = presenter
        
        router.transitionHandler = viewController
    }
}

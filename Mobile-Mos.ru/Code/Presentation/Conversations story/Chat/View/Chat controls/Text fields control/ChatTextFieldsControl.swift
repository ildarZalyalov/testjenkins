//
//  ChatTextFieldsControl.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 28.09.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import UIKit

/// Вьюха контрола с несколькими полями ввода
class ChatTextFieldsControl: UIView, UITextFieldDelegate {
    
    /// Текстовое поле, используемое в контроле
    fileprivate class ChatTextField: UITextField {
        
        /// Идентификатор поля
        var inputId: String = String()
        
        override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
            return inputView == nil ? super.canPerformAction(action, withSender: sender) : false
        }
    }
    
    fileprivate static let nibFile = UINib(nibName: String(describing: ChatTextFieldsControl.self), bundle: nil)
    
    /// Форматтер дат для полей с вводом через барабан дат
    fileprivate static let dateFormatter: DateFormatter = {
        
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "ru_RU")
        formatter.timeZone = TimeZone.autoupdatingCurrent
        formatter.dateFormat = "dd.MM.yyyy"
        
        return formatter
    }()
    
    static func loadNib() -> ChatTextFieldsControl {
        return ChatTextFieldsControl.nibFile.instantiate(withOwner: nil, options: nil).first as! ChatTextFieldsControl
    }
    
    /// Высота полей ввода
    let textFieldsHeight: CGFloat = 40
    
    /// Высота сепараторов между полями вввода
    let textFieldsSeparatorHeight: CGFloat = UIScreen.main.isRetina ? 0.5 : 1
    
    /// Отступ сверху для первого поля ввода
    let textFieldsTopOffset: CGFloat = 1
    
    /// Максимальное кол-во символов, которое можно ввести в каждое поле
    var maxTextSymbolsCount: Int?
    
    fileprivate var textFields = [ChatTextField]()
    fileprivate var textFieldHintsById = [String : String]()
    
    @IBOutlet fileprivate(set) weak var commandsButton: UIButton!
    @IBOutlet fileprivate(set) weak var sendButton: UIButton!
    @IBOutlet fileprivate weak var textFieldsContainer: UIView!
    @IBOutlet fileprivate weak var hintLabel: UILabel!
    
    @IBOutlet fileprivate var textFieldsBorderViewToSendButtonConstraint: NSLayoutConstraint!
    @IBOutlet fileprivate var textFieldsBorderViewTrailingConstraint: NSLayoutConstraint!
    @IBOutlet fileprivate weak var textFieldsBorderViewToHintLabelConstraint: NSLayoutConstraint!
    
    /// Значения в полях ввода по идентификаторам полей
    var values: [String : String] {
        return textFields.reduce([String : String](), { $0 + [$1.inputId : $1.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) ?? String()] })
    }
    
    /// Отображается ли системная клавиатура
    var isSystemKeyboardDisplayed: Bool {
        
        for textField in textFields {
            guard !textField.isFirstResponder else { return true }
        }
        
        return false
    }
    
    //MARK: - Приватные методы
    
    /// Добавить текстовое поле
    ///
    /// - Parameters:
    ///   - textInput: описание поля ввода
    ///   - index: индекс текстового поля в массиве полей
    ///   - yOrigin: сдвиг текстового поля по вертикальной оси
    /// - Returns: созданное текстовое поле
    fileprivate func addTextField(with textInput: ChatMessageTextInputControl.TextInput, and index: Int, at yOrigin: CGFloat) -> ChatTextField {
        
        let textField = ChatTextField(frame: CGRect(x: 0,
                                                    y: yOrigin,
                                                    width: textFieldsContainer.width,
                                                    height: textFieldsHeight))
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.font = UIFont(customName: .graphikLCGRegular, size: 15)
        textField.textAlignment = .left
        textField.textColor = UIColorPalette.textInputsControlTextColor
        textField.placeholder = textInput.placeholder
        textField.isSecureTextEntry = textInput.isSecure
        textField.inputId = textInput.inputId
        textField.text = textInput.text
        textField.delegate = self
        
        switch textInput.type {
        case .text:
            textField.keyboardType = .default
        case .digits:
            textField.keyboardType = .numberPad
        case .date:
            addDatePickerInput(for: textField, at: index)
        }
        
        textFieldHintsById[textInput.inputId] = textInput.title
        textFieldsContainer.addSubview(textField)
        
        let views = ["textField": textField]
        var constraints = NSLayoutConstraint.visualConstraints(with: "H:|-0-[textField]-0-|", options: [], metrics: nil, views: views)
        constraints += [NSLayoutConstraint(item: textField, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1, constant: textFieldsHeight)]
        textFieldsContainer.addConstraints(constraints)
        
        return textField
    }
    
    /// Добавить сепаратор
    ///
    /// - Parameter yOrigin: сдвиг сепаратора по вертикальной оси
    /// - Returns: созданный сепаратор
    fileprivate func addSeparator(at yOrigin: CGFloat) -> UIView {
        
        let separator = UIView(frame: CGRect(x: 0,
                                             y: yOrigin,
                                             width: textFieldsContainer.width,
                                             height: textFieldsSeparatorHeight))
        separator.translatesAutoresizingMaskIntoConstraints = false
        
        separator.backgroundColor = UIColorPalette.textInputsControlSeparatorColor
        
        textFieldsContainer.addSubview(separator)
        
        let views = ["separator": separator]
        var constraints = NSLayoutConstraint.visualConstraints(with: "H:|-0-[separator]-0-|", options: [], metrics: nil, views: views)
        constraints += [NSLayoutConstraint(item: separator, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1, constant: textFieldsSeparatorHeight)]
        textFieldsContainer.addConstraints(constraints)
        
        return separator
    }
    
    /// Добавить барабан для ввода дат к полю
    ///
    /// - Parameters:
    ///   - textField: текстовое поле
    ///   - index: индекс текстового поля
    fileprivate func addDatePickerInput(for textField: UITextField, at index: Int) {
        
        let input = DatePickerInputView(frame: CGRect.zero)
        input.datePicker.locale = Locale.autoupdatingCurrent
        input.datePicker.tag = index
        
        input.datePicker.addTarget(self, action: #selector(updateTextFieldFromDatePicker(datePicker:)), for: .valueChanged)
        textField.inputView = input
    }
    
    /// Добавить констрейны текстовых полей по вертикальной оси
    ///
    /// - Parameters:
    ///   - textFields: текстовые поля
    ///   - textFieldSeparators: сепараторы
    fileprivate func addVerticalConstraints(for textFields: [UITextField], and textFieldSeparators: [UIView]) {
        
        var fields = textFields
        var separators = textFieldSeparators
        var views = [String : AnyObject]()
        var constraintsFormat = "V:|-\(textFieldsTopOffset)-"
        
        while !fields.isEmpty {
            
            if let textField = fields.first {
                
                let textFieldName = "textField\(fields.count)"
                views[textFieldName] = textField
                constraintsFormat += "[\(textFieldName)]-0-"
                
                fields.removeFirst()
            }
            
            if let separator = separators.first {
                
                let separatorName = "separator\(separators.count)"
                views[separatorName] = separator
                constraintsFormat += "[\(separatorName)]-0-"
                
                separators.removeFirst()
            }
        }
        
        constraintsFormat += "|"
        
        textFieldsContainer.addConstraints(NSLayoutConstraint.visualConstraints(with: constraintsFormat, options: [], metrics: nil, views: views))
    }
    
    /// Обновить состояние кнопки отправки
    ///
    /// - Parameters:
    ///   - activeField: активное поле (в котором на данный момент осуществляется ввод текста, если есть)
    ///   - range: измененный промежуток текста (если есть)
    ///   - replacement: текст, на который был заменен текст из range (если есть)
    fileprivate func refreshSendButton(activeField: UITextField? = nil, range: NSRange = NSRange(), replacement: String = "") {
        
        let allFieldsHaveValues = textFields.filter {
            
            var text = $0.text
            if $0 === activeField {
                text = ($0.text as NSString?)?.replacingCharacters(in: range, with: replacement)
            }
            
            return text?.isEmpty ?? true
            
        }.count == 0
        
        sendButton.isEnabled = allFieldsHaveValues
    }
    
    /// Обновить состояние лейбла с подсказкой
    ///
    /// - Parameter isHidden: спрятать ли лейбл
    fileprivate func setHintLabelHidden(_ isHidden: Bool) {
        
        guard hintLabel.isHidden != isHidden else { return }
        
        hintLabel.isHidden = isHidden
        
        textFieldsBorderViewToHintLabelConstraint.priority = isHidden ? UILayoutPriority.defaultLow : UILayoutPriority.defaultHigh
        setNeedsLayout()
        layoutIfNeeded()
    }

    /// Обновить значение в текстовом поле при выборе даты в барабане дат
    @objc
    fileprivate func updateTextFieldFromDatePicker(datePicker: UIDatePicker) {
        guard let textField = textFields[safe: datePicker.tag] else { return }
        textField.text = ChatTextFieldsControl.dateFormatter.string(from: datePicker.date)
        refreshSendButton()
    }
    
    /// 💩 костыль - отключаем анимации при активации другого поля, чтобы не дергалась панель с подсказками над клавиатурой
    fileprivate func disableAnimationsTemporaryIfNeeded() {
        guard !textFields.filter({ $0.isFirstResponder }).isEmpty else { return }
        UIView.setAnimationsEnabled(false)
        DispatchQueue.main.async {
            UIView.setAnimationsEnabled(true)
        }
    }
    
    override var canBecomeFirstResponder: Bool {
        return false
    }
    
    @discardableResult
    override func becomeFirstResponder() -> Bool {
        textFields.first?.becomeFirstResponder()
        return false
    }
    
    override func resignFirstResponder() -> Bool {
        endEditing(true)
        return true
    }
    
    //MARK: - Открытые методы
    
    /// Сконфигурировать вьюху
    ///
    /// - Parameter control: информация о контроле с несколькими полями ввода
    func configure(with control: ChatMessageTextInputControl) {
        
        textFields.removeAll()
        textFieldsContainer.subviews.forEach {
            $0.resignFirstResponder()
            $0.removeFromSuperview()
        }
        
        var yOrigin: CGFloat = textFieldsTopOffset
        var separators = [UIView]()
    
        for (index, input) in control.inputs.enumerated() {
            
            let textField = addTextField(with: input, and: index, at: yOrigin)
            textFields.append(textField)
            yOrigin += textField.height
            
            guard index < control.inputs.count - 1 else { continue }
            
            let separator = addSeparator(at: yOrigin)
            separators.append(separator)
            yOrigin += separator.height
        }
        
        textFieldsBorderViewToSendButtonConstraint.isActive = textFields.count > 1
        textFieldsBorderViewTrailingConstraint.isActive = textFields.count <= 1
        
        addVerticalConstraints(for: textFields, and: separators)
        
        refreshSendButton()
        
        if let hint = control.title, !hint.isEmpty {
            hintLabel.text = hint
            setHintLabelHidden(false)
        }
        else {
            setHintLabelHidden(true)
        }
    }
    
    /// Очистить текст в полях
    func clearTextInFields() {
        for field in textFields {
            field.text = nil
        }
    }
    
    //MARK: - UITextFieldDelegate
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        disableAnimationsTemporaryIfNeeded()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        guard let field = textField as? ChatTextField else { return }
        
        if let hint = textFieldHintsById[field.inputId], !hint.isEmpty {
            hintLabel.text = hint
            setHintLabelHidden(false)
        }
        else {
            setHintLabelHidden(true)
        }
        
        if let datePickerInput = field.inputView as? DatePickerInputView {
            updateTextFieldFromDatePicker(datePicker: datePickerInput.datePicker)
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        var shouldChange = true
        
        if let maxCount = maxTextSymbolsCount, let currentText = textField.text as NSString? {
            shouldChange = currentText.replacingCharacters(in: range, with: string).count <= maxCount
        }
        
        if shouldChange {
            refreshSendButton(activeField: textField, range: range, replacement: string)
        }
        
        return shouldChange
    }
}

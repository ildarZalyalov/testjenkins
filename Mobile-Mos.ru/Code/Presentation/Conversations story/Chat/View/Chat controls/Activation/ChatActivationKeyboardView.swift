//
//  ChatActivationKeyboardView.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 26.09.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import UIKit

class ChatActivationKeyboardView: UIView {
    
    fileprivate static let nibFile = UINib(nibName: String(describing: ChatActivationKeyboardView.self), bundle: nil)
    
    static func loadNib() -> ChatActivationKeyboardView {
        return ChatActivationKeyboardView.nibFile.instantiate(withOwner: nil, options: nil).first as! ChatActivationKeyboardView
    }
    
    @IBOutlet fileprivate weak var activateButton: KeyboardButtonControl!
    @IBOutlet fileprivate weak var activateButtonShadowView: UIImageView!
    @IBOutlet fileprivate weak var activateButtonBackgroundView: UIImageView!
    
    /// Высота разделителя сверху клавиатуры
    let topSeparatorHeight: CGFloat = UIScreen.main.isRetina ? 0.5 : 1
    
    /// Цвет заголовка кнопки
    var buttonTitleColor: UIColor = UIColorPalette.commandKeyboardButtonsTitleDefaultColor {
        didSet {
            activateButton.setTitleColor(buttonTitleColor, for: .normal)
            activateButton.normalTintColor = buttonTitleColor
        }
    }
    
    /// Цвет заголовка кнопки при фокусе на них
    var buttonHighlightedTitleColor: UIColor = UIColorPalette.commandKeyboardButtonsHighlightedTitleDefaultColor {
        didSet {
            activateButton.setTitleColor(buttonHighlightedTitleColor, for: .highlighted)
            activateButton.highlightedTintColor = buttonHighlightedTitleColor
        }
    }
    
    /// Цвет теней кнопки
    var buttonShadowColor: UIColor = UIColorPalette.commandKeyboardButtonsShadowDefaultColor {
        didSet {
            activateButtonShadowView.tintColor = buttonShadowColor
        }
    }
    
    var activateButtonTitle: String? {
        return activateButton.title(for: .normal)
    }
    
    var onChatActivation: (() -> ())?
    
    override func awakeFromNib() {
        
        let topSeparator = UIView(frame: CGRect(x: 0, y: 0, width: bounds.width, height: topSeparatorHeight))
        topSeparator.autoresizingMask = [.flexibleWidth, .flexibleBottomMargin]
        topSeparator.backgroundColor = UIColorPalette.commandKeyboardTopSeparatorColor
        addSubview(topSeparator)
        
        activateButtonShadowView.image = #imageLiteral(resourceName: "chatActivationButtonShadow").resizableImage(withCapInsets: UIEdgeInsets(top: 34, left: 39, bottom: 34, right: 39))
        activateButtonBackgroundView.image = #imageLiteral(resourceName: "chatActivationButtonBackground").resizableImage(withCapInsets: UIEdgeInsets(top: 24, left: 29, bottom: 24, right: 29))
        
        activateButton.setTitleColor(buttonTitleColor, for: .normal)
        activateButton.setTitleColor(buttonHighlightedTitleColor, for: .highlighted)
        
        activateButton.isExclusiveTouch = true
        activateButton.normalTintColor = buttonTitleColor
        activateButton.highlightedTintColor = buttonHighlightedTitleColor
        activateButtonBackgroundView.tintColor = buttonHighlightedTitleColor
        activateButtonShadowView.tintColor = buttonShadowColor

        activateButton.isHighlightedChanged = { [weak self] _, isHighlighted in
            guard let strongSelf = self else { return }
            strongSelf.activateButtonBackgroundView.tintColor = isHighlighted ? strongSelf.buttonTitleColor : strongSelf.buttonHighlightedTitleColor
        }
    }
    
    @objc
    @IBAction fileprivate func activateButtonPressed() {
        onChatActivation?()
    }
}

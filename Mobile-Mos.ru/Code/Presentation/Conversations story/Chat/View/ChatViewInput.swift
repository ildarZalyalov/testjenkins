//
//  ChatViewInput.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 27.04.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Тип обновления чата
///
/// - regular: обычное быстрое обновление (будут добавлены/удалены/передвинуты сообщения, а также перерисованы сообщения в зоне видимости)
/// - pagination: пагинация (во время подгрузки более старых сообщений)
/// - reload: полное обновление сообщений
enum ChatViewUpdateType {
    case regular
    case pagination
    case reload
}

protocol ChatViewInput: class {
    
    var showsEmptyChatState: Bool { get }
    
    var showsTypingIndicator: Bool { get set }
    
    func displayEmptyChatState()
    
    func display(dataSource: ChatDataSource, for updateType: ChatViewUpdateType, completion: (() -> ())?)
    
    func display(dataSource: ChatDataSource, for updateType: ChatViewUpdateType, newMesagesCount: Int, completion: (() -> ())?)
    
    func showTextInput(allowSwitchingTo commands: CommandButtonsDataSource?)
    
    func showCommands(commands: CommandButtonsDataSource, animated: Bool, delayUnhidingInput: Bool)
    
    func showTextFieldsInput(with textFieldsControl: ChatMessageTextInputControl)
    
    func clearTextFieldsInput()
    
    func hideInput(animated: Bool)
    
    func updateScenarioTitle(with scenarioTitle: String?, animated: Bool)
    
    func showNetworkErrorStatus()
    
    func hideNetworkErrorStatus()
    
    func showLoadingContentStatus()
    
    func hideLoadingContentStatus()
    
    func scrollChatToBottom(animated: Bool)
    
    func scrollChatToMessage(with messageUid: String, animated: Bool)
    
    func startSendMessageProgressTimer(with timeout: TimeInterval)
    
    func completeSendMessageProgressTimer()
}

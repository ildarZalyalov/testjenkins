//
//  ChatViewController.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 27.04.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import UIKit
import Chatto
import ChattoAdditions

class ChatViewController: BaseChatViewController, DataTransferModuleController, ConfigurableModuleController, NavigationBarCustomizingController, ChatViewInput, ChatDataSourceProtocol {
    
    var output: ChatViewOutput!
    
    @IBOutlet weak var showInputViewButtonContainer: UIView!
    @IBOutlet weak var showInputViewButton: KeyboardButtonControl!
    @IBOutlet weak var showInputViewButtonShadowView: UIImageView!
    
    @IBOutlet weak var scrollToBottomButtonContainer: UIView!
    @IBOutlet weak var scrollToBottomButton: KeyboardButtonControl!
    @IBOutlet weak var scrollToBottomButtonShadowView: UIImageView!
    
    @IBOutlet weak var scrollToBottomNewMessagesContainer: UIView!
    @IBOutlet weak var scrollToBottomNewMessagesLabel: UILabel!
    
    @IBOutlet weak var loadingStatusIndicator: UIActivityIndicatorView!
    
    var stringBuilder: ChatMessageStringBuilder!
    var mapPreviewUrlBuilder: YandexMapPreviewUrlBuilder!
    var textFormattingExtractor: ChatTextFormattingExtractor!
    
    var conversationStyle: ConversationStyle!
    
    var avatarUrl: URL?
    var avatarPlaceholderName: String?
    
    var chatItems = [ChatItemProtocol]()
    var hasMoreNext: Bool {
        return output?.hasMoreMessagesNext ?? false
    }
    var hasMorePrevious: Bool {
        return output?.hasMoreMessagesPrevious ?? false
    }
    
    weak var delegate: ChatDataSourceDelegateProtocol?
    weak var itemsDecorator: ChatItemsDecorator?
    
    let chatTopInset: CGFloat = 26
    let scrollToBottomButtonBottomOffset: CGFloat = 10
    
    let chatUpdatesAnimationDuration: TimeInterval = 0.45
    
    let chatTitleView = SubtitledNavigationTitleView.loadNib()
    let chatInputView = ChatInputView()
    
    var originalInputContainerBottomConstraint: NSLayoutConstraint?
    var hiddenInputContainerBottomConstraint: NSLayoutConstraint?
    
    override func loadView() {
        substitutesMainViewAutomatically = false
        super.loadView()
    }
    
    override func viewDidLoad() {
        
        constants.updatesAnimationDuration = chatUpdatesAnimationDuration
        
        super.viewDidLoad()
        
        var insets = chatContentInsets
        insets.top = chatTopInset
        chatContentInsets = insets
        
        chatDataSource = ChatItemsDataSourceProxy(with: self)
        
        let decorator = ChatItemsDecorator()
        chatItemsDecorator = decorator
        itemsDecorator = decorator
        
        collectionView.delaysContentTouches = false
        
        navigationItem.titleView = chatTitleView
        
        chatInputView.textInputPanel.sendButton.addTarget(self, action: #selector(sendButtonPressed(button:)), for: .touchUpInside)
        chatInputView.textInputPanel.commandsButton.addTarget(self, action: #selector(switchToCommandsButtonPressed(button:)), for: .touchUpInside)
        
        chatInputView.textFieldsControl.sendButton.addTarget(self, action: #selector(textInputControlSendButtonPressed(button:)), for: .touchUpInside)
        chatInputView.textFieldsControl.commandsButton.addTarget(self, action: #selector(switchToCommandsButtonPressed(button:)), for: .touchUpInside)
        
        chatInputView.commandKeyboard.delegate = self
        
        chatInputView.activationView.onChatActivation = { [weak self] in
            
            guard let strongSelf = self else { return }
            guard let title = strongSelf.chatInputView.activationView.activateButtonTitle else { return }
            
            strongSelf.output.didPressChatActivationButton(with: title)
        }
        
        configureFloatingButton(button: showInputViewButton, with: showInputViewButtonShadowView, in: showInputViewButtonContainer)
        configureFloatingButton(button: scrollToBottomButton, with: scrollToBottomButtonShadowView, in: scrollToBottomButtonContainer)
        
        let scrollToBottomBottomConstraint = NSLayoutConstraint(item: scrollToBottomButtonContainer, attribute: .bottom, relatedBy: .equal, toItem: inputContainer, attribute: .top, multiplier: 1, constant: -scrollToBottomButtonBottomOffset)
        scrollToBottomBottomConstraint.priority = .defaultLow
        view.addConstraint(scrollToBottomBottomConstraint)
        
        tryFindInputViewBottomConstraints()
        
        output.setupInitialState()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        output.chatWillAppear()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        output.chatWillDisappear()
    }
    
    override var shouldAutorotate: Bool {
        // для дочерних контроллеров, которые могут поменять ориентацию
        return true
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        let isStatusBarDark = conversationStyle?.isStatusBarDark ?? true
        return isStatusBarDark ? .default : .lightContent
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return .portrait
    }
    
    override var bottomLayoutGuide: UILayoutSupport {
        guard #available(iOS 11.0, *) else { return super.bottomLayoutGuide }
        return ChatViewControllerFakeBottomLayoutGuide(safeAreaLayoutGuide: view.safeAreaLayoutGuide)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        performModuleConfigurationIfNeeded(for: segue)
    }
    
    //MARK: - Actions
    
    @IBAction func unwindToChat(_: UIStoryboardSegue) {}
    
    @IBAction func pressedShowInputView() {
        guard isInputViewHidden else { return }
        showInputView(animated: true, userInitiated: true)
    }
    
    @IBAction func pressedScrollToBottom() {
        
        guard !isScrolledAtBottom() else { return }
        
        scrollChatToBottom(animated: true)
        if isInputViewHidden {
            showInputView(animated: true, userInitiated: true)
        }
    }
    
    //MARK: - Методы
    
    func assignStyle(for conversation: Conversation) {
        
        title = conversation.title
        
        conversationStyle = conversation.style
        avatarUrl = conversation.avatarSmallIconUrl
        avatarPlaceholderName = conversation.avatarPlaceholderSmallIconName
        
        chatInputView.textInputPanel.commandsButton.tintColor = conversationStyle.controlsTextColor
        chatInputView.textFieldsControl.commandsButton.tintColor = conversationStyle.controlsTextColor
        
        chatInputView.commandKeyboard.buttonsTitleColor = conversationStyle.controlsTextColor
        chatInputView.commandKeyboard.buttonsShadowColor = conversationStyle.controlsShadowColor
        
        chatInputView.activationView.buttonTitleColor = conversationStyle.controlsTextColor
        chatInputView.activationView.buttonShadowColor = conversationStyle.controlsShadowColor
        
        chatInputView.sendMessageProgressTintColor = conversationStyle.controlsTextColor
        
        setNeedsStatusBarAppearanceUpdate()
    }
    
    func addAvatarBarButtonItem(with avatarSize: CGSize) {
        let button = UIButton(type: .system)
        button.frame = CGRect(x: 0, y: 0, width: avatarSize.width, height: avatarSize.height)
        button.addTarget(self, action: #selector(didPressChatAvatar), for: .touchUpInside)
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: button)
    }
    
    func expandInlineKeyboard(outside: Bool, cellObjectWith cellObjectUid: String) {
        
        guard let objectAndIndex = chatItems.lastElementAndIndex(where: { chatItem in
            guard let cellObject = chatItem as? BaseMessageCellObjectProtocol else { return false }
            return cellObject.messageUid == cellObjectUid
        }) else { return }
        
        guard let cellObject = objectAndIndex.element as? BaseMessageCellObjectProtocol else { return }
        
        let newCellObject = cellObject.copy()
        let updateClosure: (BaseMessageCellObjectProtocol) -> () = outside ? { $0.inlineOutsideKeyboard.isCollapsed = false } : { $0.inlineKeyboard.isCollapsed = false }
        
        if let scrollCellObject = newCellObject as? ChatMessageScrollCellObject {
            scrollCellObject.messagesCellObjects.forEach(updateClosure)
        }
        else {
            updateClosure(newCellObject)
        }
        
        chatItems[objectAndIndex.index] = newCellObject
        output.didUpdateVisualState(for: newCellObject)
        
        delegate?.chatDataSourceDidUpdate(self, updateType: .reload)
    }
    
    func chattoUpdateType(from updateType: ChatViewUpdateType) -> UpdateType {
        switch updateType {
        case .pagination:
            return .pagination
        case .reload:
            return .reload
        default:
            return .normal
        }
    }
    
    func configureFloatingButton(button: KeyboardButtonControl,
                                 with shadowView: UIImageView,
                                 in container: UIView) {
        
        container.superview?.bringSubview(toFront: container)
        shadowView.tintColor = conversationStyle.controlsShadowColor
        
        button.isExclusiveTouch = true
        button.normalTintColor = conversationStyle.controlsTextColor
        button.highlightedTintColor = UIColor.white
        
        weak var weakButton = button
        button.isHighlightedChanged = { _, isHighlighted in
            guard let strongButton = weakButton else { return }
            strongButton.backgroundColor = isHighlighted ? strongButton.normalTintColor : UIColor.white
        }
    }
    
    //MARK: - DataTransferModuleController
    
    func performSegue(with identifier: String, sender: AnyObject?, configurationClosure: ModuleConfigurationClosure?) {
        
        if let closure = configurationClosure {
            addModuleConfiguration(configurationClosure: closure, for: identifier)
        }
        
        performSegue(withIdentifier: identifier, sender: sender)
    }
    
    //MARK: - ConfigurableModuleController
    
    func configureModule(with object: Any) {
        if let configuration = object as? ChatModuleConfiguration {
            assignStyle(for: configuration.conversation)
            output.configure(with: configuration)
        }
    }
    
    //MARK: - NavigationBarCustomizingController
    
    var navigationBarState: NavigationBarState? {
        
        let navigationBarBackgroundView = ChatNavigationBarView.loadNib()
        navigationBarBackgroundView.shadowColor = conversationStyle.headerShadowColor
        navigationBarBackgroundView.backgroundColor = conversationStyle.headerColor
        navigationBarBackgroundView.gradientColor = conversationStyle.gradientColor
        
        if let placeholderName = avatarPlaceholderName {
            navigationBarBackgroundView.avatarImage = UIImage(named: placeholderName)
        }
        navigationBarBackgroundView.avatarImageUrl = avatarUrl
        
        addAvatarBarButtonItem(with: navigationBarBackgroundView.avatarSize)
        
        let titleTextAttributes = [
            NSAttributedStringKey.font : UIFont(customName: .graphikLCGBold, size: 15) as Any,
            NSAttributedStringKey.foregroundColor : conversationStyle.titleColor
        ]
        let subtitleTextAttributes = [
            NSAttributedStringKey.font : UIFont(customName: .graphikLCGMedium, size: 12) as Any,
            NSAttributedStringKey.foregroundColor : conversationStyle.titleColor.withAlphaComponent(0.8)
        ]
        
        chatTitleView.titleTextAttributes = titleTextAttributes
        chatTitleView.subtitleTextAttributes = subtitleTextAttributes
        chatTitleView.title = title
        
        var state = NavigationBarState()
        state.backgroundState = NavigationBarBackgroundState.customLimitedHeight(backgroundView: navigationBarBackgroundView)
        state.tintColor = conversationStyle.backButtonColor
        state.isShadowHidden = true
        state.titleTextAttributes = titleTextAttributes
        state.isHidden = false
        
        return state
    }
    
    //MARK: - Chatto методы
    
    override func createChatInputView() -> UIView {
        return chatInputView
    }
    
    override func createCollectionViewLayout() -> UICollectionViewLayout {
        let layout = ChatLayout()
        layout.delegate = self
        layout.chatLayoutDelegate = self
        return layout
    }
    
    //MARK: - Обработчики нажатий и работа с клавиатурой
    
    @objc
    func didPressChatAvatar() {
        chatInputView.textInputPanel.resignFirstResponder()
        output.didPressChatAvatar()
    }
    
    @objc
    fileprivate func sendButtonPressed(button: UIButton) {
        if let text = chatInputView.textInputPanel.text {
            output.didPressSendButton(with: text)
            chatInputView.textInputPanel.text = nil
        }
    }
    
    @objc
    fileprivate func textInputControlSendButtonPressed(button: UIButton) {
        output.didSubmitTextInput(with: chatInputView.textFieldsControl.values)
    }
    
    @objc
    fileprivate func switchToCommandsButtonPressed(button: UIButton) {
        chatInputView.currentInputMode = .command
    }
    
    //MARK: - Презентеры ячеек с сообщениями
    
    override func createPresenterBuilders() -> [ChatItemType: [ChatItemPresenterBuilderProtocol]] {
        return createPresenterBuildersHierarchy() + createMessageScrollPresenterBuilderHierarchy()
    }
    
    //MARK: - ChatViewInput
    
    var showsEmptyChatState: Bool {
        return chatInputView.currentInputMode == .chatActivation
    }
    
    var showsTypingIndicator: Bool {

        get {
            return itemsDecorator?.shouldShowTypingIndicator ?? false
        }
        set {
            itemsDecorator?.shouldShowTypingIndicator = newValue
            itemsDecorator?.shouldUseTypingIndicatorBottomMargin = isInputViewHidden
        }
    }
    
    func displayEmptyChatState() {
        chatInputView.currentInputMode = .chatActivation
    }
    
    func display(dataSource: ChatDataSource, for updateType: ChatViewUpdateType, completion: (() -> ())?) {
        display(dataSource: dataSource, for: updateType, newMesagesCount: 0, completion: completion)
    }
    
    func display(dataSource: ChatDataSource, for updateType: ChatViewUpdateType, newMesagesCount: Int, completion: (() -> ())?) {
        
        if completion != nil {
            
            let delay = constants.updatesAnimationDuration / 2
            
            // в очередь, чтобы гарантировать completion перед началом выполнения анимации обновления, т.е. одновременно с ней
            updateQueue.addTask { taskCompletion in
                DispatchQueue.main.asyncAfter(deadline: .now() + delay, execute: completion!)
                taskCompletion()
            }
        }
        
        itemsDecorator?.shouldShowTopHistoryLoader = hasMorePrevious
        itemsDecorator?.shouldShowBottomHistoryLoader = hasMoreNext
        
        chatItems = dataSource.chatItems
        delegate?.chatDataSourceDidUpdate(self, updateType: chattoUpdateType(from: updateType))
        
        let willScrollToBottom = updateType != .pagination && isScrolledAtBottom()
        if !willScrollToBottom && isScrollToBottomButtonHidden {
            setScrollToBottomButton(hidden: false)
        }
        
        if !isScrollToBottomButtonHidden, newMesagesCount > 0 {
            incrementScrollToBottomNewMessagesCount(with: newMesagesCount)
        }
    }
    
    func showTextInput(allowSwitchingTo commands: CommandButtonsDataSource?) {
        
        if commands != nil {
            chatInputView.commandKeyboard.buttons = commands!.buttons
        }
        
        if isInputViewHidden {
            setInputViewHidden(hidden: false)
        }
        
        let textInputBecomesFirstResponder = transitionCoordinator == nil
        
        chatInputView.textInputPanel.isCommandsButtonHidden = commands == nil
        chatInputView.setCurrentInputMode(to: .text, becomeFirstResponder: textInputBecomesFirstResponder)
    }
    
    func showCommands(commands: CommandButtonsDataSource, animated: Bool, delayUnhidingInput: Bool) {
        
        let currentMode = chatInputView.currentInputMode
        
        let textModes: [ChatInputView.ChatInputMode] = [.text, .textFields]
        var modesToRefreshFrom: Set<ChatInputView.ChatInputMode> = [.command, .text, .textFields]
        if chatInputView.isSystemKeyboardDisplayed {
            modesToRefreshFrom.subtract(textModes)
        }
 
        let shouldRefresh = !isInputViewHidden && modesToRefreshFrom.contains(currentMode)
        guard !shouldRefresh else {
            refreshCommandKeyboard(with: commands, animated: animated)
            return
        }
        
        let updateCommands: () -> () = { [weak self] in
            
            guard let strongSelf = self else { return }
            
            strongSelf.chatInputView.commandKeyboard.buttons = commands.buttons
            strongSelf.chatInputView.currentInputMode = .command
        }
        
        let updateCommandsAndShowInputView: (Bool) -> () = { [weak self] _ in
            
            guard let strongSelf = self else { return }
            
            updateCommands()
            strongSelf.inputContainer.setNeedsLayout()
            strongSelf.inputContainer.layoutIfNeeded()
            strongSelf.showInputView(animated: true, userInitiated: false, withDelay: delayUnhidingInput)
        }
        
        guard !isInputViewHidden else {
            updateCommandsAndShowInputView(true)
            return
        }
        
        let modesToAnimateFrom: Set<ChatInputView.ChatInputMode> = [.chatActivation]
        let shouldAnimate = animated && modesToAnimateFrom.contains(currentMode)
        
        guard shouldAnimate else {
            updateCommands()
            return
        }
        
        hideInputView(animated: true, userInitiated: false, completion: updateCommandsAndShowInputView)
    }
    
    func showTextFieldsInput(with textFieldsControl: ChatMessageTextInputControl) {
        
        if isInputViewHidden {
            setInputViewHidden(hidden: false)
        }
        
        let currentModeWasTextFields = chatInputView.currentInputMode == .textFields
        let updateTextFields = {
            
            self.chatInputView.textFieldsControl.configure(with: textFieldsControl)
            self.chatInputView.currentInputMode = .textFields
            
            if currentModeWasTextFields {
                self.chatInputView.textFieldsControl.becomeFirstResponder()
            }
        }

        if currentModeWasTextFields {
            UIView.performWithoutAnimation(updateTextFields)
        }
        else {
            updateTextFields()
        }
    }
    
    func clearTextFieldsInput() {
        chatInputView.textFieldsControl.clearTextInFields()
    }
    
    func hideInput(animated: Bool) {
        guard !isInputViewHidden else { return }
        hideInputView(animated: animated, userInitiated: false)
    }

    func updateScenarioTitle(with scenarioTitle: String?, animated: Bool) {
        chatTitleView.shouldAnimateSubtitleAppearance = animated
        chatTitleView.subtitle = scenarioTitle
    }
    
    func showNetworkErrorStatus() {
        chatTitleView.startSubtitleAnimation(with: StringsHelper.networkErrorTexts)
    }
    
    func hideNetworkErrorStatus() {
        chatTitleView.stopSubtitleAnimation()
    }
    
    func showLoadingContentStatus() {
        loadingStatusIndicator.startAnimating()
    }
    
    func hideLoadingContentStatus() {
        loadingStatusIndicator.stopAnimating()
    }
    
    func startSendMessageProgressTimer(with timeout: TimeInterval) {
        chatInputView.startSendMessageProgressTimer(with: timeout)
    }
    
    func scrollChatToBottom(animated: Bool) {
        guard !isScrolledAtBottom() || hasMoreNext else { return }
        output.chatWillScrollToBottom()
        scrollToBottom(animated: animated)
    }
    
    func scrollChatToMessage(with messageUid: String, animated: Bool) {

        // в очередь, чтобы гарантировать скролл только после всех
        // текущих операций обновления чата
        updateQueue.addTask { [weak self] completion in
            
            guard let strongSelf = self else { return }
            
            guard let chatItem = strongSelf.chatItemCompanionCollection.makeIterator().first(where: {
                guard let baseMessageCellObject = $0.chatItem as? BaseMessageCellObjectProtocol else { return false }
                return baseMessageCellObject.messageUid == messageUid
            }) else { return }
            guard let row = strongSelf.chatItemCompanionCollection.indexOf(chatItem.uid) else { return }
            
            let indexPath = IndexPath(row: row, section: 0)
            
            strongSelf.collectionView.scrollToItem(at: indexPath, at: .centeredVertically, animated: animated)
            
            completion()
        }
    }
    
    func completeSendMessageProgressTimer() {
        chatInputView.completeSendMessageProgressTimer()
    }

    //MARK: - ChatDataSourceProtocol
    
    func loadNext() {
        output.loadMessagesRecent()
    }
    
    func loadPrevious() {
        output.loadMessagesHistory()
    }
    
    func adjustNumberOfMessages(preferredMaxCount: Int?, focusPosition: Double, completion:(_ didAdjust: Bool) -> Void) {
        completion(false)
    }
}

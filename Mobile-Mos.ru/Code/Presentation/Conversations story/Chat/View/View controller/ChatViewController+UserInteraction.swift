//
//  ChatViewController+UserInteraction.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 09.01.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

//MARK: - BaseChatMessageHandler
extension ChatViewController: BaseChatMessageHandler {
    
    func userDidTapOnFailIcon(viewModel: BaseMessageViewModelProtocol, failIconView: UIView) {
        output.didTapOnFailIconOfMessage(with: viewModel.messageUid)
    }
    
    func userDidTapOnAvatar(viewModel: BaseMessageViewModelProtocol) {}
    func userDidBeginLongPressOnBubble(viewModel: BaseMessageViewModelProtocol) {}
    func userDidEndLongPressOnBubble(viewModel: BaseMessageViewModelProtocol) {}
    func userDidSelectMessage(viewModel: BaseMessageViewModelProtocol) {}
    func userDidDeselectMessage(viewModel: BaseMessageViewModelProtocol) {}
    
    func userDidTapOnBubble(viewModel: BaseMessageViewModelProtocol) {
        guard !viewModel.areInteractionGesturesDisabled else { return }
        output.didTapOnMessage(with: viewModel.messageUid)
    }
    
    func userDidPressInlineKeyboardButton(viewModel: BaseMessageViewModelProtocol, with buttonId: String)  {
        output.didSelectInlineCommand(with: buttonId, inMessageWith: viewModel.messageUid)
    }
    
    func userDidPressExpandInlineKeyboardButton(viewModel: BaseMessageViewModelProtocol) {
        expandInlineKeyboard(outside: false, cellObjectWith: viewModel.messageUid)
    }
    
    func userDidPressToOpen(url: URL, inMessage viewModel: BaseMessageViewModelProtocol) {
        output.didTapOn(url: url, inMessageWith: viewModel.messageUid)
    }
    
    func userDidPressOptionButton(viewModel: BaseMessageViewModelProtocol, with buttonId: String) {
        guard let type = viewModel.optionButtons.filter({ $0.itemId == buttonId }).first?.type else { return }
        output.didSelectOption(with: type, inMessageWith: viewModel.messageUid)
    }
    
    func userDidPressDocumentIcon(viewModel: ChatDocumentMessageViewModelProtocol) {
        output.didTapOnDocumentIconInMessage(with: viewModel.messageUid)
    }
    
    func userDidPressOnPhoto(viewModel: ChatPhotoMessageViewModelProtocol) {
        output.didTapOnPhotoInMessage(with: viewModel.messageUid)
    }
}

//MARK: - CommandKeyboardViewDelegate
extension ChatViewController: CommandKeyboardViewDelegate {
    
    func didPressCommand(with itemId: String) {
        output.didSelectCommand(with: itemId)
    }
}

//MARK: - InlineOutsideInteractionHandler
extension ChatViewController: InlineOutsideInteractionHandler {
    
    func userDidPressInlineKeyboardButton(viewModel: InlineOutsideViewModel, with buttonId: String) {
        output.didSelectInlineCommand(with: buttonId, inMessageWith: viewModel.messageUid)
    }
    
    func userDidPressExpandInlineKeyboardButton(viewModel: InlineOutsideViewModel) {
        expandInlineKeyboard(outside: true, cellObjectWith: viewModel.messageUid)
    }
}

//MARK: - ChatMessageScrollInteractionHandler
extension ChatViewController: ChatMessageScrollInteractionHandler {
    
    func userWillBeginScrollingBubbles() {
        accessoryViewRevealerIsEnabled = false
    }
    
    func userDidEndScrollingBubbles() {
        accessoryViewRevealerIsEnabled = true
    }
}

//
//  ChatViewController+MessagePresenterBuilders.swift.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 16.08.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import Chatto
import ChattoAdditions

extension ChatViewController {
    
    //MARK: - Иерархия билдеров
    
    func createPresenterBuildersHierarchy() -> [ChatItemType: [ChatItemPresenterBuilderProtocol]] {
        
        return [
            ChatMessageContent.ContentType.text.rawValue: [
                createTextMessagePresenterBuilder()
            ],
            ChatMessageContent.ContentType.photo.rawValue: [
                createPhotoMessagePresenterBuilder()
            ],
            ChatMessageContent.ContentType.weblink.rawValue: [
                createExternalLinkMessagePresenterBuilder()
            ],
            ChatMessageContent.ContentType.document.rawValue: [
                createDocumentMessagePresenterBuilder()
            ],
            ChatMessageContent.ContentType.map.rawValue: [
                createMapMessagePresenterBuilder()
            ],
            InlineOutsideCellObject.chatItemType : [
                createInlineOutsidePresenterBuilder()
            ],
            MessageLoaderCellObject.chatItemType : [
                MessageLoaderPresenterBuilder()
            ],
            TypingIndicatorCellObject.chatItemType : [
                TypingIndicatorPresenterBuilder(textCellStyle: createTextMessageStyle(for: conversationStyle), cellStyle: createBaseMessageStyle(for: conversationStyle))
            ],
            MessageStatusCellObject.chatItemType : [
                MessageStatusPresenterBuilder(stringBuilder: stringBuilder)
            ],
            TimeSeparatorCellObject.chatItemType : [
                TimeSeparatorPresenterBuilder(stringBuilder: stringBuilder)
            ]
        ]
    }
    
    func createMessageScrollPresenterBuilderHierarchy() -> [ChatItemType: [ChatItemPresenterBuilderProtocol]] {
        
        let presenterBuilders = createPresenterBuildersHierarchy().reduce([ChatItemType: ChatItemPresenterBuilderProtocol](), {
            
            guard let presenterBuilder = $1.value.first else { return $0 }
            
            if let baseBuilder = presenterBuilder as? BaseMessagePresenterBuilderProtocol {
                let baseStyle = createBaseMessageStyle(for: conversationStyle)
                baseBuilder.baseMessageStyle = baseStyle
            }
            
            return $0 + [$1.key : presenterBuilder]
        })
        
        let scrollPresenterBuilder = ChatMessageScrollPresenterBuilder(presenterBuilders: presenterBuilders, interactionHandler: self)
        scrollPresenterBuilder.gestureToLowerPriorityAgainstScrolling = navigationController?.interactivePopGestureRecognizer
        
        return [
            ChatMessageContent.ContentType.messageScroll.rawValue: [scrollPresenterBuilder]
        ]
    }
    
    //MARK: - Вспомогательные методы
    
    func createBaseMessageStyle(for style: ConversationStyle) -> BaseMessageCollectionViewCellStyleProtocol {
        let outgoingMessageColor = conversationStyle.outgoingMessageBackgroundColor
        return BaseMessageCellStyleDefault(outgoingColor: outgoingMessageColor)
    }
    
    func createTextMessageStyle(for style: ConversationStyle) -> ChatTextMessageCollectionViewCellStyleProtocol {
        
        let outgoingMessageColor = conversationStyle.outgoingMessageBackgroundColor
        let outgoingMessageTextColor = conversationStyle.outgoingMessageTextColor
        let inlineTextColor = conversationStyle.controlsTextColor
        
        return ChatTextBubbleViewStyleDefault(outgoingColor: outgoingMessageColor, outgoingTextColor: outgoingMessageTextColor, inlineTextColor: inlineTextColor)
    }
    
    //MARK: - Создаем билдеры
    
    func createInlineOutsidePresenterBuilder() -> InlineOutsidePresenterBuilder {
        
        let inlineTextColor = conversationStyle.controlsTextColor
        
        let inlineOutsidePresenterBuilder = InlineOutsidePresenterBuilder(interactionHandler: self)
        inlineOutsidePresenterBuilder.cellStyle = InlineOutsideCellStyleDefault(inlineTextColor: inlineTextColor)
        
        return inlineOutsidePresenterBuilder
    }
    
    func createTextMessagePresenterBuilder() -> ChatTextMessagePresenterBuilder<ChatTextMessageViewModelBuilder, ChatTextMessageHandler> {
        
        let baseStyle = createBaseMessageStyle(for: conversationStyle)
        let textStyle = createTextMessageStyle(for: conversationStyle)
        
        let textMessagePresenterBuilder = ChatTextMessagePresenterBuilder(
            viewModelBuilder: ChatTextMessageViewModelBuilder(style: textStyle, formattingExtractor: textFormattingExtractor),
            interactionHandler: ChatTextMessageHandler(baseHandler: self)
        )
        
        textMessagePresenterBuilder.baseMessageStyle = baseStyle
        textMessagePresenterBuilder.textCellStyle = textStyle
        
        return textMessagePresenterBuilder
    }
    
    func createPhotoMessagePresenterBuilder() -> ChatPhotoMessagePresenterBuilder<ChatPhotoMessageViewModelBuilder, ChatPhotoMessageHandler> {
        
        let outgoingMessageColor = conversationStyle.outgoingMessageBackgroundColor
        let outgoingMessageTextColor = conversationStyle.outgoingMessageTextColor
        let inlineTextColor = conversationStyle.controlsTextColor
        
        let baseStyle = createBaseMessageStyle(for: conversationStyle)
        let photoStyle = ChatPhotoBubbleViewStyleDefault(outgoingColor: outgoingMessageColor, outgoingTextColor: outgoingMessageTextColor, inlineTextColor: inlineTextColor)
        
        let photoMessagePresenterBuilder = ChatPhotoMessagePresenterBuilder(
            viewModelBuilder: ChatPhotoMessageViewModelBuilder(style: photoStyle, formattingExtractor: textFormattingExtractor),
            interactionHandler: ChatPhotoMessageHandler(baseHandler: self)
        )
        
        photoMessagePresenterBuilder.baseMessageStyle = baseStyle
        photoMessagePresenterBuilder.photoCellStyle = photoStyle
        
        return photoMessagePresenterBuilder
    }
    
    func createMapMessagePresenterBuilder() -> ChatMapMessagePresenterBuilder<ChatMapMessageViewModelBuilder, ChatMapMessageHandler> {
        
        let outgoingMessageColor = conversationStyle.outgoingMessageBackgroundColor
        let outgoingMessageTextColor = conversationStyle.outgoingMessageTextColor
        let inlineTextColor = conversationStyle.controlsTextColor
        
        let baseStyle = createBaseMessageStyle(for: conversationStyle)
        let mapStyle = ChatMapBubbleViewStyleDefault(outgoingColor: outgoingMessageColor, outgoingTextColor: outgoingMessageTextColor, inlineTextColor: inlineTextColor)
        
        let mapMessagePresenterBuilder = ChatMapMessagePresenterBuilder(
            mapPreviewUrlBuilder: mapPreviewUrlBuilder,
            viewModelBuilder: ChatMapMessageViewModelBuilder(style: mapStyle, formattingExtractor: textFormattingExtractor),
            interactionHandler: ChatMapMessageHandler(baseHandler: self)
        )
        
        mapMessagePresenterBuilder.baseMessageStyle = baseStyle
        mapMessagePresenterBuilder.mapCellStyle = mapStyle
        
        return mapMessagePresenterBuilder
    }
    
    func createExternalLinkMessagePresenterBuilder() -> ChatExternalLinkMessagePresenterBuilder<ChatExternalLinkMessageViewModelBuilder, ChatExternalLinkMessageHandler> {
        
        let outgoingMessageColor = conversationStyle.outgoingMessageBackgroundColor
        let outgoingMessageTextColor = conversationStyle.outgoingMessageTextColor
        let inlineTextColor = conversationStyle.controlsTextColor
        
        let baseStyle = createBaseMessageStyle(for: conversationStyle)
        let linkStyle = ChatExternalLinkBubbleViewStyleDefault(outgoingColor: outgoingMessageColor, outgoingTextColor: outgoingMessageTextColor, inlineTextColor: inlineTextColor)
        
        let linkMessagePresenterBuilder = ChatExternalLinkMessagePresenterBuilder(
            viewModelBuilder: ChatExternalLinkMessageViewModelBuilder(style: linkStyle, formattingExtractor: textFormattingExtractor),
            interactionHandler: ChatExternalLinkMessageHandler(baseHandler: self)
        )
        
        linkMessagePresenterBuilder.baseMessageStyle = baseStyle
        linkMessagePresenterBuilder.linkCellStyle = linkStyle
        linkMessagePresenterBuilder.onMessageWillBeShown = { [weak self] messageId in
            self?.output.willShowMessageWithExternalLink(with: messageId)
        }
        
        return linkMessagePresenterBuilder
    }
    
    func createDocumentMessagePresenterBuilder() -> ChatDocumentMessagePresenterBuilder<ChatDocumentMessageViewModelBuilder, ChatDocumentMessageHandler> {
        
        let outgoingMessageColor = conversationStyle.outgoingMessageBackgroundColor
        let outgoingMessageTextColor = conversationStyle.outgoingMessageTextColor
        let inlineTextColor = conversationStyle.controlsTextColor
        
        let baseStyle = createBaseMessageStyle(for: conversationStyle)
        let documentStyle = ChatDocumentBubbleViewStyleDefault(outgoingColor: outgoingMessageColor, outgoingTextColor: outgoingMessageTextColor, inlineTextColor: inlineTextColor)
        
        let documentMessagePresenterBuilder = ChatDocumentMessagePresenterBuilder(
            viewModelBuilder: ChatDocumentMessageViewModelBuilder(style: documentStyle, formattingExtractor: textFormattingExtractor),
            interactionHandler: ChatDocumentMessageHandler(baseHandler: self)
        )
        
        documentMessagePresenterBuilder.baseMessageStyle = baseStyle
        documentMessagePresenterBuilder.documentCellStyle = documentStyle
        
        return documentMessagePresenterBuilder
    }
}

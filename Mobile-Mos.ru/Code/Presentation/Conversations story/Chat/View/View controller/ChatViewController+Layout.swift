//
//  ChatViewController+Layout.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 09.01.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation
import Chatto

extension ChatViewController: ChatLayoutDelegate {
    
    /// Длительности анимации обновления командной клавиатуры
    var refreshCommandsAnimationDuration: TimeInterval { return 0.23 }
    
    /// Длительность анимации пропадания/показа командной клавиатуры при скролле
    var hideOrShowCommandsAnimationDuration: TimeInterval { return 0.3 }
    
    /// Длительности анимаций пропадания/показа командной клавиатуры
    var hideCommandsAnimationDuration: TimeInterval { return 0.3 }
    var showCommandsAnimationDuration: TimeInterval { return 0.3 }
    
    var showCommandsAnimationDelay: TimeInterval { return 0.75 }
    
    var chatPanningMinimumVelocityToHideCommands: CGFloat { return 100 }
    var chatPanningMinimumVelocityToShowCommands: CGFloat { return 600 }
    
    var hideOrShowScrollToBottomAnimationDuration: TimeInterval { return 0.3 }
    var hideOrShowScrollToBottomAnimationFastDuration: TimeInterval { return 0.15 }
    
    //MARK: - Обновление инсетов чата
    
    var chatContentInsets: UIEdgeInsets {
        get { return layoutConfiguration.contentInsets }
        set { layoutConfiguration = ChatLayoutConfiguration(contentInsets: newValue, scrollIndicatorInsets: layoutConfiguration.scrollIndicatorInsets) }
    }
    
    //MARK: - Обновление командной клавиатуры
    
    func refreshCommandKeyboard(with commands: CommandButtonsDataSource, animated: Bool) {
        
        chatInputView.commandKeyboard.fixateCurrentKeyboardHeight = true
        
        chatInputView.commandKeyboard.buttons = commands.buttons
        chatInputView.currentInputMode = .command
        
        inputContainer.setNeedsLayout()
        inputContainer.layoutIfNeeded()
        
        chatInputView.commandKeyboard.fixateCurrentKeyboardHeight = false
        chatInputView.commandKeyboard.recalculateHeight()
        
        guard animated else {
            chatInputView.invalidateIntrinsicContentSize()
            return
        }
        
        UIView.animate(withDuration: refreshCommandsAnimationDuration) { [weak self] in

            guard let strongSelf = self else { return }

            strongSelf.chatInputView.invalidateIntrinsicContentSize()
            strongSelf.view.setNeedsLayout()
            strongSelf.view.layoutIfNeeded()
        }
    }
    
    //MARK: - Показ/скрытие командной клавиатуры при прокрутке
    
    var isInputViewHidden: Bool {
        return !inputContainer.isUserInteractionEnabled
    }
    
    func tryFindInputViewBottomConstraints() {
        
        // да-да, полухак 💩, но с ним можно реально сдвинуть любую input view вниз, а не просто спрятать.
        // если изменятся констрейны при обновлении библиотеки, то пострадает только кнопка скролла чата вниз,
        // которая при скрытии командной клавиатуры останется "висеть"
        originalInputContainerBottomConstraint = inputContainer.superview?.constraints.filter { $0.secondItem === inputContainer && $0.secondAttribute == .bottom }.first
        
        guard originalInputContainerBottomConstraint != nil else { return }
        
        hiddenInputContainerBottomConstraint = NSLayoutConstraint(item: view, attribute: .bottom, relatedBy: .equal, toItem: inputContainer, attribute: .bottom, multiplier: 1, constant: 0)
        view.addConstraint(hiddenInputContainerBottomConstraint!)
        hiddenInputContainerBottomConstraint?.isActive = false
        
        bottomSpaceView.removeFromSuperview()
    }
    
    func setInputViewHidden(hidden: Bool, showingInputViewButton: Bool = true) {
        
        guard isInputViewHidden != hidden else { return }
        
        let inputAlpha: CGFloat = hidden ? 0 : 1
        let buttonAlpha: CGFloat = hidden && showingInputViewButton ? 1 : 0
        
        if #available(iOS 11.0, *) {
            
            let bottomInsetAdjustment = hidden ? view.safeAreaInsets.bottom : -view.safeAreaInsets.bottom
            var insets = chatContentInsets
            insets.bottom += bottomInsetAdjustment
            
            chatContentInsets = insets
        }
        
        if hidden {
            hiddenInputContainerBottomConstraint?.constant = -inputContainer.height
        }
        
        // деактивируем сначала оба констрейна т.к. они конфликтные
        let allBottomConstraints: [NSLayoutConstraint] = [originalInputContainerBottomConstraint, hiddenInputContainerBottomConstraint].compactMap { $0 }
        NSLayoutConstraint.deactivate(allBottomConstraints)
        
        originalInputContainerBottomConstraint?.isActive = !hidden
        hiddenInputContainerBottomConstraint?.isActive = hidden
        
        inputContainer.isUserInteractionEnabled = !hidden
        inputContainer.alpha = inputAlpha
        inputContainer.superview?.layoutIfNeeded()
        
        showInputViewButtonContainer.alpha = buttonAlpha
    }
    
    func showInputView(animated: Bool = false, userInitiated: Bool = true, withDelay: Bool = false) {
        
        let animations: () -> () = { [weak self] in
            self?.setInputViewHidden(hidden: false, showingInputViewButton: false)
        }
        
        if animated {
            if userInitiated {
                UIView.animate(withDuration: hideOrShowCommandsAnimationDuration, animations: animations)
            }
            else {
                let delay = withDelay ? showCommandsAnimationDelay : 0
                UIView.animate(withDuration: showCommandsAnimationDuration, delay: delay, options: .curveEaseOut, animations: animations, completion: nil)
            }
        }
        else {
            animations()
        }
    }
    
    func hideInputView(animated: Bool = false, userInitiated: Bool = true, completion: ((Bool) -> ())? = nil) {
        
        let animations: () -> () = { [weak self] in
            self?.setInputViewHidden(hidden: true, showingInputViewButton: userInitiated)
        }
        
        if animated {
            let duration = userInitiated ? hideOrShowCommandsAnimationDuration : hideCommandsAnimationDuration
            UIView.animate(withDuration: duration, animations: animations, completion: completion)
        }
        else {
            animations()
            completion?(true)
        }
    }
    
    func hideOrShowInputView(onlyOnPanning: Bool) {
        
        guard chatInputView.currentInputMode == .command else { return }
        guard collectionView.contentSize.height > collectionView.height else { return }
        
        var shouldShowInputView = false
        
        if onlyOnPanning {
            
            let velocity = collectionView.panGestureRecognizer.velocity(in: collectionView).y
            let isScrolledToBottomDirection = velocity < 0
            let maximumVelocity = isScrolledToBottomDirection ? chatPanningMinimumVelocityToShowCommands : chatPanningMinimumVelocityToHideCommands
            
            guard isScrolledToBottomDirection != inputContainer.isUserInteractionEnabled else { return }
            guard abs(velocity) >= maximumVelocity else { return }
            
            shouldShowInputView = isScrolledToBottomDirection
        }
        else {
            shouldShowInputView = isInputViewHidden
        }
        
        if shouldShowInputView {
            showInputView(animated: true, userInitiated: true)
        }
        else {
            hideInputView(animated: true, userInitiated: true)
        }
    }
    
    //MARK: - Показ/скрытие кнопки прокрутки чата до конца при прокрутке
    
    var isScrollToBottomButtonHidden: Bool {
        return scrollToBottomButtonContainer.alpha == 0
    }
    
    var isScrollToBottomMessagesCountHidden: Bool {
        return scrollToBottomNewMessagesContainer.isHidden
    }
    
    var canShowScrollToBottomButton: Bool {
        return collectionView.contentSize.height > collectionView.height
    }
    
    func incrementScrollToBottomNewMessagesCount(with count: Int) {
        
        scrollToBottomNewMessagesContainer.isHidden = false
        
        var totalCount = 0
        if let currentText = scrollToBottomNewMessagesLabel.text, let currentCount = Int(currentText) {
            totalCount = currentCount
        }
        
        totalCount += count
        scrollToBottomNewMessagesLabel.text = String(totalCount)
    }
    
    func setScrollToBottomButton(hidden: Bool, fast: Bool = false) {
        
        let buttonAlpha: CGFloat = hidden ? 0 : 1
        
        guard canShowScrollToBottomButton else { return }
        guard scrollToBottomButtonContainer.alpha != buttonAlpha else { return }
        
        let duration = fast ? hideOrShowScrollToBottomAnimationFastDuration : hideOrShowScrollToBottomAnimationDuration
        
        UIView.animate(withDuration: duration, animations: {
            self.scrollToBottomButtonContainer.alpha = buttonAlpha
        }) { _ in
            if hidden {
                self.scrollToBottomNewMessagesLabel.text = nil
                self.scrollToBottomNewMessagesContainer.isHidden = true
            }
        }
    }
    
    func hideOrShowScrollToBottomButtonIfNeeded() {
        
        guard canShowScrollToBottomButton else { return }
        
        let shouldHideOnlyAtBottom = !isScrollToBottomButtonHidden && !isScrollToBottomMessagesCountHidden
        var closeToBottom = isCloseToBottom() && !hasMoreNext
        var fast = false
        
        if closeToBottom && shouldHideOnlyAtBottom {
            closeToBottom = isScrolledAtBottom()
            fast = true
        }
        
        setScrollToBottomButton(hidden: closeToBottom, fast: fast)
    }
    
    //MARK: - UIScrollViewDelegate
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        super.scrollViewDidScroll(scrollView)
        hideOrShowScrollToBottomButtonIfNeeded()
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        UIMenuController.shared.setMenuVisible(false, animated: false)
        hideOrShowInputView(onlyOnPanning: true)
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        hideOrShowScrollToBottomButtonIfNeeded()
        
        let forceShowInputView = !inputContainer.isUserInteractionEnabled && isScrolledAtBottom()
        guard decelerate || forceShowInputView else { return }
        
        hideOrShowInputView(onlyOnPanning: !forceShowInputView)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        hideOrShowScrollToBottomButtonIfNeeded()
    }
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        hideOrShowScrollToBottomButtonIfNeeded()
    }
    
    //MARK: - ChatLayoutDelegate
    
    func isMessageItem(at indexPath: IndexPath) -> Bool {
        
        guard let item = chatItemCompanionCollection[safe: indexPath.row] else { return false }
        
        // "карусели" с более чем 1 сообщением показываем по-старому, им не идет выезжать слева
        if let scrollCellObject = item.chatItem as? ChatMessageScrollCellObject {
            return scrollCellObject.messagesCellObjects.count == 1
        }
        
        // если есть внешние инлайны, то сообщение анимируется вместе с ними по-старому
        if let messageCellObject = item.chatItem as? BaseMessageCellObjectProtocol {
            return messageCellObject.inlineOutsideKeyboard.inlineButtons.isEmpty
        }
        
        return item.chatItem is TypingIndicatorCellObject
    }
    
    func isIncomingMessageItem(at indexPath: IndexPath) -> Bool {
        guard let item = chatItemCompanionCollection[safe: indexPath.row] else { return false }
        guard !(item.chatItem is TypingIndicatorCellObject) else { return true }
        guard let messageItem = item.chatItem as? BaseMessageCellObjectProtocol else { return false }
        return messageItem.isIncoming
    }
}

//MARK: -

/// Делегат ChatLayout
protocol ChatLayoutDelegate: class {
    
    /// Является ли ячейка ячейкой сообщения
    ///
    /// - Parameter indexPath: IndexPath ячейки
    func isMessageItem(at indexPath: IndexPath) -> Bool
    
    /// Является ли ячейка ячейкой входящего сообщения
    ///
    /// - Parameter indexPath: IndexPath ячейки
    func isIncomingMessageItem(at indexPath: IndexPath) -> Bool
}

/// Layout чата
/// Расширяет layout по умолчанию фичами, например, анимацией сообщений при показе/удалении
class ChatLayout: ChatCollectionViewLayout {
    
    weak var chatLayoutDelegate: ChatLayoutDelegate?
    
    override func prepare(forCollectionViewUpdates updateItems: [UICollectionViewUpdateItem]) {
        super.prepare(forCollectionViewUpdates: updateItems)
    }
    
    override func initialLayoutAttributesForAppearingItem(at itemIndexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        
        let attributes = super.initialLayoutAttributesForAppearingItem(at: itemIndexPath)
        guard attributes != nil else { return attributes }
        
        let isMessage = chatLayoutDelegate?.isMessageItem(at: itemIndexPath) ?? false
        guard isMessage else { return attributes }
        
        let isIncoming = chatLayoutDelegate?.isIncomingMessageItem(at: itemIndexPath) ?? false
        
        var center = attributes!.center
        let halfOfItemWidth = attributes!.size.width / 2
        let centerOffset = isIncoming ? -halfOfItemWidth : halfOfItemWidth
        
        center.x += centerOffset
        attributes!.center = center
        
        return attributes
    }
    
    override func finalLayoutAttributesForDisappearingItem(at itemIndexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        
        let attributes = super.finalLayoutAttributesForDisappearingItem(at: itemIndexPath)
        attributes?.alpha = 0
        
        return attributes
    }
}

//MARK: -

/// Фейковый Bottom Layout Guide для контроллера чата
/// Нужен, чтобы subviews (в том числе input views) НЕ делали отступ от края экрана
/// Возвращается в переопределенном методе bottomLayoutGuide в классе контроллера чата
class ChatViewControllerFakeBottomLayoutGuide: NSObject, UILayoutSupport {
    
    let topAnchor: NSLayoutYAxisAnchor
    let bottomAnchor: NSLayoutYAxisAnchor
    let heightAnchor: NSLayoutDimension
    let length: CGFloat = 0
    
    init(safeAreaLayoutGuide: UILayoutGuide) {
        self.topAnchor = safeAreaLayoutGuide.topAnchor
        self.bottomAnchor = safeAreaLayoutGuide.bottomAnchor
        self.heightAnchor = safeAreaLayoutGuide.heightAnchor
    }
}

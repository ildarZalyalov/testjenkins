//
//  ChatMapMessagePresenterBuilder.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 15.08.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import Chatto
import ChattoAdditions

class ChatMapMessagePresenterBuilder<ViewModelBuilderT, InteractionHandlerT>:
    ChatTextMessagePresenterBuilder<ViewModelBuilderT, InteractionHandlerT> where
    ViewModelBuilderT: ViewModelBuilderProtocol,
    ViewModelBuilderT.ViewModelT: ChatMapMessageViewModelProtocol,
    InteractionHandlerT: BaseInteractionHandlerProtocol,
    InteractionHandlerT.ViewModelT == ViewModelBuilderT.ViewModelT {
    
    public init(
        mapPreviewUrlBuilder: YandexMapPreviewUrlBuilder,
        viewModelBuilder: ViewModelBuilderT,
        interactionHandler: InteractionHandlerT? = nil) {
        self.mapPreviewUrlBuilder = mapPreviewUrlBuilder
        super.init(viewModelBuilder: viewModelBuilder,
                   interactionHandler: interactionHandler)
    }
    
    let mapPreviewUrlBuilder: YandexMapPreviewUrlBuilder
    
    lazy var sizingMapCell: ChatMapMessageCollectionViewCell = {
        
        var cell: ChatMapMessageCollectionViewCell? = nil
        
        if Thread.isMainThread {
            cell = ChatMapMessageCollectionViewCell.createSizingCell()
        } else {
            DispatchQueue.main.sync(execute: {
                cell = ChatMapMessageCollectionViewCell.createSizingCell()
            })
        }
        
        return cell!
    }()
    
    lazy var mapCellStyle: ChatMapMessageCollectionViewCellStyleProtocol = ChatMapBubbleViewStyleDefault()
    
    override func createPresenterWithChatItem(_ chatItem: ChatItemProtocol) -> ChatItemPresenterProtocol {
        
        assert(canHandleChatItem(chatItem))
        
        return ChatMapMessagePresenter<ViewModelBuilderT, InteractionHandlerT>(
            mapPreviewUrlBuilder: mapPreviewUrlBuilder,
            messageModel: chatItem as! ModelT,
            viewModelBuilder: viewModelBuilder,
            interactionHandler: interactionHandler,
            sizingCell: sizingMapCell,
            baseCellStyle: baseMessageStyle,
            mapCellStyle: mapCellStyle,
            layoutCache: layoutCache
        )
    }
    
    override var presenterType: ChatItemPresenterProtocol.Type {
        return ChatMapMessagePresenter<ViewModelBuilderT, InteractionHandlerT>.self
    }
}

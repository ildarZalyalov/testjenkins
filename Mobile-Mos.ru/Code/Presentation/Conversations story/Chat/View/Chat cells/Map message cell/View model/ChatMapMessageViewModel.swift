//
//  ChatMapMessageViewModel.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 15.08.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import ChattoAdditions

/// Протокол view model для ячейки сообщения с картой
protocol ChatMapMessageViewModelProtocol: ChatTextMessageViewModelProtocol  {
    
    /// Координаты на превью карты
    var mapCoordinates: Location { get }
    
    /// Координаты на превью карты в текстовом представлении (для копирования в буфер обмена)
    var mapCoordinateString: String { get }
    
    /// Название объекта
    var title: String? { get }
    
    /// Описание объекта
    var description: String? { get }
}

/// View model сообщения с картой
class ChatMapMessageViewModel: ChatTextMessageViewModel, ChatMapMessageViewModelProtocol {
    
    /// Координаты на превью карты
    var mapCoordinates: Location
    
    /// Координаты на превью карты в текстовом представлении (для копирования в буфер обмена)
    let mapCoordinateString: String
    
    /// Название объекта
    var title: String?
    
    /// Описание объекта
    var description: String?
    
    init(textModel: ChatTextMessageViewModel, coordinates: Location, coordinateString: String) {
        mapCoordinates = coordinates
        mapCoordinateString = coordinateString
        super.init(messageUid: textModel.messageUid,
                   textMessage: textModel.textMessage,
                   messageViewModel: textModel.messageViewModel,
                   inlineButtons: textModel.inlineButtons,
                   optionButtons: textModel.optionButtons,
                   isShowingExpandButton: textModel.isShowingExpandButton,
                   formattedText: textModel.formattedText)
    }
}

/// Билдер view model сообщения с картой
class ChatMapMessageViewModelBuilder: ViewModelBuilderProtocol {
    
    let textMessageViewModelBuilder: ChatTextMessageViewModelBuilder
    
    init(style: ChatTextMessageCollectionViewCellStyleProtocol, formattingExtractor: ChatTextFormattingExtractor) {
        textMessageViewModelBuilder = ChatTextMessageViewModelBuilder(style: style, formattingExtractor: formattingExtractor)
    }
    
    func createViewModel(_ mapMessage: ChatMapMessageCellObject) -> ChatMapMessageViewModel {
        
        let textViewModel = textMessageViewModelBuilder.createViewModel(mapMessage)
        let coordinateString = "\(mapMessage.location.latitude), \(mapMessage.location.longitude)"
        
        let viewModel = ChatMapMessageViewModel(textModel: textViewModel,
                                                coordinates: mapMessage.location,
                                                coordinateString: coordinateString)
        
        viewModel.title = mapMessage.title
        viewModel.description = mapMessage.description
        viewModel.customBubbleColor = UIColor.white
        
        return viewModel
    }
    
    func canCreateViewModel(fromModel model: Any) -> Bool {
        return model is ChatMapMessageCellObject
    }
}

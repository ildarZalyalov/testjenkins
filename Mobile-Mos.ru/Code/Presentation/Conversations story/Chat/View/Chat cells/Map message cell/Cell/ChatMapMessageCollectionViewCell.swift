//
//  ChatMapMessageCollectionViewCell.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 15.08.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Протокол описывает возможные действия, которые могут быть вызваны из меню (UIMenuController) по долгому тапу на ячейку
@objc protocol ChatMapMessageCellEditActions : NSObjectProtocol {
    
    /// Скопировать строку с координатами
    @objc optional func copyCoordinates(_ sender: Any?)
}

/// Ячейка для сообщения с картой
class ChatMapMessageCollectionViewCell: ChatTextMessageCollectionViewCell, ChatMapMessageCellEditActions {
    
    override class func createSizingCell() -> ChatMapMessageCollectionViewCell {
        let cell = ChatMapMessageCollectionViewCell(frame: CGRect.zero)
        cell.viewContext = .sizing
        return cell
    }
    
    //MARK: - Property forwarding
    
    var mapPreviewUrlBuilder: YandexMapPreviewUrlBuilder? {
        didSet {
            guard let mapBubbleView = bubbleView as? ChatMapBubbleView else { return }
            mapBubbleView.mapPreviewUrlBuilder = mapPreviewUrlBuilder
        }
    }
    
    var mapMessageViewModel: ChatMapMessageViewModelProtocol? {
        didSet {
            textMessageViewModel = mapMessageViewModel
        }
    }
    
    var mapMessageStyle: ChatMapMessageCollectionViewCellStyleProtocol? {
        didSet {
            textMessageStyle = mapMessageStyle
        }
    }
    
    //MARK: Создаем баббл view
    
    override func createBubbleView() -> ChatMapBubbleView {
        let view = ChatMapBubbleView()
        view.delegate = self
        view.textBubbleDelegate = self
        return view
    }
    
    //MARK: - ChatMapMessageCellEditActions
    
    func copyCoordinates(_ sender: Any?) {
        UIPasteboard.general.string = mapMessageViewModel?.mapCoordinateString
    }
}

//
//  ChatMapBubbleView.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 14.08.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// View для баббла для ChatMessage, у которого тип контента - карта.
class ChatMapBubbleView: ChatTextBubbleView {
    
    /// Текущая ссылка на превью карты
    fileprivate var mapPreviewUrl: URL?
    
    /// Билдер ссылок на превью карты
    var mapPreviewUrlBuilder: YandexMapPreviewUrlBuilder?
    
    /// Область карты, показываемая вокруг координаты на превью карты
    let mapPreviewLocationSpan: YandexMapPreviewUrlBuilder.CoordinateSpan = (0.002, 0.002)
    
    /// Вью с картинкой превью карты
    private var mapPreviewView: UIImageView = {
        
        let mapPreviewView = UIImageView()
        mapPreviewView.contentMode = .scaleAspectFill
        mapPreviewView.clipsToBounds = true
        
        return mapPreviewView
    }()
    
    /// Вью с пином на превью карты
    private var pinView: UIImageView = UIImageView(image: #imageLiteral(resourceName: "messageLocationIcon"))
    
    /// Лейбл с названием местоположения
    private var locationNameLabel: UILabel = {
        
        let locationNameLabel = UILabel()
        locationNameLabel.numberOfLines = 0
        
        return locationNameLabel
    }()
    
    /// Лейбл с описанием местоположения
    private var locationDescriptionLabel: UILabel = {
        
        let locationDescriptionLabel = UILabel()
        locationDescriptionLabel.numberOfLines = 0
        
        return locationDescriptionLabel
    }()
    
    private func locationNameAttributedString(withTailTruncation: Bool) -> NSAttributedString? {
        
        guard let style = style as? ChatMapBubbleViewStyleProtocol else { return nil }
        guard let viewModel = messageViewModel as? ChatMapMessageViewModelProtocol else { return nil }
        
        var locationNameAttributes: [NSAttributedStringKey : Any] = [
            NSAttributedStringKey.font : style.locationNameFont(viewModel: viewModel, isSelected: isSelected),
            NSAttributedStringKey.foregroundColor : style.locationNameTextColor(viewModel: viewModel, isSelected: isSelected),
            ]
        
        if var paragraphStyle = style.locationNameParagraphStyle(viewModel: viewModel, isSelected: isSelected) {
            
            if withTailTruncation, let mutableStyle = paragraphStyle.mutableCopy() as? NSMutableParagraphStyle {
                mutableStyle.lineBreakMode = .byTruncatingTail
                paragraphStyle = mutableStyle
            }
            
            locationNameAttributes[NSAttributedStringKey.paragraphStyle] = paragraphStyle
        }
        
        return NSAttributedString(string: viewModel.title ?? String(), attributes: locationNameAttributes)
    }
    
    private func locationDescriptionAttributedString(withTailTruncation: Bool) -> NSAttributedString? {
        
        guard let style = style as? ChatMapBubbleViewStyleProtocol else { return nil }
        guard let viewModel = messageViewModel as? ChatMapMessageViewModelProtocol else { return nil }
        
        var locationDescriptionAttributes: [NSAttributedStringKey : Any] = [
            NSAttributedStringKey.font : style.locationDescriptionFont(viewModel: viewModel, isSelected: isSelected),
            NSAttributedStringKey.foregroundColor : style.locationDescriptionTextColor(viewModel: viewModel, isSelected: isSelected),
            ]
        
        if var paragraphStyle = style.locationDescriptionParagraphStyle(viewModel: viewModel, isSelected: isSelected) {
            
            if withTailTruncation, let mutableStyle = paragraphStyle.mutableCopy() as? NSMutableParagraphStyle {
                mutableStyle.lineBreakMode = .byTruncatingTail
                paragraphStyle = mutableStyle
            }
            
            locationDescriptionAttributes[NSAttributedStringKey.paragraphStyle] = paragraphStyle
        }
        
        return NSAttributedString(string: viewModel.description ?? String(), attributes: locationDescriptionAttributes)
    }
    
    override func initializeContentSubviews(contentParentView: UIView) {
        super.initializeContentSubviews(contentParentView: contentParentView)
        contentParentView.addSubview(mapPreviewView)
        contentParentView.addSubview(pinView)
        contentParentView.addSubview(locationNameLabel)
        contentParentView.addSubview(locationDescriptionLabel)
    }
    
    override func updateContentViews() {
        
        super.updateContentViews()
        
        guard let viewModel = messageViewModel as? ChatMapMessageViewModelProtocol else { return }
        
        locationNameLabel.isHidden = viewModel.title?.isEmpty ?? true
        locationNameLabel.attributedText = locationNameAttributedString(withTailTruncation: true)
        
        locationDescriptionLabel.isHidden = viewModel.description?.isEmpty ?? true
        locationDescriptionLabel.attributedText = locationDescriptionAttributedString(withTailTruncation: true)
    }
    
    override func createLayoutModelContext(preferredMaxLayoutWidth: CGFloat) -> BaseBubbleLayoutModel.LayoutContext? {
        
        guard let locationName = locationNameAttributedString(withTailTruncation: false) else { return nil }
        guard let locationDescription = locationDescriptionAttributedString(withTailTruncation: false) else { return nil }
        
        guard let style = style as? ChatMapBubbleViewStyleProtocol else { return nil }
        guard let viewModel = messageViewModel as? ChatMapMessageViewModelProtocol else { return nil }
        
        let model = super.createLayoutModelContext(preferredMaxLayoutWidth: preferredMaxLayoutWidth)
        
        guard let baseModel = model as? ChatTextBubbleLayoutModel.ChatTextLayoutContext else { return nil }
        
        let mapParameters = ChatMapBubbleLayoutModel.ChatMapLayoutContext.ChatMapLayoutParameters(
            mapPreviewSize: style.mapPreviewSize(viewModel: viewModel, isSelected: isSelected),
            locationName: locationName,
            locationDescription: locationDescription
        )
        
        return ChatMapBubbleLayoutModel.ChatMapLayoutContext(
            parameters: baseModel.layoutParameters,
            textParameters: baseModel.chatTextLayoutParameters,
            mapParameters: mapParameters
        )
    }
    
    override func layoutSubviews() {
        
        super.layoutSubviews()
        
        guard let mapLayout = calculateBubbleLayout(preferredMaxLayoutWidth: preferredMaxLayoutWidth) as? ChatMapBubbleLayoutModel else { return }
        
        mapPreviewView.bma_rect = mapLayout.mapFrame
        locationNameLabel.bma_rect = mapLayout.locationNameFrame
        locationDescriptionLabel.bma_rect = mapLayout.locationDescriptionFrame
        
        pinView.center = CGPoint(x: mapLayout.mapFrame.midX, y: mapLayout.mapFrame.midY - pinView.height / 2)
        
        guard let viewModel = messageViewModel as? ChatMapMessageViewModelProtocol else { return }
        
        let previewUrl = mapPreviewUrlBuilder?.urlForMapPreview(with: viewModel.mapCoordinates, span: mapPreviewLocationSpan, size: mapLayout.mapFrame.size)
        
        guard previewUrl != mapPreviewUrl else { return }
        
        mapPreviewUrl = previewUrl
        mapPreviewView.loadAndDisplayImage(from: previewUrl)
    }
}

//
//  ChatMapBubbleLayoutModel.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 14.08.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Модель лейаута для ChatMapBubbleView. Умеет расчитывать размер ChatMapBubbleView при заданных параметрах, а также кеширует его
class ChatMapBubbleLayoutModel: ChatTextBubbleLayoutModel {
    
    /// Содержит входные данные для расчета, а также служит ключем хэширования
    class ChatMapLayoutContext: ChatTextBubbleLayoutModel.ChatTextLayoutContext {
        
        struct ChatMapLayoutParameters {
            
            /// Размер превью с картой
            let mapPreviewSize: CGSize
            
            /// Название местоположения
            let locationName: NSAttributedString
            
            /// Описание местоположения
            let locationDescription: NSAttributedString
        }
        
        let chatMapLayoutParameters: ChatMapLayoutParameters
        
        override var bubbleLayoutType: BaseBubbleLayoutModel.Type {
            return ChatMapBubbleLayoutModel.self
        }
        
        init(parameters: LayoutParameters,
             textParameters: ChatTextLayoutParameters,
             mapParameters: ChatMapLayoutParameters) {
            self.chatMapLayoutParameters = mapParameters
            super.init(parameters: parameters, textParameters: textParameters)
        }
        
        override var hashValue: Int {
            
            let params = chatMapLayoutParameters
            let hashes = [super.hashValue,
                          params.mapPreviewSize.combinedHashValue,
                          params.locationName.hashValue,
                          params.locationDescription.hashValue]
            
            return hashes.combinedHashValue
        }
        
        static func == (lhs: ChatMapLayoutContext, rhs: ChatMapLayoutContext) -> Bool {
            
            if lhs === rhs { return true }
            
            typealias BaseContext = ChatTextBubbleLayoutModel.ChatTextLayoutContext
            let baseEqual = lhs as BaseContext == rhs as BaseContext
            
            let lhsParams = lhs.chatMapLayoutParameters
            let rhsParams = rhs.chatMapLayoutParameters
            
            let lhsValues = (lhsParams.mapPreviewSize, lhsParams.locationName, lhsParams.locationDescription)
            let rhsValues = (rhsParams.mapPreviewSize, rhsParams.locationName, rhsParams.locationDescription)
            
            return lhsValues == rhsValues && baseEqual
        }
    }
    
    let maxLocationNameHeight: CGFloat = 32
    
    let maxLocationDescriptionHeight: CGFloat = 54
    
    let labelsTopSpace: CGFloat = 12
    
    let betweenLabelsSpace: CGFloat = 7
    
    let labelsBottomSpace: CGFloat = 8
    
    let labelsLeftRightInset: CGFloat = 12
    
    
    var mapSize: CGSize = CGSize.zero
    
    /// Фрейм карты
    var mapFrame: CGRect = CGRect.zero
    
    var locationNameSize: CGSize = CGSize.zero
    
    var locationNameFrame: CGRect = CGRect.zero
    
    var locationDescriptionSize: CGSize = CGSize.zero
    
    var locationDescriptionFrame: CGRect = CGRect.zero
    
    /// Размер превью карты
    var mapPreviewSize: CGSize = CGSize.zero
    
    /// Посчитать размер контента баббла
    override func calculateIntrinsicContentSize() -> CGSize {
        
        var contentSize = super.calculateIntrinsicContentSize()
        
        guard let mapLayoutContext = layoutContext as? ChatMapLayoutContext else { return contentSize }
        let layoutParameters = mapLayoutContext.chatMapLayoutParameters
        
        mapSize = layoutParameters.mapPreviewSize
        mapPreviewSize = layoutParameters.mapPreviewSize
        
        let contentWidth = max(contentSize.width, mapPreviewSize.width)
        let atLeastOneLabelHasText = !layoutParameters.locationName.string.isEmpty || !layoutParameters.locationDescription.string.isEmpty
        let bothLabelsHaveText = !layoutParameters.locationName.string.isEmpty && !layoutParameters.locationDescription.string.isEmpty
        
        if atLeastOneLabelHasText {
            
            mapPreviewSize.height += labelsTopSpace + labelsBottomSpace
            if bothLabelsHaveText {
                mapPreviewSize.height += betweenLabelsSpace
            }
            
            let maxWidth = contentWidth - 2 * labelsLeftRightInset
            let sizeConstraint = CGSize(width: maxWidth, height: CGFloat.greatestFiniteMagnitude)
            
            if !layoutParameters.locationName.string.isEmpty {
                
                locationNameSize = layoutParameters.locationName.boundingRect(with: sizeConstraint,
                                                                              options: .usesLineFragmentOrigin,
                                                                              context: nil).size
                if locationNameSize.height > maxLocationNameHeight {
                    locationNameSize.height = maxLocationNameHeight
                }
                
                mapPreviewSize.height += locationNameSize.height
            }
            
            if !layoutParameters.locationDescription.string.isEmpty {
                
                locationDescriptionSize = layoutParameters.locationDescription.boundingRect(with: sizeConstraint,
                                                                                            options: .usesLineFragmentOrigin,
                                                                                            context: nil).size
                if locationDescriptionSize.height > maxLocationDescriptionHeight {
                    locationDescriptionSize.height = maxLocationDescriptionHeight
                }
                
                mapPreviewSize.height += locationDescriptionSize.height
            }
        }
        
        contentSize.width = contentWidth
        contentSize.height += mapPreviewSize.height
        
        return contentSize
    }
    
    /// Пересчитать кеш лейаута контента баббла
    override func calculateContentLayout(for contentSize: CGSize) {
        
        super.calculateContentLayout(for: contentSize)
        
        mapFrame = CGRect(x: 0,
                          y: textSize.height,
                          width: contentSize.width,
                          height: mapSize.height)
        
        locationNameFrame = CGRect(x: labelsLeftRightInset,
                                   y: mapFrame.origin.y + mapFrame.height + labelsTopSpace,
                                   width: contentSize.width - 2 * labelsLeftRightInset,
                                   height: locationNameSize.height)
        
        locationDescriptionFrame = CGRect(x: labelsLeftRightInset,
                                   y: locationNameFrame.origin.y + locationNameFrame.height + betweenLabelsSpace,
                                   width: contentSize.width - 2 * labelsLeftRightInset,
                                   height: locationDescriptionSize.height)
    }
}

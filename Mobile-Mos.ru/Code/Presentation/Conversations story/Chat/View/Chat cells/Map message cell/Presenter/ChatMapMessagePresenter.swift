//
//  ChatMapMessagePresenter.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 15.08.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import ChattoAdditions

class ChatMapMessagePresenter<ViewModelBuilderT, InteractionHandlerT>:
    ChatTextMessagePresenter<ViewModelBuilderT, InteractionHandlerT> where
    ViewModelBuilderT: ViewModelBuilderProtocol,
    ViewModelBuilderT.ViewModelT: ChatMapMessageViewModelProtocol,
    InteractionHandlerT: BaseInteractionHandlerProtocol,
    InteractionHandlerT.ViewModelT == ViewModelBuilderT.ViewModelT {
    
    public init(
        mapPreviewUrlBuilder: YandexMapPreviewUrlBuilder,
        messageModel: ModelT,
        viewModelBuilder: ViewModelBuilderT,
        interactionHandler: InteractionHandlerT?,
        sizingCell: ChatMapMessageCollectionViewCell,
        baseCellStyle: BaseMessageCollectionViewCellStyleProtocol,
        mapCellStyle: ChatMapMessageCollectionViewCellStyleProtocol,
        layoutCache: NSCache<AnyObject, AnyObject>) {
        self.mapPreviewUrlBuilder = mapPreviewUrlBuilder
        self.mapCellStyle = mapCellStyle
        super.init(
            messageModel: messageModel,
            viewModelBuilder: viewModelBuilder,
            interactionHandler: interactionHandler,
            sizingCell: sizingCell,
            baseCellStyle: baseCellStyle,
            textCellStyle: mapCellStyle,
            layoutCache: layoutCache)
    }
    
    let mapPreviewUrlBuilder: YandexMapPreviewUrlBuilder
    let mapCellStyle: ChatMapMessageCollectionViewCellStyleProtocol
    
    let copySelector: Selector = #selector(UIResponderStandardEditActions.copy(_:))
    let copyCoordinatesSelector: Selector = #selector(ChatMapMessageCellEditActions.copyCoordinates(_:))
    
    let copyCoordinatesTitle = "Скопировать координаты"
    
    override class func registerCells(_ collectionView: UICollectionView) {
        collectionView.register(ChatMapMessageCollectionViewCell.self, forCellWithReuseIdentifier: "chat-map-message-incoming")
        collectionView.register(ChatMapMessageCollectionViewCell.self, forCellWithReuseIdentifier: "chat-map-message-outcoming")
    }
    
    override func dequeueCell(collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell {
        let identifier = messageViewModel.isIncoming ? "chat-map-message-incoming" : "chat-map-message-outcoming"
        return collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath)
    }
    
    public var mapCell: ChatMapMessageCollectionViewCell? {
        
        if let cell = self.cell {
            if let mapCell = cell as? ChatMapMessageCollectionViewCell {
                return mapCell
            } else {
                assert(false, "Invalid cell was given to presenter!")
            }
        }
        
        return nil
    }
    
    override func configureCell(_ cell: BaseMessageCollectionViewCell<ChatTextBubbleView>, decorationAttributes: ChatItemDecorationAttributes, animated: Bool, additionalConfiguration: (() -> Void)?) {
        
        guard let cell = cell as? ChatMapMessageCollectionViewCell else {
            assert(false, "Invalid cell received")
            return
        }
        
        super.configureCell(cell, decorationAttributes: decorationAttributes, animated: animated) { () -> Void in
            
            cell.mapPreviewUrlBuilder = self.mapPreviewUrlBuilder
            cell.mapMessageViewModel = self.messageViewModel
            cell.mapMessageStyle = self.mapCellStyle
            
            additionalConfiguration?()
        }
    }
    
    override func updateCurrentCell() {
        if let cell = mapCell, let decorationAttributes = decorationAttributes {
            configureCell(cell, decorationAttributes: decorationAttributes, animated: itemVisibility != .appearing, additionalConfiguration: nil)
        }
    }
    
    override func canShowMenu() -> Bool {
        
        let menuItem = UIMenuItem(title: copyCoordinatesTitle, action: copyCoordinatesSelector)
        UIMenuController.shared.menuItems = [menuItem]
        
        return true
    }
    
    override func canPerformMenuControllerAction(_ action: Selector) -> Bool {
        
        guard action != copySelector else {
            return !messageViewModel.text.isEmpty
        }
        
        guard action != copyCoordinatesSelector else {
            return true
        }
        
        return false
    }
}

//
//  ChatMapMessageCellObject.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 15.08.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import ChattoAdditions

/// Cell object для сообщения с картой
class ChatMapMessageCellObject: ChatTextMessageCellObject {
    
    /// Точка на карте, которую нужно отобразить
    var location: Location = moscowLocation
    
    /// Название объекта
    var title: String?
    
    /// Описание объекта
    var description: String?
    
    /// Создает копию cell object
    ///
    /// - Returns: копия
    override func copy() -> BaseMessageCellObjectProtocol {
        let copy = ChatMapMessageCellObject(messageUid: messageUid, messageModel: _messageModel, text: text)
        copy.location = location
        copy.title = title
        copy.description = description
        copy.isLastMessage = isLastMessage
        copy.textFormatting = textFormatting
        copy.inlineKeyboard = inlineKeyboard
        copy.inlineOutsideKeyboard = inlineOutsideKeyboard
        copy.optionsKeyboard = optionsKeyboard
        return copy
    }
}

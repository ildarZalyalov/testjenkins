//
//  ChatMapBubbleViewStyleProtocol.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 15.08.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

typealias ChatMapMessageCollectionViewCellStyleProtocol = ChatMapBubbleViewStyleProtocol

/// Стиль для ChatMapBubbleView
protocol ChatMapBubbleViewStyleProtocol: ChatTextBubbleViewStyleProtocol {
    
    /// Размер превью с картой
    func mapPreviewSize(viewModel: ChatMapMessageViewModelProtocol, isSelected: Bool) -> CGSize
    
    /// Шрифт для названия местоположения
    func locationNameFont(viewModel: ChatMapMessageViewModelProtocol, isSelected: Bool) -> UIFont
    
    /// Стиль абзаца для названия местоположения
    func locationNameParagraphStyle(viewModel: ChatMapMessageViewModelProtocol, isSelected: Bool) -> NSParagraphStyle?
    
    /// Цвет текста для названия местоположения
    func locationNameTextColor(viewModel: ChatTextMessageViewModelProtocol, isSelected: Bool) -> UIColor
    
    /// Шрифт для описания местоположения
    func locationDescriptionFont(viewModel: ChatMapMessageViewModelProtocol, isSelected: Bool) -> UIFont
    
    /// Стиль абзаца для описания местоположения
    func locationDescriptionParagraphStyle(viewModel: ChatMapMessageViewModelProtocol, isSelected: Bool) -> NSParagraphStyle?
    
    /// Цвет текста для описания местоположения
    func locationDescriptionTextColor(viewModel: ChatTextMessageViewModelProtocol, isSelected: Bool) -> UIColor
}

//
//  ChatMapMessageHandler.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 15.08.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import ChattoAdditions

/// Обработчик действий пользователя для сообщений с картой
class ChatMapMessageHandler: BaseInteractionHandlerProtocol {
    
    weak var baseHandler: BaseChatMessageHandler?
    
    init(baseHandler: BaseChatMessageHandler) {
        self.baseHandler = baseHandler
    }
    
    func userDidTapOnFailIcon(viewModel: ChatMapMessageViewModel, failIconView: UIView) {
        baseHandler?.userDidTapOnFailIcon(viewModel: viewModel, failIconView: failIconView)
    }
    
    func userDidTapOnAvatar(viewModel: ChatMapMessageViewModel) {
        baseHandler?.userDidTapOnAvatar(viewModel: viewModel)
    }
    
    func userDidTapOnBubble(viewModel: ChatMapMessageViewModel) {
        baseHandler?.userDidTapOnBubble(viewModel: viewModel)
    }
    
    func userDidBeginLongPressOnBubble(viewModel: ChatMapMessageViewModel) {
        baseHandler?.userDidBeginLongPressOnBubble(viewModel: viewModel)
    }
    
    func userDidEndLongPressOnBubble(viewModel: ChatMapMessageViewModel) {
        baseHandler?.userDidEndLongPressOnBubble(viewModel: viewModel)
    }
    
    func userDidSelectMessage(viewModel: ChatMapMessageViewModel) {
        baseHandler?.userDidSelectMessage(viewModel: viewModel)
    }
    
    func userDidDeselectMessage(viewModel: ChatMapMessageViewModel) {
        baseHandler?.userDidDeselectMessage(viewModel: viewModel)
    }
    
    func userDidPressInlineKeyboardButton(viewModel: BaseMessageViewModelProtocol, with buttonId: String) {
        baseHandler?.userDidPressInlineKeyboardButton(viewModel: viewModel, with: buttonId)
    }
    
    func userDidPressExpandInlineKeyboardButton(viewModel: BaseMessageViewModelProtocol) {
        baseHandler?.userDidPressExpandInlineKeyboardButton(viewModel: viewModel)
    }
    
    func userDidPressOptionButton(viewModel: BaseMessageViewModelProtocol, with buttonId: String) {
        baseHandler?.userDidPressOptionButton(viewModel: viewModel, with: buttonId)
    }
    
    func userDidPressToOpen(url: URL, inMessage viewModel: BaseMessageViewModelProtocol) {
        baseHandler?.userDidPressToOpen(url: url, inMessage: viewModel)
    }
}

//
//  MessageLoaderPresenter.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 19.06.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import Chatto

class MessageLoaderPresenter: ChatItemPresenterProtocol {
    
    let messageLoaderModel: MessageLoaderCellObject
    init (messageLoaderModel: MessageLoaderCellObject) {
        self.messageLoaderModel = messageLoaderModel
    }
    
    fileprivate static let cellReuseIdentifier = String(describing: MessageLoaderCell.self)
    
    //MARK: - ChatItemPresenterProtocol
    
    var canCalculateHeightInBackground: Bool {
        return true
    }
    
    static func registerCells(_ collectionView: UICollectionView) {
        collectionView.register(MessageLoaderCell.self, forCellWithReuseIdentifier: cellReuseIdentifier)
    }
    
    func dequeueCell(collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell {
        return collectionView.dequeueReusableCell(withReuseIdentifier: MessageLoaderPresenter.cellReuseIdentifier, for: indexPath)
    }
    
    func configureCell(_ cell: UICollectionViewCell, decorationAttributes: ChatItemDecorationAttributesProtocol?) {}
    
    func heightForCell(maximumWidth width: CGFloat, decorationAttributes: ChatItemDecorationAttributesProtocol?) -> CGFloat {
        return 24
    }
    
    func cellWillBeShown(_ cell: UICollectionViewCell) {
        guard let messageLoaderCell = cell as? MessageLoaderCell else { return }
        messageLoaderCell.activityIndicator.startAnimating()
    }
    
    func cellWasHidden(_ cell: UICollectionViewCell) {
        guard let messageLoaderCell = cell as? MessageLoaderCell else { return }
        messageLoaderCell.activityIndicator.stopAnimating()
    }
}

class MessageLoaderPresenterBuilder: ChatItemPresenterBuilderProtocol {
    
    public func canHandleChatItem(_ chatItem: ChatItemProtocol) -> Bool {
        return chatItem is MessageLoaderCellObject
    }
    
    public func createPresenterWithChatItem(_ chatItem: ChatItemProtocol) -> ChatItemPresenterProtocol {
        precondition(canHandleChatItem(chatItem), "Попытка создать MessageLoaderPresenter для \(type(of: chatItem))")
        return MessageLoaderPresenter(messageLoaderModel: chatItem as! MessageLoaderCellObject)
    }
    
    public var presenterType: ChatItemPresenterProtocol.Type {
        return MessageLoaderPresenter.self
    }
}

//
//  MessageLoaderCellObject.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 19.06.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import Chatto

class MessageLoaderCellObject: ChatItemProtocol {
    
    let uid: String = UUID().uuidString
    let type: ChatItemType = MessageLoaderCellObject.chatItemType
    
    static var chatItemType: ChatItemType {
        return String(describing: MessageLoaderCellObject.self)
    }
}

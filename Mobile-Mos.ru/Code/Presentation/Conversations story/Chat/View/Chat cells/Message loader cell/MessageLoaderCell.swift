//
//  MessageLoaderCell.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 19.06.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import UIKit

class MessageLoaderCell: UICollectionViewCell {
    
    let activityIndicator = UIActivityIndicatorView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    fileprivate func commonInit() {
        
        activityIndicator.autoresizingMask = [.flexibleLeftMargin, .flexibleRightMargin, .flexibleTopMargin, .flexibleBottomMargin]
        activityIndicator.activityIndicatorViewStyle = .gray
        contentView.addSubview(activityIndicator)
        
        backgroundColor = UIColor.clear
        contentView.backgroundColor = UIColor.clear
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        activityIndicator.center = contentView.center
    }
}

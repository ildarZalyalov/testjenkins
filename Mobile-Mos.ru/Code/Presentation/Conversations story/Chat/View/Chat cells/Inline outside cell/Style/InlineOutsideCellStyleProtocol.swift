//
//  InlineOutsideCellStyleProtocol.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 18.07.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Инсеты справа и слева для инлайн кнопок

typealias InlineKeyboardButtonInsets = (left: CGFloat, right: CGFloat)

/// Протокол стиля для ячейки с внешней инлайн клавиатурой
protocol InlineOutsideCellStyleProtocol {
    
    /// Шрифт заголовков инлайн кнопок
    func inlineButtonTextFont(viewModel: InlineKeyboardButtonViewModel, isSelected: Bool) -> UIFont
    
    /// Цвет заголовков инлайн кнопок
    func inlineButtonTextColor(viewModel: InlineKeyboardButtonViewModel, isSelected: Bool) -> UIColor
    
    /// Цвет заливки инлайн кнопок
    func inlineButtonBackgroundColor(viewModel: InlineKeyboardButtonViewModel, isSelected: Bool) -> UIColor
    
    /// Размер ряда инлайн кнопок
    func inlineButtonRowSize(viewModel: InlineOutsideViewModel, isSelected: Bool) -> CGSize
    
    /// Максимальное отображаемое кол-во рядов инлайн кнопок до их разворачивания 
    /// (если будет превышено, то отобразится кнопка разворачивания)
    func inlineButtonMaxCollapsedRowCount(viewModel: InlineOutsideViewModel, isSelected: Bool) -> Int
    
    /// Цвет сепараторов инлайн кнопок
    func inlineButtonSeparatorColor(viewModel: InlineOutsideViewModel, isSelected: Bool) -> UIColor
    
    /// Инсеты справа и слева инлайн кнопок
    func inlineButtonInsets(viewModel: InlineOutsideViewModel, isSelected: Bool) -> InlineKeyboardButtonInsets
    
    /// Радиус скругления набора инлайн кнопок
    func inlineButtonCornerRadius(viewModel: InlineOutsideViewModel, isSelected: Bool) -> CGFloat
}

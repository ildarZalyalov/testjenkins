//
//  InlineOutsideCellObject.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 18.07.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import Chatto

/// Cell object для внешней инлайн клавиатуры
class InlineOutsideCellObject: ChatItemProtocol {
    
    let uid: String = UUID().uuidString
    let type: ChatItemType = InlineOutsideCellObject.chatItemType
    
    /// Cell object сообщения, с которым связана клавиатура
    var messageCellObject: BaseMessageCellObjectProtocol
    
    static var chatItemType: ChatItemType {
        return String(describing: InlineOutsideCellObject.self)
    }
    
    init(with messageCellObject: BaseMessageCellObjectProtocol) {
        self.messageCellObject = messageCellObject
    }
}

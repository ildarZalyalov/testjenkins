//
//  InlineOutsideViewModel.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 18.07.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// View model ячейки с внешней инлайн клавиатурой
class InlineOutsideViewModel {
    
    /// Идентификатор сообщения, с которым связана клавиатура
    var messageUid: String
    
    /// Связана ли клавиатура со входящим сообщением
    var isIncomingMessage: Bool
    
    /// View model кнопок на клавиатуре
    var inlineButtons: [[InlineKeyboardButtonViewModel]]
    
    /// Показывается ли кнопка "раскрытия" дополнительных кнопок
    var isShowingExpandButton: Bool
    
    init(messageUid: String,
         isIncomingMessage: Bool,
         inlineButtons: [[InlineKeyboardButtonViewModel]],
         isShowingExpandButton: Bool) {
        
        self.messageUid = messageUid
        self.isIncomingMessage = isIncomingMessage
        self.inlineButtons = inlineButtons
        self.isShowingExpandButton = isShowingExpandButton
    }
}

/// Билдер view model ячейки с внешней инлайн клавиатурой
class InlineOutsideViewModelBuilder {
    
    func createViewModel(from message: BaseMessageCellObjectProtocol) -> InlineOutsideViewModel {
        return InlineOutsideViewModel(messageUid: message.messageUid,
                                      isIncomingMessage: message.isIncoming,
                                      inlineButtons: inlineButtonsViewModel(from: message),
                                      isShowingExpandButton: message.inlineOutsideKeyboard.isCollapsed)
    }
    
    fileprivate func inlineButtonsViewModel(from message: BaseMessageCellObjectProtocol) -> [[InlineKeyboardButtonViewModel]] {
        
        let keyboard = message.inlineOutsideKeyboard
        let maxNumberOfButtons = keyboard.isCollapsed ? keyboard.maxRowCountBeforeCollapsing : Int.max
        
        return keyboard.inlineButtons.lazy.prefix(maxNumberOfButtons).map { $0.map {
            InlineKeyboardButtonViewModel(itemId: $0.itemId,
                                          title: $0.title,
                                          isEnabled: $0.isAlwaysActive || message.isLastMessage) }
        }
    }
}

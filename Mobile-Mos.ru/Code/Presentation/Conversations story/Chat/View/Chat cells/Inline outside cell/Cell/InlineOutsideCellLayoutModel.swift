//
//  InlineOutsideCellLayoutModel.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 18.07.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import Chatto

/// Модель лейаута для ячейки с внешней инлайн клавиатурой
class InlineOutsideCellLayoutModel {
    
    /// Содержит входные данные для расчета, а также служит ключом хэширования
    class LayoutContext: Hashable {
        
        /// Количества кнопок в каждом ряду
        let inlineButtonsByRowCounts: [Int]
        /// Размер ряда инлайн кнопок (смотри BaseBubbleViewStyleProtocol)
        let inlineButtonRowSize: CGSize
        /// Инсеты справа и слева инлайн кнопок
        let inlineButtonsInsets: InlineKeyboardButtonInsets
        
        init(inlineButtonsByRowCounts: [Int],
             inlineButtonRowSize: CGSize,
             inlineButtonsInsets: InlineKeyboardButtonInsets) {
            
            self.inlineButtonsByRowCounts = inlineButtonsByRowCounts
            self.inlineButtonRowSize = inlineButtonRowSize
            self.inlineButtonsInsets = inlineButtonsInsets
        }
        
        var hashValue: Int {
            return bma_combine(hashes: [inlineButtonsByRowCounts.combinedHashValue,
                                        inlineButtonRowSize.combinedHashValue,
                                        inlineButtonsInsets.left.hashValue,
                                        inlineButtonsInsets.right.hashValue])
        }
        
        static func == (lhs: LayoutContext, rhs: LayoutContext) -> Bool {
            
            if lhs === rhs { return true }
            
            let arrayEqual = lhs.inlineButtonsByRowCounts == rhs.inlineButtonsByRowCounts
            let lhsValues = (lhs.inlineButtonRowSize, lhs.inlineButtonsInsets.left, lhs.inlineButtonsInsets.right)
            let rhsValues = (rhs.inlineButtonRowSize, rhs.inlineButtonsInsets.left, rhs.inlineButtonsInsets.right)
            
            return lhsValues == rhsValues && arrayEqual
        }
    }
    
    /// Высота горизонтального сепаратора между рядами кнопок
    static let inlineButtonRowSpace: CGFloat = 1
    
    /// Толщина вертикального сепаратора между кнопками в ряду
    static let inlineButtonColumnSpace: CGFloat = 1
    
    let layoutContext: LayoutContext
    
    /// Размер полученного лейаута
    var size: CGSize = CGSize.zero
    
    /// Фрейм вьюхи-бекграунда инлайн клавиатуры
    var inlineButtonBackgroundFrame: CGRect = CGRect.zero
    
    /// Фрейм вьюхи, содержащей все кнопки инлайн клавиатуры
    var inlineButtonsParentFrame: CGRect = CGRect.zero
    
    /// Фреймы инлайн кнопок (ряды объединены в один массив друг за другом)
    var inlineButtonFrames: [CGRect] = []
    
    required init(layoutContext: LayoutContext) {
        self.layoutContext = layoutContext
    }
    
    /// Пересчитать кэш лейаута
    func calculateLayout() {

        let buttonsRowCount = layoutContext.inlineButtonsByRowCounts.count
        let buttonSize = layoutContext.inlineButtonRowSize
        let buttonInsets = layoutContext.inlineButtonsInsets
        
        let inlineButtonRowSpace = InlineOutsideCellLayoutModel.inlineButtonRowSpace
        let inlineButtonColumnSpace = InlineOutsideCellLayoutModel.inlineButtonColumnSpace
        
        if buttonsRowCount > 0 {
            
            var inlineButtonsWidth = buttonSize.width + buttonInsets.left + buttonInsets.right
            inlineButtonsWidth += 2 * inlineButtonColumnSpace
            
            var inlineButtonsHeight = CGFloat(buttonsRowCount) * buttonSize.height
            inlineButtonsHeight += (CGFloat(buttonsRowCount) + 1) * inlineButtonRowSpace
            
            size = CGSize(width: inlineButtonsWidth, height: inlineButtonsHeight)
            
            inlineButtonBackgroundFrame = CGRect(x: buttonInsets.left,
                                                 y: 0,
                                                 width: size.width - buttonInsets.left - buttonInsets.right,
                                                 height: inlineButtonsHeight)
            
            inlineButtonsParentFrame = CGRect(x: 0, y: 0, width: inlineButtonBackgroundFrame.width, height: inlineButtonBackgroundFrame.height)
            inlineButtonsParentFrame = inlineButtonsParentFrame.insetBy(dx: inlineButtonColumnSpace, dy: inlineButtonRowSpace)
        }
        
        inlineButtonFrames = []
        
        guard buttonsRowCount > 0 else { return }
        
        var currentY: CGFloat = 0
        let buttonHeight = layoutContext.inlineButtonRowSize.height
        
        for buttonsCount in layoutContext.inlineButtonsByRowCounts {
            
            let count = CGFloat(buttonsCount)
            var buttonWidth = inlineButtonsParentFrame.width
            buttonWidth -= (count - 1) * inlineButtonColumnSpace
            buttonWidth /= count
            
            var currentX: CGFloat = 0
            
            for _ in 0 ..< buttonsCount {
                
                let buttonFrame = CGRect(x: currentX, y: currentY, width: buttonWidth, height: buttonHeight)
                inlineButtonFrames.append(buttonFrame)
                
                currentX += inlineButtonColumnSpace + buttonWidth
            }
            
            currentY += buttonHeight + inlineButtonRowSpace
        }
    }
}

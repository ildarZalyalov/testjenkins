//
//  InlineOutsideInteractionHandler.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 19.07.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Обработчик взаимодействия с ячейкой с внешней инлайн клавиатурой
protocol InlineOutsideInteractionHandler: class {
    
    /// Пользователь нажал на кнопку инлайн клавиатуры
    ///
    /// - Parameters:
    ///   - viewModel: view model ячейки
    ///   - buttonId: идентификатор кнопки
    func userDidPressInlineKeyboardButton(viewModel: InlineOutsideViewModel, with buttonId: String)
    
    /// Пользователь нажал на кнопку "расхлопывания" инлайн клавиатуры
    ///
    /// - Parameter viewModel: view model ячейки
    func userDidPressExpandInlineKeyboardButton(viewModel: InlineOutsideViewModel)
}

//
//  InlineOutsidePresenter.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 18.07.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import Chatto

/// Presenter ячейки с внешней инлайн клавиатурой
class InlineOutsidePresenter: ChatItemPresenterProtocol {
    
    /// Cell object для внешней инлайн клавиатуры
    let inlineOutsideModel: InlineOutsideCellObject
    
    /// Ячейка, используемая для подсчета лейаута
    let sizingCell: InlineOutsideCell
    
    /// Кэш лейаутов данного типа ячеек
    let layoutCache: NSCache<AnyObject, AnyObject>
    
    /// Стиль для ячейки
    let cellStyle: InlineOutsideCellStyleProtocol
    
    /// Обработчик взаимодействия с ячейкой
    weak var interactionHandler: InlineOutsideInteractionHandler?
    
    /// Билдер view model для ячейки
    let viewModelBuilder = InlineOutsideViewModelBuilder()
    
    init(inlineOutsideModel: InlineOutsideCellObject,
         sizingCell: InlineOutsideCell,
         layoutCache: NSCache<AnyObject, AnyObject>,
         cellStyle: InlineOutsideCellStyleProtocol,
         interactionHandler: InlineOutsideInteractionHandler?) {
        
        self.inlineOutsideModel = inlineOutsideModel
        self.sizingCell = sizingCell
        self.layoutCache = layoutCache
        self.cellStyle = cellStyle
        self.interactionHandler = interactionHandler
    }
    
    fileprivate static let cellReuseIdentifier = String(describing: InlineOutsideCell.self)
    
    fileprivate(set) lazy var viewModel: InlineOutsideViewModel = {
        return self.viewModelBuilder.createViewModel(from: self.inlineOutsideModel.messageCellObject)
    }()
    
    //MARK: - ChatItemPresenterProtocol
    
    var canCalculateHeightInBackground: Bool {
        return sizingCell.canCalculateSizeInBackground
    }
    
    static func registerCells(_ collectionView: UICollectionView) {
        collectionView.register(InlineOutsideCell.self, forCellWithReuseIdentifier: cellReuseIdentifier)
    }
    
    func dequeueCell(collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell {
        return collectionView.dequeueReusableCell(withReuseIdentifier: InlineOutsidePresenter.cellReuseIdentifier, for: indexPath)
    }
    
    func configureCell(_ cell: UICollectionViewCell, decorationAttributes: ChatItemDecorationAttributesProtocol?) {
        
        guard let inlineOutsideCell = cell as? InlineOutsideCell else { return }
        
        inlineOutsideCell.update(with: viewModel, layoutCache: layoutCache, style: cellStyle)
        inlineOutsideCell.onInlineKeyboardButtonPress = { [weak self] _, buttonId in
            guard let strongSelf = self else { return }
            strongSelf.interactionHandler?.userDidPressInlineKeyboardButton(viewModel: strongSelf.viewModel, with: buttonId)
        }
        inlineOutsideCell.onExpandInlineKeyboardButtonPress = { [weak self] _ in
            guard let strongSelf = self else { return }
            strongSelf.interactionHandler?.userDidPressExpandInlineKeyboardButton(viewModel: strongSelf.viewModel)
        }
    }
    
    func heightForCell(maximumWidth width: CGFloat, decorationAttributes: ChatItemDecorationAttributesProtocol?) -> CGFloat {
        configureCell(sizingCell, decorationAttributes: decorationAttributes)
        return sizingCell.sizeThatFits(CGSize(width: width, height: .greatestFiniteMagnitude)).height
    }
}

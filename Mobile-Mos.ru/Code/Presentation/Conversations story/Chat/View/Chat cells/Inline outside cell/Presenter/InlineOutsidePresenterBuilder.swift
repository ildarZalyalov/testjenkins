//
//  InlineOutsidePresenterBuilder.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 19.07.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import Chatto

/// Билдер presenter-а ячейки с внешней инлайн клавиатурой
class InlineOutsidePresenterBuilder: ChatItemPresenterBuilderProtocol {
    
    /// Обработчик взаимодействия с ячейкой
    weak var interactionHandler: InlineOutsideInteractionHandler?
    
    /// Кэш лейаутов данного типа ячеек
    let layoutCache = NSCache<AnyObject, AnyObject>()
    
    /// Стиль для ячейки
    lazy var cellStyle: InlineOutsideCellStyleProtocol = InlineOutsideCellStyleDefault()
    
    init(interactionHandler: InlineOutsideInteractionHandler) {
        self.interactionHandler = interactionHandler
    }
    
    lazy var sizingCell: InlineOutsideCell = {
        var cell: InlineOutsideCell? = nil
        if Thread.isMainThread {
            cell = InlineOutsideCell.sizingCell()
        } else {
            DispatchQueue.main.sync(execute: {
                cell =  InlineOutsideCell.sizingCell()
            })
        }
        
        return cell!
    }()
    
    public func canHandleChatItem(_ chatItem: ChatItemProtocol) -> Bool {
        return chatItem is InlineOutsideCellObject
    }
    
    public func createPresenterWithChatItem(_ chatItem: ChatItemProtocol) -> ChatItemPresenterProtocol {
        precondition(canHandleChatItem(chatItem), "Попытка создать InlineOutsidePresenter для \(type(of: chatItem))")
        return InlineOutsidePresenter(inlineOutsideModel: chatItem as! InlineOutsideCellObject,
                                      sizingCell: sizingCell,
                                      layoutCache: layoutCache,
                                      cellStyle: cellStyle,
                                      interactionHandler: interactionHandler)
    }
    
    public var presenterType: ChatItemPresenterProtocol.Type {
        return InlineOutsidePresenter.self
    }
}

//
//  InlineOutsideCell.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 18.07.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import UIKit
import ChattoAdditions

/// Ячейка с внешней инлайн клавиатурой
class InlineOutsideCell: UICollectionViewCell, BackgroundSizingQueryable {
    
    /// Контекст описывает отображается ли ячейка или используется для вычисления лейаута
    var viewContext: ViewContext = .normal
    
    /// Может ли ячейка подсчитывать свой лейаут в фоновом потоке
    var canCalculateSizeInBackground = true
    
    private let buttonControlsCache = KeyboardButtonControlCache()
    private var buttonControls = [KeyboardButtonControl]()
    
    /// View model для кнопок
    private var buttonsViewModel: InlineOutsideViewModel?
    
    /// Кэш лейаутов данного типа ячеек
    private var cellLayoutCache: NSCache<AnyObject, AnyObject>?
    
    /// Стиль для ячейки
    private var cellStyle: InlineOutsideCellStyleProtocol?
    
    /// Обработчик нажатия на кнопку в клавиатуре
    var onInlineKeyboardButtonPress: ((_ cell: InlineOutsideCell, _ buttonId: String) -> Void)?
    
    /// Обработчик нажатия на кнопку "расхлопывания" дополнительных кнопок
    var onExpandInlineKeyboardButtonPress: ((_ cell: InlineOutsideCell) -> Void)?
    
    /// View-бекграунд инлайн клавиатуры
    private let inlineButtonBackgroundView: UIView = {
        let view = UIView()
        view.clipsToBounds = true
        return view
    }()
    
    /// Родительсвкая view для кнопок клавиатуры
    private let buttonControlsParentView: UIView = {
        let view = UIView()
        view.clipsToBounds = true
        return view
    }()
    
    /// Кнопка "расхлопывания" дополнительных кнопок
    private lazy var expandButton: ChatMessageExpandInlineKeyboardButton = {
        
        let borderWidth = InlineOutsideCellLayoutModel.inlineButtonColumnSpace
        let button = ChatMessageExpandInlineKeyboardButton(withBorderWidth: borderWidth)
        
        button.addTarget(self, action: #selector(self.expandButtonPressed(button:)), for: .touchUpInside)
        self.buttonControlsParentView.addSubview(button)

        return button
    }()
    
    /// View model кнопки "расхлопывания" дополнительных кнопок
    private lazy var expandButtonViewModel: InlineKeyboardButtonViewModel = {
        return InlineKeyboardButtonViewModel(itemId: UUID().uuidString, title: String(), isEnabled: true)
    }()
    
    static func sizingCell() -> InlineOutsideCell {
        let cell = InlineOutsideCell(frame: CGRect.zero)
        cell.viewContext = .sizing
        return cell
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    private func commonInit() {
        
        backgroundColor = UIColor.clear
        contentView.backgroundColor = UIColor.clear
        
        addSubview(inlineButtonBackgroundView)
        inlineButtonBackgroundView.addSubview(buttonControlsParentView)

        buttonControlsCache.onNewCachedItemHandler = { [unowned self] button in
            
            button.contentHorizontalAlignment = .left
            button.contentVerticalAlignment = .fill
            button.contentEdgeInsets = UIEdgeInsets(top: 0, left: 12, bottom: 0, right: 12)
            button.titleLabel?.numberOfLines = 0
            
            button.addTarget(self, action: #selector(self.buttonPressed(button:)), for: .touchUpInside)
            self.buttonControlsParentView.addSubview(button)
        }
    }
    
    @objc
    private func buttonPressed(button: UIButton) {
        if let keyboardControl = button as? KeyboardButtonControl, keyboardControl.buttonId != nil {
            onInlineKeyboardButtonPress?(self, keyboardControl.buttonId!)
        }
    }
    
    @objc
    private func expandButtonPressed(button: UIButton) {
        onExpandInlineKeyboardButtonPress?(self)
    }
    
    override func layoutSubviews() {
        
        super.layoutSubviews()
        
        guard let layout = calculateLayout(preferredMaxLayoutWidth: contentView.bounds.width) else { return }
        
        inlineButtonBackgroundView.bma_rect = layout.inlineButtonBackgroundFrame
        buttonControlsParentView.bma_rect = layout.inlineButtonsParentFrame
        
        let minCount = min(buttonControls.count, layout.inlineButtonFrames.count)
        for index in 0 ..< minCount {
            buttonControls[index].bma_rect = layout.inlineButtonFrames[index]
        }
        
        guard !expandButton.isHidden, let lastFrame = layout.inlineButtonFrames.last else {
            return
        }
        expandButton.bma_rect = lastFrame
    }
    
    override func sizeThatFits(_ size: CGSize) -> CGSize {
        return calculateLayout(preferredMaxLayoutWidth: size.width)?.size ?? CGSize.zero
    }
    
    func update(with viewModel: InlineOutsideViewModel,
                layoutCache: NSCache<AnyObject, AnyObject>,
                style: InlineOutsideCellStyleProtocol) {
        
        buttonsViewModel = viewModel
        cellLayoutCache = layoutCache
        cellStyle = style
        
        guard viewContext != .sizing else {
            return
        }
        
        defer {
            setNeedsLayout()
        }
        
        guard viewModel.inlineButtons.count > 0 else { return }
        
        var count = 0
        viewModel.inlineButtons.forEach { count += $0.count }
        
        let parentViewHidden = count == 0
        inlineButtonBackgroundView.isHidden = parentViewHidden
        
        guard !parentViewHidden else { return }
        
        if count != buttonControls.count {
            buttonControls = buttonControlsCache.obtain(count: count)
        }
        
        let separatorColor = style.inlineButtonSeparatorColor(viewModel: viewModel, isSelected: isSelected)
        if inlineButtonBackgroundView.backgroundColor != separatorColor {
            inlineButtonBackgroundView.backgroundColor = separatorColor
        }
        
        let cornerRadius = style.inlineButtonCornerRadius(viewModel: viewModel, isSelected: isSelected)
        if inlineButtonBackgroundView.layer.cornerRadius != cornerRadius {
            inlineButtonBackgroundView.layer.cornerRadius = cornerRadius
        }
        if buttonControlsParentView.layer.cornerRadius != cornerRadius {
            buttonControlsParentView.layer.cornerRadius = cornerRadius
        }
        
        var buttonIndex = 0
        
        for row in viewModel.inlineButtons {
            for model in row {
                update(button: &buttonControls[buttonIndex], with: model)
                buttonIndex += 1
            }
        }
        
        let shouldHideExpandButton = !viewModel.isShowingExpandButton
        expandButton.isHidden = shouldHideExpandButton
        
        guard viewModel.isShowingExpandButton else { return }
        
        updateExpandButton()
    }
    
    /// Обновить кнопку инлайн клавиатуры
    ///
    /// - Parameters:
    ///   - button: кнопка инлайн клавиатуры
    ///   - model: view model кнопки инлайн клавиатуры
    private func update(button: inout KeyboardButtonControl, with model: InlineKeyboardButtonViewModel) {
        
        guard let style = cellStyle else { return }
        
        let font = style.inlineButtonTextFont(viewModel: model, isSelected: isSelected)
        let backgroundColor = style.inlineButtonBackgroundColor(viewModel: model, isSelected: isSelected)
        let titleColor = style.inlineButtonTextColor(viewModel: model, isSelected: isSelected)
        
        button.buttonId = model.itemId
        button.isEnabled = model.isEnabled
        
        button.normalTintColor = backgroundColor
        button.highlightedTintColor = titleColor
        
        if button.backgroundColor != backgroundColor {
            button.backgroundColor = backgroundColor
        }
        
        button.isHighlightedChanged = { highlightedButton, isHighlighted in
            highlightedButton.backgroundColor = isHighlighted ? highlightedButton.highlightedTintColor : highlightedButton.normalTintColor
        }
        
        if button.titleLabel?.font != font {
            button.titleLabel?.font = font
        }
        
        if button.title(for: .normal) != model.title {
            button.setTitle(model.title, for: .normal)
        }
        if button.titleColor(for: .normal) != titleColor {
            button.setTitleColor(titleColor, for: .normal)
        }
        if button.titleColor(for: .highlighted) != backgroundColor {
            button.setTitleColor(backgroundColor, for: .highlighted)
        }
    }
    
    /// Обновить кнопку "расхлопывания" инлайн клавиатуры
    private func updateExpandButton() {
        
        guard let viewModel = buttonsViewModel, let style = cellStyle else { return }
        
        expandButtonViewModel.isEnabled = viewModel.inlineButtons.last?.last?.isEnabled ?? true
        
        let backgroundColor = style.inlineButtonBackgroundColor(viewModel: expandButtonViewModel, isSelected: isSelected)
        let titleColor = style.inlineButtonTextColor(viewModel: expandButtonViewModel, isSelected: isSelected)

        expandButton.update(withIconColor: titleColor, andBackgroundColor: backgroundColor)
    }
    
    private func calculateLayout(preferredMaxLayoutWidth: CGFloat) -> InlineOutsideCellLayoutModel? {
        
        guard let viewModel = buttonsViewModel, let style = cellStyle else { return nil }
        
        var inlineButtonsByRowCounts = viewModel.inlineButtons.map { $0.count }
        if viewModel.isShowingExpandButton {
            inlineButtonsByRowCounts += [1]
        }
        
        // мало ли не главный поток, тогда не стоит стучаться к вьюхам
        var selected = false
        if Thread.isMainThread {
            selected = isSelected
        }
        
        let layoutContext = InlineOutsideCellLayoutModel.LayoutContext(
            inlineButtonsByRowCounts: inlineButtonsByRowCounts,
            inlineButtonRowSize: style.inlineButtonRowSize(viewModel: viewModel, isSelected: selected),
            inlineButtonsInsets: style.inlineButtonInsets(viewModel: viewModel, isSelected: selected))
        
        let layoutKey = layoutContext.hashValue as AnyObject
        if let layoutModel = cellLayoutCache?.object(forKey: layoutKey) as? InlineOutsideCellLayoutModel, layoutModel.layoutContext == layoutContext {
            return layoutModel
        }
        
        let layoutModel = InlineOutsideCellLayoutModel(layoutContext: layoutContext)
        
        layoutModel.calculateLayout()
        cellLayoutCache?.setObject(layoutModel, forKey: layoutKey)
        
        return layoutModel
    }
}

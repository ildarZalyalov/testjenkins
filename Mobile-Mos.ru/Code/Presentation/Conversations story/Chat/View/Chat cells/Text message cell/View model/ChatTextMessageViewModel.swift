//
//  ChatTextMessageViewModel.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 30.05.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import ChattoAdditions

/// Протокол view model для ячейки текстового сообщения
protocol ChatTextMessageViewModelProtocol: BaseMessageViewModelProtocol, TextMessageViewModelProtocol {
    
    /// Форматированный текст сообщения без распарсенных тегов форматирования
    /// (поле text содержит теги форматирования в исходном виде)
    var formattedText: NSAttributedString { get }
}

/// View model текстового сообщения
class ChatTextMessageViewModel: TextMessageViewModel<ChatTextMessageCellObject>, ChatTextMessageViewModelProtocol {
    
    var messageUid: String
    var areInteractionGesturesDisabled: Bool = false
    var customBubbleColor: UIColor?
    var inlineButtons: [[InlineKeyboardButtonViewModel]]
    var optionButtons: [OptionsKeyboardButtonViewModel]
    var isShowingExpandButton: Bool
    var formattedText: NSAttributedString
    
    init(messageUid: String,
         textMessage: ChatTextMessageCellObject,
         messageViewModel: MessageViewModelProtocol,
         inlineButtons: [[InlineKeyboardButtonViewModel]] = [],
         optionButtons: [OptionsKeyboardButtonViewModel] = [],
         isShowingExpandButton: Bool = false,
         formattedText: NSAttributedString) {
        
        self.messageUid = messageUid
        self.areInteractionGesturesDisabled = textMessage.areInteractionGesturesDisabled
        self.inlineButtons = inlineButtons
        self.optionButtons = optionButtons
        self.isShowingExpandButton = isShowingExpandButton
        self.formattedText = formattedText
        super.init(textMessage: textMessage, messageViewModel: messageViewModel)
    }
}

/// Билдер view model текстового сообщения
class ChatTextMessageViewModelBuilder: ViewModelBuilderProtocol {
    
    let messageViewModelBuilder = MessageViewModelDefaultBuilder()
    
    let textCellStyle: ChatTextMessageCollectionViewCellStyleProtocol
    let textFormattingExtractor: ChatTextFormattingExtractor
    
    init(style: ChatTextMessageCollectionViewCellStyleProtocol, formattingExtractor: ChatTextFormattingExtractor) {
        textCellStyle = style
        textFormattingExtractor = formattingExtractor
    }
    
    func createViewModel(_ textMessage: ChatTextMessageCellObject) -> ChatTextMessageViewModel {
        
        let messageViewModel = messageViewModelBuilder.createMessageViewModel(textMessage)
        
        let formattedText = NSAttributedString(string: textMessage.text, attributes: nil)
        let textMessageViewModel = ChatTextMessageViewModel(messageUid: textMessage.messageUid, textMessage: textMessage, messageViewModel: messageViewModel, formattedText: formattedText)
        
        let lineSpacing = textCellStyle.textParagraphStyle(viewModel: textMessageViewModel, isSelected: false)?.lineSpacing ?? 0
        let font = textCellStyle.textFont(viewModel: textMessageViewModel, isSelected: false)
        let mediumFont = textCellStyle.mediumTextFont(viewModel: textMessageViewModel, isSelected: false)
        let boldFont = textCellStyle.boldTextFont(viewModel: textMessageViewModel, isSelected: false)
        
        let fontPalette = ChatTextFormattingFontPalette(lineSpacing: lineSpacing,
                                                        normalFont: font,
                                                        mediumFont: mediumFont,
                                                        boldFont: boldFont)
        
        if let formatting = textMessage.textFormatting {
            textMessageViewModel.formattedText = textFormattingExtractor.attributedString(from: formatting, with: fontPalette, for: textMessage.text)
            if let bubbleColor = formatting.messageBackgroundColor {
                textMessageViewModel.customBubbleColor = bubbleColor
            }
        }
        
        let keyboard = textMessage.inlineKeyboard
        let maxNumberOfButtons = keyboard.isCollapsed ? keyboard.maxRowCountBeforeCollapsing : Int.max
        
        textMessageViewModel.isShowingExpandButton = keyboard.isCollapsed
        textMessageViewModel.inlineButtons = keyboard.inlineButtons.lazy.prefix(maxNumberOfButtons).map { $0.map {
            InlineKeyboardButtonViewModel(itemId: $0.itemId,
                                          title: $0.title,
                                          isEnabled: $0.isAlwaysActive || textMessage.isLastMessage) }
        }
        
        textMessageViewModel.optionButtons = textMessage.optionsKeyboard.optionButtons.map {
            OptionsKeyboardButtonViewModel(itemId: $0.itemId, type: $0.type, isSelected: $0.isSelected)
        }
        
        return textMessageViewModel
    }
    
    func canCreateViewModel(fromModel model: Any) -> Bool {
        return model is ChatTextMessageCellObject
    }
}

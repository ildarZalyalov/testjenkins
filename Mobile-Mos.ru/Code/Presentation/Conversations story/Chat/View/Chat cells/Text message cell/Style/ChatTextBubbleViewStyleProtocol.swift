//
//  ChatTextBubbleViewStyleProtocol.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 25.05.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import ChattoAdditions

typealias ChatTextMessageCollectionViewCellStyleProtocol = ChatTextBubbleViewStyleProtocol

/// Стиль для ChatTextBubbleView
protocol ChatTextBubbleViewStyleProtocol: BaseBubbleViewStyleProtocol {
    
    /// Шрифт для текста сообщения
    func textFont(viewModel: ChatTextMessageViewModelProtocol, isSelected: Bool) -> UIFont
    
    /// Шрифт для медиум начертания текста сообщения
    func mediumTextFont(viewModel: ChatTextMessageViewModelProtocol, isSelected: Bool) -> UIFont?
    
    /// Шрифт для жирного начертания текста сообщения
    func boldTextFont(viewModel: ChatTextMessageViewModelProtocol, isSelected: Bool) -> UIFont?
    
    /// Стиль параграфа для текста сообщения (если нужно задать отличный от умолчания)
    func textParagraphStyle(viewModel: ChatTextMessageViewModelProtocol, isSelected: Bool) -> NSParagraphStyle?
    
    /// Цвет текста сообщения
    func textColor(viewModel: ChatTextMessageViewModelProtocol, isSelected: Bool) -> UIColor
    
    /// Инсеты для текста сообщения
    func textInsets(viewModel: ChatTextMessageViewModelProtocol, isSelected: Bool) -> UIEdgeInsets
}

//
//  ChatTextMessageTextView.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 25.05.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// UITextView с костылями для отключения выделения текста и других фич
/// Смотри приватный оригинал ChatMessageTextView в ChattoAdditions/TextBubbleView.swift
class ChatTextMessageTextView: UITextView {
    
    // Смотри https://github.com/badoo/Chatto/issues/363
    func disableGestureRecognizers() {
        gestureRecognizers?.forEach { $0.isEnabled = type(of: $0) == UILongPressGestureRecognizer.self && $0.delaysTouchesEnded }
    }
    
    override var canBecomeFirstResponder: Bool {
        return false
    }
    
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        return false
    }
    
    override var selectedRange: NSRange {
        get {
            return NSRange(location: 0, length: 0)
        }
        set {
            //для увеличения скорости скроллинга
        }
    }
    
    override var contentOffset: CGPoint {
        get {
            return .zero
        }
        set {
            //для увеличения скорости скроллинга
        }
    }
}

//
//  ChatTextBubbleView.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 25.05.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import ChattoAdditions

/// Делегат ChatTextBubbleView
protocol ChatTextBubbleViewDelegate: class {
    
    /// Было совершено действие для открытия веб-ссылки в тексте сообщения
    ///
    /// - Parameter url: веб-ссылка
    func didPressToOpen(url: URL)
}

/// View для баббла для ChatMessage, у которого тип контента - текст.
/// Смотри оригинал TextBubbleView в ChattoAdditions/TextBubbleView.swift
class ChatTextBubbleView: BaseBubbleView, UITextViewDelegate {
    
    /// Префикс схемы, совместимой с http (http/https)
    fileprivate let httpCompatibleSchemePrefix = "http"
    
    /// Текущий текст сообщения, включая все теги форматирования
    fileprivate var currentText: String = String()
    
    /// Текущие атрибуты отображения текста сообщения по умолчанию
    fileprivate var defaultTextAttributes = [NSAttributedStringKey : Any]()
    
    /// Контекст использования, позволяет задать используется ли view для вычисления размера или для отображения
    override var viewContext: ViewContext {
        didSet {
            switch viewContext {
            case .sizing:
                textView.dataDetectorTypes = UIDataDetectorTypes()
                textView.isSelectable = false
            default:
                textView.dataDetectorTypes = .all
                textView.isSelectable = true
            }
        }
    }
    
    /// Делегат ChatTextBubbleView
    weak var textBubbleDelegate: ChatTextBubbleViewDelegate? {
        didSet {
            textView.delegate = textBubbleDelegate != nil ? self : nil
        }
    }
    
    private var textView: UITextView = {
        
        let textView = ChatTextMessageTextView()
        
        UIView.performWithoutAnimation({ // ячейка перестает "моргать" при появлении
            textView.backgroundColor = UIColor.clear
        })
        
        textView.isEditable = false
        textView.isSelectable = true
        textView.dataDetectorTypes = .all
        textView.scrollsToTop = false
        textView.isScrollEnabled = false
        textView.bounces = false
        textView.bouncesZoom = false
        textView.showsHorizontalScrollIndicator = false
        textView.showsVerticalScrollIndicator = false
        textView.isUserInteractionEnabled = true
        textView.isExclusiveTouch = true
        textView.textContainer.lineFragmentPadding = 0
        textView.disableGestureRecognizers()
        
        return textView
    }()
 
    override func initializeContentSubviews(contentParentView: UIView) {
        contentParentView.addSubview(textView)
    }
    
    override func updateContentViews() {
        
        guard let style = style as? ChatTextBubbleViewStyleProtocol else { return }
        guard let viewModel = messageViewModel as? ChatTextMessageViewModelProtocol else { return }
        
        let font = style.textFont(viewModel: viewModel, isSelected: isSelected)
        let paragraphStyle = style.textParagraphStyle(viewModel: viewModel, isSelected: isSelected)
        let textColor = style.textColor(viewModel: viewModel, isSelected: isSelected)
        
        var needsToUpdateText = false
        
        let defaultFont = defaultTextAttributes[NSAttributedStringKey.font] as? UIFont
        if font != defaultFont {
            defaultTextAttributes[NSAttributedStringKey.font] = font
            needsToUpdateText = true
        }
        
        let defaultParagraphStyle = defaultTextAttributes[NSAttributedStringKey.paragraphStyle] as? NSParagraphStyle
        if paragraphStyle != defaultParagraphStyle {
            defaultTextAttributes[NSAttributedStringKey.paragraphStyle] = paragraphStyle
            needsToUpdateText = true
        }
        
        let defaultColor = defaultTextAttributes[NSAttributedStringKey.foregroundColor] as? UIColor
        if textColor != defaultColor {
            
            defaultTextAttributes[NSAttributedStringKey.foregroundColor] = textColor
            
            let linkColor = viewModel.isIncoming ? UIColorPalette.chatMessageDefaultLinkColor : textColor
            textView.linkTextAttributes = [
                NSAttributedStringKey.foregroundColor.rawValue : linkColor,
                NSAttributedStringKey.underlineStyle.rawValue : NSUnderlineStyle.styleSingle.rawValue
            ]
            
            needsToUpdateText = true
        }
        
        if needsToUpdateText || currentText != viewModel.text {
            
            currentText = viewModel.text
            
            // устанавливаем стиль по умолчанию, чтобы части текста без форматирования имели шрифт и отступы
            let attributedText = NSMutableAttributedString(string: viewModel.formattedText.string,
                                                           attributes: defaultTextAttributes)
            
            // добавляем форматирование в нужных фрагментах текста
            let allTextRange = NSMakeRange(0, viewModel.formattedText.length)
            viewModel.formattedText.enumerateAttributes(in: allTextRange, options: []) { (attributes, range, _) in
                attributedText.addAttributes(attributes, range: range)
            }
            
            textView.attributedText = attributedText
        }
        
        let textInsets = style.textInsets(viewModel: viewModel, isSelected: isSelected)
        if textView.textContainerInset != textInsets {
            textView.textContainerInset = textInsets
        }
    }
    
    override func createLayoutModelContext(preferredMaxLayoutWidth: CGFloat) -> BaseBubbleLayoutModel.LayoutContext? {
        
        guard let style = style as? ChatTextBubbleViewStyleProtocol else { return nil }
        guard let viewModel = messageViewModel as? ChatTextMessageViewModelProtocol else { return nil }
        
        var inlineButtonsByRowCounts = viewModel.inlineButtons.map { $0.count }
        if viewModel.isShowingExpandButton {
            inlineButtonsByRowCounts += [1]
        }
        
        let baseParameters = BaseBubbleLayoutModel.LayoutContext.LayoutParameters(
            inlineButtonsByRowCounts: inlineButtonsByRowCounts,
            optionButtonsCount: viewModel.optionButtons.count,
            inlineButtonRowSize: style.inlineButtonRowSize(viewModel: viewModel, isSelected: isSelected),
            contentInsets: style.contentInsets(viewModel: viewModel, isSelected: isSelected),
            fixedLayoutHeight: fixedLayoutHeight ?? 0
        )
        
        let textParameters = ChatTextBubbleLayoutModel.ChatTextLayoutContext.ChatTextLayoutParameters(
            text: viewModel.formattedText,
            font: style.textFont(viewModel: viewModel, isSelected: isSelected),
            paragraphStyle: style.textParagraphStyle(viewModel: viewModel, isSelected: isSelected) ?? NSParagraphStyle.default,
            textInsets: style.textInsets(viewModel: viewModel, isSelected: isSelected),
            preferredMaxLayoutWidth: preferredMaxLayoutWidth
        )
        
        return ChatTextBubbleLayoutModel.ChatTextLayoutContext(
            parameters: baseParameters,
            textParameters: textParameters
        )
    }
    
    override func layoutSubviews() {
        
        super.layoutSubviews()

        guard let viewModel = messageViewModel as? ChatTextMessageViewModelProtocol else { return }
        guard let textLayout = calculateBubbleLayout(preferredMaxLayoutWidth: preferredMaxLayoutWidth) as? ChatTextBubbleLayoutModel else { return }
        
        textView.bma_rect = textLayout.textFrame
        textView.isHidden = viewModel.text.isEmpty
    }
    
    //MARK: - UITextViewDelegate
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool {
        
        let canBeOpenedInBrowser = URL.scheme?.lowercased().hasPrefix(httpCompatibleSchemePrefix) ?? false
        guard canBeOpenedInBrowser else { return true }
        
        textBubbleDelegate?.didPressToOpen(url: URL)
        return false
    }
    
    @available(iOS 10.0, *)
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        
        let canBeOpenedInBrowser = URL.scheme?.lowercased().hasPrefix(httpCompatibleSchemePrefix) ?? false
        guard canBeOpenedInBrowser else { return true }
        guard interaction == .invokeDefaultAction else { return true }
        
        textBubbleDelegate?.didPressToOpen(url: URL)
        return false
    }
}

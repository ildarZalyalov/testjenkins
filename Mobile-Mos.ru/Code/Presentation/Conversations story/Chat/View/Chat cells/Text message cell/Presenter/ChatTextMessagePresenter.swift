//
//  ChatTextMessagePresenter.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 01.06.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import Chatto
import ChattoAdditions

class ChatTextMessagePresenter<ViewModelBuilderT, InteractionHandlerT>:
    BaseMessagePresenter<ChatTextBubbleView, ViewModelBuilderT, InteractionHandlerT>,
    BaseMessagePresenterProtocol where
    ViewModelBuilderT: ViewModelBuilderProtocol,
    ViewModelBuilderT.ViewModelT: ChatTextMessageViewModelProtocol,
    InteractionHandlerT: BaseInteractionHandlerProtocol,
    InteractionHandlerT.ViewModelT == ViewModelBuilderT.ViewModelT {
    
    public typealias ModelT = ViewModelBuilderT.ModelT
    public typealias ViewModelT = ViewModelBuilderT.ViewModelT
    
    public init (
        messageModel: ModelT,
        viewModelBuilder: ViewModelBuilderT,
        interactionHandler: InteractionHandlerT?,
        sizingCell: ChatTextMessageCollectionViewCell,
        baseCellStyle: BaseMessageCollectionViewCellStyleProtocol,
        textCellStyle: ChatTextMessageCollectionViewCellStyleProtocol,
        layoutCache: NSCache<AnyObject, AnyObject>) {
        self.layoutCache = layoutCache
        self.textCellStyle = textCellStyle
        super.init(
            messageModel: messageModel,
            viewModelBuilder: viewModelBuilder,
            interactionHandler: interactionHandler,
            sizingCell: sizingCell,
            cellStyle: baseCellStyle
        )
    }
    
    let layoutCache: NSCache<AnyObject, AnyObject>
    let textCellStyle: ChatTextMessageCollectionViewCellStyleProtocol
    
    override class func registerCells(_ collectionView: UICollectionView) {
        collectionView.register(ChatTextMessageCollectionViewCell.self, forCellWithReuseIdentifier: "chat-text-message-incoming")
        collectionView.register(ChatTextMessageCollectionViewCell.self, forCellWithReuseIdentifier: "chat-text-message-outcoming")
    }
    
    override func dequeueCell(collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell {
        let identifier = messageViewModel.isIncoming ? "chat-text-message-incoming" : "chat-text-message-outcoming"
        return collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath)
    }
    
    override func createViewModel() -> ViewModelBuilderT.ViewModelT {
        
        let viewModel = viewModelBuilder.createViewModel(messageModel)
        
        let updateClosure = { [weak self] (old: Any, new: Any) -> Void in
            self?.updateCurrentCell()
        }
        viewModel.avatarImage.observe(self, closure: updateClosure)
        
        return viewModel
    }
    
    public var textCell: ChatTextMessageCollectionViewCell? {
        
        if let cell = self.cell {
            if let textCell = cell as? ChatTextMessageCollectionViewCell {
                return textCell
            } else {
                assert(false, "Invalid cell was given to presenter!")
            }
        }
        
        return nil
    }
    
    override func configureCell(_ cell: BaseMessageCollectionViewCell<ChatTextBubbleView>, decorationAttributes: ChatItemDecorationAttributes, animated: Bool, additionalConfiguration: (() -> Void)?) {
        
        guard let cell = cell as? ChatTextMessageCollectionViewCell else {
            assert(false, "Invalid cell received")
            return
        }
        
        super.configureCell(cell, decorationAttributes: decorationAttributes, animated: animated) { () -> Void in
            
            cell.layoutCache = self.layoutCache
            cell.textMessageViewModel = self.messageViewModel
            cell.textMessageStyle = self.textCellStyle
            
            cell.onInlineKeyboardButtonPress = { [weak self] cell, buttonId in
                guard let strongSelf = self else { return }
                strongSelf.onInlineKeyboardButtonPress(with: buttonId, in: cell)
            }
            
            cell.onExpandInlineKeyboardButtonPress = { [weak self] cell in
                guard let strongSelf = self else { return }
                strongSelf.onExpandInlineKeyboardButtonPress(in: cell)
            }
            
            cell.onOptionButtonPress = { [weak self] cell, buttonId in
                guard let strongSelf = self else { return }
                strongSelf.onOptionButtonPress(with: buttonId, in: cell)
            }
            
            cell.onUrlInMessageTextOpen = { [weak self] cell, url in
                guard let strongSelf = self else { return }
                strongSelf.onOpenUrlPress(with: url, in: cell)
            }
            
            additionalConfiguration?()
        }
    }
    
    func updateCurrentCell() {
        if let cell = textCell, let decorationAttributes = decorationAttributes {
            configureCell(cell, decorationAttributes: decorationAttributes, animated: itemVisibility != .appearing, additionalConfiguration: nil)
        }
    }
    
    override func canShowMenu() -> Bool {
        return true
    }
    
    override func canPerformMenuControllerAction(_ action: Selector) -> Bool {
        let selector = #selector(UIResponderStandardEditActions.copy(_:))
        return action == selector
    }
    
    override func performMenuControllerAction(_ action: Selector) {
        
        let selector = #selector(UIResponderStandardEditActions.copy(_:))
        
        if action == selector {
            UIPasteboard.general.string = messageViewModel.text
        } else {
            assert(false, "Unexpected action")
        }
    }
    
    func onInlineKeyboardButtonPress(with buttonId: String, in cell: ChatTextMessageCollectionViewCell) {
        
        guard let messageViewModel = cell.textMessageViewModel else {
            return
        }
        
        interactionHandler?.userDidPressInlineKeyboardButton(viewModel: messageViewModel, with: buttonId)
    }
    
    func onExpandInlineKeyboardButtonPress(in cell: ChatTextMessageCollectionViewCell) {
        
        guard let messageViewModel = cell.textMessageViewModel else {
            return
        }
        
        interactionHandler?.userDidPressExpandInlineKeyboardButton(viewModel: messageViewModel)
    }
    
    func onOptionButtonPress(with buttonId: String, in cell: ChatTextMessageCollectionViewCell) {
        
        guard let messageViewModel = cell.textMessageViewModel else {
            return
        }
        
        interactionHandler?.userDidPressOptionButton(viewModel: messageViewModel, with: buttonId)
    }
    
    func onOpenUrlPress(with url: URL, in cell: ChatTextMessageCollectionViewCell) {
        
        guard let messageViewModel = cell.textMessageViewModel else {
            return
        }
        
        interactionHandler?.userDidPressToOpen(url: url, inMessage: messageViewModel)
    }
    
    //MARK: - BaseMessagePresenterProtocol
    
    func widthForBubbleInCell(maximumWidth width: CGFloat, decorationAttributes: ChatItemDecorationAttributesProtocol?) -> CGFloat {
        
        guard let decorationAttributes = decorationAttributes as? ChatItemDecorationAttributes else {
            assert(false, "Expecting decoration attributes")
            return 0
        }
        
        configureCell(sizingCell, decorationAttributes: decorationAttributes, animated: false, additionalConfiguration: nil)
        
        let layoutConstants = sizingCell.baseStyle.layoutConstants(viewModel: sizingCell.messageViewModel)
        let preferredWidthForBubble = (width * layoutConstants.maxContainerWidthPercentageForBubbleView).bma_round()
        
        return sizingCell.bubbleView.sizeThatFits(CGSize(width: preferredWidthForBubble, height: CGFloat.greatestFiniteMagnitude)).width
    }
}

//
//  ChatTextMessagePresenterBuilder.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 01.06.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import Chatto
import ChattoAdditions

class ChatTextMessagePresenterBuilder<ViewModelBuilderT, InteractionHandlerT>:
    ChatItemPresenterBuilderProtocol, BaseMessagePresenterBuilderProtocol where
    ViewModelBuilderT: ViewModelBuilderProtocol,
    ViewModelBuilderT.ViewModelT: ChatTextMessageViewModelProtocol,
    InteractionHandlerT: BaseInteractionHandlerProtocol,
    InteractionHandlerT.ViewModelT == ViewModelBuilderT.ViewModelT {
    
    typealias ViewModelT = ViewModelBuilderT.ViewModelT
    typealias ModelT = ViewModelBuilderT.ModelT
    
    public init(
        viewModelBuilder: ViewModelBuilderT,
        interactionHandler: InteractionHandlerT? = nil) {
        self.viewModelBuilder = viewModelBuilder
        self.interactionHandler = interactionHandler
    }
    
    let viewModelBuilder: ViewModelBuilderT
    let interactionHandler: InteractionHandlerT?
    let layoutCache = NSCache<AnyObject, AnyObject>()
    
    lazy var sizingTextCell: ChatTextMessageCollectionViewCell = {
        
        var cell: ChatTextMessageCollectionViewCell? = nil
        
        if Thread.isMainThread {
            cell = ChatTextMessageCollectionViewCell.createSizingCell()
        } else {
            DispatchQueue.main.sync(execute: {
                cell = ChatTextMessageCollectionViewCell.createSizingCell()
            })
        }
        
        return cell!
    }()
    
    lazy var textCellStyle: ChatTextMessageCollectionViewCellStyleProtocol = ChatTextBubbleViewStyleDefault()
    lazy var baseMessageStyle: BaseMessageCollectionViewCellStyleProtocol = BaseMessageCellStyleDefault()
    
    func canHandleChatItem(_ chatItem: ChatItemProtocol) -> Bool {
        return viewModelBuilder.canCreateViewModel(fromModel: chatItem)
    }
    
    func createPresenterWithChatItem(_ chatItem: ChatItemProtocol) -> ChatItemPresenterProtocol {
        
        assert(canHandleChatItem(chatItem))
        
        return ChatTextMessagePresenter<ViewModelBuilderT, InteractionHandlerT>(
            messageModel: chatItem as! ModelT,
            viewModelBuilder: viewModelBuilder,
            interactionHandler: interactionHandler,
            sizingCell: sizingTextCell,
            baseCellStyle: baseMessageStyle,
            textCellStyle: textCellStyle,
            layoutCache: layoutCache
        )
    }
    
    var presenterType: ChatItemPresenterProtocol.Type {
        return ChatTextMessagePresenter<ViewModelBuilderT, InteractionHandlerT>.self
    }
}

//
//  ChatTextMessageCellObject.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 30.05.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import ChattoAdditions

//О том, как устроено отображение сообщений в библиотеке Chatto - https://github.com/badoo/Chatto/wiki
//Используем стандартные классы из поставки ChattoAdditions, где модели из ChattoAdditions выступают в роли cell objects у нас

/// Cell object для текстового сообщения
class ChatTextMessageCellObject: TextMessageModel<MessageModel>, BaseMessageCellObjectProtocol {
    
    let messageUid: String
    
    /// Отключена ли обработка жестов для сообщения (тапа, длинного нажатия)
    var areInteractionGesturesDisabled: Bool = false
    
    /// Является ли последним сообщением в диалоге
    var isLastMessage: Bool = false
    
    /// Данные о форматировании текста сообщения (если в нем есть теги форматирования)
    var textFormatting: ChatTextFormatting? = nil
    
    /// Инлайн клавиатура
    var inlineKeyboard = InlineKeyboardCellObject()
    
    /// Внешняя инлайн клавиатура
    var inlineOutsideKeyboard = InlineKeyboardCellObject()
    
    /// Клавиатура опций взаимодействия с сообщением
    var optionsKeyboard = OptionsKeyboardCellObject()
    
    init(messageUid: String, messageModel: MessageModel, text: String) {
        self.messageUid = messageUid
        super.init(messageModel: messageModel, text: text)
    }
    
    /// Обновить статус сообщения
    ///
    /// - Parameter status: новый статус сообщения
    func setStatus(status: MessageStatus) {
        _messageModel.status = status
    }
    
    /// Создает копию cell object
    ///
    /// - Returns: копия
    func copy() -> BaseMessageCellObjectProtocol {
        let copy = ChatTextMessageCellObject(messageUid: messageUid, messageModel: _messageModel, text: text)
        copy.isLastMessage = isLastMessage
        copy.textFormatting = textFormatting
        copy.inlineKeyboard = inlineKeyboard
        copy.inlineOutsideKeyboard = inlineOutsideKeyboard
        copy.optionsKeyboard = optionsKeyboard
        return copy
    }
}

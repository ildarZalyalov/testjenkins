//
//  ChatTextMessageHandler.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 28.04.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import ChattoAdditions

/// Обработчик действий пользователя для текстовых сообщений
class ChatTextMessageHandler: BaseInteractionHandlerProtocol {
    
    weak var baseHandler: BaseChatMessageHandler?
    
    init(baseHandler: BaseChatMessageHandler) {
        self.baseHandler = baseHandler
    }
    
    func userDidTapOnFailIcon(viewModel: ChatTextMessageViewModel, failIconView: UIView) {
        baseHandler?.userDidTapOnFailIcon(viewModel: viewModel, failIconView: failIconView)
    }
    
    func userDidTapOnAvatar(viewModel: ChatTextMessageViewModel) {
        baseHandler?.userDidTapOnAvatar(viewModel: viewModel)
    }
    
    func userDidTapOnBubble(viewModel: ChatTextMessageViewModel) {
        baseHandler?.userDidTapOnBubble(viewModel: viewModel)
    }
    
    func userDidBeginLongPressOnBubble(viewModel: ChatTextMessageViewModel) {
        baseHandler?.userDidBeginLongPressOnBubble(viewModel: viewModel)
    }
    
    func userDidEndLongPressOnBubble(viewModel: ChatTextMessageViewModel) {
        baseHandler?.userDidEndLongPressOnBubble(viewModel: viewModel)
    }
    
    func userDidSelectMessage(viewModel: ChatTextMessageViewModel) {
        baseHandler?.userDidSelectMessage(viewModel: viewModel)
    }
    
    func userDidDeselectMessage(viewModel: ChatTextMessageViewModel) {
        baseHandler?.userDidDeselectMessage(viewModel: viewModel)
    }
    
    func userDidPressInlineKeyboardButton(viewModel: BaseMessageViewModelProtocol, with buttonId: String) {
        baseHandler?.userDidPressInlineKeyboardButton(viewModel: viewModel, with: buttonId)
    }
    
    func userDidPressExpandInlineKeyboardButton(viewModel: BaseMessageViewModelProtocol) {
        baseHandler?.userDidPressExpandInlineKeyboardButton(viewModel: viewModel)
    }
    
    func userDidPressOptionButton(viewModel: BaseMessageViewModelProtocol, with buttonId: String) {
        baseHandler?.userDidPressOptionButton(viewModel: viewModel, with: buttonId)
    }
    
    func userDidPressToOpen(url: URL, inMessage viewModel: BaseMessageViewModelProtocol) {
        baseHandler?.userDidPressToOpen(url: url, inMessage: viewModel)
    }
}

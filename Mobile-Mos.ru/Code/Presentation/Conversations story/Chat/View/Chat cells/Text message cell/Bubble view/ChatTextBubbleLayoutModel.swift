//
//  ChatTextBubbleLayoutModel.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 25.05.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import Chatto

/// Модель лейаута для ChatTextBubbleView. Умеет расчитывать размер ChatTextBubbleView при заданных параметрах, а также кеширует его
/// Смотри приватный оригинал TextBubbleLayoutModel в ChattoAdditions/TextBubbleView.swift
class ChatTextBubbleLayoutModel: BaseBubbleLayoutModel {
    
    /// Содержит входные данные для расчета, а также служит ключем хэширования
    class ChatTextLayoutContext: BaseBubbleLayoutModel.LayoutContext {
        
        struct ChatTextLayoutParameters {
            
            /// Форматированный текст сообщения
            let text: NSAttributedString
            /// Шрифт по умолчанию для текста сообщения
            let font: UIFont
            /// Стиль параграфа по умолчанию для текста сообщения
            let paragraphStyle: NSParagraphStyle
            /// Инсеты для текста сообщения
            let textInsets: UIEdgeInsets
            /// Предпочитаемая максимальная ширина для сообщения
            let preferredMaxLayoutWidth: CGFloat
        }
        
        let chatTextLayoutParameters: ChatTextLayoutParameters
        
        override var bubbleLayoutType: BaseBubbleLayoutModel.Type {
            return ChatTextBubbleLayoutModel.self
        }
        
        init(parameters: LayoutParameters,
             textParameters: ChatTextLayoutParameters) {
            self.chatTextLayoutParameters = textParameters
            super.init(parameters: parameters)
        }
        
        override var hashValue: Int {
            
            let params = chatTextLayoutParameters
            let hashes = [super.hashValue,
                          params.text.hashValue,
                          params.font.hashValue,
                          params.paragraphStyle.hashValue,
                          params.textInsets.combinedHashValue,
                          params.preferredMaxLayoutWidth.hashValue]
            
            return hashes.combinedHashValue
        }
        
        static func == (lhs: ChatTextLayoutContext, rhs: ChatTextLayoutContext) -> Bool {
            
            if lhs === rhs { return true }
            
            typealias BaseContext = BaseBubbleLayoutModel.LayoutContext
            let baseEqual = lhs as BaseContext == rhs as BaseContext
            
            let lhsParams = lhs.chatTextLayoutParameters
            let rhsParams = rhs.chatTextLayoutParameters
            
            let lhsValues = (lhsParams.text, lhsParams.font, lhsParams.paragraphStyle, lhsParams.textInsets, lhsParams.preferredMaxLayoutWidth)
            let rhsValues = (rhsParams.text, rhsParams.font, rhsParams.paragraphStyle, rhsParams.textInsets, rhsParams.preferredMaxLayoutWidth)
            
            return lhsValues == rhsValues && baseEqual
        }
    }
    
    /// Фрейм текста
    var textFrame: CGRect = CGRect.zero
    
    /// Размер текста
    var textSize: CGSize = CGSize.zero
    
    /// Посчитать размер контента баббла
    override func calculateIntrinsicContentSize() -> CGSize {
        
        var contentSize = super.calculateIntrinsicContentSize()
        
        guard let textLayoutContext = layoutContext as? ChatTextLayoutContext else { return contentSize }
        
        let layoutParameters = textLayoutContext.chatTextLayoutParameters
        let textHorizontalInset = layoutParameters.textInsets.bma_horziontalInset
        let maxTextWidth = layoutParameters.preferredMaxLayoutWidth - textHorizontalInset
        
        if layoutParameters.text.string.isEmpty {
            textSize = CGSize.zero
        }
        else {
            textSize = textSizeThatFitsWidth(maxTextWidth, with: textLayoutContext)
            textSize = textSize.bma_outsetBy(dx: textHorizontalInset, dy: layoutParameters.textInsets.bma_verticalInset)
        }
        
        contentSize.width = max(contentSize.width, textSize.width)
        contentSize.height += textSize.height
        
        return contentSize
    }
        
    /// Пересчитать кеш лейаута контента баббла
    override func calculateContentLayout(for contentSize: CGSize) {
        super.calculateContentLayout(for: contentSize)
        textFrame = CGRect(origin: CGPoint.zero, size: textSize)
    }
    
    private func textSizeThatFitsWidth(_ width: CGFloat, with textLayoutContext: ChatTextLayoutContext) -> CGSize {
        
        let textContainer: NSTextContainer = {
            let size = CGSize(width: width, height: .greatestFiniteMagnitude)
            let container = NSTextContainer(size: size)
            container.lineFragmentPadding = 0
            return container
        }()
        
        let textStorage = replicateUITextViewNSTextStorage(with: textLayoutContext)
        
        let layoutManager: NSLayoutManager = {
            let layoutManager = NSLayoutManager()
            layoutManager.addTextContainer(textContainer)
            textStorage.addLayoutManager(layoutManager)
            return layoutManager
        }()
        
        let rect = layoutManager.usedRect(for: textContainer)
        return rect.size.bma_round()
    }
    
    private func replicateUITextViewNSTextStorage(with textLayoutContext: ChatTextLayoutContext) -> NSTextStorage {
        
        let layoutParameters = textLayoutContext.chatTextLayoutParameters
        
        // устанавливаем стиль по умолчанию, чтобы части текста без форматирования имели шрифт и отступы
        // по поводу NSOriginalFont смотри https://github.com/badoo/Chatto/issues/129
        let storage = NSTextStorage(string: layoutParameters.text.string, attributes: [
            NSAttributedStringKey.font: layoutParameters.font,
            NSAttributedStringKey(rawValue: "NSOriginalFont"): layoutParameters.font,
            NSAttributedStringKey.paragraphStyle: layoutParameters.paragraphStyle
            ])
        
        // добавляем форматирование в нужных фрагментах текста
        let allTextRange = NSMakeRange(0, layoutParameters.text.length)
        layoutParameters.text.enumerateAttributes(in: allTextRange, options: []) { (attributes, range, _) in
            storage.addAttributes(attributes, range: range)
        }
        
        return storage
    }
}

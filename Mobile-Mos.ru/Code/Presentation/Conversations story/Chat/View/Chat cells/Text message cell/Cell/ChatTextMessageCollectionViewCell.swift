//
//  ChatTextMessageCollectionViewCell.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 29.05.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import ChattoAdditions

class ChatTextMessageCollectionViewCell: BaseMessageCollectionViewCell<ChatTextBubbleView>, BaseBubbleViewDelegate, ChatTextBubbleViewDelegate, FixedLayoutHeightSpecificable {
    
    /// Множитель ускорения анимации скрытия поций взаимодействия с сообщением
    let optionButtonsHideAnimationMultiplier: CGFloat = 1.5
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    fileprivate func commonInit() {
        tapGestureRecognizer.delegate = self
        contentView.clipsToBounds = true
    }
    
    class func createSizingCell() -> ChatTextMessageCollectionViewCell {
        let cell = ChatTextMessageCollectionViewCell(frame: CGRect.zero)
        cell.viewContext = .sizing
        return cell
    }
    
    var onInlineKeyboardButtonPress: ((_ cell: ChatTextMessageCollectionViewCell, _ buttonId: String) -> Void)?
    var onExpandInlineKeyboardButtonPress: ((_ cell: ChatTextMessageCollectionViewCell) -> Void)?
    var onOptionButtonPress: ((_ cell: ChatTextMessageCollectionViewCell, _ buttonId: String) -> Void)?
    var onUrlInMessageTextOpen: ((_ cell: ChatTextMessageCollectionViewCell, _ url: URL) -> Void)?
    
    //MARK: - Property forwarding
    
    var fixedLayoutHeight: CGFloat? {
        didSet {
            bubbleView.fixedLayoutHeight = fixedLayoutHeight
        }
    }
    
    override var viewContext: ViewContext {
        didSet {
            bubbleView.viewContext = viewContext
        }
    }
    
    override var isSelected: Bool {
        didSet {
            bubbleView.isSelected = isSelected
        }
    }
    
    var textMessageViewModel: ChatTextMessageViewModelProtocol? {
        didSet {
            messageViewModel = textMessageViewModel
            bubbleView.messageViewModel = textMessageViewModel
        }
    }
    
    var textMessageStyle: ChatTextMessageCollectionViewCellStyleProtocol? {
        didSet {
            bubbleView.style = textMessageStyle
        }
    }
    
    var layoutCache: NSCache<AnyObject, AnyObject>? {
        didSet {
            bubbleView.layoutCache = layoutCache
        }
    }
    
    //MARK: Создаем баббл view
    
    override func createBubbleView() -> ChatTextBubbleView {
        let view = ChatTextBubbleView()
        view.delegate = self
        view.textBubbleDelegate = self
        return view
    }
    
    override func performBatchUpdates(_ updateClosure: @escaping () -> Void, animated: Bool, completion: (() -> Void)?) {
        super.performBatchUpdates({ () -> Void in
            self.bubbleView.performBatchUpdates(updateClosure, animated: false, completion: nil)
        }, animated: animated, completion: completion)
    }
    
    override func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        return self.bubbleView.messageBubbleContains(point: touch.location(in: bubbleView))
    }
    
    override func revealAccessoryView(withOffset offset: CGFloat, animated: Bool) {
        
        super.revealAccessoryView(withOffset: offset, animated: animated)
        
        guard let preferredOffset = preferredOffsetToRevealAccessoryView() else { return }
        
        var optionsAlphaValue: CGFloat = offset * optionButtonsHideAnimationMultiplier / preferredOffset
        optionsAlphaValue = 1 - optionsAlphaValue
    
        if animated {
            UIView.animate(withDuration: animationDuration, animations: {
                self.bubbleView.setOptionButtonsAlpha(alpha: optionsAlphaValue)
            })
        }
        else {
            bubbleView.setOptionButtonsAlpha(alpha: optionsAlphaValue)
        }
    }
    
    //MARK: - BaseBubbleViewDelegate
    
    func didPressInlineButton(with itemId: String) {
        onInlineKeyboardButtonPress?(self, itemId)
    }
    
    func didPressExpandInlineButtons() {
        onExpandInlineKeyboardButtonPress?(self)
    }
    
    func didPressOptionButton(with itemId: String) {
        onOptionButtonPress?(self, itemId)
    }
    
    //MARK: - ChatTextBubbleViewDelegate
    
    func didPressToOpen(url: URL) {
        onUrlInMessageTextOpen?(self, url)
    }
}

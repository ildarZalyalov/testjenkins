//
//  BaseChatMessageHandler.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 28.04.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import ChattoAdditions

/// Базовый обработчик действий пользователя для всех типов ячеек
protocol BaseChatMessageHandler: class {
    
    /// Пользователь нажал на иконку ошибки отправки сообщения
    ///
    /// - Parameters:
    ///   - viewModel: view model сообщения
    ///   - failIconView: view с иконкой ошибки отправки
    func userDidTapOnFailIcon(viewModel: BaseMessageViewModelProtocol, failIconView: UIView)
    
    /// Пользователь нажал на аватар
    ///
    /// - Parameter viewModel: view model сообщения
    func userDidTapOnAvatar(viewModel: BaseMessageViewModelProtocol)
    
    /// Пользователь нажал на балун с сообщением
    ///
    /// - Parameter viewModel: view model сообщения
    func userDidTapOnBubble(viewModel: BaseMessageViewModelProtocol)
    
    /// Пользователь начал долгий тап по балуну с сообщением
    ///
    /// - Parameter viewModel: view model сообщения
    func userDidBeginLongPressOnBubble(viewModel: BaseMessageViewModelProtocol)
    
    /// Пользователь закончил долгий тап по балуну с сообщением
    ///
    /// - Parameter viewModel: view model сообщения
    func userDidEndLongPressOnBubble(viewModel: BaseMessageViewModelProtocol)
    
    /// Пользователь выбрал сообщение
    ///
    /// - Parameter viewModel: view model сообщения
    func userDidSelectMessage(viewModel: BaseMessageViewModelProtocol)
    
    /// Пользователь убрал выделение с сообщения
    ///
    /// - Parameter viewModel: view model сообщения
    func userDidDeselectMessage(viewModel: BaseMessageViewModelProtocol)
    
    /// Пользователь нажал на кнопку инлайн клавиатуры
    ///
    /// - Parameters:
    ///   - viewModel: view model сообщения
    ///   - buttonId: идентификатор кнопки
    func userDidPressInlineKeyboardButton(viewModel: BaseMessageViewModelProtocol, with buttonId: String)
    
    /// Пользователь нажал на кнопку "расхлопывания" инлайн клавиатуры
    ///
    /// - Parameter viewModel: view model сообщения
    func userDidPressExpandInlineKeyboardButton(viewModel: BaseMessageViewModelProtocol)
    
    /// Пользователь нажал на веб-ссылку в тексте сообщения, чтобы открыть ее
    ///
    /// - Parameters:
    ///   - url: веб-ссылка
    ///   - inMessage: view model сообщения
    func userDidPressToOpen(url: URL, inMessage viewModel: BaseMessageViewModelProtocol)
    
    /// Пользователь нажал на кнопку клавиатуры опций взаимодействия с сообщением
    ///
    /// - Parameters:
    ///   - viewModel: view model сообщения
    ///   - buttonId: идентификатор кнопки
    func userDidPressOptionButton(viewModel: BaseMessageViewModelProtocol, with buttonId: String)
    
    /// Пользователь нажал на иконку статуса загрузки документа в сообщении
    ///
    /// - Parameter viewModel: view model сообщения
    func userDidPressDocumentIcon(viewModel: ChatDocumentMessageViewModelProtocol)
    
    /// Пользователь нажал на фото в сообщении
    ///
    /// - Parameter viewModel: view model сообщения
    func userDidPressOnPhoto(viewModel: ChatPhotoMessageViewModelProtocol)
}

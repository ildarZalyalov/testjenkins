//
//  MessageStatusCell.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 03.07.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import UIKit

class MessageStatusCell: UICollectionViewCell {

    @IBOutlet weak var statusLabel: UILabel!

    var attributedText: NSAttributedString? {
        didSet {
            statusLabel.attributedText = attributedText
        }
    }
}

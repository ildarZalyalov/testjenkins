//
//  MessageStatusPresenter.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 03.07.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import UIKit
import Chatto
import ChattoAdditions

class MessageStatusPresenter: ChatItemPresenterProtocol {

    let statusModel: MessageStatusCellObject
    let stringBuilder: ChatMessageStringBuilder
    
    let fontSize: CGFloat = 11
    
    init(statusModel: MessageStatusCellObject, stringBuilder: ChatMessageStringBuilder) {
        self.statusModel = statusModel
        self.stringBuilder = stringBuilder
    }
    
    fileprivate static let cellReuseIdentifier = String(describing: MessageStatusCell.self)
    
    //MARK: - ChatItemPresenterProtocol
    
    var canCalculateHeightInBackground: Bool {
        return true
    }
    
    static func registerCells(_ collectionView: UICollectionView) {
        collectionView.register(UINib(nibName: String(describing: MessageStatusCell.self), bundle: nil), forCellWithReuseIdentifier: cellReuseIdentifier)
    }

    func dequeueCell(collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell {
        return collectionView.dequeueReusableCell(withReuseIdentifier: MessageStatusPresenter.cellReuseIdentifier, for: indexPath)
    }

    func configureCell(_ cell: UICollectionViewCell, decorationAttributes: ChatItemDecorationAttributesProtocol?) {
        
        guard let statusCell = cell as? MessageStatusCell else {
            return
        }
        
        guard let statusText = stringBuilder.statusString(for: statusModel.status) else {
            return
        }
        
        let font = UIFont(customName: .graphikLCGMedium, size: fontSize) ?? UIFont.systemFont(ofSize: fontSize)
        let color = statusModel.status == .failed ? UIColorPalette.chatMessageSendingFailedStatusColor : UIColorPalette.chatMessageOtherStatusColor

        let attributes = [
            NSAttributedStringKey.font : font,
            NSAttributedStringKey.foregroundColor: color
        ]
        
        statusCell.attributedText = NSAttributedString(string: statusText ,attributes: attributes)
    }

    func heightForCell(maximumWidth width: CGFloat, decorationAttributes: ChatItemDecorationAttributesProtocol?) -> CGFloat {
        return 16
    }
}

class MessageStatusPresenterBuilder: ChatItemPresenterBuilderProtocol {
    
    let stringBuilder: ChatMessageStringBuilder
    
    init(stringBuilder: ChatMessageStringBuilder) {
        self.stringBuilder = stringBuilder
    }
    
    public func canHandleChatItem(_ chatItem: ChatItemProtocol) -> Bool {
        return chatItem is MessageStatusCellObject
    }
    
    public func createPresenterWithChatItem(_ chatItem: ChatItemProtocol) -> ChatItemPresenterProtocol {
        precondition(canHandleChatItem(chatItem), "Попытка создать MessageStatusPresenter для \(type(of: chatItem))")
        return MessageStatusPresenter(statusModel: chatItem as! MessageStatusCellObject, stringBuilder: stringBuilder)
    }
    
    public var presenterType: ChatItemPresenterProtocol.Type {
        return MessageStatusPresenter.self
    }
}

//
//  MessageStatusCellObject.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 01.08.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import Chatto
import ChattoAdditions

class MessageStatusCellObject: ChatItemProtocol {
    
    let uid: String = UUID().uuidString
    let type: ChatItemType = MessageStatusCellObject.chatItemType
    let status: MessageStatus
    
    static var chatItemType: ChatItemType {
        return String(describing: MessageStatusCellObject.self)
    }
    
    init(with status: MessageStatus) {
        self.status = status
    }
}

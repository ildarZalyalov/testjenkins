//
//  BaseBubbleView.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 30.05.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import ChattoAdditions

/// Делегат BaseBubbleView
protocol BaseBubbleViewDelegate: class {
    
    /// Была нажата инлайн кнопка
    ///
    /// - Parameter itemId: идентификатор кнопки
    func didPressInlineButton(with itemId: String)
    
    /// Была нажата кнопка "расхлопывания" инлайн кнопок
    func didPressExpandInlineButtons()
    
    /// Была нажата кнопка опции взаимодействия с сообщением
    ///
    /// - Parameter itemId: идентификатор кнопки
    func didPressOptionButton(with itemId: String)
}

/// Базовая view для баббла (абстрактный класс).
/// Смотри оригинал TextBubbleView в ChattoAdditions/TextBubbleView.swift
class BaseBubbleView: UIView, MaximumLayoutWidthSpecificable, BackgroundSizingQueryable, FixedLayoutHeightSpecificable {
    
    // MaximumLayoutWidthSpecificable
    var preferredMaxLayoutWidth: CGFloat = 0
    
    // BackgroundSizingQueryable
    let canCalculateSizeInBackground = true
    
    // FixedLayoutHeightSpecificable
    var fixedLayoutHeight: CGFloat?
    
    /// Длительность анимации обновления
    let animationDuration: TimeInterval = 0.33
    
    /// Проигрывается ли в данный момент анимация обновления содержимого
    private(set) var isAnimatingUpdate = false
    
    /// Контекст использования, позволяет задать используется ли view для вычисления размера или для отображения
    var viewContext: ViewContext = .normal
    
    /// Выбрана ли view в данный момент
    var isSelected = false {
        didSet {
            if isSelected != oldValue {
                updateViews()
            }
        }
    }
    
    /// Кеш для лейаута, если задан
    var layoutCache: NSCache<AnyObject, AnyObject>?
    
    /// Стиль view, определеяет параметры отображения (если не задан, вью не отображается)
    var style: BaseBubbleViewStyleProtocol? {
        didSet {
            updateViews()
        }
    }
    
    /// View model сообщения (если не задана, вью не отображается)
    var messageViewModel: BaseMessageViewModelProtocol? {
        didSet {
            updateViews()
        }
    }
    
    /// Делегат
    weak var delegate: BaseBubbleViewDelegate?
    
    /// Вьюха с картинкой баббла
    private var bubbleImageView: UIImageView = UIImageView()
    
    /// Вьюха с картинкой границы баббла
    private var borderImageView: UIImageView = UIImageView()
    
    /// Вьюха, содержащая контент баббла
    private let contentParentView: UIView = {
        let view = UIView()
        view.clipsToBounds = true
        view.backgroundColor = UIColor.clear
        return view
    }()
    
    /// Вьюха-бекграунд инлайн клавиатуры, содержащая кнопки инлайн клавиатуры; ее цвет определяет цвет сепараторов
    private let buttonControlsBackgroundView: BezierPathMaskedView = {
        let view = BezierPathMaskedView()
        view.clipsToBounds = true
        return view
    }()
    
    /// Угол скругления внизу для инлайн клавиатуры
    private var buttonControlsBottomCornerRadius: CGFloat = 0
    
    /// Кнопка "расхлопывания" дополнительных кнопок
    private lazy var expandButton: ChatMessageExpandInlineKeyboardButton = {
        
        let borderWidth = BaseBubbleLayoutModel.inlineButtonColumnSpace
        let button = ChatMessageExpandInlineKeyboardButton(withBorderWidth: borderWidth)
        
        button.addTarget(self, action: #selector(self.expandButtonPressed(button:)), for: .touchUpInside)
        self.buttonControlsBackgroundView.addSubview(button)
        
        return button
    }()
    
    /// View model кнопки "расхлопывания" дополнительных кнопок
    private lazy var expandButtonViewModel: InlineKeyboardButtonViewModel = {
        return InlineKeyboardButtonViewModel(itemId: UUID().uuidString, title: String(), isEnabled: true)
    }()
    
    private let buttonControlsCache = KeyboardButtonControlCache()
    private var buttonControls = [KeyboardButtonControl]()
    
    private let optionButtonsCache = KeyboardButtonControlCache()
    private var optionButtons = [KeyboardButtonControl]()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        
        addSubview(bubbleImageView)
        addSubview(contentParentView)
        addSubview(borderImageView)
        
        contentParentView.addSubview(buttonControlsBackgroundView)
        buttonControlsCache.onNewCachedItemHandler = { [unowned self] button in
            
            button.contentHorizontalAlignment = .left
            button.contentVerticalAlignment = .fill
            button.contentEdgeInsets = UIEdgeInsets(top: 0, left: 12, bottom: 0, right: 12)
            button.titleLabel?.numberOfLines = 0
            
            button.addTarget(self, action: #selector(self.buttonPressed(button:)), for: .touchUpInside)
            self.buttonControlsBackgroundView.addSubview(button)
        }
        
        optionButtonsCache.onNewCachedItemHandler = { [unowned self] button in
            button.addTarget(self, action: #selector(self.optionButtonPressed(button:)), for: .touchUpInside)
            self.addSubview(button)
        }
        
        initializeContentSubviews(contentParentView: contentParentView)
    }
    
    @objc
    private func buttonPressed(button: UIButton) {
        if let keyboardControl = button as? KeyboardButtonControl, keyboardControl.buttonId != nil {
            delegate?.didPressInlineButton(with: keyboardControl.buttonId!)
        }
    }
    
    @objc
    private func optionButtonPressed(button: UIButton) {
        if let keyboardControl = button as? KeyboardButtonControl, keyboardControl.buttonId != nil {
            delegate?.didPressOptionButton(with: keyboardControl.buttonId!)
        }
    }
    
    @objc
    private func expandButtonPressed(button: UIButton) {
        delegate?.didPressExpandInlineButtons()
    }
    
    func performBatchUpdates(_ updateClosure: @escaping () -> Void, animated: Bool, completion: (() -> Void)?) {
        
        isAnimatingUpdate = true
        
        let updateAndRefreshViews = {
            
            updateClosure()
            self.isAnimatingUpdate = false
            self.updateViews()
            
            if animated {
                self.layoutIfNeeded()
            }
        }
        
        if animated {
            UIView.animate(withDuration: animationDuration, animations: updateAndRefreshViews, completion: { _ in
                completion?()
            })
        } else {
            updateAndRefreshViews()
        }
    }
    
    private func updateViews() {
        
        guard viewContext != .sizing else { return }
        guard !isAnimatingUpdate else { return }
        guard let style = style, let viewModel = messageViewModel else { return }
        
        let bubbleImage = style.bubbleImage(viewModel: viewModel, isSelected: isSelected)
        let borderImage = style.bubbleImageBorder(viewModel: viewModel, isSelected: isSelected)
        let contentCornerRadius = style.contentCornerRadius(viewModel: viewModel, isSelected: isSelected)
        
        if bubbleImageView.image != bubbleImage {
            bubbleImageView.image = bubbleImage
        }
        if borderImageView.image != borderImage {
            borderImageView.image = borderImage
        }
        if contentParentView.layer.cornerRadius != contentCornerRadius {
            contentParentView.layer.cornerRadius = contentCornerRadius
        }
        
        updateContentViews()
        updateInlineButtons(with: contentCornerRadius)
        updateOptionButtons()
    }
    
    /// Обновить инлайн клавиатуру
    ///
    /// - Parameter bottomCornerRadius: новый радиус скругления кнопок снизу (если есть)
    private func updateInlineButtons(with bottomCornerRadius: CGFloat) {
        
        guard let style = style, let viewModel = messageViewModel else { return }
        guard viewModel.inlineButtons.count > 0 else { return }
        
        //стараемся обойтись перечислениями т.к. они быстрые (метод вызывается часто)
        
        var count = 0
        viewModel.inlineButtons.forEach { count += $0.count }
        
        if count != buttonControls.count {
            buttonControls = buttonControlsCache.obtain(count: count)
        }
        
        let parentViewHidden = count == 0
        buttonControlsBackgroundView.isHidden = parentViewHidden
        
        guard !parentViewHidden else { return }
        
        let backgroundViewColor = style.inlineButtonSeparatorColor(viewModel: viewModel, isSelected: isSelected)
        
        if buttonControlsBackgroundView.backgroundColor != backgroundViewColor {
            buttonControlsBackgroundView.backgroundColor = backgroundViewColor
        }
        
        updateInlineButtonsKeyboardMask(with: bottomCornerRadius)
        
        var buttonIndex = 0
        
        for row in viewModel.inlineButtons {
            for model in row {
                update(button: &buttonControls[buttonIndex], with: model)
                buttonIndex += 1
            }
        }
        
        let shouldHideExpandButton = !viewModel.isShowingExpandButton
        expandButton.isHidden = shouldHideExpandButton
        
        guard viewModel.isShowingExpandButton else { return }
        
        updateExpandButton()
    }
    
    /// Обновить маску инлайн клавиатуры
    ///
    /// - Parameter cornerRadius: новый радиус скругления кнопок снизу
    private func updateInlineButtonsKeyboardMask(with bottomCornerRadius: CGFloat) {
        
        guard bottomCornerRadius != buttonControlsBottomCornerRadius else { return }
        
        buttonControlsBottomCornerRadius = bottomCornerRadius
        buttonControlsBackgroundView.maskingPathCalculator = { frame in
            
            let path = UIBezierPath()
            path.move(to: frame.origin)
            
            path.addLine(to: CGPoint(x: frame.maxX, y: frame.origin.y))
            path.addLine(to: CGPoint(x: frame.maxX, y: frame.maxY - bottomCornerRadius))
            
            path.addArc(withCenter: CGPoint(x: frame.maxX - bottomCornerRadius, y: frame.maxY - bottomCornerRadius), radius: bottomCornerRadius, startAngle: 0, endAngle: CGFloat.pi / 2, clockwise: true)
            
            path.addLine(to: CGPoint(x: frame.minX + bottomCornerRadius, y: frame.maxY))
            
            path.addArc(withCenter: CGPoint(x: frame.minX + bottomCornerRadius, y: frame.maxY - bottomCornerRadius), radius: bottomCornerRadius, startAngle: CGFloat.pi / 2, endAngle: CGFloat.pi, clockwise: true)
            
            path.close()
            
            return path
        }
    }
    
    /// Обновить кнопку инлайн клавиатуры
    ///
    /// - Parameters:
    ///   - button: кнопка инлайн клавиатуры
    ///   - model: view model кнопки инлайн клавиатуры
    private func update(button: inout KeyboardButtonControl, with model: InlineKeyboardButtonViewModel) {
        
        guard let style = style else { return }
        
        let font = style.inlineButtonTextFont(viewModel: model, isSelected: isSelected)
        let backgroundColor = style.inlineButtonBackgroundColor(viewModel: model, isSelected: isSelected)
        let titleColor = style.inlineButtonTextColor(viewModel: model, isSelected: isSelected)
        
        button.buttonId = model.itemId
        button.isEnabled = model.isEnabled
        
        button.normalTintColor = backgroundColor
        button.highlightedTintColor = titleColor
        
        if button.backgroundColor != backgroundColor {
            button.backgroundColor = backgroundColor
        }
        
        button.isHighlightedChanged = { highlightedButton, isHighlighted in
            highlightedButton.backgroundColor = isHighlighted ? highlightedButton.highlightedTintColor : highlightedButton.normalTintColor
        }
        
        if button.titleLabel?.font != font {
            button.titleLabel?.font = font
        }
        
        if button.title(for: .normal) != model.title {
            button.setTitle(model.title, for: .normal)
        }
        if button.titleColor(for: .normal) != titleColor {
            button.setTitleColor(titleColor, for: .normal)
        }
        if button.titleColor(for: .highlighted) != backgroundColor {
            button.setTitleColor(backgroundColor, for: .highlighted)
        }
    }
    
    /// Обновить кнопку "расхлопывания" инлайн клавиатуры
    private func updateExpandButton() {
        
        guard let style = style, let viewModel = messageViewModel else { return }
        
        expandButtonViewModel.isEnabled = viewModel.inlineButtons.last?.last?.isEnabled ?? true
        
        let backgroundColor = style.inlineButtonBackgroundColor(viewModel: expandButtonViewModel, isSelected: isSelected)
        let titleColor = style.inlineButtonTextColor(viewModel: expandButtonViewModel, isSelected: isSelected)
        
        expandButton.update(withIconColor: titleColor, andBackgroundColor: backgroundColor)
    }
    
    /// Обновить клавиатуру опций взаимодействия с сообщением
    private func updateOptionButtons() {
        
        guard let style = style, let viewModel = messageViewModel else { return }
        guard viewModel.optionButtons.count > 0 else {
            optionButtons.forEach { $0.isHidden = true }
            return
        }
        
        //стараемся обойтись перечислениями т.к. они быстрые (метод вызывается часто)
        
        let count = viewModel.optionButtons.count
        
        if count != optionButtons.count {
            optionButtons = optionButtonsCache.obtain(count: count)
        }

        let iconColor = style.optionButtonIconColor(viewModel: viewModel, isSelected: isSelected)
        let openWeblinkIconColor = style.optionButtonOpenWeblinkIconColor(viewModel: viewModel, isSelected: isSelected)
        let iconHighlightedColor = style.optionButtonIconHighlightedColor(viewModel: viewModel, isSelected: isSelected)
        
        for (index, buttonModel) in viewModel.optionButtons.enumerated() {
            
            let button = optionButtons[index]
            
            button.buttonId = buttonModel.itemId
            button.normalTintColor = buttonModel.type == .openWeblink ? openWeblinkIconColor : iconColor
            button.highlightedTintColor = iconHighlightedColor
            button.setImage(iconForOptionButton(with: buttonModel.type), for: .normal)
            button.isHidden = false
        }
    }
    
    /// Получить иконку для кнопки опции взаимодействия с сообщением
    ///
    /// - Parameter type: тип кнопки
    /// - Returns: иконка
    private func iconForOptionButton(with type: OptionsKeyboardButtonType) -> UIImage {
        switch type {
        case .share:
            return #imageLiteral(resourceName: "shareButtonIcon")
        case .openWeblink:
            return #imageLiteral(resourceName: "messageOpenWeblinkIcon")
        }
    }
    
    override func sizeThatFits(_ size: CGSize) -> CGSize {
        return calculateBubbleLayout(preferredMaxLayoutWidth: size.width)?.size ?? CGSize.zero
    }
    
    override func layoutSubviews() {
        
        super.layoutSubviews()
        
        guard let layout = calculateBubbleLayout(preferredMaxLayoutWidth: preferredMaxLayoutWidth) else { return }
        
        bubbleImageView.bma_rect = layout.bubbleFrame
        borderImageView.bma_rect = layout.bubbleFrame
        contentParentView.bma_rect = layout.contentParentFrame
        buttonControlsBackgroundView.frame = layout.inlineButtonBackgroundFrame
        
        let minCount = min(buttonControls.count, layout.inlineButtonFrames.count)
        for index in 0 ..< minCount {
            buttonControls[index].bma_rect = layout.inlineButtonFrames[index]
        }
        
        if !expandButton.isHidden, let lastFrame = layout.inlineButtonFrames.last {
            expandButton.bma_rect = lastFrame
        }
        
        let minOptionCount = min(optionButtons.count, layout.optionButtonFrames.count)
        for index in 0 ..< minOptionCount {
            optionButtons[index].bma_rect = layout.optionButtonFrames[index]
        }
    }
    
    func calculateBubbleLayout(preferredMaxLayoutWidth: CGFloat) -> BaseBubbleLayoutModel? {
        
        guard let layoutContext = createLayoutModelContext(preferredMaxLayoutWidth: preferredMaxLayoutWidth) else { return nil }
        
        let layoutKey = layoutContext.hashValue as AnyObject
        if let layoutModel = layoutCache?.object(forKey: layoutKey) as? BaseBubbleLayoutModel, layoutModel.layoutContext == layoutContext {
            return layoutModel
        }
        
        let layoutModel = layoutContext.bubbleLayoutType.init(layoutContext: layoutContext)
        
        layoutModel.calculateLayout()
        layoutCache?.setObject(layoutModel, forKey: layoutKey)
        
        return layoutModel
    }
    
    /// Находится ли точка внутри баббла с сообщением
    func messageBubbleContains(point: CGPoint) -> Bool {
        return borderImageView.frame.contains(point)
    }
    
    /// Установить прозрачность кнопкам опций взаимодействия с сообщением
    ///
    /// - Parameter alpha: прозрачность
    func setOptionButtonsAlpha(alpha: CGFloat) {
        let alphaValue = alpha.clamped(to: 0 ... 1)
        optionButtons.forEach { $0.alpha = alphaValue }
    }
    
    //MARK: - Переопределить в наследниках
    
    /// Инициализация views для содержимого. Вызывается при создании view баббла. Можно переопределить, чтобы добавить subviews.
    func initializeContentSubviews(contentParentView: UIView) {}
    
    /// Обновить содержимое т.к. изменились параметры или модель
    func updateContentViews() {
        fatalError("Необходимо реализовать метод updateContentViews() в наследнике!")
    }
    
    /// Попытаться создать контекст лейаута содержимого. Если вернуть nil, то подсчет лейаута не будет произведен.
    ///
    /// - Parameter preferredMaxLayoutWidth: предпочитаемая ширина баббла
    func createLayoutModelContext(preferredMaxLayoutWidth: CGFloat) -> BaseBubbleLayoutModel.LayoutContext? {
        fatalError("Необходимо реализовать метод createLayoutModelContext() в наследнике!")
    }
}

//
//  BaseMessageCellObjectProtocol.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 28.06.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import ChattoAdditions

/// Cell object для базовой ячейки сообщения
protocol BaseMessageCellObjectProtocol: MessageModelProtocol {
    
    /// Идентификатор сообщения, для которого был создан cell object
    var messageUid: String { get }
    
    /// Отключена ли обработка жестов для сообщения (тапа, длинного нажатия)
    var areInteractionGesturesDisabled: Bool { get }
    
    /// Текст в сообщении
    var text: String { get }
    
    /// Является ли последним сообщением в диалоге
    var isLastMessage: Bool { get set }
    
    /// Данные о форматировании текста сообщения (если в нем есть теги форматирования)
    var textFormatting: ChatTextFormatting? { get set }
    
    /// Инлайн клавиатура
    var inlineKeyboard: InlineKeyboardCellObject { get set }
    
    /// Внешняя инлайн клавиатура
    var inlineOutsideKeyboard: InlineKeyboardCellObject { get set }
    
    /// Клавиатура опций взаимодействия с сообщением
    var optionsKeyboard: OptionsKeyboardCellObject { get set }
    
    /// Обновить статус сообщения
    ///
    /// - Parameter status: новый статус сообщения
    func setStatus(status: MessageStatus)
    
    /// Создает копию cell object
    ///
    /// - Returns: копия
    func copy() -> BaseMessageCellObjectProtocol

}

//
//  BaseBubbleViewStyleProtocol.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 30.05.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Стиль для BaseBubbleView
protocol BaseBubbleViewStyleProtocol {
    
    /// Картинка фона для баббла (обычно рисунок с самим бабблом)
    func bubbleImage(viewModel: BaseMessageViewModelProtocol, isSelected: Bool) -> UIImage?
    
    /// Картинка окантовки для баббла
    func bubbleImageBorder(viewModel: BaseMessageViewModelProtocol, isSelected: Bool) -> UIImage?
    
    /// Шрифт заголовков инлайн кнопок
    func inlineButtonTextFont(viewModel: InlineKeyboardButtonViewModel, isSelected: Bool) -> UIFont
    
    /// Цвет заголовков инлайн кнопок
    func inlineButtonTextColor(viewModel: InlineKeyboardButtonViewModel, isSelected: Bool) -> UIColor
    
    /// Цвет заливки инлайн кнопок
    func inlineButtonBackgroundColor(viewModel: InlineKeyboardButtonViewModel, isSelected: Bool) -> UIColor
    
    /// Размер ряда инлайн кнопок.
    /// Ширина также определяет минимальную ширину баббла, если в нем есть инлайн кнопки.
    func inlineButtonRowSize(viewModel: BaseMessageViewModelProtocol, isSelected: Bool) -> CGSize
    
    /// Цвет сепараторов инлайн кнопок
    func inlineButtonSeparatorColor(viewModel: BaseMessageViewModelProtocol, isSelected: Bool) -> UIColor
    
    /// Цвет иконок кнопок опций взаимодействия с сообщением
    func optionButtonIconColor(viewModel: BaseMessageViewModelProtocol, isSelected: Bool) -> UIColor
    
    /// Цвет иконки кнопки опции открытия ссылки из сообщения
    func optionButtonOpenWeblinkIconColor(viewModel: BaseMessageViewModelProtocol, isSelected: Bool) -> UIColor
    
    /// Цвет иконок нажатых кнопок опций взаимодействия с сообщением
    func optionButtonIconHighlightedColor(viewModel: BaseMessageViewModelProtocol, isSelected: Bool) -> UIColor
    
    /// Инсеты для контента баббла (относительно самого баббла, например для учета "носика")
    func contentInsets(viewModel: BaseMessageViewModelProtocol, isSelected: Bool) -> BubbleViewContentInsets
    
    /// Радиус скругления контента баббла
    func contentCornerRadius(viewModel: BaseMessageViewModelProtocol, isSelected: Bool) -> CGFloat
}

//
//  BaseMessagePresenterProtocol.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 22.09.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import Chatto

/// Презентер для базовой ячейки сообщения
protocol BaseMessagePresenterProtocol: class {
    
    /// Возвращает ширину баббла в сообщении
    ///
    /// - Parameters:
    ///   - width: максимальная доступная для отрисовки ширина
    ///   - decorationAttributes: атрибуты отрисовки сообщения
    /// - Returns: ширина баббла в сообщении
    func widthForBubbleInCell(maximumWidth width: CGFloat, decorationAttributes: ChatItemDecorationAttributesProtocol?) -> CGFloat
}

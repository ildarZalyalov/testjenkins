//
//  BaseMessagePresenterBuilderProtocol.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 05.02.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation
import ChattoAdditions

/// Билдер презентера для базовой ячейки сообщения
protocol BaseMessagePresenterBuilderProtocol: class {
    
    /// Базовый стиль, который применяется к базовому сообщению
    var baseMessageStyle: BaseMessageCollectionViewCellStyleProtocol { get set }
}

//
//  BaseMessageViewModelProtocol.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 30.05.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// View model для кнопки инлайн клавиатуры
struct InlineKeyboardButtonViewModel {
    
    /// Идентификатор кнопки
    var itemId: String
    
    /// Заголовок кнопки
    var title: String
    
    /// Заблокировано ли нажатие на кнопку
    var isEnabled: Bool
}

/// View model для кнопки клавиатуры опций взаимодействия с сообщением
struct OptionsKeyboardButtonViewModel {
    
    /// Идентификатор кнопки
    var itemId: String
    
    /// Тип кнопки
    var type: OptionsKeyboardButtonType
    
    /// Выбрана ли кнопка (по аналогии с UIButton)
    var isSelected: Bool
}

/// View model для базовой ячейки сообщения
protocol BaseMessageViewModelProtocol {
    
    /// Идентификатор сообщения, для которого была создана модель
    var messageUid: String { get }
    
    /// Отключена ли обработка жестов для сообщения (тапа, длинного нажатия)
    var areInteractionGesturesDisabled: Bool { get }
    
    /// Цвет баббла (если не задан используются цвета, определяемые брендированием)
    var customBubbleColor: UIColor? { get }
    
    /// Ряды кнопок инлайн клавиатуры (если массив пуст, клавиатура не будет отображена)
    var inlineButtons: [[InlineKeyboardButtonViewModel]] { get }
    
    /// Кнопки клавиатуры опций взаимодействия с сообщением
    var optionButtons: [OptionsKeyboardButtonViewModel] { get }
    
    /// Показывается ли кнопка "раскрытия" дополнительных кнопок
    var isShowingExpandButton: Bool { get }
}

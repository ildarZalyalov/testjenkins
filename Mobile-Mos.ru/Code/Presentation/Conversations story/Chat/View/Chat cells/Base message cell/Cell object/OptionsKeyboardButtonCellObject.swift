//
//  OptionsKeyboardButtonCellObject.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 16.11.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Тип кнопки опций взаимодействия с сообщением
///
/// - share: шаринг сообщения
/// - openWeblink: открыть веб-ссылку
enum OptionsKeyboardButtonType {
    case share
    case openWeblink
}

/// Cell object для кнопки клавиатуры опций взаимодействия с сообщением
struct OptionsKeyboardButtonCellObject {
    
    /// Идентификатор кнопки
    var itemId: String
    
    /// Тип кнопки
    var type: OptionsKeyboardButtonType
    
    /// Выбрана ли кнопка (по аналогии с UIButton)
    var isSelected: Bool
    
    /// Данные для коллбэка (если есть)
    var callbackData: String?
}

/// Cell object для клавиатуры опций взаимодействия с сообщением
struct OptionsKeyboardCellObject {
    
    /// Кнопки клавиатуры
    var optionButtons: [OptionsKeyboardButtonCellObject] = []
}

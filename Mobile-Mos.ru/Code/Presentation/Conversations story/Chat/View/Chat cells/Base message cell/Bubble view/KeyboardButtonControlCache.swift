//
//  KeyboardButtonControlCache.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 29.05.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Кэш кнопок для разных клавиатур в сообщении
final class KeyboardButtonControlCache {
    
    fileprivate(set) var initialCacheSize = 20
    
    fileprivate var buttonControls = [KeyboardButtonControl]()
    
    /// Обработчик дополнительной инициализации для кнопок в кеше, вызывается при создании кнопки
    var onNewCachedItemHandler: ((KeyboardButtonControl) -> Void)? {
        didSet {
            guard onNewCachedItemHandler != nil else { return }
            buttonControls.forEach { onNewCachedItemHandler?($0) }
        }
    }
    
    init() {
        increaseCache(by: initialCacheSize)
    }
    
    init(sized size: Int) {
        initialCacheSize = size
        increaseCache(by: initialCacheSize)
    }
    
    fileprivate func createCachedButton() -> KeyboardButtonControl {
        
        let button = KeyboardButtonControl(type: .custom)
        button.isExclusiveTouch = true
        
        onNewCachedItemHandler?(button)
        
        return button
    }
    
    fileprivate func increaseCache(by count: Int) {
        for _ in 0 ..< count {
            buttonControls.append(createCachedButton())
        }
    }
    
    /// Получить некоторое кол-во кнопок из кэша. Если запрошено кнопок больше, чем есть в кэше, он автоматически расширится.
    ///
    /// - Parameter count: требуемое кол-во
    /// - Returns: кнопки
    func obtain(count: Int) -> [KeyboardButtonControl] {
        
        if count > buttonControls.count {
            increaseCache(by: count - buttonControls.count)
        }
        
        for (index, button) in buttonControls.enumerated() {
            button.isHidden = index >= count
        }
        
        return Array(buttonControls.prefix(upTo: count))
    }
}

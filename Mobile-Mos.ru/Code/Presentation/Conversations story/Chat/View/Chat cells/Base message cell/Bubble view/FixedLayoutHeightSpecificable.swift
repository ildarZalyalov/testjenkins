//
//  FixedLayoutHeightSpecificable.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 14.12.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Протокол для вьюх, которые умеют менять размер в зависимости от содержимого, но поддерживают установку фиксированной высоты
protocol FixedLayoutHeightSpecificable: class {
    
    /// Фиксированная высота вьюхи (если не задана, то вьюха продолжает менять высоту в зависимости от содержимого)
    var fixedLayoutHeight: CGFloat? { get set }
}

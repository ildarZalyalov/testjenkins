//
//  InlineKeyboardButtonCellObject.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 01.06.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Cell object для кнопки инлайн клавиатуры
struct InlineKeyboardButtonCellObject {
    
    /// Идентификатор кнопки
    var itemId: String
    
    /// Заголовок кнопки
    var title: String
    
    /// Остается ли кнопка активной, ели сообщение уходит в историю (становится не последним входящим)
    var isAlwaysActive: Bool
}

/// Cell object для инлайн клавиатуры
struct InlineKeyboardCellObject {
    
    /// Ряды кнопок инлайн клавиатуры
    var inlineButtons: [[InlineKeyboardButtonCellObject]] = []
    
    /// "Схлопнуты" ли кнопки
    var isCollapsed = false
    
    /// Максимальное отображаемое кол-во рядов кнопок (если их больше, то они будут "схлопнуты")
    var maxRowCountBeforeCollapsing = Int.max {
        didSet {
            // нет смысла заменять кнопку "еще" на 1 дополнительный ряд кнопок, поэтому нужно хотя бы на 2 ряда больше
            isCollapsed = inlineButtons.count > maxRowCountBeforeCollapsing + 1
        }
    }
}

//
//  BaseInteractionHandlerProtocol.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 01.06.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import ChattoAdditions

/// Базовый протокол обработчика событий в ячейке (который передается в презентер)
protocol BaseInteractionHandlerProtocol: BaseMessageInteractionHandlerProtocol {
    
    /// Пользователь нажал на кнопку инлайн клавиатуры
    ///
    /// - Parameters:
    ///   - viewModel: view model сообщения
    ///   - buttonId: идентификатор кнопки
    func userDidPressInlineKeyboardButton(viewModel: BaseMessageViewModelProtocol, with buttonId: String)
    
    /// Пользователь нажал на кнопку "расхлопывания" инлайн клавиатуры
    ///
    /// - Parameter viewModel: view model сообщения
    func userDidPressExpandInlineKeyboardButton(viewModel: BaseMessageViewModelProtocol)
    
    /// Пользователь нажал на кнопку клавиатуры опций взаимодействия с сообщением
    ///
    /// - Parameters:
    ///   - viewModel: view model сообщения
    ///   - buttonId: идентификатор кнопки
    func userDidPressOptionButton(viewModel: BaseMessageViewModelProtocol, with buttonId: String)
    
    /// Пользователь нажал на веб-ссылку в тексте сообщения, чтобы открыть ее
    ///
    /// - Parameters:
    ///   - url: веб-ссылка
    ///   - inMessage: view model сообщения
    func userDidPressToOpen(url: URL, inMessage viewModel: BaseMessageViewModelProtocol)
}

//
//  BaseBubbleLayoutModel.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 30.05.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import Chatto

/// Модель лейаута для BaseBubbleView. Умеет расчитывать размер BaseBubbleView при заданных параметрах, а также кеширует его
/// Смотри приватный оригинал TextBubbleLayoutModel в ChattoAdditions/TextBubbleView.swift
class BaseBubbleLayoutModel {
    
    /// Содержит входные данные для расчета, а также служит ключом хэширования
    class LayoutContext: Hashable {
        
        struct LayoutParameters {
            
            /// Количество инлайн кнопок в каждом ряду
            let inlineButtonsByRowCounts: [Int]
            /// Количество кнопок для взаимодействия с сообщением
            let optionButtonsCount: Int
            /// Размер ряда инлайн кнопок (смотри BaseBubbleViewStyleProtocol)
            let inlineButtonRowSize: CGSize
            /// Инсеты для контента баббла
            let contentInsets: BubbleViewContentInsets
            /// Фиксированная высота для сообщения (учитывается, если больше 0)
            let fixedLayoutHeight: CGFloat
        }
        
        let layoutParameters: LayoutParameters
        
        /// Тип модели лейаута, которая использует данный контекст (нужен для понимания какую модель создавать для данного контекста)
        var bubbleLayoutType: BaseBubbleLayoutModel.Type {
            return BaseBubbleLayoutModel.self
        }
        
        init(parameters: LayoutParameters) {
            self.layoutParameters = parameters
        }
        
        var hashValue: Int {
            
            let params = layoutParameters
            let hashes = [params.inlineButtonsByRowCounts.combinedHashValue,
                          params.optionButtonsCount.hashValue,
                          params.inlineButtonRowSize.combinedHashValue,
                          params.contentInsets.left.hashValue,
                          params.contentInsets.right.hashValue,
                          params.fixedLayoutHeight.hashValue]
            
            return hashes.combinedHashValue
        }
        
        static func == (lhs: LayoutContext, rhs: LayoutContext) -> Bool {
            
            if lhs === rhs { return true }
            
            let lhsParams = lhs.layoutParameters
            let rhsParams = rhs.layoutParameters
            
            let arrayEqual = lhsParams.inlineButtonsByRowCounts == rhsParams.inlineButtonsByRowCounts
            let lhsValues = (lhsParams.optionButtonsCount, lhsParams.inlineButtonRowSize, lhsParams.contentInsets.left, lhsParams.contentInsets.right, lhsParams.fixedLayoutHeight)
            let rhsValues = (rhsParams.optionButtonsCount, rhsParams.inlineButtonRowSize, rhsParams.contentInsets.left, rhsParams.contentInsets.right, rhsParams.fixedLayoutHeight)
            
            return lhsValues == rhsValues && arrayEqual
        }
    }
    
    /// Высота горизонтального сепаратора между рядами инлайн кнопок
    static let inlineButtonRowSpace: CGFloat = 1
    
    /// Толщина вертикального сепаратора между инлайн кнопками в ряду
    static let inlineButtonColumnSpace: CGFloat = 1
    
    /// Размер кнопки опций взаимодействия с сообщением
    let optionButtonSize = CGSize(width: 41, height: 41)
    
    /// Отступ слева от баббла для клавиатуры опций взаимодействия с сообщением
    let optionButtonsKeyboardLeftOffset: CGFloat = 0
    
    let layoutContext: LayoutContext
    
    /// Фрейм баббла
    var bubbleFrame: CGRect = CGRect.zero
    
    /// Размер полученного лейаута
    var size: CGSize = CGSize.zero
    
    /// Фрейм вьюхи, содержащей контент баббла
    var contentParentFrame: CGRect = CGRect.zero
    
    /// Фрейм вьюхи-бекграунда инлайн клавиатуры, которая содержит инлайн кнопки
    var inlineButtonBackgroundFrame: CGRect = CGRect.zero
    
    /// Размер инлайн клавиатуры
    var inlineButtonBackgroundSize: CGSize = CGSize.zero
    
    /// Фреймы инлайн кнопок (ряды объединены в один массив друг за другом)
    var inlineButtonFrames: [CGRect] = []
    
    /// Размер клавиатуры опций взаимодействия с сообщением
    var optionButtonsKeyboardSize: CGSize = CGSize.zero
    
    /// Фреймы кнопок опций взаимодействия с сообщением (сверху вниз)
    var optionButtonFrames: [CGRect] = []
    
    required init(layoutContext: LayoutContext) {
        self.layoutContext = layoutContext
    }
    
    /// Пересчитать кэш лейаута
    func calculateLayout() {
        
        var contentSize = calculateIntrinsicContentSize()
        if layoutContext.layoutParameters.fixedLayoutHeight > 0 {
            contentSize.height = layoutContext.layoutParameters.fixedLayoutHeight
        }
        
        calculateLayout(for: contentSize)
    }
    
    /// Пересчитать лейаут для конкретного размера контента
    ///
    /// - Parameters:
    ///   - contentSize: размер контента
    func calculateLayout(for contentSize: CGSize) {
        
        let layoutParameters = layoutContext.layoutParameters
        let contentWidthOutset = layoutParameters.contentInsets.left + layoutParameters.contentInsets.right
        let bubbleSize = contentSize.bma_outsetBy(dx: contentWidthOutset, dy: 0)
        
        bubbleFrame = CGRect(origin: CGPoint.zero, size: bubbleSize)
        
        calculateContentParentFrame()
        calculateContentLayout(for: contentSize)
        calculateOptionButtonsKeyboardFrame()
        
        var width = bubbleFrame.width
        if optionButtonsKeyboardSize.width > 0 {
            width += optionButtonsKeyboardLeftOffset + optionButtonsKeyboardSize.width
        }
        
        var height = bubbleFrame.height
        if optionButtonsKeyboardSize.height > height {
            
            let yOffset = (optionButtonsKeyboardSize.height - height) / 2
            bubbleFrame = bubbleFrame.offsetBy(dx: 0, dy: yOffset)
            
            height = optionButtonsKeyboardSize.height
        }
        
        size = CGSize(width: width, height: height)
    }
    
    /// Пересчитать фрейм вьюхи, содержащей контент баббла
    func calculateContentParentFrame() {
        
        let layoutParameters = layoutContext.layoutParameters
        var contentOrigin = CGPoint.zero
        var contentSize = bubbleFrame.size
        
        contentOrigin.x = layoutParameters.contentInsets.left
        contentSize.width -= layoutParameters.contentInsets.left + layoutParameters.contentInsets.right
        
        contentParentFrame = CGRect(origin: contentOrigin, size: contentSize)
    }
    
    /// Пересчитать лейаут клавиатуры опций взаимодействия с сообщением
    /// (ее размер и фреймы кнопок)
    func calculateOptionButtonsKeyboardFrame() {
        
        let layoutParameters = layoutContext.layoutParameters
        let buttonsCount = CGFloat(layoutParameters.optionButtonsCount)
        
        if buttonsCount > 0 {
            optionButtonsKeyboardSize = CGSize(width: optionButtonSize.width,
                                               height: optionButtonSize.height * buttonsCount)
        }
        else {
            optionButtonsKeyboardSize = CGSize.zero
        }
        
        optionButtonFrames = []
        
        guard buttonsCount > 0 else { return }
        
        let currentX: CGFloat = bubbleFrame.width + optionButtonsKeyboardLeftOffset
        var currentY: CGFloat = 0
        
        if bubbleFrame.height > optionButtonsKeyboardSize.height {
            currentY = (bubbleFrame.height - optionButtonsKeyboardSize.height) / 2
        }
        
        for _ in 0 ..< layoutParameters.optionButtonsCount {
            
            optionButtonFrames.append(CGRect(x: currentX,
                                             y: currentY,
                                             width: optionButtonSize.width,
                                             height: optionButtonSize.height))
            
            currentY += optionButtonSize.height
        }
    }
    
    //MARK: - Переопределить в наследниках

    /// Посчитать размер контента баббла
    /// Базовая реализация возвращает размер инлайн клавиатуры
    /// Наследники могут, переопределив, расширить величину баббла для дополнительного контента
    func calculateIntrinsicContentSize() -> CGSize {
        
        let layoutParameters = layoutContext.layoutParameters
        let buttonsRowCount = layoutParameters.inlineButtonsByRowCounts.count
        
        guard buttonsRowCount > 0 else {
            return CGSize.zero
        }
        
        let buttonSize = layoutParameters.inlineButtonRowSize
        let inlineButtonRowSpace = BaseBubbleLayoutModel.inlineButtonRowSpace
        let inlineButtonColumnSpace = BaseBubbleLayoutModel.inlineButtonColumnSpace
        
        inlineButtonBackgroundSize.width = buttonSize.width
        inlineButtonBackgroundSize.width += 2 * inlineButtonColumnSpace
        
        inlineButtonBackgroundSize.height = CGFloat(buttonsRowCount) * buttonSize.height
        inlineButtonBackgroundSize.height += (CGFloat(buttonsRowCount) + 1) * inlineButtonRowSpace
        
        return inlineButtonBackgroundSize
    }
    
    /// Пересчитать кэш лейаута контента баббла. Метод получает размер, посчитанный в calculateIntrinsicContentSize. Последний может быть
    /// переопределен, поэтому размер может отличаться от посчитанного ранее.
    ///
    /// - Parameter contentSize: посчитанный размер всего контента баббла
    func calculateContentLayout(for contentSize: CGSize) {
        
        let layoutParameters = layoutContext.layoutParameters
        let buttonsRowCount = layoutParameters.inlineButtonsByRowCounts.count
        let inlineButtonRowSpace = BaseBubbleLayoutModel.inlineButtonRowSpace
        let inlineButtonColumnSpace = BaseBubbleLayoutModel.inlineButtonColumnSpace
        
        inlineButtonBackgroundFrame = CGRect(x: 0, y: contentSize.height - inlineButtonBackgroundSize.height,
                                             width: contentSize.width, height: inlineButtonBackgroundSize.height)
        inlineButtonBackgroundFrame = inlineButtonBackgroundFrame.insetBy(dx: inlineButtonColumnSpace, dy: inlineButtonRowSpace)
        
        inlineButtonFrames = []
        
        guard buttonsRowCount > 0 else {
            return
        }
        
        var currentY: CGFloat = inlineButtonRowSpace
        let buttonHeight = layoutParameters.inlineButtonRowSize.height
        
        for buttonsCount in layoutParameters.inlineButtonsByRowCounts {
            
            let count = CGFloat(buttonsCount)
            var buttonWidth = inlineButtonBackgroundFrame.width
            buttonWidth -= (count - 1) * inlineButtonColumnSpace
            buttonWidth /= count
            
            var currentX: CGFloat = 0
            
            for _ in 0 ..< buttonsCount {
                
                let buttonFrame = CGRect(x: currentX, y: currentY, width: buttonWidth, height: buttonHeight)
                inlineButtonFrames.append(buttonFrame)
                
                currentX += inlineButtonColumnSpace + buttonWidth
            }
            
            currentY += buttonHeight + inlineButtonRowSpace
        }
    }
}

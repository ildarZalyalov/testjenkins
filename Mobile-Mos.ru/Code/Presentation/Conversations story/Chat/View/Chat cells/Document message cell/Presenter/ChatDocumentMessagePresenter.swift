//
//  ChatDocumentMessagePresenter.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 07.11.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import ChattoAdditions

class ChatDocumentMessagePresenter<ViewModelBuilderT, InteractionHandlerT>:
    ChatTextMessagePresenter<ViewModelBuilderT, InteractionHandlerT> where
    ViewModelBuilderT: ViewModelBuilderProtocol,
    ViewModelBuilderT.ViewModelT: ChatDocumentMessageViewModelProtocol,
    InteractionHandlerT: BaseInteractionHandlerProtocol,
    InteractionHandlerT.ViewModelT == ViewModelBuilderT.ViewModelT {
    
    public init(
        messageModel: ModelT,
        viewModelBuilder: ViewModelBuilderT,
        interactionHandler: InteractionHandlerT?,
        sizingCell: ChatDocumentMessageCollectionViewCell,
        baseCellStyle: BaseMessageCollectionViewCellStyleProtocol,
        documentCellStyle: ChatDocumentMessageCollectionViewCellStyleProtocol,
        layoutCache: NSCache<AnyObject, AnyObject>) {
        self.documentCellStyle = documentCellStyle
        super.init(
            messageModel: messageModel,
            viewModelBuilder: viewModelBuilder,
            interactionHandler: interactionHandler,
            sizingCell: sizingCell,
            baseCellStyle: baseCellStyle,
            textCellStyle: documentCellStyle,
            layoutCache: layoutCache)
    }
    
    let documentCellStyle: ChatDocumentMessageCollectionViewCellStyleProtocol
    
    override class func registerCells(_ collectionView: UICollectionView) {
        collectionView.register(ChatDocumentMessageCollectionViewCell.self, forCellWithReuseIdentifier: "chat-document-message-incoming")
        collectionView.register(ChatDocumentMessageCollectionViewCell.self, forCellWithReuseIdentifier: "chat-document-message-outcoming")
    }
    
    override func dequeueCell(collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell {
        let identifier = messageViewModel.isIncoming ? "chat-document-message-incoming" : "chat-document-message-outcoming"
        return collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath)
    }
    
    override func createViewModel() -> ViewModelBuilderT.ViewModelT {
        
        let viewModel = viewModelBuilder.createViewModel(messageModel)
        let updateClosure = { [weak self] (old: Any, new: Any) -> Void in
            self?.updateCurrentCell()
        }
        
        viewModel.iconState.observe(self, closure: updateClosure)
        viewModel.downloadProgress.observe(self, closure: updateClosure)
        
        return viewModel
    }
    
    public var documentCell: ChatDocumentMessageCollectionViewCell? {
        
        if let cell = self.cell {
            if let documentCell = cell as? ChatDocumentMessageCollectionViewCell {
                return documentCell
            } else {
                assert(false, "Invalid cell was given to presenter!")
            }
        }
        
        return nil
    }
    
    override func configureCell(_ cell: BaseMessageCollectionViewCell<ChatTextBubbleView>, decorationAttributes: ChatItemDecorationAttributes, animated: Bool, additionalConfiguration: (() -> Void)?) {
        
        guard let cell = cell as? ChatDocumentMessageCollectionViewCell else {
            assert(false, "Invalid cell received")
            return
        }
        
        super.configureCell(cell, decorationAttributes: decorationAttributes, animated: animated) { () -> Void in
            
            cell.documentMessageViewModel = self.messageViewModel
            cell.documentMessageStyle = self.documentCellStyle
            
            cell.onDocumentIconPress = { [weak self] cell in
                guard let strongSelf = self else { return }
                strongSelf.onDocumentIconPress(in: cell)
            }
            
            additionalConfiguration?()
        }
    }
    
    override func updateCurrentCell() {
        if let cell = documentCell, let decorationAttributes = decorationAttributes {
            configureCell(cell, decorationAttributes: decorationAttributes, animated: itemVisibility != .appearing, additionalConfiguration: nil)
        }
    }
    
    override func cellWillBeShown() {
        super.cellWillBeShown()
        documentCell?.refreshProgressView()
    }
    
    override func performMenuControllerAction(_ action: Selector) {
        
        let selector = #selector(UIResponderStandardEditActions.copy(_:))
        
        if action == selector {
            UIPasteboard.general.string = messageViewModel.text.isEmpty ? messageViewModel.fileName : messageViewModel.text
        } else {
            assert(false, "Unexpected action")
        }
    }
    
    func onDocumentIconPress(in cell: ChatDocumentMessageCollectionViewCell) {
        
        guard let documentMessageViewModel = cell.documentMessageViewModel else {
            return
        }
        guard let documentInteractionHandler = interactionHandler as? ChatDocumentMessageHandler else {
            return
        }
        
        documentInteractionHandler.userDidPressDocumentIcon(viewModel: documentMessageViewModel)
    }
}

//
//  ChatDocumentMessageHandler.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 07.11.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import ChattoAdditions

/// Обработчик действий пользователя для сообщений с документом
class ChatDocumentMessageHandler: BaseInteractionHandlerProtocol {
    
    weak var baseHandler: BaseChatMessageHandler?
    
    init(baseHandler: BaseChatMessageHandler) {
        self.baseHandler = baseHandler
    }
    
    func userDidTapOnFailIcon(viewModel: ChatDocumentMessageViewModel, failIconView: UIView) {
        baseHandler?.userDidTapOnFailIcon(viewModel: viewModel, failIconView: failIconView)
    }
    
    func userDidTapOnAvatar(viewModel: ChatDocumentMessageViewModel) {
        baseHandler?.userDidTapOnAvatar(viewModel: viewModel)
    }
    
    func userDidTapOnBubble(viewModel: ChatDocumentMessageViewModel) {
        baseHandler?.userDidTapOnBubble(viewModel: viewModel)
    }
    
    func userDidBeginLongPressOnBubble(viewModel: ChatDocumentMessageViewModel) {
        baseHandler?.userDidBeginLongPressOnBubble(viewModel: viewModel)
    }
    
    func userDidEndLongPressOnBubble(viewModel: ChatDocumentMessageViewModel) {
        baseHandler?.userDidEndLongPressOnBubble(viewModel: viewModel)
    }
    
    func userDidSelectMessage(viewModel: ChatDocumentMessageViewModel) {
        baseHandler?.userDidSelectMessage(viewModel: viewModel)
    }
    
    func userDidDeselectMessage(viewModel: ChatDocumentMessageViewModel) {
        baseHandler?.userDidDeselectMessage(viewModel: viewModel)
    }
    
    func userDidPressInlineKeyboardButton(viewModel: BaseMessageViewModelProtocol, with buttonId: String) {
        baseHandler?.userDidPressInlineKeyboardButton(viewModel: viewModel, with: buttonId)
    }
    
    func userDidPressExpandInlineKeyboardButton(viewModel: BaseMessageViewModelProtocol) {
        baseHandler?.userDidPressExpandInlineKeyboardButton(viewModel: viewModel)
    }
    
    func userDidPressToOpen(url: URL, inMessage viewModel: BaseMessageViewModelProtocol) {
        baseHandler?.userDidPressToOpen(url: url, inMessage: viewModel)
    }
    
    func userDidPressOptionButton(viewModel: BaseMessageViewModelProtocol, with buttonId: String) {
        baseHandler?.userDidPressOptionButton(viewModel: viewModel, with: buttonId)
    }
    
    func userDidPressDocumentIcon(viewModel: ChatDocumentMessageViewModelProtocol) {
        baseHandler?.userDidPressDocumentIcon(viewModel: viewModel)
    }
}

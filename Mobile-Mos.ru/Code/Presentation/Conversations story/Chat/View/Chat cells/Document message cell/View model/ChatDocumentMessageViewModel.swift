//
//  ChatDocumentMessageViewModel.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 07.11.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import ChattoAdditions

/// Состояние иконки загрузки документа
///
/// - beginDownload: начать загрузку документа
/// - cancelDownload: отменить загрузку документа
/// - openDocument: открыть документ
enum ChatDocumentMessageViewModelIconState {
    case beginDownload
    case cancelDownload
    case openDocument
}

/// Протокол view model для ячейки сообщения с документом
protocol ChatDocumentMessageViewModelProtocol: ChatTextMessageViewModelProtocol  {
    
    /// Имя файла вместе с расширением
    var fileName: String { get }
    
    /// Описание размера файла (если есть)
    var fileSizeDescription: String? { get }
    
    /// Состояние иконки загрузки документа
    var iconState: Observable<ChatDocumentMessageViewModelIconState> { get }
    
    /// Прогресс загрузки документа (от 0 до 1)
    var downloadProgress: Observable<Double> { get }
}

/// View model сообщения с документом
class ChatDocumentMessageViewModel: ChatTextMessageViewModel, ChatDocumentMessageViewModelProtocol {
    
    /// Имя файла вместе с расширением
    let fileName: String
    
    /// Описание размера файла (если есть)
    var fileSizeDescription: String?
    
    /// Состояние иконки загрузки документа
    let iconState: Observable<ChatDocumentMessageViewModelIconState> = Observable(.beginDownload)
    
    /// Прогресс загрузки документа (от 0 до 1)
    let downloadProgress: Observable<Double> = Observable(0)
    
    /// Наблюдатель за прогрессом загрузки документа
    fileprivate var downloadProgressObserver: KeyValueObserver?
    
    init(textModel: ChatTextMessageViewModel, fileName: String) {
        self.fileName = fileName
        super.init(messageUid: textModel.messageUid,
                   textMessage: textModel.textMessage,
                   messageViewModel: textModel.messageViewModel,
                   inlineButtons: textModel.inlineButtons,
                   optionButtons: textModel.optionButtons,
                   isShowingExpandButton: textModel.isShowingExpandButton,
                   formattedText: textModel.formattedText)
    }
    
    /// Получить состояние иконки загрузки документа из статуса загрузки
    ///
    /// - Parameter downloadStatus: статус загрузки документа
    /// - Returns: состояние иконки загрузки документа
    func iconState(from downloadStatus: ChatMessageDocumentDownloadState.Status) -> ChatDocumentMessageViewModelIconState {
        switch downloadStatus {
        case .notDownloaded:
            return .beginDownload
        case .downloading:
            return .cancelDownload
        case .downloadFinished:
            return .openDocument
        }
    }
    
    /// Обновить прогресс загрузки документа
    ///
    /// - Parameter progress: обновляемый прогресс загрузки документа
    func updateDownloadProgress(with progress: Progress) {
        downloadProgress.value = progress.fractionCompleted
    }
    
    /// Начать наблюдение за обновляемым прогрессом загрузки документа
    ///
    /// - Parameter progress: обновляемый прогресс загрузки документа
    func observeDownloadProgress(with progress: Progress?) {
        
        if progress != nil {
            
            updateDownloadProgress(with: progress!)
            
            downloadProgressObserver = KeyValueObserver(observedObject: progress!, observedKeyPath: #keyPath(Progress.fractionCompleted), observationContext: nil, handler: { [weak self] _ in
                DispatchQueue.main.async {
                    self?.updateDownloadProgress(with: progress!)
                }
            })
        }
        else {
            downloadProgress.value = 0
            downloadProgressObserver = nil
        }
    }
}

/// Билдер view model сообщения с документом
class ChatDocumentMessageViewModelBuilder: ViewModelBuilderProtocol {
    
    let textMessageViewModelBuilder: ChatTextMessageViewModelBuilder
    
    init(style: ChatTextMessageCollectionViewCellStyleProtocol, formattingExtractor: ChatTextFormattingExtractor) {
        textMessageViewModelBuilder = ChatTextMessageViewModelBuilder(style: style, formattingExtractor: formattingExtractor)
    }
    
    //MARK: - ViewModelBuilderProtocol
    
    func createViewModel(_ documentMessage: ChatDocumentMessageCellObject) -> ChatDocumentMessageViewModel {
        
        let textViewModel = textMessageViewModelBuilder.createViewModel(documentMessage)
        let viewModel = ChatDocumentMessageViewModel(textModel: textViewModel,
                                                     fileName: documentMessage.fileName)
        
        if let fileSize = documentMessage.fileSize {
            viewModel.fileSizeDescription = String(fileSize) + " " + StringsHelper.kilobytesString
        }
        
        let status = documentMessage.downloadState.status.value
        weak var weakViewModel = viewModel
        
        viewModel.iconState.value = viewModel.iconState(from: status)
        viewModel.observeDownloadProgress(with: documentMessage.downloadState.progress.value)
        
        documentMessage.downloadState.status.add(observer: viewModel) { _, newStatus in
            DispatchQueue.main.async {
                guard let model = weakViewModel else { return }
                model.iconState.value = model.iconState(from: newStatus)
            }
        }
        documentMessage.downloadState.progress.add(observer: viewModel) { _, newProgress in
            DispatchQueue.main.async {
                weakViewModel?.observeDownloadProgress(with: newProgress)
            }
        }
        
        return viewModel
    }
    
    func canCreateViewModel(fromModel model: Any) -> Bool {
        return model is ChatDocumentMessageCellObject
    }
}

//
//  ChatDocumentBubbleLayoutModel.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 17.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Модель лейаута для ChatDocumentBubbleView. Умеет расчитывать размер ChatDocumentBubbleView при заданных параметрах, а также кеширует его
class ChatDocumentBubbleLayoutModel: ChatTextBubbleLayoutModel {
    
    /// Содержит входные данные для расчета, а также служит ключем хэширования
    class ChatDocumentLayoutContext: ChatTextBubbleLayoutModel.ChatTextLayoutContext {
        
        struct ChatDocumentLayoutParameters {
            
            /// Имя файла
            let fileName: String
            
            /// Шрифт для имени файла
            let fileNameFont: UIFont
            
            /// Описание размера файла
            let fileSize: String
            
            /// Шрифт для описания размера файла
            let fileSizeFont: UIFont
        }
        
        let chatDocumentLayoutParameters: ChatDocumentLayoutParameters
        
        override var bubbleLayoutType: BaseBubbleLayoutModel.Type {
            return ChatDocumentBubbleLayoutModel.self
        }
        
        init(parameters: LayoutParameters,
             textParameters: ChatTextLayoutParameters,
             documentParameters: ChatDocumentLayoutParameters) {
            self.chatDocumentLayoutParameters = documentParameters
            super.init(parameters: parameters, textParameters: textParameters)
        }
        
        override var hashValue: Int {
            
            let params = chatDocumentLayoutParameters
            let hashes = [super.hashValue,
                          params.fileName.hashValue,
                          params.fileNameFont.hashValue,
                          params.fileSize.hashValue,
                          params.fileSizeFont.hashValue]
            
            return hashes.combinedHashValue
        }
        
        static func == (lhs: ChatDocumentLayoutContext, rhs: ChatDocumentLayoutContext) -> Bool {
            
            if lhs === rhs { return true }
            
            typealias BaseContext = ChatTextBubbleLayoutModel.ChatTextLayoutContext
            let baseEqual = lhs as BaseContext == rhs as BaseContext
            
            let lhsParams = lhs.chatDocumentLayoutParameters
            let rhsParams = rhs.chatDocumentLayoutParameters
            
            let lhsValues = (lhsParams.fileName, lhsParams.fileNameFont, lhsParams.fileSize, lhsParams.fileSizeFont)
            let rhsValues = (rhsParams.fileName, rhsParams.fileNameFont, rhsParams.fileSize, rhsParams.fileSizeFont)
            
            return lhsValues == rhsValues && baseEqual
        }
    }
    
    /// Размер иконки статуса загрузки документа
    let iconSize = CGSize(width: 44, height: 44)
    
    /// Отступ вокруг иконки статуса загрузки документа
    let iconMargin: CGFloat = 10
    
    /// Отступ от лейблов снизу и сверху
    let labelsTopBottomMargin: CGFloat = 8
    
    /// Отступ от лейблов справа
    let labelsTrailingMargin: CGFloat = 12
    
    /// Минимальная высота лейблов
    let labelsMinimumHeight: CGFloat = 24
    
    
    /// Фрейм иконки статуса загрузки документа
    var iconFrame: CGRect = CGRect.zero
    
    /// Размер лейбла с именем документа
    var fileNameSize: CGSize = CGSize.zero
    
    /// Фрейм лейбла с именем документа
    var fileNameFrame: CGRect = CGRect.zero
    
    /// Размер лейбла с описанием размера документа
    var fileSizeSize: CGSize = CGSize.zero
    
    /// Фрейм лейбла с описанием размера документа
    var fileSizeFrame: CGRect = CGRect.zero
    
    /// Размер всего прейвью документа (иконка + лейблы)
    var filePreviewSize: CGSize = CGSize.zero
    
    /// Посчитать размер контента баббла
    override func calculateIntrinsicContentSize() -> CGSize {
        
        var contentSize = super.calculateIntrinsicContentSize()
        
        guard let documentLayoutContext = layoutContext as? ChatDocumentLayoutContext else { return contentSize }
        let textLayoutParameters = documentLayoutContext.chatTextLayoutParameters
        let layoutParameters = documentLayoutContext.chatDocumentLayoutParameters
        
        fileNameSize = layoutParameters.fileName.sizeOfString(with: layoutParameters.fileNameFont)
        if fileNameSize.height < labelsMinimumHeight {
            fileNameSize.height = labelsMinimumHeight
        }
        
        fileSizeSize = layoutParameters.fileSize.sizeOfString(with: layoutParameters.fileSizeFont)
        if fileSizeSize.height < labelsMinimumHeight {
            fileSizeSize.height = labelsMinimumHeight
        }
        
        let wantedLabelsWidth = max(fileNameSize.width, fileSizeSize.width)
        let otherControlsWidth = iconSize.width + 2 * iconMargin + labelsTrailingMargin
        
        var width = wantedLabelsWidth + otherControlsWidth
        if width > textLayoutParameters.preferredMaxLayoutWidth {
            width = textLayoutParameters.preferredMaxLayoutWidth
        }
        
        let actualLabelsWidth = width - otherControlsWidth
        fileNameSize.width = actualLabelsWidth
        fileSizeSize.width = actualLabelsWidth
        
        let minimumHeight = iconSize.height + 2 * iconMargin
        var height = 2 * labelsTopBottomMargin + fileNameSize.height + fileSizeSize.height
        if height < minimumHeight {
            height = minimumHeight
        }
        
        if !textLayoutParameters.text.string.isEmpty {
            height -= labelsTopBottomMargin
        }
        
        filePreviewSize = CGSize(width: width, height: height)
        
        contentSize.width = max(contentSize.width, filePreviewSize.width)
        contentSize.height += filePreviewSize.height
        
        return contentSize
    }
    
    /// Пересчитать кеш лейаута контента баббла
    override func calculateContentLayout(for contentSize: CGSize) {
        
        super.calculateContentLayout(for: contentSize)
        
        guard let documentLayoutContext = layoutContext as? ChatDocumentLayoutContext else { return }
        let textIsEmpty = documentLayoutContext.chatTextLayoutParameters.text.string.isEmpty
        
        var iconTopMargin = (filePreviewSize.height - iconSize.height) / 2
        if !textIsEmpty {
            iconTopMargin -= labelsTopBottomMargin / 2
        }
        let topMargin = textIsEmpty ? labelsTopBottomMargin : 0
        
        iconFrame = CGRect(x: iconMargin,
                           y: textSize.height + iconTopMargin,
                           width: iconSize.width,
                           height: iconSize.height)
        
        fileNameFrame = CGRect(x: iconSize.width + 2 * iconMargin,
                               y: textSize.height + topMargin,
                               width: fileNameSize.width,
                               height: fileNameSize.height)
        
        fileSizeFrame = CGRect(x: iconSize.width + 2 * iconMargin,
                               y: textSize.height + topMargin + fileNameSize.height,
                               width: fileSizeSize.width,
                               height: fileSizeSize.height)
    }
}

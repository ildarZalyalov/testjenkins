//
//  ChatDocumentBubbleView.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 07.11.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import ChattoAdditions

/// Делегат баббла для ChatMessage, у которого тип контента - документ
protocol ChatDocumentBubbleViewDelegate: class {
    
    /// Пользователь нажал на иконку статуса загрузки документа
    func didPressDocumentIcon()
}

/// View для баббла для ChatMessage, у которого тип контента - документ
class ChatDocumentBubbleView: ChatTextBubbleView {
    
    /// Кнопка с иконкой статуса загрузки документа
    private lazy var iconButton: UIButton = {
        
        let iconButton = UIButton(type: .system)
        iconButton.clipsToBounds = true
        iconButton.backgroundColor = UIColor.white
        iconButton.contentMode = .center
        iconButton.addTarget(self, action: #selector(didPressIconButton), for: .touchUpInside)
        
        return iconButton
    }()
    
    /// Индикатор загрузки документа
    private lazy var iconProgressView: CircleProgressView = {
        
        let progressView = CircleProgressView()
        progressView.setLineWidth(1)
        progressView.isUserInteractionEnabled = false
        progressView.isHidden = true
        
        return progressView
    }()
    
    /// Лейбл с названием документа
    private var fileNameLabel: UILabel = {
        
        let fileNameLabel = UILabel()
        fileNameLabel.numberOfLines = 1
        
        return fileNameLabel
    }()
    
    /// Лейбл с описанием размера документа
    private var fileSizeLabel: UILabel = {
        
        let fileSizeLabel = UILabel()
        fileSizeLabel.numberOfLines = 1
        
        return fileSizeLabel
    }()
    
    weak var documentBubbleDelegate: ChatDocumentBubbleViewDelegate?
    
    @objc
    fileprivate func didPressIconButton() {
        documentBubbleDelegate?.didPressDocumentIcon()
    }
    
    /// Обновить индикатор прогресса загрузки
    func refreshProgressView() {
        guard let viewModel = messageViewModel as? ChatDocumentMessageViewModelProtocol else { return }
        iconProgressView.setProgress(CGFloat(viewModel.downloadProgress.value))
    }
    
    override func initializeContentSubviews(contentParentView: UIView) {
        super.initializeContentSubviews(contentParentView: contentParentView)
        contentParentView.addSubview(iconButton)
        contentParentView.addSubview(iconProgressView)
        contentParentView.addSubview(fileNameLabel)
        contentParentView.addSubview(fileSizeLabel)
    }
    
    override func updateContentViews() {
        
        super.updateContentViews()
        
        guard let style = style as? ChatDocumentBubbleViewStyleProtocol else { return }
        guard let viewModel = messageViewModel as? ChatDocumentMessageViewModelProtocol else { return }
        
        switch viewModel.iconState.value {
        case .beginDownload:
            iconButton.setImage(#imageLiteral(resourceName: "loadDocumentStart"), for: .normal)
        case .cancelDownload:
            iconButton.setImage(#imageLiteral(resourceName: "loadDocumentCancel"), for: .normal)
        case .openDocument:
            iconButton.setImage(#imageLiteral(resourceName: "loadDocumentFinished"), for: .normal)
        }
        
        let iconProgressShouldBeDisplayed = viewModel.iconState.value == .cancelDownload && viewModel.downloadProgress.value > 0
        let iconProgressShouldRefresh = iconProgressView.isHidden && iconProgressShouldBeDisplayed
        
        iconProgressView.isHidden = !iconProgressShouldBeDisplayed
        
        if !iconProgressView.isHidden {
            
            if iconProgressShouldRefresh {
                iconProgressView.prepareForLoading()
                iconProgressView.finishPrepareForLoading()
            }
            
            iconProgressView.setProgress(CGFloat(viewModel.downloadProgress.value))
        }
        
        let textColor = style.textColor(viewModel: viewModel, isSelected: isSelected)
        let iconTintColor = style.iconTintColor(viewModel: viewModel, isSelected: isSelected)
        let fileNameFont = style.fileNameFont(viewModel: viewModel, isSelected: isSelected)
        let fileSizeFont = style.fileSizeFont(viewModel: viewModel, isSelected: isSelected) ?? style.textFont(viewModel: viewModel, isSelected: isSelected)
        
        if iconButton.tintColor != iconTintColor {
            iconButton.tintColor = iconTintColor
            iconProgressView.setLineColor(iconTintColor)
        }
        
        if fileNameLabel.text != viewModel.fileName {
            fileNameLabel.text = viewModel.fileName
        }
        if fileNameLabel.textColor != textColor {
            fileNameLabel.textColor = textColor
        }
        if fileNameLabel.font != fileNameFont {
            fileNameLabel.font = fileNameFont
        }
        
        if fileSizeLabel.text != viewModel.fileSizeDescription {
            fileSizeLabel.text = viewModel.fileSizeDescription
        }
        fileSizeLabel.isHidden = fileSizeLabel.text?.isEmpty ?? true
        
        if fileSizeLabel.textColor != textColor {
            fileSizeLabel.textColor = textColor
        }
        if fileSizeLabel.font != fileSizeFont {
            fileSizeLabel.font = fileSizeFont
        }
    }
    
    override func createLayoutModelContext(preferredMaxLayoutWidth: CGFloat) -> BaseBubbleLayoutModel.LayoutContext? {
        
        guard let style = style as? ChatDocumentBubbleViewStyleProtocol else { return nil }
        guard let viewModel = messageViewModel as? ChatDocumentMessageViewModelProtocol else { return nil }
        
        let model = super.createLayoutModelContext(preferredMaxLayoutWidth: preferredMaxLayoutWidth)
        
        guard let baseModel = model as? ChatTextBubbleLayoutModel.ChatTextLayoutContext else { return nil }
        
        let fileSizeFont = style.fileSizeFont(viewModel: viewModel, isSelected: isSelected) ?? style.textFont(viewModel: viewModel, isSelected: isSelected)
        
        let documentParameters = ChatDocumentBubbleLayoutModel.ChatDocumentLayoutContext.ChatDocumentLayoutParameters(
            fileName: viewModel.fileName,
            fileNameFont: style.fileNameFont(viewModel: viewModel, isSelected: isSelected),
            fileSize: viewModel.fileSizeDescription ?? String(),
            fileSizeFont: fileSizeFont
        )
        
        return ChatDocumentBubbleLayoutModel.ChatDocumentLayoutContext(
            parameters: baseModel.layoutParameters,
            textParameters: baseModel.chatTextLayoutParameters,
            documentParameters: documentParameters
        )
    }
    
    override func layoutSubviews() {
        
        super.layoutSubviews()
        
        guard let documentLayout = calculateBubbleLayout(preferredMaxLayoutWidth: preferredMaxLayoutWidth) as? ChatDocumentBubbleLayoutModel else { return }

        iconButton.bma_rect = documentLayout.iconFrame
        iconProgressView.bma_rect = documentLayout.iconFrame
        
        let cornerRadius = documentLayout.iconFrame.height / 2
        if iconButton.layer.cornerRadius != cornerRadius {
            iconButton.layer.cornerRadius = cornerRadius
        }
        
        fileNameLabel.bma_rect = documentLayout.fileNameFrame
        fileSizeLabel.bma_rect = documentLayout.fileSizeFrame
    }
}

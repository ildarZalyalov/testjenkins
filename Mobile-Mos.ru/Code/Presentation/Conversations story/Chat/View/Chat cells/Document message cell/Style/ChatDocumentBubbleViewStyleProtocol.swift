//
//  ChatDocumentBubbleViewStyleProtocol.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 07.11.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

typealias ChatDocumentMessageCollectionViewCellStyleProtocol = ChatDocumentBubbleViewStyleProtocol

/// Стиль для ChatDocumentBubbleView
protocol ChatDocumentBubbleViewStyleProtocol: ChatTextBubbleViewStyleProtocol {
    
    /// Цвет иконки статуса загрузки документа
    func iconTintColor(viewModel: ChatTextMessageViewModelProtocol, isSelected: Bool) -> UIColor
    
    /// Шрифт для названия документа
    func fileNameFont(viewModel: ChatDocumentMessageViewModelProtocol, isSelected: Bool) -> UIFont
    
    /// Шрифт для описания размера документа
    func fileSizeFont(viewModel: ChatDocumentMessageViewModelProtocol, isSelected: Bool) -> UIFont?
}

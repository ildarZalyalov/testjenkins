//
//  ChatDocumentMessageCellObject.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 17.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Cell object для сообщения с документом
class ChatDocumentMessageCellObject: ChatTextMessageCellObject {
    
    /// Имя файла вместе с расширением
    var fileName: String = String()
    
    /// Размер файла в килобайтах (если есть)
    var fileSize: Float?
    
    /// Состояние загрузки документа
    var downloadState = ChatMessageDocumentDownloadState()
    
    /// Создает копию cell object
    ///
    /// - Returns: копия
    override func copy() -> BaseMessageCellObjectProtocol {
        let copy = ChatDocumentMessageCellObject(messageUid: messageUid, messageModel: _messageModel, text: text)
        copy.fileName = fileName
        copy.fileSize = fileSize
        copy.downloadState = downloadState
        copy.isLastMessage = isLastMessage
        copy.textFormatting = textFormatting
        copy.inlineKeyboard = inlineKeyboard
        copy.inlineOutsideKeyboard = inlineOutsideKeyboard
        copy.optionsKeyboard = optionsKeyboard
        return copy
    }
}

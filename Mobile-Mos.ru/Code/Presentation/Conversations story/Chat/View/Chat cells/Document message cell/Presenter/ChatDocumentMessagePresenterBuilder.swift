//
//  ChatDocumentMessagePresenterBuilder.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 07.11.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import Chatto
import ChattoAdditions

class ChatDocumentMessagePresenterBuilder<ViewModelBuilderT, InteractionHandlerT>:
    ChatTextMessagePresenterBuilder<ViewModelBuilderT, InteractionHandlerT> where
    ViewModelBuilderT: ViewModelBuilderProtocol,
    ViewModelBuilderT.ViewModelT: ChatDocumentMessageViewModelProtocol,
    InteractionHandlerT: BaseInteractionHandlerProtocol,
    InteractionHandlerT.ViewModelT == ViewModelBuilderT.ViewModelT {
    
    lazy var sizingDocumentCell: ChatDocumentMessageCollectionViewCell = {
        
        var cell: ChatDocumentMessageCollectionViewCell? = nil
        
        if Thread.isMainThread {
            cell = ChatDocumentMessageCollectionViewCell.createSizingCell()
        } else {
            DispatchQueue.main.sync(execute: {
                cell = ChatDocumentMessageCollectionViewCell.createSizingCell()
            })
        }
        
        return cell!
    }()
    
    lazy var documentCellStyle: ChatDocumentMessageCollectionViewCellStyleProtocol = ChatDocumentBubbleViewStyleDefault()
    
    override func createPresenterWithChatItem(_ chatItem: ChatItemProtocol) -> ChatItemPresenterProtocol {
        
        assert(canHandleChatItem(chatItem))
        
        return ChatDocumentMessagePresenter<ViewModelBuilderT, InteractionHandlerT>(
            messageModel: chatItem as! ModelT,
            viewModelBuilder: viewModelBuilder,
            interactionHandler: interactionHandler,
            sizingCell: sizingDocumentCell,
            baseCellStyle: baseMessageStyle,
            documentCellStyle: documentCellStyle,
            layoutCache: layoutCache
        )
    }
    
    override var presenterType: ChatItemPresenterProtocol.Type {
        return ChatDocumentMessagePresenter<ViewModelBuilderT, InteractionHandlerT>.self
    }
}

//
//  ChatDocumentCollectionViewCell.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 07.11.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Ячейка для сообщения с документом
class ChatDocumentMessageCollectionViewCell: ChatTextMessageCollectionViewCell, ChatDocumentBubbleViewDelegate {
    
    override class func createSizingCell() -> ChatDocumentMessageCollectionViewCell {
        let cell = ChatDocumentMessageCollectionViewCell(frame: CGRect.zero)
        cell.viewContext = .sizing
        return cell
    }
    
    var onDocumentIconPress: ((_ cell: ChatDocumentMessageCollectionViewCell) -> Void)?
    
    //MARK: - Property forwarding
    
    var documentMessageViewModel: ChatDocumentMessageViewModelProtocol? {
        didSet {
            textMessageViewModel = documentMessageViewModel
        }
    }
    
    var documentMessageStyle: ChatDocumentBubbleViewStyleProtocol? {
        didSet {
            textMessageStyle = documentMessageStyle
        }
    }
    
    //MARK: - Методы
    
    /// Обновить индикатор загрузки документа
    func refreshProgressView() {
        guard let documentBubbleView = bubbleView as? ChatDocumentBubbleView else { return }
        documentBubbleView.refreshProgressView()
    }
    
    //MARK: - Создаем баббл view
    
    override func createBubbleView() -> ChatDocumentBubbleView {
        let view = ChatDocumentBubbleView()
        view.delegate = self
        view.textBubbleDelegate = self
        view.documentBubbleDelegate = self
        return view
    }
    
    //MARK: - ChatDocumentBubbleViewDelegate
    
    func didPressDocumentIcon() {
        onDocumentIconPress?(self)
    }
}

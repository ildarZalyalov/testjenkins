//
//  TypingIndicatorCellObject.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 03.07.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import Chatto

class TypingIndicatorCellObject: ChatItemProtocol {
    
    let uid: String = UUID().uuidString
    let type: ChatItemType = TypingIndicatorCellObject.chatItemType
    
    static var chatItemType: ChatItemType {
        return String(describing: TypingIndicatorCellObject.self)
    }
}

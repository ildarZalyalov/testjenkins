//
//  TypingIndicatorViewModel.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 13.02.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation
import ChattoAdditions

/// View model сообщения с индикатором "вам отвечают"
class TypingIndicatorMessageViewModel: MessageViewModelProtocol {
    
    var decorationAttributes: BaseMessageDecorationAttributes = BaseMessageDecorationAttributes(canShowFailedIcon: false, isShowingTail: true, isShowingAvatar: false, isShowingSelectionIndicator: false, isSelected: false)
    
    let isIncoming: Bool = true
    
    var isUserInteractionEnabled: Bool = false
    
    let isShowingFailedIcon: Bool = false
    
    let date: String = String()
    
    let status: MessageViewModelStatus = .success
    
    var avatarImage: Observable<UIImage?> = Observable(nil)
}

/// View model ячейки с индикатором "вам отвечают"
/// Ячейка отображается как обычное текстовое сообщение с анимацией вместо текста
/// Поэтому удовлетворяет BaseMessageViewModelProtocol и TextMessageViewModelProtocol
class TypingIndicatorTextMessageViewModel: BaseMessageViewModelProtocol, TextMessageViewModelProtocol {
    
    // MARK: BaseMessageViewModelProtocol
    
    let messageUid: String = String(describing: TypingIndicatorCell.self)
    
    let areInteractionGesturesDisabled: Bool = true
    
    let customBubbleColor: UIColor? = nil
    
    let inlineButtons: [[InlineKeyboardButtonViewModel]] = []
    
    let optionButtons: [OptionsKeyboardButtonViewModel] = []
    
    let isShowingExpandButton: Bool = false
    
    //MARK: TextMessageViewModelProtocol
    
    var text: String = String()
    
    var messageViewModel: MessageViewModelProtocol = TypingIndicatorMessageViewModel()
}

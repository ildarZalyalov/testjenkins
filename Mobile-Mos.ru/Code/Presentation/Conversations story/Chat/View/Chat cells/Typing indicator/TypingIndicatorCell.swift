//
//  TypingIndicatorCell.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 03.07.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import UIKit
import ChattoAdditions

class TypingIndicatorCell: UICollectionViewCell {
    
    /// Вьюха с картинкой баббла
    let bubbleImageView = UIImageView()
    
    /// Вьюха с анимацией точек
    let animationView = UIImageView()
    
    /// Отступ вьюхи с анимацией внутри баббла
    let animationViewOffset = CGPoint(x: 26, y: 15)
    
    var textCellStyle: ChatTextMessageCollectionViewCellStyleProtocol? {
        didSet {
            guard textCellStyle != nil else { return }
            updateViews()
        }
    }
    
    var baseStyle: BaseMessageCollectionViewCellStyleProtocol? {
        didSet {
            guard baseStyle != nil else { return }
            updateLayout()
        }
    }
    
    var viewModel: TypingIndicatorTextMessageViewModel? {
        didSet {
            guard viewModel != nil else { return }
            updateViews()
            updateLayout()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    fileprivate func commonInit() {
        
        bubbleImageView.frame = CGRect(x: 0, y: 0, width: 85, height: 39)
        bubbleImageView.autoresizingMask = [.flexibleRightMargin, .flexibleTopMargin]
        contentView.addSubview(bubbleImageView)
        
        animationView.frame = CGRect(x: 0, y: 0, width: 45, height: 10)
        animationView.autoresizingMask = [.flexibleRightMargin, .flexibleTopMargin]
        contentView.addSubview(animationView)
        
        animationView.animationImages = UIImage.imageSequence(with: "dotsIndicator", framesCount: 34)
        animationView.animationDuration = 1.36
        animationView.animationRepeatCount = 0
        
        backgroundColor = UIColor.clear
        contentView.backgroundColor = UIColor.clear
    }
    
    fileprivate func updateViews() {
        guard let style = textCellStyle, let viewModel = viewModel else { return }
        bubbleImageView.image = style.bubbleImage(viewModel: viewModel, isSelected: false)
    }
    
    fileprivate func updateLayout() {
        
        guard let style = baseStyle, let textCellStyle = textCellStyle, let viewModel = viewModel else { return }
        
        let constants = style.layoutConstants(viewModel: viewModel)
        let contentInsets = textCellStyle.contentInsets(viewModel: viewModel, isSelected: false)
        var originX = constants.horizontalMargin + constants.horizontalInterspacing
        var bubbleFrame = bubbleImageView.frame
        var animationFrame = animationView.frame
        
        if viewModel.messageViewModel.decorationAttributes.isShowingTail {
            originX += contentInsets.left
        }
        
        bubbleFrame.origin = CGPoint(x: originX, y: bubbleFrame.origin.y)
        animationFrame.origin = bubbleFrame.origin
        animationFrame = animationFrame.offsetBy(dx: animationViewOffset.x, dy: animationViewOffset.y)
        
        bubbleImageView.frame = bubbleFrame
        animationView.frame = animationFrame
    }
    
    override func apply(_ layoutAttributes: UICollectionViewLayoutAttributes) {
        
        var willHide = layoutAttributes.isHidden
        if !willHide {
            willHide = layoutAttributes.alpha == 0
        }
        
        guard willHide else {
            
            bubbleImageView.isHidden = false
            animationView.isHidden = false
            
            super.apply(layoutAttributes)
            
            return
        }
        
        UIView.performWithoutAnimation {
            bubbleImageView.isHidden = true
            animationView.isHidden = true
        }
    }
}

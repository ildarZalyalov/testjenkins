//
//  TypingIndicatorPresenter.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 03.07.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import Chatto
import ChattoAdditions

class TypingIndicatorPresenter: ChatItemPresenterProtocol {
    
    let typingIndicatorModel: TypingIndicatorCellObject
    let textCellStyle: ChatTextMessageCollectionViewCellStyleProtocol
    let cellStyle: BaseMessageCollectionViewCellStyleProtocol
    
    let viewModel = TypingIndicatorTextMessageViewModel()
    
    init(typingIndicatorModel: TypingIndicatorCellObject,
         textCellStyle: ChatTextMessageCollectionViewCellStyleProtocol,
         cellStyle: BaseMessageCollectionViewCellStyleProtocol) {
        self.typingIndicatorModel = typingIndicatorModel
        self.textCellStyle = textCellStyle
        self.cellStyle = cellStyle
    }
    
    fileprivate static let cellReuseIdentifier = String(describing: TypingIndicatorCell.self)
    
    //MARK: - ChatItemPresenterProtocol
    
    var canCalculateHeightInBackground: Bool {
        return true
    }
    
    static func registerCells(_ collectionView: UICollectionView) {
        collectionView.register(TypingIndicatorCell.self, forCellWithReuseIdentifier: cellReuseIdentifier)
    }
    
    func dequeueCell(collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell {
        return collectionView.dequeueReusableCell(withReuseIdentifier: TypingIndicatorPresenter.cellReuseIdentifier, for: indexPath)
    }
    
    func configureCell(_ cell: UICollectionViewCell, decorationAttributes: ChatItemDecorationAttributesProtocol?) {
        
        guard let cell = cell as? TypingIndicatorCell else {
            assert(false, "Invalid cell received")
            return
        }
        
        if let attributes = decorationAttributes as? ChatItemDecorationAttributes {
            viewModel.messageViewModel.decorationAttributes = attributes.messageDecorationAttributes
        }
        
        cell.baseStyle = cellStyle
        cell.textCellStyle = textCellStyle
        cell.viewModel = viewModel
    }
    
    func heightForCell(maximumWidth width: CGFloat, decorationAttributes: ChatItemDecorationAttributesProtocol?) -> CGFloat {
        return 39
    }
    
    func cellWillBeShown(_ cell: UICollectionViewCell) {
        guard let typingIndicatorCell = cell as? TypingIndicatorCell else { return }
        typingIndicatorCell.animationView.startAnimating()
    }
    
    func cellWasHidden(_ cell: UICollectionViewCell) {
        guard let typingIndicatorCell = cell as? TypingIndicatorCell else { return }
        typingIndicatorCell.animationView.stopAnimating()
    }
}

class TypingIndicatorPresenterBuilder: ChatItemPresenterBuilderProtocol {
    
    let textMessageStyle: ChatTextMessageCollectionViewCellStyleProtocol
    let baseMessageStyle: BaseMessageCollectionViewCellStyleProtocol
    
    init(textCellStyle: ChatTextMessageCollectionViewCellStyleProtocol,
         cellStyle: BaseMessageCollectionViewCellStyleProtocol) {
        textMessageStyle = textCellStyle
        baseMessageStyle = cellStyle
    }
    
    public func canHandleChatItem(_ chatItem: ChatItemProtocol) -> Bool {
        return chatItem is TypingIndicatorCellObject
    }
    
    public func createPresenterWithChatItem(_ chatItem: ChatItemProtocol) -> ChatItemPresenterProtocol {
        precondition(canHandleChatItem(chatItem), "Попытка создать TypingIndicatorPresenter для \(type(of: chatItem))")
        return TypingIndicatorPresenter(typingIndicatorModel: chatItem as! TypingIndicatorCellObject, textCellStyle: textMessageStyle, cellStyle: baseMessageStyle)
    }
    
    public var presenterType: ChatItemPresenterProtocol.Type {
        return TypingIndicatorPresenter.self
    }
}

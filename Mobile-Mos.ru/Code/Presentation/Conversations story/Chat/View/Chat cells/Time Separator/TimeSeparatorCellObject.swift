//
//  TimeSeparatorCellObject.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 03.07.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import UIKit
import Chatto

class TimeSeparatorCellObject: ChatItemProtocol {
    
    let uid: String = UUID().uuidString
    let type: ChatItemType = TimeSeparatorCellObject.chatItemType
    let date: Date

    static var chatItemType: ChatItemType {
        return String(describing: TimeSeparatorCellObject.self)
    }

    init(with date: Date) {
        self.date = date
    }
}

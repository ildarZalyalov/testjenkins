//
//  TypingIndicatorCell.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 03.07.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import UIKit
import Chatto

class TimeSeparatorPresenter: ChatItemPresenterProtocol {

    let timeSeparatorModel: TimeSeparatorCellObject
    let stringBuilder: ChatMessageStringBuilder
    
    init(timeSeparatorModel: TimeSeparatorCellObject, stringBuilder: ChatMessageStringBuilder) {
        self.timeSeparatorModel = timeSeparatorModel
        self.stringBuilder = stringBuilder
    }

    fileprivate static let cellReuseIdentifier = String(describing: TimeSeparatorCell.self)
    
    //MARK: - ChatItemPresenterProtocol
    
    var canCalculateHeightInBackground: Bool {
        return true
    }
    
    static func registerCells(_ collectionView: UICollectionView) {
        collectionView.register(TimeSeparatorCell.self, forCellWithReuseIdentifier: cellReuseIdentifier)
    }
    
    func dequeueCell(collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell {
        return collectionView.dequeueReusableCell(withReuseIdentifier: TimeSeparatorPresenter.cellReuseIdentifier, for: indexPath)
    }
    
    func configureCell(_ cell: UICollectionViewCell, decorationAttributes: ChatItemDecorationAttributesProtocol?) {
        
        guard let timeSeparatorCell = cell as? TimeSeparatorCell else {
            return
        }
        
        timeSeparatorCell.text = stringBuilder.relativeDateString(for: timeSeparatorModel.date)
    }
    
    func heightForCell(maximumWidth width: CGFloat, decorationAttributes: ChatItemDecorationAttributesProtocol?) -> CGFloat {
        return 13
    }
}

public class TimeSeparatorPresenterBuilder: ChatItemPresenterBuilderProtocol {
    
    let stringBuilder: ChatMessageStringBuilder
    
    init(stringBuilder: ChatMessageStringBuilder) {
        self.stringBuilder = stringBuilder
    }
    
    public func canHandleChatItem(_ chatItem: ChatItemProtocol) -> Bool {
        return chatItem is TimeSeparatorCellObject
    }
    
    public func createPresenterWithChatItem(_ chatItem: ChatItemProtocol) -> ChatItemPresenterProtocol {
        precondition(canHandleChatItem(chatItem), "Попытка создать TimeSeparatorPresenter для \(type(of: chatItem))")
        return TimeSeparatorPresenter(timeSeparatorModel: chatItem as! TimeSeparatorCellObject, stringBuilder: stringBuilder)
    }
    
    public var presenterType: ChatItemPresenterProtocol.Type {
        return TimeSeparatorPresenter.self
    }
}


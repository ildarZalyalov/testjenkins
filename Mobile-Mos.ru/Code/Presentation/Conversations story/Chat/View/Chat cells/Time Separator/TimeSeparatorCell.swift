//
//  TimeSeparatorCell.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 03.07.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import UIKit
import Chatto

class TimeSeparatorCell: UICollectionViewCell {

    let fonSize: CGFloat = 11
    fileprivate let label: UILabel = UILabel()

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commonInit()
    }

    fileprivate func commonInit() {
        
        label.font = UIFont(customName: .graphikLCGRegular, size: fonSize) ?? UIFont.systemFont(ofSize: fonSize)
        label.textAlignment = .center
        label.textColor = UIColorPalette.chatMessageTimeSeparatorColor
        
        contentView.addSubview(label)
    }

    var text: String? {
        didSet {
            if oldValue != text {
                setTextOnLabel(text: text)
            }
        }
    }

    fileprivate func setTextOnLabel(text: String?) {
        label.text = text
        setNeedsLayout()
    }

    override func layoutSubviews() {
        
        super.layoutSubviews()
        
        label.bounds.size = label.sizeThatFits(contentView.bounds.size)
        label.center = contentView.center
    }
}

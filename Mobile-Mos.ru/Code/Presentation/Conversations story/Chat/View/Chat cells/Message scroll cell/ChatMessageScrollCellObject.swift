//
//  ChatMessageScrollCellObject.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 21.09.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import Chatto

/// Cell object для ячейки с "каруселью сообщений"
class ChatMessageScrollCellObject: ChatTextMessageCellObject {
    
    // инлайн клавиатура для "главного сообщения" в "карусели сообщений" не обрабатывается
    override var inlineKeyboard: InlineKeyboardCellObject {
        get { return InlineKeyboardCellObject() }
        set { }
    }
    
    // инлайн клавиатура для "главного сообщения" в "карусели сообщений" не обрабатывается
    override var inlineOutsideKeyboard: InlineKeyboardCellObject {
        get { return InlineKeyboardCellObject() }
        set { }
    }
    
    /// Cell object-ы сообщений в "карусели"
    var messagesCellObjects: [BaseMessageCellObjectProtocol] = []
    
    /// Создает копию cell object
    ///
    /// - Returns: копия
    override func copy() -> BaseMessageCellObjectProtocol {
        let copy = ChatMessageScrollCellObject(messageUid: messageUid, messageModel: _messageModel, text: text)
        copy.messagesCellObjects = messagesCellObjects
        copy.isLastMessage = isLastMessage
        copy.textFormatting = textFormatting
        copy.inlineKeyboard = inlineKeyboard
        copy.inlineOutsideKeyboard = inlineOutsideKeyboard
        return copy
    }
}

//
//  ChatMessageScrollPresenter.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 21.09.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import Chatto

/// Презентер ячейки с "каруселью сообщений"
class ChatMessageScrollPresenter: ChatItemPresenterProtocol {
    
    /// cell object для ячейки с "каруселью сообщений"
    let messageScrollModel: ChatMessageScrollCellObject
    
    /// Презентеры сообщений в "карусели"
    let presenters: [ChatItemPresenterProtocol]
    
    /// Обработчик взаимодействия с ячейкой с "каруселью сообщений"
    weak var interactionHandler: ChatMessageScrollInteractionHandler?
    
    /// UIGestureRecognizer, приоритет которого надо понизить по сравнению с жестом прокрутки "карусели"
    weak var gestureToLowerPriorityAgainstScrolling: UIGestureRecognizer?
    
    /// Значение максимальной допустимой ширины для сообщения, для которого закэширована высота "карусели"
    var cachedMaximumWidth: CGFloat = 0
    
    /// Закэшированная высота "карусели"
    var cachedCellHeight: CGFloat = 0
    
    init(messageScrollModel: ChatMessageScrollCellObject, presenterBuilders: [ChatItemType : ChatItemPresenterBuilderProtocol], interactionHandler: ChatMessageScrollInteractionHandler?) {
        self.messageScrollModel = messageScrollModel
        self.presenters = messageScrollModel.messagesCellObjects.compactMap {
            guard let presenterBuilder = presenterBuilders[$0.type] else { return nil }
            guard presenterBuilder.canHandleChatItem($0) else { return nil }
            return presenterBuilder.createPresenterWithChatItem($0)
        }
        self.interactionHandler = interactionHandler
    }
    
    fileprivate static let cellReuseIdentifier = String(describing: ChatMessageScrollCell.self)
    
    //MARK: - ChatItemPresenterProtocol
    
    var canCalculateHeightInBackground: Bool {
        return true
    }
    
    static func registerCells(_ collectionView: UICollectionView) {
        collectionView.register(ChatMessageScrollCell.self, forCellWithReuseIdentifier: cellReuseIdentifier)
    }
    
    func dequeueCell(collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell {
        return collectionView.dequeueReusableCell(withReuseIdentifier: ChatMessageScrollPresenter.cellReuseIdentifier, for: indexPath)
    }
    
    func configureCell(_ cell: UICollectionViewCell, decorationAttributes: ChatItemDecorationAttributesProtocol?) {
        
        guard let messageScrollCell = cell as? ChatMessageScrollCell else {
            return
        }
        
        messageScrollCell.interactionHandler = interactionHandler
        messageScrollCell.messagesBottomMargin = decorationAttributes?.bottomMargin ?? 0
        
        messageScrollCell.updateCell(with: presenters, forMessageWIthUid: messageScrollModel.messageUid)
        
        guard presenters.count > 1 else { return }
        guard let gesture = gestureToLowerPriorityAgainstScrolling else { return }
        messageScrollCell.prioritizeScrolling(to: gesture)
    }
    
    func heightForCell(maximumWidth width: CGFloat, decorationAttributes: ChatItemDecorationAttributesProtocol?) -> CGFloat {
        
        guard cachedMaximumWidth != width else { return cachedCellHeight }
        
        cachedMaximumWidth = width
        cachedCellHeight = presenters.map {
            $0.heightForCell(maximumWidth: width, decorationAttributes: decorationAttributes)
        }.max() ?? 0
        
        return cachedCellHeight
    }
}

/// Билдер презентеров ячеек с "каруселью сообщений"
public class ChatMessageScrollPresenterBuilder: ChatItemPresenterBuilderProtocol {
    
    /// Билдеры презентеров сообщений в "карусели"
    let presenterBuilders: [ChatItemType : ChatItemPresenterBuilderProtocol]
    
    /// Обработчик взаимодействия с ячейкой с "каруселью сообщений"
    weak var interactionHandler: ChatMessageScrollInteractionHandler?
    
    /// UIGestureRecognizer, приоритет которого надо понизить по сравнению с жестом прокрутки "карусели"
    weak var gestureToLowerPriorityAgainstScrolling: UIGestureRecognizer?
    
    init(presenterBuilders: [ChatItemType : ChatItemPresenterBuilderProtocol], interactionHandler: ChatMessageScrollInteractionHandler?) {
        self.presenterBuilders = presenterBuilders
        self.interactionHandler = interactionHandler
    }
    
    public func canHandleChatItem(_ chatItem: ChatItemProtocol) -> Bool {
        return chatItem is ChatMessageScrollCellObject
    }
    
    public func createPresenterWithChatItem(_ chatItem: ChatItemProtocol) -> ChatItemPresenterProtocol {
        
        precondition(canHandleChatItem(chatItem), "Попытка создать MessageScrollPresenter для \(type(of: chatItem))")
        
        let presenter = ChatMessageScrollPresenter(messageScrollModel: chatItem as! ChatMessageScrollCellObject, presenterBuilders: presenterBuilders, interactionHandler: interactionHandler)
        presenter.gestureToLowerPriorityAgainstScrolling = gestureToLowerPriorityAgainstScrolling
        
        return presenter
    }
    
    public var presenterType: ChatItemPresenterProtocol.Type {
        return ChatMessageScrollPresenter.self
    }
}


//
//  ChatMessageScrollInteractionHandler.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 24.09.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Обработчик взаимодействия с ячейкой с "каруселью сообщений"
protocol ChatMessageScrollInteractionHandler: class {
    
    /// Пользователь начал скролл по сообщениям
    func userWillBeginScrollingBubbles()
    
    /// Пользователь окончил скролл по сообщениям
    func userDidEndScrollingBubbles()
}

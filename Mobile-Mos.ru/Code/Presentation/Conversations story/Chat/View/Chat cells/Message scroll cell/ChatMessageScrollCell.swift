//
//  ChatMessageScrollCell.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 21.09.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import UIKit
import Chatto
import ChattoAdditions

/// Ячейка с "каруселью сообщений"
class ChatMessageScrollCell: UICollectionViewCell, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    /// Ячейка, содержащая в себе ячейку с сообщением
    /// (сами ячейки с сообщениями сделаны так, чтобы показываться на всю переданную ширину, а ширину баббла считать отдельно;
    /// данная ячейка позволяет сохранить ширину баббла такой, какая она была бы при доступной ширине в весь экран:
    /// мы говорим ячейке, что у нее есть весь экран, а потом просто кладем ее в другую ячейку шириной в баббл) 🙈
    fileprivate class MessageHolderCell: UICollectionViewCell {
        
        var messageCell: UICollectionViewCell!
        
        func configure(with presenter: ChatItemPresenterProtocol,
                       decorationAttributes: ChatItemDecorationAttributes,
                       collectionView: UICollectionView,
                       indexPath: IndexPath) {
            
            if messageCell == nil {
                messageCell = presenter.dequeueCell(collectionView: collectionView, indexPath: indexPath)
                contentView.addSubview(messageCell)
                contentView.isUserInteractionEnabled = true
            }
            
            presenter.configureCell(messageCell, decorationAttributes: decorationAttributes)
            
            if let fixedHeightCell = messageCell as? FixedLayoutHeightSpecificable {
                fixedHeightCell.fixedLayoutHeight = collectionView.bounds.height
                messageCell.setNeedsLayout()
            }
            
            let width = collectionView.bounds.width
            let messageSize = messageCell.sizeThatFits(CGSize(width: width, height: .greatestFiniteMagnitude))
            
            messageCell.frame = CGRect(origin: CGPoint.zero, size: messageSize)
        }
    }
    
    /// UICollectionViewFlowLayout, который обеспечивает "примагничивание" к бабблам сообщений слева при скроллинге
    fileprivate class MessagePagingFlowLayout: UICollectionViewFlowLayout {
        
        override func targetContentOffset(forProposedContentOffset proposedContentOffset: CGPoint, withScrollingVelocity velocity: CGPoint) -> CGPoint {
            
            guard let layoutCollectionView = collectionView else { return proposedContentOffset }
            
            let targetRect = CGRect(x: proposedContentOffset.x,
                                    y: 0,
                                    width: layoutCollectionView.bounds.width,
                                    height: layoutCollectionView.bounds.height)
            
            guard let attributes = layoutAttributesForElements(in: targetRect) else { return proposedContentOffset }
            
            var offsetAdjustment = CGFloat.greatestFiniteMagnitude
            let horizontalOffset = proposedContentOffset.x
            
            for attribute in attributes {
                let itemOffset = attribute.frame.origin.x
                if abs(itemOffset - horizontalOffset) < abs(offsetAdjustment) {
                    offsetAdjustment = itemOffset - horizontalOffset
                }
            }
            
            return proposedContentOffset.bma_offsetBy(dx: offsetAdjustment, dy: 0)
        }
    }
    
    /// UICollectionView, которая может отменять жесты любых дочерних вьюх, когда обнаруживает, что ее скроллят
    fileprivate class MessagePagingCollectionView: UICollectionView {
        
        override func touchesShouldCancel(in view: UIView) -> Bool {
            return true
        }
    }
    
    fileprivate var collectionView: MessagePagingCollectionView!

    /// Расстояние между сообщениями
    fileprivate let betweenMessagesSpacing: CGFloat = 0
    
    /// Отступ снизу от сообщений
    var messagesBottomMargin: CGFloat = 0
    
    /// Атрибуты сообщений в "карусели"
    fileprivate lazy var messagesAttributes = ChatItemDecorationAttributes(bottomMargin: self.messagesBottomMargin, messageDecorationAttributes: BaseMessageDecorationAttributes(canShowFailedIcon: false))
    
    /// Идентификатор текущего сообщения с "каруселью", отображаемого в ячейке
    fileprivate var messageScrollUid: String?
    
    /// Презентеры сообщений в "карусели"
    fileprivate var presenters: [ChatItemPresenterProtocol] = []
    
    /// Обработчик взаимодействия с ячейкой с "каруселью сообщений"
    weak var interactionHandler: ChatMessageScrollInteractionHandler?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commonInit()
    }
    
    fileprivate func commonInit() {
        
        let layout = MessagePagingFlowLayout()
        layout.scrollDirection = .horizontal
        layout.sectionInset = UIEdgeInsets.zero
        layout.minimumLineSpacing = betweenMessagesSpacing
        layout.minimumInteritemSpacing = betweenMessagesSpacing
        
        collectionView = MessagePagingCollectionView(frame: contentView.bounds, collectionViewLayout: layout)
        collectionView.backgroundColor = UIColor.clear
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.clipsToBounds = false
        collectionView.delaysContentTouches = false
        collectionView.canCancelContentTouches = true
        collectionView.dataSource = self
        collectionView.delegate = self
        
        if #available(iOS 11.0, *) {
            collectionView.contentInsetAdjustmentBehavior = .never
        }
        
        contentView.addSubview(collectionView)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        collectionView.frame = contentView.bounds
    }
    
    fileprivate func cellIdentifier(for presenter: ChatItemPresenterProtocol, at index: Int) -> String {
        return String(describing: type(of: presenter)) + String(index)
    }
    
    // MARK: - Открытые методы
    
    /// Приоритизировать прокрутку "карусели" над другим жестом
    ///
    /// - Parameter gestureRecognizer: другой жест
    func prioritizeScrolling(to gestureRecognizer: UIGestureRecognizer) {
        gestureRecognizer.require(toFail: collectionView.panGestureRecognizer)
    }
    
    /// Обновить ячейку
    ///
    /// - Parameters:
    ///   - presenters: презентеры сообщений в "карусели"
    ///   - messageUid: идентификатор сообщения с "каруселью"
    func updateCell(with presenters: [ChatItemPresenterProtocol], forMessageWIthUid messageUid: String) {
        
        self.presenters = presenters
        
        for (index, presenter) in presenters.enumerated() {
            type(of: presenter).registerCells(collectionView)
            collectionView.register(MessageHolderCell.self,
                                    forCellWithReuseIdentifier: cellIdentifier(for: presenter, at: index))
        }
        
        collectionView.reloadData()
        collectionView.collectionViewLayout.invalidateLayout()
        collectionView.setNeedsLayout()
        collectionView.isScrollEnabled = presenters.count > 1
        
        if messageUid != messageScrollUid {
            collectionView.setContentOffset(CGPoint.zero, animated: false)
        }
        
        messageScrollUid = messageUid
    }
    
    //MARK: - UICollectionViewDataSource
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenters.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let index = indexPath.row
        let presenter = presenters[index]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier(for: presenter, at: index), for: indexPath)
        
        guard let messageHolderCell = cell as? MessageHolderCell else { return cell }
        
        messageHolderCell.configure(with: presenter,
                                    decorationAttributes: messagesAttributes,
                                    collectionView: collectionView,
                                    indexPath: indexPath)
        
        return cell
    }
    
    //MARK: - UICollectionViewDelegateFlowLayout
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard indexPath.row < presenters.count else { return }
        let presenter = presenters[indexPath.row]
        guard let messageHolderCell = cell as? MessageHolderCell else { return }
        presenter.cellWillBeShown(messageHolderCell.messageCell)
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard indexPath.row < presenters.count else { return }
        let presenter = presenters[indexPath.row]
        guard let messageHolderCell = cell as? MessageHolderCell else { return }
        presenter.cellWasHidden(messageHolderCell.messageCell)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let presenter = presenters[indexPath.row]
        
        var width = collectionView.bounds.width
        if let basePresenter = presenter as? BaseMessagePresenterProtocol, indexPath.row < presenters.count - 1 {
            width = basePresenter.widthForBubbleInCell(maximumWidth: width, decorationAttributes: messagesAttributes)
        }
        
        let height = collectionView.bounds.height
        
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
        //почему здесь и ниже так? потому, что вместо IndexPath из UIKit и Obj-C иногда приходит null на самом деле 💩 - https://openradar.appspot.com/31375101
        guard !indexPath.isEmpty else { return false }
        let presenter = presenters[indexPath.row]
        return presenter.shouldShowMenu()
    }
    
    func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        guard !indexPath.isEmpty else { return false }
        let presenter = presenters[indexPath.row]
        return presenter.canPerformMenuControllerAction(action)
    }
    
    func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
        guard !indexPath.isEmpty else { return }
        let presenter = presenters[indexPath.row]
        presenter.performMenuControllerAction(action)
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        UIMenuController.shared.setMenuVisible(false, animated: false)
        interactionHandler?.userWillBeginScrollingBubbles()
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        interactionHandler?.userDidEndScrollingBubbles()
    }
}

//
//  ChatPhotoMessageCollectionViewCell.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 01.02.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Ячейка для сообщения с первью фото
class ChatPhotoMessageCollectionViewCell: ChatTextMessageCollectionViewCell, ChatPhotoBubbleViewDelegate {
    
    override class func createSizingCell() -> ChatPhotoMessageCollectionViewCell {
        let cell = ChatPhotoMessageCollectionViewCell(frame: CGRect.zero)
        cell.viewContext = .sizing
        return cell
    }
    
    var onPhotoPress: ((_ cell: ChatPhotoMessageCollectionViewCell) -> Void)?
    
    //MARK: - Property forwarding
    
    var photoMessageViewModel: ChatPhotoMessageViewModelProtocol? {
        didSet {
            textMessageViewModel = photoMessageViewModel
        }
    }
    
    var photoMessageStyle: ChatPhotoBubbleViewStyleProtocol? {
        didSet {
            textMessageStyle = photoMessageStyle
        }
    }
    
    //MARK: Создаем баббл view
    
    override func createBubbleView() -> ChatPhotoBubbleView {
        let view = ChatPhotoBubbleView()
        view.delegate = self
        view.textBubbleDelegate = self
        view.photoBubbleDelegate = self
        return view
    }
    
    //MARK: - ChatPhotoBubbleViewDelegate
    
    func didPressPhoto() {
        onPhotoPress?(self)
    }
}

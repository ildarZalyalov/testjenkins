//
//  ChatPhotoMessagePresenter.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 01.02.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation
import ChattoAdditions

class ChatPhotoMessagePresenter<ViewModelBuilderT, InteractionHandlerT>:
    ChatTextMessagePresenter<ViewModelBuilderT, InteractionHandlerT> where
    ViewModelBuilderT: ViewModelBuilderProtocol,
    ViewModelBuilderT.ViewModelT: ChatPhotoMessageViewModelProtocol,
    InteractionHandlerT: BaseInteractionHandlerProtocol,
    InteractionHandlerT.ViewModelT == ViewModelBuilderT.ViewModelT {
    
    public init(
        messageModel: ModelT,
        viewModelBuilder: ViewModelBuilderT,
        interactionHandler: InteractionHandlerT?,
        sizingCell: ChatPhotoMessageCollectionViewCell,
        baseCellStyle: BaseMessageCollectionViewCellStyleProtocol,
        photoCellStyle: ChatPhotoMessageCollectionViewCellStyleProtocol,
        layoutCache: NSCache<AnyObject, AnyObject>) {
        self.photoCellStyle = photoCellStyle
        super.init(
            messageModel: messageModel,
            viewModelBuilder: viewModelBuilder,
            interactionHandler: interactionHandler,
            sizingCell: sizingCell,
            baseCellStyle: baseCellStyle,
            textCellStyle: photoCellStyle,
            layoutCache: layoutCache)
    }
    
    let photoCellStyle: ChatPhotoMessageCollectionViewCellStyleProtocol
    
    override class func registerCells(_ collectionView: UICollectionView) {
        collectionView.register(ChatPhotoMessageCollectionViewCell.self, forCellWithReuseIdentifier: "chat-photo-message-incoming")
        collectionView.register(ChatPhotoMessageCollectionViewCell.self, forCellWithReuseIdentifier: "chat-photo-message-outcoming")
    }
    
    override func dequeueCell(collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell {
        let identifier = messageViewModel.isIncoming ? "chat-photo-message-incoming" : "chat-photo-message-outcoming"
        return collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath)
    }
    
    public var photoCell: ChatPhotoMessageCollectionViewCell? {
        
        if let cell = self.cell {
            if let photoCell = cell as? ChatPhotoMessageCollectionViewCell {
                return photoCell
            } else {
                assert(false, "Invalid cell was given to presenter!")
            }
        }
        
        return nil
    }
    
    override func configureCell(_ cell: BaseMessageCollectionViewCell<ChatTextBubbleView>, decorationAttributes: ChatItemDecorationAttributes, animated: Bool, additionalConfiguration: (() -> Void)?) {
        
        guard let cell = cell as? ChatPhotoMessageCollectionViewCell else {
            assert(false, "Invalid cell received")
            return
        }
        
        super.configureCell(cell, decorationAttributes: decorationAttributes, animated: animated) { () -> Void in
            
            cell.photoMessageViewModel = self.messageViewModel
            cell.photoMessageStyle = self.photoCellStyle
            
            cell.onPhotoPress = { [weak self] cell in
                guard let strongSelf = self else { return }
                strongSelf.onPhotoPress(in: cell)
            }
            
            additionalConfiguration?()
        }
    }
    
    override func updateCurrentCell() {
        if let cell = photoCell, let decorationAttributes = decorationAttributes {
            configureCell(cell, decorationAttributes: decorationAttributes, animated: itemVisibility != .appearing, additionalConfiguration: nil)
        }
    }
    
    func onPhotoPress(in cell: ChatPhotoMessageCollectionViewCell) {
        
        guard let photoMessageViewModel = cell.photoMessageViewModel else {
            return
        }
        guard let photoInteractionHandler = interactionHandler as? ChatPhotoMessageHandler else {
            return
        }
        
        photoInteractionHandler.userDidPressOnPhoto(viewModel: photoMessageViewModel)
    }
}

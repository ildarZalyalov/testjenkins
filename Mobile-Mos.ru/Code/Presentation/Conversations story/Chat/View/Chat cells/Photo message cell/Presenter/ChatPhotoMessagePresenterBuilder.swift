//
//  ChatPhotoMessagePresenterBuilder.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 01.02.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation
import Chatto
import ChattoAdditions

class ChatPhotoMessagePresenterBuilder<ViewModelBuilderT, InteractionHandlerT>:
    ChatTextMessagePresenterBuilder<ViewModelBuilderT, InteractionHandlerT> where
    ViewModelBuilderT: ViewModelBuilderProtocol,
    ViewModelBuilderT.ViewModelT: ChatPhotoMessageViewModelProtocol,
    InteractionHandlerT: BaseInteractionHandlerProtocol,
    InteractionHandlerT.ViewModelT == ViewModelBuilderT.ViewModelT {
    
    lazy var sizingPhotoCell: ChatPhotoMessageCollectionViewCell = {
        
        var cell: ChatPhotoMessageCollectionViewCell? = nil
        
        if Thread.isMainThread {
            cell = ChatPhotoMessageCollectionViewCell.createSizingCell()
        } else {
            DispatchQueue.main.sync(execute: {
                cell = ChatPhotoMessageCollectionViewCell.createSizingCell()
            })
        }
        
        return cell!
    }()
    
    lazy var photoCellStyle: ChatPhotoMessageCollectionViewCellStyleProtocol = ChatPhotoBubbleViewStyleDefault()
    
    override func createPresenterWithChatItem(_ chatItem: ChatItemProtocol) -> ChatItemPresenterProtocol {
        
        assert(canHandleChatItem(chatItem))
        
        let presenter = ChatPhotoMessagePresenter<ViewModelBuilderT, InteractionHandlerT>(
            messageModel: chatItem as! ModelT,
            viewModelBuilder: viewModelBuilder,
            interactionHandler: interactionHandler,
            sizingCell: sizingPhotoCell,
            baseCellStyle: baseMessageStyle,
            photoCellStyle: photoCellStyle,
            layoutCache: layoutCache
        )
        
        return presenter
    }
    
    override var presenterType: ChatItemPresenterProtocol.Type {
        return ChatPhotoMessagePresenter<ViewModelBuilderT, InteractionHandlerT>.self
    }
}

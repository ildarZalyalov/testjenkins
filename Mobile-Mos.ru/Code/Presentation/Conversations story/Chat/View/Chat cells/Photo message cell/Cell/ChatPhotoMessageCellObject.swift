//
//  ChatPhotoMessageCellObject.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 01.02.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

// Cell object для сообщения с фото
class ChatPhotoMessageCellObject: ChatTextMessageCellObject {
    
    /// Ссылка на фото
    var photoUrl: URL = URL(fileURLWithPath: String())
    
    /// Размер фото
    var photoSize: CGSize?
    
    /// Создает копию cell object
    ///
    /// - Returns: копия
    override func copy() -> BaseMessageCellObjectProtocol {
        let copy = ChatPhotoMessageCellObject(messageUid: messageUid, messageModel: _messageModel, text: text)
        copy.photoUrl = photoUrl
        copy.photoSize = photoSize
        copy.isLastMessage = isLastMessage
        copy.textFormatting = textFormatting
        copy.inlineKeyboard = inlineKeyboard
        copy.inlineOutsideKeyboard = inlineOutsideKeyboard
        copy.optionsKeyboard = optionsKeyboard
        return copy
    }
}

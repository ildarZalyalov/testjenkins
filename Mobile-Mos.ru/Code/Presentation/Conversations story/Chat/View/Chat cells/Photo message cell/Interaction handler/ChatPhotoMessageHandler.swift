//
//  ChatPhotoMessageHandler.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 01.02.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation
import ChattoAdditions

/// Обработчик действий пользователя для сообщений с фото
class ChatPhotoMessageHandler: BaseInteractionHandlerProtocol {
    
    weak var baseHandler: BaseChatMessageHandler?
    
    init(baseHandler: BaseChatMessageHandler) {
        self.baseHandler = baseHandler
    }
    
    func userDidTapOnFailIcon(viewModel: ChatPhotoMessageViewModel, failIconView: UIView) {
        baseHandler?.userDidTapOnFailIcon(viewModel: viewModel, failIconView: failIconView)
    }
    
    func userDidTapOnAvatar(viewModel: ChatPhotoMessageViewModel) {
        baseHandler?.userDidTapOnAvatar(viewModel: viewModel)
    }
    
    func userDidTapOnBubble(viewModel: ChatPhotoMessageViewModel) {
        baseHandler?.userDidTapOnBubble(viewModel: viewModel)
    }
    
    func userDidBeginLongPressOnBubble(viewModel: ChatPhotoMessageViewModel) {
        baseHandler?.userDidBeginLongPressOnBubble(viewModel: viewModel)
    }
    
    func userDidEndLongPressOnBubble(viewModel: ChatPhotoMessageViewModel) {
        baseHandler?.userDidEndLongPressOnBubble(viewModel: viewModel)
    }
    
    func userDidSelectMessage(viewModel: ChatPhotoMessageViewModel) {
        baseHandler?.userDidSelectMessage(viewModel: viewModel)
    }
    
    func userDidDeselectMessage(viewModel: ChatPhotoMessageViewModel) {
        baseHandler?.userDidDeselectMessage(viewModel: viewModel)
    }
    
    func userDidPressInlineKeyboardButton(viewModel: BaseMessageViewModelProtocol, with buttonId: String) {
        baseHandler?.userDidPressInlineKeyboardButton(viewModel: viewModel, with: buttonId)
    }
    
    func userDidPressExpandInlineKeyboardButton(viewModel: BaseMessageViewModelProtocol) {
        baseHandler?.userDidPressExpandInlineKeyboardButton(viewModel: viewModel)
    }
    
    func userDidPressToOpen(url: URL, inMessage viewModel: BaseMessageViewModelProtocol) {
        baseHandler?.userDidPressToOpen(url: url, inMessage: viewModel)
    }
    
    func userDidPressOptionButton(viewModel: BaseMessageViewModelProtocol, with buttonId: String) {
        baseHandler?.userDidPressOptionButton(viewModel: viewModel, with: buttonId)
    }
    
    func userDidPressOnPhoto(viewModel: ChatPhotoMessageViewModelProtocol) {
        baseHandler?.userDidPressOnPhoto(viewModel: viewModel)
    }
}

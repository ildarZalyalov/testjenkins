//
//  ChatPhotoBubbleViewStyleProtocol.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 01.02.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

typealias ChatPhotoMessageCollectionViewCellStyleProtocol = ChatPhotoBubbleViewStyleProtocol

/// Стиль для ChatPhotoBubbleView
protocol ChatPhotoBubbleViewStyleProtocol: ChatTextBubbleViewStyleProtocol {
    
    /// Размер фото на превью в ячейке
    func sizeForPhoto(viewModel: ChatPhotoMessageViewModelProtocol, isSelected: Bool) -> CGSize
}

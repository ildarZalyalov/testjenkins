//
//  ChatPhotoMessageViewModel.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 01.02.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation
import ChattoAdditions

/// Протокол view model для ячейки сообщения с фото
protocol ChatPhotoMessageViewModelProtocol: ChatTextMessageViewModelProtocol  {
    
    /// Ссылка на фото
    var photoUrl: URL { get }
    
    /// Является ли фото вертикальным
    var isPhotoVertical: Bool { get }
}

/// View model сообщения с фото
class ChatPhotoMessageViewModel: ChatTextMessageViewModel, ChatPhotoMessageViewModelProtocol {
    
    /// Ссылка на фото
    let photoUrl: URL
    
    /// Является ли фото вертикальным
    let isPhotoVertical: Bool
    
    init(textModel: ChatTextMessageViewModel, photoUrl: URL, isPhotoVertical: Bool) {
        self.photoUrl = photoUrl
        self.isPhotoVertical = isPhotoVertical
        super.init(messageUid: textModel.messageUid,
                   textMessage: textModel.textMessage,
                   messageViewModel: textModel.messageViewModel,
                   inlineButtons: textModel.inlineButtons,
                   optionButtons: textModel.optionButtons,
                   isShowingExpandButton: textModel.isShowingExpandButton,
                   formattedText: textModel.formattedText)
    }
}

/// Билдер view model сообщения с фото
class ChatPhotoMessageViewModelBuilder: ViewModelBuilderProtocol {
    
    let textMessageViewModelBuilder: ChatTextMessageViewModelBuilder
    
    let largestVerticalAspectRatio: Double = 4 / 5
    
    init(style: ChatTextMessageCollectionViewCellStyleProtocol, formattingExtractor: ChatTextFormattingExtractor) {
        textMessageViewModelBuilder = ChatTextMessageViewModelBuilder(style: style, formattingExtractor: formattingExtractor)
    }
    
    func createViewModel(_ photoMessage: ChatPhotoMessageCellObject) -> ChatPhotoMessageViewModel {
        
        let textViewModel = textMessageViewModelBuilder.createViewModel(photoMessage)
        var isPhotoVertical = false
        if let size = photoMessage.photoSize {
            isPhotoVertical = Double(size.width / size.height) < largestVerticalAspectRatio
        }
        
        let viewModel = ChatPhotoMessageViewModel(textModel: textViewModel, photoUrl: photoMessage.photoUrl, isPhotoVertical: isPhotoVertical)
        
        return viewModel
    }
    
    func canCreateViewModel(fromModel model: Any) -> Bool {
        return model is ChatPhotoMessageCellObject
    }
}

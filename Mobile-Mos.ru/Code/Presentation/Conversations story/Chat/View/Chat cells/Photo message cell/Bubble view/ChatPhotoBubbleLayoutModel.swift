//
//  ChatPhotoBubbleLayoutModel.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 01.02.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Модель лейаута для ChatPhotoBubbleView. Умеет расчитывать размер ChatPhotoBubbleView при заданных параметрах, а также кеширует его
class ChatPhotoBubbleLayoutModel: ChatTextBubbleLayoutModel {
    
    /// Содержит входные данные для расчета, а также служит ключем хэширования
    class ChatPhotoLayoutContext: ChatTextBubbleLayoutModel.ChatTextLayoutContext {
        
        struct ChatPhotoLayoutParameters {
            
            /// Размер фото
            let photoSize: CGSize
        }
        
        let chatPhotoLayoutParameters: ChatPhotoLayoutParameters
        
        override var bubbleLayoutType: BaseBubbleLayoutModel.Type {
            return ChatPhotoBubbleLayoutModel.self
        }
        
        init(parameters: LayoutParameters,
             textParameters: ChatTextLayoutParameters,
             photoParameters: ChatPhotoLayoutParameters) {
            self.chatPhotoLayoutParameters = photoParameters
            super.init(parameters: parameters, textParameters: textParameters)
        }
        
        override var hashValue: Int {
            
            let params = chatPhotoLayoutParameters
            let hashes = [super.hashValue,
                          params.photoSize.combinedHashValue]
            
            return hashes.combinedHashValue
        }
        
        static func == (lhs: ChatPhotoLayoutContext, rhs: ChatPhotoLayoutContext) -> Bool {
            
            if lhs === rhs { return true }
            
            typealias BaseContext = ChatTextBubbleLayoutModel.ChatTextLayoutContext
            let baseEqual = lhs as BaseContext == rhs as BaseContext
            
            let lhsParams = lhs.chatPhotoLayoutParameters
            let rhsParams = rhs.chatPhotoLayoutParameters
            
            return lhsParams.photoSize.equalTo(rhsParams.photoSize) && baseEqual
        }
    }
    
    
    /// Фрейм фото
    var photoFrame: CGRect = CGRect.zero
    
    /// Размер всего превью с фото
    var photoPreviewSize: CGSize = CGSize.zero
    
    /// Посчитать размер контента баббла
    override func calculateIntrinsicContentSize() -> CGSize {
        
        var contentSize = super.calculateIntrinsicContentSize()
        
        guard let photoLayoutContext = layoutContext as? ChatPhotoLayoutContext else { return contentSize }
        let layoutParameters = photoLayoutContext.chatPhotoLayoutParameters
        
        photoPreviewSize = layoutParameters.photoSize
        
        contentSize.width = max(contentSize.width, photoPreviewSize.width)
        contentSize.height += photoPreviewSize.height
        
        return contentSize
    }
    
    /// Пересчитать кеш лейаута контента баббла
    override func calculateContentLayout(for contentSize: CGSize) {
        
        super.calculateContentLayout(for: contentSize)
        
        guard let photoLayoutContext = layoutContext as? ChatPhotoLayoutContext else { return }
        let layoutParameters = photoLayoutContext.chatPhotoLayoutParameters
        
        photoFrame = CGRect(x: 0, y: textSize.height, width: contentSize.width, height: layoutParameters.photoSize.height)
    }
}

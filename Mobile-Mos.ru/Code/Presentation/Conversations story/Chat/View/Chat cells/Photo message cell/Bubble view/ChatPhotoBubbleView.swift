//
//  ChatPhotoBubbleView.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 01.02.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation
import FLAnimatedImage

/// Делегат баббла для ChatMessage, у которого тип контента - фото
protocol ChatPhotoBubbleViewDelegate: class {
    
    /// Пользователь нажал на фото в сообщении
    func didPressPhoto()
}

/// View для баббла для ChatMessage, у которого тип контента - фото.
class ChatPhotoBubbleView: ChatTextBubbleView {
    
    /// Текущая ссылка на фото
    fileprivate var currentPhotoUrl: URL?
    
    /// View для фото
    private var photoView: FLAnimatedImageView = {
        
        let photoView = FLAnimatedImageView()
        photoView.contentMode = .scaleAspectFill
        photoView.clipsToBounds = true
        photoView.isUserInteractionEnabled = true
        
        return photoView
    }()
    
    /// View c иконкой фото
    private var iconView: UIImageView = {
        
        let iconView = UIImageView(image: #imageLiteral(resourceName: "panorama-ico"))
        iconView.tintColor = UIColor.black
        
        return iconView
    }()
    
    /// Кнопка повторной загрузки
    private var retryButton: UIButton = {
        
        let retryButton = UIButton(type: .system)
        retryButton.tintColor = UIColor.black
        retryButton.isHidden = true
        retryButton.setImage(#imageLiteral(resourceName: "loadDocumentStart"), for: .normal)
        
        return retryButton
    }()
    
    weak var photoBubbleDelegate: ChatPhotoBubbleViewDelegate?
    
    @objc
    func didPressPhoto(recognizer: UIGestureRecognizer) {
        guard photoView.image != nil else { return }
        photoBubbleDelegate?.didPressPhoto()
    }
    
    @objc
    func tryLoadingPhoto() {
        
        guard let viewModel = messageViewModel as? ChatPhotoMessageViewModelProtocol else { return }
        
        retryButton.isHidden = true
        iconView.isHidden = false
        iconView.startLoadingAnimation()
        
        photoView.image = nil
        photoView.animatedImage = nil
        
        photoView.loadAndDisplayImageWithGifSupport(from: viewModel.photoUrl, placeholder: nil, with: nil) { [weak self] result in
            
            guard let strongSelf = self else { return }
            
            strongSelf.iconView.stopLoadingAnimation()
            strongSelf.iconView.isHidden = true
            
            switch result {
            case .success:
                return
            case .failure:
                strongSelf.retryButton.isHidden = false
                return
            }
        }
    }
    
    override func initializeContentSubviews(contentParentView: UIView) {
        
        super.initializeContentSubviews(contentParentView: contentParentView)
        contentParentView.addSubview(photoView)
        contentParentView.addSubview(iconView)
        contentParentView.addSubview(retryButton)
        
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(didPressPhoto(recognizer:)))
        photoView.addGestureRecognizer(tapRecognizer)
        
        retryButton.addTarget(self, action: #selector(tryLoadingPhoto), for: .touchUpInside)
    }
    
    override func updateContentViews() {
        
        super.updateContentViews()
        
        guard let style = style as? ChatPhotoBubbleViewStyleProtocol else { return }
        guard let viewModel = messageViewModel as? ChatPhotoMessageViewModelProtocol else { return }
        
        retryButton.tintColor = style.textColor(viewModel: viewModel, isSelected: isSelected)
        iconView.tintColor = style.textColor(viewModel: viewModel, isSelected: isSelected)
        
        if photoView.image != nil {
            guard currentPhotoUrl != viewModel.photoUrl else { return }
        }
        
        currentPhotoUrl = viewModel.photoUrl
        tryLoadingPhoto()
    }
    
    override func createLayoutModelContext(preferredMaxLayoutWidth: CGFloat) -> BaseBubbleLayoutModel.LayoutContext? {
        
        guard let style = style as? ChatPhotoBubbleViewStyleProtocol else { return nil }
        guard let viewModel = messageViewModel as? ChatPhotoMessageViewModelProtocol else { return nil }
        
        let model = super.createLayoutModelContext(preferredMaxLayoutWidth: preferredMaxLayoutWidth)
        
        guard let baseModel = model as? ChatTextBubbleLayoutModel.ChatTextLayoutContext else { return nil }
        
        let photoParameters = ChatPhotoBubbleLayoutModel.ChatPhotoLayoutContext.ChatPhotoLayoutParameters(
            photoSize: style.sizeForPhoto(viewModel: viewModel, isSelected: isSelected))
        
        return ChatPhotoBubbleLayoutModel.ChatPhotoLayoutContext(
            parameters: baseModel.layoutParameters,
            textParameters: baseModel.chatTextLayoutParameters,
            photoParameters: photoParameters
        )
    }
    
    override func layoutSubviews() {
        
        super.layoutSubviews()
        
        guard let photoLayout = calculateBubbleLayout(preferredMaxLayoutWidth: preferredMaxLayoutWidth) as? ChatPhotoBubbleLayoutModel else { return }
        
        photoView.bma_rect = photoLayout.photoFrame
        retryButton.bma_rect = photoLayout.photoFrame
        iconView.center = CGPoint(x: photoView.frame.midX, y: photoView.frame.midY)
    }
}

//
//  ChatExternalLinkBubbleViewStyleProtocol.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 13.09.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

typealias ChatExternalLinkMessageCollectionViewCellStyleProtocol = ChatExternalLinkBubbleViewStyleProtocol

/// Стиль для ChatExternalLinkBubbleView
protocol ChatExternalLinkBubbleViewStyleProtocol: ChatTextBubbleViewStyleProtocol {
    
    /// Ширина баббла
    func bubbleWidth(viewModel: ChatExternalLinkMessageViewModelProtocol, isSelected: Bool) -> CGFloat
    
    /// Высота фото (если оно есть)
    func photoHeight(viewModel: ChatExternalLinkMessageViewModelProtocol, isSelected: Bool) -> CGFloat
}

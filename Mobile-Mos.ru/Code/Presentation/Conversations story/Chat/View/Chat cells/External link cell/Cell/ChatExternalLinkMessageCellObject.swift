//
//  ChatExternalLinkMessageCellObject.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 13.09.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import ChattoAdditions

/// Cell object для сообщения со ссылкой на внешний ресурс
class ChatExternalLinkMessageCellObject: ChatTextMessageCellObject {
    
    /// Превью (если есть)
    var preview: ChatMessageExternalResourcePreview?
    
    /// Источник (если есть)
    var source: String?
    
    /// Ссылка на внешний ресурс (для копирования в буфер обмена)
    var urlString: String?
    
    /// Создает копию cell object
    ///
    /// - Returns: копия
    override func copy() -> BaseMessageCellObjectProtocol {
        let copy = ChatExternalLinkMessageCellObject(messageUid: messageUid, messageModel: _messageModel, text: text)
        copy.preview = preview
        copy.source = source
        copy.urlString = urlString
        copy.isLastMessage = isLastMessage
        copy.textFormatting = textFormatting
        copy.inlineKeyboard = inlineKeyboard
        copy.inlineOutsideKeyboard = inlineOutsideKeyboard
        copy.optionsKeyboard = optionsKeyboard
        return copy
    }
}

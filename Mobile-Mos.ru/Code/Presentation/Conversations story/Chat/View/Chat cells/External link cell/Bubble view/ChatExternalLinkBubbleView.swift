//
//  ChatExternalLinkBubbleView.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 13.09.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// View для баббла для ChatMessage, у которого тип контента - веб-ссылка или пост в соц сети.
class ChatExternalLinkBubbleView: ChatTextBubbleView {
    
    /// View для фото
    private var photoView: UIImageView = {
        
        let photoView = UIImageView()
        photoView.contentMode = .scaleAspectFill
        photoView.clipsToBounds = true
        
        return photoView
    }()
    
    /// Label для заголовка
    private var titleLabel: UILabel = {
        
        let titleLabel = UILabel()
        titleLabel.numberOfLines = 0
        
        return titleLabel
    }()
    
    /// Расстояние между строками в заголовке
    private let titleLineSpacing: CGFloat = 7
    
    /// Заголовок
    private var titleAttributedString: NSAttributedString {
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .left
        paragraphStyle.lineSpacing = titleLineSpacing
        
        let viewModel = messageViewModel as? ChatExternalLinkMessageViewModelProtocol
        let attributes: [NSAttributedStringKey : Any] = [
            NSAttributedStringKey.font : UIFont(customName: .graphikLCGSemibold, size: 15) as Any,
            NSAttributedStringKey.foregroundColor : UIColorPalette.chatMessageExternalLinkTitleColor,
            NSAttributedStringKey.paragraphStyle : paragraphStyle
        ]
        
        return NSAttributedString(string: viewModel?.preview?.title ?? String(), attributes: attributes)
    }
    
    /// Label для описания
    private var descriptionTextView: UITextView = {
        
        let textView = ChatTextMessageTextView()
        
        UIView.performWithoutAnimation({ // ячейка перестает "моргать" при появлении
            textView.backgroundColor = UIColor.clear
        })
        
        textView.isEditable = false
        textView.isSelectable = true
        textView.dataDetectorTypes = .all
        textView.scrollsToTop = false
        textView.isScrollEnabled = false
        textView.bounces = false
        textView.bouncesZoom = false
        textView.showsHorizontalScrollIndicator = false
        textView.showsVerticalScrollIndicator = false
        textView.isUserInteractionEnabled = true
        textView.isExclusiveTouch = true
        textView.textContainer.lineFragmentPadding = 0
        textView.textContainerInset = UIEdgeInsets.zero
        textView.disableGestureRecognizers()
        
        textView.linkTextAttributes = [
            NSAttributedStringKey.foregroundColor.rawValue : UIColorPalette.chatMessageDefaultLinkColor,
            NSAttributedStringKey.underlineStyle.rawValue : NSUnderlineStyle.styleSingle.rawValue
        ]
        
        return textView
    }()
    
    /// Расстояние между строками в описании
    private let descriptionLineSpacing: CGFloat = 6
    
    /// Описание
    private var descriptionAttributedString: NSAttributedString {
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .left
        paragraphStyle.lineSpacing = descriptionLineSpacing
        
        let viewModel = messageViewModel as? ChatExternalLinkMessageViewModelProtocol
        let attributes: [NSAttributedStringKey : Any] = [
            NSAttributedStringKey.font : UIFont(customName: .graphikLCGRegular, size: 14) as Any,
            NSAttributedStringKey.foregroundColor : UIColorPalette.chatMessageExternalLinkDescriptionColor,
            NSAttributedStringKey.paragraphStyle : paragraphStyle
        ]
        
        return NSAttributedString(string: viewModel?.preview?.description ?? String(), attributes: attributes)
    }
    
    /// Label для источника
    private var sourceLabel: UILabel = {
        
        let sourceLabel = UILabel()
        sourceLabel.font = UIFont(customName: .graphikLCGRegular, size: 13)
        sourceLabel.textColor = UIColorPalette.chatMessageExternalLinkSourceColor
        sourceLabel.numberOfLines = 1
        
        return sourceLabel
    }()
    
    override func initializeContentSubviews(contentParentView: UIView) {
        super.initializeContentSubviews(contentParentView: contentParentView)
        contentParentView.addSubview(photoView)
        contentParentView.addSubview(titleLabel)
        contentParentView.addSubview(descriptionTextView)
        contentParentView.addSubview(sourceLabel)
    }
    
    override func updateContentViews() {
        
        super.updateContentViews()
        
        guard let viewModel = messageViewModel as? ChatExternalLinkMessageViewModelProtocol else { return }
        
        photoView.image = nil
        photoView.loadAndDisplayImage(from: viewModel.preview?.photoUrls.first)
        
        titleLabel.attributedText = titleAttributedString
        titleLabel.isHidden = titleLabel.text?.isEmpty ?? true
        
        descriptionTextView.attributedText = descriptionAttributedString
        descriptionTextView.isHidden = descriptionTextView.text?.isEmpty ?? true
        descriptionTextView.delegate = textBubbleDelegate != nil ? self : nil
        
        sourceLabel.text = viewModel.source
    }
    
    override func createLayoutModelContext(preferredMaxLayoutWidth: CGFloat) -> BaseBubbleLayoutModel.LayoutContext? {
        
        guard let style = style as? ChatExternalLinkBubbleViewStyleProtocol else { return nil }
        guard let viewModel = messageViewModel as? ChatExternalLinkMessageViewModelProtocol else { return nil }
        
        let model = super.createLayoutModelContext(preferredMaxLayoutWidth: preferredMaxLayoutWidth)
        
        guard let baseModel = model as? ChatTextBubbleLayoutModel.ChatTextLayoutContext else { return nil }
        
        let hasNoPhoto = viewModel.preview?.photoUrls.isEmpty ?? true
        
        let linkParameters = ChatExternalLinkBubbleLayoutModel.ChatExternalLinkLayoutContext.ChatExternalLinkLayoutParameters(
            bubbleWidth: style.bubbleWidth(viewModel: viewModel, isSelected: isSelected),
            photoHeight: hasNoPhoto ? 0 : style.photoHeight(viewModel: viewModel, isSelected: isSelected),
            title: titleAttributedString,
            description: descriptionAttributedString,
            hasSource: viewModel.source != nil
        )
        
        return ChatExternalLinkBubbleLayoutModel.ChatExternalLinkLayoutContext(
            parameters: baseModel.layoutParameters,
            textParameters: baseModel.chatTextLayoutParameters,
            linkParameters: linkParameters
        )
    }
    
    override func layoutSubviews() {
        
        super.layoutSubviews()
        
        guard let linkLayout = calculateBubbleLayout(preferredMaxLayoutWidth: preferredMaxLayoutWidth) as? ChatExternalLinkBubbleLayoutModel else { return }
        
        photoView.bma_rect = linkLayout.photoFrame
        titleLabel.bma_rect = linkLayout.titleFrame
        descriptionTextView.bma_rect = linkLayout.descriptionFrame
        sourceLabel.bma_rect = linkLayout.sourceFrame
    }
}

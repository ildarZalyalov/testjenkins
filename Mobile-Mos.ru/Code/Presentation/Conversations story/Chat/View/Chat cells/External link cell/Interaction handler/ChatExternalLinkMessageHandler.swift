//
//  ChatExternalLinkMessageHandler.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 13.09.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import ChattoAdditions

/// Обработчик действий пользователя для сообщений с внешней ссылкой
class ChatExternalLinkMessageHandler: BaseInteractionHandlerProtocol {
    
    weak var baseHandler: BaseChatMessageHandler?
    
    init(baseHandler: BaseChatMessageHandler) {
        self.baseHandler = baseHandler
    }
    
    func userDidTapOnFailIcon(viewModel: ChatExternalLinkMessageViewModel, failIconView: UIView) {
        baseHandler?.userDidTapOnFailIcon(viewModel: viewModel, failIconView: failIconView)
    }
    
    func userDidTapOnAvatar(viewModel: ChatExternalLinkMessageViewModel) {
        baseHandler?.userDidTapOnAvatar(viewModel: viewModel)
    }
    
    func userDidTapOnBubble(viewModel: ChatExternalLinkMessageViewModel) {
        baseHandler?.userDidTapOnBubble(viewModel: viewModel)
    }
    
    func userDidBeginLongPressOnBubble(viewModel: ChatExternalLinkMessageViewModel) {
        baseHandler?.userDidBeginLongPressOnBubble(viewModel: viewModel)
    }
    
    func userDidEndLongPressOnBubble(viewModel: ChatExternalLinkMessageViewModel) {
        baseHandler?.userDidEndLongPressOnBubble(viewModel: viewModel)
    }
    
    func userDidSelectMessage(viewModel: ChatExternalLinkMessageViewModel) {
        baseHandler?.userDidSelectMessage(viewModel: viewModel)
    }
    
    func userDidDeselectMessage(viewModel: ChatExternalLinkMessageViewModel) {
        baseHandler?.userDidDeselectMessage(viewModel: viewModel)
    }
    
    func userDidPressInlineKeyboardButton(viewModel: BaseMessageViewModelProtocol, with buttonId: String) {
        baseHandler?.userDidPressInlineKeyboardButton(viewModel: viewModel, with: buttonId)
    }
    
    func userDidPressExpandInlineKeyboardButton(viewModel: BaseMessageViewModelProtocol) {
        baseHandler?.userDidPressExpandInlineKeyboardButton(viewModel: viewModel)
    }
    
    func userDidPressOptionButton(viewModel: BaseMessageViewModelProtocol, with buttonId: String) {
        baseHandler?.userDidPressOptionButton(viewModel: viewModel, with: buttonId)
    }
    
    func userDidPressToOpen(url: URL, inMessage viewModel: BaseMessageViewModelProtocol) {
        baseHandler?.userDidPressToOpen(url: url, inMessage: viewModel)
    }
}

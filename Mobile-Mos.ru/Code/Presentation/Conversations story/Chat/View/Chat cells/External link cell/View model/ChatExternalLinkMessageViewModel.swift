//
//  ChatExternalLinkMessageViewModel.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 13.09.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import ChattoAdditions

/// Протокол view model для ячейки сообщения с внешней ссылкой
protocol ChatExternalLinkMessageViewModelProtocol: ChatTextMessageViewModelProtocol  {
    
    /// Превью (если есть)
    var preview: ChatMessageExternalResourcePreview? { get }
    
    /// Источник (если есть)
    var source: String? { get }
    
    /// Ссылка на внешний ресурс (для копирования в буфер обмена)
    var urlString: String? { get }
}

/// View model сообщения с внешней ссылкой
class ChatExternalLinkMessageViewModel: ChatTextMessageViewModel, ChatExternalLinkMessageViewModelProtocol {
    
    /// Превью (если есть)
    let preview: ChatMessageExternalResourcePreview?
    
    /// Источник (если есть)
    let source: String?
    
    /// Ссылка на внешний ресурс (для копирования в буфер обмена)
    var urlString: String?
    
    init(textModel: ChatTextMessageViewModel, preview: ChatMessageExternalResourcePreview?, source: String?, urlString: String?) {
        self.preview = preview
        self.source = source
        self.urlString = urlString
        super.init(messageUid: textModel.messageUid,
                   textMessage: textModel.textMessage,
                   messageViewModel: textModel.messageViewModel,
                   inlineButtons: textModel.inlineButtons,
                   optionButtons: textModel.optionButtons,
                   isShowingExpandButton: textModel.isShowingExpandButton,
                   formattedText: textModel.formattedText)
    }
}

/// Билдер view model сообщения с внешней ссылкой
class ChatExternalLinkMessageViewModelBuilder: ViewModelBuilderProtocol {
    
    let textMessageViewModelBuilder: ChatTextMessageViewModelBuilder
    
    init(style: ChatTextMessageCollectionViewCellStyleProtocol, formattingExtractor: ChatTextFormattingExtractor) {
        textMessageViewModelBuilder = ChatTextMessageViewModelBuilder(style: style, formattingExtractor: formattingExtractor)
    }
    
    func createViewModel(_ linkMessage: ChatExternalLinkMessageCellObject) -> ChatExternalLinkMessageViewModel {
        
        let textViewModel = textMessageViewModelBuilder.createViewModel(linkMessage)
        let viewModel = ChatExternalLinkMessageViewModel(textModel: textViewModel,
                                                         preview: linkMessage.preview,
                                                         source: linkMessage.source,
                                                         urlString: linkMessage.urlString)
        
        if linkMessage.preview != nil || linkMessage.source != nil {
            viewModel.customBubbleColor = UIColor.white
        }
        
        return viewModel
    }
    
    func canCreateViewModel(fromModel model: Any) -> Bool {
        return model is ChatExternalLinkMessageCellObject
    }
}

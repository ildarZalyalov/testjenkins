//
//  ChatExternalLinkMessagePresenter.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 13.09.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import ChattoAdditions

class ChatExternalLinkMessagePresenter<ViewModelBuilderT, InteractionHandlerT>:
    ChatTextMessagePresenter<ViewModelBuilderT, InteractionHandlerT> where
    ViewModelBuilderT: ViewModelBuilderProtocol,
    ViewModelBuilderT.ViewModelT: ChatExternalLinkMessageViewModelProtocol,
    InteractionHandlerT: BaseInteractionHandlerProtocol,
    InteractionHandlerT.ViewModelT == ViewModelBuilderT.ViewModelT {
    
    public init(
        messageModel: ModelT,
        viewModelBuilder: ViewModelBuilderT,
        interactionHandler: InteractionHandlerT?,
        sizingCell: ChatExternalLinkMessageCollectionViewCell,
        baseCellStyle: BaseMessageCollectionViewCellStyleProtocol,
        linkCellStyle: ChatExternalLinkMessageCollectionViewCellStyleProtocol,
        layoutCache: NSCache<AnyObject, AnyObject>) {
        self.linkCellStyle = linkCellStyle
        super.init(
            messageModel: messageModel,
            viewModelBuilder: viewModelBuilder,
            interactionHandler: interactionHandler,
            sizingCell: sizingCell,
            baseCellStyle: baseCellStyle,
            textCellStyle: linkCellStyle,
            layoutCache: layoutCache)
    }
    
    let linkCellStyle: ChatExternalLinkMessageCollectionViewCellStyleProtocol
    
    var onMessageWillBeShown: ChatExternalLinkMessageWillBeShownHandler?
    
    override class func registerCells(_ collectionView: UICollectionView) {
        collectionView.register(ChatExternalLinkMessageCollectionViewCell.self, forCellWithReuseIdentifier: "chat-external-link-message-incoming")
        collectionView.register(ChatExternalLinkMessageCollectionViewCell.self, forCellWithReuseIdentifier: "chat-external-link-message-outcoming")
    }
    
    override func dequeueCell(collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell {
        let identifier = messageViewModel.isIncoming ? "chat-external-link-message-incoming" : "chat-external-link-message-outcoming"
        return collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath)
    }
    
    public var linkCell: ChatExternalLinkMessageCollectionViewCell? {
        
        if let cell = self.cell {
            if let linkCell = cell as? ChatExternalLinkMessageCollectionViewCell {
                return linkCell
            } else {
                assert(false, "Invalid cell was given to presenter!")
            }
        }
        
        return nil
    }
    
    override func configureCell(_ cell: BaseMessageCollectionViewCell<ChatTextBubbleView>, decorationAttributes: ChatItemDecorationAttributes, animated: Bool, additionalConfiguration: (() -> Void)?) {
        
        guard let cell = cell as? ChatExternalLinkMessageCollectionViewCell else {
            assert(false, "Invalid cell received")
            return
        }
        
        super.configureCell(cell, decorationAttributes: decorationAttributes, animated: animated) { () -> Void in
        
            cell.linkMessageViewModel = self.messageViewModel
            cell.linkMessageStyle = self.linkCellStyle
            
            additionalConfiguration?()
        }
    }
    
    override func updateCurrentCell() {
        if let cell = linkCell, let decorationAttributes = decorationAttributes {
            configureCell(cell, decorationAttributes: decorationAttributes, animated: itemVisibility != .appearing, additionalConfiguration: nil)
        }
    }
    
    override func cellWillBeShown() {
        onMessageWillBeShown?(messageViewModel.messageUid)
    }
    
    override func performMenuControllerAction(_ action: Selector) {
        
        let selector = #selector(UIResponderStandardEditActions.copy(_:))
        
        if action == selector {
            UIPasteboard.general.string = messageViewModel.preview?.description ??
                messageViewModel.preview?.title ??
                messageViewModel.urlString ??
                messageViewModel.source
        } else {
            assert(false, "Unexpected action")
        }
    }
}

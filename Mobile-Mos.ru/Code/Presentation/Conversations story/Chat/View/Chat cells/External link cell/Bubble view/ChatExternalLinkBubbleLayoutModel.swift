//
//  ChatExternalLinkBubbleLayoutModel.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 13.09.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Модель лейаута для ChatExternalLinkBubbleView. Умеет расчитывать размер ChatExternalLinkBubbleView при заданных параметрах, а также кеширует его
class ChatExternalLinkBubbleLayoutModel: ChatTextBubbleLayoutModel {
    
    /// Содержит входные данные для расчета, а также служит ключем хэширования
    class ChatExternalLinkLayoutContext: ChatTextBubbleLayoutModel.ChatTextLayoutContext {
        
        struct ChatExternalLinkLayoutParameters {
            
            /// Ширина баббла
            let bubbleWidth: CGFloat
            
            /// Высота фото в баббле (если 0, то фото не отображается)
            let photoHeight: CGFloat
            
            /// Заголовок
            let title: NSAttributedString
            
            /// Описание
            let description: NSAttributedString
            
            /// Задан ли источник
            let hasSource: Bool
        }
        
        let chatExternalLinkLayoutParameters: ChatExternalLinkLayoutParameters
        
        override var bubbleLayoutType: BaseBubbleLayoutModel.Type {
            return ChatExternalLinkBubbleLayoutModel.self
        }
        
        init(parameters: LayoutParameters,
             textParameters: ChatTextLayoutParameters,
             linkParameters: ChatExternalLinkLayoutParameters) {
            self.chatExternalLinkLayoutParameters = linkParameters
            super.init(parameters: parameters, textParameters: textParameters)
        }
        
        override var hashValue: Int {
            
            let params = chatExternalLinkLayoutParameters
            let hashes = [super.hashValue,
                          params.bubbleWidth.hashValue,
                          params.photoHeight.hashValue,
                          params.title.hashValue,
                          params.description.hashValue,
                          params.hasSource.hashValue]
            
            return hashes.combinedHashValue
        }
        
        static func == (lhs: ChatExternalLinkLayoutContext, rhs: ChatExternalLinkLayoutContext) -> Bool {
            
            if lhs === rhs { return true }
            
            typealias BaseContext = ChatTextBubbleLayoutModel.ChatTextLayoutContext
            let baseEqual = lhs as BaseContext == rhs as BaseContext
            
            let lhsParams = lhs.chatExternalLinkLayoutParameters
            let rhsParams = rhs.chatExternalLinkLayoutParameters
            
            let lhsValues = (lhsParams.bubbleWidth, lhsParams.photoHeight, lhsParams.title, lhsParams.description, lhsParams.hasSource)
            let rhsValues = (rhsParams.bubbleWidth, rhsParams.photoHeight, rhsParams.title, rhsParams.description, rhsParams.hasSource)
            
            return lhsValues == rhsValues && baseEqual
        }
    }
    
    /// Оффсет снизу и сверху для текстовых блоков
    let topBottomOffset: CGFloat = 8
    
    /// Инсет слева и справа для текстовых блоков
    let leftRightInset: CGFloat = 12
    
    /// Margin для текстовых блоков
    let textBlocksMargin: CGFloat = 5
    
    /// Высота блока с источником, если он отображается
    let sourceHeight: CGFloat = 18
    
    /// Фрейм фото
    var photoFrame: CGRect = CGRect.zero
    
    /// Фрейм заголовка
    var titleFrame: CGRect = CGRect.zero
    
    /// Фрейм описания
    var descriptionFrame: CGRect = CGRect.zero
    
    /// Фрейм источника
    var sourceFrame: CGRect = CGRect.zero
    
    /// Размер превью
    var previewSize: CGSize = CGSize.zero
    
    /// Есть ли текстовые блоки в превью
    var hasTextualPreviewData: Bool {
        guard let context = layoutContext as? ChatExternalLinkLayoutContext else { return false }
        let layoutParameters = context.chatExternalLinkLayoutParameters
        return !layoutParameters.title.string.isEmpty || !layoutParameters.description.string.isEmpty || layoutParameters.hasSource
    }
    
    /// Высота заголовка
    var titleHeight: CGFloat {
        
        guard let context = layoutContext as? ChatExternalLinkLayoutContext else { return 0 }
        let layoutParameters = context.chatExternalLinkLayoutParameters
        guard !layoutParameters.title.string.isEmpty else { return 0 }
        
        var sizeToFitText = CGSize(width: layoutParameters.bubbleWidth, height: CGFloat.greatestFiniteMagnitude)
        sizeToFitText.width -= 2 * leftRightInset
        let titleHeight = layoutParameters.title.boundingRect(with: sizeToFitText,
                                                              options: .usesLineFragmentOrigin,
                                                              context: nil).height
        
        return ceil(titleHeight)
    }
    
    /// Высота описания
    var descriptionHeight: CGFloat {
        
        guard let context = layoutContext as? ChatExternalLinkLayoutContext else { return 0 }
        let layoutParameters = context.chatExternalLinkLayoutParameters
        guard !layoutParameters.description.string.isEmpty else { return 0 }
        
        var sizeToFitText = CGSize(width: layoutParameters.bubbleWidth, height: CGFloat.greatestFiniteMagnitude)
        sizeToFitText.width -= 2 * leftRightInset
        let descriptionHeight = layoutParameters.description.boundingRect(with: sizeToFitText,
                                                                          options: .usesLineFragmentOrigin,
                                                                          context: nil).height
        
        return ceil(descriptionHeight)
    }
    
    /// Посчитать размер контента баббла
    override func calculateIntrinsicContentSize() -> CGSize {
        
        var contentSize = super.calculateIntrinsicContentSize()
        
        guard let context = layoutContext as? ChatExternalLinkLayoutContext else { return contentSize }
        let layoutParameters = context.chatExternalLinkLayoutParameters
        
        previewSize = CGSize(width: layoutParameters.bubbleWidth, height: layoutParameters.photoHeight)
        
        if !layoutParameters.title.string.isEmpty {
            previewSize.height += titleHeight
            previewSize.height += 2 * textBlocksMargin
        }
        
        if !layoutParameters.description.string.isEmpty {
            previewSize.height += descriptionHeight
            previewSize.height += 2 * textBlocksMargin
        }
        
        if layoutParameters.hasSource {
            previewSize.height += sourceHeight
            previewSize.height += 2 * textBlocksMargin
        }
        
        if hasTextualPreviewData {
            previewSize.height += 2 * topBottomOffset
        }
        
        contentSize.width = max(contentSize.width, previewSize.width)
        contentSize.height += previewSize.height
        
        return contentSize
    }
    
    /// Пересчитать кеш лейаута контента баббла
    override func calculateContentLayout(for contentSize: CGSize) {
        
        super.calculateContentLayout(for: contentSize)
        
        guard let context = layoutContext as? ChatExternalLinkLayoutContext else { return }
        let layoutParameters = context.chatExternalLinkLayoutParameters
        
        photoFrame = CGRect(x: 0, y: textSize.height, width: layoutParameters.bubbleWidth, height: layoutParameters.photoHeight)
        
        var currentY = textSize.height + photoFrame.height
        
        if hasTextualPreviewData {
            currentY += topBottomOffset
        }
        
        if !layoutParameters.title.string.isEmpty {
            
            currentY += textBlocksMargin
            
            titleFrame = CGRect(x: 0, y: currentY, width: layoutParameters.bubbleWidth, height: titleHeight)
            titleFrame = titleFrame.insetBy(dx: leftRightInset, dy: 0)
            
            currentY += titleFrame.height + textBlocksMargin
        }
        
        if !layoutParameters.description.string.isEmpty {
            
            currentY += textBlocksMargin
            
            descriptionFrame = CGRect(x: 0, y: currentY, width: layoutParameters.bubbleWidth, height: descriptionHeight)
            descriptionFrame = descriptionFrame.insetBy(dx: leftRightInset, dy: 0)
            
            currentY += descriptionFrame.height + textBlocksMargin
        }
        
        if layoutParameters.hasSource {
            
            currentY += textBlocksMargin
            
            sourceFrame = CGRect(x: 0, y: currentY, width: layoutParameters.bubbleWidth, height: sourceHeight)
            sourceFrame = sourceFrame.insetBy(dx: leftRightInset, dy: 0)
        }
    }
}

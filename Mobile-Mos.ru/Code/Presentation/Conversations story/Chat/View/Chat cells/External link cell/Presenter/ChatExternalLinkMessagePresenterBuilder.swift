//
//  ChatExternalLinkMessagePresenterBuilder.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 13.09.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import Chatto
import ChattoAdditions

typealias ChatExternalLinkMessageWillBeShownHandler = (String) -> ()

class ChatExternalLinkMessagePresenterBuilder<ViewModelBuilderT, InteractionHandlerT>:
    ChatTextMessagePresenterBuilder<ViewModelBuilderT, InteractionHandlerT> where
    ViewModelBuilderT: ViewModelBuilderProtocol,
    ViewModelBuilderT.ViewModelT: ChatExternalLinkMessageViewModelProtocol,
    InteractionHandlerT: BaseInteractionHandlerProtocol,
    InteractionHandlerT.ViewModelT == ViewModelBuilderT.ViewModelT {
    
    lazy var sizingLinkCell: ChatExternalLinkMessageCollectionViewCell = {
        
        var cell: ChatExternalLinkMessageCollectionViewCell? = nil
        
        if Thread.isMainThread {
            cell = ChatExternalLinkMessageCollectionViewCell.createSizingCell()
        } else {
            DispatchQueue.main.sync(execute: {
                cell = ChatExternalLinkMessageCollectionViewCell.createSizingCell()
            })
        }
        
        return cell!
    }()
    
    lazy var linkCellStyle: ChatExternalLinkMessageCollectionViewCellStyleProtocol = ChatExternalLinkBubbleViewStyleDefault()
    
    var onMessageWillBeShown: ChatExternalLinkMessageWillBeShownHandler?
    
    override func createPresenterWithChatItem(_ chatItem: ChatItemProtocol) -> ChatItemPresenterProtocol {
        
        assert(canHandleChatItem(chatItem))
        
        let presenter = ChatExternalLinkMessagePresenter<ViewModelBuilderT, InteractionHandlerT>(
            messageModel: chatItem as! ModelT,
            viewModelBuilder: viewModelBuilder,
            interactionHandler: interactionHandler,
            sizingCell: sizingLinkCell,
            baseCellStyle: baseMessageStyle,
            linkCellStyle: linkCellStyle,
            layoutCache: layoutCache
        )
        
        presenter.onMessageWillBeShown = onMessageWillBeShown
        
        return presenter
    }
    
    override var presenterType: ChatItemPresenterProtocol.Type {
        return ChatExternalLinkMessagePresenter<ViewModelBuilderT, InteractionHandlerT>.self
    }
}

//
//  ChatExternalLinkMessageCollectionViewCell.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 13.09.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Ячейка для сообщения с первью внешней ссылки
class ChatExternalLinkMessageCollectionViewCell: ChatTextMessageCollectionViewCell {
    
    override class func createSizingCell() -> ChatExternalLinkMessageCollectionViewCell {
        let cell = ChatExternalLinkMessageCollectionViewCell(frame: CGRect.zero)
        cell.viewContext = .sizing
        return cell
    }
    
    //MARK: - Property forwarding
    
    var linkMessageViewModel: ChatExternalLinkMessageViewModelProtocol? {
        didSet {
            textMessageViewModel = linkMessageViewModel
        }
    }
    
    var linkMessageStyle: ChatExternalLinkBubbleViewStyleProtocol? {
        didSet {
            textMessageStyle = linkMessageStyle
        }
    }
    
    //MARK: Создаем баббл view
    
    override func createBubbleView() -> ChatExternalLinkBubbleView {
        let view = ChatExternalLinkBubbleView()
        view.delegate = self
        view.textBubbleDelegate = self
        return view
    }
}

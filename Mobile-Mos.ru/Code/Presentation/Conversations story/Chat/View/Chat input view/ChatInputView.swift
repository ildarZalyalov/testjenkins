//
//  ChatInputView.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 26.07.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import UIKit

/// UIView, которая отображает различные методы ввода для чата
class ChatInputView: UIView {
    
    /// Режим ввода
    ///
    /// - none: панель ввода не оображается
    /// - text: текстовый ввод с клавиатуры
    /// - textFields: текстовые поля для ввода данных
    /// - command: ввод с командной клавиатуры
    /// - chatActivation: командная клавиатура с кнопкой активации чата
    enum ChatInputMode: String {
        case none
        case text
        case textFields
        case command
        case chatActivation
    }
    
    /// Панель с полем ввода и кнопкой "Отправить"
    let textInputPanel = TextInputToolbar.loadNib()
    
    /// Панель с контролом из нескольких текстовых полей
    let textFieldsControl = ChatTextFieldsControl.loadNib()
    
    /// Командная клавиатура
    let commandKeyboard = CommandKeyboardView(frame: CGRect.zero)
    
    /// Командная клавиатура с кнопкой активации чата
    let activationView = ChatActivationKeyboardView.loadNib()
    
    /// Максимальное кол-во символов доступное для ввода в поле ввода
    let maxCountOfSymbolsInTextInputPanel: Int = 2000
    
    /// Максимальное кол-во символов доступное для ввода в панель с контролом из нескольких текстовых полей
    let maxCountOfSymbolsInTextFieldsControl: Int = 100
    
    /// Индикатор прогресса отправки сообщения
    fileprivate let progressView = LoadingTimeoutProgressView(progressViewStyle: .default)
    
    /// Высота индикатора прогресса отправки сообщения
    fileprivate let progressViewHeight: CGFloat = 2
    
    /// Набор NSLayoutConstraint для каждого метода ввода
    fileprivate var inputViewsConstraints = [Int : [NSLayoutConstraint]]()
    
    /// Вызывать для активной inputView becomeFirstResponder()
    fileprivate var forceFirstResponderOnActiveInputMode = true
    
    /// UIView методов ввода по типам метода ввода
    fileprivate lazy var inputViewsByInputMode: [ChatInputMode : UIView] = [
        .text : textInputPanel,
        .textFields : textFieldsControl,
        .command : commandKeyboard,
        .chatActivation : activationView
    ]
    
    /// Отображается ли системная клавиатура
    var isSystemKeyboardDisplayed: Bool {
        switch currentInputMode {
        case .text:
            return textInputPanel.isSystemKeyboardDisplayed
        case .textFields:
            return textFieldsControl.isSystemKeyboardDisplayed
        default:
            return false
        }
    }
    
    /// Текущий метод ввода
    var currentInputMode: ChatInputMode = .none {
        
        didSet {
            
            guard currentInputMode != oldValue else { return }
            
            invalidateIntrinsicContentSize()
            setupActiveInputView()
            
            setNeedsLayout()
            layoutIfNeeded()
            
            guard forceFirstResponderOnActiveInputMode else { return }
            guard let activeView = inputViewsByInputMode[currentInputMode] else { return }
            
            DispatchQueue.main.async {
                activeView.becomeFirstResponder()
            }
        }
    }
    
    /// Цвет индикатора прогресса отправки сообщения
    var sendMessageProgressTintColor: UIColor? {
        get { return progressView.progressTintColor }
        set { progressView.progressTintColor = newValue }
    }
    
    init() {
        super.init(frame: CGRect.zero)
        commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    fileprivate func commonInit() {
        
        clipsToBounds = false
        translatesAutoresizingMaskIntoConstraints = false
        
        textInputPanel.maxTextSymbolsCount = maxCountOfSymbolsInTextInputPanel
        textFieldsControl.maxTextSymbolsCount = maxCountOfSymbolsInTextFieldsControl
        
        addInputView(inputView: textInputPanel)
        addInputView(inputView: textFieldsControl)
        addInputView(inputView: commandKeyboard)
        addInputView(inputView: activationView)
        
        progressView.frame = CGRect(x: 0, y: -progressViewHeight / 2, width: bounds.width, height: progressViewHeight)
        progressView.autoresizingMask = [.flexibleWidth, .flexibleBottomMargin]
        progressView.trackTintColor = UIColor.clear
        progressView.isHidden = true
        addSubview(progressView)
    }
    
    //MARK: - Приватные методы
    
    /// Добавить UIView метода ввода на панель ввода
    ///
    /// - Parameter inputView: UIView метода ввода
    fileprivate func addInputView(inputView: UIView) {
        
        addSubview(inputView)
        inputView.isHidden = true
        inputView.translatesAutoresizingMaskIntoConstraints = false
        
        let viewConstraints = NSLayoutConstraint.edgesConstraints(for: inputView)
        addConstraints(viewConstraints)
        
        inputViewsConstraints[inputView.hashValue] = viewConstraints
    }
    
    /// Установить активную UIView метода ввода
    fileprivate func setupActiveInputView() {
        
        for (inputMode, inputView) in inputViewsByInputMode {
            
            let isActive = inputMode == currentInputMode
            inputView.isHidden = !isActive
            
            // если новая активная UIView не станет first responder, то таковым останется старая
            // значит для данного случая надо вызвать у такой UIView resignFirstResponder
            // если же новая активная UIView станет first responder, то доверяем системе корректно
            // лишить статуса first responder старую активную UIView
            if !forceFirstResponderOnActiveInputMode && !isActive && inputView.isFirstResponder {
                inputView.resignFirstResponder()
            }
            
            guard let viewConstraints = inputViewsConstraints[inputView.hashValue] else {
                continue
            }
            
            if isActive {
                NSLayoutConstraint.activate(viewConstraints)
            }
            else {
                NSLayoutConstraint.deactivate(viewConstraints)
            }
        }
    }
    
    override var intrinsicContentSize: CGSize {
        
        var height: CGFloat
        
        switch currentInputMode {
        case .none:
            height = 0
        case .text:
            height = textInputPanel.intrinsicContentSize.height
        case .textFields:
            height = textFieldsControl.intrinsicContentSize.height
        case .command:
            height = commandKeyboard.intrinsicContentSize.height
        case .chatActivation:
            height = activationView.intrinsicContentSize.height
        }
        
        return CGSize(width: CGFloat.greatestFiniteMagnitude, height: height)
    }
    
    //MARK: - Открытые методы
    
    /// Установить текущий метод ввода
    ///
    /// - Parameters:
    ///   - mode: метод ввода
    ///   - becomeFirstResponder: вызывать ли becomeFirstResponder() для активной inputView
    func setCurrentInputMode(to mode: ChatInputMode, becomeFirstResponder: Bool = false) {
        forceFirstResponderOnActiveInputMode = becomeFirstResponder
        currentInputMode = mode
        forceFirstResponderOnActiveInputMode = true
    }
    
    /// Запустить прогресс отправки сообщения
    ///
    /// - Parameter timeout: таймаут успешной отправки (по его истечении отправка будет считаться проваленной)
    func startSendMessageProgressTimer(with timeout: TimeInterval) {
        progressView.startProgressTimer(with: timeout)
    }
    
    /// Заполнить прогресс отправки сообщения до конца (прерывает текущую анимацию прогресса отправки сообщения)
    func completeSendMessageProgressTimer() {
        progressView.completeProgressTimer()
    }
    
    /// Убрать first responder статус с активной UIView метода ввода
    func resignActiveInputViewAsFirstResponder() {
        guard let activeView = inputViewsByInputMode[currentInputMode] else { return }
        activeView.endEditing(true)
    }
}

//
//  ChatViewOutput.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 27.04.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol ChatViewOutput: class {
    
    //MARK: - Свойства
    
    var hasMoreMessagesPrevious: Bool { get }
    
    var hasMoreMessagesNext: Bool { get }
    
    //MARK: - Методы
    
    /// Настроить модуль
    func setupInitialState()
    
    func configure(with configuration: ChatModuleConfiguration)
    
    func chatWillAppear()
    
    func chatWillDisappear()
    
    func chatWillScrollToBottom()
    
    func didPressChatAvatar()
    
    func loadMessagesHistory()
    
    func loadMessagesRecent()
    
    func didPressChatActivationButton(with title: String)
    
    /// Пользователь нажал кнопку отправления сообщения
    ///
    /// - Parameter messageText: текст сообщения
    func didPressSendButton(with messageText: String)
    
    func didSubmitTextInput(with inputValuesById: [String : String])
    
    func didSelectCommand(with itemId: String)
    
    func didTapOnMessage(with messageId: String)
    
    func didTapOn(url: URL, inMessageWith messageId: String)
    
    func didTapOnDocumentIconInMessage(with messageId: String)
    
    func didTapOnPhotoInMessage(with messageId: String)
    
    func didSelectInlineCommand(with itemId: String, inMessageWith messageId: String)
    
    func didSelectOption(with type: OptionsKeyboardButtonType, inMessageWith messageId: String)
    
    func didTapOnFailIconOfMessage(with messageId: String)
    
    func didUpdateVisualState(for messageCellObject: BaseMessageCellObjectProtocol)
    
    func willShowMessageWithExternalLink(with messageId: String)
}

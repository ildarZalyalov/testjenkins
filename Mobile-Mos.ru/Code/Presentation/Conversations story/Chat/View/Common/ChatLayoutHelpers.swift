//
//  ChatLayoutHelpers.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 18.07.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import Chatto
import ChattoAdditions

/// Инсеты справа и слева для контента баббла

typealias BubbleViewContentInsets = (left: CGFloat, right: CGFloat)

/// Считаем хэши, используя функцию комбинирования хешей в Chatto

extension CGSize {
    
    var combinedHashValue: Int {
        return bma_combine(hashes: [width.hashValue, height.hashValue])
    }
}

extension UIEdgeInsets {
    
    var combinedHashValue: Int {
        return bma_combine(hashes: [top.hashValue, left.hashValue, bottom.hashValue, right.hashValue])
    }
}

extension Array where Element: Hashable {
    
    var combinedHashValue: Int {
        return bma_combine(hashes: map { $0.hashValue })
    }
}

/// Полезные методы для массива декорированных элементов чата

extension Array where Element == DecoratedChatItem {
    
    /// Добавить декорированный элемент с параметрами
    ///
    /// - Parameters:
    ///   - chatItem: исходный элемент чата
    ///   - bottomMargin: отступ снизу от элемента
    ///   - canShowFailedIcon: может ли баббл показывать иконку ошибки отправки
    ///   - isShowingTail: показывать ли хвостик у баббла
    ///   - isShowingAvatar: показывать ли аватар
    ///   - isShowingSelectionIndicator: показывать ли индикатор возможности выбора баббла
    ///   - isSelected: выбран ли баббл
    mutating func appendDecoratedItem(with chatItem: ChatItemProtocol,
                                      bottomMargin: CGFloat = 0,
                                      canShowFailedIcon: Bool = false,
                                      isShowingTail: Bool = false,
                                      isShowingAvatar: Bool = false,
                                      isShowingSelectionIndicator: Bool = false,
                                      isSelected: Bool = false) {
        
        let baseAttributes = BaseMessageDecorationAttributes(canShowFailedIcon: canShowFailedIcon,
                                                             isShowingTail: isShowingTail,
                                                             isShowingAvatar: isShowingAvatar,
                                                             isShowingSelectionIndicator: isShowingSelectionIndicator,
                                                             isSelected: isSelected)
        
        let attributes = ChatItemDecorationAttributes(bottomMargin: bottomMargin,
                                                      messageDecorationAttributes: baseAttributes)
        
        append(DecoratedChatItem(chatItem: chatItem, decorationAttributes: attributes))
    }
    
    /// Обновить отступ снизу у декорированного элемента
    ///
    /// - Parameter bottomMargin: новый отступ
    mutating func updateLastItemBottomMargin(with bottomMargin: CGFloat) {
        
        guard let lastItem = last else {
            return
        }
        
        let currentAttributes = lastItem.decorationAttributes as? ChatItemDecorationAttributes
        let baseAttributes = currentAttributes?.messageDecorationAttributes ?? BaseMessageDecorationAttributes()
        let newAttributes = ChatItemDecorationAttributes(bottomMargin: bottomMargin, messageDecorationAttributes: baseAttributes)
        
        self[count - 1] = DecoratedChatItem(chatItem: lastItem.chatItem, decorationAttributes: newAttributes)
    }
}

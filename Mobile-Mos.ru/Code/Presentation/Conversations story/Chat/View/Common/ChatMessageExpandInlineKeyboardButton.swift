//
//  ChatMessageExpandInlineKeyboardButton.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 02.08.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Кнопка "расхлопывания" инлайн клавиатуры
/// В отдельном классе ради простоты настройки
class ChatMessageExpandInlineKeyboardButton: KeyboardButtonControl {
    
    init(withBorderWidth borderWidth: CGFloat) {
        super.init(frame: CGRect.zero)
        commonInit(withBorderWidth: borderWidth)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit(withBorderWidth: 0)
    }
    
    fileprivate func commonInit(withBorderWidth borderWidth: CGFloat) {
        
        let expandIcon = #imageLiteral(resourceName: "collapseInlineButtonsIcon")
        let bordersOffset = 2 * borderWidth
        
        setTitle(nil, for: .normal)
        setImage(expandIcon, for: .normal)
        setImage(expandIcon, for: .highlighted)
        contentHorizontalAlignment = .center
        imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: expandIcon.size.width / 2 + bordersOffset)
        isHidden = true
    }
    
    func update(withIconColor iconColor: UIColor, andBackgroundColor buttonBackgroundColor: UIColor) {
        
        normalTintColor = buttonBackgroundColor
        highlightedTintColor = iconColor
        
        if backgroundColor != buttonBackgroundColor {
            backgroundColor = buttonBackgroundColor
        }
        
        if imageView?.tintColor != iconColor {
            imageView?.tintColor = iconColor
        }
        
        isHighlightedChanged = { highlightedButton, isHighlighted in
            highlightedButton.imageView?.tintColor = isHighlighted ? highlightedButton.normalTintColor : highlightedButton.highlightedTintColor
            highlightedButton.backgroundColor = isHighlighted ? highlightedButton.highlightedTintColor : highlightedButton.normalTintColor
        }
    }
}

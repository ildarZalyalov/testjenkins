//
//  ChatDataSourceFactoryImplementation.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 03.05.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import Chatto
import ChattoAdditions

class ChatDataSourceFactoryImplementation: ChatDataSourceFactory {
    
    var dateFormatter: DateStringFormatter!
    
    var inlineButtonsRowDisplayLimit = 3
    
    //MARK: - ChatDataSourceFactory
    
    func buildDataSourceConfiguration(from messages: [ChatMessage]) -> ChatDataSourceConfiguration {
        
        let dataSource = ChatDataSource()
        let cellObjectsConfiguration = createCellObjects(from: messages)
        dataSource.insert(items: cellObjectsConfiguration.cellObjects, at: .bottom)
        
        var configuration = ChatDataSourceConfiguration(dataSource: dataSource, selectionItems: cellObjectsConfiguration.selectionItems)
        setLastMessageFlag(in: &configuration, to: true)
        return configuration
    }
    
    func prepend(history: [ChatMessage], to configuration: inout ChatDataSourceConfiguration) {
        
        let cellObjectsConfiguration = createCellObjects(from: history)
        
        configuration.dataSource.insert(items: cellObjectsConfiguration.cellObjects, at: .top)
        configuration.selectionItems += cellObjectsConfiguration.selectionItems
    }
    
    func append(newMessages: [ChatMessage], to configuration: inout ChatDataSourceConfiguration) {
        
        setLastMessageFlag(in: &configuration, to: false)
        defer {
            setLastMessageFlag(in: &configuration, to: true)
        }
        
        let cellObjectsConfiguration = createCellObjects(from: newMessages)
        
        configuration.dataSource.insert(items: cellObjectsConfiguration.cellObjects, at: .bottom)
        configuration.selectionItems += cellObjectsConfiguration.selectionItems
    }
    
    func updateMessageStatus(where shouldUpdate: (ChatMessage) -> Bool, with transform: (ChatMessage) -> ChatMessage.Status, in configuration: inout ChatDataSourceConfiguration) {
        
        configuration.dataSource.update(where: { cellObject in
            
            guard let message = configuration.selectionItems[cellObject.messageUid] else {
                return false
            }
            
            return shouldUpdate(message)
            
        }, with: { cellObject in
            
            guard var message = configuration.selectionItems[cellObject.messageUid] else {
                return [cellObject]
            }
            
            message.status = transform(message)
            cellObject.setStatus(status: message.status.chattoMessageStatus)
            configuration.selectionItems += [cellObject.messageUid : message]
            
            return [cellObject]
        })
    }
    
    func update(where shouldUpdate: (ChatMessage) -> Bool, with transform: (ChatMessage) -> ChatMessage?, in configuration: inout ChatDataSourceConfiguration) {
        update(where: shouldUpdate, with: transform, in: &configuration, updatingLastMessage: true)
    }
    
    func remove(where shouldRemove: (ChatMessage) -> Bool, in configuration: inout ChatDataSourceConfiguration) {
        
        setLastMessageFlag(in: &configuration, to: false)
        defer {
            setLastMessageFlag(in: &configuration, to: true)
        }
        
        let deletedMessages = configuration.dataSource.remove(where: { cellObject in
            
            guard let message = configuration.selectionItems[cellObject.messageUid] else {
                return false
            }
            
            return shouldRemove(message)
        })
        
        configuration.selectionItems.removeValues(for: deletedMessages.map { $0.messageUid })
    }
    
    func refresh(configuration: inout ChatDataSourceConfiguration, withMessagesHistory messages: [ChatMessage], hasMoreMessagesToLoadNext: Bool) {
        
        guard !messages.isEmpty else { return }
        
        if !hasMoreMessagesToLoadNext {
            setLastMessageFlag(in: &configuration, to: false)
        }
        defer {
            if !hasMoreMessagesToLoadNext {
                setLastMessageFlag(in: &configuration, to: true)
            }
        }
        
        var updatedMessagesById = messages.toDictionary { $0.messageId }
        var updatedMessagesIds = [Int64]()

        update(where: {
            guard $0.canBeUpdatedInHistory else {
                updatedMessagesById[$0.messageId] = nil
                return false
            }
            return updatedMessagesById[$0.messageId] != nil
        }, with: {
            let updatedMessage = updatedMessagesById[$0.messageId]
            updatedMessagesIds.append($0.messageId)
            return updatedMessage
        }, in: &configuration,
           updatingLastMessage: false)
        
        guard !hasMoreMessagesToLoadNext else { return }
        
        // удаляем только после преобразования т.к. одно сообщение может
        // генерировать несколько cell objects, надо убедиться, что
        // мы преобразовали все старые cell objects
        updatedMessagesById.removeValues(for: updatedMessagesIds)
        guard !updatedMessagesById.isEmpty else { return }
        
        let newMessages = updatedMessagesById.values.sorted(by: { $0.messageId < $1.messageId })
        let cellObjectsConfiguration = createCellObjects(from: newMessages)
        
        configuration.selectionItems += cellObjectsConfiguration.selectionItems
        
        guard let lastSentIndex = configuration.dataSource.chatItems.lastElementAndIndex(where: { $0.status == .success })?.index else {
            configuration.dataSource.insert(items: cellObjectsConfiguration.cellObjects, at: .top)
            return
        }
        
        let lastSentMessageUid = configuration.dataSource.chatItems[lastSentIndex].messageUid
        
        if lastSentIndex + 1 < configuration.dataSource.chatItems.count {
            configuration.dataSource.insert(items: cellObjectsConfiguration.cellObjects, at: lastSentIndex + 1)
        }
        else if let lastSentMessage = obtainMessage(with: lastSentMessageUid, in: configuration),
                lastSentMessage.messageId < Int64.max,
                let lastNewMessageId = newMessages.last?.messageId, lastSentMessage.messageId > lastNewMessageId {
            
            // последнее валидное сообщение в чате старше сообщений, которых в чате нет
            // значит, надо заполнять "дыру"
            guard let firstNewMessageId = newMessages.first?.messageId else { return }
            
            // находим первое сообщение с конца, идентификатор которого меньше первого "нового" сообщения
            guard let lastOldMessageIndex = configuration.dataSource.chatItems.lastElementAndIndex(where: {
                guard let message = obtainMessage(with: $0.messageUid, in: configuration) else { return false }
                return message.messageId < firstNewMessageId
            })?.index else { return }
            
            // если это не последнее сообщение, добавляем "новые" после него
            if lastOldMessageIndex + 1 < configuration.dataSource.chatItems.count {
                configuration.dataSource.insert(items: cellObjectsConfiguration.cellObjects, at: lastOldMessageIndex + 1)
            }
            else {
                // иначе просто добавляем их в конец
                configuration.dataSource.insert(items: cellObjectsConfiguration.cellObjects, at: .bottom)
            }
        }
        else {
            configuration.dataSource.insert(items: cellObjectsConfiguration.cellObjects, at: .bottom)
        }
    }
    
    func updateSelectionItems(with messages: [ChatMessage],
                              in configuration: inout ChatDataSourceConfiguration) {
        messages.forEach { configuration.selectionItems[$0.uid] = $0 }
    }
    
    //MARK: - Приватные методы
    
    fileprivate typealias CellObjectsConfiguration = (cellObjects: [BaseMessageCellObjectProtocol], selectionItems: [String : ChatMessage])
    
    fileprivate func obtainMessage(with messageUid: String, in configuration: ChatDataSourceConfiguration) -> ChatMessage? {
        
        let message = configuration.selectionItems[messageUid]
        
        if let parentMessageUid = message?.parentMessageUid {
            return configuration.selectionItems[parentMessageUid]
        }
        else {
            return message
        }
    }
    
    fileprivate func createCellObjects(from messages: [ChatMessage]) -> CellObjectsConfiguration {
        
        var cellObjects = [BaseMessageCellObjectProtocol]()
        var selectionItems = [String : ChatMessage]()
        
        for message in messages {
            
            var cellObject: BaseMessageCellObjectProtocol?
            
            switch message.content.type {
            case .text:
                cellObject = createTextCellObject(from: message)
            case .photo:
                cellObject = createPhotoCellObject(from: message)
            case .map:
                cellObject = createMapCellObject(from: message)
            case .weblink:
                cellObject = createExternalLinkCellObject(from: message)
            case .document:
                cellObject = createDocumentCellObject(from: message)
            case .messageGroup:
                cellObject = createTextCellObject(forTextIn: message)
            case .messageScroll:
                cellObject = createMessageScrollCellObject(from: message)
            }
            
            if let cellObject = cellObject {
                
                let textFormatting = message.content.textFormatting?.convertToChatTextFormatting()
                
                if shouldSeparateText(in: message), let textCellObject = createTextCellObject(forTextIn: message) {
                    textCellObject.textFormatting = textFormatting
                    cellObjects.append(textCellObject)
                }
                else {
                    cellObject.textFormatting = textFormatting
                }
                
                cellObject.inlineKeyboard = createInlineKeyboardCellObject(from: message.replyMarkup.inlineMarkup)
                cellObject.inlineOutsideKeyboard = createInlineKeyboardCellObject(from: message.replyMarkup.inlineOutsideMarkup)
                cellObject.optionsKeyboard = createOptionsKeyboardCellObject(for: message)
                
                cellObjects.append(cellObject)
                selectionItems[cellObject.messageUid] = message
                
                if let history = message.content.messageGroupObject?.messageGroupHistory {
                    let configuration = createCellObjects(from: history)
                    cellObjects += configuration.cellObjects
                    selectionItems += configuration.selectionItems
                }
            }
        }
        
        return (cellObjects, selectionItems)
    }
    
    fileprivate func createCellObjectBase(from message: ChatMessage) -> MessageModel {
        return MessageModel(uid: UUID().uuidString.lowercased(),
                            senderId: message.senderId,
                            type: message.content.type.rawValue,
                            isIncoming: message.isIncoming,
                            date: message.date,
                            status: message.status.chattoMessageStatus)
    }
    
    fileprivate func createInlineKeyboardCellObject(from inlineMarkup: ChatInlineKeyboardMarkup?) -> InlineKeyboardCellObject {
        
        var cellObject = InlineKeyboardCellObject()
        
        guard let markup = inlineMarkup else { return cellObject }
        
        cellObject.inlineButtons = markup.buttons.lazy.map { $0.map {
            InlineKeyboardButtonCellObject(itemId: $0.itemId,
                                           title: $0.title,
                                           isAlwaysActive: $0.isAlwaysActive) }
        }
        
        cellObject.maxRowCountBeforeCollapsing = inlineButtonsRowDisplayLimit
        
        return cellObject
    }
    
    fileprivate func createOptionsKeyboardCellObject(for message: ChatMessage) -> OptionsKeyboardCellObject {
        
        var cellObject = OptionsKeyboardCellObject()
        let optionsMarkup = message.optionsMarkup
        
        if let shareCallbackData = optionsMarkup.shareCallbackData {
            let button = OptionsKeyboardButtonCellObject(itemId: UUID().uuidString.lowercased(), type: .share, isSelected: false, callbackData: shareCallbackData)
            cellObject.optionButtons.append(button)
        }
        
        if message.content.type == .weblink {
            let button = OptionsKeyboardButtonCellObject(itemId: UUID().uuidString.lowercased(), type: .openWeblink, isSelected: false, callbackData: nil)
            cellObject.optionButtons.append(button)
        }
        
        return cellObject
    }
    
    fileprivate func shouldSeparateText(in message: ChatMessage) -> Bool {
        
        guard !message.content.text.isEmpty else { return false }
        
        switch message.content.type {
        case .messageScroll, .map:
            return true
        case .weblink:
            let disablePreview = message.content.weblinkObject?.disablePreview ?? false
            return !disablePreview
        default:
            return false
        }
    }
    
    //MARK: - Создание разных типов cell objects
    
    fileprivate func createTextCellObject(forTextIn message: ChatMessage) -> ChatTextMessageCellObject? {
        
        let baseMessage = MessageModel(uid: UUID().uuidString.lowercased(),
                                       senderId: message.senderId,
                                       type: ChatMessageContent.ContentType.text.rawValue,
                                       isIncoming: message.isIncoming,
                                       date: message.date,
                                       status: message.status.chattoMessageStatus)
        
        let cellObject = ChatTextMessageCellObject(messageUid: message.uid,
                                                   messageModel: baseMessage,
                                                   text: message.content.text)
        cellObject.areInteractionGesturesDisabled = true
        
        return cellObject
    }
    
    fileprivate func createTextCellObject(from message: ChatMessage) -> ChatTextMessageCellObject? {
        let baseMessage = createCellObjectBase(from: message)
        return ChatTextMessageCellObject(messageUid: message.uid,
                                         messageModel: baseMessage,
                                         text: message.content.text)
    }
    
    fileprivate func createPhotoCellObject(from message: ChatMessage) -> ChatPhotoMessageCellObject? {
        
        let baseMessage = createCellObjectBase(from: message)
        let cellObject = ChatPhotoMessageCellObject(messageUid: message.uid,
                                                    messageModel: baseMessage,
                                                    text: message.content.text)
        
        if let photoUrl = message.content.photoObject?.photoUrl {
            cellObject.photoUrl = photoUrl
        }
        cellObject.photoSize = message.content.photoObject?.photoSize
        
        return cellObject
    }
    
    fileprivate func createMapCellObject(from message: ChatMessage) -> ChatMapMessageCellObject? {
        
        let baseMessage = createCellObjectBase(from: message)
        let cellObject = ChatMapMessageCellObject(messageUid: message.uid,
                                                  messageModel: baseMessage,
                                                  text: String())
        
        if let location = message.content.mapObject?.location {
            cellObject.location = location
        }
        
        cellObject.title = message.content.mapObject?.title
        cellObject.description = message.content.mapObject?.description
        
        return cellObject
    }
    
    fileprivate func createExternalLinkCellObject(from message: ChatMessage) -> BaseMessageCellObjectProtocol? {
        
        let disablePreview = message.content.weblinkObject?.disablePreview ?? false
        
        if disablePreview {
            
            let cellObject = createTextCellObject(forTextIn: message)
            cellObject?.areInteractionGesturesDisabled = false
            
            return cellObject
        }
        else {
            
            let baseMessage = createCellObjectBase(from: message)
            let cellObject = ChatExternalLinkMessageCellObject(messageUid: message.uid,
                                                               messageModel: baseMessage,
                                                               text: String())
            
            if let weblinkObject = message.content.weblinkObject {
                
                cellObject.urlString = weblinkObject.linkUrl.absoluteString
                cellObject.preview = weblinkObject.preview
                
                let shouldHideSource = weblinkObject.preview?.hideSource ?? false
                
                if let publicationDate = weblinkObject.preview?.publicationDate {
                    cellObject.source = dateFormatter.relativeDateString(from: publicationDate)
                }
                else if !shouldHideSource {
                    cellObject.source = weblinkObject.linkUrl.host
                }
            }
            
            return cellObject
        }
    }
    
    fileprivate func createDocumentCellObject(from message: ChatMessage) -> ChatDocumentMessageCellObject? {
        
        let baseMessage = createCellObjectBase(from: message)
        let cellObject = ChatDocumentMessageCellObject(messageUid: message.uid,
                                                       messageModel: baseMessage,
                                                       text: message.content.text)
        
        cellObject.fileName = message.content.documentObject?.fileName ?? String()
        cellObject.fileSize = message.content.documentObject?.fileSize
        
        if let downloadState = message.content.documentObject?.downloadState {
            cellObject.downloadState = downloadState
        }
        
        return cellObject
    }
    
    fileprivate func createMessageScrollCellObject(from message: ChatMessage) -> ChatMessageScrollCellObject? {
        
        guard let messageScrollObject = message.content.messageScrollObject else { return nil }
        
        //фильтруем вложенные сообщения со вложенными сообщениями и ставим uid родительского
        //всем вложенным, чтобы работали обработчики
        var messages = messageScrollObject.messages.filter { $0.content.type != .messageScroll }
        messages.forEachMutate { $0.uid = message.uid }
        
        guard !messages.isEmpty else { return nil }
        
        let baseMessage = createCellObjectBase(from: message)
        let cellObject = ChatMessageScrollCellObject(messageUid: message.uid,
                                                     messageModel: baseMessage,
                                                     text: message.content.text)
        
        cellObject.messagesCellObjects = createCellObjects(from: messages).cellObjects
        
        return cellObject
    }
    
    //MARK: - Обработка isLastMessage
    
    fileprivate func setLastMessageFlag(in configuration: inout ChatDataSourceConfiguration, to value: Bool) {
        
        guard let lastCellObject = configuration.dataSource.chatItems.last else { return }
        
        let newLastCellObject = lastCellObject.copy()
        newLastCellObject.isLastMessage = value
        
        if let messageScrollCellObject = newLastCellObject as? ChatMessageScrollCellObject {
            messageScrollCellObject.messagesCellObjects.forEachMutate { $0.isLastMessage = value }
        }
        
        let index = configuration.dataSource.chatItems.count - 1
        configuration.dataSource.replaceItem(at: index, with: newLastCellObject)
    }
    
    //MARK: - Обновление сообщений
    
    /// Обновить сообщения в источнике данных
    ///
    /// - Parameters:
    ///   - shouldUpdate: условие обновления
    ///   - transform: преобразователь для сообщений, которые надо обновить; если он вернет nil, сообщение не обновляется
    ///   - configuration: текущая конфигурация источника данных
    ///   - updatingLastMessage: обновлять ли флаг последнего сообщения
    fileprivate func update(where shouldUpdate: (ChatMessage) -> Bool, with transform: (ChatMessage) -> ChatMessage?, in configuration: inout ChatDataSourceConfiguration, updatingLastMessage: Bool) {
        
        if updatingLastMessage {
            setLastMessageFlag(in: &configuration, to: false)
        }
        defer {
            if updatingLastMessage {
                setLastMessageFlag(in: &configuration, to: true)
            }
        }
        
        var updatedMessagesUids = Set<String>()
        
        configuration.dataSource.update(where: { cellObject in
            
            guard let message = configuration.selectionItems[cellObject.messageUid] else {
                return false
            }
            
            return shouldUpdate(message)
            
        }, with: { oldCellObject in
            
            // если мы уже создали новые cell Objects для обновленного сообщения
            // значит это один из старых, удаляем его
            guard !updatedMessagesUids.contains(oldCellObject.messageUid) else { return [] }
            guard let oldMessage = configuration.selectionItems[oldCellObject.messageUid] else { return [] }
            guard let newMessage = transform(oldMessage) else { return [] }
            
            let cellObjectsConfiguration = createCellObjects(from: [newMessage])
            
            guard let newCellObject = cellObjectsConfiguration.cellObjects.first else { return [] }
            
            updatedMessagesUids.insert(oldCellObject.messageUid)
            configuration.selectionItems += [newCellObject.messageUid : newMessage]
            
            return cellObjectsConfiguration.cellObjects
        })
    }
}

fileprivate extension ChatMessage.Status {
    
    var chattoMessageStatus: MessageStatus {
        switch self {
        case .success:
            return .success
        case .failed:
            return .failed
        case .sending:
            return .sending
        }
    }
}

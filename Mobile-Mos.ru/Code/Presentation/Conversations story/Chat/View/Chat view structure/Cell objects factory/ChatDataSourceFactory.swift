//
//  ChatDataSourceFactory.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 03.05.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Конфигурация источника данных для чата
struct ChatDataSourceConfiguration {
    
    /// Источник данных
    var dataSource: ChatDataSource
    
    /// Словарь с сообщениями по идентификатору
    var selectionItems: [String : ChatMessage]
}

/// Фабрика для работы с источником данных для чата
protocol ChatDataSourceFactory {
    
    /// Максимальное отображаемое кол-во рядов кнопок инлайн клавиатуры (если их больше, то они будут "схлопнуты")
    var inlineButtonsRowDisplayLimit: Int { get set }
    
    /// Создать источник данных для чата
    ///
    /// - Parameter messages: сообщения
    /// - Returns: конфигурация источника данных для чата
    func buildDataSourceConfiguration(from messages: [ChatMessage]) -> ChatDataSourceConfiguration
    
    /// Добавить к источнику данных для чата сообщения из истории переписки сверху
    ///
    /// - Parameters:
    ///   - history: сообщения из истории переписки
    ///   - configuration: текущая конфигурация источника данных
    func prepend(history: [ChatMessage], to configuration: inout ChatDataSourceConfiguration)
    
    /// Добавить к источнику данных для чата новые сообщения снизу
    ///
    /// - Parameters:
    ///   - newMessages: новые сообщения
    ///   - configuration: текущая конфигурация источника данных
    func append(newMessages: [ChatMessage], to configuration: inout ChatDataSourceConfiguration)
    
    /// Обновить статусы сообщений в источнике данных
    ///
    /// - Parameters:
    ///   - shouldUpdate: условие обновления
    ///   - transform: преобразователь статусов для сообщений
    ///   - configuration: текущая конфигурация источника данных
    func updateMessageStatus(where shouldUpdate: (ChatMessage) -> Bool, with transform: (ChatMessage) -> ChatMessage.Status, in configuration: inout ChatDataSourceConfiguration)
    
    /// Обновить сообщения в источнике данных
    ///
    /// - Parameters:
    ///   - shouldUpdate: условие обновления
    ///   - transform: преобразователь для сообщений, которые надо обновить; если он вернет nil, сообщение не обновляется
    ///   - configuration: текущая конфигурация источника данных
    func update(where shouldUpdate: (ChatMessage) -> Bool, with transform: (ChatMessage) -> ChatMessage?, in configuration: inout ChatDataSourceConfiguration)
    
    /// Удалить сообщения в источнике данных
    ///
    /// - Parameters:
    ///   - shouldRemove: условие удаления
    ///   - configuration: текущая конфигурация источника данных
    func remove(where shouldRemove: (ChatMessage) -> Bool, in configuration: inout ChatDataSourceConfiguration)
    
    /// Обновить сообщения в источнике данных в связи с обновлением истории сообщений
    ///
    /// - Parameters:
    ///   - configuration: текущая конфигурация источника данных
    ///   - messages: актуальные сообщения из истории сообщений
    ///   - hasMoreMessagesToLoadNext: возможна загрузка более новых сообщений из истории сообщений
    func refresh(configuration: inout ChatDataSourceConfiguration, withMessagesHistory messages: [ChatMessage], hasMoreMessagesToLoadNext: Bool)
    
    /// Обновить словарь с сообщениями по идентификатору в источнике данных
    ///
    /// - Parameters:
    ///   - messages: сообщения, которые надо обновить в источнике данных
    ///   - configuration: текущая конфигурация источника данных
    func updateSelectionItems(with messages: [ChatMessage], in configuration: inout ChatDataSourceConfiguration)
}

//
//  ChatDataSource.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 28.04.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import Chatto
import ChattoAdditions

/// Источник данных для чата
class ChatDataSource {
    
    /// Позиция для вставки новых сообщений
    ///
    /// - top: сверху
    /// - bottom: снизу
    enum InsertPosition {
        case top
        case bottom
    }
    
    fileprivate(set) var chatItems: [BaseMessageCellObjectProtocol]

    init() {
        chatItems = []
    }
    
    /// Получаем или устанавливаем сообщение по идентификатору
    ///
    /// - Parameter identifier: идентификатор
    subscript(identifier: String) -> BaseMessageCellObjectProtocol? {
        
        get {
            return chatItems.first(where: { $0.uid == identifier })
        }
        
        set {
            
            guard let message = newValue else {
                return
            }
            
            guard let index = chatItems.index(where: { $0.uid == identifier }) else {
                return
            }
            
            chatItems[index] = message
        }
    }
    
    /// Вставить новые сообщения
    ///
    /// - Parameters:
    ///   - items: сообщения
    ///   - position: позиция вставки
    func insert(items: [BaseMessageCellObjectProtocol], at position: InsertPosition) {
        switch position {
        case .bottom:
            chatItems.append(contentsOf: items)
        case .top:
            chatItems.insert(contentsOf: items, at: 0)
        }
    }
    
    /// Вставить новые сообщения по индексу
    ///
    /// - Parameters:
    ///   - items: сообщения
    ///   - index: индекс вставки
    func insert(items: [BaseMessageCellObjectProtocol], at index: Int) {
        guard chatItems.indices.contains(index) else { return }
        chatItems.insert(contentsOf: items, at: index)
    }
    
    /// Обновить сообщения по условию
    ///
    /// - Parameters:
    ///   - shouldUpdate: условие обновления
    ///   - transform: преобразователь для сообщений, которые надо обновить; если он вернет пустой массив, сообщение удаляется
    func update(where shouldUpdate: (BaseMessageCellObjectProtocol) -> Bool, with transform: (BaseMessageCellObjectProtocol) -> [BaseMessageCellObjectProtocol]) {
        chatItems = chatItems.reduce([BaseMessageCellObjectProtocol](), { $0 + (shouldUpdate($1) ? transform($1) : [$1]) })
    }
    
    /// Заменить сообщение по индексу
    ///
    /// - Parameters:
    ///   - index: индекс
    ///   - newItem: новое сообщение
    func replaceItem(at index: Int, with newItem: BaseMessageCellObjectProtocol) {
        guard chatItems.indices.contains(index) else { return }
        chatItems[index] = newItem
    }
    
    /// Заменить сообщения в интервале
    ///
    /// - Parameters:
    ///   - subrange: интервал индексов
    ///   - newItems: новые сообщения
    func replace(subrange: CountableRange<Int>, with newItems: [BaseMessageCellObjectProtocol]) {
        guard chatItems.indices.contains(subrange.lowerBound) else { return }
        guard chatItems.indices.contains(subrange.upperBound - 1) else { return }
        chatItems.replaceSubrange(subrange, with: newItems)
    }
    
    /// Удалить сообщения
    ///
    /// - Parameter shouldRemove: условие удаления
    /// - Returns: удаленные сообщения
    @discardableResult func remove(where shouldRemove: (BaseMessageCellObjectProtocol) -> Bool) -> [BaseMessageCellObjectProtocol] {
        
        var removedItems = [BaseMessageCellObjectProtocol]()
        
        chatItems = chatItems.filter { item in
            
            let willRemove = shouldRemove(item)
            
            if willRemove {
                removedItems.append(item)
            }
            
            return !willRemove
        }
        
        return removedItems
    }
}

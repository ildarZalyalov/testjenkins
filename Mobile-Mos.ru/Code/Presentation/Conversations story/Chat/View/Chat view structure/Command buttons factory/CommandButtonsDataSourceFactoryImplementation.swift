//
//  CommandButtonsDataSourceFactoryImplementation.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 23.05.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class CommandButtonsDataSourceFactoryImplementation: CommandButtonsDataSourceFactory {
    
    let goBackButtonTitle = "Вернуться назад ↩️"
    
    typealias KeyboardButton = CommandKeyboardView.KeyboardButtonViewModel
    
    //MARK: - CommandButtonsDataSourceFactory
    
    var noUserInputCommandButtonId = UUID().uuidString.lowercased()
    
    func buildDataSourceConfigurationForNoUserInput() -> CommandButtonsDataSourceConfiguration {
        
        let dataSource = CommandButtonsDataSource()
        var cellObjects = [[KeyboardButton]]()
        var selectionItems = [String : ChatReplyKeyboardButton]()
        
        var rowArray = [KeyboardButton]()
        
        let button = ChatReplyKeyboardButton(itemId: noUserInputCommandButtonId, title: goBackButtonTitle, isAlwaysActive: true, action: ChatMessageButtonAction())
        let cellObject = KeyboardButton(itemId: noUserInputCommandButtonId, title: goBackButtonTitle)
        
        rowArray.append(cellObject)
        selectionItems[cellObject.itemId] = button
        
        cellObjects.append(rowArray)
        
        dataSource.buttons = cellObjects
        dataSource.resizeKeyboardToFit = true
        
        return (dataSource, selectionItems)
    }
    
    func buildDataSourceConfiguration(from commandsMarkup: ChatReplyKeyboardMarkup) -> CommandButtonsDataSourceConfiguration {
        
        let dataSource = CommandButtonsDataSource()
        var cellObjects = [[KeyboardButton]]()
        var selectionItems = [String : ChatReplyKeyboardButton]()
        
        for (rowIndex, row) in commandsMarkup.buttons.enumerated() {
            
            var rowArray = [KeyboardButton]()
            
            for (buttonIndex, button) in row.enumerated() {
                
                let itemId = String(rowIndex) + String(buttonIndex)
                let cellObject = KeyboardButton(itemId: itemId, title: button.title)
                
                rowArray.append(cellObject)
                selectionItems[cellObject.itemId] = button
            }
            
            cellObjects.append(rowArray)
        }
        
        dataSource.buttons = cellObjects
        dataSource.resizeKeyboardToFit = commandsMarkup.resizeKeyboard
        
        return (dataSource, selectionItems)
    }
}

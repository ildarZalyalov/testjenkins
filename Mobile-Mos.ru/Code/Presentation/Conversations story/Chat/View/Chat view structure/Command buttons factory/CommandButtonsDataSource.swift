//
//  CommandButtonsDataSource.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 23.05.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Источник данных для командной клавиатуры
class CommandButtonsDataSource {
    
    typealias KeyboardButton = CommandKeyboardView.KeyboardButtonViewModel
    
    /// Набор рядов кнопок
    var buttons = [[KeyboardButton]]()
    
    /// Подогнать ли размер клавиатуры под кол-во кнопок (если полученная высота меньше стандартной высоты клавиатуры)
    var resizeKeyboardToFit = true
}

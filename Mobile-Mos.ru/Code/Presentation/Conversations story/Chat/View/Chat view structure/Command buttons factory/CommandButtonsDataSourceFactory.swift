//
//  CommandButtonsDataSourceFactory.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 23.05.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Конфигурация источника данных для командной клавиатуры (источник данных + словарь с кнопками по идентификатору)
typealias CommandButtonsDataSourceConfiguration = (dataSource: CommandButtonsDataSource, selectionItems: [String : ChatReplyKeyboardButton])

/// Фабрика для работы с источником данных для командной клавиатуры
protocol CommandButtonsDataSourceFactory {
    
    /// Идентификатор кнопки отправки сообщения с контекстом ошибки отсутствия возможности пользовательского ввода
    var noUserInputCommandButtonId: String { get }
    
    /// Создать источник данных для командной клавиатуры в случае ошибки отсутствия возможности пользовательского ввода
    ///
    /// - Returns: конфигурация источника данных для командной клавиатуры
    func buildDataSourceConfigurationForNoUserInput() -> CommandButtonsDataSourceConfiguration
    
    /// Создать источник данных для командной клавиатуры
    ///
    /// - Parameter commandsMarkup: разметка командной клавиатуры
    /// - Returns: конфигурация источника данных для командной клавиатуры
    func buildDataSourceConfiguration(from commandsMarkup: ChatReplyKeyboardMarkup) -> CommandButtonsDataSourceConfiguration
}

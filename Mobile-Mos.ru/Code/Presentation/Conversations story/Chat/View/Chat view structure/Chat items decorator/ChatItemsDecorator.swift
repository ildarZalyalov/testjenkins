//
//  ChatItemsDecorator.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 28.04.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import Chatto
import ChattoAdditions

/// Декоратор чата, добавляет к сообщениям ячейки с дополнительными элементами
final class ChatItemsDecorator: ChatItemsDecoratorProtocol {
    
    /// Маленький отступ
    let shortSeparation: CGFloat = 4
    
    /// Обычный отступ
    let normalSeparation: CGFloat = 12
    
    /// Отступ индикатора "вам отвечают" снизу
    let typingIndicatorBottomMargin: CGFloat = 40
    
    /// Отступ внешней инлайн клавиатуры от сообщения
    let outsideInlineSeparation: CGFloat = 4
    
    /// Отступ статуса сообщения от сообщения
    let messageStatusSeparation: CGFloat = 4
    
    /// Отступ сепаратора с датой и временем снизу и сверху от других ячеек
    let timeSeparatorSeparation: CGFloat = 17
    
    /// Календарь, используемый для подсчета надо ли отображать сепаратор с датой и временем
    let calendar = Calendar.autoupdatingCurrent
    
    
    /// Отступ последней ячейки в чате
    var lastItemSeparation: CGFloat = 7
    
    /// Надо ли отображать ячейку с индикатором загрузки истории вверху чата
    var shouldShowTopHistoryLoader: Bool = false
    
    /// Надо ли отображать ячейку с индикатором загрузки истории внизу чата
    var shouldShowBottomHistoryLoader: Bool = false
    
    /// Надо ли отображать ячейку с индикатором "вам отвечают"
    var shouldShowTypingIndicator: Bool = false {
        didSet {
            guard !oldValue && shouldShowTypingIndicator else { return }
            typingIdicatorCellObject = TypingIndicatorCellObject()
        }
    }
    
    /// Добавлять ли отступ снизу для индикатора "вам отвечают"
    var shouldUseTypingIndicatorBottomMargin = false
    
    fileprivate var typingIdicatorCellObject = TypingIndicatorCellObject()
    
    //MARK: - ChatItemsDecoratorProtocol
    
    func decorateItems(_ chatItems: [ChatItemProtocol]) -> [DecoratedChatItem] {
        
        var decoratedChatItems = [DecoratedChatItem]()
        
        if shouldShowTopHistoryLoader {
            decoratedChatItems.appendDecoratedItem(with: MessageLoaderCellObject(), bottomMargin: normalSeparation)
        }
        
        for (index, chatItem) in chatItems.enumerated() {
            
            let previous: ChatItemProtocol? = (index - 1 >= 0) ? chatItems[index - 1] : nil
            let next: ChatItemProtocol? = (index + 1 < chatItems.count) ? chatItems[index + 1] : nil
            
            var canShowTail = false
            var addTimeSeparator = false
            
            // определяем показывать ли хвостик и временной разделитель
            // хвостик показывается, если предыдущее сообщение было от другого пользователя или предыдущий элемент не сообщение
            // временной разделитель показывается, если предыдущее сообщение было отправлено вчера или предыдущий элемент не сообщение. но не для первого сообщения
            if let currentMessage = chatItem as? MessageModelProtocol {
                if let previousMessage = previous as? MessageModelProtocol {
                    canShowTail = currentMessage.senderId != previousMessage.senderId
                    addTimeSeparator = !calendar.isDate(currentMessage.date, inSameDayAs: previousMessage.date)
                } else {
                    canShowTail = true
                    addTimeSeparator = index > 0
                }
            }
            
            // если нам надо показать хвостик, убеждаемся, что можем это сделать
            // мы не можем показывать хвостики, если в сообщении нет текста
            if let messageItem = chatItem as? BaseMessageCellObjectProtocol, canShowTail {
                canShowTail = !messageItem.text.isEmpty
            }
            
            if let messageItem = chatItem as? MessageModelProtocol, addTimeSeparator {
                decoratedChatItems.updateLastItemBottomMargin(with: timeSeparatorSeparation)
                decoratedChatItems.appendDecoratedItem(with: TimeSeparatorCellObject(with: messageItem.date), bottomMargin: timeSeparatorSeparation)
            }
            
            let bottomMargin = separationAfterItem(chatItem, next: next)
            decoratedChatItems.appendDecoratedItem(with: chatItem, bottomMargin: bottomMargin, canShowFailedIcon: true, isShowingTail: canShowTail, isShowingAvatar: false)
            
            if let messageItem = chatItem as? MessageModelProtocol, messageItem.status == .failed {
                decoratedChatItems.updateLastItemBottomMargin(with: messageStatusSeparation)
                decoratedChatItems.appendDecoratedItem(with: MessageStatusCellObject(with: messageItem.status), bottomMargin: bottomMargin)
            }
            
            if let messageItem = chatItem as? BaseMessageCellObjectProtocol, !messageItem.inlineOutsideKeyboard.inlineButtons.isEmpty {
                decoratedChatItems.updateLastItemBottomMargin(with: outsideInlineSeparation)
                decoratedChatItems.appendDecoratedItem(with: InlineOutsideCellObject(with: messageItem), bottomMargin: bottomMargin)
            }
        }
        
        if shouldShowBottomHistoryLoader {
            decoratedChatItems.appendDecoratedItem(with: MessageLoaderCellObject(), bottomMargin: normalSeparation)
        }
        
        if shouldShowTypingIndicator {
            
            let bottomMargin = shouldUseTypingIndicatorBottomMargin ? typingIndicatorBottomMargin : 0
            let lastMessage = chatItems.last as? MessageModelProtocol
            let isShowingTail = !(lastMessage?.isIncoming ?? false)
            
            decoratedChatItems.appendDecoratedItem(with: typingIdicatorCellObject, bottomMargin: bottomMargin, isShowingTail: isShowingTail)
        }
        
        return decoratedChatItems
    }
    
    /// Считаем отступ для элемента
    ///
    /// - Parameters:
    ///   - current: элемент
    ///   - next: следующий элемент (если есть)
    /// - Returns: отступ для элемента снизу
    func separationAfterItem(_ current: ChatItemProtocol?, next: ChatItemProtocol?) -> CGFloat {
        
        guard let nextItem = next else {
            
            guard shouldShowTypingIndicator else { return lastItemSeparation }
            
            let isIncoming = (current as? MessageModelProtocol)?.isIncoming ?? false
            return isIncoming ? shortSeparation : normalSeparation
        }
        
        guard let currentMessage = current as? MessageModelProtocol else { return normalSeparation }
        guard let nextMessage = nextItem as? MessageModelProtocol else { return normalSeparation }
        
        if currentMessage.senderId != nextMessage.senderId {
            return normalSeparation
        } else {
            return shortSeparation
        }
    }
}

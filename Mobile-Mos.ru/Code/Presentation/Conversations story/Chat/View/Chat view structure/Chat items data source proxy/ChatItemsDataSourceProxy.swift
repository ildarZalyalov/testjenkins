//
//  ChatItemsDataSourceProxy.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 04.05.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import Chatto

/// Прокси для ChatDataSourceProtocol. Используется в ChatViewController, чтобы избежать циклических ссылок
class ChatItemsDataSourceProxy: ChatDataSourceProtocol {
    
    weak var dataSource: ChatDataSourceProtocol?
    
    init(with dataSource: ChatDataSourceProtocol?) {
        self.dataSource = dataSource
    }
    
    //MARK: - ChatDataSourceProtocol
    
    weak var delegate: ChatDataSourceDelegateProtocol? {
        get {
            return dataSource?.delegate
        }
        set {
            dataSource?.delegate = newValue
        }
    }
    
    var chatItems: [ChatItemProtocol] {
        return dataSource?.chatItems ?? []
    }
    
    var hasMoreNext: Bool {
        return dataSource?.hasMoreNext ?? false
    }
    
    var hasMorePrevious: Bool {
        return dataSource?.hasMorePrevious ?? false
    }
    
    func loadNext() {
        dataSource?.loadNext()
    }
    
    func loadPrevious() {
        dataSource?.loadPrevious()
    }
    
    func adjustNumberOfMessages(preferredMaxCount: Int?, focusPosition: Double, completion:(_ didAdjust: Bool) -> Void) {
        dataSource?.adjustNumberOfMessages(preferredMaxCount: preferredMaxCount, focusPosition: focusPosition, completion: completion)
    }
}

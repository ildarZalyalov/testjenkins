//
//  ChatInteractorInput.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 27.04.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import ChattoAdditions

protocol ChatInteractorInput: class {
    
    //MARK: - Свойства
    
    var isUserLoggedIn: Bool { get }
    
    var isExpandingLastCachedMessageGroup: Bool { get }
    
    var conversation: Conversation! { get }
    
    var conversationGreetingMessages: [ChatMessage] { get }
    
    var locationServicesAreAvailable: Bool? { get }
    
    var isRecipientTyping: Bool { get }
    
    //MARK: - Методы
    
    func configure(with conversation: Conversation)
    
    func setConversation(active: Bool)
    
    func requestLocationServicesAuthorization()
    
    func findUserLocation(toSendwith messageContext: String?)
    
    func geocode(location: Location, toSendWith messageContext: String?)
    
    func sendUserMessage(with text: String, messageContext: String?, hidingResponseTyping: Bool)
    
    func sendUserMessage(with text: String, messageContext: String?, hidingResponseTyping: Bool, mapObject: ChatMessageMapObject)
    
    func send(message: ChatMessage)
    
    func sendCallback(with data: String, for message: ChatMessage?, embeddedMessage: ChatMessage?)
    
    func obtainMessagesHistoryPortion(before messageId: Int64?, withLimit limit: Int)
    
    func obtainCachedMessagesHistoryPortion(before: Bool, message: ChatMessage?, withLimit limit: Int)
    
    func obtainCachedMessagesHistoryPortion(including message: ChatMessage, withLimit limit: Int)
    
    func obtainLastIncomingCachedMessage()
    
    func deleteFromCachedMessagesHistory(message: ChatMessage)
    
    func attemptRefreshMessagesHistory(withLimit limit: Int)
    
    func obtainExternalLinkPreviewIfNeeded(for message: ChatMessage)
    
    func attemptExpandingLastCachedMessageGroup(withUserMessageText text: String?, onlyIfGroupHistoryIsEmpty: Bool)
    
    func startDownloadingDocument(in message: ChatMessage)
    
    func cancelDownloadForDocument(in message: ChatMessage)
    
    //MARK: - Events
    
    func sendEvent(with eventName: String, eventJson: String)
    
    func setStatsDataToAnalyticsManager(_ statsData: StatsData)
    
    func sendDidLoadEvent(with chatId: String)
}

//
//  ChatInteractor.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 27.04.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import ChattoAdditions
import CoreLocation

class ChatInteractor: NSObject, ChatInteractorInput, SocketConnectionObserverDelegate, CLLocationManagerDelegate {
    
    weak var output: ChatInteractorOutput!
    
    var previewLoader: WeblinkPreviewLoader!
    var messageFactory: ChatMessageFactory!
    var analyticsManager: AnalyticsManager!
    var fileDownloadManager: FileDownloadManager!
    var fileCacheManager: FileCacheManager!
    
    var connectionObserver: SocketConnectionObserver! {
        
        didSet {
            
            guard connectionObserver != nil else {
                return
            }
            
            connectionObserver.delegate = self
            output.didChangeChatConnectionStatus(to: connectionObserver.isSocketConnected)
        }
    }
    
    var userService: UserService!
    var historyService: ChatMessageHistoryService!
    
    var conversationsService: ConversationsService! {
        
        didSet {
            
            guard conversationsService != nil else {
                return
            }
            
            conversationsService.cacheObsoleteHandler = { [weak self] in
                self?.conversation.shouldUpdateHistory = true
                self?.output?.didMakeMessageHistoryObsolete()
            }
        }
    }
    
    var messageService: ChatMessageService! {
        
        didSet {
            
            guard messageService != nil else {
                return
            }
            
            messageService.newMessageHandler = { [weak self] message in
                
                guard message.chatId == self?.conversation.itemId else { return }
                self?.output.didObtainNewMessages(messages: [message])
                
                if message.content.type == .messageGroup {
                    self?.attemptExpandingLastCachedMessageGroup()
                }
            }
            
            messageService.editMessageHandler = { [weak self] message in
                guard message.chatId == self?.conversation.itemId else { return }
                self?.output.didEdit(message: message)
            }
            
            messageService.failSendingMessagesHandler = { [weak self] in
                self?.output.didMarkAllSendingAsFailed()
            }
        }
    }
    
    var interactionService: ChatMessageInteractionService!
    
    var typingManager: ChatMessageTypingManager! {
        
        didSet {
            
            guard typingManager != nil else {
                return
            }
            
            typingManager.typingHandler = { [weak self] eventInfo in
                guard eventInfo.conversationId == self?.conversation.itemId else { return }
                guard eventInfo.sender !== self else { return }
                self?.output.didReceiveMessageTyping(eventSource: eventInfo.source)
            }
            
            typingManager.typingStoppedHandler = { [weak self] eventInfo in
                guard eventInfo.conversationId == self?.conversation.itemId else { return }
                guard eventInfo.sender !== self else { return }
                self?.output.didReceiveMessageTypingStopped(eventSource: eventInfo.source)
            }
        }
    }
    
    var yandexGeocoderService: YandexGeocoderService!
    var geocodeRequest: HTTPRequest?
    
    var locationMessageContext: String?
    var locationManager: CLLocationManager! {
        didSet { locationManager?.delegate = self }
    }
    
    var currentUser: User {
        return userService.currentUser
    }
    
    var isRefreshingMessagesHistory = false
    
    // пока хотим посмотреть без задержек, но старые значения сохранены т.к. могут быть возвращены
    let willExpandMessageInGroupNotificationDelay: TimeInterval = 0 // 0.15
    let expandingMessageInGroupDelay: TimeInterval = 0 // 1.75
    
    deinit {
        geocodeRequest?.cancel()
    }
    
    //MARK: - Приватные методы
    
    func sendUserMessage(with text: String, content: ChatMessageContent?, messageContext: String?, hidingResponseTyping: Bool) {
        
        let context = messageContext ?? ChatMessage.MessageContextValues.text.rawValue
        
        var message = messageService.createOutgoingMessage(from: text, messageContext: context, for: conversation, and: currentUser)
        message.replyMarkup.hideTypingIndicator = hidingResponseTyping
        if let messageContent = content {
            message.content = messageContent
        }
        
        output.didObtainNewMessages(messages: [message])
        send(message: message)
    }
    
    func scheduleExpandingMessage(in messageGroup: [ChatMessage]) {
        
        guard !messageGroup.isEmpty else {
            isExpandingLastCachedMessageGroup = false
            return
        }
        
        let notificationDelay = DispatchTime.now() + willExpandMessageInGroupNotificationDelay
        let delay = DispatchTime.now() + expandingMessageInGroupDelay
        
        var expandingMessageGroup = messageGroup
        let messageToExpand = expandingMessageGroup.removeFirst()
        let isLastMeessageInGroup = expandingMessageGroup.isEmpty
        let messageDoesNotHaveCommands = messageToExpand.replyMarkup.commandsMarkup?.buttons.isEmpty ?? true
        let messageHasTextKeyboard = messageToExpand.replyMarkup.showTextKeyboard
        
        DispatchQueue.main.asyncAfter(deadline: notificationDelay) { [weak self] in
            self?.output.willExpandLastCachedMessageGroup(messageDoesNotHaveCommands: messageDoesNotHaveCommands, messageHasTextKeyboard: messageHasTextKeyboard)
        }
        
        DispatchQueue.main.asyncAfter(deadline: delay, execute: { [weak self] in
            
            guard let strongSelf = self else { return }
            
            strongSelf.isExpandingLastCachedMessageGroup = !isLastMeessageInGroup
            strongSelf.output.didObtainNewMessages(messages: [messageToExpand])
            
            guard !isLastMeessageInGroup else { return }
            
            strongSelf.scheduleExpandingMessage(in: expandingMessageGroup)
        })
    }
    
    func attemptExpandingLastCachedMessageGroup(afterLoadingFromServerBefore messageId: Int64? = nil, afterLoadingFromCache result: ChatMessageCachedHistoryPortionObtainResult? = nil) {
        
        guard messageId == nil else { return }
        
        if let cacheResult = result {
            switch cacheResult {
            case .success(let history):
                guard !history.hasMoreNext else { return }
            default:
                break
            }
        }
        
        attemptExpandingLastCachedMessageGroup(withUserMessageText: nil, onlyIfGroupHistoryIsEmpty: true)
    }
    
    //MARK: - ChatInteractorInput
    
    var isUserLoggedIn: Bool {
        return currentUser.isLoggedIn
    }
    
    var isExpandingLastCachedMessageGroup = false
    
    var conversation: Conversation!
    
    var conversationGreetingMessages: [ChatMessage] {
        return conversation.greetingMessagesTexts.map { messageFactory.buildFakeIncomingMessage(from: $0, for: conversation, and: currentUser) }
    }
    
    var locationServicesAreAvailable: Bool? {
        return CLLocationManager.areLocationServicesAvailable
    }
    
    var isRecipientTyping: Bool {
        return typingManager.isTypingInConversation(with: conversation.itemId)
    }
    
    func configure(with conversation: Conversation) {
        self.conversation = conversation
    }
    
    func setConversation(active: Bool) {
        if active {
            conversationsService.activate(conversation: &conversation!)
        }
        else {
            conversationsService.deactivate(conversation: &conversation!)
        }
    }
    
    func requestLocationServicesAuthorization() {
        locationManager.requestWhenInUseAuthorization()
    }
    
    func findUserLocation(toSendwith messageContext: String?) {
        locationMessageContext = messageContext
        locationManager.requestLocation()
    }
    
    func geocode(location: Location, toSendWith messageContext: String?) {
        
        geocodeRequest?.cancel()
        geocodeRequest = yandexGeocoderService.geocodeCoordinate(location) { [weak self] result in
            
            var address: String? = nil
            
            switch result {
            case .success(let response):
                address = response.text
            default:
                break
            }
            
            self?.geocodeRequest = nil
            self?.output.didFinishGeocoding(location: location, with: address, toSendwith: messageContext)
        }
    }
    
    func sendUserMessage(with text: String, messageContext: String?, hidingResponseTyping: Bool) {
        sendUserMessage(with: text, content: nil, messageContext: messageContext, hidingResponseTyping: hidingResponseTyping)
    }
    
    func sendUserMessage(with text: String, messageContext: String?, hidingResponseTyping: Bool, mapObject: ChatMessageMapObject) {
        
        var content = ChatMessageContent()
        content.type = .map
        content.mapObject = mapObject
        
        sendUserMessage(with: text, content: content, messageContext: messageContext, hidingResponseTyping: hidingResponseTyping)
    }
    
    func send(message: ChatMessage) {
        messageService.send(message: message) { [weak self] sentMessage, didDeleteUnsent in
            self?.output.didSend(message: sentMessage, successfully: sentMessage.status == .success, didDeleteUnsent: didDeleteUnsent)
        }
        output.didBeginSending(message: message, with: TimeInterval(messageService.sendMessageTimeout))
    }
    
    func sendCallback(with data: String, for message: ChatMessage?, embeddedMessage: ChatMessage?) {
        interactionService.sendCallback(with: data, by: currentUser, for: message, embeddedMessage: embeddedMessage, in: conversation)
        output.didBeginSendingCallback(with: data, for: message)
    }
    
    func deleteFromCachedMessagesHistory(message: ChatMessage) {
        historyService.deleteFromCachedHistory(message: message)
    }
    
    func obtainMessagesHistoryPortion(before messageId: Int64?, withLimit limit: Int) {
        
        historyService.obtainHistoryPortion(before: messageId, for: conversation, and: currentUser, withLimit: limit) { [weak self] historyResult in
            
            var result: ChatMessageObtainMultipleResult
            
            switch historyResult {
            case .success(let history):
                self?.attemptExpandingLastCachedMessageGroup(afterLoadingFromServerBefore: messageId)
                result = .success(items: history.messages)
            case .failure(let error):
                result = .failure(error: error)
            }
            
            self?.output.didFinishObtainingMessagesHistoryPortion(result: result)
        }
    }
    
    func obtainCachedMessagesHistoryPortion(before: Bool, message: ChatMessage?, withLimit limit: Int) {
        
        if before {
            historyService.obtainCachedHistoryPortion(before: message, for: conversation, and: currentUser, withLimit: limit, completion: { [weak self] result in
                self?.output.didFinishObtainingCachedMessagesHistoryPortion(before: before, message: message, with: result)
                guard message == nil else { return }
                self?.attemptExpandingLastCachedMessageGroup(afterLoadingFromCache: result)
            })
        }
        else {
            historyService.obtainCachedHistoryPortion(after: message, for: conversation, and: currentUser, withLimit: limit, completion: { [weak self] result in
                self?.output.didFinishObtainingCachedMessagesHistoryPortion(before: before, message: message, with: result)
                self?.attemptExpandingLastCachedMessageGroup(afterLoadingFromCache: result)
            })
        }
    }
    
    func obtainCachedMessagesHistoryPortion(including message: ChatMessage, withLimit limit: Int) {
        historyService.obtainCachedHistoryPortion(including: message, for: conversation, and: currentUser, withLimit: limit) { [weak self] result in
            self?.output.didFinishObtainingCachedMessagesHistoryPortion(including: message, with: result)
        }
    }
    
    func obtainLastIncomingCachedMessage() {
        historyService.obtainLastIncomingCachedMessage(for: conversation, and: currentUser) { [weak self] result in
            self?.output.didFinishObtainingLastIncomingCachedMessage(with: result)
        }
    }
    
    func attemptRefreshMessagesHistory(withLimit limit: Int) {
        
        guard !isRefreshingMessagesHistory else { return }
        isRefreshingMessagesHistory = true
        
        historyService.obtainHistoryPortion(before: nil, for: conversation, and: currentUser, withLimit: limit) { [weak self] historyResult in
            
            guard let strongSelf = self else { return }
            strongSelf.isRefreshingMessagesHistory = false
            
            switch historyResult {
                
            case .success(let history):
                
                strongSelf.conversationsService.markMessageHistoryRefreshed(for: strongSelf.conversation)
                strongSelf.conversation.shouldUpdateHistory = false
                
                if !history.messages.isEmpty {
                    strongSelf.attemptExpandingLastCachedMessageGroup()
                }
                
                let result = ChatMessageObtainMultipleResult.success(items: history.messages)
                strongSelf.output.didFinishRefreshingMessagesHistoryPortion(result: result)
                
            default:
                return
            }
        }
    }
    
    func obtainExternalLinkPreviewIfNeeded(for message: ChatMessage) {
        
        var url: URL?
        var requireUserGeneratedImages = false
        
        switch message.content.type {
        case .weblink:
            requireUserGeneratedImages = message.content.weblinkObject?.type == .social
            let disablePreview = message.content.weblinkObject?.disablePreview ?? false
            if !disablePreview && message.content.weblinkObject?.preview == nil {
                url = message.content.weblinkObject?.linkUrl
            }
        default:
            url = nil
        }
        
        guard url != nil else { return }
        
        var messageWithPreview = message
        
        previewLoader.obtainPreview(for: url!, requireUserGeneratedImages: requireUserGeneratedImages) { [weak self] data in
            
            guard data.title != nil || data.description != nil else { return }
            
            var preview = ChatMessageExternalResourcePreview()
            preview.title = data.title
            preview.description = data.description
            
            if let photoUrlString = data.topImage, let photoUrl = URL(string: photoUrlString) {
                preview.photoUrls = [photoUrl]
            }
            
            switch messageWithPreview.content.type {
            case .weblink:
                if messageWithPreview.content.weblinkObject?.type == .social {
                    preview.title = nil
                }
                messageWithPreview.content.weblinkObject?.preview = preview
            default:
                return
            }
            
            self?.historyService.updateInCachedHistory(message: messageWithPreview)
            self?.output.didEdit(message: messageWithPreview)
        }
    }
    
    func attemptExpandingLastCachedMessageGroup(withUserMessageText text: String?, onlyIfGroupHistoryIsEmpty: Bool) {
        
        guard !isExpandingLastCachedMessageGroup else { return }
        isExpandingLastCachedMessageGroup = true
        
        historyService.obtainLastCachedMessageGroup(for: conversation, and: currentUser) { [weak self] result in
            
            guard let strongSelf = self else { return }
            
            var didExpandMessageGroup = false
            
            defer {
                if !didExpandMessageGroup {
                    strongSelf.isExpandingLastCachedMessageGroup = false
                }
            }
            
            var messageGroup: ChatMessage?
            
            switch result {
            case .success(let message):
                messageGroup = message
            case .failure(_):
                messageGroup = nil
            }
            
            guard messageGroup != nil else { return }
            guard var messageGroupObject = messageGroup!.content.messageGroupObject else { return }
            
            if onlyIfGroupHistoryIsEmpty {
                guard messageGroupObject.messageGroupHistory.isEmpty else { return }
            }
            
            guard var messagesToExpand = messageGroupObject.messageGroups.first else { return }
            messageGroupObject.messageGroups.removeFirst()
            
            if let userText = text {
                
                let context = ChatMessage.MessageContextValues.text.rawValue
                var userMessage = strongSelf.messageService.createOutgoingMessage(from: userText,
                                                                                  messageContext: context,
                                                                                  for: strongSelf.conversation,
                                                                                  and: strongSelf.currentUser)
                userMessage.parentMessageUid = messageGroup!.uid
                userMessage.status = .success
                
                messageGroupObject.messageGroupHistory.append(userMessage)
                strongSelf.output.didObtainNewMessages(messages: [userMessage])
            }
            
            messagesToExpand.forEachMutate { $0.date = Date() }
            messageGroupObject.messageGroupHistory.append(contentsOf: messagesToExpand)
            
            messageGroup!.content.messageGroupObject = messageGroupObject
            strongSelf.historyService.updateInCachedHistory(message: messageGroup!)
            
            didExpandMessageGroup = true
            
            for message in messagesToExpand {
                guard let statsData = message.statsData else { continue }
                strongSelf.sendEvent(with: statsData.eventName, eventJson: statsData.eventJson)
            }
            
            strongSelf.scheduleExpandingMessage(in: messagesToExpand)
        }
    }
    
    func startDownloadingDocument(in message: ChatMessage) {
        
        guard let url = message.content.documentObject?.fileUrl else { return }
        
        fileDownloadManager.downloadFile(at: url) { [weak self] result in
            
            guard let strongSelf = self else { return }
            
            switch result {
            case .success(_, let url):
                if let cacheKey = strongSelf.messageFactory.cacheKeyForDocument(in: message) {
                    try? strongSelf.fileCacheManager.addItem(at: url, toCacheFor: cacheKey)
                }
            default:
                break
            }
            
            strongSelf.messageFactory.updateDocumentDownloadStateIfNeeded(in: message)
        }
        
        messageFactory.updateDocumentDownloadStateIfNeeded(in: message)
    }
    
    func cancelDownloadForDocument(in message: ChatMessage) {
        
        guard let url = message.content.documentObject?.fileUrl else { return }
        
        fileDownloadManager.cancelDownloadForFile(at: url)
        messageFactory.updateDocumentDownloadStateIfNeeded(in: message)
    }
    
    //MARK: - Helpers
    
    func sendEvent(with eventName: String, eventJson: String) {
        if !eventJson.isEmpty {
            analyticsManager.sendSomeEvent(with: eventName, eventJson: eventJson, errorText: nil)
        }
    }
    
    func setStatsDataToAnalyticsManager(_ statsData: StatsData) {
        analyticsManager.setStatsData(with: statsData)
    }
    
    func sendDidLoadEvent(with chatId: String) {
        
        guard let chat = ConversationIdentificator.init(rawValue: chatId) else { return }
        
        let eventName = AnalyticsConstants.screenLoadedEventName
        switch chat {
        case .children:
            analyticsManager.sendChildsConversationEvent(with: .screenLoaded(withString: eventName), aboutAppData: nil, appSharingSource: nil)
        case .transport:
            analyticsManager.sendTransportConversationEvent(with: .screenLoaded(withString: eventName), aboutAppData: nil, appSharingSource: nil)
        case .myHome:
            analyticsManager.sendMyHomeConversationEvent(with: .screenLoaded(withString: eventName), aboutAppData: nil, appSharingSource: nil)
        case .health:
            analyticsManager.sendMedecineEvent(with: .screenLoaded(withString: eventName), aboutAppData: nil, appSharingSource: nil)
        case .cityNews:
            analyticsManager.sendNewsConversationEvent(with: .screenLoaded(withString: eventName), aboutAppData: nil, appSharingSource: nil)
        case .qa:
            analyticsManager.sendConsultationConversationEvent(with: .screenLoaded(withString: eventName), aboutAppData: nil, appSharingSource: nil)
        default:
            return
        }
    }
    
    //MARK: - SocketConnectionObserverDelegate 
    
    func socketObserver(observer: SocketConnectionObserver, observedConnectionChangeTo connected: Bool) {
        output.didChangeChatConnectionStatus(to: connected)
    }
    
    //MARK: - CLLocationManagerDelegate
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last ?? manager.location
        output.didFinishFindingUserLocation(with: Location(with: location), toSendwith: locationMessageContext)
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        output.didFinishFindingUserLocation(with: Location(with: manager.location), toSendwith: locationMessageContext)
    }
}

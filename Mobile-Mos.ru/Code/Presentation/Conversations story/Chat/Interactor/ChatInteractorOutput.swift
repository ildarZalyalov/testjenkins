//
//  ChatInteractorOutput.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 27.04.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol ChatInteractorOutput: class {
    
    func didFinishFindingUserLocation(with location: Location?, toSendwith messageContext: String?)
    
    func didFinishGeocoding(location: Location, with address: String?, toSendwith messageContext: String?)
    
    func didObtainNewMessages(messages: [ChatMessage])
    
    func didBeginSending(message: ChatMessage, with timeout: TimeInterval)
    
    func didSend(message: ChatMessage, successfully success: Bool, didDeleteUnsent: Bool)
    
    func didBeginSendingCallback(with data: String, for message: ChatMessage?)
    
    func didEdit(message: ChatMessage)
    
    func didMarkAllSendingAsFailed()
    
    func didFinishObtainingMessagesHistoryPortion(result: ChatMessageObtainMultipleResult)
    
    func didFinishObtainingCachedMessagesHistoryPortion(before: Bool, message: ChatMessage?, with result: ChatMessageCachedHistoryPortionObtainResult)
    
    func didFinishObtainingCachedMessagesHistoryPortion(including message: ChatMessage, with result: ChatMessageCachedHistoryPortionObtainResult)
    
    func didFinishObtainingLastIncomingCachedMessage(with result: ChatMessageObtainResult)
    
    func didFinishRefreshingMessagesHistoryPortion(result: ChatMessageObtainMultipleResult)
    
    func didChangeChatConnectionStatus(to connected: Bool)
    
    func didMakeMessageHistoryObsolete()
    
    func didReceiveMessageTyping(eventSource: ChatMessageTypingEventSource)
    
    func didReceiveMessageTypingStopped(eventSource: ChatMessageTypingEventSource)
    
    func willExpandLastCachedMessageGroup(messageDoesNotHaveCommands: Bool, messageHasTextKeyboard: Bool)
}

//
//  ChatCustomControlResultHandler.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 06.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Оработчик результата работы кастомного контрола
protocol ChatCustomControlResultHandler: class {
    
    /// Отправить сообщение
    ///
    /// - Parameters:
    ///   - text: текст сообщения
    ///   - context: контекст сообщения
    func handleSendMessage(with text: String, context: String?)
    
    /// Отправить сообщение с местоположением
    ///
    /// - Parameters:
    ///   - text: текст сообщения
    ///   - location: местоположение
    ///   - locationTitle: назавание места
    ///   - locationDescription: описание места
    ///   - context: контекст сообщения
    func handleSendMessage(with text: String, location: Location, locationTitle: String, locationDescription: String?, context: String?)
    
    /// Отправить коллбэк запрос
    ///
    /// - Parameters:
    ///   - callbackData: данные для запроса
    ///   - message: сообщение, с которым был свзяан контрол (если есть)
    ///   - embeddedMessage: вложенное сообщение, если сообщение является каруселью
    func handleSendCallback(with callbackData: String, for message: ChatMessage?, embeddedMessage: ChatMessage?)
}

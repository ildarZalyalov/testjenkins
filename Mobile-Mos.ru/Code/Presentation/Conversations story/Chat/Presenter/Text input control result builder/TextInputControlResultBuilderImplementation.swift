//
//  TextInputControlResultBuilderImplementation.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 29.09.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class TextInputControlResultBuilderImplementation: TextInputControlResultBuilder {
    
    var action: ChatMessageButtonAction?
    
    var message: ChatMessage?
    var embeddedMessage: ChatMessage?
    
    /// Символ, которым заменяются символы "безопасных" полей при отправке в сообщении
    let secureInputPlaceholderSymbol = "●"
    
    /// Максимальная длина строки для "безопасных" полей при отправке в сообщении
    let secureInputPlaceholderMaxCount = 12
    
    //MARK: - Приватные методы
    
    /// Получить текст с описанием ошибки валидации для поля
    ///
    /// - Parameters:
    ///   - input: поле ввода
    ///   - value: значение в поле
    /// - Returns: текст с описанием ошибки
    fileprivate func errorMessage(for input: ChatMessageTextInputControl.TextInput, with value: String?) -> String {
        
        var errorMessage: String
        
        if let fieldTitle = input.placeholder {
            errorMessage = String(format: StringsHelper.textInputsControlErrorInFieldMessageFormat, fieldTitle)
        }
        else if let text = value {
            errorMessage = String(format: StringsHelper.textInputsControlErrorInValueMessageFormat, text)
        }
        else {
            errorMessage = StringsHelper.textInputsControlErrorInUnknownFieldMessage
        }
        
        if let hint = input.title {
            errorMessage += "\n" + String(format: StringsHelper.textInputsControlErrorHintMessageFormat, hint)
        }
        
        return errorMessage
    }
    
    //MARK: - TextInputControlResultBuilder
    
    func validateInputValues(inputValuesById: [String : String]) -> String? {
        
        guard let control = action?.textInput else { return nil }
        
        for input in control.inputs {
            
            guard let regexString = input.formatRegex else { continue }
            guard let text = inputValuesById[input.inputId], !text.isEmpty else {
                return errorMessage(for: input, with: nil)
            }
            
            do {
    
                let regex = try NSRegularExpression(pattern: regexString, options: [.caseInsensitive, .anchorsMatchLines])
                let checkRange = NSMakeRange(0, text.count)
                let matchedRange = regex.rangeOfFirstMatch(in: text, options: [], range: checkRange)
                
                if !NSEqualRanges(checkRange, matchedRange) {
                    return errorMessage(for: input, with: text)
                }
            }
            catch _ {
                continue
            }
        }
        
        return nil
    }
    
    func buildMessageText(with inputValuesById: [String : String]) -> String? {
        
        guard let control = action?.textInput else { return nil }
        
        var text = String()
        
        for input in control.inputs {
            
            guard var value = inputValuesById[input.inputId] else { continue }
            
            if let title = input.placeholder {
                text += "\(title): "
            }
            
            if input.isSecure {
                
                var count = value.count
                if count > secureInputPlaceholderMaxCount {
                    count = secureInputPlaceholderMaxCount
                }
                
                value = String(repeating: secureInputPlaceholderSymbol, count: count)
            }
            
            text += "\(value)\n"
        }
        
        return text.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }
    
    func buildCallbackData(with inputValuesById: [String : String]) -> String? {
        
        guard let data = action?.callbackData else { return nil }
        guard !inputValuesById.isEmpty else { return data }
        
        let jsonDictionary: [String : Any] = [
            "callback" : data,
            "control_value" : inputValuesById
        ]
        
        guard let jsonData = try? JSONSerialization.data(withJSONObject: jsonDictionary, options: []) else { return nil }
        
        return String(data: jsonData, encoding: .utf8)
    }
}

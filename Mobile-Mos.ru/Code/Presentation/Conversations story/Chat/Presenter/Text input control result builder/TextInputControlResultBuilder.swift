//
//  TextInputControlResultBuilder.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 29.09.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Билдер результата работы контрола с несколькими полями ввода для отправки на сервер
protocol TextInputControlResultBuilder {
    
    /// "Действие" кнопки, в которой задан контрол с несколькими полями ввода
    var action: ChatMessageButtonAction? { get set }
    
    /// Сообщение, в котором была кнопка с контролом
    var message: ChatMessage? { get set }
    
    /// Вложенное сообщение, если сообщение является каруселью
    var embeddedMessage: ChatMessage? { get set }
    
    /// Провалидировать введенные значения согласно регулярным выражениям, определенным для контрола
    ///
    /// - Parameter inputValuesById: введенные значения (ключи - идентификаторы полей)
    /// - Returns: сообщение об ошибке или nil, если валидация прошла успешно
    func validateInputValues(inputValuesById: [String : String]) -> String?
    
    /// Построить текст для отправки введенных значений в сообщении
    ///
    /// - Parameter inputValuesById: введенные значения (ключи - идентификаторы полей)
    /// - Returns: текст для исходящего сообщения
    func buildMessageText(with inputValuesById: [String : String]) -> String?
    
    /// Построить данные для отправки введенных значений в коллбэк запросе на сервер
    ///
    /// - Parameter inputValuesById: inputValuesById: введенные значения (ключи - идентификаторы полей)
    /// - Returns: данные для отправки в коллбэк запросе
    func buildCallbackData(with inputValuesById: [String : String]) -> String?
}

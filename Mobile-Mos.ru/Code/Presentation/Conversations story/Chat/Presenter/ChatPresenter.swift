//
//  ChatPresenter.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 27.04.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import ChattoAdditions

class ChatPresenter: ChatViewOutput, ChatInteractorOutput, ChatMessageButtonActionHandler, ChatCustomControlResultHandler, ChatMessageUnsentActionsAlertEventHandler, PaymentProcessDelegate, ChatMessageAskAuthorizationEventHandler {
    
    weak var view: ChatViewInput!
    var router: ChatRouterInput!
    var interactor: ChatInteractorInput!
    
    var dataSourceFactory: ChatDataSourceFactory!
    var dataSourceConfiguration: ChatDataSourceConfiguration!
    
    var commandsDataSourceFactory: CommandButtonsDataSourceFactory!
    var commandsById: [String : ChatReplyKeyboardButton]!
    
    var textInputResultBuilder: TextInputControlResultBuilder!
    
    var hapticFeedManager: HapticFeedbackManager?
    var soundsManager: SoundsManager!
    
    var isLoadingMessages = false
    var isLoadingHistoryFromServer = false
    var shouldHideResponseTyping = false
    
    let messageHistoryPartMessageCount = 20
    
    var messageHistoryRefreshMessageCount: Int {
        return conversation.type != .media ? messageHistoryPartMessageCount : 1
    }
    
    var isChatEmpty: Bool {
        return dataSourceConfiguration == nil
    }
    
    var canLoadHistoryFromServer: Bool {
        guard conversation.type != .media else { return false }
        guard !hasMoreMessagesPrevious else { return false }
        return interactor.isUserLoggedIn && !isLoadingHistoryFromServer
    }
    
    var conversation: Conversation {
        return interactor.conversation
    }
    var initialMessage: ChatMessage?
    
    var dataSource: ChatDataSource {
        return dataSourceConfiguration.dataSource
    }
    var messageById: [String : ChatMessage] {
        return dataSourceConfiguration.selectionItems
    }
    
    //MARK: - Приватные методы
    
    func loadMessages(inHistory: Bool, fromServer: Bool = false) {
        
        guard !isLoadingMessages else { return }
        
        isLoadingMessages = true
        isLoadingHistoryFromServer = fromServer
        
        var referenceMessage: ChatMessage?
        
        if isChatEmpty {
            referenceMessage = initialMessage
        }
        else {
            
            let chatItems = dataSourceConfiguration?.dataSource.chatItems
            let messageUid = inHistory ? chatItems?.first?.messageUid : chatItems?.last?.messageUid
            
            if messageUid != nil {
                referenceMessage = messageById[messageUid!]
            }
        }
        
        if inHistory && isLoadingHistoryFromServer {
            
            if isChatEmpty {
                view.showLoadingContentStatus()
            }
            
            interactor.obtainMessagesHistoryPortion(before: referenceMessage?.messageId, withLimit: messageHistoryPartMessageCount)
        }
        else if isChatEmpty && referenceMessage != nil {
            interactor.obtainCachedMessagesHistoryPortion(including: referenceMessage!,
                                                          withLimit: messageHistoryPartMessageCount)
        }
        else {
            interactor.obtainCachedMessagesHistoryPortion(before: inHistory,
                                                          message: referenceMessage,
                                                          withLimit: messageHistoryPartMessageCount)
        }
    }
    
    func display(isTyping: Bool, eventSource: ChatMessageTypingEventSource) {
        
        let supressedEventsSources: Set<ChatMessageTypingEventSource> = [.messageSent, .messageObtained]
        
        guard !supressedEventsSources.contains(eventSource) else { return }
        guard view.showsTypingIndicator != isTyping else { return }
        
        view.showsTypingIndicator = isTyping
        view.display(dataSource: dataSource, for: .regular, completion: nil)
    }
    
    func display(messages: [ChatMessage], newMesagesCount: Int, completion: (() -> ())?) {
        
        let updateType: ChatViewUpdateType = isChatEmpty ? .reload : .regular
        
        if isChatEmpty {
            dataSourceConfiguration = dataSourceFactory.buildDataSourceConfiguration(from: messages)
        }
        else {
            dataSourceFactory.append(newMessages: messages, to: &dataSourceConfiguration!)
        }
        
        view.display(dataSource: dataSource, for: updateType, newMesagesCount: newMesagesCount, completion: completion)
    }
    
    func displayHistoryPart(with historyMessages: [ChatMessage], prependingToCurrentMessages: Bool, canUpdateInputUI: Bool) {
        
        var messages = historyMessages
        
        if canUpdateInputUI {
            
            if isChatEmpty && !hasMoreMessagesNext {
                
                if messages.filter({ $0.isIncoming && $0.status == .success }).isEmpty {
                    view.displayEmptyChatState()
                }
                else {
                    updateUIWithLastIncomingMessage(for: messages, fromHistory: true)
                }
            }
            else {
                interactor.obtainLastIncomingCachedMessage()
            }
        }
        
        if prependingToCurrentMessages && !hasMoreMessagesPrevious {
            messages.insert(contentsOf: interactor.conversationGreetingMessages, at: 0)
        }
        
        guard !messages.isEmpty else {

            if !isChatEmpty {
                view.display(dataSource: dataSource, for: .pagination, completion: nil)
            }
            
            return
        }
        
        if isChatEmpty {
            
            view.showsTypingIndicator = interactor.isRecipientTyping
            display(messages: messages, newMesagesCount: 0, completion: nil)
            
            if let messageUid = initialMessage?.uid {
                view.scrollChatToMessage(with: messageUid, animated: false)
            }
        }
        else {
            
            if prependingToCurrentMessages {
                dataSourceFactory.prepend(history: messages, to: &dataSourceConfiguration!)
            }
            else {
                dataSourceFactory.append(newMessages: messages, to: &dataSourceConfiguration!)
            }
            
            view.display(dataSource: dataSource, for: .pagination, completion: nil)
        }
        
        if conversation.shouldUpdateHistory {
            interactor.attemptRefreshMessagesHistory(withLimit: messageHistoryRefreshMessageCount)
        }
    }
    
    func updateUIWithLastIncomingMessage(for messages: [ChatMessage], fromHistory: Bool) {
        
        guard var lastIncomingMessage = messages.last(where: { $0.isIncoming }) else {
            return
        }
        
        if lastIncomingMessage.content.type == .messageGroup,
            let lastMessageInGroup = lastIncomingMessage.content.messageGroupObject?.messageGroupHistory.last {
            lastIncomingMessage = lastMessageInGroup
        }
        
        updateUserInput(with: lastIncomingMessage, fromHistory: fromHistory)
        view.updateScenarioTitle(with: lastIncomingMessage.scenarioTitle, animated: !fromHistory)
    }
    
    func updateUserInput(with message: ChatMessage, fromHistory: Bool) {
        
        shouldHideResponseTyping = message.replyMarkup.hideTypingIndicator
        
        let commandsMarkup = message.replyMarkup.commandsMarkup
        let shouldShowCommands = !(commandsMarkup?.buttons.isEmpty ?? true)
        var configuration: CommandButtonsDataSourceConfiguration?
        
        if shouldShowCommands, commandsMarkup != nil {
            configuration = commandsDataSourceFactory.buildDataSourceConfiguration(from: commandsMarkup!)
            commandsById = configuration!.selectionItems
        }
        
        let shouldDelayIfUnhidingInput = conversation.type == .media && !interactor.isExpandingLastCachedMessageGroup
        
        if message.replyMarkup.showTextKeyboard {
            view.showTextInput(allowSwitchingTo: configuration?.dataSource)
        }
        else if shouldShowCommands, configuration != nil {
            view.showCommands(commands: configuration!.dataSource, animated: !fromHistory, delayUnhidingInput: shouldDelayIfUnhidingInput)
        }
        else if conversation.type == .media && interactor.isExpandingLastCachedMessageGroup {
            view.hideInput(animated: true)
        }
        else {
            
            let configuration = commandsDataSourceFactory.buildDataSourceConfigurationForNoUserInput()
            commandsById = configuration.selectionItems
            
            view.showCommands(commands: configuration.dataSource, animated: !fromHistory, delayUnhidingInput: shouldDelayIfUnhidingInput)
        }
    }
    
    func tryFindButton(with buttonId: String, in message: ChatMessage) -> ChatReplyKeyboardButton? {
        
        let tryFindButton: ([[ChatReplyKeyboardButton]]?) -> ChatReplyKeyboardButton? = { buttons in
            
            if let button = buttons?.lazy.reduce([], +).first(where: { $0.itemId == buttonId }) {
                return button
            }
            
            return nil
        }
        
        if let button = tryFindButton(message.replyMarkup.inlineMarkup?.buttons) {
            return button
        }
        if let button = tryFindButton(message.replyMarkup.inlineOutsideMarkup?.buttons) {
            return button
        }
        if let button = tryFindButton(message.replyMarkup.commandsMarkup?.buttons) {
            return button
        }
        
        return nil
    }
    
    typealias MessageInScrollWithInlineButton = (childMessage: ChatMessage, button: ChatReplyKeyboardButton)
    func tryFindButton(with buttonId: String, inMessageScroll message: ChatMessage) -> MessageInScrollWithInlineButton? {
        
        guard let messageScrollObject = message.content.messageScrollObject else { return nil }
        
        for childMessage in messageScrollObject.messages {
            
            let inlineMarkup = childMessage.replyMarkup.inlineMarkup
            let inlineButtons: [ChatReplyKeyboardButton] = inlineMarkup?.buttons.reduce([], +) ?? []
            
            if let button = inlineButtons.first(where: { $0.itemId == buttonId }) {
                return (childMessage, button)
            }
        }
        
        return nil
    }
    
    func triggerChatButtonPressedFeedback() {
        hapticFeedManager?.triggerSelectionFeedback()
    }
    
    func triggerChatMessageIncomingFeedback() {
        soundsManager.playCustomSound(.messageIn)
        hapticFeedManager?.triggerImpactFeedback(with: .light)
    }
    
    func triggerChatMessageSentFailureFeedback() {
        soundsManager.playCustomSound(.messageError)
        hapticFeedManager?.triggerNotificationFeedback(with: .error)
    }
    
    //MARK: - ChatViewOutput
    
    var hasMoreMessagesPrevious: Bool = true
    
    var hasMoreMessagesNext: Bool = false
    
    func setupInitialState() {
        loadMessages(inHistory: true)
    }
    
    func configure(with configuration: ChatModuleConfiguration) {
        
        interactor.configure(with: configuration.conversation)
        interactor.sendDidLoadEvent(with: configuration.conversation.itemId)
        
        guard configuration.message?.chatId == configuration.conversation.itemId else { return }
        initialMessage = configuration.message
    }
    
    func chatWillAppear() {
        interactor.setConversation(active: true)
    }
    
    func chatWillScrollToBottom() {
        
        guard hasMoreMessagesNext else { return }
        
        dataSourceConfiguration = nil
        initialMessage = nil
        setupInitialState()
    }
    
    func chatWillDisappear() {
        interactor.setConversation(active: false)
    }
    
    func didPressChatAvatar() {
        router.showConversationInfo(for: conversation)
    }
    
    func loadMessagesHistory() {
        loadMessages(inHistory: true)
    }
    
    func loadMessagesRecent() {
        loadMessages(inHistory: false)
    }
    
    func didPressChatActivationButton(with title: String) {
        let text = title.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let context = ChatMessage.MessageContextValues.start.rawValue
        interactor.sendUserMessage(with: text, messageContext: context, hidingResponseTyping: shouldHideResponseTyping)
    }
    
    func didPressSendButton(with messageText: String) {
        let text = messageText.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        interactor.sendUserMessage(with: text, messageContext: nil, hidingResponseTyping: shouldHideResponseTyping)
    }
    
    func didSubmitTextInput(with inputValuesById: [String : String]) {
        
        if let errorMessage = textInputResultBuilder.validateInputValues(inputValuesById: inputValuesById) {
            router.showErrorAlert(with: errorMessage)
            return
        }
        
        view.clearTextFieldsInput()
        
        if let context = textInputResultBuilder.action?.messageContext {
            if let messageText = textInputResultBuilder.buildMessageText(with: inputValuesById) {
                interactor.sendUserMessage(with: messageText, messageContext: context, hidingResponseTyping: shouldHideResponseTyping)
            }
        }
        
        if let callbackData = textInputResultBuilder.buildCallbackData(with: inputValuesById){
            interactor.sendCallback(with: callbackData, for: textInputResultBuilder.message, embeddedMessage: textInputResultBuilder.embeddedMessage)
        }
    }
    
    func didSelectCommand(with itemId: String) {
        
        guard let command = commandsById[itemId] else { return }
        
        triggerChatButtonPressedFeedback()
        
        perform(action: command.action, forButtonWithId: command.itemId, andTitle: command.title, in: nil, embeddedMessage: nil)
    }
    
    func didTapOnMessage(with messageId: String) {
        
        guard let message = messageById[messageId] else {
            return
        }
        
        switch message.content.type {
        case .map:
            if let mapObject = message.content.mapObject {
                router.showMapModule(for: mapObject)
            }
        case .weblink:
            if let weblinkObject = message.content.weblinkObject {
                router.showWebBrowser(for: weblinkObject.linkUrl, with: conversation.style)
                guard !weblinkObject.statsData.eventJson.isEmpty else { return }
                interactor.setStatsDataToAnalyticsManager(weblinkObject.statsData)
            }
        default:
            return
        }
    }
    
    func didTapOn(url: URL, inMessageWith messageId: String) {
        router.showWebBrowser(for: url, with: conversation.style)
    }
    
    func didTapOnDocumentIconInMessage(with messageId: String) {
        
        guard let message = messageById[messageId] else {
            return
        }
        
        guard let documentObject = message.content.documentObject else { return }
        let status = documentObject.downloadState.status.value
        
        switch status {
        case .notDownloaded:
            interactor.startDownloadingDocument(in: message)
        case .downloading:
            interactor.cancelDownloadForDocument(in: message)
        case .downloadFinished:
            if let fileUrl = documentObject.downloadState.fileCacheUrl {
                interactor.setStatsDataToAnalyticsManager(documentObject.statsData)
                router.showOpenWithDialogForFile(at: fileUrl, in: message)
            }
        }
    }
    
    func didTapOnPhotoInMessage(with messageId: String) {
        
        guard let message = messageById[messageId] else {
            return
        }
        
        router.showPhotoGalleryForPhotos(in: message)
    }
    
    func didSelectInlineCommand(with itemId: String, inMessageWith messageId: String) {
        
        guard let message = messageById[messageId] else { return }
        
        var button: ChatReplyKeyboardButton?
        var embeddedMessage: ChatMessage?
        
        if message.content.messageScrollObject != nil {
            if let messageAndButton = tryFindButton(with: itemId, inMessageScroll: message) {
                embeddedMessage = messageAndButton.childMessage
                button = messageAndButton.button
            }
        }
        else {
            button = tryFindButton(with: itemId, in: message)
        }
        
        guard button != nil else { return }
        
        triggerChatButtonPressedFeedback()
        
        perform(action: button!.action, forButtonWithId: button!.itemId, andTitle: button!.title, in: message, embeddedMessage: embeddedMessage)
    }
    
    func didSelectOption(with type: OptionsKeyboardButtonType, inMessageWith messageId: String) {
        
        guard let message = messageById[messageId] else { return }
        
        switch type {
        case .share:
            router.showSharingModule(with: message.content.sharingItems, completion: { [weak self] completed in
                guard completed else { return }
                guard let callbackData = message.optionsMarkup.shareCallbackData else { return }
                self?.interactor.sendCallback(with: callbackData, for: message, embeddedMessage: nil)
            })
        case .openWeblink:
            if let weblinkObject = message.content.weblinkObject {
                router.showWebBrowser(for: weblinkObject.linkUrl, with: conversation.style)
            }
        }
    }
    
    func didTapOnFailIconOfMessage(with messageId: String) {
        
        guard let message = messageById[messageId] else {
            return
        }
        
        router.showUnsentActionsModule(for: message, eventHandler: self)
    }
    
    func didUpdateVisualState(for messageCellObject: BaseMessageCellObjectProtocol) {
        dataSourceConfiguration.dataSource.update(where: { $0.uid == messageCellObject.messageUid }, with: { _ in [messageCellObject] })
    }
    
    func willShowMessageWithExternalLink(with messageId: String) {
        
        guard let message = messageById[messageId] else {
            return
        }
        
        interactor.obtainExternalLinkPreviewIfNeeded(for: message)
    }
    
    //MARK: - ChatInteractorOutput
    
    func didFinishFindingUserLocation(with location: Location?, toSendwith messageContext: String?) {
        if let userLocation = location {
            interactor.geocode(location: userLocation, toSendWith: messageContext)
        }
        else {
            router.showGeolocationErrorAlert()
        }
    }
    
    func didFinishGeocoding(location: Location, with address: String?, toSendwith messageContext: String?) {
        handleSendMessage(with: String(),
                          location: location,
                          locationTitle: StringsHelper.chatUserLocationOnMapTitle,
                          locationDescription: address,
                          context: messageContext)
    }
    
    func didObtainNewMessages(messages: [ChatMessage]) {
        
        guard !messages.isEmpty else {
            return
        }
        
        let lastIsIncoming = messages.last?.isIncoming ?? false
        if view.showsTypingIndicator {
            view.showsTypingIndicator = !lastIsIncoming
        }
        
        let newMesagesCount = messages.filter { $0.isIncoming }.count
        display(messages: messages, newMesagesCount: newMesagesCount, completion: { [weak self] in
            
            guard let strongSelf = self else { return }
            guard messages.first(where: { $0.isIncoming }) != nil else { return }
            
            strongSelf.triggerChatMessageIncomingFeedback()
            strongSelf.updateUIWithLastIncomingMessage(for: messages, fromHistory: false)
        })
    }
    
    func didBeginSending(message: ChatMessage, with timeout: TimeInterval) {
        view.startSendMessageProgressTimer(with: timeout)
        view.scrollChatToBottom(animated: false)
    }
    
    func didSend(message: ChatMessage, successfully success: Bool, didDeleteUnsent: Bool) {
        
        dataSourceFactory.updateSelectionItems(with: [message], in: &dataSourceConfiguration!)
        dataSourceFactory.updateMessageStatus(where: { $0.uid == message.uid }, with: { _ in message.status }, in: &dataSourceConfiguration!)
        if didDeleteUnsent {
            dataSourceFactory.remove(where: { $0.status == .sending || $0.status == .failed }, in: &dataSourceConfiguration!)
        }
        
        view.completeSendMessageProgressTimer()
        view.showsTypingIndicator = success && !message.replyMarkup.hideTypingIndicator
        view.display(dataSource: dataSource, for: .regular, completion: nil)
        
        if !success {
            triggerChatMessageSentFailureFeedback()
        }
    }
    
    func didBeginSendingCallback(with data: String, for message: ChatMessage?) {
        view.scrollChatToBottom(animated: false)
    }
    
    func didEdit(message: ChatMessage) {
        
        guard !isChatEmpty else { return }
        
        dataSourceFactory.update(where: { $0.uid == message.uid }, with: { _ in message }, in: &dataSourceConfiguration!)
        view.display(dataSource: dataSource, for: .reload, completion: nil)
    }
    
    func didMarkAllSendingAsFailed() {
        
        guard !isChatEmpty else { return }
        
        dataSourceFactory.updateMessageStatus(where: { $0.status == .sending }, with: { _ in .failed }, in: &dataSourceConfiguration!)
        view.display(dataSource: dataSource, for: .regular, completion: nil)
    }
    
    func didFinishObtainingMessagesHistoryPortion(result: ChatMessageObtainMultipleResult) {
        
        isLoadingMessages = false
        view.hideLoadingContentStatus()
        
        switch result {
            
        case .success(let messages):
            
            hasMoreMessagesPrevious = messages.count == messageHistoryPartMessageCount
            
            let obtainedIncomingMessages = !messages.filter({ $0.isIncoming }).isEmpty
            let canUpdateInputUI = isChatEmpty || (view.showsEmptyChatState && obtainedIncomingMessages)
            displayHistoryPart(with: messages, prependingToCurrentMessages: true, canUpdateInputUI: canUpdateInputUI)
            
        case .failure(_):
            return
        }
    }
    
    func didFinishObtainingCachedMessagesHistoryPortion(before: Bool, message: ChatMessage?, with result: ChatMessageCachedHistoryPortionObtainResult) {
        
        isLoadingMessages = false
        
        switch result {
        case .success(let history):
            
            hasMoreMessagesPrevious = isChatEmpty || before ? history.hasMorePrevious : hasMoreMessagesPrevious
            hasMoreMessagesNext = isChatEmpty || !before ? history.hasMoreNext : hasMoreMessagesNext
            
            let fallbackToServerLoading = before && canLoadHistoryFromServer
            if fallbackToServerLoading {
                hasMoreMessagesPrevious = true
            }
            
            displayHistoryPart(with: history.messages,
                               prependingToCurrentMessages: before,
                               canUpdateInputUI: isChatEmpty)
            
            if fallbackToServerLoading {
                loadMessages(inHistory: true, fromServer: true)
            }
            
        case .failure(_):
            return
        }
    }
    
    func didFinishObtainingCachedMessagesHistoryPortion(including message: ChatMessage, with result: ChatMessageCachedHistoryPortionObtainResult) {
        didFinishObtainingCachedMessagesHistoryPortion(before: true, message: message, with: result)
    }
    
    func didFinishObtainingLastIncomingCachedMessage(with result: ChatMessageObtainResult) {
        switch result {
        case .success(let message):
            guard message != nil else { return }
            updateUIWithLastIncomingMessage(for: [message!], fromHistory: true)
        case .failure(_):
            return
        }
    }
    
    func didFinishRefreshingMessagesHistoryPortion(result: ChatMessageObtainMultipleResult) {
        
        switch result {
            
        case .success(let messages):
            
            guard !messages.isEmpty else { return }
            
            if isChatEmpty {
                dataSourceConfiguration = dataSourceFactory.buildDataSourceConfiguration(from: messages)
            }
            else {
                dataSourceFactory.refresh(configuration: &dataSourceConfiguration!, withMessagesHistory: messages, hasMoreMessagesToLoadNext: hasMoreMessagesNext)
            }

            view.display(dataSource: dataSource, for: .reload, completion: nil)
            
            guard !hasMoreMessagesNext else { return }
            
            updateUIWithLastIncomingMessage(for: messages, fromHistory: true)
            
        case .failure(_):
            return
        }
    }
    
    func didChangeChatConnectionStatus(to connected: Bool) {
        if connected {
            view.hideNetworkErrorStatus()
        }
        else {
            view.showNetworkErrorStatus()
        }
    }
    
    func didMakeMessageHistoryObsolete() {
        if conversation.shouldUpdateHistory {
            interactor.attemptRefreshMessagesHistory(withLimit: messageHistoryRefreshMessageCount)
        }
    }
    
    func didReceiveMessageTyping(eventSource: ChatMessageTypingEventSource) {
        display(isTyping: true, eventSource: eventSource)
    }
    
    func didReceiveMessageTypingStopped(eventSource: ChatMessageTypingEventSource) {
        display(isTyping: false, eventSource: eventSource)
    }
    
    func willExpandLastCachedMessageGroup(messageDoesNotHaveCommands: Bool, messageHasTextKeyboard: Bool) {
        
        if messageDoesNotHaveCommands && !messageHasTextKeyboard {
            view.hideInput(animated: true)
        }
        
        display(isTyping: true, eventSource: .local)
    }
    
    //MARK: - ChatMessageButtonActionHandler
    
    func perform(action: ChatMessageButtonAction,
                 forButtonWithId buttonId: String,
                 andTitle buttonTitle: String,
                 in message: ChatMessage?,
                 embeddedMessage: ChatMessage?) {
        
        guard buttonId != commandsDataSourceFactory.noUserInputCommandButtonId else {
            
            let context = ChatMessage.MessageContextValues.inputNotPossible.rawValue
            interactor.sendUserMessage(with: buttonTitle, messageContext:context, hidingResponseTyping: shouldHideResponseTyping)
            
            return
        }
        
        if let statsData = action.statsData, !statsData.eventName.isEmpty {
            
            let actionTypesToSupress: Set<ChatMessageButtonAction.ActionType> = [.url, .share, .details]
            
            if actionTypesToSupress.contains(action.type) {
                interactor.setStatsDataToAnalyticsManager(statsData)
            }
            else {
                interactor.sendEvent(with: statsData.eventName, eventJson: statsData.eventJson)
            }
        }
        
        switch action.type {
        case .phoneCall:
            if let phone = action.phone {
                router.attempt(calling: phone)
            }
        case .url:
            if let url = action.url {
                router.showWebBrowser(for: url, with: conversation.style)
            }
        case .share:
            
            let messageToShare = embeddedMessage ?? message
            guard messageToShare != nil else { return }

            var sharingItems = messageToShare!.content.sharingItems
            
            if let shareData = action.shareData, !shareData.text.isEmpty {
                sharingItems.append(shareData.text)
            }
            
            router.showSharingModuleForChatMessage(with: sharingItems, conversationId:conversation.itemId)
            
        case .calendarEvent:
            if let eventControl = action.calendarEvent {
                let messageWithEvent = embeddedMessage ?? message
                router.showCalendarEventModule(for: eventControl, in: message, with: messageWithEvent?.content.previewString, and: action.mapObject)
            }
        case .userLocation:
            
            if let locationServicesAreAvailable = interactor.locationServicesAreAvailable {
                
                if locationServicesAreAvailable {
                    interactor.findUserLocation(toSendwith: action.messageContext)
                }
                else {
                    router.showGeolocationAlert()
                }
            }
            else {
                interactor.requestLocationServicesAuthorization()
            }
        case .customLocation:
            let type = action.customLocationObject?.type ?? .onMap
            router.showLocationSelectionModule(with: type, messageContext: action.messageContext, style: conversation.style, and: self)
        case .addressAutocomplete:
            router.showAddressAutocompleteModule(with: action.messageContext, callbackData: action.callbackData, style: conversation.style, and: self, in: message, embeddedMessage: embeddedMessage)
        case .callback:
            if let callbackData = action.callbackData {
                interactor.sendCallback(with: callbackData, for: message, embeddedMessage: embeddedMessage)
            }
        case .sendMessage:
            interactor.sendUserMessage(with: buttonTitle, messageContext: action.messageContext, hidingResponseTyping: shouldHideResponseTyping)
        case .nextMessage:
            
            guard !interactor.isExpandingLastCachedMessageGroup else { return }
            
            let userMessageText = message == nil ? buttonTitle : nil
            interactor.attemptExpandingLastCachedMessageGroup(withUserMessageText: userMessageText,
                                                              onlyIfGroupHistoryIsEmpty: false)
            
            if let callbackData = action.callbackData {
                interactor.sendCallback(with: callbackData, for: message, embeddedMessage: nil)
            }
            
        case .alert:
            if let alertControl = action.alert {
                router.showButtonActionAlertModule(with: alertControl, eventHandler: self, in: message, embeddedMessage: embeddedMessage)
            }
        case .textInput:
            
            if let textFieldsControl = action.textInput {
                
                textInputResultBuilder.action = action
                textInputResultBuilder.message = message
                textInputResultBuilder.embeddedMessage = embeddedMessage
                
                view.showTextFieldsInput(with: textFieldsControl)
            }
            
        case .calendar:
            if let calendar = action.calendar {
                router.showDateSelectionModule(for: calendar,
                                               messageContext: action.messageContext,
                                               callbackData: action.callbackData,
                                               with: conversation.style,
                                               and: self,
                                               in: message,
                                               embeddedMessage: embeddedMessage)
            }
        case .dateInterval:
            if let control = action.dateIntervalCalendar {
                router.showDateIntervalSelectionModule(for: control,
                                                       messageContext: action.messageContext,
                                                       callbackData: action.callbackData,
                                                       with: conversation.style,
                                                       and: self,
                                                       in: message,
                                                       embeddedMessage: embeddedMessage)
            }
        case .timetable:
            if let timetable = action.timetable {
                router.showDateAndTimeSelectionModule(for: timetable,
                                                      messageContext: action.messageContext,
                                                      callbackData: action.callbackData,
                                                      with: conversation.style,
                                                      and: self,
                                                      in: message,
                                                      embeddedMessage: embeddedMessage)
            }
        case .map:
            if let mapObject = action.mapObject {
                router.showMapModule(for: mapObject)
            }
        case .details:
            if let details = action.details {
                router.showDetailsModule(for: details, with: conversation.style, mapObject: action.mapObject, and: self, in: message, embeddedMessage: embeddedMessage)
            }
        case .payment:
            if !interactor.isUserLoggedIn {
                router.showAskAuthorizationModule(with: self)
            }
            else if let payment = action.payment {
                router.showPaymentModule(for: payment, and: conversation, callbackData: action.callbackData, chatResultHandler: self, delegate: self, in: message, embeddedMessage: embeddedMessage)
            }
        }
    }
    
    //MARK: - ChatCustomControlResultHandler
    
    func handleSendMessage(with text: String, context: String?) {
        interactor.sendUserMessage(with: text, messageContext: context, hidingResponseTyping: shouldHideResponseTyping)
    }
    
    func handleSendMessage(with text: String, location: Location, locationTitle: String, locationDescription: String?, context: String?) {
        let mapObject = ChatMessageMapObject(location: location, title: locationTitle, description: locationDescription)
        interactor.sendUserMessage(with: text, messageContext: context, hidingResponseTyping: shouldHideResponseTyping, mapObject: mapObject)
    }
    
    func handleSendCallback(with callbackData: String, for message: ChatMessage?, embeddedMessage: ChatMessage?) {
        interactor.sendCallback(with: callbackData, for: message, embeddedMessage: embeddedMessage)
    }
    
    //MARK: - ChatMessageUnsentActionsAlertEventHandler
    
    func didSelectDeleteMessage(for message: ChatMessage) {
        
        guard !isChatEmpty else { return }
        
        dataSourceFactory.remove(where: { $0.uid == message.uid }, in: &dataSourceConfiguration!)
        view.display(dataSource: dataSource, for: .regular, completion: nil)
        
        interactor.deleteFromCachedMessagesHistory(message: message)
    }
    
    func didSelectResendMessage(for message: ChatMessage) {
        
        var sendingMessage = message
        sendingMessage.status = .sending
        
        dataSourceFactory.updateMessageStatus(where: { $0.uid == sendingMessage.uid }, with: { _ in .sending }, in: &dataSourceConfiguration!)
        view.display(dataSource: dataSource, for: .regular, completion: nil)
        
        interactor.send(message: sendingMessage)
    }
    
    //MARK: - ChatMessageAskAuthorizationEventHandler
    
    func didSelectToAuthorize() {
        router.showAuthorizationModule()
    }
    
    //MARK: - PaymentProcessDelegate
    
    func didSelect(paymentMethod: ChatPaymentMethod, for configuration: PaymentProcessConfiguration) {
        router.showPaymentProcessModule(for: configuration)
    }
    
    func didProcess(request: PaymentRequest, for configuration: PaymentProcessConfiguration, with paymentMethod: PaymentProvider) {
        router.showPaymentSuccessModule(for: configuration, request: request, selectedProvider: paymentMethod)
    }
}

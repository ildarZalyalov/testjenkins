//
//  ChatMessageButtonActionHandler.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 21.08.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Обработчик действий по нажатию на кнопки (командные, инлайн и в кастомных контролах)
protocol ChatMessageButtonActionHandler: class {

    /// Выполнить действие для кнопки
    ///
    /// - Parameters:
    ///   - action: действие
    ///   - buttonId: идентификатор кнопки
    ///   - buttonTitle: заголовок кнопки
    ///   - message: сообщение, в котором была расположена кнопка (если это была не командная кнопка или контрол из командной кнопки)
    ///   - embeddedMessage: вложенное сообщение в карусели, в котором была расположена кнопка (если сообщение было одним из вложенных в карусели)
    func perform(action: ChatMessageButtonAction,
                 forButtonWithId buttonId: String,
                 andTitle buttonTitle: String,
                 in message: ChatMessage?,
                 embeddedMessage: ChatMessage?)
}

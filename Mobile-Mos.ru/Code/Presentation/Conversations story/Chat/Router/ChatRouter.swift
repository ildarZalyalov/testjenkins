//
//  ChatRouter.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 27.04.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import SafariServices
import EventKit
import EventKitUI

class ChatRouter: NSObject, ChatRouterInput, SFSafariViewControllerDelegate, UIDocumentInteractionControllerDelegate, EKEventEditViewDelegate {
    
    weak var transitionHandler: UIViewController!
    weak var alertTransitionHandler: UIViewController!
    weak var toOtherModulesTransitionHandler: DataTransferModuleController!
    
    var alertsFactory: ChatAlertFactory!
    var commonAlertFactory: CommonAlertsFactory!
    var photoGalleryFactory: PhotoGalleryControllerFactory!
    var applicationRouter: ApplicationRouter!
    var application: UIApplication!
    var analyticsManager: AnalyticsManager!
    
    let showChatInfoSegueIdentifier = "showChatInfo"
    let showLocationOnMapSegueIdentifier = "showLocationOnMap"
    let showDetailsAboutMessageSegueIdentifier = "showDetailsAboutMessage"
    let showDateSelectionSegueIdentifier = "showDateSelection"
    let showDateIntervalSelectionSegueIdentifier = "showDateIntervalSelection"
    let showDateAndTimeSelectionSegueIdentifier = "showDateAndTimeSelection"
    let showLocationSelectionSegueIdentifier = "showLocationSelection"
    let showSearchLocationSelectionSegueIdentifier = "searchLocationSegue"
    let showPaymentSegueIdentifier = "showPayment"
    let showPaymentUserInfoSegueIdentifier = "showPaymentUserInfo"
    let showPaymentSuccessSegueIdentifier = "showPaymentSuccess"
    
    //MARK: - ChatRouterInput
    
    func attempt(calling phoneNumber: String) {
        application.attempt(calling: phoneNumber)
    }
    
    func showWebBrowser(for url: URL, with style: ConversationStyle) {
        
        analyticsManager.setSelectedUrl(with: url)
        
        let webBrowser = SFSafariViewController(url: url, entersReaderIfAvailable: true)
        webBrowser.delegate = self
        
        if #available(iOS 10.0, *) {
            webBrowser.preferredControlTintColor = style.controlsTextColor
        }
        
        transitionHandler.present(webBrowser, animated: true, completion: nil)
    }
    
    func showOpenWithDialogForFile(at url: URL, in message: ChatMessage) {
        
        let documentController = UIDocumentInteractionController(url: url)
        documentController.name = message.content.documentObject?.fileName
        documentController.delegate = self
        
        documentController.presentPreview(animated: true)
        
        guard let statsData = analyticsManager.currentStatsData else { return }
        sendEvent(with: statsData.eventName, eventFunction: statsData.eventFunction, eventJson: statsData.eventJson, sharingSource: nil, errorText: nil)
    }
    
    func showUnsentActionsModule(for message: ChatMessage, eventHandler: ChatMessageUnsentActionsAlertEventHandler?) {
        let alert = alertsFactory.alertWithUnsentActions(for: message, eventHandler: eventHandler)
        alertTransitionHandler.present(alert, animated: true, completion: nil)
    }
    
    func showAskAuthorizationModule(with eventHandler: ChatMessageAskAuthorizationEventHandler?) {
        let alert = alertsFactory.alertForAskingAuthorization(eventHandler: eventHandler)
        alertTransitionHandler.present(alert, animated: true, completion: nil)
    }
    
    func showAuthorizationModule() {
        applicationRouter.showAuthorizationModule()
    }
    
    func showButtonActionAlertModule(with control: ChatMessageAlertControl,
                                     eventHandler: ChatMessageButtonActionHandler?,
                                     in message: ChatMessage?,
                                     embeddedMessage: ChatMessage?) {
        let alert = alertsFactory.alertForButtonAction(with: control, eventHandler: eventHandler, in: message, embeddedMessage: embeddedMessage)
        alertTransitionHandler.present(alert, animated: true, completion: nil)
    }
    
    func showConversationInfo(for conversation: Conversation) {
        toOtherModulesTransitionHandler.performSegue(with: showChatInfoSegueIdentifier, sender: nil) { controller in
            controller.configureModule(with: conversation)
        }
    }
    
    func showMapModule(for mapObject: ChatMessageMapObject) {
        toOtherModulesTransitionHandler.performSegue(with: showLocationOnMapSegueIdentifier, sender: nil) { controller in
            let configuration = LocationOnMapModuleConfiguration(mapObject: mapObject)
            controller.configureModule(with: configuration)
        }
    }
    
    func showDetailsModule(for details: ChatMessageDetailsControl,
                           with style: ConversationStyle,
                           mapObject: ChatMessageMapObject?,
                           and buttonsHandler: ChatMessageButtonActionHandler?,
                           in message: ChatMessage?,
                           embeddedMessage: ChatMessage?) {
        
        toOtherModulesTransitionHandler.performSegue(with: showDetailsAboutMessageSegueIdentifier, sender: nil) { controller in
        
            var configuration = DetailsAboutMessageModuleConfiguration(control: details, style: style)
            configuration.buttonsHandler = buttonsHandler
            configuration.mapObject = mapObject
            configuration.message = message
            configuration.embeddedMessage = embeddedMessage
            
            controller.configureModule(with: configuration)
        }
    }
    
    func showDateSelectionModule(for calendar: ChatMessageCalendarControl,
                                 messageContext: String?,
                                 callbackData: String?,
                                 with style: ConversationStyle,
                                 and resultHandler: ChatCustomControlResultHandler?,
                                 in message: ChatMessage?,
                                 embeddedMessage: ChatMessage?) {
        
        toOtherModulesTransitionHandler.performSegue(with: showDateSelectionSegueIdentifier, sender: nil) { controller in
            
            var configuration = DateSelectionModuleConfiguration(control: calendar, style: style)
            configuration.resultHandler = resultHandler
            configuration.messageContext = messageContext
            configuration.callbackData = callbackData
            configuration.message = message
            configuration.embeddedMessage = embeddedMessage
            
            controller.configureModule(with: configuration)
        }
    }
    
    func showDateIntervalSelectionModule(for control: ChatMessageDateIntervalControl,
                                         messageContext: String?,
                                         callbackData: String?,
                                         with style: ConversationStyle,
                                         and resultHandler: ChatCustomControlResultHandler?,
                                         in message: ChatMessage?,
                                         embeddedMessage: ChatMessage?) {
        
        toOtherModulesTransitionHandler.performSegue(with: showDateIntervalSelectionSegueIdentifier, sender: nil) { controller in
            
            var configuration = DateIntervalSelectionModuleConfiguration(control: control, style: style)
            configuration.resultHandler = resultHandler
            configuration.messageContext = messageContext
            configuration.callbackData = callbackData
            configuration.message = message
            configuration.embeddedMessage = embeddedMessage
            
            controller.configureModule(with: configuration)
        }
    }
    
    func showDateAndTimeSelectionModule(for timetable: ChatMessageTimetableControl,
                                        messageContext: String?,
                                        callbackData: String?,
                                        with style: ConversationStyle,
                                        and resultHandler: ChatCustomControlResultHandler?,
                                        in message: ChatMessage?,
                                        embeddedMessage: ChatMessage?) {
        
        toOtherModulesTransitionHandler.performSegue(with: showDateAndTimeSelectionSegueIdentifier, sender: nil) { controller in
            
            var configuration = DateAndTimeSelectionModuleConfiguration(control: timetable, style: style)
            configuration.resultHandler = resultHandler
            configuration.messageContext = messageContext
            configuration.callbackData = callbackData
            configuration.message = message
            configuration.embeddedMessage = embeddedMessage
            
            controller.configureModule(with: configuration)
        }
    }
    
    func showLocationSelectionModule(with type: ChatCustomLocationControl.ControlType,
                                     messageContext: String?,
                                     style: ConversationStyle,
                                     and resultHandler: ChatCustomControlResultHandler?) {
        
        var locationSelectionSegueIdetifier: String
        
        switch type {
        case .onMap:
            locationSelectionSegueIdetifier = showLocationSelectionSegueIdentifier
        case .fromAdressSearching:
            locationSelectionSegueIdetifier = showSearchLocationSelectionSegueIdentifier
        }
        
        toOtherModulesTransitionHandler.performSegue(with: locationSelectionSegueIdetifier, sender: nil) { controller in
            
            var configuration = LocationSelectionModuleConfiguration(style: style, messageContext: messageContext)
            configuration.resultHandler = resultHandler
            
            controller.configureModule(with: configuration)
        }
    }
    
    func showAddressAutocompleteModule(with messageContext: String?,
                                       callbackData: String?,
                                       style: ConversationStyle,
                                       and resultHandler: ChatCustomControlResultHandler?,
                                       in message: ChatMessage?,
                                       embeddedMessage: ChatMessage?) {
        
        toOtherModulesTransitionHandler.performSegue(with: showSearchLocationSelectionSegueIdentifier, sender: nil) { controller in
            
            var configuration = LocationSelectionModuleConfiguration(style: style, messageContext: messageContext, callbackData: callbackData)
            configuration.shouldUseAutocomplete = true
            configuration.resultHandler = resultHandler
            configuration.message = message
            configuration.embeddedMessage = embeddedMessage
            
            controller.configureModule(with: configuration)
        }
    }
    
    func showPaymentModule(for payment: ChatMessagePaymentControl,
                           and conversation: Conversation,
                           callbackData: String?,
                           chatResultHandler: ChatCustomControlResultHandler?,
                           delegate: PaymentProcessDelegate?,
                           in message: ChatMessage?,
                           embeddedMessage: ChatMessage?) {
        
        toOtherModulesTransitionHandler.performSegue(with: showPaymentSegueIdentifier, sender: nil) { controller in
            
            var configuration = PaymentProcessConfiguration(control: payment, conversation: conversation, callbackData: callbackData)
            
            configuration.message = message
            configuration.embeddedMessage = embeddedMessage
            configuration.controlResultHandler = chatResultHandler
            configuration.paymentDelegate = delegate
            
            controller.configureModule(with: configuration)
        }
    }
    
    func showPaymentProcessModule(for configuration: PaymentProcessConfiguration) {
        toOtherModulesTransitionHandler.performSegue(with: showPaymentUserInfoSegueIdentifier, sender: self) { controller in
            controller.configureModule(with: configuration)
        }
    }
    
    func showPaymentSuccessModule(for configuration: PaymentProcessConfiguration,
                                  request: PaymentRequest?,
                                  selectedProvider: PaymentProvider?) {
        
        toOtherModulesTransitionHandler.performSegue(with: showPaymentSuccessSegueIdentifier, sender: self) { controller in
            
            var moduleConfiguration = PaymentSuccessModuleConfiguration(configuration: configuration)
            moduleConfiguration.paymentRequest = request
            moduleConfiguration.selectedProvider = selectedProvider
            
            controller.configureModule(with: moduleConfiguration)
        }
    }
    
    func showGeolocationAlert() {
        let alert = commonAlertFactory.getLocationServicesNotAuthorizedAlert()
        alertTransitionHandler.present(alert, animated: true, completion: nil)
    }
    
    func showGeolocationErrorAlert() {
        let alert = commonAlertFactory.getFailedToGetUserLocationAlert()
        alertTransitionHandler.present(alert, animated: true, completion: nil)
    }
    
    func showErrorAlert(with message: String) {
        let alert = commonAlertFactory.getErrorAlert(with: nil, body: message)
        alertTransitionHandler.present(alert, animated: true, completion: nil)
    }
    
    func showCalendarEventModule(for eventControl: ChatMessageCalendarEventControl,
                                 in message: ChatMessage?,
                                 with eventNotes: String?,
                                 and mapObject: ChatMessageMapObject?){
        
        guard !EKEventStore.requestIfNeededAuthorizationAccess(for: .event, completion: { [weak self] granted, _ in
            guard granted else { return }
            self?.showCalendarEventModule(for: eventControl, in: message, with: eventNotes, and: mapObject)
        }) else { return }
        guard EKEventStore.isAuthorizationGranted(for: .event) else {
            
            let alert = commonAlertFactory.getCalendarServicesNotAuthorizedAlert()
            alertTransitionHandler.present(alert, animated: true, completion: nil)
            
            return
        }
        
        let event = EKEvent(eventStore: EKEventStore.sharedStore)
        event.title = eventControl.title
        event.notes = eventNotes
        event.startDate = eventControl.startDate
        
        if let endDate = eventControl.endDate {
            event.endDate = endDate
        }
        
        if let eventLocation = mapObject {
            
            let location = EKStructuredLocation(title: eventLocation.previewString)
            location.geoLocation = CLLocation(latitude: eventLocation.location.latitude, longitude: eventLocation.location.longitude)
            
            event.structuredLocation = location
        }
        
        let eventEditController = EKEventEditViewController()
        eventEditController.eventStore = EKEventStore.sharedStore
        eventEditController.event = event
        eventEditController.editViewDelegate = self
        
        transitionHandler.present(eventEditController, animated: true, completion: nil)
    }
    
    func showSharingModule(with data: [Any], completion: @escaping (_ completed: Bool) -> ()) {
        
        let activityController = UIActivityViewController(activityItems: data, applicationActivities: nil)
        activityController.completionWithItemsHandler = { activityType, completed, _, _ in
            completion(completed)
        }
        
        transitionHandler.present(activityController, animated: true, completion: nil)
    }
    
    func showSharingModuleForChatMessage(with data: [Any], conversationId: String) {
        
        let activityController = UIActivityViewController(activityItems: data, applicationActivities: nil)
        activityController.completionWithItemsHandler = { [weak self] activityType, completed, _, _ in
            
            if completed {
                guard let statsData = self?.analyticsManager.currentStatsData else { return }
                
                self?.sendEvent(with: statsData.eventName, eventFunction: statsData.eventFunction, eventJson: statsData.eventJson, sharingSource: activityType?.rawValue ?? AnalyticsConstants.emptyValueName, errorText: nil)
            }
        }
        
        transitionHandler.present(activityController, animated: true, completion: nil)
    }
    
    func showPhotoGalleryForPhotos(in message: ChatMessage) {
        
        guard let photoUrl = message.content.photoObject?.photoUrl else { return }
 
        let source = PhotoGallerySource(url: photoUrl)
        let photosViewController = photoGalleryFactory.createPhotoGallery(for: [source])
        photosViewController.modalTransitionStyle = .crossDissolve
        
        transitionHandler.present(photosViewController, animated: true)
    }
    
    //MARK: - SFSafariViewControllerDelegate
    
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        transitionHandler.dismiss(animated: true, completion: nil)
    }
    
    func safariViewController(_ controller: SFSafariViewController, didCompleteInitialLoad didLoadSuccessfully: Bool) {
        
        let errorText = didLoadSuccessfully ? AnalyticsConstants.emptyValueName : AnalyticsConstants.notLoadedErrorValue
        
        if let url = analyticsManager.selectedUrl {
            
            if url.absoluteString == ChildrenServiceDetailEmptyView.electronnyDnevnikURLString {
                analyticsManager.sendChildsConversationEvent(with: .diaryLinkSelected(withString: AnalyticsConstants.childDiaryViewingEventName), aboutAppData: nil, appSharingSource: nil)
            }
            else if url.absoluteString == ChildrenServiceDetailEmptyView.sppInstructionURLString {
                analyticsManager.sendChildsConversationEvent(with: .foodLinkSelected(withString: AnalyticsConstants.foodInstructionViewingEventName), aboutAppData: nil, appSharingSource: nil)
            }
        }
    
        guard let statsData = analyticsManager.currentStatsData else { return }
        sendEvent(with: statsData.eventName, eventFunction: statsData.eventFunction, eventJson: statsData.eventJson, sharingSource: nil, errorText: errorText)
    }
    
    //MARK: - UIDocumentInteractionControllerDelegate
    
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController {
        return transitionHandler
    }
    
    //MARK: - EKEventEditViewDelegate
    
    func eventEditViewController(_ controller: EKEventEditViewController, didCompleteWith action: EKEventEditViewAction) {
        transitionHandler.dismiss(animated: true, completion: nil)
    }
    
    //MARK: Event helpers
    
    fileprivate func sendEvent(with eventName: String, eventFunction: String, eventJson: String, sharingSource: String?, errorText: String?) {
        
        analyticsManager.setStatsData(with: nil)
        
        if !eventJson.isEmpty {
            analyticsManager.sendSomeEvent(with: eventName, eventJson: eventJson, errorText: errorText)
        }
        else if !eventFunction.isEmpty, let sharingAppName = sharingSource {
            analyticsManager.sendSomeSharingEvent(with: eventName, eventFunction: eventFunction, sharingSource: sharingAppName)
        }
    }
}

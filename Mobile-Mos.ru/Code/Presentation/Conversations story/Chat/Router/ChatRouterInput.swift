//
//  ChatRouterInput.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 27.04.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol ChatRouterInput: class {
    
    func attempt(calling phoneNumber: String)

    func showWebBrowser(for url: URL, with style: ConversationStyle)
    
    func showOpenWithDialogForFile(at url: URL, in message: ChatMessage)
    
    func showUnsentActionsModule(for message: ChatMessage, eventHandler: ChatMessageUnsentActionsAlertEventHandler?)
    
    func showAskAuthorizationModule(with eventHandler: ChatMessageAskAuthorizationEventHandler?)
    
    func showAuthorizationModule()
    
    func showButtonActionAlertModule(with control: ChatMessageAlertControl,
                                     eventHandler: ChatMessageButtonActionHandler?,
                                     in message: ChatMessage?,
                                     embeddedMessage: ChatMessage?)
    
    func showConversationInfo(for conversation: Conversation)
    
    func showMapModule(for mapObject: ChatMessageMapObject)
    
    func showDetailsModule(for details: ChatMessageDetailsControl,
                           with style: ConversationStyle,
                           mapObject: ChatMessageMapObject?,
                           and buttonsHandler: ChatMessageButtonActionHandler?,
                           in message: ChatMessage?,
                           embeddedMessage: ChatMessage?)
    
    func showDateSelectionModule(for calendar: ChatMessageCalendarControl,
                                 messageContext: String?,
                                 callbackData: String?,
                                 with style: ConversationStyle,
                                 and resultHandler: ChatCustomControlResultHandler?,
                                 in message: ChatMessage?,
                                 embeddedMessage: ChatMessage?)
    
    func showDateIntervalSelectionModule(for control: ChatMessageDateIntervalControl,
                                         messageContext: String?,
                                         callbackData: String?,
                                         with style: ConversationStyle,
                                         and resultHandler: ChatCustomControlResultHandler?,
                                         in message: ChatMessage?,
                                         embeddedMessage: ChatMessage?)
    
    func showDateAndTimeSelectionModule(for timetable: ChatMessageTimetableControl,
                                        messageContext: String?,
                                        callbackData: String?,
                                        with style: ConversationStyle,
                                        and resultHandler: ChatCustomControlResultHandler?,
                                        in message: ChatMessage?,
                                        embeddedMessage: ChatMessage?)
    
    func showLocationSelectionModule(with type: ChatCustomLocationControl.ControlType,
                                     messageContext: String?,
                                     style: ConversationStyle,
                                     and resultHandler: ChatCustomControlResultHandler?)
    
    func showAddressAutocompleteModule(with messageContext: String?,
                                       callbackData: String?,
                                       style: ConversationStyle,
                                       and resultHandler: ChatCustomControlResultHandler?,
                                       in message: ChatMessage?,
                                       embeddedMessage: ChatMessage?)
    
    func showPaymentModule(for payment: ChatMessagePaymentControl,
                           and conversation: Conversation,
                           callbackData: String?,
                           chatResultHandler: ChatCustomControlResultHandler?,
                           delegate: PaymentProcessDelegate?,
                           in message: ChatMessage?,
                           embeddedMessage: ChatMessage?)
    
    func showPaymentProcessModule(for configuration: PaymentProcessConfiguration)
    
    func showPaymentSuccessModule(for configuration: PaymentProcessConfiguration,
                                  request: PaymentRequest?,
                                  selectedProvider: PaymentProvider?)
    
    func showGeolocationAlert()
    
    func showGeolocationErrorAlert()
    
    func showErrorAlert(with message: String)
    
    func showCalendarEventModule(for eventControl: ChatMessageCalendarEventControl,
                                 in message: ChatMessage?,
                                 with eventNotes: String?,
                                 and mapObject: ChatMessageMapObject?)
    
    func showSharingModule(with data: [Any], completion: @escaping (_ completed: Bool) -> ())
    
    func showSharingModuleForChatMessage(with data: [Any], conversationId: String)
    
    func showPhotoGalleryForPhotos(in message: ChatMessage)
}

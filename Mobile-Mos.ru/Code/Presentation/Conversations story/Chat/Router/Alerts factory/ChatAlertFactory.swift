//
//  ChatAlertFactory.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 01.07.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Обработчик выбора пунктов в action sheet действий с неотправленным сообщением
protocol ChatMessageUnsentActionsAlertEventHandler {
    
    /// Пользователь выбрал удалить сообщение
    ///
    /// - Parameter message: сообщение
    func didSelectDeleteMessage(for message: ChatMessage)
    
    /// Пользователь выбрал попробовать отправить сообщение снова
    ///
    /// - Parameter message: сообщение
    func didSelectResendMessage(for message: ChatMessage)
}

protocol ChatMessageAskAuthorizationEventHandler {
    
    func didSelectToAuthorize()
}

/// Фабрика alert и action sheet для модуля диалога
protocol ChatAlertFactory {
    
    /// Создать action sheet действий с неотправленным сообщением
    ///
    /// - Parameters:
    ///   - message: неотправленное сообщение
    ///   - eventHandler: обработчик выбора пунктов
    /// - Returns: action sheet
    func alertWithUnsentActions(for message: ChatMessage, eventHandler: ChatMessageUnsentActionsAlertEventHandler?) -> UIViewController
    
    /// Создать алерт для обработки нажатия на командную/инлайн кнопку
    ///
    /// - Parameters:
    ///   - control: данные для построения алерта
    ///   - eventHandler: обработчик действий по нажатию на кнопки внутри алерта
    ///   - message: сообщение, внутри которого была кнопка (если есть)
    ///   - embeddedMessage: вложенное сообщение, внутри которого была кнопка (если сообщение было каруселью)
    /// - Returns: алерт
    func alertForButtonAction(with control: ChatMessageAlertControl,
                              eventHandler: ChatMessageButtonActionHandler?,
                              in message: ChatMessage?,
                              embeddedMessage: ChatMessage?) -> UIViewController
    
    func alertForAskingAuthorization(eventHandler: ChatMessageAskAuthorizationEventHandler?) -> UIViewController
}

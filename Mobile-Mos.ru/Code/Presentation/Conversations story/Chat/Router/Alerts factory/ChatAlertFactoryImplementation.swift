//
//  ChatAlertFactoryImplementation.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 01.07.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class ChatAlertFactoryImplementation: ChatAlertFactory {
    
    func alertWithUnsentActions(for message: ChatMessage, eventHandler: ChatMessageUnsentActionsAlertEventHandler?) -> UIViewController {
        
        let alertController = UIAlertController(title: nil, message: "Сообщение не отправлено. Нажмите «Отправить снова»", preferredStyle: .actionSheet)
        
        let resendAction = UIAlertAction(title: "Отправить снова", style: .default) { _ in
            eventHandler?.didSelectResendMessage(for: message)
        }
        alertController.addAction(resendAction)
        
        let deleteAction = UIAlertAction(title: "Удалить", style: .destructive) { _ in
            eventHandler?.didSelectDeleteMessage(for: message)
        }
        alertController.addAction(deleteAction)
        
        let cancelAction = UIAlertAction(title: "Отменить", style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        
        return alertController
    }
    
    func alertForButtonAction(with control: ChatMessageAlertControl,
                              eventHandler: ChatMessageButtonActionHandler?,
                              in message: ChatMessage?,
                              embeddedMessage: ChatMessage?) -> UIViewController {
        
        let alertController = UIAlertController(title: control.title, message: control.text, preferredStyle: .alert)
        
        for button in control.buttons {
            
            let action = UIAlertAction(title: button.title, style: .default) { _ in
                guard button.action != nil else { return }
                eventHandler?.perform(action: button.action!, forButtonWithId: button.itemId, andTitle: button.title, in: message, embeddedMessage: embeddedMessage)
            }
            
            alertController.addAction(action)
        }
        
        return alertController
    }
    
    func alertForAskingAuthorization(eventHandler: ChatMessageAskAuthorizationEventHandler?) -> UIViewController {
        
        let alertController = UIAlertController(title: "Для продолжения требуется авторизация", message: nil, preferredStyle: .actionSheet)
        
        let authorizeAction = UIAlertAction(title: "Авторизоваться", style: .default) { _ in
            eventHandler?.didSelectToAuthorize()
        }
        alertController.addAction(authorizeAction)
        
        let cancelAction = UIAlertAction(title: "Отменить", style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        
        return alertController
    }
}

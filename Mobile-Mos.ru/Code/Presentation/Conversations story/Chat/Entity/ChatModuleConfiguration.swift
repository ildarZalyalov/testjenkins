//
//  ChatModuleConfiguration.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 07.12.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Конфигурация модуля чата
struct ChatModuleConfiguration {
    
    /// Диалог
    let conversation: Conversation
    
    /// Сообщение в чате, которое надо показать (если есть)
    /// (если сообщение не относится к данному чату, то чат будет открыт как обычно)
    let message: ChatMessage?
}

//
//  ChatModuleConfigurator.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 27.04.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import UIKit

class ChatModuleConfigurator: BaseModuleConfigurator {
    
    override func configureModule(for view: UIViewController) {
        
        let viewController = view as! ChatViewController
        let presenter = ChatPresenter()
        let interactor = ChatInteractor()
        let router = ChatRouter()
        
        let messageFactory = UIApplication.shared.serviceBuilder.getChatMessageFactory()
        let fileDownloadManager = UIApplication.shared.serviceBuilder.fileDownloadManager
        let fileCacheManager = UIApplication.shared.serviceBuilder.defaultFileCacheManager
        let connectionObserver = UIApplication.shared.serviceBuilder.getSocketConnectionObserver()
        let userService = UIApplication.shared.serviceBuilder.getUserService()
        let messageService = UIApplication.shared.serviceBuilder.getChatMessageService()
        let historyService = UIApplication.shared.serviceBuilder.getChatMessageHistoryService()
        let conversationsService = UIApplication.shared.serviceBuilder.getConversationsService()
        let interactionService = UIApplication.shared.serviceBuilder.getChatMessageInteractionService()
        let geocodingService = UIApplication.shared.serviceBuilder.getYandexGeocoderService()
        
        let dateFormatter = DateStringFormatterImplementation()
        let dataSourceFactory = ChatDataSourceFactoryImplementation()
        dataSourceFactory.dateFormatter = dateFormatter
        
        let commandsDataSourceFactory = CommandButtonsDataSourceFactoryImplementation()
        let textInputResultBuilder = TextInputControlResultBuilderImplementation()
        let stringBuilder = ChatMessageStringBuilderImplementation()
        let textFormattingExtractor = ChatTextFormattingExtractorImplementation()
        let alertsFactory = ChatAlertFactoryImplementation()
        let photoGalleryFactory = PhotoGalleryControllerFactoryImplementation()
        let commonAlertsFactory = CommonAlertsFactoryImplementation()
        let applicationRouter = ApplicationRouterBuilder.getApplicationRouter()
        let application = UIApplication.shared
        let analyticsManager: AnalyticsManager = AnalyticsManagerBuilder.getAnalyticsManager()
        
        let mapPreviewUrlBuilder = YandexMapPreviewUrlBuilderImplementation()
        mapPreviewUrlBuilder.parametersEncoder = HTTPRequestParameterEncoderDefault()
        
        let previewLoader = WeblinkPreviewLoaderImplementation()
        previewLoader.openGraphParser = OpenGraphParserImplementation()
        
        let locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        
        let soundsManager = SoundsManagerImplementation()
        
        let typingManager = ChatMessageTypingManagerImplementation()
        typingManager.messageService = messageService
        typingManager.interactionService = interactionService
        
        viewController.output = presenter
        viewController.stringBuilder = stringBuilder
        viewController.mapPreviewUrlBuilder = mapPreviewUrlBuilder
        viewController.textFormattingExtractor = textFormattingExtractor
        
        presenter.view = viewController
        presenter.interactor = interactor
        presenter.router = router
        presenter.dataSourceFactory = dataSourceFactory
        presenter.commandsDataSourceFactory = commandsDataSourceFactory
        presenter.textInputResultBuilder = textInputResultBuilder
        presenter.soundsManager = soundsManager
        if #available(iOS 10.0, *) {
            let hapticFeedManager = HapticFeedbackManagerImplementation()
            presenter.hapticFeedManager = hapticFeedManager
        }
        
        interactor.output = presenter
        interactor.previewLoader = previewLoader
        interactor.messageFactory = messageFactory
        interactor.fileDownloadManager = fileDownloadManager
        interactor.fileCacheManager = fileCacheManager
        interactor.connectionObserver = connectionObserver
        interactor.userService = userService
        interactor.messageService = messageService
        interactor.historyService = historyService
        interactor.conversationsService = conversationsService
        interactor.interactionService = interactionService
        interactor.yandexGeocoderService = geocodingService
        interactor.locationManager = locationManager
        interactor.typingManager = typingManager
        interactor.analyticsManager = analyticsManager
        
        router.transitionHandler = viewController
        router.alertTransitionHandler = viewController
        router.toOtherModulesTransitionHandler = viewController
        router.alertsFactory = alertsFactory
        router.commonAlertFactory = commonAlertsFactory
        router.photoGalleryFactory = photoGalleryFactory
        router.applicationRouter = applicationRouter
        router.application = application
        router.analyticsManager = analyticsManager
    }
}

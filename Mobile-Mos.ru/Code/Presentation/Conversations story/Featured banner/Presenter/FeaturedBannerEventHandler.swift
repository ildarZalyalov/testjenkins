//
//  FeaturedBannerEventHandler.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 07.08.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Обработчик событий, происходящих в модуле баннера
protocol FeaturedBannerEventHandler: class {
    
    func contentInsets() -> UIEdgeInsets
    
    /// Обновился стиль статус бара для баннера
    ///
    /// - Parameter style: новый стиль статус бара
    func didUpdateBannerStatusBarStyle(style: UIStatusBarStyle)
    
    /// Пользователь хочет увидеть информацию о своем аккаунте
    func userWantsToDisplayAccount()
}

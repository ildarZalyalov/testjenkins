//
//  FeaturedBannerPresenter.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 21.02.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class FeaturedBannerPresenter: FeaturedBannerViewOutput, FeaturedBannerInteractorOutput {
    
    weak var view: FeaturedBannerViewInput!
    var router: FeaturedBannerRouterInput!
    var interactor: FeaturedBannerInteractorInput!
    
    var weatherStringData: WeatherStringBuilderOutput = ("", "")
    
    weak var eventHandler: FeaturedBannerEventHandler?
    
    //MARK: - Методы
    
    func redisplayCurrentBannerBackground() {
        let configuration = interactor.obtainСurrentBannerBackgroundConfiguration()
        updateBanner(with: configuration)
    }
    
    func updateBanner(with configuration: BannerBackgroundConfiguration, factoidTitle: String = "", factoidMessage: String = "") {
        
        view.refreshBannerBackground(with: configuration.background, and: configuration.textColor, factoidTitleText: factoidTitle, factoidMessageText: factoidMessage)
        eventHandler?.didUpdateBannerStatusBarStyle(style: configuration.statusBarStyle)
    }
    
    //MARK: - FeaturedBannerViewOutput
    
    func configure(with eventHandler: FeaturedBannerEventHandler?) {
        self.eventHandler = eventHandler
    }
    
    func setupInitialState() {
    
        if let insets = eventHandler?.contentInsets() {
            view.apply(contentInsets: insets)
        }
        
        redisplayCurrentBannerBackground()
        interactor.refreshCurrentBannerBackground()
        interactor.refreshUserLoginState()
        interactor.obtainCurrentWeather()
    }
    
    func didPressDisplayAccount() {
        eventHandler?.userWantsToDisplayAccount()
    }
    
    func updateBannerStatusBarStyle(style: UIStatusBarStyle) {
        eventHandler?.didUpdateBannerStatusBarStyle(style: style)
    }
    
    //MARK: - FeaturedBannerInteractorOutput
    
    var isRefreshingBannerDisabled: Bool {
        return view.isRefreshingBannerDisabled
    }
    
    func didUpdateUserLoginState(isUserAuhorized: Bool) {
        view.refreshAuthorizationUI(isUserAuhorized: isUserAuhorized)
    }
    
    func didFinishRefreshingBannerBackgroundConfiguration(configuration: BannerBackgroundConfiguration?) {
        
        guard let newConfiguration = configuration else { return }
        updateBanner(with: newConfiguration, factoidTitle: weatherStringData.title, factoidMessage: weatherStringData.subtitle)
    }
    
    func didFinishObtainingCurrentWeather(with result: WeatherObtainMultipleResult) {
        
         let configuration = interactor.obtainСurrentBannerBackgroundConfiguration()
        
        switch result {
        case .success(let weather):
            
            let weatherBuildedString = interactor.buildWeatherStrings(from: weather.temperatureInCelcius, weatherText: weather.weatherText)
           
            weatherStringData = weatherBuildedString
            
            updateBanner(with: configuration, factoidTitle: weatherBuildedString.title, factoidMessage: weatherBuildedString.subtitle)
            
        case .failure(let error):
            
            updateBanner(with: configuration)
            weatherStringData = ("", "")
            
            print("Error: \(error.localizedDescription)")
        }
    }
}

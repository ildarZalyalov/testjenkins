//
//  FeaturedBannerModuleLoader.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 21.02.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import UIKit

class FeaturedBannerModuleLoader: BaseModuleLoader {
    
    override func loadModuleViewController() -> UIViewController {
        return FeaturedBannerViewController(nibName: String(describing: FeaturedBannerViewController.self), bundle: nil)
    }
    
    override func configureModule(for view: UIViewController) {
        
        let viewController = view as! FeaturedBannerViewController
        let presenter = FeaturedBannerPresenter()
        let interactor = FeaturedBannerInteractor()
        let router = FeaturedBannerRouter()
        
        let blurBuilder = UIImageBlurEffectBuilder()
        let userService = UIApplication.shared.serviceBuilder.getUserService()
        let bannerService = UIApplication.shared.serviceBuilder.getBannerService()
        let notificationCenter = NotificationCenter.default
        let weatherService = UIApplication.shared.serviceBuilder.getWeatherService()
        let weatherStringBuilder = WeatherStringBuilder()
        
        viewController.output = presenter
        viewController.imageBlurBuilder = blurBuilder
        
        presenter.view = viewController
        presenter.interactor = interactor
        presenter.router = router
        
        interactor.output = presenter
        interactor.userService = userService
        interactor.bannerService = bannerService
        interactor.weatherService = weatherService
        interactor.weatherStringBuilder = weatherStringBuilder
        interactor.notificationCenter = notificationCenter
    }
}

//
//  FeaturedBannerInteractorOutput.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 21.02.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol FeaturedBannerInteractorOutput: class {
    
    var isRefreshingBannerDisabled: Bool { get }
    
    func didUpdateUserLoginState(isUserAuhorized: Bool)
    
    func didFinishObtainingCurrentWeather(with result: WeatherObtainMultipleResult)
    
    func didFinishRefreshingBannerBackgroundConfiguration(configuration: BannerBackgroundConfiguration?)
}

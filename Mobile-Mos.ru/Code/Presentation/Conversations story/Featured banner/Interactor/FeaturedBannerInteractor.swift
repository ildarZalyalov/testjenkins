//
//  FeaturedBannerInteractor.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 21.02.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class FeaturedBannerInteractor: FeaturedBannerInteractorInput {
    
    weak var output: FeaturedBannerInteractorOutput!
    
    var bannerService: BannerService!
    var userService: UserService!
    var weatherService: WeatherService!
    var weatherStringBuilder: WeatherStringBuilder!
    
    var notificationCenter: NotificationCenter! {
        didSet {
            guard notificationCenter != nil else { return }
            notificationCenter.addObserver(self, selector: #selector(applicationDidBecomeActive), name: .UIApplicationDidBecomeActive, object: nil)
            notificationCenter.addObserver(self, selector: #selector(userLoginStateDidChange), name: .UserLoginStateDidChange, object: nil)
        }
    }
    
    deinit {
        notificationCenter.removeObserver(self)
    }
    
    //MARK: - Нотификации активности приложения
    
    @objc
    func applicationDidBecomeActive() {
        
        guard !output.isRefreshingBannerDisabled else { return }
        
        refreshCurrentBannerBackground()
        obtainCurrentWeather()
    }
    
    @objc
    func userLoginStateDidChange() {
        output.didUpdateUserLoginState(isUserAuhorized: userService.currentUser.isLoggedIn)
    }
    
    //MARK: - FeaturedBannerInteractorInput
    
    func obtainСurrentBannerBackgroundConfiguration() -> BannerBackgroundConfiguration {
        return bannerService.obtainСurrentBannerBackgroundConfiguration()
    }
    
    func obtainCurrentWeather() {
        weatherService.getCurrentWeatherInMoscow { [weak self] (result) in
            self?.output.didFinishObtainingCurrentWeather(with: result)
        }
    }
    
    func buildWeatherStrings(from temperature: Int, weatherText: String) -> WeatherStringBuilderOutput {
        
        return weatherStringBuilder.buildWeatherString(from: temperature, weatherText: weatherText)
    }
    
    func refreshUserLoginState() {
        output.didUpdateUserLoginState(isUserAuhorized: userService.currentUser.isLoggedIn)
    }
    
    func refreshCurrentBannerBackground() {
        bannerService.refreshCurrentBannerBackgroundConfiguration { [weak self] configuration in
            self?.output.didFinishRefreshingBannerBackgroundConfiguration(configuration: configuration)
        }
    }
}

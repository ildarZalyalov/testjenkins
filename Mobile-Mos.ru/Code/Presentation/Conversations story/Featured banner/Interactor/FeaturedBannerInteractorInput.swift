//
//  FeaturedBannerInteractorInput.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 21.02.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol FeaturedBannerInteractorInput: class {
    
    func obtainСurrentBannerBackgroundConfiguration() -> BannerBackgroundConfiguration
    
    func refreshUserLoginState()
    
    func refreshCurrentBannerBackground()
    
    func obtainCurrentWeather()
    
    func buildWeatherStrings(from temperature: Int, weatherText: String) -> WeatherStringBuilderOutput
}

//
//  FeaturedBannerViewController.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 21.02.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import UIKit

class FeaturedBannerViewController: BaseViewController, FeaturedBannerViewInput, ConfigurableModuleController, AnimatableView, UICollectionViewDelegate {
    
    var output: FeaturedBannerViewOutput!
    
    var imageBlurBuilder: UIImageBlurEffectBuilder!
    
    weak var backgroundImageView: UIImageView!
    weak var blurredBackgroundImageView: UIImageView!
    
    @IBOutlet weak var backgroundOverlayImageView: UIImageView!
    
    @IBOutlet weak var factoidTitleLabel: UILabel!
    @IBOutlet weak var factoidMessageLabel: UILabel!
    
    @IBOutlet weak var bannerTitleLabel: UILabel!
    @IBOutlet weak var bannerMessageLabel: UILabel!
    
    @IBOutlet var bannerMessageLabelToTitleConstraint: NSLayoutConstraint!
    @IBOutlet var bannerMessageLabelToOverlayConstraint: NSLayoutConstraint!
    
    @IBOutlet var leftSideContentConstraints: [NSLayoutConstraint]!
    @IBOutlet var rightSideContentConstraints: [NSLayoutConstraint]!
    @IBOutlet var topSideContentConstraints: [NSLayoutConstraint]!
    
    @IBOutlet weak var accountButton: UIButton!
    
    let bannerBackgroundBlurRadius: CGFloat = 30
    let bannerBackgroundSaturationDelta: CGFloat = 1.3
    
    let interpolationAmountX = 15
    let interpolationAmountY = 40
    
    let interpolationHorizontalKeypath = "center.x"
    let interpolationVerticalKeypath = "center.y"
    
    var featuredBannerImage: UIImage?
    var didLoadAdBanner = false
    
    let bannerTitleLineHeight: CGFloat = 24
    let bannerTitleBaselineOffset: Double = 1.3
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        let backgroundView = UIImageView(frame: CGRect.zero)
        backgroundView.contentMode = .scaleAspectFill
        view.insertSubview(backgroundView, at: 0)
        backgroundImageView = backgroundView
        
        let blurredBackgroundView = UIImageView(frame: CGRect.zero)
        blurredBackgroundView.contentMode = .scaleAspectFill
        view.insertSubview(blurredBackgroundView, aboveSubview: backgroundView)
        blurredBackgroundImageView = blurredBackgroundView
        
        applyBackgroundInterpolation()
        output.setupInitialState()
    }
    
    override var shouldAutorotate: Bool {
        return false
    }
    
    @IBAction func pressedDisplayAccountButton() {
        output.didPressDisplayAccount()
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override func viewDidLayoutSubviews() {
        
        // создаю их и управляю размером руками, чтобы они не расширялись автоматически
        // в случае получения больших картинок (дефолтное поведение)
        let backgroundFrame = view.bounds.insetBy(dx: CGFloat(-interpolationAmountX), dy: CGFloat(-interpolationAmountY))
        
        backgroundImageView.frame = backgroundFrame
        blurredBackgroundImageView.frame = backgroundFrame
    }
    
    //MARK: - Приватные методы
    
    func applyBackgroundInterpolation() {
        
        let interpolationHorizontal = UIInterpolatingMotionEffect(keyPath: interpolationHorizontalKeypath, type: .tiltAlongHorizontalAxis)
        interpolationHorizontal.minimumRelativeValue = interpolationAmountX
        interpolationHorizontal.maximumRelativeValue = -interpolationAmountX
        
        let interpolationVertical = UIInterpolatingMotionEffect(keyPath: interpolationVerticalKeypath, type: .tiltAlongVerticalAxis)
        interpolationVertical.minimumRelativeValue = interpolationAmountY
        interpolationVertical.maximumRelativeValue = -interpolationAmountY
        
        let group = UIMotionEffectGroup()
        group.motionEffects = [interpolationHorizontal, interpolationVertical]
        
        backgroundImageView.addMotionEffect(group)
    }
    
    
    func attributtedBannerTitle(with text: String, and textColor: UIColor) -> NSAttributedString {
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .left
        paragraphStyle.lineBreakMode = .byTruncatingTail
        paragraphStyle.minimumLineHeight = bannerTitleLineHeight
        paragraphStyle.maximumLineHeight = bannerTitleLineHeight
        
        var attributes: [NSAttributedStringKey : Any] = [
            NSAttributedStringKey.paragraphStyle : paragraphStyle,
            NSAttributedStringKey.foregroundColor : textColor,
            NSAttributedStringKey.baselineOffset : NSNumber(value: bannerTitleBaselineOffset)
        ]
        
        if let font = bannerTitleLabel.font {
            attributes += [NSAttributedStringKey.font : font]
        }
        
        return NSAttributedString(string: text, attributes: attributes)
    }
    
    //MARK: - ConfigurableModuleController
    
    func configureModule(with object: Any) {
        output.configure(with: object as? FeaturedBannerEventHandler)
    }

    //MARK: - FeaturedBannerViewInput
    
    var isRefreshingBannerDisabled: Bool {
        return didLoadAdBanner
    }
    
    func apply(contentInsets: UIEdgeInsets) {
        
        let adjustConstraint: (_ constraint: NSLayoutConstraint, _ inset: CGFloat) -> () = {
            $0.constant += $1
        }
        
        leftSideContentConstraints.forEach { adjustConstraint($0, contentInsets.left) }
        rightSideContentConstraints.forEach { adjustConstraint($0, contentInsets.right) }
        topSideContentConstraints.forEach { adjustConstraint($0, contentInsets.top) }
        
        view.setNeedsLayout()
    }
    
    func refreshBannerBackground(with image: UIImage,
                                 and textColor: UIColor?,
                                 factoidTitleText: String?,
                                 factoidMessageText: String?) {
        
        backgroundImageView.image = image
        blurredBackgroundImageView.image = imageBlurBuilder.imageByApplyingBlur(to: image,
                                                                                withRadius: bannerBackgroundBlurRadius,
                                                                                tintColor: nil,
                                                                                saturationDeltaFactor: bannerBackgroundSaturationDelta,
                                                                                maskImage: nil)
        
        factoidTitleLabel.text = factoidTitleText
        factoidMessageLabel.text = factoidMessageText
        
        factoidTitleLabel.isHidden = factoidTitleText == nil
        factoidMessageLabel.isHidden = factoidMessageText == nil
        
        guard let color = textColor else { return }
        
        factoidTitleLabel.textColor = color
        factoidMessageLabel.textColor = color
    }
    
    func refreshAuthorizationUI(isUserAuhorized: Bool) {
        let icon = isUserAuhorized ? #imageLiteral(resourceName: "userAccountIcon") : #imageLiteral(resourceName: "authorizeIcon")
        accountButton.setImage(icon, for: .normal)
    }
    
    //MARK: - AnimatableView
    
    func animateInteractiveTransition(with progress: CGFloat) {
        blurredBackgroundImageView.alpha = progress
    }
    
}

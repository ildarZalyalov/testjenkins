//
//  FeaturedBannerViewOutput.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 21.02.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol FeaturedBannerViewOutput: class {
    
    func configure(with eventHandler: FeaturedBannerEventHandler?)

    func setupInitialState()
    
    func didPressDisplayAccount()
    
    func updateBannerStatusBarStyle(style: UIStatusBarStyle)
}

//
//  FeaturedBannerViewInput.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 21.02.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol FeaturedBannerViewInput: class {
    
    var isRefreshingBannerDisabled: Bool { get }
    
    func apply(contentInsets: UIEdgeInsets)
    
    func refreshBannerBackground(with image: UIImage,
                                 and textColor: UIColor?,
                                 factoidTitleText: String?,
                                 factoidMessageText: String?)
    
    func refreshAuthorizationUI(isUserAuhorized: Bool)
}

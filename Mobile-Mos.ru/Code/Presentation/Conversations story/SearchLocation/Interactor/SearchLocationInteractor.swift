//
//  SearchLocationInteractor.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 07.12.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class SearchLocationInteractor: SearchLocationInteractorInput {
    
    weak var output: SearchLocationInteractorOutput!
    
    var yandexGeocoderService: YandexGeocoderService!
    var autocompleteService: AddressAutocompleteService!
    
    weak var currentAutocompleteRequest: HTTPRequest?
    
    //MARK: - SearchLocationInteractorInput
    
    var searchAddressMinimumStringLength: Int {
        return yandexGeocoderService.searchAdressMinimumStringLength
    }
    
    var obtainAutocompleteMinimumStringLength: Int {
        return autocompleteService.searchAddressMinimumStringLength
    }
    
    func searchSuggest(with string: String) {
        yandexGeocoderService.searchAddress(with: string) { [weak self] (result) in
            self?.output.didFinishSearchingSuggests(with: result)
        }
    }
    
    func obtainAutocomplete(with string: String) {
        currentAutocompleteRequest?.cancel()
        currentAutocompleteRequest = autocompleteService.searchAddress(with: string) { [weak self] (result) in
            self?.output.didFinishObtainingAutocomplete(with: result)
        }
    }
    
    func buildCallbackData(with callbackData: String?, for autocomplete: AddressAutocompleteResponse) -> String? {
        
        guard let data = callbackData else { return nil }
        
        let controlValueJson: [String : String] = [
            "name" : autocomplete.address,
            "data" : autocomplete.addressData
        ]
        
        let jsonDictionary: [String : Any] = [
            "callback" : data,
            "control_value" : controlValueJson
        ]
        
        guard let jsonData = try? JSONSerialization.data(withJSONObject: jsonDictionary, options: []) else { return nil }
        
        return String(data: jsonData, encoding: .utf8)
    }
}

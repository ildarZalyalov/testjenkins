//
//  SearchLocationInteractorOutput.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 07.12.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol SearchLocationInteractorOutput: class {
    
    func didFinishSearchingSuggests(with result: YandexAddressSearchRequestResult)
    
    func didFinishObtainingAutocomplete(with result: AddressAutocompleteMultipleObtainResult)
}

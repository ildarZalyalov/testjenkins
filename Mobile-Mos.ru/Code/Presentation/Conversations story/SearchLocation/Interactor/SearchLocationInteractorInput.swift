//
//  SearchLocationInteractorInput.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 07.12.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol SearchLocationInteractorInput: class {
    
    var searchAddressMinimumStringLength: Int { get }
    
    var obtainAutocompleteMinimumStringLength: Int { get }
    
    func searchSuggest(with string: String)
    
    func obtainAutocomplete(with string: String)
    
    func buildCallbackData(with callbackData: String?, for autocomplete: AddressAutocompleteResponse) -> String?
}

//
//  SearchTextViewDelegate.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 11.12.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol SearchTextViewDelegate: class {
    
    func didPressSend()
}

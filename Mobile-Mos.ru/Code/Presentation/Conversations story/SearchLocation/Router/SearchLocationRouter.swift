//
//  SearchLocationRouter.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 07.12.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class SearchLocationRouter: SearchLocationRouterInput {
    
    weak var transitionHandler: UIViewController!
    let unwindToChatSegueIdentifier = "unwindToChat"
    
    //MARK: - SearchLocationRouterInput
    
    func closeModule(completion: (() -> ())?) {
        transitionHandler.performSegue(withIdentifier: unwindToChatSegueIdentifier, sender: self)
        transitionHandler.transitionCoordinator?.animate(alongsideTransition: nil, completion: { _ in
            completion?()
        })
    }
}

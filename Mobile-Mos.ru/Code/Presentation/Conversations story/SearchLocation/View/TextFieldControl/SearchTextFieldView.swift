//
//  SearchTextFieldView.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 08.12.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import UIKit

class SearchTextFieldView: UIView {

    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var sendButton: UIButton!
    
    fileprivate var delegate: SearchTextViewDelegate?
    
    fileprivate static let nibFile = UINib(nibName: String(describing: SearchTextFieldView.self), bundle: nil)
    
    static func loadNib() -> SearchTextFieldView {
        return SearchTextFieldView.nibFile.instantiate(withOwner: nil, options: nil).first as! SearchTextFieldView
    }
    
    func configureDelegate(with delegate: SearchTextViewDelegate?) {
        self.delegate = delegate
    }
    
    @IBAction func sendButtonPressed(_ sender: Any) {
        delegate?.didPressSend()
    }
    
}

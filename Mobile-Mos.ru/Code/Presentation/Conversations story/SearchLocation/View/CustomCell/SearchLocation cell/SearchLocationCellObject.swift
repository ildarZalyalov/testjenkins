//
//  SearchLocationCellObject.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 08.12.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

struct SearchLocationCellObject: CellObjectWithId {
    
    var itemId: String
    
    var locationName: String
    
    var searchString: String 
}

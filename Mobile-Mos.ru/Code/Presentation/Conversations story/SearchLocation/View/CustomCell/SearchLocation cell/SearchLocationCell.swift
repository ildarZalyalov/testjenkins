//
//  SearchLocationCell.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 08.12.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import UIKit

class SearchLocationCell: UITableViewCell, ConfigurableView {
    
    @IBOutlet weak var locationNameLabel: UILabel!
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        super.setHighlighted(highlighted, animated: animated)
        contentView.alpha = highlighted ? 0.5 : 1
    }
    
    let lineHeightMultiple: CGFloat = 1.31
    
    //MARK: - ConfigurableView
    
    func configure(with object: Any) {
        
        guard let cellObject = object as? SearchLocationCellObject else {
            return
        }
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineHeightMultiple = lineHeightMultiple
        paragraphStyle.alignment = .left
        paragraphStyle.lineBreakMode = .byTruncatingTail
        
        let attributes: [NSAttributedStringKey : Any] = [NSAttributedStringKey.font : locationNameLabel.font, NSAttributedStringKey.paragraphStyle : paragraphStyle]
        let nameString = NSMutableAttributedString(string: cellObject.locationName, attributes: attributes)
        
        let range = (cellObject.locationName as NSString).range(of: cellObject.searchString, options: .caseInsensitive)
        if range.location != NSNotFound {
            nameString.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColorPalette.searchStringSubstringColor, range: range);
        }
        
        locationNameLabel.attributedText = nameString
    }
}

//
//  SearchLocationDataSourceFactoryImplementation.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 08.12.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class SearchLocationDataSourceFactoryImplementation: SearchLocationDataSourceFactory {
    
    //MARK: - SearchLocationDataSourceFactory
    
    func buildDataSourceConfiguration(from models: [YandexAddressSearchResponse], searchString: String) -> SearchLocationYandexDataSourceConfiguration {
        
        let dataStructure = TableViewDataSourceStructure()
        var selectionItems = [String : YandexAddressSearchResponse]()
        
        for model in models {
            
            let cellObject = SearchLocationCellObject(itemId: UUID().uuidString,
                                                      locationName: model.address,
                                                      searchString: searchString)
            
            dataStructure.appendCellObject(cellObject)
            selectionItems[cellObject.itemId] = model
        }
        
        let dataSource = TableViewDataSource(with: dataStructure)
        
        return (dataSource, selectionItems)
    }
    
    func buildDataSourceConfiguration(from models: [AddressAutocompleteResponse], searchString: String) -> SearchLocationAutocompleteDataSourceConfiguration {
        
        let dataStructure = TableViewDataSourceStructure()
        var selectionItems = [String : AddressAutocompleteResponse]()
        
        for model in models {
            
            let cellObject = SearchLocationCellObject(itemId: UUID().uuidString,
                                                      locationName: model.address,
                                                      searchString: searchString)
            
            dataStructure.appendCellObject(cellObject)
            selectionItems[cellObject.itemId] = model
        }
        
        let dataSource = TableViewDataSource(with: dataStructure)
        
        return (dataSource, selectionItems)
    }
}

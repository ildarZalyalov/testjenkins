//
//  SearchLocationDataSourceFactory.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 08.12.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

typealias SearchLocationYandexDataSourceConfiguration = (dataSource: TableViewDataSource, selectionItems: [String : YandexAddressSearchResponse])

typealias SearchLocationAutocompleteDataSourceConfiguration = (dataSource: TableViewDataSource, selectionItems: [String : AddressAutocompleteResponse])

protocol SearchLocationDataSourceFactory {

    func buildDataSourceConfiguration(from models: [YandexAddressSearchResponse], searchString: String) -> SearchLocationYandexDataSourceConfiguration
    
    func buildDataSourceConfiguration(from models: [AddressAutocompleteResponse], searchString: String) -> SearchLocationAutocompleteDataSourceConfiguration
}

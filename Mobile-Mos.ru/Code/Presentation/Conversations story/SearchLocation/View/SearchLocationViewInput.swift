//
//  SearchLocationViewInput.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 07.12.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol SearchLocationViewInput: class {
    
    func setup(with style: ConversationStyle)
    
    func setupHelpText(_ helpText: String)
    
    func displaySearchResult(with dataSource: TableViewDataSource)
    
    func showReversedName(_ name: String)
}

//
//  SearchLocationViewOutput.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 07.12.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol SearchLocationViewOutput: class {
    
    func setupInitialState()
    
    func didPressCancel()
    
    func canSearchTag(with text: String) -> Bool
    
    func didPressSubmit(with id: String)
    
    func didSelectObject(with id: String)
    
    func didSearchTag(with text: String)
    
    func configure(with configuration: LocationSelectionModuleConfiguration)
}

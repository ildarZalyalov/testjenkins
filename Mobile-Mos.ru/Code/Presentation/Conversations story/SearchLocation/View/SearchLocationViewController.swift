//
//  SearchLocationViewController.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 07.12.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import UIKit

class SearchLocationViewController: BaseViewController, SearchLocationViewInput, ConfigurableModuleController, UITextFieldDelegate, UITableViewDelegate, SearchTextViewDelegate {
    
    var output: SearchLocationViewOutput!
    
    @IBOutlet weak var cancelButton: UIBarButtonItem!
    @IBOutlet weak var searchTextFieldViewContainer: UIView!
    @IBOutlet weak var searchTextFieldViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var loadingProgressView: LoadingTimeoutProgressView!
    @IBOutlet weak var startTypingHelpTextLabel: UILabel!
    
    var statusBarStyle = UIStatusBarStyle.default
    var notificationCenter: NotificationCenter!
    var tableViewDataDisplayManager: TableViewDataSource!
    let navigationItemsFontSize: CGFloat = 15
    let searchRequestTimeout = 1.0
    let timeOutIntervalForProgress = TimeInterval(20.0)
    var lastSelectedId: String!
    
    lazy var searchTextFieldView: SearchTextFieldView = {
       return SearchTextFieldView.loadNib()
    }()
    
    lazy var searchRequestPerformer: DelayedSelectorPerformer = {
        return DelayedSelectorPerformer(target: self, selector: #selector(beginSearching(with:)))
    }()
    
    //MARK: - Методы
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        output.setupInitialState()
        
        searchTextFieldViewContainer.addSubview(searchTextFieldView)
        tableView.assignEmptyTableFooterToHideEmptyCellsSeparators()
        
        registerForKeyboardNotifications()
        setupTextField()
    }
    
    deinit {
        notificationCenter.removeObserver(self)
    }
    
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return statusBarStyle
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    //MARK: - ConfigurableModuleController
    
    func configureModule(with object: Any) {
        if let configuration = object as? LocationSelectionModuleConfiguration {
            output.configure(with: configuration)
        }
    }
    
    //MARK: - SearchLocationViewInput
    
    func setup(with style: ConversationStyle) {
        
        statusBarStyle = style.isStatusBarDark ? .default : .lightContent
        setNeedsStatusBarAppearanceUpdate()
        
        let attributes: [NSAttributedStringKey : Any] = [
            NSAttributedStringKey.font : UIFont.systemFont(ofSize: navigationItemsFontSize) as Any,
            NSAttributedStringKey.foregroundColor : style.controlsTextColor
        ]
        
        cancelButton.setTitleTextAttributes(attributes, for: .normal)
        cancelButton.setTitleTextAttributes(attributes, for: .highlighted)
        setupLoadingIndicatorColor(style.controlsTextColor)
    }
    
    func setupHelpText(_ helpText: String) {
        startTypingHelpTextLabel.text = helpText
    }
    
    func displaySearchResult(with dataSource: TableViewDataSource) {
        
        startTypingHelpTextLabel.isHidden = true
        
        tableViewDataDisplayManager = dataSource
        tableViewDataDisplayManager.delegate = self
        tableViewDataDisplayManager.assign(to: tableView)
        
        tableView.isHidden = dataSource.dataStructure.isEmpty()
        
        hideLoadingIndicator()
        tableView.reloadData()
    }
    
    func showReversedName(_ name: String) {
        searchTextFieldView.textField.text = name
        searchTextFieldView.sendButton.isEnabled = true
    }
    
    //MARK: - KeyboardNotifications
    
    @objc func keyboardWillShow(with notification: Notification) {
        
        guard let info = notification.userInfo,
            let keyboardEndSize = (info[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size
            else { return }
        
        searchTextFieldViewBottomConstraint.constant = keyboardEndSize.height
        
        if #available(iOS 11.0, *) {
            searchTextFieldViewBottomConstraint.constant -= view.safeAreaInsets.bottom
        }
    }
    
    //MARK: - UITableViewDelegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: false)
        
        let dataStructure = tableViewDataDisplayManager.dataStructure
        guard let cellObject = dataStructure.cellObject(at: indexPath) as? CellObjectWithId else {
            return
        }
        
        lastSelectedId = cellObject.itemId
        output.didSelectObject(with: cellObject.itemId)
    }
    
    //MARK: - SearchTextViewDelegate
    
    func didPressSend() {
        output.didPressSubmit(with: lastSelectedId)
    }
    
    //MARK: - Custom methods
    
    func setupTextField() {
        
        searchTextFieldView.textField.delegate = self
        searchTextFieldView.textField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        searchTextFieldView.textField.becomeFirstResponder()
        searchTextFieldView.configureDelegate(with: self)
    }
    
    func setupLoadingIndicatorColor(_ color: UIColor) {
        loadingProgressView.progressTintColor = color
    }
    
    func registerForKeyboardNotifications(){
        
        notificationCenter.addObserver(self,
                                       selector: #selector(self.keyboardWillShow(with:)),
                                       name: NSNotification.Name.UIKeyboardWillShow,
                                       object: nil)
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        
        searchRequestPerformer.cancelDelayedPerform()
        
        if let searchText = textField.text, !searchText.isEmpty {
            
            searchTextFieldView.sendButton.isEnabled = false
            searchRequestPerformer.argument = searchText
            searchRequestPerformer.perform(afterDelay: searchRequestTimeout)
        }
    }
    
    @objc func beginSearching(with text: String) {
        guard output.canSearchTag(with: text) else { return }
        displayLoadingIndicator()
        output.didSearchTag(with: text)
    }
    
    func hideLoadingIndicator() {
        
        let hideLoadingHandler = { [weak self] in
            
            guard let strongSelf = self else { return }
            
            strongSelf.loadingProgressView.completeProgressTimer()
        }
        
        DispatchQueue.main.async(execute: hideLoadingHandler)
    }
    
    func displayLoadingIndicator() {
        
        loadingProgressView.isHidden = false
        loadingProgressView.startProgressTimer(with: timeOutIntervalForProgress)
    }
    
    //MARK: - Buttons action
    
    @IBAction func cancelButtonPressed(_ sender: Any) {
        
        searchTextFieldView.textField.resignFirstResponder()
        output.didPressCancel()
    }
    
}

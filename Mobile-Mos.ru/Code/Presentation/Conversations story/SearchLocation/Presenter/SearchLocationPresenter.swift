//
//  SearchLocationPresenter.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 07.12.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class SearchLocationPresenter: SearchLocationViewOutput, SearchLocationInteractorOutput {
    
    weak var view: SearchLocationViewInput!
    var router: SearchLocationRouterInput!
    var interactor: SearchLocationInteractorInput!
    
    var moduleConfiguration: LocationSelectionModuleConfiguration!
    var dataSourceFactory: SearchLocationDataSourceFactory!
    
    var searchString = ""
    
    var currentReversedNames: [String : String] = [:]
    var selectionAddressesById: [String : YandexAddressSearchResponse]!
    
    var selectionAutocompleteById: [String : AddressAutocompleteResponse]!
    
    var shouldUseAutocomplete: Bool {
        return moduleConfiguration.shouldUseAutocomplete
    }
    
    //MARK: - Приватные методы
    
    func handleSubmitAddress(_ address: YandexAddressSearchResponse) {
        
        let handler = moduleConfiguration.resultHandler
        let messageText = String()
        let location = address.location
        let messageContext = moduleConfiguration.messageContext
        let title = StringsHelper.chatLocationOnMapTitle
        let description = address.address
        
        router.closeModule {
            handler?.handleSendMessage(with: messageText,
                                       location: location,
                                       locationTitle: title,
                                       locationDescription: description,
                                       context: messageContext)
        }
    }
    
    func handleSubmitAutocomplete(_ autocomplete: AddressAutocompleteResponse) {
        
        let handler = moduleConfiguration.resultHandler
        let messageText = autocomplete.address
        let messageContext = moduleConfiguration.messageContext
        
        let callbackData = interactor.buildCallbackData(with: moduleConfiguration.callbackData, for: autocomplete)
        let message = moduleConfiguration.message
        let embeddedMessage = moduleConfiguration.embeddedMessage
        
        router.closeModule {
            handler?.handleSendMessage(with: messageText, context: messageContext)
            guard let data = callbackData else { return }
            handler?.handleSendCallback(with: data, for: message, embeddedMessage: embeddedMessage)
        }
    }
    
    //MARK: - SearchLocationViewOutput
    
    func setupInitialState() {
        
        view.setup(with: moduleConfiguration.style)
        
        if shouldUseAutocomplete {
            view.setupHelpText(StringsHelper.chatAutocompleteStartHelpText)
        }
        else {
            view.setupHelpText(StringsHelper.chatAddressSearchStartHelpText)
        }
    }
    
    func configure(with configuration: LocationSelectionModuleConfiguration) {
        self.moduleConfiguration = configuration
    }
    
    func didPressCancel() {
        router.closeModule(completion: nil)
    }
    
    func canSearchTag(with text: String) -> Bool {
        let minimumLength = shouldUseAutocomplete ? interactor.obtainAutocompleteMinimumStringLength : interactor.searchAddressMinimumStringLength
        return text.count >= minimumLength
    }
    
    func didSearchTag(with text: String) {
    
        guard canSearchTag(with: text) else { return }
        
        searchString = text
        
        if shouldUseAutocomplete {
            interactor.obtainAutocomplete(with: text)
        }
        else {
            interactor.searchSuggest(with: text)
        }
    }
    
    func didSelectObject(with id: String) {
        
        if let autocomplete = selectionAutocompleteById[id] {
            handleSubmitAutocomplete(autocomplete)
        }
        else {
            
            guard let object = selectionAddressesById[id] else { return }
            
            if let reversedName = currentReversedNames[object.address] {
                view.showReversedName(reversedName)
            }
            else {
                view.showReversedName(object.address)
            }
        }
    }
    
    func didPressSubmit(with id: String) {
        guard let address = selectionAddressesById[id] else { return }
        handleSubmitAddress(address)
    }
    
    //MARK: - SearchLocationInteractorOutput
    
    func didFinishSearchingSuggests(with result: YandexAddressSearchRequestResult) {
        
        switch result {
            
        case .success(response:(let suggests, let names)):
            
            let configuration = dataSourceFactory.buildDataSourceConfiguration(from: suggests, searchString: searchString)
            currentReversedNames = names
            selectionAddressesById = configuration.selectionItems
            
            view.displaySearchResult(with: configuration.dataSource)
            
        case .failure(let error):
            print("Ошибка поиска по саджестам: \(error.localizedDescription)")
        }
    }
    
    func didFinishObtainingAutocomplete(with result: AddressAutocompleteMultipleObtainResult) {
        
        switch result {
            
        case .success(let addresses):
            
            let configuration = dataSourceFactory.buildDataSourceConfiguration(from: addresses, searchString: searchString)
            selectionAutocompleteById = configuration.selectionItems
            
            view.displaySearchResult(with: configuration.dataSource)
            
        case .failure(let error):
            print("Ошибка поиска по автокомплиту: \(error.localizedDescription)")
        }
    }
}

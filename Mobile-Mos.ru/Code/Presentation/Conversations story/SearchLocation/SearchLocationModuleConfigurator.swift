//
//  SearchLocationModuleConfigurator.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 07.12.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import UIKit

class SearchLocationModuleConfigurator: BaseModuleConfigurator {
    
    override func configureModule(for view: UIViewController) {
        
        let viewController = view as! SearchLocationViewController
        let presenter = SearchLocationPresenter()
        let interactor = SearchLocationInteractor()
        let router = SearchLocationRouter()
        let notificationCenterDefault = NotificationCenter.default
        let yandexGeocoderService = UIApplication.shared.serviceBuilder.getYandexGeocoderService()
        let autocompleteService = UIApplication.shared.serviceBuilder.getAddressAutocompleteService()
        let dataSourceFactory = SearchLocationDataSourceFactoryImplementation()
        
        viewController.output = presenter
        viewController.notificationCenter = notificationCenterDefault
        
        presenter.view = viewController
        presenter.interactor = interactor
        presenter.router = router
        presenter.dataSourceFactory = dataSourceFactory
        
        interactor.output = presenter
        interactor.yandexGeocoderService = yandexGeocoderService
        interactor.autocompleteService = autocompleteService
        
        router.transitionHandler = viewController
    }
}

//
//  DateAndTimeSelectionRouterInput.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 12.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol DateAndTimeSelectionRouterInput: class {

    func closeModule(completion: (() -> ())?)
}

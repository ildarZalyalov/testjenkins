//
//  DateAndTimeSelectionModuleConfigurator.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 12.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import UIKit

class DateAndTimeSelectionModuleConfigurator: BaseModuleConfigurator {
    
    override func configureModule(for view: UIViewController) {
        
        let viewController = view as! DateAndTimeSelectionViewController
        let presenter = DateAndTimeSelectionPresenter()
        let interactor = DateAndTimeSelectionInteractor()
        let router = DateAndTimeSelectionRouter()
        
        let dateDataSourceFactory = DateSelectionDataSourceFactoryImplementation()
        let timeDataSourceFactory = TimeSelectionDataSourceFactoryImplementation()
        
        viewController.output = presenter
        
        presenter.view = viewController
        presenter.interactor = interactor
        presenter.router = router
        presenter.dateComponentsDataSourceFactory = dateDataSourceFactory
        presenter.timeComponentsDataSourceFactory = timeDataSourceFactory
        
        interactor.output = presenter
        
        router.transitionHandler = viewController
    }
}

//
//  DateAndTimeSelectionInteractor.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 12.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class DateAndTimeSelectionInteractor: DateAndTimeSelectionInteractorInput {
    
    weak var output: DateAndTimeSelectionInteractorOutput!
    
    lazy var dateFormatter: DateFormatter = {
        
        let localeIdentifier = "en_US_POSIX"
        let timeZoneAbbreviation = "UTC"
        
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: localeIdentifier)
        formatter.timeZone = TimeZone(abbreviation: timeZoneAbbreviation)
        formatter.dateStyle = .none
        formatter.timeStyle = .short
        return formatter
    }()
    
    //MARK: - DateAndTimeSelectionInteractorInput
    
    func computeDateAndTimeSelectionParameters(for control: ChatMessageTimetableControl, with calendar: Calendar) {
        
        var minimumDate: Date = Date.distantFuture
        var maximumDate: Date = Date.distantPast
        var displayedYear: Int? = nil
        
        defer {
            
            let parameters = DateAndTimeSelectionParameters(minimumDate: minimumDate,
                                                            maximumDate: maximumDate,
                                                            displayedYear: displayedYear)
            
            output.didFinishComputingSelectionParameters(parameters: parameters)
        }
        
        for (date, _) in control.datesAndTimes {
            minimumDate = date < minimumDate ? date : minimumDate
            maximumDate = date > maximumDate ? date : maximumDate
        }
        
        displayedYear = calendar.dateComponents([.year], from: minimumDate).year
    }
    
    func controlValue(for dateAndTime: Date, dateFormat: String, timeFormat: String, dateTimeSeparator: String) -> String {
        dateFormatter.dateFormat = dateFormat + dateTimeSeparator + timeFormat
        return dateFormatter.string(from: dateAndTime)
    }
    
    func controlCallbackValue(for dateAndTime: Date, callbackData: String?, dateFormat: String, timeFormat: String, timeData: String?) -> String?{
        
        guard let data = callbackData else { return nil }
        
        dateFormatter.dateFormat = dateFormat
        let dateValue = dateFormatter.string(from: dateAndTime)
        
        dateFormatter.dateFormat = timeFormat
        let timeValue = dateFormatter.string(from: dateAndTime)
        
        var controlValue: [String : String] = [
            "date" : dateValue,
            "time" : timeValue,
        ]
        
        if timeData != nil {
            controlValue["data"] = timeData!
        }
        
        let jsonDictionary: [String : Any] = [
            "callback" : data,
            "control_value" : controlValue
        ]
        
        guard let jsonData = try? JSONSerialization.data(withJSONObject: jsonDictionary, options: []) else { return nil }
        
        return String(data: jsonData, encoding: .utf8)
    }
}

//
//  DateAndTimeSelectionInteractorInput.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 12.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol DateAndTimeSelectionInteractorInput: class {
    
    func computeDateAndTimeSelectionParameters(for control: ChatMessageTimetableControl, with calendar: Calendar)
    
    func controlValue(for dateAndTime: Date, dateFormat: String, timeFormat: String, dateTimeSeparator: String) -> String
    
    func controlCallbackValue(for dateAndTime: Date, callbackData: String?, dateFormat: String, timeFormat: String, timeData: String?) -> String?
}

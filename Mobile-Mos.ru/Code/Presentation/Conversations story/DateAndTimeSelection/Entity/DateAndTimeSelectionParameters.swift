//
//  DateAndTimeSelectionParameters.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 07.12.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

struct DateAndTimeSelectionParameters {
    
    var minimumDate: Date
    
    var maximumDate: Date
    
    var displayedYear: Int?
}

//
//  DateAndTimeSelectionViewInput.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 12.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol DateAndTimeSelectionViewInput: class {
    
    func setup(with style: ConversationStyle)
    
    func displayTitle(_ title: String)
    
    func displayYears(with dataSource: [HorizontalPickerElementCellObject])
    
    func scrollToYear(with yearId: String, animated: Bool)
    
    func displayMonths(with dataSource: [HorizontalPickerElementCellObject])
    
    func scrollToMonth(with monthId: String, animated: Bool)
    
    func displayCalendar(with calendar: Calendar, minimumDate: Date, maximumDate: Date)
    
    func dispayTimes(with dataSource: CollectionViewDataSource)
    
    func hideTimes()
    
    func displaySubmitButton(with title: String)
    
    func hideSubmitButton()
    
    func scrollCalendar(to date: Date, animated: Bool)
    
    func selectDateInCalendar(date: Date?)
    
    func setYearSelection(enabled: Bool)
    
    func setMonthSelection(enabled: Bool)
}

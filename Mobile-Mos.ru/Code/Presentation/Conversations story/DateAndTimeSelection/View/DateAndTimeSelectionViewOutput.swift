//
//  DateAndTimeSelectionViewOutput.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 12.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol DateAndTimeSelectionViewOutput: class {
    
    func setupInitialState()
    
    func configure(with configuration: DateAndTimeSelectionModuleConfiguration)
    
    func isEnabledDate(date: Date) -> Bool
    
    func didScrollYear(to yearId: String)
    
    func didScrollMonth(to monthId: String)
    
    func didScrollCalendar(to date: Date)
    
    func didSelectInCalendar(date: Date)
    
    func didSelectTime(with timeId: String)
    
    func didPressCancel()
    
    func didPressSubmit()
}

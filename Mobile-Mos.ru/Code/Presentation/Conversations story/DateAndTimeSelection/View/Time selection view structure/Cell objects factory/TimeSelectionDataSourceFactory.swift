//
//  TimeSelectionDataSourceFactory.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 13.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Конфигурация источника данных для выбора времени
typealias TimeSelectionDataSourceConfiguration = (dataSource: CollectionViewDataSource, selectionItems: [String : ChatMessageTimetableControl.TimeValue])

/// Фабрика источника данных для выбора времени
protocol TimeSelectionDataSourceFactory {
    
    /// Построить источник данных для выбора времени
    ///
    /// - Parameters:
    ///   - model: набор дат со временем
    ///   - timeStringColor: цвет для отображение текста со временем
    /// - Returns: источник данных
    func buildDataSourceConfiguration(from model: [ChatMessageTimetableControl.TimeValue], and timeStringColor: UIColor) -> TimeSelectionDataSourceConfiguration
}

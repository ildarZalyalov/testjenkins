//
//  TimeSelectionDataSourceFactoryImplementation.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 13.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

struct TimeSelectionHeaderCellObject: HeaderObject {}

class TimeSelectionDataSourceFactoryImplementation: TimeSelectionDataSourceFactory {
    
    fileprivate lazy var timeFormatter: DateFormatter = {
        
        let localeIdentifier = "ru_RU"
        let timeZoneAbbreviation = "UTC"
        
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: localeIdentifier)
        formatter.timeZone = TimeZone(abbreviation: timeZoneAbbreviation)
        formatter.dateFormat = "HH:mm"
        
        return formatter
    }()
    
    //MARK: - TimeSelectionDataSourceFactory
    
    func buildDataSourceConfiguration(from model: [ChatMessageTimetableControl.TimeValue], and timeStringColor: UIColor) -> TimeSelectionDataSourceConfiguration {
        
        let dataStructure = CollectionViewDataSourceStructure()
        var selectionItems = [String : ChatMessageTimetableControl.TimeValue]()
        
        dataStructure.appendSection(with: TimeSelectionHeaderCellObject())
        
        for timeValue in model {
            
            let cellObject = TimeSelectionCellObject(itemId: UUID().uuidString, timeString: timeFormatter.string(from: timeValue.time), timeStringColor: timeStringColor)
            
            dataStructure.appendCellObject(cellObject)
            selectionItems[cellObject.itemId] = timeValue
        }
        
        let dataSource = CollectionViewDataSource(with: dataStructure)
        
        return TimeSelectionDataSourceConfiguration(dataSource: dataSource, selectionItems: selectionItems)
    }
}

//
//  TimeSelectionCellObject.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 13.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Cell object ячейки для отображения времени
struct TimeSelectionCellObject: CellObjectWithId {
    
    /// Идентификатор конкретного значения времени
    var itemId: String
    
    /// Строка с временем
    let timeString: String
    
    /// Цвет для отображения строки с временем
    let timeStringColor: UIColor
}

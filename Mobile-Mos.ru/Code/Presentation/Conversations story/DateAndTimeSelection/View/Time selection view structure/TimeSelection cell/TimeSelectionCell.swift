//
//  TimeSelectionCell.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 13.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import UIKit

/// Ячейка для отображения времени
class TimeSelectionCell: UICollectionViewCell, ConfigurableView {
    
    @IBOutlet weak var timeLabel: UILabel!
    
    var timeStringColor: UIColor?
    
    override var isHighlighted: Bool {
        didSet {
            timeLabel.textColor = isHighlighted ? UIColor.white : timeStringColor
            contentView.backgroundColor = isHighlighted ? timeStringColor : UIColor.white
        }
    }
    
    override var isSelected: Bool {
        didSet {
            timeLabel.textColor = isSelected ? UIColor.white : timeStringColor
            contentView.backgroundColor = isSelected ? timeStringColor : UIColor.white
        }
    }
    
    //MARK: - ConfigurableView
    
    func configure(with object: Any) {
        
        guard let cellObject = object as? TimeSelectionCellObject else {
            return
        }
        
        timeLabel.text = cellObject.timeString
        timeLabel.textColor = cellObject.timeStringColor
        
        timeStringColor = cellObject.timeStringColor
    }
}

//
//  DateAndTimeSelectionModuleConfiguration.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 12.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Конфигурация модуля расписания
struct DateAndTimeSelectionModuleConfiguration {
    
    /// Информация о контроле расписания
    var control: ChatMessageTimetableControl
    
    /// Стиль текущего чата
    var style: ConversationStyle
    
    /// Контекст для отправки сообщения (если есть)
    var messageContext: String?
    
    // Данные для коллбэк запроса (если есть)
    var callbackData: String?
    
    /// Сообщение, с которым связан контрол (если есть)
    var message: ChatMessage?
    
    /// Вложенное сообщение, если сообщение является каруселью
    var embeddedMessage: ChatMessage?
    
    /// Обработчик результата работы контрола
    weak var resultHandler: ChatCustomControlResultHandler?
    
    init(control: ChatMessageTimetableControl, style: ConversationStyle) {
        self.control = control
        self.style = style
    }
}

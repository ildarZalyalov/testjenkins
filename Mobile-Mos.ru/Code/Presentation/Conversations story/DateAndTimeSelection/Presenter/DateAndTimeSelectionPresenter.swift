//
//  DateAndTimeSelectionPresenter.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 12.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class DateAndTimeSelectionPresenter: DateAndTimeSelectionViewOutput, DateAndTimeSelectionInteractorOutput {
    
    weak var view: DateAndTimeSelectionViewInput!
    var router: DateAndTimeSelectionRouterInput!
    var interactor: DateAndTimeSelectionInteractorInput!
    
    var dateComponentsDataSourceFactory: DateSelectionDataSourceFactory!
    var yearSelectionItems = [String : DateComponents]()
    var monthSelectionItems = [String : DateComponents]()
    
    var timeComponentsDataSourceFactory: TimeSelectionDataSourceFactory!
    var timeSelectionItems = [String : ChatMessageTimetableControl.TimeValue]()
    
    var moduleConfiguration: DateAndTimeSelectionModuleConfiguration!
    var control: ChatMessageTimetableControl {
        return moduleConfiguration.control
    }
    
    var displayedYear: Int? {
        didSet {
            guard displayedYear != oldValue else { return }
            refreshMonths()
        }
    }
    
    var calendar: Calendar {
        return dateComponentsDataSourceFactory.calendar
    }
    
    var minimumDate = Date()
    var maximumDate = Date()
    
    var selectedDate: Date? {
        
        didSet {
            
            if selectedDate != nil {
                refreshTimes()
            }
            else if oldValue != nil {
                view.selectDateInCalendar(date: nil)
                view.hideTimes()
            }
            
            selectedTime = nil
        }
    }
    
    var selectedTime: Date? {
        didSet {
            if oldValue == nil && selectedTime != nil {
                view.displaySubmitButton(with: control.submitTitle)
            }
            else if oldValue != nil && selectedTime == nil {
                view.hideSubmitButton()
            }
        }
    }
    
    var selectedTimeValue: ChatMessageTimetableControl.TimeValue?
    
    //MARK: - Приватные методы
    
    func initializeDateAndTimeSelection() {
        
        view.displayTitle(control.title)
        view.setup(with: moduleConfiguration.style)
        
        let yearConfiguration = dateComponentsDataSourceFactory.buildDataSourceConfigurationForYears(with: minimumDate, and: maximumDate)
        yearSelectionItems = yearConfiguration.selectionItems
        
        view.displayYears(with: yearConfiguration.dataSource)

        refreshMonths()
        
        view.displayCalendar(with: calendar, minimumDate: minimumDate, maximumDate: maximumDate)
    }
    
    func selectDateInPickers(date: Date, animated: Bool) {
        
        let dateComponents = calendar.dateComponents([.year, .month], from: date)
        
        if let year = yearSelectionItems.filter({ $0.value.year == dateComponents.year }).first {
            displayedYear = year.value.year
            view.scrollToYear(with: year.key, animated: animated)
        }
        
        if let month = monthSelectionItems.filter({ $0.value.month == dateComponents.month }).first {
            view.scrollToMonth(with: month.key, animated: animated)
        }
    }
    
    func refreshMonths() {
        
        let limit = DateSelectionDataSourceMonthsLimit(displayedYear: displayedYear, minimumDate: minimumDate, maximumDate: maximumDate)
        let monthConfiguration = dateComponentsDataSourceFactory.buildDataSourceConfigurationForMonths(with: limit)
        monthSelectionItems = monthConfiguration.selectionItems
        
        view.displayMonths(with: monthConfiguration.dataSource)
    }
    
    func refreshTimes() {
        
        guard let selected = selectedDate else { return }
        
        guard let dateKey = control.datesAndTimes.keys.filter({
            self.calendar.compare($0, to: selected, toGranularity: .day) == .orderedSame
        }).first  else { return }
        
        guard let times = control.datesAndTimes[dateKey] else { return }
        
        let timeConfiguration = timeComponentsDataSourceFactory.buildDataSourceConfiguration(from: times, and: moduleConfiguration.style.controlsTextColor)
        timeSelectionItems = timeConfiguration.selectionItems
        
        view.dispayTimes(with: timeConfiguration.dataSource)
    }
    
    fileprivate func dropSelectedDateIfNeeded(displayedMonthDate: Date) {
        
        guard let selected = selectedDate else { return }
        
        if calendar.compare(displayedMonthDate, to: selected, toGranularity: .month) != .orderedSame {
            selectedDate = nil
            selectedTime = nil
        }
    }
    
    //MARK: - DateAndTimeSelectionViewOutput
    
    func setupInitialState() {
        interactor.computeDateAndTimeSelectionParameters(for: control, with: calendar)
    }
    
    func configure(with configuration: DateAndTimeSelectionModuleConfiguration) {
        self.moduleConfiguration = configuration
    }
    
    func isEnabledDate(date: Date) -> Bool {
        
        for enabledDate in control.datesAndTimes.keys {
            if calendar.compare(date, to: enabledDate, toGranularity: .day) == .orderedSame {
                return true
            }
        }
        
        return false
    }
    
    func didScrollYear(to yearId: String) {
        
        guard let year = yearSelectionItems[yearId] else { return }
        guard year.year != displayedYear else { return }
        guard var yearDate = calendar.date(from: year) else { return }
        
        var scrolledToPreviousYear = false
        if displayedYear != nil && year.year != nil {
            scrolledToPreviousYear = displayedYear! > year.year!
        }
        
        displayedYear = year.year
        
        if scrolledToPreviousYear {
            yearDate = calendar.date(byAdding: .year, value: 1, to: yearDate) ?? yearDate
            yearDate = calendar.date(byAdding: .month, value: -1, to: yearDate) ?? yearDate
        }
        
        // скроллинг года вперед перелистывает месяцы на начало, скроллинг назад - на последний месяц, но, если год в прошлом есть, значит как минимум последний месяц доступен, иначе имеем "дыру"
        // поэтому проверяем только минимальную дату
        if calendar.compare(yearDate, to: minimumDate, toGranularity: .month) == .orderedAscending {
            yearDate = minimumDate
        }
        
        if scrolledToPreviousYear, let lastMonthComponent = calendar.dateComponents([.month], from: yearDate).month, let lastMonthItemId = monthSelectionItems.first(where: { $0.value.month == lastMonthComponent })?.key {
            view.scrollToMonth(with: lastMonthItemId, animated: false)
        }

        dropSelectedDateIfNeeded(displayedMonthDate: yearDate)
        view.scrollCalendar(to: yearDate, animated: false)
    }
    
    func didScrollMonth(to monthId: String) {
        
        guard var month = monthSelectionItems[monthId] else { return }
        month.year = displayedYear
        guard let monthDate = calendar.date(from: month) else { return }
        
        view.scrollCalendar(to: monthDate, animated: true)
        dropSelectedDateIfNeeded(displayedMonthDate: monthDate)
    }
    
    func didScrollCalendar(to date: Date) {
        selectDateInPickers(date: date, animated: true)
        dropSelectedDateIfNeeded(displayedMonthDate: date)
    }
    
    func didSelectInCalendar(date: Date) {
        guard date != selectedDate else { return }
        selectedDate = date
    }
    
    func didSelectTime(with timeId: String) {
        guard let dateAndTime = timeSelectionItems[timeId] else { return }
        guard dateAndTime.time != selectedTime else { return }
        selectedTime = dateAndTime.time
        selectedTimeValue = dateAndTime
    }
    
    func didPressCancel() {
        router.closeModule(completion: nil)
    }
    
    func didPressSubmit() {
        
        guard let dateAndTime = selectedTime else { return }
        guard let dateAndTimeValue = selectedTimeValue else { return }
        
        let handler = moduleConfiguration.resultHandler
        
        let messageText = interactor.controlValue(for: dateAndTime, dateFormat: control.dateOutputFormat, timeFormat: control.timeOutputFormat, dateTimeSeparator: control.dateTimeSeparatorString)
        let messageContext = moduleConfiguration.messageContext
        
        let callBackData = interactor.controlCallbackValue(for: dateAndTime, callbackData: moduleConfiguration.callbackData, dateFormat: control.dateOutputFormat, timeFormat: control.timeOutputFormat, timeData: dateAndTimeValue.data)
        let message = moduleConfiguration.message
        let embeddedMessage = moduleConfiguration.embeddedMessage
        
        router.closeModule {
            
            guard handler != nil else { return }
            
            if messageContext != nil {
                handler!.handleSendMessage(with: messageText, context: messageContext!)
            }
            
            if callBackData != nil {
                handler!.handleSendCallback(with: callBackData!, for: message, embeddedMessage: embeddedMessage)
            }
        }
    }
    
    //MARK: - DateAndTimeSelectionInteractorOutput
    
    func didFinishComputingSelectionParameters(parameters: DateAndTimeSelectionParameters) {
        
        minimumDate = parameters.minimumDate
        maximumDate = parameters.maximumDate
        displayedYear = parameters.displayedYear
        
        initializeDateAndTimeSelection()
    }
}

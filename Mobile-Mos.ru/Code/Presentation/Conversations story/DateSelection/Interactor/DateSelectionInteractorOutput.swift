//
//  DateSelectionInteractorOutput.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 05.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol DateSelectionInteractorOutput: class {
    
    func didFinishComputingDateSelectionParameters(parameters: DateSelectionParameters)
}

//
//  DateSelectionInteractorInput.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 05.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol DateSelectionInteractorInput: class {
    
    func computeDateSelectionParameters(for control: ChatMessageCalendarControl, with calendar: Calendar)
    
    func controlValue(for date: Date, with format: String?) -> String?
    
    func controlCallbackValue(for date: Date, callbackData: String?, format: String?) -> String?
}

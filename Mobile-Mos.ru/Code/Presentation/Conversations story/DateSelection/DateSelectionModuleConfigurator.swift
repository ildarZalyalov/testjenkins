//
//  DateSelectionModuleConfigurator.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 05.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import UIKit

class DateSelectionModuleConfigurator: BaseModuleConfigurator {
    
    override func configureModule(for view: UIViewController) {
        
        let viewController = view as! DateSelectionViewController
        let presenter = DateSelectionPresenter()
        let interactor = DateSelectionInteractor()
        let router = DateSelectionRouter()
        
        let dataSourceFactory = DateSelectionDataSourceFactoryImplementation()
        
        viewController.output = presenter
        
        presenter.view = viewController
        presenter.interactor = interactor
        presenter.router = router
        presenter.dateComponentsDataSourceFactory = dataSourceFactory
        
        interactor.output = presenter
        
        router.transitionHandler = viewController
    }
}

//
//  DateSelectionRouter.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 05.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class DateSelectionRouter: DateSelectionRouterInput {
    
    weak var transitionHandler: UIViewController!
    
    let unwindToChatSegueIdentifier = "unwindToChat"
    
    //MARK: - DateSelectionRouterInput
    
    func closeModule(completion: (() -> ())?) {
        transitionHandler.performSegue(withIdentifier: unwindToChatSegueIdentifier, sender: self)
        transitionHandler.transitionCoordinator?.animate(alongsideTransition: nil, completion: { _ in
            completion?()
        })
    }
}

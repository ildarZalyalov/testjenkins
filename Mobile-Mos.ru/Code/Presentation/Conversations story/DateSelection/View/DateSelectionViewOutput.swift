//
//  DateSelectionViewOutput.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 05.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol DateSelectionViewOutput: class {
    
    func setupInitialState()
    
    func configure(with configuration: DateSelectionModuleConfiguration)
    
    func isEnabledDate(date: Date) -> Bool
    
    func didScrollYear(to yearId: String)
    
    func didScrollMonth(to monthId: String)
    
    func didScrollCalendar(to date: Date)
    
    func didSelectInCalendar(date: Date)
    
    func didPressCancel()
    
    func didPressSubmit()
}

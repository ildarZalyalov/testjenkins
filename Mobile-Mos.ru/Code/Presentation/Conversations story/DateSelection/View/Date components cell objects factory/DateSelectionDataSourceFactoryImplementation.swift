//
//  DateSelectionDataSourceFactoryImplementation.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 06.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class DateSelectionDataSourceFactoryImplementation: DateSelectionDataSourceFactory {
    
    fileprivate static let timeZoneAbbreviation = "UTC"
    fileprivate static var timeZone = TimeZone(abbreviation: timeZoneAbbreviation)
    
    fileprivate static let localeIdentifier = "ru_RU"
    fileprivate static var calendar: Calendar = {
       
        var calendar = Calendar.autoupdatingCurrent
        calendar.locale = Locale(identifier: localeIdentifier)
        
        if let zone = timeZone {
            calendar.timeZone = zone
        }
        
        return calendar
    }()
    
    fileprivate typealias Class = DateSelectionDataSourceFactoryImplementation
    
    //MARK: - Приватные методы
    
    fileprivate func months(_ monthNames: [String],
                            byClampingWith minimumIndex: Int?,
                            and maximumIndex: Int?) -> [Int : String] {
        
        let availableRange: ClosedRange = 0 ... monthNames.count - 1
        
        let minimum = (minimumIndex ?? 0).clamped(to: availableRange)
        let maximum = (maximumIndex ?? monthNames.count - 1).clamped(to: availableRange)
        
        return monthNames[minimum ... maximum].toIndicesDictionary()
    }
    
    fileprivate func months(_ monthNames: [String],
                            with limit: DateSelectionDataSourceMonthsLimit?) -> [Int : String] {
        
        guard let limitation = limit else { return monthNames.toIndicesDictionary() }
        
        let minimumComponents = Class.calendar.dateComponents([.year, .month], from: limitation.minimumDate)
        let maximumComponents = Class.calendar.dateComponents([.year, .month], from: limitation.maximumDate)
        
        var minimumIndex: Int? = nil
        var maximumIndex: Int? = nil
        
        if minimumComponents.year == limitation.displayedYear, let month = minimumComponents.month {
            minimumIndex = month - 1
        }
        if maximumComponents.year == limitation.displayedYear, let month = maximumComponents.month {
            maximumIndex = month - 1
        }
        
        return months(monthNames, byClampingWith: minimumIndex, and: maximumIndex)
    }
    
    //MARK: - DateSelectionDataSourceFactory
    
    var calendar: Calendar {
        return Class.calendar
    }
    
    var timeZone: TimeZone {
        return Class.calendar.timeZone
    }
    
    func buildDataSourceConfigurationForYears(with minimumDate: Date, and maximumDate: Date) -> DateSelectionDataSourceConfiguration {
        
        var dataSource = [HorizontalPickerElementCellObject]()
        var selectionItems = [String : DateComponents]()
        
        var startDateComponents = Class.calendar.dateComponents([.year], from: minimumDate)
        startDateComponents.month = 1
        startDateComponents.day = 1
        
        var startDate = Class.calendar.date(from: startDateComponents) ?? minimumDate
        startDate = Class.calendar.date(byAdding: .day, value: -1, to: startDate) ?? startDate
        
        var yearSet = Set<Int>()
        let eachMonthFirstDayComponents = DateComponents(month: 1, day: 1)
        
        Class.calendar.enumerateDates(startingAfter: startDate, matching: eachMonthFirstDayComponents, matchingPolicy: .strict, repeatedTimePolicy: .first, direction: .forward) { date, _, stop in
            
            guard let date = date else {
                stop = true
                return
            }
            
            stop = date > maximumDate
            
            guard !stop else { return }
            
            let selectionItem = Class.calendar.dateComponents([.year], from: date)
            
            guard let year = selectionItem.year else { return }

            yearSet.insert(year)
        }
        
        let years = Array(yearSet).sorted(by: <)
        for year in years {

            let cellObject = HorizontalPickerElementCellObject(elementId: UUID().uuidString,
                                                               title: String(year).firstLetterCapitalized())
            dataSource.append(cellObject)
            selectionItems[cellObject.itemId] = DateComponents(year: year)
        }
        
        return (dataSource, selectionItems)
    }
    
    func buildDataSourceConfigurationForMonths(with limit: DateSelectionDataSourceMonthsLimit) -> DateSelectionDataSourceConfiguration {
        
        var dataSource = [HorizontalPickerElementCellObject]()
        var selectionItems = [String : DateComponents]()
        
        let monthNames = months(Class.calendar.standaloneMonthSymbols, with: limit)
        
        for (index, monthName) in monthNames.sorted(by: <) {

            let cellObject = HorizontalPickerElementCellObject(elementId: UUID().uuidString,
                                                               title: monthName.firstLetterCapitalized())
            dataSource.append(cellObject)
            
            selectionItems[cellObject.itemId] = DateComponents(calendar: Class.calendar,
                                                               timeZone: Class.timeZone,
                                                               year: limit.displayedYear,
                                                               month: index + 1)
        }
        
        return (dataSource, selectionItems)
    }
}

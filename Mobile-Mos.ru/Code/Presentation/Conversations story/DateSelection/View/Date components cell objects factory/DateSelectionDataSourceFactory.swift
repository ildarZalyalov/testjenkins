//
//  DateSelectionDataSourceFactory.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 06.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Конфигурация источника данных для выбора дат
typealias DateSelectionDataSourceConfiguration = (dataSource: [HorizontalPickerElementCellObject], selectionItems: [String : DateComponents])

/// Ограничение для источника данных для выбора месяца
struct DateSelectionDataSourceMonthsLimit {
    
    /// Отображаемый год (если есть)
    var displayedYear: Int?
    
    /// Минимальная дата
    var minimumDate: Date
    
    /// Максимальная дата
    var maximumDate: Date
}

/// Фабрика источника данных для выбора дат
protocol DateSelectionDataSourceFactory {
    
    /// Используемый календарь
    var calendar: Calendar { get }
    
    /// Используемый часовой пояс
    var timeZone: TimeZone { get }
    
    /// Построить источник данных для выбора года
    ///
    /// - Parameters:
    ///   - minimumDate: Минимальная дата для выбора
    ///   - maximumDate: Максимальная дата для выбора
    /// - Returns: источник данных
    func buildDataSourceConfigurationForYears(with minimumDate: Date, and maximumDate: Date) -> DateSelectionDataSourceConfiguration
    
    /// Построить источник данных для выбора месяца
    ///
    /// - Parameter limit: ограничение для источника данных
    /// - Returns: источник данных
    func buildDataSourceConfigurationForMonths(with limit: DateSelectionDataSourceMonthsLimit) -> DateSelectionDataSourceConfiguration
}

//
//  CalendarViewCell.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 09.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import UIKit
import JTAppleCalendar

/// Ячейка с днем в календаре
class CalendarViewCell: JTAppleCell {
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var selectionView: UIView!
    
    override func prepareForReuse() {
        selectionView.isHidden = true
    }
    
    /// Обработать выделение ячейки
    ///
    /// - Parameter isSelected: выделена ли ячейка
    func handleSelection(isSelected: Bool) {
        dateLabel.font = isSelected ? UIFont(customName: .graphikLCGMedium, size: 16) : UIFont(customName: .graphikLCGRegular, size: 16)
        selectionView.isHidden = !isSelected || dateLabel.isHidden
    }
}

//
//  DateSelectionViewInput.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 05.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol DateSelectionViewInput: class {
    
    func setup(with style: ConversationStyle)
    
    func displayTitle(_ title: String)
    
    func displayYears(with dataSource: [HorizontalPickerElementCellObject])
    
    func scrollToYear(with yearId: String, animated: Bool)
    
    func displayMonths(with dataSource: [HorizontalPickerElementCellObject])
    
    func scrollToMonth(with monthId: String, animated: Bool)
    
    func displayCalendar(with calendar: Calendar, minimumDate: Date, maximumDate: Date)
    
    func displaySubmitButton(with title: String)
    
    func hideSubmitButton()
    
    func scrollCalendar(to date: Date, animated: Bool)
    
    func selectDateInCalendar(date: Date?)
    
    func setYearSelection(hidden: Bool)
    
    func setCalendarSelection(hidden: Bool)
}

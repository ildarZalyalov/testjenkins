//
//  CalendarViewWeekDaysHeader.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 09.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import UIKit
import JTAppleCalendar

/// Заголовок с днями недели в календаре
class CalendarViewWeekDaysHeader: JTAppleCollectionReusableView {}

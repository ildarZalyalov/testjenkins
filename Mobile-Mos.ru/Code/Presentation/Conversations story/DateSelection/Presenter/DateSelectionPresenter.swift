//
//  DateSelectionPresenter.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 05.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class DateSelectionPresenter: DateSelectionViewOutput, DateSelectionInteractorOutput {
    
    weak var view: DateSelectionViewInput!
    var router: DateSelectionRouterInput!
    var interactor: DateSelectionInteractorInput!
    
    var dateComponentsDataSourceFactory: DateSelectionDataSourceFactory!
    var yearSelectionItems = [String : DateComponents]()
    var monthSelectionItems = [String : DateComponents]()
    
    var moduleConfiguration: DateSelectionModuleConfiguration!
    var control: ChatMessageCalendarControl {
        return moduleConfiguration.control
    }
    
    var displayedYear: Int? {
        didSet {
            guard displayedYear != oldValue else { return }
            refreshMonths()
        }
    }
    
    var calendar: Calendar {
        return dateComponentsDataSourceFactory.calendar
    }
    
    var initialDate = Date()
    var minimumDate = Date()
    var maximumDate = Date()
    
    var selectedDate: Date? {
        didSet {
            if oldValue == nil && selectedDate != nil {
                view.displaySubmitButton(with: control.submitTitle)
            }
            else if oldValue != nil && selectedDate == nil {
                view.selectDateInCalendar(date: nil)
                view.hideSubmitButton()
            }
        }
    }
    
    //MARK: - Приватные методы
    
    func initializeDateSelection() {
        
        view.displayTitle(control.title)
        view.setup(with: moduleConfiguration.style)
        
        if control.disableSelectYear {
            view.setYearSelection(hidden: true)
        }
        else {
            
            let yearConfiguration = dateComponentsDataSourceFactory.buildDataSourceConfigurationForYears(with: minimumDate, and: maximumDate)
            yearSelectionItems = yearConfiguration.selectionItems
            
            view.displayYears(with: yearConfiguration.dataSource)
        }
        
        refreshMonths()
        
        if control.disableSelectDays {
            view.setCalendarSelection(hidden: true)
        }
        else {
            view.displayCalendar(with: calendar, minimumDate: minimumDate, maximumDate: maximumDate)
        }
        
        selectDateInPickers(date: initialDate, animated: false)
        view.scrollCalendar(to: initialDate, animated: false)
        
        selectedDate = initialDate
        
        if let date = selectedDate {
            view.selectDateInCalendar(date: date)
        }
    }
    
    func selectDateInPickers(date: Date, animated: Bool) {
        
        let dateComponents = calendar.dateComponents([.year, .month], from: date)
        
        if !control.disableSelectYear, let year = yearSelectionItems.filter({ $0.value.year == dateComponents.year }).first {
            displayedYear = year.value.year
            view.scrollToYear(with: year.key, animated: animated)
        }
        
        if let month = monthSelectionItems.filter({ $0.value.month == dateComponents.month }).first {
            view.scrollToMonth(with: month.key, animated: animated)
        }
    }
    
    func refreshMonths() {
    
        let limit = DateSelectionDataSourceMonthsLimit(displayedYear: displayedYear, minimumDate: minimumDate, maximumDate: maximumDate)
        let monthConfiguration = dateComponentsDataSourceFactory.buildDataSourceConfigurationForMonths(with: limit)
        monthSelectionItems = monthConfiguration.selectionItems
        
        view.displayMonths(with: monthConfiguration.dataSource)
    }
    
    fileprivate func dropSelectedDateIfNeeded(displayedMonthDate: Date) {
        
        guard let selected = selectedDate else { return }
        
        if calendar.compare(displayedMonthDate, to: selected, toGranularity: .month) != .orderedSame {
            selectedDate = nil
        }
    }
    
    //MARK: - DateSelectionViewOutput
    
    func setupInitialState() {
        interactor.computeDateSelectionParameters(for: control, with: calendar)
    }
    
    func configure(with configuration: DateSelectionModuleConfiguration) {
        self.moduleConfiguration = configuration
    }
    
    func isEnabledDate(date: Date) -> Bool {

        let minimumResult = calendar.compare(date, to: minimumDate, toGranularity: .day)
        let maximumResult = calendar.compare(date, to: maximumDate, toGranularity: .day)
        
        return (minimumResult == .orderedSame || minimumResult == .orderedDescending) &&
            (maximumResult == .orderedSame || maximumResult == .orderedAscending)
    }
    
    func didScrollYear(to yearId: String) {
        
        guard let year = yearSelectionItems[yearId] else { return }
        guard year.year != displayedYear else { return }
        guard var yearDate = calendar.date(from: year) else { return }
        
        var scrolledToPreviousYear = false
        if displayedYear != nil && year.year != nil {
            scrolledToPreviousYear = displayedYear! > year.year!
        }
        
        displayedYear = year.year
        
        if scrolledToPreviousYear {
            yearDate = calendar.date(byAdding: .year, value: 1, to: yearDate) ?? yearDate
            yearDate = calendar.date(byAdding: .month, value: -1, to: yearDate) ?? yearDate
        }
        
        // скроллинг года вперед перелистывает месяцы на начало, скроллинг назад - на последний месяц, но, если год в прошлом есть, значит как минимум последний месяц доступен, иначе имеем "дыру"
        // поэтому проверяем только минимальную дату
        if calendar.compare(yearDate, to: minimumDate, toGranularity: .month) == .orderedAscending {
            yearDate = minimumDate
        }
        
        if scrolledToPreviousYear, let lastMonthComponent = calendar.dateComponents([.month], from: yearDate).month, let lastMonthItemId = monthSelectionItems.first(where: { $0.value.month == lastMonthComponent })?.key {
            view.scrollToMonth(with: lastMonthItemId, animated: false)
        }
        
        guard !control.disableSelectDays else {
            selectedDate = yearDate
            return
        }
        
        dropSelectedDateIfNeeded(displayedMonthDate: yearDate)
        view.scrollCalendar(to: yearDate, animated: false)
    }
    
    func didScrollMonth(to monthId: String) {
        
        guard var month = monthSelectionItems[monthId] else { return }
        month.year = displayedYear
        guard let monthDate = calendar.date(from: month) else { return }
        
        if control.disableSelectDays {
            didSelectInCalendar(date: monthDate)
        }
        else {
            view.scrollCalendar(to: monthDate, animated: true)
            dropSelectedDateIfNeeded(displayedMonthDate: monthDate)
        }
    }
    
    func didScrollCalendar(to date: Date) {
        selectDateInPickers(date: date, animated: true)
        dropSelectedDateIfNeeded(displayedMonthDate: date)
    }
    
    func didSelectInCalendar(date: Date) {
        guard date != selectedDate else { return }
        selectedDate = date
    }
    
    func didPressCancel() {
        router.closeModule(completion: nil)
    }
    
    func didPressSubmit() {
        
        guard let date = selectedDate else { return }
        
        let handler = moduleConfiguration.resultHandler
        
        let messageText = interactor.controlValue(for: date, with: control.dateOutputFormat)
        let messageContext = moduleConfiguration.messageContext
        
        let callBackData = interactor.controlCallbackValue(for: date, callbackData: moduleConfiguration.callbackData, format: control.dateOutputFormat)
        let message = moduleConfiguration.message
        let embeddedMessage = moduleConfiguration.embeddedMessage
        
        router.closeModule {
            
            guard handler != nil else { return }
            
            if messageText != nil && messageContext != nil {
                handler!.handleSendMessage(with: messageText!, context: messageContext!)
            }
            
            if callBackData != nil {
                handler!.handleSendCallback(with: callBackData!, for: message, embeddedMessage: embeddedMessage)
            }
        }
    }
    
    //MARK: - DateSelectionInteractorOutput
    
    func didFinishComputingDateSelectionParameters(parameters: DateSelectionParameters) {
        
        initialDate = parameters.initialDate
        minimumDate = parameters.minimumDate
        maximumDate = parameters.maximumDate
        displayedYear = parameters.displayedYear
        
        initializeDateSelection()
    }
}

//
//  LocationSelectionViewController.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 19.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import UIKit
import MapKit

class LocationSelectionViewController: BaseViewController, LocationSelectionViewInput, ConfigurableModuleController, CommandKeyboardViewDelegate, YMKMapCameraListener {
    
    var output: LocationSelectionViewOutput!
    
    var mapLogoSelector: YMKMapLogoTypeSelector!
    var userPinIconConfigurator: UserPinIconConfigurator!
    
    @IBOutlet weak var cancelBarButtonItem: UIBarButtonItem!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var contentStackView: UIStackView!
    @IBOutlet weak var rootStackView: UIStackView!
    
    @IBOutlet weak var mapViewContainer: UIView!
    @IBOutlet weak var yandexLogoView: UIImageView!
    @IBOutlet weak var geoButton: UIButton!
    var mapView: YMKMapView!
    
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var submitContainerView: UIView!
    let submitKeyboard = CommandKeyboardView(frame: CGRect.zero)
    
    @IBOutlet var regularMapPinConstraints: [NSLayoutConstraint]!
    @IBOutlet var movingMapPinConstraints: [NSLayoutConstraint]!
    
    let navigationItemSpaceWidth: CGFloat = 9
    let navigationItemsFontSize: CGFloat = 15
    
    let zoomForLocation: Float = 15
    let zoomForUserLocation: Float = 15
    
    let iconForAvailableGeo = #imageLiteral(resourceName: "findMe")
    let iconForNonAvailableGeo = #imageLiteral(resourceName: "findMe-disabled")
    
    let floatingPinStateChangeAnimationDuration: TimeInterval = 0.3
    
    var isPinFloating = false
    var statusBarStyle = UIStatusBarStyle.default
    
    //MARK: - Методы
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // ограничение языка на #available
        if #available(iOS 10.0, *) {}
        else {
            
            if var rightBarButtonItems = navigationItem.rightBarButtonItems {
                
                let spaceItem = UIBarButtonItem(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
                spaceItem.width = navigationItemSpaceWidth
                
                rightBarButtonItems = [spaceItem] + rightBarButtonItems + [spaceItem]
                navigationItem.rightBarButtonItems = rightBarButtonItems
            }
            
            let falseSpaceOnLeft = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
            navigationItem.leftBarButtonItems = [falseSpaceOnLeft]
        }
        
        let submitButton = CommandKeyboardView.KeyboardButtonViewModel(itemId: UUID().uuidString, title: StringsHelper.chatSendLocationSubmitButtonTitle)
        
        configureMapView()
        refreshMapViewLogo()
        
        submitKeyboard.buttons = [[submitButton]]
        submitKeyboard.delegate = self
        submitKeyboard.translatesAutoresizingMaskIntoConstraints = false
        submitContainerView.insertSubview(submitKeyboard, at: 0)
        submitContainerView.addConstraints(NSLayoutConstraint.edgesConstraints(for: submitKeyboard))
        
        output.setupInitialState()
        
        preferredContentSize = rootStackView.systemLayoutSizeFitting(UILayoutFittingCompressedSize)
    }
    
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return statusBarStyle
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    @IBAction func pressedCancel() {
        output.didPressCancel()
    }
    
    @IBAction func findMeButtonPressed(_ sender: UIButton) {
        refreshGeopositionState()
        output.didPressFindMe()
    }
    
    //MARK: - Приватные методы
    
    func configureMapView() {
        
        mapView = YMKMapView(frame: mapViewContainer.bounds)
        mapView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        mapView.map.mapType = .vectorMap
        mapView.map.isRotateGesturesEnabled = false
        mapView.map.userLocationLayer?.isEnabled = true
        
        userPinIconConfigurator.configureUserPinIcon(with: mapView.map.userLocationLayer)
        
        mapView.map.addCameraListener(with: self)
        mapViewContainer.insertSubview(mapView, at: 0)
        
        mapView.updateMapNightMode()
    }
    
    func refreshMapViewLogo() {
        let logoType = mapLogoSelector.logoType(for: mapView.map)
        yandexLogoView.image = UIImage(named: logoType.rawValue)
    }
    
    func refreshGeopositionState() {
        if let servicesAvailable = output.locationServicesAreAvailable, servicesAvailable {
            geoButton.setImage(iconForAvailableGeo, for: .normal)
        }
        else {
            geoButton.setImage(iconForNonAvailableGeo, for: .normal)
        }
    }
    
    func changeMapPinFloatingState() {
        
        isPinFloating = !isPinFloating
        
        if isPinFloating {
            NSLayoutConstraint.deactivate(regularMapPinConstraints)
            NSLayoutConstraint.activate(movingMapPinConstraints)
        }
        else {
            NSLayoutConstraint.deactivate(movingMapPinConstraints)
            NSLayoutConstraint.activate(regularMapPinConstraints)
        }
        
        UIView.animate(withDuration: floatingPinStateChangeAnimationDuration) {
            self.view.layoutIfNeeded()
        }
    }
    
    //MARK: - ConfigurableModuleController
    
    func configureModule(with object: Any) {
        if let configuration = object as? LocationSelectionModuleConfiguration {
            output.configure(with: configuration)
        }
    }
    
    //MARK: - LocationSelectionViewInput
    
    func setup(with style: ConversationStyle) {
        
        statusBarStyle = style.isStatusBarDark ? .default : .lightContent
        setNeedsStatusBarAppearanceUpdate()
        
        let attributes: [NSAttributedStringKey : Any] = [
            NSAttributedStringKey.font : UIFont(customName: .graphikLCGRegular, size: navigationItemsFontSize) as Any,
            NSAttributedStringKey.foregroundColor : style.controlsTextColor
        ]
        
        cancelBarButtonItem.setTitleTextAttributes(attributes, for: .normal)
        cancelBarButtonItem.setTitleTextAttributes(attributes, for: .highlighted)
        
        submitKeyboard.buttonsTitleColor = style.controlsTextColor
        submitKeyboard.buttonsShadowColor = style.controlsShadowColor
    }
    
    func show(location: Location) {
        mapView.moveCameraPosition(to: location.latitude, longitute: location.longitude, zoom: zoomForLocation)
    }
    
    func showUserLocation(with location: Location) {
        mapView.moveCameraPosition(to: location.latitude, longitute: location.longitude, zoom: zoomForUserLocation)
    }
    
    func update(description: String?) {
        descriptionLabel.text = description ?? StringsHelper.chatLocationOnMapFailedToLoadAddress
    }
    
    func showLoadingStatus() {
        descriptionLabel.text = StringsHelper.chatLocationOnMapLoadingAddress
        loadingIndicator.startAnimating()
    }
    
    func hideLoadingStatus() {
        descriptionLabel.text = StringsHelper.chatLocationOnMapFailedToLoadAddress
        loadingIndicator.stopAnimating()
    }
    
    //MARK: - CommandKeyboardViewDelegate
    
    func didPressCommand(with itemId: String) {
        output.didPressSubmit()
    }
    
    //MARK: YMKMapCameraListener
    func onCameraPositionChanged(with map: YMKMap?, cameraPosition: YMKCameraPosition, cameraUpdateSource: YMKCameraUpdateSource, finished: Bool) {
        
        if cameraUpdateSource == .gestures && finished == isPinFloating {
            changeMapPinFloatingState()
        }
        
        guard finished else { return }
        let target = cameraPosition.target
        
        let location = Location(latitude: target.latitude, longitude: target.longitude)
        output.didMoveCamera(to: location)
    }
}

//
//  LocationSelectionViewInput.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 19.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol LocationSelectionViewInput: class {
    
    func setup(with style: ConversationStyle)
    
    func show(location: Location)
    
    func showUserLocation(with location: Location)
    
    func update(description: String?)
    
    func showLoadingStatus()
    
    func hideLoadingStatus()
}

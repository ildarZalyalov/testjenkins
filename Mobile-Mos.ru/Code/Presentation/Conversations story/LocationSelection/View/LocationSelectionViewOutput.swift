//
//  LocationSelectionViewOutput.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 19.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol LocationSelectionViewOutput: class {
    
    var locationServicesAreAvailable: Bool? { get }
    
    func setupInitialState()
    
    func configure(with configuration: LocationSelectionModuleConfiguration)
    
    func didMoveCamera(to location: Location)
    
    func didPressFindMe()
    
    func didPressCancel()
    
    func didPressSubmit()
}

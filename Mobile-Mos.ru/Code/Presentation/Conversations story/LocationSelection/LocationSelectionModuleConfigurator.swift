//
//  LocationSelectionModuleConfigurator.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 19.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import UIKit

class LocationSelectionModuleConfigurator: BaseModuleConfigurator {
    
    override func configureModule(for view: UIViewController) {
        
        let viewController = view as! LocationSelectionViewController
        let presenter = LocationSelectionPresenter()
        let interactor = LocationSelectionInteractor()
        let router = LocationSelectionRouter()
        
        let logoSelector = YMKMapLogoTypeSelector()
        logoSelector.preferredLanguages = NSLocale.preferredLanguages
        
        let yandexGeocoderService = UIApplication.shared.serviceBuilder.getYandexGeocoderService()
        let alertsFactory = CommonAlertsFactoryImplementation()
        
        let locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        
        let userPinIconConfigurator = UserPinIconConfiguration()
        
        viewController.output = presenter
        viewController.mapLogoSelector = logoSelector
        viewController.userPinIconConfigurator = userPinIconConfigurator
        
        presenter.view = viewController
        presenter.interactor = interactor
        presenter.router = router
        
        interactor.output = presenter
        interactor.yandexGeocoderService = yandexGeocoderService
        interactor.locationManager = locationManager
        
        router.transitionHandler = viewController
        router.alertTransitionHandler = viewController
        router.alertFactory = alertsFactory
    }
}

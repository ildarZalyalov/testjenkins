//
//  LocationSelectionRouter.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 19.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class LocationSelectionRouter: LocationSelectionRouterInput {
    
    weak var transitionHandler: UIViewController!
    weak var alertTransitionHandler: UIViewController!
    
    var alertFactory: CommonAlertsFactory!
    
    let unwindToChatSegueIdentifier = "unwindToChat"
    
    //MARK: - LocationSelectionRouterInput
    
    func showGeolocationAlert() {
        let alert = alertFactory.getLocationServicesNotAuthorizedAlert()
        alertTransitionHandler.present(alert, animated: true, completion: nil)
    }
    
    func showGeolocationErrorAlert() {
        let alert = alertFactory.getFailedToGetUserLocationAlert()
        alertTransitionHandler.present(alert, animated: true, completion: nil)
    }
    
    func closeModule(completion: (() -> ())?) {
        transitionHandler.performSegue(withIdentifier: unwindToChatSegueIdentifier, sender: self)
        transitionHandler.transitionCoordinator?.animate(alongsideTransition: nil, completion: { _ in
            completion?()
        })
    }
}

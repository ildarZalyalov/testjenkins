//
//  LocationSelectionPresenter.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 19.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class LocationSelectionPresenter: LocationSelectionViewOutput, LocationSelectionInteractorOutput {
    
    weak var view: LocationSelectionViewInput!
    var router: LocationSelectionRouterInput!
    var interactor: LocationSelectionInteractorInput!
    
    var moduleConfiguration: LocationSelectionModuleConfiguration!
    
    var userInitiatedFindingUserLocation = false
    
    var selectedLocation: Location?
    var geocodedAddress: String?
    
    //MARK: - LocationSelectionViewOutput
    
    var locationServicesAreAvailable: Bool? {
        return interactor.locationServicesAreAvailable
    }
    
    func setupInitialState() {
        
        view.setup(with: moduleConfiguration.style)
        
        if let servicesAvailable = interactor.locationServicesAreAvailable, servicesAvailable {
            interactor.findUserLocation()
        }
        else {
            view.show(location: moscowLocation)
        }
    }
    
    func configure(with configuration: LocationSelectionModuleConfiguration) {
        self.moduleConfiguration = configuration
    }
    
    func didMoveCamera(to location: Location) {
        
        selectedLocation = location
        
        view.showLoadingStatus()
        interactor.geocode(location: location)
    }
    
    func didPressFindMe() {
        
        if let locationServicesAreAvailable = interactor.locationServicesAreAvailable {
            
            if locationServicesAreAvailable {
                userInitiatedFindingUserLocation = true
                interactor.findUserLocation()
            }
            else {
                router.showGeolocationAlert()
            }
        }
        else {
            interactor.requestLocationServicesAuthorization()
        }
    }
    
    func didPressCancel() {
        router.closeModule(completion: nil)
    }
    
    func didPressSubmit() {
        
        guard let location = selectedLocation else { return }
        
        let handler = moduleConfiguration.resultHandler
        
        let messageText = String()
        let messageContext = moduleConfiguration.messageContext
        let title = StringsHelper.chatLocationOnMapTitle
        let description = geocodedAddress
       
        router.closeModule {
            guard handler != nil else { return }
            handler?.handleSendMessage(with: messageText, location: location, locationTitle: title, locationDescription: description, context: messageContext)
        }
    }
    
    //MARK: - LocationSelectionInteractorOutput
    
    func didFinishFindingUserLocation(with location: Location?) {
        
        if let userLocation = location {
            view.showUserLocation(with: userLocation)
        }
        else if userInitiatedFindingUserLocation {
            router.showGeolocationErrorAlert()
        }
        else {
            view.show(location: moscowLocation)
        }
        
        userInitiatedFindingUserLocation = false
    }
    
    func didFinishGeocodingLocation(with address: String?) {
        view.hideLoadingStatus()
        view.update(description: address)
    }
}

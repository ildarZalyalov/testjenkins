//
//  LocationSelectionModuleConfiguration.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 20.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Конфигурация модуля выбора местоположения
struct LocationSelectionModuleConfiguration {
    
    /// Стиль текущего чата
    var style: ConversationStyle
    
    /// Контекст для отправки сообщения (если есть)
    var messageContext: String?
    
    // Данные для коллбэк запроса (если есть)
    var callbackData: String?
    
    /// Сообщение, с которым связан контрол (если есть)
    var message: ChatMessage?
    
    /// Вложенное сообщение, если сообщение является каруселью
    var embeddedMessage: ChatMessage?
    
    /// Использовать автокомплит на мобильном бекенде
    var shouldUseAutocomplete: Bool = false
    
    /// Обработчик результата работы контрола
    weak var resultHandler: ChatCustomControlResultHandler?
    
    init(style: ConversationStyle,
         messageContext: String? = nil,
         callbackData: String? = nil) {
        self.style = style
        self.messageContext = messageContext
        self.callbackData = callbackData
    }
}

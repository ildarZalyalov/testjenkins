//
//  LocationSelectionInteractorInput.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 19.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol LocationSelectionInteractorInput: class {
    
    var locationServicesAreAvailable: Bool? { get }
    
    var userLocation: Location? { get }
    
    func requestLocationServicesAuthorization()
    
    func findUserLocation()
    
    func geocode(location: Location)
}

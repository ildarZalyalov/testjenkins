//
//  LocationSelectionInteractorOutput.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 19.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol LocationSelectionInteractorOutput: class {
    
    func didFinishFindingUserLocation(with location: Location?)
    
    func didFinishGeocodingLocation(with address: String?)
}

//
//  LocationSelectionInteractor.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 19.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import CoreLocation

class LocationSelectionInteractor: NSObject, LocationSelectionInteractorInput, CLLocationManagerDelegate {
    
    weak var output: LocationSelectionInteractorOutput!

    var yandexGeocoderService: YandexGeocoderService!
    var geocodeRequest: HTTPRequest?
    
    var locationManager: CLLocationManager! {
        didSet { locationManager?.delegate = self }
    }
    
    deinit {
        geocodeRequest?.cancel()
    }
    
    //MARK: - LocationOnMapInteractorInput
    
    var locationServicesAreAvailable: Bool? {
        return CLLocationManager.areLocationServicesAvailable
    }
    
    var userLocation: Location? {
        return Location(with: locationManager.location)
    }
    
    func requestLocationServicesAuthorization() {
        locationManager.requestWhenInUseAuthorization()
    }
    
    func findUserLocation() {
        locationManager.requestLocation()
    }
    
    func geocode(location: Location) {
        
        geocodeRequest?.cancel()
        geocodeRequest = yandexGeocoderService.geocodeCoordinate(location) { [weak self] result in
            
            var address: String? = nil
            
            defer {
                self?.geocodeRequest = nil
                self?.output.didFinishGeocodingLocation(with: address)
            }
            
            switch result {
            case .success(let response):
                address = response.text
            default:
                return
            }
        }
    }

    //MARK: - CLLocationManagerDelegate
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last ?? manager.location
        output.didFinishFindingUserLocation(with: Location(with: location))
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        output.didFinishFindingUserLocation(with: Location(with: manager.location))
    }
}

//
//  LocationOnMapInteractor.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 05.09.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import CoreLocation

class LocationOnMapInteractor: NSObject, LocationOnMapInteractorInput, CLLocationManagerDelegate {
    
    weak var output: LocationOnMapInteractorOutput!
    
    var yandexGeocoderService: YandexGeocoderService!
    var geocodeRequest: HTTPRequest?
    
    var locationManager: CLLocationManager! {
        didSet { locationManager?.delegate = self }
    }
    
    deinit {
        geocodeRequest?.cancel()
    }
    
    //MARK: - LocationOnMapInteractorInput
    
    var locationServicesAreAvailable: Bool? {
        return CLLocationManager.areLocationServicesAvailable
    }
    
    var userLocation: Location? {
        return Location(with: locationManager.location)
    }
    
    func requestLocationServicesAuthorization() {
        locationManager.requestWhenInUseAuthorization()
    }
    
    func findUserLocation() {
        locationManager.requestLocation()
    }
    
    func calculateDistance(to location: Location) {
        
        var distance: Double? = nil
        
        defer {
            output.didFinishCalculatingDistanceToUserLocation(with: distance)
        }
        
        guard let lastKnownUserLocation = userLocation else { return }
        
        let userPlacemark = CLLocation(latitude: lastKnownUserLocation.latitude, longitude: lastKnownUserLocation.longitude)
        let placePlacemark = CLLocation(latitude: location.latitude, longitude: location.longitude)
        
        distance = placePlacemark.distance(from: userPlacemark)
    }
    
    func geocode(location: Location) {
        
        geocodeRequest?.cancel()
        geocodeRequest = yandexGeocoderService.geocodeCoordinate(location) { [weak self] result in
            
            var address: String? = nil
            
            switch result {
            case .success(let response):
                address = response.text
            default:
                return
            }
            
            self?.geocodeRequest = nil
            self?.output.didFinishGeocodingLocation(with: address)
        }
    }
    
    //MARK: - CLLocationManagerDelegate
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last ?? manager.location
        output.didFinishFindingUserLocation(with: Location(with: location))
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        output.didFinishFindingUserLocation(with: Location(with: manager.location))
    }
}

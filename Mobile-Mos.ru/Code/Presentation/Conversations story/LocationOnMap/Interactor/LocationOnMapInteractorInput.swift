//
//  LocationOnMapInteractorInput.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 05.09.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol LocationOnMapInteractorInput: class {
    
    var locationServicesAreAvailable: Bool? { get }
    
    var userLocation: Location? { get }
    
    func requestLocationServicesAuthorization()
    
    func findUserLocation()
    
    func calculateDistance(to location: Location)
    
    func geocode(location: Location)
}

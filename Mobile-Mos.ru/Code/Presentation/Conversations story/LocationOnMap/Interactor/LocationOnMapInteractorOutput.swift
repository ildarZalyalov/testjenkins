//
//  LocationOnMapInteractorOutput.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 05.09.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol LocationOnMapInteractorOutput: class {
    
    func didFinishFindingUserLocation(with location: Location?)
    
    func didFinishCalculatingDistanceToUserLocation(with distance: Double?)
    
    func didFinishGeocodingLocation(with address: String?)
}

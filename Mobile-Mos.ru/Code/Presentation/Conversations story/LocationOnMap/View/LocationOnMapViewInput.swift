//
//  LocationOnMapViewInput.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 05.09.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol LocationOnMapViewInput: class {
    
    func setup(with style: ConversationStyle)
    
    func display(mapObject: ChatMessageMapObject)
    
    func displayDistance(distanceString: String)
    
    func showLoadingStatus()
    
    func hideLoadingStatus()
    
    func updateMapObjectInfo(with description: String?)
    
    func showUserLocation(with location: Location)
}

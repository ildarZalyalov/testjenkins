//
//  LocationOnMapViewOutput.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 05.09.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol LocationOnMapViewOutput: class {
    
    var locationServicesAreAvailable: Bool? { get }
    
    func setupInitialState()
    
    func configure(with moduleConfiguration: LocationOnMapModuleConfiguration)
    
    func didPressShare()
    
    func didPressFindMe()
    
    func didPressCreateRoute()
}

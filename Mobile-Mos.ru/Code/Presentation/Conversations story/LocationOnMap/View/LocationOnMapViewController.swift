//
//  LocationOnMapViewController.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 05.09.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import UIKit
import CoreLocation

class LocationOnMapViewController: BaseViewController, LocationOnMapViewInput, NavigationBarCustomizingController, ConfigurableModuleController {
    
    var output: LocationOnMapViewOutput!
    
    var mapLogoSelector: YMKMapLogoTypeSelector!
    var mapPinIconConfigurator: MapPinIconConfiguratorProtocol!
    var userPinIconConfigurator: UserPinIconConfigurator!
    var mapView: YMKMapView!
    
    @IBOutlet weak var mapViewContainer: UIView!
    @IBOutlet weak var yandexLogoView: UIImageView!
    @IBOutlet weak var geoButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var routeButton: UIButton!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var titleLabelHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var descriptionLabelHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var routeButtonWidthConstraint: NSLayoutConstraint!
    
    @IBOutlet var noInfoConstraints: [NSLayoutConstraint]!
    @IBOutlet var onlyTitleConstraints: [NSLayoutConstraint]!
    @IBOutlet var onlyDescriptionConstraints: [NSLayoutConstraint]!
    @IBOutlet var fullInfoConstraints: [NSLayoutConstraint]!
    
    let zoomForLocation: Float = 15
    let zoomForUserLocation: Float = 15
    let iconScaleForPlacemark = 1.0
    let maxCompactHeightForTitle: CGFloat = 48
    let maxCompactHeightForDescription: CGFloat = 36
    let routeButtonCompactWidth: CGFloat = 124.0
    let titleMinimumLineHeight: CGFloat = 24
    let titleBaselineOffset: Double = 2
    let descriptionLineSpacing: CGFloat = 4
    
    let iconForAvailableGeo = #imageLiteral(resourceName: "findMe")
    let iconForNonAvailableGeo = #imageLiteral(resourceName: "findMe-disabled")
    let iconForPlacemarkOnMap = #imageLiteral(resourceName: "selectedPinIcon")
    
    var descriptionText: String?
    var statusBarStyle = UIStatusBarStyle.default
    
    //MARK: - Методы
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        configureMapView()
        refreshMapViewLogo()
        
        if UIDevice.current.hasCompactScreen {
            
            titleLabelHeightConstraint.constant = maxCompactHeightForTitle
            descriptionLabelHeightConstraint.constant = maxCompactHeightForDescription
            routeButtonWidthConstraint.constant = routeButtonCompactWidth
            
            let title = StringsHelper.chatLocationOnMapCompactRouteButtonTitle
            routeButton.setTitle(title, for: .normal)
        }
        
        output.setupInitialState()
    }
    
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return statusBarStyle
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    //MARK: - Приватные методы
    
    func configureMapView() {
        
        mapView = YMKMapView(frame: mapViewContainer.bounds)
        mapView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        mapView.map.mapType = .vectorMap
        mapView.map.isRotateGesturesEnabled = false
        mapView.map.userLocationLayer?.isEnabled = true
        
        userPinIconConfigurator.configureUserPinIcon(with: mapView.map.userLocationLayer)
        
        mapViewContainer.insertSubview(mapView, at: 0)
        
        mapView.updateMapNightMode()
    }
    
    func refreshMapViewLogo() {
        let logoType = mapLogoSelector.logoType(for: mapView.map)
        yandexLogoView.image = UIImage(named: logoType.rawValue)
    }
    
    func refreshGeopositionState() {
        if let servicesAvailable = output.locationServicesAreAvailable, servicesAvailable {
            geoButton.setImage(iconForAvailableGeo, for: .normal)
        }
        else {
            geoButton.setImage(iconForNonAvailableGeo, for: .normal)
        }
    }
    
    func setTitle(title: String) {

        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .left
        paragraphStyle.lineBreakMode = .byTruncatingTail
        paragraphStyle.minimumLineHeight = titleMinimumLineHeight
        
        let attributes: [NSAttributedStringKey : Any] = [
            NSAttributedStringKey.foregroundColor : titleLabel.textColor,
            NSAttributedStringKey.font: titleLabel.font,
            NSAttributedStringKey.paragraphStyle: paragraphStyle,
            NSAttributedStringKey.baselineOffset: NSNumber(value: titleBaselineOffset)
        ]
        
        titleLabel.attributedText = NSAttributedString(string: title, attributes: attributes)
    }
    
    func setDescription(description: String) {
        
        descriptionText = description
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .left
        paragraphStyle.lineSpacing = descriptionLineSpacing
        paragraphStyle.lineBreakMode = .byTruncatingTail
        
        let attributes: [NSAttributedStringKey : Any] = [
            NSAttributedStringKey.foregroundColor : descriptionLabel.textColor,
            NSAttributedStringKey.font: descriptionLabel.font,
            NSAttributedStringKey.paragraphStyle: paragraphStyle
        ]
        
        descriptionLabel.attributedText = NSAttributedString(string: description, attributes: attributes)
    }
    
    fileprivate func updateConstraintsForContent() {
        
        titleLabel.isHidden = titleLabel.text?.isEmpty ?? true
        descriptionLabel.isHidden = descriptionLabel.text?.isEmpty ?? true
        
        let allConstraintsCombinations: [[NSLayoutConstraint]] = [
            noInfoConstraints,
            onlyTitleConstraints,
            onlyDescriptionConstraints,
            fullInfoConstraints
        ]
        
        var activeConstraints: [NSLayoutConstraint] = fullInfoConstraints
        
        if titleLabel.isHidden && descriptionLabel.isHidden {
            activeConstraints = noInfoConstraints
        }
        else if titleLabel.isHidden {
            activeConstraints = onlyDescriptionConstraints
        }
        else if descriptionLabel.isHidden {
            activeConstraints = onlyTitleConstraints
        }
        
        for constraints in allConstraintsCombinations {
            guard constraints != activeConstraints else { continue }
            NSLayoutConstraint.deactivate(constraints)
        }
        
        NSLayoutConstraint.activate(activeConstraints)
    }
    
    //MARK: - Actions
    
    @IBAction func shareButtonPressed() {
        output.didPressShare()
    }
    
    @IBAction func findMeButtonPressed(_ sender: UIButton) {
        refreshGeopositionState()
        output.didPressFindMe()
    }
    
    @IBAction func plusButtonPressed(_ sender: Any) {
        mapView.moveCameraPosition(to: mapView.map.cameraPosition.target.latitude, longitute: mapView.map.cameraPosition.target.longitude, zoom: mapView.map.cameraPosition.zoom + 1)
    }
    
    @IBAction func minusButtonPressed(_ sender: Any) {
        mapView.moveCameraPosition(to: mapView.map.cameraPosition.target.latitude, longitute: mapView.map.cameraPosition.target.longitude, zoom: mapView.map.cameraPosition.zoom - 1)
    }
    
    @IBAction func createRoutePressed() {
        output.didPressCreateRoute()
    }
    
    //MARK: - ConfigurableModuleController
    
    func configureModule(with object: Any) {
        if let moduleConfiguration = object as? LocationOnMapModuleConfiguration {
            output.configure(with: moduleConfiguration)
        }
    }
    
    //MARK: - NavigationBarCustomizingController
    
    var navigationBarState: NavigationBarState? {
        
        let conversationStyle = mapConversation.style
        var state = NavigationBarState()
        
        state.isHidden = false
        state.isShadowHidden = false
        state.isTranslucent = false
        state.barTintColor = conversationStyle.headerColor
        state.backgroundState = NavigationBarBackgroundState.regular
        state.tintColor = conversationStyle.backButtonColor
        state.titleTextAttributes = [
            NSAttributedStringKey.font : UIFont(customName: .graphikLCGBold, size: 15) as Any,
            NSAttributedStringKey.foregroundColor : conversationStyle.titleColor
        ]
        
        return state
    }
    
    //MARK: - LocationOnMapViewInput
    
    func setup(with style: ConversationStyle) {
        statusBarStyle = style.isStatusBarDark ? .default : .lightContent
        setNeedsStatusBarAppearanceUpdate()
    }
    
    func display(mapObject: ChatMessageMapObject) {
        
        mapView.moveCameraPosition(to: mapObject.location.latitude,
                                   longitute: mapObject.location.longitude,
                                   zoom: zoomForLocation,
                                   animated: false)
        
        let title = mapObject.title ?? StringsHelper.chatLocationOnMapTitle
        setTitle(title: title)
        
        if let description = mapObject.description {
            setDescription(description: description)
        }
        
        updateConstraintsForContent()
        
        let point = YMKPoint(latitude: mapObject.location.latitude, longitude: mapObject.location.longitude)
        
        let placemarkMapObject = mapView.addMapObject(with: point, andObjectData: mapObject)
        let iconStyle = YMKIconStyle(scale: iconScaleForPlacemark)
        
        mapPinIconConfigurator.configure(with: placemarkMapObject, iconStyle: iconStyle, icon: iconForPlacemarkOnMap)
    }
    
    func displayDistance(distanceString: String) {
        distanceLabel.text = distanceString + StringsHelper.chatLocationOnMapDistanceStringSuffix
    }
    
    func showLoadingStatus() {
        
        descriptionLabel.text = StringsHelper.chatLocationOnMapLoadingAddress
        loadingIndicator.startAnimating()
        
        view.setNeedsLayout()
    }
    
    func hideLoadingStatus() {
        
        descriptionLabel.text = descriptionText
        loadingIndicator.stopAnimating()
        
        view.setNeedsLayout()
    }
    
    func updateMapObjectInfo(with description: String?) {
        
        if description != nil {
            setDescription(description: description!)
        }
        
        updateConstraintsForContent()
    }
    
    func showUserLocation(with location: Location) {
        mapView.moveCameraPosition(to: location.latitude, longitute: location.longitude, zoom: zoomForUserLocation)
    }
}

//
//  LocationOnMapRouterInput.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 05.09.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol LocationOnMapRouterInput: class {

    /// Отобразить алерт геолокации
    func showGeolocationAlert()
    
    func showGeolocationErrorAlert()
    
    func showSharing(with data: [Any])
    
    func showCreateRouteModule(for startLocation: Location, and endLocation: Location)
}

//
//  LocationOnMapRouter.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 05.09.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class LocationOnMapRouter: LocationOnMapRouterInput {
    
    weak var transitionHandler: UIViewController!
    weak var alertTransitionHandler: UIViewController!
    
    var alertFactory: CommonAlertsFactory!
    
    //MARK: - LocationOnMapRouterInput
    
    func showGeolocationAlert() {
        let alert = alertFactory.getLocationServicesNotAuthorizedAlert()
        alertTransitionHandler.present(alert, animated: true, completion: nil)
    }
    
    func showGeolocationErrorAlert() {
        let alert = alertFactory.getFailedToGetUserLocationAlert()
        alertTransitionHandler.present(alert, animated: true, completion: nil)
    }
    
    func showSharing(with data: [Any]) {
        let activityController = UIActivityViewController(activityItems: data, applicationActivities: nil)
        transitionHandler.present(activityController, animated: true, completion: nil)
    }
    
    func showCreateRouteModule(for startLocation: Location, and endLocation: Location) {
        let alert = alertFactory.getCreateRouteActionSheet(for: startLocation, and: endLocation)
        alertTransitionHandler.present(alert, animated: true, completion: nil)
    }
}

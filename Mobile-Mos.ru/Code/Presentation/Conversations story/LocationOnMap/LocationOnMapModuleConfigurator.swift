//
//  LocationOnMapModuleConfigurator.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 05.09.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import UIKit

class LocationOnMapModuleConfigurator: BaseModuleConfigurator {
    
    override func configureModule(for view: UIViewController) {
        
        let viewController = view as! LocationOnMapViewController
        let presenter = LocationOnMapPresenter()
        let interactor = LocationOnMapInteractor()
        let router = LocationOnMapRouter()
        
        let logoSelector = YMKMapLogoTypeSelector()
        logoSelector.preferredLanguages = NSLocale.preferredLanguages
        
        let distanceStringBuilder = DistanceStringBuilderImplementation()
        let yandexGeocoderService = UIApplication.shared.serviceBuilder.getYandexGeocoderService()
        
        let locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        
        let mapPinConfigurator = MapPinIconConfigurator()
        let userPinIconConfigurator = UserPinIconConfiguration()
        let alertsFactory = CommonAlertsFactoryImplementation()
        
        viewController.output = presenter
        viewController.mapLogoSelector = logoSelector
        viewController.mapPinIconConfigurator = mapPinConfigurator
        viewController.userPinIconConfigurator = userPinIconConfigurator
        
        presenter.view = viewController
        presenter.interactor = interactor
        presenter.router = router
        presenter.distanceStringsBuilder = distanceStringBuilder
        
        interactor.output = presenter
        interactor.yandexGeocoderService = yandexGeocoderService
        interactor.locationManager = locationManager
        
        router.transitionHandler = viewController
        router.alertTransitionHandler = viewController
        router.alertFactory = alertsFactory
    }
}

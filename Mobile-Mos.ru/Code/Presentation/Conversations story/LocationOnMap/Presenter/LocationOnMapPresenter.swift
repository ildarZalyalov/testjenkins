//
//  LocationOnMapPresenter.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 05.09.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class LocationOnMapPresenter: LocationOnMapViewOutput, LocationOnMapInteractorOutput {
    
    weak var view: LocationOnMapViewInput!
    var router: LocationOnMapRouterInput!
    var interactor: LocationOnMapInteractorInput!
    
    var distanceStringsBuilder: DistanceStringBuilder!
    
    var moduleConfiguration: LocationOnMapModuleConfiguration!
    var mapObject: ChatMessageMapObject {
        return moduleConfiguration.mapObject
    }
    
    //MARK: - Приватные методы
    
    func convertDistanceStringsToDistanceDescription(strings: FormattedDistanceStrings) -> String {
        return strings.distanceString + " " + strings.distanceUnitString
    }
    
    //MARK: - LocationOnMapViewOutput
    
    var locationServicesAreAvailable: Bool? {
        return interactor.locationServicesAreAvailable
    }
    
    func setupInitialState() {
        
        if let style = moduleConfiguration.conversationStyle {
            view.setup(with: style)
        }
        
        let distanceStrings = distanceStringsBuilder.unknownDistanceStrings
        let distanceDescription = convertDistanceStringsToDistanceDescription(strings: distanceStrings)
        
        view.display(mapObject: mapObject)
        view.displayDistance(distanceString: distanceDescription)
        
        interactor.calculateDistance(to: mapObject.location)
        
        if mapObject.description == nil {
            view.showLoadingStatus()
            interactor.geocode(location: mapObject.location)
        }
    }
    
    func configure(with moduleConfiguration: LocationOnMapModuleConfiguration) {
        self.moduleConfiguration = moduleConfiguration
    }
    
    func didPressShare() {
        
        var sharedInfo = [Any]()
        
        if let title = mapObject.title {
            sharedInfo.append(title)
        }
        
        if let description = mapObject.description {
            sharedInfo.append(description)
        }
        
        let coordinateString = "\(mapObject.location.latitude), \(mapObject.location.longitude)"
        sharedInfo.append(coordinateString)
        
        router.showSharing(with: sharedInfo)
    }
    
    func didPressFindMe() {
        
        if let locationServicesAreAvailable = interactor.locationServicesAreAvailable {
            
            if locationServicesAreAvailable {
                interactor.findUserLocation()
            }
            else {
                router.showGeolocationAlert()
            }
        }
        else {
            interactor.requestLocationServicesAuthorization()
        }
    }
    
    func didPressCreateRoute() {
        if let userLocation = interactor.userLocation {
            router.showCreateRouteModule(for: userLocation, and: mapObject.location)
        }
        else {
            router.showGeolocationErrorAlert()
        }
    }
    
    //MARK: - LocationOnMapInteractorOutput
    
    func didFinishFindingUserLocation(with location: Location?) {
        if let userLocation = location {
            view.showUserLocation(with: userLocation)
        }
        else {
            router.showGeolocationErrorAlert()
        }
    }
    
    func didFinishCalculatingDistanceToUserLocation(with distance: Double?) {
        
        var distanceStrings = distanceStringsBuilder.unknownDistanceStrings
        
        if let distanceValue = distance,
           let strings = distanceStringsBuilder.distanceStrings(forDistance: distanceValue) {
            distanceStrings = strings
        }
        
        let distanceDescription = convertDistanceStringsToDistanceDescription(strings: distanceStrings)
        view.displayDistance(distanceString: distanceDescription)
    }
    
    func didFinishGeocodingLocation(with address: String?) {
        view.hideLoadingStatus()
        view.updateMapObjectInfo(with: address)
    }
}

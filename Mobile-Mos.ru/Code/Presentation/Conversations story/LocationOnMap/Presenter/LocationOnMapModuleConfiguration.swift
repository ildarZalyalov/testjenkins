//
//  LocationOnMapModuleConfiguration.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 04.06.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Конфигурация модуля отображения точки на карте
struct LocationOnMapModuleConfiguration {
    
    /// Информация о точке на карте
    let mapObject: ChatMessageMapObject
    
    /// Стиль чата (если есть)
    var conversationStyle: ConversationStyle?
    
    init(mapObject: ChatMessageMapObject) {
        self.mapObject = mapObject
    }
}

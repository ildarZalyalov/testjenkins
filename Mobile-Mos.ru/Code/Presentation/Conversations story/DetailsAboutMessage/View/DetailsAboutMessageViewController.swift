//
//  DetailsAboutMessageViewController.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 03.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import UIKit
import WebKit

class DetailsAboutMessageViewController: BaseViewController, DetailsAboutMessageViewInput, ConfigurableModuleController, CommandKeyboardViewDelegate, UIScrollViewDelegate, ParallaxScrollHeaderControllerDelegate, WKNavigationDelegate {
    
    var output: DetailsAboutMessageViewOutput!
    
    var headerController: ParallaxScrollHeaderController!
    var imageBlurBuilder: UIImageBlurEffectBuilder!
    
    @IBOutlet weak var cancelBarButtonItem: UIBarButtonItem!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var webViewContainer: UIView!
    @IBOutlet weak var buttonsKeyboardContainer: UIView!
    
    let ios9NavigationItemSpaceWidth: CGFloat = 9
    let navigationItemsFontSize: CGFloat = 15
    let mapPreviewHeight: CGFloat = 120
    
    let overscrollBeginOffset: CGFloat = 0.4
    let overscrollSpeedMultiplier: CGFloat = 3
    let overscrollProgressInterval: ClosedRange<CGFloat> = 0 ... 1
    
    let mapPreviewBlurRadius: CGFloat = 30
    let mapPreviewSaturationDelta: CGFloat = 1.3
    
    let buttonsKeyboard = CommandKeyboardView(frame: CGRect.zero)
    let webView: WKWebView = {
        
        let configuration = WKWebViewConfiguration()
        configuration.suppressesIncrementalRendering = true
        configuration.selectionGranularity = .character
       
        return WKWebView(frame: CGRect.zero, configuration: configuration)
    }()
    
    var mapPreviewView: UIImageView?
    var mapPreviewBlurredView: UIImageView?
    
    lazy var contentErrorView: ContentObtainErrorView = {
        
        let errorView = ContentObtainErrorView.loadNib()
        errorView.tintColor = progressView.tintColor
        errorView.add(retryAction: #selector(pressedRetryObtainContent), with: self)
        errorView.isHidden = true
        errorView.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(errorView)
        view.addConstraints(NSLayoutConstraint.centerConstraints(for: errorView))
        
        return errorView
    }()
    
    var webViewScrollOriginalDelegate: UIScrollViewDelegate?
    var isObservingWebViewProgress = false
    var webViewProgressObservationContext = 0
    
    var statusBarStyle = UIStatusBarStyle.default
    var isInitialized = false
    
    //MARK: - Методы
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        add(view: webView, to: webViewContainer)
        add(view: buttonsKeyboard, to: buttonsKeyboardContainer)
        
        buttonsKeyboard.delegate = self
        buttonsKeyboard.isHidden = true
        
        // чтобы выключить зум при попытке его начать
        webView.scrollView.delegate = self
        webView.navigationDelegate = self
        if #available(iOS 11.0, *) {
            webView.scrollView.contentInsetAdjustmentBehavior = .never
        }
        
        // ограничение языка на #available
        if #available(iOS 10.0, *) {}
        else if var rightBarButtonItems = navigationItem.rightBarButtonItems {
            
            let spaceItem = UIBarButtonItem(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
            spaceItem.width = ios9NavigationItemSpaceWidth
            
            rightBarButtonItems = [spaceItem] + rightBarButtonItems
            navigationItem.rightBarButtonItems = rightBarButtonItems
        }
        
        output.setupInitialState()
    }
    
    deinit {
        stopWebViewProgressObserving()
        headerController.scrollView = nil
        webView.scrollView.delegate = webViewScrollOriginalDelegate
    }
    
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return statusBarStyle
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        guard !isInitialized else { return }
        isInitialized = true
        
        refreshWebViewInsets()
        displayMapPreviewIfNeeded()
    }
    
    override func viewSafeAreaInsetsDidChange() {
        refreshWebViewInsets()
    }
    
    @IBAction func pressedCancel() {
        output.didPressCancel()
    }
    
    @objc
    func tappedOnMapPreview(recognizer: UITapGestureRecognizer) {
        output.didPressMapPreview()
    }
    
    @objc
    func pressedRetryObtainContent() {
        output.didPressRetryObtainContent()
    }
    
    //MARK: - Приватные методы
    
    func add(view: UIView, to container: UIView) {
        
        container.addSubview(view)
        view.translatesAutoresizingMaskIntoConstraints = false
        
        let viewConstraints = NSLayoutConstraint.edgesConstraints(for: view)
        container.addConstraints(viewConstraints)
    }
    
    func refreshWebViewInsets() {
        
        let topInset = navigationController?.navigationBar.height ?? 0
        var bottomInset = buttonsKeyboard.buttons.count > 0 ? buttonsKeyboardContainer.height : 0
        
        if #available(iOS 11.0, *), bottomInset == 0 {
            bottomInset += view.safeAreaInsets.bottom
        }
        
        let webViewInsets = UIEdgeInsetsMake(topInset, 0, bottomInset, 0)
        
        webView.scrollView.contentInset = webViewInsets
        webView.scrollView.scrollIndicatorInsets = webViewInsets
    }
    
    func displayMapPreviewIfNeeded() {
        
        guard isInitialized else { return }
        guard !webViewContainer.isHidden else { return }
        guard headerController.scrollView == nil else { return }
        guard let mapView = mapPreviewView else { return }
        
        headerController.scrollView = webView.scrollView
        headerController.headerView = mapPreviewView
        
        let size = CGSize(width: view.width, height: mapPreviewHeight)
        let previewUrl = output.obtainUrlForMapPreview(with: size)
        
        mapView.loadAndDisplayImage(from: previewUrl, placeholder: nil, with: nil) { [weak self] result in
            
            guard let strongSelf = self else { return }
            
            switch result {
            case .success(let image, _, _):
                
                let blurRadius = strongSelf.mapPreviewBlurRadius
                let blurSaturation = strongSelf.mapPreviewSaturationDelta
                let blurredImage = strongSelf.imageBlurBuilder.imageByApplyingBlur(to: image,
                                                                        withRadius: blurRadius,
                                                                        tintColor: nil,
                                                                        saturationDeltaFactor: blurSaturation,
                                                                        maskImage: nil)
                
                strongSelf.mapPreviewBlurredView?.image = blurredImage
                
            default:
                return
            }
        }
    }
    
    func startWebViewProgressObserving() {
        
        guard !isObservingWebViewProgress else { return }
        isObservingWebViewProgress = true
        
        let keyPath = #keyPath(WKWebView.estimatedProgress)
        webView.addObserver(self, forKeyPath: keyPath, options: .new, context: &webViewProgressObservationContext)
    }
    
    func stopWebViewProgressObserving() {
        
        guard isObservingWebViewProgress else { return }
        isObservingWebViewProgress = false
        
        let keyPath = #keyPath(WKWebView.estimatedProgress)
        webView.removeObserver(self, forKeyPath: keyPath, context: &webViewProgressObservationContext)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        guard let change = change else { return }
        guard context == &webViewProgressObservationContext else {
            
            super.observeValue(forKeyPath: keyPath,
                               of: object,
                               change: change,
                               context: context)
            return
        }
        
        guard keyPath == #keyPath(WKWebView.estimatedProgress) else { return }
        guard let progress = (change[.newKey] as AnyObject).floatValue else { return }
        
        progressView.progress = progress
    }
    
    //MARK: - ConfigurableModuleController
    
    func configureModule(with object: Any) {
        if let configuration = object as? DetailsAboutMessageModuleConfiguration {
            output.configure(with: configuration)
        }
    }
    
    //MARK: - DetailsAboutMessageViewInput
    
    func setup(with style: ConversationStyle) {

        statusBarStyle = style.isStatusBarDark ? .default : .lightContent
        setNeedsStatusBarAppearanceUpdate()
        
        let attributes: [NSAttributedStringKey : Any] = [
            NSAttributedStringKey.font : UIFont(customName: .graphikLCGRegular, size: navigationItemsFontSize) as Any,
            NSAttributedStringKey.foregroundColor : style.controlsTextColor
        ]
        
        cancelBarButtonItem.setTitleTextAttributes(attributes, for: .normal)
        cancelBarButtonItem.setTitleTextAttributes(attributes, for: .highlighted)
        
        buttonsKeyboard.buttonsTitleColor = style.controlsTextColor
        buttonsKeyboard.buttonsShadowColor = style.controlsShadowColor
        
        progressView.tintColor = style.controlsTextColor
    }
    
    func setupMapPreview() {
        
        headerController.defaultHeight = mapPreviewHeight
        headerController.mode = .fill
        headerController.delegate = self
        
        mapPreviewView = UIImageView()
        mapPreviewView?.contentMode = .scaleAspectFill
        mapPreviewView?.clipsToBounds = true
        mapPreviewView?.isUserInteractionEnabled = true
        mapPreviewView?.backgroundColor = UIColor.lightGray
        
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(tappedOnMapPreview(recognizer:)))
        mapPreviewView?.addGestureRecognizer(tapRecognizer)
        
        mapPreviewBlurredView = UIImageView()
        mapPreviewBlurredView?.contentMode = .scaleAspectFill
        mapPreviewBlurredView?.clipsToBounds = true
        mapPreviewBlurredView?.alpha = 0
        mapPreviewBlurredView?.isUserInteractionEnabled = false
        mapPreviewBlurredView?.translatesAutoresizingMaskIntoConstraints = false
        
        mapPreviewView?.addSubview(mapPreviewBlurredView!)
        mapPreviewView?.addConstraints(NSLayoutConstraint.edgesConstraints(for: mapPreviewBlurredView!))
        
        let pinView: UIImageView = UIImageView(image: #imageLiteral(resourceName: "messageLocationIcon"))
        pinView.translatesAutoresizingMaskIntoConstraints = false
        mapPreviewView?.addSubview(pinView)
        
        let pinOffset = CGPoint(x: 0, y: -pinView.height / 2)
        mapPreviewView?.addConstraints(NSLayoutConstraint.centerConstraints(for: pinView, with: pinOffset))
    }
    
    func displayTitle(_ title: String) {
        navigationItem.title = title
    }
    
    func displayTemplate(_ htmlString: String) {
        webView.loadHTMLString(htmlString, baseURL: nil)
    }
    
    func displayButtons(with dataSource: [CommandKeyboardView.KeyboardButtonViewModel]) {
        
        buttonsKeyboard.buttons = dataSource.map { [$0] }
        buttonsKeyboard.isHidden = false
        
        view.setNeedsLayout()
        view.layoutIfNeeded()
    }
    
    func showLoadingStatus() {
        contentErrorView.isHidden = true
        webViewContainer.isHidden = true
        buttonsKeyboardContainer.isHidden = true
    }
    
    func hideLoadingStatus() {
        contentErrorView.isHidden = true
        webViewContainer.isHidden = false
        buttonsKeyboardContainer.isHidden = false
    }
    
    func showContentLoadError() {
        contentErrorView.isHidden = false
    }
    
    //MARK: - CommandKeyboardViewDelegate
    
    func didPressCommand(with itemId: String) {
        output.didPressButton(with: itemId)
    }
    
    //MARK: - UIScrollViewDelegate
    
    func scrollViewWillBeginZooming(_ scrollView: UIScrollView, with view: UIView?) {
        scrollView.pinchGestureRecognizer?.isEnabled = false
    }
    
    //MARK: - WKNavigationDelegate
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        
        progressView.progress = 0
        progressView.isHidden = false
        
        startWebViewProgressObserving()
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        progressView.isHidden = true
        showContentLoadError()
    }
    
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        self.webView(webView, didFail: navigation, withError: error)
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        progressView.isHidden = true
        hideLoadingStatus()
        displayMapPreviewIfNeeded()
    }
    
    //MARK: - ParallaxScrollHeaderControllerDelegate
    
    func parallaxHeaderDidScroll(_ headerController: ParallaxScrollHeaderController) {
        
        var progress = headerController.progress - headerController.progressDefaultValue
        progress -= overscrollBeginOffset
        progress *= overscrollSpeedMultiplier
        progress = progress.clamped(to: overscrollProgressInterval)
        
        mapPreviewBlurredView?.alpha = progress
    }
}

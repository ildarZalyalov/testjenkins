//
//  DetailsAboutMessageViewInput.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 03.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol DetailsAboutMessageViewInput: class {
    
    func setup(with style: ConversationStyle)
    
    func setupMapPreview()
    
    func displayTitle(_ title: String)
    
    func displayTemplate(_ htmlString: String)
    
    func displayButtons(with dataSource: [CommandKeyboardView.KeyboardButtonViewModel])
    
    func showLoadingStatus()
    
    func hideLoadingStatus()
    
    func showContentLoadError()
}

//
//  DetailsAboutMessageViewOutput.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 03.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol DetailsAboutMessageViewOutput: class {
    
    func setupInitialState()
    
    func configure(with configuration: DetailsAboutMessageModuleConfiguration)
    
    func obtainUrlForMapPreview(with size: CGSize) -> URL?
    
    func didPressMapPreview()
    
    func didPressButton(with buttonId: String)
    
    func didPressCancel()
    
    func didPressRetryObtainContent()
}

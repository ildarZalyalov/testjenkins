//
//  DetailsAboutMessageInteractorOutput.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 03.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol DetailsAboutMessageInteractorOutput: class {
    
    func didFinishLoading(template: String?)
}

//
//  DetailsAboutMessageInteractorInput.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 03.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol DetailsAboutMessageInteractorInput: class {
    
    func loadTemplate(with url: URL, valuesJsonString: String)
    
    func obtainUrlForMapPreview(with location: Location, size: CGSize) -> URL
}

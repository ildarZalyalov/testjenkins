//
//  DetailsAboutMessageInteractor.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 03.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class DetailsAboutMessageInteractor: DetailsAboutMessageInteractorInput {
    
    weak var output: DetailsAboutMessageInteractorOutput!
    
    var loadTemplateHandler: HTMLTemplateProcessor!
    var mapPreviewUrlBuilder: YandexMapPreviewUrlBuilder!
    var analyticsManager: AnalyticsManager!
    
    let mapPreviewLocationSpan: YandexMapPreviewUrlBuilder.CoordinateSpan = (0.001, 0.001)
    
    //MARK: - DetailsAboutMessageInteractorInput
    
    func loadTemplate(with url: URL, valuesJsonString: String) {
        
        loadTemplateHandler.processTemplate(at: url, with: valuesJsonString) { [weak self] resultHTML in
          
            self?.output.didFinishLoading(template: resultHTML)
            
            guard let statsData = self?.analyticsManager.currentStatsData else { return }
            
            let errorText = resultHTML != nil ? AnalyticsConstants.emptyValueName : AnalyticsConstants.notLoadedErrorValue
            self?.analyticsManager.sendSomeEvent(with: statsData.eventName, eventJson: statsData.eventJson, errorText: errorText)
            self?.analyticsManager.setStatsData(with: nil)
        }
    }
    
    func obtainUrlForMapPreview(with location: Location, size: CGSize) -> URL {
        return mapPreviewUrlBuilder.urlForMapPreview(with: location, span: mapPreviewLocationSpan, size: size)
    }
}

//
//  DetailsAboutMessageRouter.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 03.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class DetailsAboutMessageRouter: DetailsAboutMessageRouterInput {
    
    weak var transitionHandler: UIViewController!
    weak var toOtherModulesTransitionHandler: DataTransferModuleController!
    
    let unwindToChatSegueIdentifier = "unwindToChat"
    let showLocationOnMapSegueIdentifier = "showLocationOnMapFromDetails"
    
    //MARK: - DetailsAboutMessageRouterInput
    
    func showMapModule(for mapObject: ChatMessageMapObject, with style: ConversationStyle) {
        
        toOtherModulesTransitionHandler.performSegue(with: showLocationOnMapSegueIdentifier, sender: nil) { controller in
            
            var configuration = LocationOnMapModuleConfiguration(mapObject: mapObject)
            configuration.conversationStyle = style
            
            controller.configureModule(with: configuration)
        }
    }
    
    func closeModule(completion: (() -> ())?) {
        transitionHandler.performSegue(withIdentifier: unwindToChatSegueIdentifier, sender: self)
        transitionHandler.transitionCoordinator?.animate(alongsideTransition: nil, completion: { _ in
            completion?()
        })
    }
}

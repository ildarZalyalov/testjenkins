//
//  DetailsAboutMessageRouterInput.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 03.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol DetailsAboutMessageRouterInput: class {
    
    func showMapModule(for mapObject: ChatMessageMapObject, with style: ConversationStyle)

    func closeModule(completion: (() -> ())?)
}

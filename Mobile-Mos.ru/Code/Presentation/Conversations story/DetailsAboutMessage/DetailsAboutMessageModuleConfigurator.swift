//
//  DetailsAboutMessageModuleConfigurator.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 03.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import UIKit

class DetailsAboutMessageModuleConfigurator: BaseModuleConfigurator {
    
    override func configureModule(for view: UIViewController) {
        
        let viewController = view as! DetailsAboutMessageViewController
        let presenter = DetailsAboutMessagePresenter()
        let interactor = DetailsAboutMessageInteractor()
        let router = DetailsAboutMessageRouter()
        
        let blurBuilder = UIImageBlurEffectBuilder()
        let buttonsDataSourceFactory = ChatControlButtonsDataSourceFactoryImplementation()
        let loadTemplateHandler = HTMLTemplateProcessorImplementation()
        let headerController = ParallaxScrollHeaderController()
        let analyticsManager = AnalyticsManagerBuilder.getAnalyticsManager()
        
        let mapPreviewUrlBuilder = YandexMapPreviewUrlBuilderImplementation()
        mapPreviewUrlBuilder.parametersEncoder = HTTPRequestParameterEncoderDefault()
        
        viewController.output = presenter
        viewController.headerController = headerController
        viewController.imageBlurBuilder = blurBuilder
        
        presenter.view = viewController
        presenter.interactor = interactor
        presenter.router = router
        presenter.buttonsDataSourceFactory = buttonsDataSourceFactory
        
        interactor.output = presenter
        interactor.loadTemplateHandler = loadTemplateHandler
        interactor.mapPreviewUrlBuilder = mapPreviewUrlBuilder
        interactor.analyticsManager = analyticsManager
        
        router.transitionHandler = viewController
        router.toOtherModulesTransitionHandler = viewController
    }
}

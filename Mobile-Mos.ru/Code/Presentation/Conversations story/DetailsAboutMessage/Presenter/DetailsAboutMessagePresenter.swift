//
//  DetailsAboutMessagePresenter.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 03.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class DetailsAboutMessagePresenter: DetailsAboutMessageViewOutput, DetailsAboutMessageInteractorOutput {
    
    weak var view: DetailsAboutMessageViewInput!
    var router: DetailsAboutMessageRouterInput!
    var interactor: DetailsAboutMessageInteractorInput!
    
    var buttonsDataSourceFactory: ChatControlButtonsDataSourceFactory!
    var buttonsById: [String : ChatCustomControlButton]!
    
    var moduleConfiguration: DetailsAboutMessageModuleConfiguration!
    var control: ChatMessageDetailsControl {
        return moduleConfiguration.control
    }
    
    //MARK: - Приватные методы
    
    func refreshTemplate() {
        view.showLoadingStatus()
        interactor.loadTemplate(with: control.templateUrl, valuesJsonString: control.valuesJsonString)
    }
    
    //MARK: - DetailsAboutMessageViewOutput
    
    func setupInitialState() {
        
        view.displayTitle(control.title)
        view.setup(with: moduleConfiguration.style)
        
        if moduleConfiguration.mapObject != nil {
            view.setupMapPreview()
        }
        
        refreshTemplate()
        
        guard !control.buttons.isEmpty else { return }
        
        let configuration = buttonsDataSourceFactory.buildDataSourceConfiguration(from: control.buttons)
        buttonsById = configuration.selectionItems
        view.displayButtons(with: configuration.dataSource)
    }
    
    func configure(with configuration: DetailsAboutMessageModuleConfiguration) {
        self.moduleConfiguration = configuration
    }
    
    func obtainUrlForMapPreview(with size: CGSize) -> URL? {
        guard let location = moduleConfiguration.mapObject?.location else { return nil }
        return interactor.obtainUrlForMapPreview(with: location, size: size)
    }
    
    func didPressMapPreview() {
        if let mapObject = moduleConfiguration.mapObject {
            router.showMapModule(for: mapObject, with: moduleConfiguration.style)
        }
    }
    
    func didPressButton(with buttonId: String) {
        
        guard let button = buttonsById[buttonId] else { return }
        
        let buttonsHandlerReference = moduleConfiguration.buttonsHandler
        
        router.closeModule { [weak self] in
            guard let action = button.action else { return }
            buttonsHandlerReference?.perform(action: action,
                                             forButtonWithId: button.itemId,
                                             andTitle: button.title,
                                             in: self?.moduleConfiguration.message,
                                             embeddedMessage: self?.moduleConfiguration.message)
        }
    }
    
    func didPressCancel() {
        router.closeModule(completion: nil)
    }
    
    func didPressRetryObtainContent() {
        refreshTemplate()
    }
    
    //MARK: - DetailsAboutMessageInteractorOutput
    
    func didFinishLoading(template: String?) {
        
        guard let htmlString = template else {
            view.showContentLoadError()
            return
        }
        
        view.displayTemplate(htmlString)
    }
}

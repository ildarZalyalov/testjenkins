//
//  DetailsAboutMessageModuleConfiguration.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 05.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Конифгурация модуля контрола "Подробнее"
struct DetailsAboutMessageModuleConfiguration {
    
    /// Информация о контроле
    var control: ChatMessageDetailsControl
    
    /// Стиль диалога, в котором находится сообщение с контролом
    var style: ConversationStyle
    
    /// Сообщение с контролом (если есть)
    var message: ChatMessage?
    
    /// Вложенное сообщение с контролом (если сообщение было каруселью)
    var embeddedMessage: ChatMessage?
    
    /// Информация об объекте на карте (если есть)
    var mapObject: ChatMessageMapObject?
    
    /// Обработчик нажатий на кнопки в контроле
    weak var buttonsHandler: ChatMessageButtonActionHandler?
    
    init(control: ChatMessageDetailsControl, style: ConversationStyle) {
        self.control = control
        self.style = style
    }
}

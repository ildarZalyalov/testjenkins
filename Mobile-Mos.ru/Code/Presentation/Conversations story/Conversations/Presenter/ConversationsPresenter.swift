//
//  ConversationsPresenter.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 10.02.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation


class ConversationsPresenter: ConversationsViewOutput, ConversationsInteractorOutput, FeaturedBannerEventHandler, UserProfileEventHandler, PromoteAuthorizationCellEventHandler, AuthorizationProcessHandler {
    
    weak var view: ConversationsViewInput!
    var router: ConversationsRouterInput!
    var interactor: ConversationsInteractorInput!
    
    var userProfileViewModelBuilder: UserProfileViewModelBuilder!
    var dataSourceFactory: ConversationsDataSourceFactory!
    
    var dataSourceConfiguration: ConversationsDataSourceConfiguration!
    var userProfileSelectionItems: [String : UserProfileRowViewModel] = [:]
    
    weak var animatableBannerView: AnimatableView?
    
    var isDisplayingLoadingState = false
    
    let retryRefreshConversationsDelay: TimeInterval = 3
    lazy var retryRefreshConversationsPerformer = DelayedWorkPerformer { [weak self] in
        self?.refreshCurrentConversations(forceServerUpdate: false)
    }
    
    //MARK: - Приватные методы
    
    func refreshCurrentConversations(forceServerUpdate: Bool) {
        
        retryRefreshConversationsPerformer.cancelDelayedPerform()
        
        if !isDisplayingLoadingState {

            isDisplayingLoadingState = true

            let loadingDataSource = dataSourceFactory.buildDataSourceForLoadingState()
            view.displayConversations(with: loadingDataSource)
        }
        
        if forceServerUpdate {
            interactor.obtainCurrentConversationsFromServer()
        }
        else {
            interactor.obtainCurrentConversations()
        }
    }
    
    func redispayCurrentConversations() {
        isDisplayingLoadingState = false
        view.displayConversations(with: dataSourceConfiguration.dataSource)
    }
    
    func rebuildDataSourceFrom(conversations: [Conversation], lastMessages: [String : ChatMessage]) {
        
        let isUserAuthorized = interactor.currentUser.isLoggedIn
        let typingStatuses = interactor.obtainTypingStatuses(for: conversations)

        dataSourceConfiguration = dataSourceFactory.buildDataSourceConfiguration(from: conversations, typingStatuses: typingStatuses, and: lastMessages, isUserAuthorized: isUserAuthorized, authorizationCellEventHandler: self)
        
        if !isUserAuthorized {
            let models = userProfileViewModelBuilder.buildUserProfileViewModel(for: interactor.currentUser)
            userProfileSelectionItems = dataSourceFactory.updateDataSource(with: models, in: &dataSourceConfiguration!)
        }
    }
    
    //MARK: - ConversationsViewOutput
    
    func setupInitialState() {
        animatableBannerView = router.embedBannerModule(with: self)
        refreshCurrentConversations(forceServerUpdate: false)
        interactor.sendEvent(with: .screenLoaded(withString: AnalyticsConstants.screenLoadedEventName), chatName: nil)
    }
    
    func didSelectItem(with itemId: String) {
        
        if let conversation = dataSourceConfiguration.selectionItems[itemId] {
            
            view.updateBackButtonTintColor(with: conversation.style.backButtonColor)
            
            if conversation.itemId == mapConversation.itemId {
                router.showMapsDialog()
            }
            else {
                router.showDialog(for: conversation)
            }
            
            interactor.sendEvent(with: .selectedChat(withString: AnalyticsConstants.selectChatEventName), chatName: conversation.title)
        }
        else if let object = userProfileSelectionItems[itemId] {
            
            switch object.type {
            case .aboutApp        : router.showAboutAppInfo()
            case .agreement       : router.showAgreementModule()
            case .appsAndServices : router.showServicesAndAppsModule()
            case .feedback        : router.showSupportModule()
            default: return
            }
        }
    }
    
    func userDidOverscroll(with progress: CGFloat) {
        animatableBannerView?.animateInteractiveTransition(with: progress)
    }
    
    //MARK: - ConversationsInteractorOutput
    
    func didFinishObtainingCurrentConversations(result: ConversationObtainMultipleResult, with lastMessages: [String : ChatMessage]) {
        switch result {
        case .success(let conversations):
            rebuildDataSourceFrom(conversations: conversations, lastMessages: lastMessages)
            redispayCurrentConversations()
        case .failure(_):
            retryRefreshConversationsPerformer.performOnMain(afterDelay: retryRefreshConversationsDelay)
        }
    }
    
    func didUpdateConversations(conversations: [Conversation], with lastMessages: [String : ChatMessage]) {
        rebuildDataSourceFrom(conversations: conversations, lastMessages: lastMessages)
        redispayCurrentConversations()
    }
    
    func didChangeChatConnectionStatus(to connected: Bool) {
        if connected {
            view.hideNetworkErrorStatus()
        }
        else {
            view.showNetworkErrorStatus()
        }
    }

    //MARK: - FeaturedBannerEventHandler
    
    func contentInsets() -> UIEdgeInsets {
        return view.bannerContentInsets
    }
    
    func didUpdateBannerStatusBarStyle(style: UIStatusBarStyle) {
        view.updateBannerStatusBarStyle(style: style)
    }
    
    func userWantsToDisplayAccount() {
        
        if interactor.currentUser.isLoggedIn {
            router.showUserProfile(with: self)
            interactor.sendEvent(with: .userAccount(withString: AnalyticsConstants.userAccountEventName), chatName: nil)
        }
        else {
            router.showAuthorizationModule(with: self)
            interactor.sendEvent(with: .authorizationOnBanner(withString: AnalyticsConstants.authorizationIconEventName), chatName: nil)
        }
    }
    
    //MARK: - UserProfileEventHandler
    
    func didExitFromProfile() {
        refreshCurrentConversations(forceServerUpdate: true)
    }

    //MARK: - PromoteAuthorizationCellEventHandler
    
    func didPressAuthorizeButton() {
        userWantsToDisplayAccount()
        interactor.sendEvent(with: .login(withString: AnalyticsConstants.authorizationButtonEventName), chatName: nil)
    }
    
    //MARK: - AuthorizationProcessHandler
    
    func handleCancelAuthorization() -> Bool {
        return false
    }
    
    func handleFinishAuthorization() {
        refreshCurrentConversations(forceServerUpdate: true)
    }
}

//
//  ConversationsInteractor.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 10.02.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class ConversationsInteractor: ConversationsInteractorInput, SocketConnectionObserverDelegate {
    
    weak var output: ConversationsInteractorOutput!
    
    var userService: UserService!
    var messageHistoryService: ChatMessageHistoryService!
    var analyticsManager: AnalyticsManager!
    
    var connectionObserver: SocketConnectionObserver! {
    
        didSet {
            
            guard connectionObserver != nil else { return }

            connectionObserver.delegate = self
            output.didChangeChatConnectionStatus(to: connectionObserver.isSocketConnected)
        }
    }
    
    var conversationsService: ConversationsService! {
        
        didSet {
            
            guard conversationsService != nil else { return }
            
            conversationsService.cacheObsoleteHandler = { [weak self] in
                self?.obtainCurrentConversationsFromServer(reportOnlySuccess: true)
            }
            
            conversationsService.obtainCachedConversationsController { [weak self] result in
                switch result {
                case .success(let controller):
                    self?.conversationsCacheController = controller
                default:
                    return
                }
            }
        }
    }
    
    var conversationsCacheController: DataBaseFetchController<Conversation>! {
        
        didSet {
            
            guard conversationsCacheController != nil else { return }
            
            let onControllerUpdate: DataBaseFetchUpdateBlock = { [weak self] _ in
                guard let strongSelf = self else { return }
                strongSelf.conversations = strongSelf.conversationsCacheController.results
                guard !strongSelf.isObtainingConversations else { return }
                strongSelf.output?.didUpdateConversations(conversations: strongSelf.conversations,
                                                          with: strongSelf.lastMessagesByConversationId)
            }
            
            conversationsCacheController.onUpdate = onControllerUpdate
            onControllerUpdate(DataBaseFetchUpdate.initalUpdate)
        }
    }
    
    var typingManager: ChatMessageTypingManager! {
        
        didSet {
            
            guard typingManager != nil else { return }
            
            let typingChangedHandler: ChatMessageTypingChangedHandler = { [weak self] eventInfo in
                guard let strongSelf = self else { return }
                strongSelf.output?.didUpdateConversations(conversations: strongSelf.conversations, with: strongSelf.lastMessagesByConversationId)
            }
            
            typingManager.typingHandler = typingChangedHandler
            typingManager.typingStoppedHandler = typingChangedHandler
        }
    }
    
    var conversations = [Conversation]() {
        didSet {
            guard !conversations.isEmpty else { return }
            conversations.append(mapConversation)
        }
    }
    
    var lastMessagesByConversationId = [String : ChatMessage]()
    
    var isObtainingConversationsFromCache = false
    var isObtainingConversationsFromServer = false
    var isObtainingConversations: Bool {
        return isObtainingConversationsFromCache || isObtainingConversationsFromServer
    }
    
    var lastMessageControllersById = [String : DataBaseFetchController<ChatMessage>]()
    
    //MARK: - Приватные методы
    
    func setupLastMessageControllers(for conversations: [Conversation]) {
        
        lastMessageControllersById.removeAll()
        
        for conversation in conversations {
            
            let conversationId = conversation.itemId
            
            let onControllerUpdate: DataBaseFetchUpdateBlock = { [weak self] _ in
                
                guard let strongSelf = self else { return }
                guard let controller = strongSelf.lastMessageControllersById[conversationId] else { return }
                
                strongSelf.lastMessagesByConversationId[conversationId] = controller.results.last
                
                guard !strongSelf.isObtainingConversations else { return }
                strongSelf.output?.didUpdateConversations(conversations: strongSelf.conversations,
                                                          with: strongSelf.lastMessagesByConversationId)
            }
            
            messageHistoryService.obtainLastCachedMessageController(for: conversation, and: userService.currentUser, completion: { [weak self] result in
                switch result {
                case .success(let controller):
                    self?.lastMessageControllersById[conversationId] = controller
                    controller.onUpdate = onControllerUpdate
                    onControllerUpdate(DataBaseFetchUpdate.initalUpdate)
                default:
                    return
                }
            })
        }
    }
    
    func obtainCurrentConversations(reportOnlySuccess: Bool) {
        
        guard !isObtainingConversationsFromServer else { return }
        guard !isObtainingConversationsFromCache else { return }
        isObtainingConversationsFromCache = true
        
        obtainConversations(forceServerUpdate: false) { [weak self] conversationsResult in
            
            guard let strongSelf = self else { return }
            strongSelf.isObtainingConversationsFromCache = false
            
            switch conversationsResult {
                case .success: break
                case .failure: if reportOnlySuccess { return }
            }
            
            strongSelf.output.didFinishObtainingCurrentConversations(result: conversationsResult, with: strongSelf.lastMessagesByConversationId)
        }
    }
    
    func obtainCurrentConversationsFromServer(reportOnlySuccess: Bool) {
        
        isObtainingConversationsFromServer = true
        
        obtainConversations(forceServerUpdate: true) { [weak self] conversationsResult in
            
            guard let strongSelf = self else { return }
            strongSelf.isObtainingConversationsFromServer = false
            
            switch conversationsResult {
                case .success: break
                case .failure: if reportOnlySuccess { return }
            }
            
            strongSelf.output.didFinishObtainingCurrentConversations(result: conversationsResult, with: strongSelf.lastMessagesByConversationId)
        }
    }
    
    func obtainConversations(forceServerUpdate: Bool, completion: @escaping ConversationObtainMultipleHandler) {
        
        let obtainCompletion: ConversationObtainMultipleHandler = { [weak self] obtainResult in
            
            guard let strongSelf = self else { return }
            
            var result: ConversationObtainMultipleResult

            switch obtainResult {
            case .success(var obtainedConversations):
                obtainedConversations.assignDefaultValues()
                strongSelf.setupLastMessageControllers(for: obtainedConversations)
                strongSelf.conversations = obtainedConversations
                result = .success(conversations: strongSelf.conversations)
            default:
                result = obtainResult
            }
            
            completion(result)
        }
        
        if forceServerUpdate {
            conversationsService.obtainConversations(for: currentUser) { serverResult in
                obtainCompletion(serverResult)
            }
        }
        else {
            conversationsService.obtainCachedConversations(for: currentUser) { [weak self] cachedResult in
                
                guard let strongSelf = self else { return }
                
                var shouldObtainFromServer: Bool
                
                switch cachedResult {
                case .success(let conversations):
                    shouldObtainFromServer = conversations.isEmpty
                default:
                    shouldObtainFromServer = true
                }
                
                guard shouldObtainFromServer else {
                    obtainCompletion(cachedResult)
                    return
                }
                
                strongSelf.conversationsService.obtainConversations(for: strongSelf.currentUser) { serverResult in
                    obtainCompletion(serverResult)
                }
            }
        }
    }
    
    //MARK: - ConversationsInteractorInput
    
    var currentUser: User {
        return userService.currentUser
    }
    
    func obtainTypingStatuses(for conversations: [Conversation]) -> [String : Bool] {
        return conversations.toDictionary(with: { $0.itemId }).mapValues({ typingManager.isTypingInConversation(with: $0.itemId) })
    }
    
    func obtainCurrentConversations() {
        obtainCurrentConversations(reportOnlySuccess: false)
    }
    
    func obtainCurrentConversationsFromServer() {
        obtainCurrentConversationsFromServer(reportOnlySuccess: false)
    }
    
    func sendEvent(with event: ConversationsScreenEvents, chatName: String?) {
        analyticsManager.sendMainScreenEvent(with: event, chatName: chatName)
    }
    
    //MARK: - SocketConnectionObserverDelegate
    
    func socketObserver(observer: SocketConnectionObserver, observedConnectionChangeTo connected: Bool) {
        output.didChangeChatConnectionStatus(to: connected)
    }
}

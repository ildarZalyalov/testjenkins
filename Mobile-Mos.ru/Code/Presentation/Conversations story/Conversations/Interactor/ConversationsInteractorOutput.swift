//
//  ConversationsInteractorOutput.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 10.02.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol ConversationsInteractorOutput: class {

    func didFinishObtainingCurrentConversations(result: ConversationObtainMultipleResult, with lastMessages: [String : ChatMessage])
    
    func didUpdateConversations(conversations: [Conversation], with lastMessages: [String : ChatMessage])
    
    func didChangeChatConnectionStatus(to connected: Bool)
}

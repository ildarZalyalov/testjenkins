//
//  ConversationsInteractorInput.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 10.02.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol ConversationsInteractorInput: class {
    
    var currentUser: User { get }
    
    func obtainTypingStatuses(for conversations: [Conversation]) -> [String : Bool]

    func obtainCurrentConversations()
    
    func obtainCurrentConversationsFromServer()
    
    func sendEvent(with event: ConversationsScreenEvents, chatName: String?)
}

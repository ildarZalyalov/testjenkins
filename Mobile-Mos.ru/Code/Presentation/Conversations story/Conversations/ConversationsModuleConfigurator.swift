//
//  ConversationsModuleConfigurator.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 10.02.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import UIKit

class ConversationsModuleConfigurator: BaseModuleConfigurator {
    
    override func configureModule(for view: UIViewController) {
        
        let viewController = view as! ConversationsViewController
        let presenter = ConversationsPresenter()
        let interactor = ConversationsInteractor()
        let router = ConversationsRouter()
        
        let dataSourceFactory = ConversationsDataSourceFactoryImplementation()
        
        let stringsBuilder = UserProfileStringBuilderImplementation()
        let userProfileViewModelBuilder = UserProfileViewModelBuilderImplementation()
        userProfileViewModelBuilder.stringBuilder = stringsBuilder
        
        let connectionObserver = UIApplication.shared.serviceBuilder.getSocketConnectionObserver()
        let conversationsService = UIApplication.shared.serviceBuilder.getConversationsService()
        let userService = UIApplication.shared.serviceBuilder.getUserService()
        let messageHistoryService = UIApplication.shared.serviceBuilder.getChatMessageHistoryService()
        
        let headerController = ParallaxScrollHeaderController()
        let bannerLoader = FeaturedBannerModuleLoader()
        let alertsFactory = CommonAlertsFactoryImplementation()
        
        let messageService = UIApplication.shared.serviceBuilder.getChatMessageService()
        let interactionService = UIApplication.shared.serviceBuilder.getChatMessageInteractionService()
        let analyticsManager = AnalyticsManagerBuilder.getAnalyticsManager()
        
        let typingManager = ChatMessageTypingManagerImplementation()
        typingManager.messageService = messageService
        typingManager.interactionService = interactionService
        
        viewController.output = presenter
        viewController.headerController = headerController
        
        presenter.view = viewController
        presenter.interactor = interactor
        presenter.router = router
        presenter.userProfileViewModelBuilder = userProfileViewModelBuilder
        presenter.dataSourceFactory = dataSourceFactory
        
        interactor.output = presenter
        interactor.connectionObserver = connectionObserver
        interactor.conversationsService = conversationsService
        interactor.userService = userService
        interactor.messageHistoryService = messageHistoryService
        interactor.typingManager = typingManager
        interactor.analyticsManager = analyticsManager
        
        router.parentViewController = viewController
        router.embeddableHeaderController = headerController
        router.bannerModuleLoader = bannerLoader
        router.alertsFactory = alertsFactory
        router.transitionHandler = viewController
        router.toOtherModulesTransitionHandler = viewController
        router.alertTransitionHandler = viewController
    }
}

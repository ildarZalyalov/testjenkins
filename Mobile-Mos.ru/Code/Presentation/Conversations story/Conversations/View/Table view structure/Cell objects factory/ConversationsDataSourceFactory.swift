//
//  ConversationsDataSourceFactory.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 14.02.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Конфигурация источника данных для списка диалогов
struct ConversationsDataSourceConfiguration {
    
    /// Источник данных
    var dataSource: TableViewDataSource
    
    /// Словарь с диалогами по идентификатору
    var selectionItems: [String : Conversation]
}

/// Фабрика для работы с источником данных для списка диалогов
protocol ConversationsDataSourceFactory {
    
    /// Создать источник данных для отображения состояния загрузки диалогов
    ///
    /// - Returns: источник данных для отображения состояния загрузки диалогов
    func buildDataSourceForLoadingState() -> TableViewDataSource
    
    /// Создать источник данных для списка диалогов
    ///
    /// - Parameters:
    ///   - conversations: диалоги
    ///   - typingStatuses: наличие события "вам печатают" в каждом диалоге по идентификатору диалога
    ///   - lastMessages: последние сообщения в каждом диалоге по идентификатору диалога
    ///   - isUserAuthorized: авторизован ли пользователь
    ///   - authorizationCellEventHandler: обработчик событий в ячейке предложения авторизации
    /// - Returns: источник данных для списка диалогов
    func buildDataSourceConfiguration(from conversations: [Conversation],
                                      typingStatuses: [String : Bool],
                                      and lastMessages: [String: ChatMessage],
                                      isUserAuthorized: Bool,
                                      authorizationCellEventHandler: PromoteAuthorizationCellEventHandler?) -> ConversationsDataSourceConfiguration
    
    /// Обновить источник данных секциями ячеек из ЛК
    ///
    /// - Parameters:
    ///   - models: модели секций ячеек ЛК
    ///   - configuration: конфигурация источника данных
    /// - Returns: cловарь с моделями ячеек по идентификатору
    func updateDataSource(with models: [UserProfileSectionViewModel], in configuration: inout ConversationsDataSourceConfiguration) -> [String : UserProfileRowViewModel]
}

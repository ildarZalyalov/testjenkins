//
//  ConversationsDataSourceFactoryImplementation.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 14.02.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class ConversationsDataSourceFactoryImplementation: ConversationsDataSourceFactory {
    
    let numberOfConversationPlaceholders: Int = {
        
        let deviceScreenType = UIDevice.current.deviceScreenType
        
        switch deviceScreenType {
        case .iPhone6Plus, .iPhoneX:
            return 6
        case .iPhone6:
            return 5
        default:
            return 4
        }
    }()
    
    //MARK: - ConversationsDataSourceFactory
    
    func buildDataSourceForLoadingState() -> TableViewDataSource {
        
        let dataStructure = TableViewDataSourceStructure()
        dataStructure.appendSection()
        
        dataStructure.appendCellObject(ConversationGroupHeaderPlaceholderCellObject())
        dataStructure.appendCellObject(ConversationPlaceholderCellObject(numberOfPlaceholders: numberOfConversationPlaceholders))
        
        return TableViewDataSource(with: dataStructure)
    }
    
    func buildDataSourceConfiguration(from conversations: [Conversation],
                                      typingStatuses: [String : Bool],
                                      and lastMessages: [String: ChatMessage],
                                      isUserAuthorized: Bool,
                                      authorizationCellEventHandler: PromoteAuthorizationCellEventHandler?) -> ConversationsDataSourceConfiguration {
        
        var dataStructure = TableViewDataSourceStructure()
        var selectionItems = [String : Conversation]()
        let sortedConversations = conversations.sorted(by: { lastMessages[$0.itemId]?.dateBasedOnMessageGroupHistory ?? Date.distantPast > lastMessages[$1.itemId]?.dateBasedOnMessageGroupHistory ?? Date.distantPast })
        
        let newConversationsKey = "newConversationsKey"
        let recentConversationsKey = "recentConversationsKey"
        let groupedConversations = sortedConversations.group(by: { (conversation) -> String in
            let unreadMessagesCount = conversation.unreadMessagesCount + conversation.unseenMessagesCount
            return unreadMessagesCount > 0 ? newConversationsKey: recentConversationsKey
        })
        
        if let newConversations = groupedConversations[newConversationsKey] {
            let headerCellObject = ConversationGroupHeaderCellObject(headerTitle: StringsHelper.conversationsSectionNewTitle,
                                                                     isSearchButtonDisplayed: dataStructure.numberOfSections() == 0)
            dataStructure.appendSection(with: [headerCellObject])
            append(section: newConversations, typingStatuses: typingStatuses, with: lastMessages, to: &dataStructure, and: &selectionItems)
        }
        
        if let recentConversations = groupedConversations[recentConversationsKey] {
            let headerCellObject = ConversationGroupHeaderCellObject(headerTitle: StringsHelper.conversationsSectionRecentTitle,
                                                                     isSearchButtonDisplayed: dataStructure.numberOfSections() == 0)
            dataStructure.appendSection(with: [headerCellObject])
            append(section: recentConversations, typingStatuses: typingStatuses, with: lastMessages, to: &dataStructure, and: &selectionItems)
        }
        
        if !isUserAuthorized {
            let authorizationCellObject = PromoteAuthorizationCellObject(eventHandler: authorizationCellEventHandler)
            dataStructure.appendSection(with: [authorizationCellObject])
        }

        let dataSource = TableViewDataSource(with: dataStructure)
        
        return ConversationsDataSourceConfiguration(dataSource: dataSource, selectionItems: selectionItems)
    }
    
    func updateDataSource(with models: [UserProfileSectionViewModel], in configuration: inout ConversationsDataSourceConfiguration) -> [String : UserProfileRowViewModel] {
        
        let dataSource = configuration.dataSource
        var selectionItems = [String : UserProfileRowViewModel]()
        
        for section in models {
    
            var sectionsItems = [UserProfileCellObject]()
            
            for item in section.sectionInfo.enumerated() {
                
                let isLastCellInSection = item.offset == (section.sectionInfo.count - 1)
                let cellObject = UserProfileCellObject(itemId: UUID().uuidString, icon: item.element.icon, iconUrl: nil, iconName: nil, title: item.element.title, subtitle: item.element.subtitle, isLastCellInSection: isLastCellInSection)
                
                selectionItems[cellObject.itemId] = item.element
                sectionsItems.append(cellObject)
            }
            
            dataSource.dataStructure.appendSection(with: sectionsItems)
        }
        
        return selectionItems
    }
    
    //MARK: - Приватные методы
    
    fileprivate func append(section conversations: [Conversation],
                            typingStatuses: [String : Bool],
                            with lastMessages: [String: ChatMessage],
                            to dataStructure: inout TableViewDataSourceStructure,
                            and selectionItems: inout [String : Conversation]) {
        
        for conversation in conversations {
            
            let message = lastMessages[conversation.itemId]
            
            let unreadMessagesCount = conversation.unreadMessagesCount + conversation.unseenMessagesCount
            let previewText = message?.content.previewString ?? conversation.descriptionText
            let isLastMessageFailed = message?.status == .failed
            let isRecipientTyping = typingStatuses[conversation.itemId] ?? false

            let cellObject = ConversationCellObject(itemId: conversation.itemId,
                                                    recipientName: conversation.title,
                                                    previewText: previewText,
                                                    recipientAvatarUrl: conversation.avatarIconUrl,
                                                    recipientAvatarPlaceholderName: conversation.avatarPlaceholderIconName,
                                                    unreadMessagesCount: unreadMessagesCount,
                                                    isLastMessageFailed: isLastMessageFailed,
                                                    isRecipientTyping: isRecipientTyping)
            
            dataStructure.appendCellObject(cellObject)
            selectionItems[cellObject.itemId] = conversation
        }
    }
}

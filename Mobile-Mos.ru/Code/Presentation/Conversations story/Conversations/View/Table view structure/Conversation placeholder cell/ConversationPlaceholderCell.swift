//
//  ConversationPlaceholderCell.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 08.08.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class ConversationPlaceholderCell: UITableViewCell, ConfigurableView {
    
    fileprivate var placeholderImageViewsCache: [UIImageView] = []
    fileprivate var placeholderImageViews: ArraySlice<UIImageView> = []
    
    let placeholderWidthInset: CGFloat = 13
    let placeholderHeight: CGFloat = 82
    
    override func awakeFromNib() {
        NotificationCenter.default.addObserver(self, selector: #selector(applicationDidBecomeActive), name: .UIApplicationDidBecomeActive, object: nil)
    }
    
    override func prepareForReuse() {
        placeholderImageViews.forEach { $0.stopLoadingAnimation() }
    }
    
    override func systemLayoutSizeFitting(_ targetSize: CGSize) -> CGSize {
        return CGSize(width: targetSize.width, height: CGFloat(placeholderImageViews.count) * placeholderHeight)
    }
    
    override func systemLayoutSizeFitting(_ targetSize: CGSize, withHorizontalFittingPriority horizontalFittingPriority: UILayoutPriority, verticalFittingPriority: UILayoutPriority) -> CGSize {
        return systemLayoutSizeFitting(targetSize)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func willMove(toWindow newWindow: UIWindow?) {
        if newWindow != nil {
            placeholderImageViews.forEach { $0.startLoadingAnimation() }
        }
    }
    
    //MARK: - Приватные методы
    
    fileprivate func createPlaceholderView() -> UIImageView {
        
        let image = #imageLiteral(resourceName: "chatListElementPlaceholder").resizableImage(withCapInsets: UIEdgeInsets(top: 40, left: 215, bottom: 40, right: 15))
        
        let imageView = UIImageView(image: image)
        imageView.autoresizingMask = [.flexibleWidth, .flexibleBottomMargin]
        contentView.addSubview(imageView)
        
        return imageView
    }
    
    fileprivate func increasePlaceholdersCacheIfNeeded(for count: Int) {
        
        let currentCacheSize = placeholderImageViewsCache.count
        let difference = count - currentCacheSize
        
        if difference > 0 {
            for _ in 0 ..< difference {
                placeholderImageViewsCache.append(createPlaceholderView())
            }
        }
        
        for (index, placeholer) in placeholderImageViewsCache.enumerated() {
            placeholer.isHidden = index >= count
        }
        
        placeholderImageViews = placeholderImageViewsCache.prefix(upTo: count)
    }
    
    fileprivate func calculatePlaceholdersLayout() {
        
        var currentY: CGFloat = 0
        
        for placeholder in placeholderImageViews {
            placeholder.frame = CGRect(x: 0, y: currentY, width: contentView.width, height: placeholderHeight).insetBy(dx: placeholderWidthInset, dy: 0)
            currentY += placeholderHeight
        }
    }
    
    @objc
    fileprivate func applicationDidBecomeActive() {
        placeholderImageViews.forEach { $0.startLoadingAnimation() }
    }
    
    //MARK: - ConfigurableView
    
    func configure(with object: Any) {
        
        guard let cellObject = object as? ConversationPlaceholderCellObject else {
            return
        }
        
        increasePlaceholdersCacheIfNeeded(for: cellObject.numberOfPlaceholders)
        calculatePlaceholdersLayout()
        placeholderImageViews.forEach { $0.startLoadingAnimation() }
    }
}

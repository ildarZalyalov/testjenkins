//
//  ConversationGroupHeaderCellObject.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 04.08.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

struct ConversationGroupHeaderCellObject: CellObject {
    
    var headerTitle: String
    
    var isSearchButtonDisplayed: Bool
}

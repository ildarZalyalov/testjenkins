//
//  ConversationGroupHeaderCell.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 04.08.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import UIKit

class ConversationGroupHeaderCell: UITableViewCell, ConfigurableView {
    
    @IBOutlet weak var headerTitleLabel: UILabel!
    @IBOutlet weak var searchButton: UIButton!
    
    //MARK: - ConfigurableView
    
    func configure(with object: Any) {
        
        guard let headerObject = object as? ConversationGroupHeaderCellObject else {
            return
        }
        
        headerTitleLabel.text = headerObject.headerTitle
        searchButton.isHidden = !headerObject.isSearchButtonDisplayed
    }
}

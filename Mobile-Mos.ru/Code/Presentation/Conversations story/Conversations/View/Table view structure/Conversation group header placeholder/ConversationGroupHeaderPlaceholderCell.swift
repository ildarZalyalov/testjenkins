//
//  ConversationGroupHeaderPlaceholderCell.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 08.08.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class ConversationGroupHeaderPlaceholderCell: UITableViewCell, ConfigurableView {
    
    @IBOutlet weak var placeholderImageView: UIImageView!
    
    override func awakeFromNib() {
        NotificationCenter.default.addObserver(self, selector: #selector(applicationDidBecomeActive), name: .UIApplicationDidBecomeActive, object: nil)
        placeholderImageView.image = #imageLiteral(resourceName: "chatListHeaderPlaceholder").resizableImage(withCapInsets: UIEdgeInsets(top: 9, left: 11, bottom: 9, right: 11))
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func willMove(toWindow newWindow: UIWindow?) {
        if newWindow != nil {
            placeholderImageView.startLoadingAnimation()
        }
    }
    
    @objc
    fileprivate func applicationDidBecomeActive() {
        placeholderImageView.startLoadingAnimation()
    }
    
    //MARK: - ConfigurableView
    
    func configure(with object: Any) {
        placeholderImageView.startLoadingAnimation()
    }
}

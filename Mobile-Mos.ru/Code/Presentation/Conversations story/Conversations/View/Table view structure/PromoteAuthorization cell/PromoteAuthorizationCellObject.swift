//
//  PromoteAuthorizationCellObject.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 08.12.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

struct PromoteAuthorizationCellObject: CellObject {
    
    weak var eventHandler: PromoteAuthorizationCellEventHandler?
}

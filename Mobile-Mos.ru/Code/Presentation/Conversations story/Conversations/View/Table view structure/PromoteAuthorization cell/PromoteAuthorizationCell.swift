//
//  PromoteAuthorizationCell.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 08.12.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import UIKit

protocol PromoteAuthorizationCellEventHandler: class {
    
    func didPressAuthorizeButton()
}

class PromoteAuthorizationCell: UITableViewCell, ConfigurableView {
    
    @IBOutlet weak var leftCornerImageView: UIImageView!
    @IBOutlet weak var rightCornerImageView: UIImageView!
    
    @IBOutlet weak var buttonBackgroundImageView: UIImageView!
    @IBOutlet weak var buttonShadowImageView: UIImageView!

    weak var eventHandler: PromoteAuthorizationCellEventHandler?
    
    @IBAction func pressedAuthorizeButton() {
        eventHandler?.didPressAuthorizeButton()
    }
    
    override func awakeFromNib() {
        
        leftCornerImageView.image = #imageLiteral(resourceName: "promoteAuthorizationBannerLeftCorner").resizableImage(withCapInsets: UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 1))
        rightCornerImageView.image = #imageLiteral(resourceName: "promoteAuthorizationBannerRightCorner").resizableImage(withCapInsets: UIEdgeInsets(top: 0, left: 1, bottom: 0, right: 16))
        
        /// Растягиваемая картинка с фоном кнопки
        let buttonBackgroundImage = #imageLiteral(resourceName: "roundedButtonBackground").resizableImage(withCapInsets: UIEdgeInsets(top: 0, left: 17, bottom: 0, right: 17))
        buttonBackgroundImageView.image = buttonBackgroundImage
        
        let buttonShadowImage = #imageLiteral(resourceName: "roundedButtonShadow").resizableImage(withCapInsets: UIEdgeInsets(top: 0, left: 27, bottom: 0, right: 27))
        buttonShadowImageView.image = buttonShadowImage
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        separatorInset = UIEdgeInsets(top: 0, left: contentView.width, bottom: 0, right: 0)
    }
    
    //MARK: - ConfigurableView
    
    func configure(with object: Any) {
        
        guard let cellObject = object as? PromoteAuthorizationCellObject else {
            return
        }
        
        eventHandler = cellObject.eventHandler
    }
}

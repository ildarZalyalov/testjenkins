//
//  ConversationCellObject.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 14.02.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Cell оbject диалога
struct ConversationCellObject: CellObjectWithId {
    
    /// Идентификатор диалога
    var itemId: String
    
    /// Название диалога
    var recipientName: String
    
    /// Текст превью диалога
    var previewText: String
    
    /// Ссылка на аватарку для диалога
    var recipientAvatarUrl: URL?
    
    /// Название плейсхолдера для аватарки диалога
    var recipientAvatarPlaceholderName: String?
    
    /// Количество непрочитанных сообщений в диалоге
    var unreadMessagesCount = 0
    
    /// Является ли последнее сообщение в диалоге отправленным с ошибкой
    var isLastMessageFailed = false
    
    /// Отображать ли для диалога событие "вам печатают"
    var isRecipientTyping = false
}

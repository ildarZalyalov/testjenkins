//
//  ConversationCell.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 14.02.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import UIKit

class ConversationCell: UITableViewCell, ConfigurableView {
    
    let cellHeight: CGFloat = 81
    
    let maxNumberOfUnreadDisplayed = 99
    let maxNumberOfUnreadExceededText = "99+"
    
    let previewAnimationTexts: [String] = StringsHelper.conversationIsTypingPreviewTexts
    let previewAnimationStepDuration: TimeInterval = 0.23
    
    @IBOutlet weak var avatarImageView: UIImageView!
    
    @IBOutlet weak var recipientLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    
    @IBOutlet weak var previewTextLabel: UILabel!
    
    @IBOutlet weak var photoPreviewView: UIImageView!
    
    @IBOutlet weak var unreadCountLabelBackgroundView: UIView!
    @IBOutlet weak var unreadCountLabel: UILabel!
    
    @IBOutlet var onlyPreviewTextConstraints: [NSLayoutConstraint]!
    @IBOutlet var textAndPhotoPreviewConstraints: [NSLayoutConstraint]!
    @IBOutlet var textAndUnreadCountConstraints: [NSLayoutConstraint]!
    @IBOutlet var allPreviewViewsConstraints: [NSLayoutConstraint]!
    
    var isShowingPreviewAnimation: Bool = false
    var previewAnimationTextIndex = 0
    lazy var previewAnimationPerformer: DelayedSelectorPerformer = {
        return DelayedSelectorPerformer(target: self, selector: #selector(updatePreviewAnimationText))
    }()
    
    override func awakeFromNib() {
        NotificationCenter.default.addObserver(self, selector: #selector(applicationDidBecomeActive), name: .UIApplicationDidBecomeActive, object: nil)
    }
    
    override func prepareForReuse() {
        avatarImageView.stopLoadingAnimation()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func systemLayoutSizeFitting(_ targetSize: CGSize) -> CGSize {
        return CGSize(width: targetSize.width, height: cellHeight)
    }
    
    override func systemLayoutSizeFitting(_ targetSize: CGSize, withHorizontalFittingPriority horizontalFittingPriority: UILayoutPriority, verticalFittingPriority: UILayoutPriority) -> CGSize {
        return systemLayoutSizeFitting(targetSize)
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        
        contentView.alpha = highlighted ? 0.5 : 1
        
        guard highlighted && !unreadCountLabelBackgroundView.isHidden else {
            super.setHighlighted(highlighted, animated: animated)
            return
        }
        
        // чтобы цвет фона значка не пропадал при нажатии на ячейку
        let unreadBackgroundColor = unreadCountLabelBackgroundView.backgroundColor
        super.setHighlighted(highlighted, animated: animated)
        unreadCountLabelBackgroundView.backgroundColor = unreadBackgroundColor
    }
    
    override func willMove(toWindow newWindow: UIWindow?) {
        if newWindow != nil && avatarImageView.image == nil {
            avatarImageView.startLoadingAnimation()
        }
    }
    
    @objc
    fileprivate func applicationDidBecomeActive() {
        if avatarImageView.image == nil {
            avatarImageView.startLoadingAnimation()
        }
    }
    
    //MARK: - ConfigurableView
    
    func configure(with object: Any) {
        
        guard let cellObject = object as? ConversationCellObject else {
            return
        }
        
        avatarImageView.image = nil
        
        var placeholderImage: UIImage?
        if let placeholderName = cellObject.recipientAvatarPlaceholderName {
            placeholderImage = UIImage(named: placeholderName)
        }
        
        if placeholderImage == nil {
            avatarImageView.startLoadingAnimation()
        }
        else {
            avatarImageView.stopLoadingAnimation()
        }
        
        avatarImageView.loadAndDisplayImage(from: cellObject.recipientAvatarUrl,
                                            placeholder: placeholderImage,
                                            with: nil) { [weak self] result in
                                                
                                                switch result {
                                                case .success:
                                                    self?.avatarImageView.stopLoadingAnimation()
                                                case .failure:
                                                    return
                                                }
        }
        
        recipientLabel.text = cellObject.recipientName
        
        if cellObject.isRecipientTyping {
            startPreviewAnimation()
        }
        else {
            
            stopPreviewAnimation()
            
            let previewText = cellObject.isLastMessageFailed ? StringsHelper.conversationMessageSendingFailedPreviewText : cellObject.previewText
            previewTextLabel.attributedText = attributedPreview(with: previewText)
        }
        
        if cellObject.isLastMessageFailed {
            unreadCountLabelBackgroundView.isHidden = false
            unreadCountLabel.text = StringsHelper.conversationMessageSendingFailedUnreadText
        }
        else {
            
            let unreadCount = cellObject.unreadMessagesCount
            unreadCountLabelBackgroundView.isHidden = unreadCount <= 0
            
            if !unreadCountLabelBackgroundView.isHidden {
                unreadCountLabel.text = unreadCount > maxNumberOfUnreadDisplayed ? maxNumberOfUnreadExceededText : String(describing: unreadCount)
            }
        }
        
        updateConstraintsForContent()
    }
    
    //MARK: - Оформление
    
    fileprivate func attributedPreview(with text: String) -> NSAttributedString {
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .left
        paragraphStyle.lineBreakMode = .byTruncatingTail
        paragraphStyle.lineSpacing = 2.5
        
        let font = previewTextLabel.font
        let textColor = previewTextLabel.textColor
        
        let attributes: [NSAttributedStringKey : Any] = [
            NSAttributedStringKey.font : font as Any,
            NSAttributedStringKey.foregroundColor: textColor as Any,
            NSAttributedStringKey.paragraphStyle: paragraphStyle
        ]
        
        return NSAttributedString(string: text, attributes: attributes)
    }
    
    fileprivate func updateConstraintsForContent() {
        
        let allConstraintsCombinations: [[NSLayoutConstraint]] = [
            allPreviewViewsConstraints,
            textAndUnreadCountConstraints,
            textAndPhotoPreviewConstraints,
            onlyPreviewTextConstraints
        ]
        
        var activeConstraints: [NSLayoutConstraint] = allPreviewViewsConstraints
        
        if unreadCountLabelBackgroundView.isHidden && photoPreviewView.isHidden {
            activeConstraints = onlyPreviewTextConstraints
        }
        else if unreadCountLabelBackgroundView.isHidden {
            activeConstraints = textAndPhotoPreviewConstraints
        }
        else if photoPreviewView.isHidden {
            activeConstraints = textAndUnreadCountConstraints
        }
        
        for constraints in allConstraintsCombinations {
            guard constraints != activeConstraints else { continue }
            NSLayoutConstraint.deactivate(constraints)
        }
        
        NSLayoutConstraint.activate(activeConstraints)
    }
    
    //MARK: - Анимация
    
    func startPreviewAnimation() {
        
        guard !isShowingPreviewAnimation else { return }
        
        isShowingPreviewAnimation = true
        previewAnimationTextIndex = 0
        
        updatePreviewAnimationText()
    }
    
    func stopPreviewAnimation() {
        
        guard isShowingPreviewAnimation else { return }
        
        isShowingPreviewAnimation = false
        previewAnimationPerformer.cancelDelayedPerform()
    }
    
    @objc
    fileprivate func updatePreviewAnimationText() {
        
        guard isShowingPreviewAnimation else { return }
        
        previewTextLabel.text = previewAnimationTexts[previewAnimationTextIndex]
        
        previewAnimationTextIndex += 1
        if previewAnimationTextIndex == previewAnimationTexts.count {
            previewAnimationTextIndex = 0
        }
        
        previewAnimationPerformer.perform(afterDelay: previewAnimationStepDuration)
    }
}

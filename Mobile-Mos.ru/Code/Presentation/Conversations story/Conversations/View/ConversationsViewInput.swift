//
//  ConversationsViewInput.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 10.02.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol ConversationsViewInput: class {
    
    var bannerContentInsets: UIEdgeInsets { get }
    
    func updateBannerStatusBarStyle(style: UIStatusBarStyle)
    
    func displayConversations(with dataSource: TableViewDataSource)
    
    func updateBackButtonTintColor(with color: UIColor)
    
    func showNetworkErrorStatus()
    
    func hideNetworkErrorStatus()
}

//
//  ConversationsViewController.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 10.02.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import UIKit

class ConversationsViewController: BaseViewController, ConversationsViewInput, UITableViewDelegate, NavigationBarCustomizingController, ParallaxScrollHeaderControllerDelegate, UINavigationBarDelegate {
    
    var output: ConversationsViewOutput!
    
    var tableViewDataDisplayManager: TableViewDataSource!
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var navigationBar: UINavigationBar!
    
    var headerController: ParallaxScrollHeaderController!
    
    let headerHeight: CGFloat = {
       
        let deviceScreenType = UIDevice.current.deviceScreenType
        
        switch deviceScreenType {
        case .iPhone6Plus, .iPhoneX:
            return 195
        case .iPhone6:
            return 175
        default:
            return 150
        }
    }()
    
    let headerWithNotificationsHeight: CGFloat = {
        
        let deviceScreenType = UIDevice.current.deviceScreenType
        
        switch deviceScreenType {
        case .iPhone6Plus, .iPhoneX:
            return 250
        case .iPhone6:
            return 225
        default:
            return 192
        }
    }()
    
    let heightToCurveMultiplier: CGFloat = {
        
        let deviceScreenType = UIDevice.current.deviceScreenType
        
        switch deviceScreenType {
        case .iPhone6Plus, .iPhoneX:
            return 1 / 7
        case .iPhone6:
            return 1 / 8
        default:
            return 1 / 10
        }
    }()
    
    let headerTapViewHorizontalInset: CGFloat = 65
    
    lazy var navbarStateBoundaryPointY: CGFloat = {
       return -UIApplication.shared.statusBarFrame.height
    }()
    var statusBarStyle: UIStatusBarStyle = .default
    var statusBarStyleCurrent: UIStatusBarStyle = .default
    
    var backButtonTintColor = UIColor.white
    
    let overscrollBeginOffset: CGFloat = 0.4
    let overscrollSpeedMultiplier: CGFloat = 3
    let overscrollProgressInterval: ClosedRange<CGFloat> = 0 ... 1
    
    var updatedInsets = false
    var isNotificationsModuleDisplayed = false
    
    let chatTitleView = SubtitledNavigationTitleView.loadNib()
    
    //MARK: - Методы
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        navigationBar.topItem?.titleView = chatTitleView
        navigationBar.delegate = self
        pinToTopLayoutGuide(subview: navigationBar, attribute: .bottom)
        
        setupHeaderScrollToTopView()
        
        tableView.estimatedRowHeight = 81
        tableView.estimatedSectionHeaderHeight = 1
        tableView.estimatedSectionFooterHeight = 1
        
        tableView.registerCellNib(for: ConversationCellObject.self)
        tableView.registerCellNib(for: UserProfileCellObject.self)
        
        headerController.scrollView = tableView
        headerController.defaultHeight = headerHeight
        headerController.mode = .topFill
        headerController.delegate = self
        
        headerController.headerViewMaskingPathCalculator = { [unowned self] frame in
            
            let path = UIBezierPath()
            let currentHeight = self.headerController.defaultHeight
            let notificationsOffset = currentHeight - self.headerHeight
            let headerHeight = frame.height - notificationsOffset
            let curveHeight = headerHeight * self.heightToCurveMultiplier
            
            path.move(to: frame.origin)
            
            path.addLine(to: CGPoint(x: frame.maxX, y: frame.origin.y))
            path.addLine(to: CGPoint(x: frame.maxX, y: frame.maxY - curveHeight))
            
            path.addQuadCurve(to: CGPoint(x: frame.origin.x, y: frame.maxY - curveHeight), controlPoint: CGPoint(x: frame.midX, y: frame.maxY + curveHeight))
            
            path.close()
            
            return path
        }
        
        output.setupInitialState()
    }
    
    deinit {
        headerController.scrollView = nil
    }
    
    override var shouldAutorotate: Bool {
        // для дочерних контроллеров, которые могут поменять ориентацию
        return true
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return statusBarStyleCurrent
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return .portrait
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        refreshNavigationBarState()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        
        guard !updatedInsets else { return }
        updatedInsets = true
        
        if #available(iOS 11, *) {
            
            var insets = tableView.contentInset
            insets.bottom += view.safeAreaInsets.bottom
            
            tableView.contentInset = insets
            tableView.scrollIndicatorInsets = insets
        }
    }
    
    @IBAction func scrollToTop() {
        tableView.setContentOffset(CGPoint(x: 0, y: -headerHeight), animated: true)
    }
    
    @IBAction func unwindToConversations(_: UIStoryboardSegue) {}
    
    func setupHeaderScrollToTopView() {
        
        let tapView = UIView(frame: navigationBar.bounds)
        tapView.backgroundColor = UIColor.clear
        tapView.translatesAutoresizingMaskIntoConstraints = false
        tapView.isUserInteractionEnabled = true
        
        let tapViewInsets = UIEdgeInsets(top: 0, left: headerTapViewHorizontalInset, bottom: 0, right: headerTapViewHorizontalInset)
        navigationBar.addSubview(tapView)
        navigationBar.addConstraints(NSLayoutConstraint.edgesConstraints(for: tapView, with: tapViewInsets))
        
        let tapHeaderRecognizer = UITapGestureRecognizer(target: self, action: #selector(scrollToTop))
        tapView.addGestureRecognizer(tapHeaderRecognizer)
    }
    
    func refreshNavigationBarState() {
        
        if tableView.contentOffset.y < navbarStateBoundaryPointY && !navigationBar.isHidden {
            
            navigationBar.isHidden = true
            
            statusBarStyleCurrent = statusBarStyle
            setNeedsStatusBarAppearanceUpdate()
            
        } else if tableView.contentOffset.y > navbarStateBoundaryPointY && navigationBar.isHidden {
            
            navigationBar.isHidden = false
            
            statusBarStyleCurrent = .default
            setNeedsStatusBarAppearanceUpdate()
        }
    }
    
    //MARK: - NavigationBarCustomizingController
    
    var navigationBarState: NavigationBarState? {
        
        let titleTextAttributes = [
            NSAttributedStringKey.font : UIFont(customName: .graphikLCGBold, size: 15) as Any,
            NSAttributedStringKey.foregroundColor : UIColorPalette.conversationsPageTitleColor
        ]
        let subtitleTextAttributes = [
            NSAttributedStringKey.font : UIFont(customName: .graphikLCGMedium, size: 12) as Any,
            NSAttributedStringKey.foregroundColor : UIColorPalette.conversationsPageTitleColor.withAlphaComponent(0.8)
        ]
        
        chatTitleView.titleTextAttributes = titleTextAttributes
        chatTitleView.subtitleTextAttributes = subtitleTextAttributes
        chatTitleView.title = StringsHelper.conversationsPageTitle
        
        var state = NavigationBarState()
        
        state.isHidden = true
        state.backgroundState = NavigationBarBackgroundState.regular
        state.tintColor = backButtonTintColor
        state.isUserInteractionEnabled = false
        
        return state
    }
    
    //MARK: - ConversationsViewInput
    
    var bannerContentInsets: UIEdgeInsets {
        let topInset = UIApplication.shared.statusBarFrame.height
        return UIEdgeInsets(top: topInset, left: 0, bottom: 0, right: 0)
    }
    
    func updateBannerStatusBarStyle(style: UIStatusBarStyle) {
        
        statusBarStyle = style
        
        if navigationBar.isHidden {
            statusBarStyleCurrent = style
            setNeedsStatusBarAppearanceUpdate()
        }
    }
    
    func displayConversations(with dataSource: TableViewDataSource) {
        
        tableViewDataDisplayManager = dataSource
        tableViewDataDisplayManager.delegate = self
        tableViewDataDisplayManager.assign(to: tableView)
        
        tableView.reloadData()
    }
    
    func updateBackButtonTintColor(with color: UIColor) {
        
        // чтобы не моргал цвет стрелочки при отмене анимации перехода назад на список,
        // надо при переходе ставить цвет состояния списка как у чата
        // нам не важно какой он на данной странице, ведь navigation bar все равно спрятан
        
        backButtonTintColor = color
    }
    
    func showNetworkErrorStatus() {
        chatTitleView.startSubtitleAnimation(with: StringsHelper.networkErrorTexts)
    }
    
    func hideNetworkErrorStatus() {
        chatTitleView.stopSubtitleAnimation()
    }
    
    //MARK: - UITableViewDelegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: false)
        
        let dataStructure = tableViewDataDisplayManager.dataStructure
        guard let cellObject = dataStructure.cellObject(at: indexPath) as? CellObjectWithId else {
            return
        }
        
        output.didSelectItem(with: cellObject.itemId)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        refreshNavigationBarState()
    }
    
    //MARK: - ParallaxScrollHeaderControllerDelegate
    
    func parallaxHeaderDidScroll(_ headerController: ParallaxScrollHeaderController) {
        
        var progress = headerController.progress - headerController.progressDefaultValue
        progress -= overscrollBeginOffset
        progress *= overscrollSpeedMultiplier
        progress = progress.clamped(to: overscrollProgressInterval)
        
        output.userDidOverscroll(with: progress)
    }
    
    //MARK: - UINavigationBarDelegate
    
    func position(for bar: UIBarPositioning) -> UIBarPosition {
        return .topAttached
    }
}

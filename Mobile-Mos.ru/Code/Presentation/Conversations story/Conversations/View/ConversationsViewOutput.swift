//
//  ConversationsViewOutput.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 10.02.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol ConversationsViewOutput: class {
    
    func setupInitialState()
    
    func didSelectItem(with itemId: String)
    
    func userDidOverscroll(with progress: CGFloat)
}

//
//  ConversationsRouter.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 10.02.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import UIKit

class ConversationsRouter: ConversationsRouterInput {
    
    weak var parentViewController: UIViewController!
    
    var embeddableHeaderController: ParallaxScrollHeaderController!
    var bannerModuleLoader: BaseModuleLoader!
    
    var alertsFactory: CommonAlertsFactory!
    
    let childSegueIdentifier = "showChat"
    let childMapsSegueIdentifier = "showChatMap"
    let userProfileSegueIdentifier = "userProfile"
    let authorizationSeugeIdentifier = "authorization"
    
    let servicesAndAppsIdentifier = "servicesAndApps"
    let aboutAppIdentifier = "aboutApp"
    let rulesIdentifier = "rules"
    let supportSegueIdentifier = "support"
    
    weak var transitionHandler: UIViewController!
    weak var toOtherModulesTransitionHandler: DataTransferModuleController!
    weak var alertTransitionHandler: UIViewController!
    
    //MARK: - ConversationsRouterInput
    
    func embedBannerModule(with eventHandler: FeaturedBannerEventHandler?) -> AnimatableView? {
        
        let viewController = bannerModuleLoader.loadAndConfigureModule()
        
        if let configurableController = viewController as? ConfigurableModuleController {
            configurableController.configureModule(with: eventHandler as Any)
        }
        
        viewController.willMove(toParentViewController: parentViewController)
        parentViewController.addChildViewController(viewController)
        viewController.didMove(toParentViewController: parentViewController)
        
        embeddableHeaderController.headerView = viewController.view
        
        return viewController as? AnimatableView
    }
    
    func showDialog(for conversation: Conversation) {
        toOtherModulesTransitionHandler.performSegue(with: childSegueIdentifier, sender: nil) { configurableController in
            let configuration = ChatModuleConfiguration(conversation: conversation, message: nil)
            configurableController.configureModule(with: configuration)
        }
    }
    
    func showMapsDialog() {
        toOtherModulesTransitionHandler.performSegue(with: childMapsSegueIdentifier, sender: nil, configurationClosure: nil)
    }
    
    func showUserProfile(with userProfileEventHandler: UserProfileEventHandler?) {
        toOtherModulesTransitionHandler.performSegue(with: userProfileSegueIdentifier, sender: nil) { (configurableController) in
            configurableController.configureModule(with: userProfileEventHandler as Any)
        }
    }
    
    func showAuthorizationModule(with handler: AuthorizationProcessHandler?) {
        toOtherModulesTransitionHandler.performSegue(with: authorizationSeugeIdentifier, sender: nil) {  configurableController in
            configurableController.configureModule(with: handler as Any)
        }
    }
    
    func showAboutAppInfo() {
        transitionHandler.performSegue(withIdentifier: aboutAppIdentifier, sender: nil)
    }
    
    func showAgreementModule() {
        transitionHandler.performSegue(withIdentifier: rulesIdentifier, sender: nil)
    }
    
    func showServicesAndAppsModule() {
        transitionHandler.performSegue(withIdentifier: servicesAndAppsIdentifier, sender: nil)
    }
    
    func showSupportModule() {
        transitionHandler.performSegue(withIdentifier: supportSegueIdentifier, sender: nil)
    }
}

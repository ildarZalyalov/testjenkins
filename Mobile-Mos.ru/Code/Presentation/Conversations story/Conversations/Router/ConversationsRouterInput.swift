//
//  ConversationsRouterInput.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 10.02.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol ConversationsRouterInput: class {
    
    func embedBannerModule(with eventHandler: FeaturedBannerEventHandler?) -> AnimatableView?
    
    func showDialog(for conversation: Conversation)
    
    func showMapsDialog()
    
    func showUserProfile(with userProfileEventHandler: UserProfileEventHandler?)
    
    func showAuthorizationModule(with handler: AuthorizationProcessHandler?)
    
    func showAboutAppInfo()
    
    func showAgreementModule()
    
    func showServicesAndAppsModule()
    
    func showSupportModule()
}

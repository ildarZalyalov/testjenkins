//
//  StoryboardFactoryImplementation.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 07.03.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class StoryboardFactoryImplementation: StoryboardFactory {
    
    func getStoryboard(with name: AppStoryboard) -> StoryboardDataSourceConfiguration {
        
        let storyboard = UIStoryboard(name: name.rawValue, bundle: nil)
        
        return (storyboard)
    }
}

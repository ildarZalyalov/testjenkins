//
//  Storyboard factory.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 07.03.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Перечисление доступных в приложении storyboard
enum AppStoryboard: String {
    case map             = "CityMapStoryboard"
    case mapitemInfo     = "MapItemInfoStoryboard"
    case conversations   = "ConversationsStoryboard"
    case userProfile     = "UserProfileStoryboard"
    case aboutApp        = "AboutAppStoryboard"
    case authorization   = "AuthorizationStoryboard"
    case payment         = "PaymentStoryboard"
    case onboarding      = "OnboardingStoryboard"
    case ctMosRu         = "CTMosRuStoryboard"
}

/// Перечисление контроллеров, которые можно загружать из storyboard
enum AppModule: String {
    case conversationList = "ConversationsViewController"
    case chat = "ChatViewController"
    case mapItemPanorama = "MapItemPanoramaController"
    case paymentProcessing = "PaymentProcessingViewController"
    case paymentMethodsProcessing = "PaymentMethodsProcessingViewController"
    case overlapItemInfo = "OverlapDetailInfoViewController"
    case userDataProcessing = "UserDataProcessingViewController"
    case ctMosRuApplicationProcessing = "CTMosRuApplicationProcessing"
    case ctMosRuApplicationSendSuccess = "CTMosRuApplicationSendingSuccess"
}

typealias StoryboardDataSourceConfiguration = (UIStoryboard)

/// Фабрика storyboard
protocol StoryboardFactory {
    
    /// Получить storyboard по перечислению AppStoryboard
    ///
    /// - Parameter name: тип storyboard
    func getStoryboard(with name: AppStoryboard) -> StoryboardDataSourceConfiguration
}

extension UIStoryboard {
    
    /// Создать контроллер по перечислению AppModule
    ///
    /// - Parameter module: тип контроллера
    func instantiateViewController(for module: AppModule) -> UIViewController {
        return instantiateViewController(withIdentifier: module.rawValue)
    }
}

//
//  MapPinIconConfigurator.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 21.04.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class MapPinIconConfigurator: MapPinIconConfiguratorProtocol {
    /// Длительность анимаций пинов на карте
    fileprivate let pinAnimationDuration:Float = 0.2
    
    func configure(with placeMapObject: YMKPlacemarkMapObject, iconStyle: YMKIconStyle, icon: UIImage) {
        placeMapObject.setIconWith(icon, style: iconStyle)
    }
}

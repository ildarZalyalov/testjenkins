//
//  MapPinIconConfiguratorProtocol.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 21.04.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Протокол для конфигурации иконки YMKPlacemarkObject объекта
protocol MapPinIconConfiguratorProtocol: class {
    
    
    /// Метод для конфигурации YMKPlacemarkMapObject
    ///
    /// - Parameters:
    ///   - placeMapObject: YMKPlacemarkMapObject объект отображающий на YandexMap
    ///   - iconStyle: YMKIconStyle объект отвечающий за стиль отображения точки на карте
    ///   - icon: UIImage изображение для объекта карты
    func configure (with placeMapObject: YMKPlacemarkMapObject, iconStyle: YMKIconStyle, icon: UIImage)
}

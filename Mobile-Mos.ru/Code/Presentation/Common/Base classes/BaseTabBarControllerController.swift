//
//  BaseTabBarControllerController.swift
//  Portable
//
//  Created by Ivan Erasov on 15.05.17.
//  Copyright © 2017. All rights reserved.
//

import Foundation

/// Базовый UITabBarController
class BaseTabBarController: UITabBarController {
    
    /// Возвращает корневой таб бар контроллер, если таковой присутствует
    static var rootTabBarController: BaseTabBarController? {
        guard let window = UIApplication.shared.delegate?.window else { return nil }
        return window?.rootViewController as? BaseTabBarController
    }
    
    /// Обеспечивает управление текущей ориентацией, видом статус бара выбранному контроллеру
    
    override var shouldAutorotate: Bool {
        return selectedViewController?.shouldAutorotate ?? super.shouldAutorotate
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return selectedViewController?.supportedInterfaceOrientations ?? super.supportedInterfaceOrientations
    }
    
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return selectedViewController?.preferredInterfaceOrientationForPresentation ?? super.preferredInterfaceOrientationForPresentation
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return selectedViewController?.preferredStatusBarStyle ?? super.preferredStatusBarStyle
    }
    
    override var prefersStatusBarHidden: Bool {
        return selectedViewController?.prefersStatusBarHidden ?? super.prefersStatusBarHidden
    }
    
    override var preferredStatusBarUpdateAnimation: UIStatusBarAnimation {
        return selectedViewController?.preferredStatusBarUpdateAnimation ?? super.preferredStatusBarUpdateAnimation
    }
}

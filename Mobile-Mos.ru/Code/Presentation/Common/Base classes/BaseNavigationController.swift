//
//  BaseNavigationController.swift
//  Portable
//
//  Created by Ivan Erasov on 03.02.17.
//  Copyright © 2017. All rights reserved.
//

import UIKit

/// Базовый UINavigationController
class BaseNavigationController: UINavigationController {
    
    /// Обеспечивает управление текущей ориентацией, видом статус бара самому верхнему контроллеру в стеке
    
    override var shouldAutorotate: Bool {
        return topViewController?.shouldAutorotate ?? super.shouldAutorotate
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return topViewController?.supportedInterfaceOrientations ?? super.supportedInterfaceOrientations
    }
    
    /// Дублирует childViewControllerForStatusBarStyle т.к. на iOS 10 последний не работает
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return topViewController?.preferredStatusBarStyle ?? super.preferredStatusBarStyle
    }
    
    /// Дублирует childViewControllerForStatusBarHidden т.к. на iOS 10 последний не работает
    override var prefersStatusBarHidden: Bool {
        return topViewController?.prefersStatusBarHidden ?? super.prefersStatusBarHidden
    }
    
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return topViewController?.preferredInterfaceOrientationForPresentation ?? super.preferredInterfaceOrientationForPresentation
    }
    
    override var preferredStatusBarUpdateAnimation: UIStatusBarAnimation {
        return topViewController?.preferredStatusBarUpdateAnimation ?? super.preferredStatusBarUpdateAnimation
    }
    
    override var childViewControllerForStatusBarStyle: UIViewController? {
        return topViewController
    }
    
    override var childViewControllerForStatusBarHidden: UIViewController? {
        return topViewController
    }
    
    override func childViewControllerForHomeIndicatorAutoHidden() -> UIViewController? {
        return topViewController
    }
    
    override func childViewControllerForScreenEdgesDeferringSystemGestures() -> UIViewController? {
        return topViewController
    }
}

//
//  BaseModuleConfigurator.swift
//  Portable
//
//  Created by Ivan Erasov on 31.01.17.
//  Copyright © 2017. All rights reserved.
//

import Foundation
import UIKit

/// Базовый класс для конфигураторов модулей, обеспечивает вызов методов конфигурации при инициализации конфигуратора в storyboard
class BaseModuleConfigurator: NSObject {
    
    @IBOutlet weak var viewController: UIViewController!
    
    override func awakeFromNib() {
        configureModule(for: viewController)
        finishModuleConfiguration(for: viewController)
    }
    
    /// Сконфигурировать модуль. В конкретных контроллерах переопределяется.
    ///
    /// - Parameter view: контроллер, для которого надо сконфигурировать модуль
    func configureModule(for view: UIViewController) {
    }
    
    /// Закончить конфигурацию модуля для контроллера
    ///
    /// - Parameter view: контроллер, для которого сконфигурировали модуль
    func finishModuleConfiguration(for view: UIViewController) {
        view.didFinishModuleConfiguration()
    }
}

extension UIViewController {
    
    /// Коллбэк о том, что конфигурация модуля завершена. В конкретных контроллерах можно переопределить и что-то сделать в этот момент.
    func didFinishModuleConfiguration() {
        
    }
}

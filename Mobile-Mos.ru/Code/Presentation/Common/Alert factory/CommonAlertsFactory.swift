//
//  AlertFactory.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 08.07.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Фабрика алертов и action sheet, используемых на нескольких экранах
protocol CommonAlertsFactory {
    
    /// Получить алерт с ошибкой
    ///
    /// - Parameters:
    ///   - body: сообщение с ошибкой
    /// - Returns: алерт с ошибкой
    func getErrorAlert(with body: String) -> UIViewController
    
    /// Получить алерт с ошибкой
    ///
    /// - Parameters:
    ///   - title: заголовок
    ///   - body: сообщение с ошибкой
    /// - Returns: алерт с ошибкой
    func getErrorAlert(with title: String?, body: String) -> UIViewController
    
    /// Получить алерт с ошибкой
    ///
    /// - Parameters:
    ///   - title: заголовок
    ///   - body: сообщение с ошибкой
    ///   - closeHandler: обработчик закрытия алерта
    /// - Returns: алерт с ошибкой
    func getErrorAlert(with title: String?,
                       body: String,
                       closeHandler: ((UIAlertAction) -> ())?) -> UIViewController
    
    /// Получить алерт с сообщением
    ///
    /// - Parameters:
    ///   - title: заголовок
    ///   - body: сообщение
    /// - Returns: алерт с сообщением
    func getOkAlert(with title: String?, body: String) -> UIViewController
    
    /// Получить алерт с сообщением о том, что использование геолокационных сервисов запрещено пользователем
    func getLocationServicesNotAuthorizedAlert() -> UIViewController
    
    /// Получить алерт с сообщением о том, что использование календаря запрещено пользователем
    func getCalendarServicesNotAuthorizedAlert() -> UIViewController
    
    /// Получить алерт с сообщением о том, что использование уведомлений запрещено пользователем
    func getUserNotificationsNotAuthorizedAlert() -> UIViewController
    
    /// Получить алерт с сообщением о том, что не удалось определить местоположение пользователя
    func getFailedToGetUserLocationAlert() -> UIViewController
    
    /// Получить алерт, сообщающий о доступном обновлении приложения
    ///
    /// - Parameters:
    ///   - version: новая версия приложения
    ///   - releaseNotes: информация о новой версии (что нового)
    /// - Returns: алерт об обновлении приложения
    func getNewApplicationVersionAlert(with version: String, and releaseNotes: String) -> UIViewController
    
    /// Получить action sheet с выбором приложения-навигатора для прокладки маршрута
    ///
    /// - Parameters:
    ///   - startLocation: начальная точка маршрута
    ///   - endLocation: конечная точка маршрута
    /// - Returns: action sheet с выбором приложения-навигатора
    func getCreateRouteActionSheet(for startLocation: Location, and endLocation: Location) -> UIViewController
}

//
//  AlertFactoryImplementation.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 08.07.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class CommonAlertsFactoryImplementation: CommonAlertsFactory {
    
    let errorTitle = "Ошибка 😕"
    
    //MARK: - CommonAlertsFactory
    
    func getErrorAlert(with body: String) -> UIViewController {
        
        let alert = UIAlertController(title: errorTitle, message: body, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        
        return alert
    }
    
    func getErrorAlert(with title: String? , body: String) -> UIViewController {
        
        let titleString = title ?? errorTitle
        let alert = UIAlertController(title: titleString, message: body, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        
        return alert
    }
    
    func getErrorAlert(with title: String?,
                       body: String,
                       closeHandler: ((UIAlertAction) -> ())?) -> UIViewController {
        
        let titleString = title ?? errorTitle
        let alert = UIAlertController(title: titleString, message: body, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: closeHandler))
        
        return alert
    }
    
    func getOkAlert(with title: String? = nil, body: String) -> UIViewController {
        
        let alert = UIAlertController(title: title, message: body, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        
        return alert
    }
    
    func getLocationServicesNotAuthorizedAlert() -> UIViewController {
        
        let alert = UIAlertController(title: "Доступ к геолокации",
                                      message: "Разрешить отслеживать локацию.\nСлужбы геолокации -> Моя Москва",
                                      preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Не сейчас", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Включить геолокацию", style: .cancel, handler: { (_) -> Void in
            if let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) {
                UIApplication.shared.open(url: settingsUrl)
            }
        }))
        
        return alert
    }
    
    func getCalendarServicesNotAuthorizedAlert() -> UIViewController {
        
        let alert = UIAlertController(title: "Доступ к календарю",
                                      message: "Разрешите приложению добавлять события в календарь на устройстве. \nКалендари -> Моя Москва",
                                      preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Не сейчас", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Разрешить", style: .cancel, handler: { (_) -> Void in
            if let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) {
                UIApplication.shared.open(url: settingsUrl)
            }
        }))
        
        return alert
    }
    
    func getUserNotificationsNotAuthorizedAlert() -> UIViewController {
        
        let alert = UIAlertController(title: "Уведомления отключены",
                                      message: "Разрешите приложению отправлять вам уведомления. \nУведомления -> Моя Москва",
                                      preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Не сейчас", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Включить уведомления", style: .cancel, handler: { (_) -> Void in
            if let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) {
                UIApplication.shared.open(url: settingsUrl)
            }
        }))
        
        return alert
    }
    
    func getFailedToGetUserLocationAlert() -> UIViewController {
        return getErrorAlert(with: "Не получилось обнаружить ваше местоположение")
    }
    
    func getNewApplicationVersionAlert(with version: String, and releaseNotes: String) -> UIViewController {
        
        let titlePrefix = "Доступна новая версия приложения"
        let title = titlePrefix + " " + version
        let message = "\n" + releaseNotes
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Позже", style: .default, handler: nil))
        alert.addAction(UIAlertAction(title: "Обновить", style: .default, handler: { _ in
            if UIApplication.shared.canOpenURL(applicationUrl) {
                UIApplication.shared.open(url: applicationUrl)
            }
        }))
        
        return alert
    }
    
    func getCreateRouteActionSheet(for startLocation: Location, and endLocation: Location) -> UIViewController {
        
        let actionSheet = UIAlertController(title: "Выберите приложение", message: nil, preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: yandexNavigatorAppName, style: .default, handler: { (_) -> Void in
            
            guard let url = yandexNavigatorAppRouteUrl(from: startLocation, to: endLocation) else { return }
            guard let appStoreUrl = yandexNavigatorAppStoreUrl else { return }
            guard UIApplication.shared.canOpenURL(url) else {
                UIApplication.shared.open(url: appStoreUrl)
                return
            }
            
            UIApplication.shared.open(url: url)
            
        }))
        actionSheet.addAction(UIAlertAction(title: yandexMapsAppName, style: .default, handler: { (_) -> Void in
            
            guard let url = yandexMapsAppRouteUrl(from: startLocation, to: endLocation) else { return }
            guard let appStoreUrl = yandexMapsAppStoreUrl else { return }
            guard UIApplication.shared.canOpenURL(url) else {
                UIApplication.shared.open(url: appStoreUrl)
                return
            }
            
            UIApplication.shared.open(url: url)
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Отменить", style: .cancel, handler: nil))
        
        return actionSheet
    }
}

//
//  YMKMapLogoTypeSelector.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 13.04.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Класс, который выбирает лого Яндекса в зависимости от настроек карты и телефона
class YMKMapLogoTypeSelector {
    
    /// Тип лого
    ///
    /// - coloredRu: цветное русское
    /// - coloredEn: цветное английское
    /// - darkRu: темное русское
    /// - darkEn: темное английское
    enum LogoType: String {
        case coloredRu = "yandexLogoColorRu"
        case coloredEn = "yandexLogoColorEn"
        case darkRu = "yandexLogoRu"
        case darkEn = "yandexLogoEn"
    }
    
    /// Текущая настройка языка в телефоне (список языков в порядке очередности, начиная с текущего)
    var preferredLanguages: [String]!
    
    /// Вернуть тип лого для карты
    ///
    /// - Parameter map: карта
    /// - Returns: тип лого
    func logoType(for map: YMKMap) -> LogoType {
        
        let darkMapTypes: Set<YMKMapType> = [.satellite, .hybrid]
        let isMapDark = map.isNightModeEnabled || darkMapTypes.contains(map.mapType)
        let isRussainLanguage = preferredLanguages.first?.hasPrefix(localeIdentifierRussian) ?? false
        
        if isMapDark {
            return isRussainLanguage ? .darkRu : .darkEn
        } else {
            return isRussainLanguage ? .coloredRu : .coloredEn
        }
    }
}

//
//  AutolayoutHelpers.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 20.03.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import UIKit

///Помогает использовать упрощенные методы для работы с Visual Constraints
extension NSLayoutConstraint {
    
    /// Возвращает NSLayoutConstraint заданные через String
    ///
    /// - Parameters:
    ///   - format:  формат Visual Constraint-ов
    ///   - options: опции формата если нужны, иначе дефолтные
    ///   - metrics: словарь с метриками если нужно, иначе nil
    ///   - views:   словарь со строковым представлением Views
    static public func visualConstraints (with
        format  : String,
        options : NSLayoutFormatOptions = NSLayoutFormatOptions(),
        metrics : [String : AnyObject]? = nil,
        views   : [String : AnyObject]) -> [NSLayoutConstraint]
    {
        let constraints = NSLayoutConstraint.constraints(withVisualFormat: format, options: options, metrics: metrics, views: views)
        return constraints
    }
    
    /// Возвращает NSLayoutConstraints заданные слева и справа относительно родительской view
    ///
    /// - Parameters:
    ///   - view: UIView, к которому применяются констрейны
    ///   - leading: отступ слева
    ///   - trailing: отступ справа
    static public func leadingTrailingEdgesConstraints(for view: UIView, leading: CGFloat = 0, trailing: CGFloat = 0) -> [NSLayoutConstraint] {
        
        let dict = ["view": view]
        let metrics = ["LEFT": leading, "RIGHT": trailing]
        
        return visualConstraints(with: "H:|-LEFT-[view]-RIGHT-|", options: [], metrics: metrics as [String : AnyObject]?, views: dict)
    }
    
    /// Возвращает NSLayoutConstraints заданные сверху и снизу относительно родительской view
    ///
    /// - Parameters:
    ///   - view: UIView, к которому применяются констрейны
    ///   - top: отступ сверху
    ///   - bottom: отступ снизу
    static public func topBottomEdgesConstraints(for view: UIView, top: CGFloat = 0, bottom: CGFloat = 0) -> [NSLayoutConstraint] {
        
        let dict = ["view": view]
        let metrics = ["TOP": top, "BOTTOM": bottom]
        
        return visualConstraints(with: "V:|-TOP-[view]-BOTTOM-|", options: [], metrics: metrics as [String : AnyObject]?, views: dict)
    }
    
    /// Возвращает NSLayoutConstraints заданные отношением UIEdgeInsets
    ///
    /// - Parameters:
    ///   - view: UIView к которому применяется edges
    ///   - edges: UIEdgeInsets если нужно, иначе UIEdgeInsetsMake(0, 0, 0, 0)
    static public func edgesConstraints(for view: UIView, with edges: UIEdgeInsets = UIEdgeInsets.zero) -> [NSLayoutConstraint] {
        
        var constraints: [NSLayoutConstraint] = []
        
        constraints += leadingTrailingEdgesConstraints(for: view, leading: edges.left, trailing: edges.right)
        constraints += topBottomEdgesConstraints(for: view, top: edges.top, bottom: edges.bottom)
        
        return constraints
    }
    
    /// Возвращает NSLayoutConstraints для Width&Heights из CGSize
    ///
    /// - Parameters:
    ///   - view: UIView который нужно растянуть
    ///   - size: CGSize изначальный размер UIView
    static public func sizeConstraints (for view:UIView,with size:CGSize) -> [NSLayoutConstraint] {
        let dict = ["view":view]
        let metrics = ["WIDTH":size.width, "HEIGHT":size.height]
        
        var constraints:[NSLayoutConstraint] = []
        
        constraints += visualConstraints(with: "H:[view(WIDTH)]", options: [], metrics: metrics as [String : AnyObject]?, views: dict)
        constraints += visualConstraints(with: "V:[view(HEIGHT)]", options: [], metrics: metrics as [String : AnyObject]?, views: dict)
        
        return constraints
    }
    
    /// Возвращает NSLayoutConstraints выставляющие UIView по центру его SuperView
    ///
    /// - Parameters:
    ///   - view: UIView который нужно поставить по центру
    ///   - centerOffset: СGPoint сдвиг относительно центра если нужно, иначе CGPoint.zero
    static public func centerConstraints (for view: UIView,with centerOffset: CGPoint = CGPoint.zero) -> [NSLayoutConstraint] {
        return [
            NSLayoutConstraint(item: view, attribute: .centerX, relatedBy: .equal, toItem: view.superview!, attribute: .centerX, multiplier: 1, constant: centerOffset.x),
            NSLayoutConstraint(item: view, attribute: .centerY, relatedBy: .equal, toItem: view.superview!, attribute: .centerY, multiplier: 1, constant: centerOffset.y)
        ]
    }
    
    /// Возвращает NSLayoutConstraint центр по Y для UIView со сдвигом или без (offset)
    ///
    /// - Parameters:
    ///   - view: UIView который нужно выставить центр по Y координате
    ///   - offset: CGFloat сдвиг по Y если нужно, иначе CGFloat = 0
    static public func centerYConstraint (for view: UIView,with offset: CGFloat = 0) -> NSLayoutConstraint {
        return NSLayoutConstraint(item: view, attribute: .centerY, relatedBy: .equal, toItem: view.superview, attribute: .centerY, multiplier: 1, constant: offset)
    }
    
    /// Возвращает NSLayoutConstraint центр по X для UIView со сдвигом или без (offset)
    ///
    /// - Parameters:
    ///   - view: UIView который нужно выставить центр по X координате
    ///   - offset: CGFloat сдвиг по X если нужно, иначе CGFloat = 0
    static public func centerXConstraint (for view: UIView,with offset: CGFloat = 0) -> NSLayoutConstraint {
        return NSLayoutConstraint(item: view, attribute: .centerX, relatedBy: .equal, toItem: view.superview, attribute: .centerX, multiplier: 1, constant: offset)
    }
    
    /// Возвращает NSLayoutConstraint Width пропорционального отношения firstView и secondView с множителем(multipler)
    ///
    /// - Parameters:
    ///   - firstView:  UIView которое будет пропорциональным относительно secondView
    ///   - secondView: UIView которое будет пропорциональным относительно firstView
    ///   - multiplier: CGFloat множитель, который будет применен к width
    ///   - constant:   CGFloat константа, которая будет добавлена к multiplier
    static public func proportionalWidthConstraint (for firstView: UIView, secondView: UIView, multiplier: CGFloat = 1.0, constant: CGFloat = 0) -> NSLayoutConstraint {
        return NSLayoutConstraint(item: firstView, attribute: .width, relatedBy: .equal, toItem: secondView, attribute: .width, multiplier: multiplier, constant: constant)
    }
    
    /// Возвращает NSLayoutConstraint Height пропорционального отношения firstView и secondView с множителем(multipler)
    ///
    /// - Parameters:
    ///   - firstView:  UIView которое будет пропорциональным относительно secondView
    ///   - secondView: UIView которое будет пропорциональным относительно firstView
    ///   - multiplier: CGFloat множитель, который будет применен к height
    ///   - constant:   CGFloat константа, которая будет добавлена к multiplier
    static public func proportionalHeightConstraint (for firstView: UIView, secondView: UIView, multiplier: CGFloat = 1.0, constant: CGFloat = 0) -> NSLayoutConstraint {
        return NSLayoutConstraint(item: firstView, attribute: .height, relatedBy: .equal, toItem: secondView, attribute: .height, multiplier: multiplier, constant: constant)
    }
    
    /// Возвращает NSLayoutConstraint пропорционального отношения firstView.height и secondView.width с множителем(multipler)
    ///
    /// - Parameters:
    ///   - firstView:  UIView которое будет пропорциональным относительно secondView
    ///   - secondView: UIView которое будет пропорциональным относительно firstView
    ///   - multiplier: CGFloat множитель, который будет применен к height и width
    ///   - constant:   CGFloat константа, которая будет добавлена к multiplier
    static public func proportionalHeightToWidthConstraint (for firstView: UIView, secondView: UIView, multiplier: CGFloat = 1.0, constant: CGFloat = 0) -> NSLayoutConstraint {
        return NSLayoutConstraint(item: firstView, attribute: .height, relatedBy: .equal, toItem: secondView, attribute: .width, multiplier: multiplier, constant: constant)
    }
    
    /// Возвращает NSLayoutConstraint пропорционального отношения firstView.height и secondView.width с множителем(multipler)
    ///
    /// - Parameters:
    ///   - firstView:  UIView которое будет пропорциональным относительно secondView
    ///   - secondView: UIView которое будет пропорциональным относительно firstView
    ///   - multiplier: CGFloat множитель, который будет применен к witdh и height
    ///   - constant:   CGFloat константа, которая будет добавлена к multiplier
    static public func proportionalWidthToHeightConstraint (for firstView: UIView, secondView: UIView, multiplier: CGFloat = 1.0, constant: CGFloat = 0) -> NSLayoutConstraint {
        return NSLayoutConstraint(item: firstView, attribute: .width, relatedBy: .equal, toItem: secondView, attribute: .height, multiplier: multiplier, constant: constant)
    }
}

/// UIScrollView с intrinsicContentSize первой view ее контента
class SizableSingleContentViewScrollView: UIScrollView {
    
    override var intrinsicContentSize: CGSize {
        guard let contentSubview = subviews.first else { return super.intrinsicContentSize }
        return contentSubview.systemLayoutSizeFitting(UILayoutFittingCompressedSize)
    }
    
    /// Должна ли UIScrollView "держать скролл внизу" при изменении высоты
    var shouldScrollToBottomOnHeightChange = true
    
    /// Последняя высота, для которой делали "скролл вниз"
    fileprivate var lastHeightOnWhichScrolledToBottom: CGFloat = 0
    
    override func layoutSubviews() {
        
        super.layoutSubviews()
        
        guard shouldScrollToBottomOnHeightChange else { return }
        guard lastHeightOnWhichScrolledToBottom != height else { return }
        
        lastHeightOnWhichScrolledToBottom = height
        
        scrollToBottom(animated: true)
    }
}

/// UICollectionView с intrinsicContentSize ее контента
class SizableCollectionView: UICollectionView {
    
    override var intrinsicContentSize: CGSize {
        return collectionViewLayout.collectionViewContentSize
    }
}

extension UIViewController {
    
    /// Safe area insets с обратной совместимостью на ОС младше 11-й версии
    var backwardCompatibleSafeAreaInsets: UIEdgeInsets {
        if #available(iOS 11.0, *) {
            return view.safeAreaInsets
        }
        else {
            return UIEdgeInsets(top: topLayoutGuide.length, left: 0, bottom: bottomLayoutGuide.length, right: 0)
        }
    }
    
    /// Добавить констрейн к верху view, учитывая отступы safe area
    ///
    /// - Parameters:
    ///   - subview: дочерняя вьюха
    ///   - attribute: атрибут констрейна
    ///   - relation: отношение атрибута к верху view
    func pinToTopLayoutGuide(subview: UIView,
                             attribute: NSLayoutAttribute,
                             relatedBy relation: NSLayoutRelation = .equal) {
        if #available(iOS 11, *) {
            view.addConstraint(NSLayoutConstraint(item: subview, attribute: attribute, relatedBy: relation, toItem: view.safeAreaLayoutGuide, attribute: .top, multiplier: 1, constant: 0))
        }
        else {
            view.addConstraint(NSLayoutConstraint(item: subview, attribute: attribute, relatedBy: relation, toItem: topLayoutGuide, attribute: .bottom, multiplier: 1, constant: 0))
        }
    }
    
    /// Добавить констрейн к низу view, учитывая отступы safe area
    ///
    /// - Parameters:
    ///   - subview: дочерняя вьюха
    ///   - attribute: атрибут констрейна
    ///   - relation: отношение атрибута к низу view
    func pinToBottomLayoutGuide(subview: UIView,
                                attribute: NSLayoutAttribute,
                                relatedBy relation: NSLayoutRelation = .equal) {
        if #available(iOS 11, *) {
            view.addConstraint(NSLayoutConstraint(item: subview, attribute: attribute, relatedBy: relation, toItem: view.safeAreaLayoutGuide, attribute: .bottom, multiplier: 1, constant: 0))
        }
        else {
            view.addConstraint(NSLayoutConstraint(item: subview, attribute: attribute, relatedBy: relation, toItem: bottomLayoutGuide, attribute: .top, multiplier: 1, constant: 0))
        }
    }
}

//
//  EmptyTableFooterView.swift
//  Portable
//
//  Created by Ivan Erasov on 14.02.2018.
//  Copyright © 2018. All rights reserved.
//

import Foundation

/// Cell object пустого футера
struct EmptyTableFooterObject: FooterObject {
    
    /// Высота футера
    var footerHeight: CGFloat = CGFloat.leastNormalMagnitude
}

/// View пустого футера
class EmptyTableFooterView: UITableViewHeaderFooterView, ConfigurableView {
    
    fileprivate var footerHeight: CGFloat = CGFloat.leastNormalMagnitude
    
    func configure(with object: Any) {
        guard let cellObject = object as? EmptyTableFooterObject else { return }
        footerHeight = cellObject.footerHeight
    }
    
    override func systemLayoutSizeFitting(_ targetSize: CGSize) -> CGSize {
        return CGSize(width: targetSize.width, height: footerHeight)
    }
    
    override func systemLayoutSizeFitting(_ targetSize: CGSize, withHorizontalFittingPriority horizontalFittingPriority: UILayoutPriority, verticalFittingPriority: UILayoutPriority) -> CGSize {
        return systemLayoutSizeFitting(targetSize)
    }
}

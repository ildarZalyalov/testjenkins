//
//  EmptyTableHeaderView.swift
//  Portable
//
//  Created by Ivan Erasov on 14.02.2018.
//  Copyright © 2018. All rights reserved.
//

import Foundation

/// Cell object пустого заголовка
struct EmptyTableHeaderObject: HeaderObject {
    
    /// Высота заголовка
    var headerHeight: CGFloat = CGFloat.leastNormalMagnitude
}

/// View пустого заголовка
class EmptyTableHeaderView: UITableViewHeaderFooterView, ConfigurableView {

    fileprivate var headerHeight: CGFloat = CGFloat.leastNormalMagnitude
    
    func configure(with object: Any) {
        guard let cellObject = object as? EmptyTableHeaderObject else { return }
        headerHeight = cellObject.headerHeight
    }
    
    override func systemLayoutSizeFitting(_ targetSize: CGSize) -> CGSize {
        return CGSize(width: targetSize.width, height: headerHeight)
    }
    
    override func systemLayoutSizeFitting(_ targetSize: CGSize, withHorizontalFittingPriority horizontalFittingPriority: UILayoutPriority, verticalFittingPriority: UILayoutPriority) -> CGSize {
        return systemLayoutSizeFitting(targetSize)
    }
}

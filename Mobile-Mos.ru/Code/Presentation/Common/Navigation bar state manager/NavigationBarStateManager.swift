//
//  NavigationBarStateManager.swift
//  Portable
//
//  Created by Ivan Erasov on 09.03.17.
//  Copyright © 2017. All rights reserved.
//

import Foundation
import UIKit

/// Менеджер управления состоянием UINavigationBar
class NavigationBarStateManager: NSObject, UINavigationControllerDelegate, UINavigationBarDelegate, UIScrollViewDelegate {
    
    /// View, которая содержит "стандартный фон"
    fileprivate lazy var backgroundView: UIView = {
        
        let frame = self.navigationController?.navigationBar.bounds ?? CGRect.zero
        
        let view = UIView(frame: frame)
        view.backgroundColor = UIColor.clear
        view.isUserInteractionEnabled = false
        
        self.regularStateView.translatesAutoresizingMaskIntoConstraints = false
        self.regularStateView.delegate = self
        view.addSubview(self.regularStateView)
    
        var constraints = [NSLayoutConstraint(item: self.regularStateView, attribute: .bottom, relatedBy: .equal, toItem: view, attribute: .bottom, multiplier: 1, constant: 0)]
        constraints += NSLayoutConstraint.leadingTrailingEdgesConstraints(for: self.regularStateView)
        view.addConstraints(constraints)
        
        return view
    }()
    
    /// Navigation bar, который отображается как "стандартный фон"
    fileprivate lazy var regularStateView = UINavigationBar()
    
    /// View, которая отображается как "кастомный фон"
    fileprivate weak var customStateView: UIView?
    
    /// View, которая отображается как текущий фон
    var currentNavigationBarBackgroundView: UIView {
        return customStateView ?? backgroundView
    }
    
    /// UINavigationController, с которым работает менеджер
    @IBOutlet weak var navigationController: UINavigationController? {
        
        didSet {
            
            oldValue?.delegate = nil
            backgroundView.removeFromSuperview()
            customStateView?.removeFromSuperview()
            
            guard let navigationController = navigationController else { return }
            
            navigationController.delegate = self
            makeNavigationBarTransparent(for: navigationController)
            addBackgroundView(to: navigationController)
            
            if let controller = navigationController.topViewController as? NavigationBarCustomizingController,
                let state = controller.navigationBarState {
                apply(state: state, animated: false)
            }
        }
    }
    
    //MARK: - Приватные методы
    
    /// Сделать UINavigationBar прозрачным
    ///
    /// - Parameter controller: контроллер, для которого делаем UINavigationBar прозрачным
    fileprivate func makeNavigationBarTransparent(for controller: UINavigationController) {
        controller.navigationBar.setBackgroundImage(UIImage(), for: .default)
        controller.navigationBar.shadowImage = UIImage()
        controller.navigationBar.isTranslucent = true
        controller.navigationBar.backgroundColor = UIColor.clear
    }
    
    /// Добавить NSLayoutConstraint для некоторой subview UINavigationController, чтобы она располагалась под UINavigationBar
    ///
    /// - Parameters:
    ///   - someView: subview UINavigationController
    ///   - controller: UINavigationController
    fileprivate func pin(view someView: UIView, to controller: UINavigationController) {
        
        let view = controller.view
        let navigationBar = controller.navigationBar
        
        let bottomConstraint = NSLayoutConstraint(item: someView, attribute: .bottom, relatedBy: .equal, toItem: navigationBar, attribute: .bottom, multiplier: 1, constant: 0)
        bottomConstraint.priority = UILayoutPriority.defaultHigh
        
        let constraints = [
            NSLayoutConstraint(item: someView, attribute: .left, relatedBy: .equal, toItem: view, attribute: .left, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: someView, attribute: .right, relatedBy: .equal, toItem: view, attribute: .right, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: someView, attribute: .top, relatedBy: .equal, toItem: view, attribute: .top, multiplier: 1, constant: 0),
            bottomConstraint
        ]
        
        view?.addConstraints(constraints)
    }
    
    /// Ограничить высоту для некоторой subview UINavigationController ее текущей высотой
    ///
    /// - Parameters:
    ///   - someView: subview UINavigationController
    ///   - controller: UINavigationController
    fileprivate func limitHeight(of someView: UIView, on controller: UINavigationController) {
        let view = controller.view
        view?.addConstraint(NSLayoutConstraint(item: someView, attribute: .height, relatedBy: .lessThanOrEqual, toItem: nil, attribute: .height, multiplier: 1, constant: someView.height))
    }
    
    /// Добавить "стандартный фон" на UINavigationController
    ///
    /// - Parameter controller: UINavigationController, на который добавляем фон
    fileprivate func addBackgroundView(to controller: UINavigationController) {
        backgroundView.translatesAutoresizingMaskIntoConstraints = false
        controller.view?.insertSubview(backgroundView, belowSubview: controller.navigationBar)
        pin(view: backgroundView, to: controller)
    }
    
    /// Добавить "кастомный фон" на UINavigationController
    ///
    /// - Parameters:
    ///   - customView: view, которая будет отображаться как "кастомный фон"
    ///   - controller: UINavigationController, на который добавляем фон
    fileprivate func add(customView: UIView, to controller: UINavigationController) {
        customView.translatesAutoresizingMaskIntoConstraints = false
        controller.view?.insertSubview(customView, aboveSubview: backgroundView)
        pin(view: customView, to: controller)
    }
    
    /// Проиграть неинтерактивные анимации перехода (ловим начало анимации в координаторе, дальше запускаем свою анимацию с теми же параметрами; тем самым обходим баг iOS 11, где первый переход по факту не анимируется + гарантируем контроль над анимацией)
    ///
    /// - Parameters:
    ///   - view: view, в которой надо проигрывать анимацию
    ///   - performer: проигрыватель анимаций
    ///   - coordinator: координатор перехода
    fileprivate func performNonInteractiveAnimations(in view: UIView, with performer: AnimationsPerformer, using coordinator: UIViewControllerTransitionCoordinator) {
    
        let animationsQueued = coordinator.animateAlongsideTransition(in: view, animation: { context in
            
            guard !performer.animationsStarted && !performer.animationsCompleted else { return }
            
            UIView.animate(withDuration: context.transitionDuration,
                           delay: 0,
                           options: [.overrideInheritedDuration, context.animationCurveOption],
                           animations: {
                performer.animate()
            }, completion: nil)
            
        }, completion: { _ in
            performer.complete()
        })
        
        if !animationsQueued {
            performer.animate()
            performer.complete()
        }
    }
    
    /// Проиграть интерактивные анимации перехода (делаем все по документации)
    ///
    /// - Parameters:
    ///   - view: view, в которой надо проигрывать анимацию
    ///   - performer: проигрыватель анимаций
    ///   - coordinator: координатор перехода
    fileprivate func performInteractiveAnimations(in view: UIView, with performer: AnimationsPerformer, using coordinator: UIViewControllerTransitionCoordinator) {
        
        let animationsQueued = coordinator.animateAlongsideTransition(in: view, animation: { context in
            
            guard !performer.animationsStarted && !performer.animationsCompleted else { return }
            
            performer.animate()
            
        }) { context in
            
            guard performer.animationsStarted && !performer.animationsCompleted else { return }
            
            if context.isCancelled {
                performer.reverse()
            }
            else {
                performer.complete()
            }
        }
        
        if !animationsQueued {
            performer.animate()
            performer.complete()
        }
    }
    
    //MARK: - Управление состоянием
    
    /// Анимация при изменении "кастомного фона"
    ///
    /// - Parameter state: состояние UINavigationBar
    /// - Returns: анимация или nil, если состояние для параметра не изменилось
    func customViewChangedAnimations(for state: NavigationBarState) -> StateChangeAnimations? {
        
        guard let navigationController = navigationController else {
            return nil
        }
        
        guard let backgroundState = state.backgroundState else {
            return nil
        }
        
        let viewToSwitchFrom = customStateView ?? backgroundView
        var shouldLimitHeight = false
        var viewToSwithTo: UIView
        
        switch backgroundState {
        case .custom(let customView):
            viewToSwithTo = customView
        case .customLimitedHeight(let customView):
            viewToSwithTo = customView
            shouldLimitHeight = true
        case .regular:
            viewToSwithTo = backgroundView
        }
        
        guard viewToSwitchFrom != viewToSwithTo else {
            return nil
        }
        
        if viewToSwithTo == backgroundView {
            viewToSwithTo.isHidden = false
        }
        else {
            viewToSwithTo.alpha = 0
            add(customView: viewToSwithTo, to: navigationController)
        }
        
        let animations = {
            viewToSwitchFrom.alpha = 0
            viewToSwithTo.alpha = 1
        }
        
        let reversion = { [weak self] in
            if viewToSwithTo != self?.backgroundView {
                viewToSwithTo.removeFromSuperview()
            }
        }
        
        let completion = { [weak self] in
            
            guard let strongSelf = self else { return }
            
            viewToSwitchFrom.isHidden = true
            
            if viewToSwitchFrom == strongSelf.customStateView {
                viewToSwitchFrom.removeFromSuperview()
            }
            
            if viewToSwithTo != strongSelf.backgroundView {
                
                strongSelf.customStateView = viewToSwithTo
                
                if shouldLimitHeight {
                    strongSelf.limitHeight(of: viewToSwithTo, on: navigationController)
                }
            }
        }
        
        return StateChangeAnimations(preparation: nil,
                                     animations: animations,
                                     reversion: reversion,
                                     completion: completion)
    }
    
    /// Применить состояние к UINavigationBar текущего UINavigationController
    ///
    /// - Parameter state: состояние UINavigationBar
    /// - Parameter animated: анимирован ли переход
    func apply(state: NavigationBarState, animated: Bool) {
        
        guard let navigation = navigationController else { return }
        
        var possibleAnimations = [StateChangeAnimations?]()
        possibleAnimations.append(tintColorChangedAnimations(for: navigation.navigationBar, with: state))
        possibleAnimations.append(titleTextAttributesChangedAnimations(for: navigation.navigationBar, with: state))
        possibleAnimations.append(largeTitleTextAttributesChangedAnimations(for: navigation.navigationBar, with: state))
        possibleAnimations.append(customViewChangedAnimations(for: state))
        possibleAnimations.append(hiddenChangedAnimations(for: regularStateView, with: state))
        possibleAnimations.append(translucentChangedAnimations(for: regularStateView, with: state))
        possibleAnimations.append(barTintColorChangedAnimations(for: regularStateView, with: state))
        possibleAnimations.append(shadowHiddenChangedAnimations(for: regularStateView, with: state))
        possibleAnimations.append(interactionEnabledChangedAnimations(for: navigation.navigationBar, with: state))
        
        let animations = possibleAnimations.compactMap { $0 }
        
        guard !animations.isEmpty else {
            return
        }
        
        let performer = AnimationsPerformer(animations: animations)
        performer.prepare()
        
        guard animated, let coordinator = navigation.transitionCoordinator else {
            performer.animate()
            performer.complete()
            return
        }
        
        let view: UIView = navigation.view
        
        if coordinator.initiallyInteractive {
            performInteractiveAnimations(in: view, with: performer, using: coordinator)
        }
        else {
            performNonInteractiveAnimations(in: view, with: performer, using: coordinator)
        }
    }
    
    //MARK: - UINavigationControllerDelegate
    
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        if let controller = viewController as? NavigationBarCustomizingController,
            let state = controller.navigationBarState {
            apply(state: state, animated: animated)
        }
    }
    
    func navigationController(_ navigationController: UINavigationController, didShow viewController: UIViewController, animated: Bool) {
        regularStateView.setNeedsLayout()
        regularStateView.layoutIfNeeded()
    }
    
    //MARK: - UINavigationBarDelegate
    
    func position(for bar: UIBarPositioning) -> UIBarPosition {
        return .topAttached
    }
}

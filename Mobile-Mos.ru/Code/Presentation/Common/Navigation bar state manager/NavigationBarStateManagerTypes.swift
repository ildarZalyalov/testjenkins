//
//  NavigationBarStateManagerTypes.swift
//  Portable
//
//  Created by Ivan Erasov on 25.07.17.
//  Copyright © 2017. All rights reserved.
//

import Foundation
import UIKit

/// Состояние фона UINavigationBar
///
/// - regular: стандартный фон, описываемый свойствами UINavigationBar
/// - custom: кастомный фон, представляющий из себя заранее настроенную UIView
/// - customLimitedHeight: кастомный фон, представляющий из себя заранее настроенную UIView c высотой, ограниченной значением на момент показа экрана
enum NavigationBarBackgroundState {
    
    case regular
    case custom(backgroundView: UIView)
    case customLimitedHeight(backgroundView: UIView)
    
    var isRegularState: Bool {
        switch self {
        case .regular:
            return true
        default:
            return false
        }
    }
}

/// Состояние UINavigationBar, инкапсулирует параметры, набор которых определяет внешний вид UINavigationBar
struct NavigationBarState {
    
    /// Скрыт ли UINavigationBar
    var isHidden = false
    
    /// Прозрачен ли UINavigationBar
    var isTranslucent = true
    
    /// Цвет оттенка UINavigationBar
    var barTintColor: UIColor?
    
    /// Спрятана ли тень UINavigationBar
    var isShadowHidden = false
    
    /// Цвет элементов внутри UINavigationBar
    var tintColor: UIColor?
    
    /// Параметры заголовка UINavigationBar (если nil, то не будут изменены)
    var titleTextAttributes: [NSAttributedStringKey : Any]?
    
    /// Параметры заголовка UINavigationBar для больших заголовков в iOS 11 (если nil, то не будут изменены)
    var largeTitleTextAttributes: [NSAttributedStringKey : Any]?
    
    /// Обрабатывает ли нажатия UINavigationBar или допускает нажатия на элементы на странице под ним
    var isUserInteractionEnabled = true
    
    /// Состояние фона UINavigationBar
    var backgroundState: NavigationBarBackgroundState?
}

/// Контроллер, который может менять состояние UINavigationBar.
/// Если NavigationBarStateManager работает с UINavigationController, 
/// и последний собирается показать такой контроллер, менеджер пытается получить
/// от него новое состояние для UINavigationBar.
protocol NavigationBarCustomizingController {
    
    /// Состояние UINavigationBar, которое следует установить (если есть)
    var navigationBarState: NavigationBarState? { get }
}

extension UINavigationController {
    
    /// Доступ к менеджеру в UINavigationController, если он задан
    var stateManager: NavigationBarStateManager? {
        return delegate as? NavigationBarStateManager
    }
}

extension UIViewControllerTransitionCoordinatorContext {
    
    /// Получить кривую анимации как опцию анимации
    var animationCurveOption: UIViewAnimationOptions {
        switch completionCurve {
        case .linear:
            return .curveLinear
        case .easeIn:
            return .curveEaseIn
        case .easeOut:
            return .curveEaseOut
        case .easeInOut:
            return .curveEaseInOut
        }
    }
}


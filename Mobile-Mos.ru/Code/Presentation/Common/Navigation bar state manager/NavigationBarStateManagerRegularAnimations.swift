//
//  NavigationBarStateManagerRegularAnimations.swift
//  Portable
//
//  Created by Ivan Erasov on 25.07.17.
//  Copyright © 2017. All rights reserved.
//

import Foundation

extension NavigationBarStateManager {
    
    /// Структура описывает шаги анимации при смене состояния UINavigationBar
    /// В конце анимации вызывается либо блок отката, либо блок завершения
    struct StateChangeAnimations {
        
        /// Подготовка - блок вызывается перед началом анимации
        var preparation: (() -> ())?
        
        /// Анимация - блок вызывается во время анимации
        var animations: (() -> ())?
        
        /// Откат - блок вызывается, если анимация была интерактивной и была отменена
        var reversion: (() -> ())?
        
        /// Завершение - блок вызывается после завершения анимации
        var completion: (() -> ())?
    }
    
    /// Проигрыватель анимаций при смене состояния UINavigationBar
    /// Предоставляет удобный интерфейс и позволяет отслеживать были запущены анимации
    class AnimationsPerformer {
        
        /// Анимации
        let animations: [StateChangeAnimations]
        
        /// Были ли запущены анимации
        var animationsStarted = false
        
        /// Были ли завершены анимации
        var animationsCompleted = false
        
        init(animations: [StateChangeAnimations]) {
            self.animations = animations
        }
        
        /// Подготовить анимации
        func prepare() {
            animations.forEach { $0.preparation?() }
        }
        
        /// Запустить анимации
        func animate() {
            animationsStarted = true
            animations.forEach { $0.animations?() }
        }
        
        /// Завершить анимации
        func complete() {
            animationsCompleted = true
            animations.forEach { $0.completion?() }
        }
        
        /// Откатить анимации
        func reverse() {
            animationsStarted = false
            animationsCompleted = false
            animations.forEach { $0.reversion?() }
        }
    }
    
    /// Анимация при скрытии/показе UINavigationBar
    ///
    /// - Parameters:
    ///   - navigationBar: UINavigationBar, анимацию для которого хотим получить
    ///   - state: состояние UINavigationBar
    /// - Returns: анимация или nil, если состояние для параметра не изменилось
    func hiddenChangedAnimations(for navigationBar: UINavigationBar, with state: NavigationBarState) -> StateChangeAnimations? {
        
        guard state.isHidden != navigationBar.isHidden else {
            return nil
        }
        
        let preparation = {
            navigationBar.isHidden = false
        }
        let animations = {
            navigationBar.alpha = state.isHidden ? 0 : 1
        }
        let completion = {
            navigationBar.isHidden = state.isHidden
        }
        
        return StateChangeAnimations(preparation: preparation,
                                     animations: animations,
                                     reversion: nil,
                                     completion: completion)
    }
    
    /// Анимация при изменении прозрачности UINavigationBar
    ///
    /// - Parameters:
    ///   - navigationBar: UINavigationBar, анимацию для которого хотим получить
    ///   - state: состояние UINavigationBar
    /// - Returns: анимация или nil, если состояние для параметра не изменилось
    func translucentChangedAnimations(for navigationBar: UINavigationBar, with state: NavigationBarState) -> StateChangeAnimations? {
        
        guard state.isTranslucent != navigationBar.isTranslucent else {
            return nil
        }
        
        let completion = {
            navigationBar.isTranslucent = state.isTranslucent
        }
        
        return StateChangeAnimations(preparation: nil,
                                     animations: nil,
                                     reversion: nil,
                                     completion: completion)
    }
    
    /// Анимация при изменении оттенка UINavigationBar
    ///
    /// - Parameters:
    ///   - navigationBar: UINavigationBar, анимацию для которого хотим получить
    ///   - state: состояние UINavigationBar
    /// - Returns: анимация или nil, если состояние для параметра не изменилось
    func barTintColorChangedAnimations(for navigationBar: UINavigationBar, with state: NavigationBarState) -> StateChangeAnimations? {
        
        guard state.barTintColor != navigationBar.barTintColor else {
            return nil
        }
        
        let previousBarTintColor = navigationBar.barTintColor
        
        let animations = {
            navigationBar.barTintColor = state.barTintColor
        }
        let reversion = {
            navigationBar.barTintColor = previousBarTintColor
        }
        
        return StateChangeAnimations(preparation: nil,
                                     animations: animations,
                                     reversion: reversion,
                                     completion: nil)
    }
    
    /// Анимация при изменении показа тени UINavigationBar
    ///
    /// - Parameters:
    ///   - navigationBar: UINavigationBar, анимацию для которого хотим получить
    ///   - state: состояние UINavigationBar
    /// - Returns: анимация или nil, если состояние для параметра не изменилось
    func shadowHiddenChangedAnimations(for navigationBar: UINavigationBar, with state: NavigationBarState) -> StateChangeAnimations? {
        
        let isShadowHidden = navigationBar.shadowImage != nil && navigationBar.backgroundImage(for: .default) != nil
        
        guard state.isShadowHidden != isShadowHidden else {
            return nil
        }
        
        let setShadowHidden: (Bool) -> () = { isHidden in
            navigationBar.setBackgroundImage((isHidden ? UIImage() : nil), for: .default)
            navigationBar.shadowImage = isHidden ? UIImage() : nil
        }
        
        if state.isShadowHidden {
            
            let preparation = {
                setShadowHidden(state.isShadowHidden)
            }
            let reversion = {
                setShadowHidden(isShadowHidden)
            }
            
            return StateChangeAnimations(preparation: preparation,
                                         animations: nil,
                                         reversion: reversion,
                                         completion: nil)
        }
        else {
            
            let completion = {
                setShadowHidden(state.isShadowHidden)
            }
            
            return StateChangeAnimations(preparation: nil,
                                         animations: nil,
                                         reversion: nil,
                                         completion: completion)
        }
    }
    
    /// Анимация при изменении цвета элементов внутри UINavigationBar
    ///
    /// - Parameters:
    ///   - navigationBar: UINavigationBar, анимацию для которого хотим получить
    ///   - state: состояние UINavigationBar
    /// - Returns: анимация или nil, если состояние для параметра не изменилось
    func tintColorChangedAnimations(for navigationBar: UINavigationBar, with state: NavigationBarState) -> StateChangeAnimations? {
        
        guard state.tintColor != navigationBar.tintColor else {
            return nil
        }
        
        let previousTintColor = navigationBar.tintColor
        
        let animations = {
            navigationBar.tintColor = state.tintColor
        }
        let reversion = {
            navigationBar.tintColor = previousTintColor
        }
        
        return StateChangeAnimations(preparation: nil,
                                     animations: animations,
                                     reversion: reversion,
                                     completion: nil)
    }
    
    /// Анимация при изменении параметров заголовка UINavigationBar
    ///
    /// - Parameters:
    ///   - navigationBar: UINavigationBar, анимацию для которого хотим получить
    ///   - state: состояние UINavigationBar
    /// - Returns: анимация или nil, если состояние для параметра не изменилось
    func titleTextAttributesChangedAnimations(for navigationBar: UINavigationBar, with state: NavigationBarState) -> StateChangeAnimations? {
        
        guard state.titleTextAttributes != nil else {
            return nil
        }
        
        let previousAttributes = navigationBar.titleTextAttributes
        
        let animations = {
            navigationBar.titleTextAttributes = state.titleTextAttributes
        }
        let reversion = {
            navigationBar.titleTextAttributes = previousAttributes
        }
        
        return StateChangeAnimations(preparation: nil,
                                     animations: animations,
                                     reversion: reversion,
                                     completion: nil)
    }
    
    /// Анимация при изменении параметров большого заголовка UINavigationBar
    ///
    /// - Parameters:
    ///   - navigationBar: UINavigationBar, анимацию для которого хотим получить
    ///   - state: состояние UINavigationBar
    /// - Returns: анимация или nil, если состояние для параметра не изменилось
    func largeTitleTextAttributesChangedAnimations(for navigationBar: UINavigationBar, with state: NavigationBarState) -> StateChangeAnimations? {
        
        guard state.largeTitleTextAttributes != nil else {
            return nil
        }
        
        guard #available(iOS 11.0, *) else {
            return nil
        }
        
        let previousAttributes = navigationBar.largeTitleTextAttributes
        
        let animations = {
            navigationBar.largeTitleTextAttributes = state.largeTitleTextAttributes
        }
        let reversion = {
            navigationBar.largeTitleTextAttributes = previousAttributes
        }
        
        return StateChangeAnimations(preparation: nil,
                                     animations: animations,
                                     reversion: reversion,
                                     completion: nil)
    }
    
    /// Анимация при изменении интерактивности UINavigationBar
    ///
    /// - Parameters:
    ///   - navigationBar: UINavigationBar, анимацию для которого хотим получить
    ///   - state: состояние UINavigationBar
    /// - Returns: анимация или nil, если состояние для параметра не изменилось
    func interactionEnabledChangedAnimations(for navigationBar: UINavigationBar, with state: NavigationBarState) -> StateChangeAnimations? {
        
        guard state.isUserInteractionEnabled != navigationBar.isUserInteractionEnabled else {
            return nil
        }
        
        let completion = {
            navigationBar.isUserInteractionEnabled = state.isUserInteractionEnabled
        }
        
        return StateChangeAnimations(preparation: nil,
                                     animations: nil,
                                     reversion: nil,
                                     completion: completion)
    }
}

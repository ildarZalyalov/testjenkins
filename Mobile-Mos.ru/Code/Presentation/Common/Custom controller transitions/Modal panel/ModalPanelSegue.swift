//
//  ModalPanelSegue.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 04.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import UIKit

/// Segue для показа UIViewController в модальной панели
/// Модальная панель открывается на часть экрана снизу, имеет сверху скругленные углы и затемнение
class ModalPanelSegue: UIStoryboardSegue, UIViewControllerTransitioningDelegate {
    
    override func perform() {
        
        destination.modalPresentationCapturesStatusBarAppearance = true
        destination.modalPresentationStyle = .custom
        destination.transitioningDelegate = self
        
        source.present(destination, animated: true, completion: nil)
    }
    
    //MARK: - UIViewControllerTransitioningDelegate
    
    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        return ModalPanelPresentationController(presentedViewController: presented, presenting: presenting)
    }
}

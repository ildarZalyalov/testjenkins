//
//  ModalPanelPresentationController.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 04.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import UIKit

/// UIPresentationController для показа UIViewController в модальной панели
/// Модальная панель открывается на часть экрана снизу, имеет сверху скругленные углы и затемнение
class ModalPanelPresentationController: UIPresentationController {
    
    /// Минимально допустимый отступ панели от верха экрана
    var minimumTopContentOffset: CGFloat {
        switch UIDevice.current.deviceScreenType {
        case .iPhone4, .iPhone5:
            return 20
        default:
            return 45
        }
    }
    
    /// Радиус скругления верхних углов панели
    let modelPanelCornerRadius: CGFloat = 10
    
    let preferredContentSizeChangedAnimationDuration: TimeInterval = 0.3
    
    fileprivate var presentationDidEnd: Bool = false
    
    /// Вьюха, в которой располагается показываемый контроллер
    fileprivate lazy var modalPanelView: UIView = {
        
        let modalPanelView = UIView()
        modalPanelView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        modalPanelView.backgroundColor = UIColor.white
        modalPanelView.clipsToBounds = true
        modalPanelView.layer.cornerRadius = self.modelPanelCornerRadius
        
        let presentedViewControllerContainer = UIView()
        presentedViewControllerContainer.backgroundColor = UIColor.white
        presentedViewControllerContainer.translatesAutoresizingMaskIntoConstraints = false
        modalPanelView.addSubview(presentedViewControllerContainer)
        
        let constraintsInsets = UIEdgeInsets(top: 0, left: 0, bottom: self.modelPanelCornerRadius, right: 0)
        let constraints = NSLayoutConstraint.edgesConstraints(for: presentedViewControllerContainer, with: constraintsInsets)
        modalPanelView.addConstraints(constraints)
        
        self.presentedViewController.view.frame = presentedViewControllerContainer.bounds
        self.presentedViewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        presentedViewControllerContainer.addSubview(self.presentedViewController.view)
        
        return modalPanelView
    }()
    
    /// Вьюха затемнения
    fileprivate lazy var dimmingView: UIView = {
        
        let dimmingView = UIView()
        dimmingView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        dimmingView.backgroundColor = UIColorPalette.modalPanelDimmingColor
        dimmingView.alpha = 0.0
        
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleTapOnDimmingView(recognizer:)))
        dimmingView.addGestureRecognizer(tapRecognizer)
        
        return dimmingView
    }()
    
    /// Закэшированный фрейм вьюхи, в которой располагается показываемый контроллер
    fileprivate var lastCalculatedFrameOfPresentedView: CGRect?
    
    /// Закэшированный предпочитаемый размер показываемого контроллера
    /// (чтобы не пересчитывать высоту, если он не изменился)
    fileprivate var lastPreferredContentSizeForFrameCalculation: CGSize?
    
    //MARK: - Приватные методы
    
    @objc
    fileprivate func handleTapOnDimmingView(recognizer: UITapGestureRecognizer) {
        presentingViewController.dismiss(animated: true, completion: nil)
    }
    
    //MARK: - UIPresentationController
    
    override var shouldPresentInFullscreen: Bool {
        return false
    }
    
    override var shouldRemovePresentersView: Bool {
        return false
    }
    
    override var presentedView: UIView? {
        return modalPanelView
    }
    
    override var frameOfPresentedViewInContainerView: CGRect {
        
        guard let container = containerView else { return CGRect.zero }
        var frame = container.bounds
        
        if let lastCalculatedFrame = lastCalculatedFrameOfPresentedView, lastCalculatedFrame.width == frame.width {
            return lastCalculatedFrame
        }
        
        var height = frame.height
        
        if !presentedViewController.preferredContentSize.equalTo(CGSize.zero) {
            
            lastPreferredContentSizeForFrameCalculation = presentedViewController.preferredContentSize
            height = presentedViewController.preferredContentSize.height
            
            if #available(iOS 11.0, *), !presentationDidEnd {
                height += container.safeAreaInsets.bottom
            }
        }
        
        var difference = frame.height - height
        if difference < minimumTopContentOffset {
            difference = minimumTopContentOffset
        }
        
        // учитываем сдвиг на отступ панели сверху
        frame = frame.offsetBy(dx: 0, dy: difference)
        frame.size.height -= difference
        
        // учитываем радиус скругления снизу, чтобы его не было видно
        frame.size.height += modelPanelCornerRadius
        
        lastCalculatedFrameOfPresentedView = frame
        
        return frame
    }
    
    override func presentationTransitionWillBegin() {
        
        if let container = containerView {
            dimmingView.frame = container.bounds
            container.insertSubview(dimmingView, at: 0)
        }
        
        guard let coordinator = presentedViewController.transitionCoordinator else {
            dimmingView.alpha = 1.0
            return
        }
        
        coordinator.animate(alongsideTransition: { [weak self] _ in
            self?.dimmingView.alpha = 1.0
        })
    }
    
    override func presentationTransitionDidEnd(_ completed: Bool) {
        presentationDidEnd = completed
    }
    
    override func dismissalTransitionWillBegin() {
        
        guard let coordinator = presentedViewController.transitionCoordinator else {
            dimmingView.alpha = 0.0
            return
        }
        
        coordinator.animate(alongsideTransition: { [weak self] _ in
            self?.dimmingView.alpha = 0.0
        })
    }
    
    override func containerViewWillLayoutSubviews() {
        presentedView?.frame = frameOfPresentedViewInContainerView
    }
    
    //MARK: - UIContentContainer
    
    override func preferredContentSizeDidChange(forChildContentContainer container: UIContentContainer) {
        
        guard presentationDidEnd else { return }
        if let lastContentSize = lastPreferredContentSizeForFrameCalculation {
            guard !presentedViewController.preferredContentSize.equalTo(lastContentSize) else { return }
        }
        
        lastCalculatedFrameOfPresentedView = CGRect.zero
        
        UIView.animate(withDuration: preferredContentSizeChangedAnimationDuration,
                       delay: 0,
                       options: .curveEaseOut,
                       animations: {
            self.modalPanelView.frame = self.frameOfPresentedViewInContainerView
        }, completion: nil)
    }
}

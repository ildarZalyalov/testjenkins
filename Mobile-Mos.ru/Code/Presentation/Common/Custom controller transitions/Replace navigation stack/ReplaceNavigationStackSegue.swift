//
//  ReplaceNavigationStackSegue.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 24.05.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation
import UIKit

/// Segue для замены всего стека UINavigationController на destination
class ReplaceNavigationStackSegue: UIStoryboardSegue {
    
    override func perform() {
        source.navigationController?.setViewControllers([destination], animated: true)
    }
}

//
//  ChatControlButtonsDataSourceFactoryImplementation.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 04.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class ChatControlButtonsDataSourceFactoryImplementation: ChatControlButtonsDataSourceFactory {
    
    func buildDataSourceConfiguration(from buttons: [ChatCustomControlButton]) -> ChatControlButtonsDataSourceConfiguration {
        
        var dataSource = [CommandKeyboardView.KeyboardButtonViewModel]()
        var selectionItems = [String : ChatCustomControlButton]()
        
        for button in buttons {
            
            let cellObject = CommandKeyboardView.KeyboardButtonViewModel(itemId: button.itemId, title: button.title)
            
            dataSource.append(cellObject)
            selectionItems[cellObject.itemId] = button
        }
        
        return (dataSource, selectionItems)
    }
}

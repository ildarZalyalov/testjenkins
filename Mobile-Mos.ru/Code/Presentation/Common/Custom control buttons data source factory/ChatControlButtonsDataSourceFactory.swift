//
//  ChatControlButtonsDataSourceFactory.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 04.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Конфигурация источника данных для командной клавиатуры в контроле в чате
typealias ChatControlButtonsDataSourceConfiguration = (dataSource: [CommandKeyboardView.KeyboardButtonViewModel], selectionItems: [String : ChatCustomControlButton])

/// Строитель источника данных для командной клавиатуры в контроле в чате
protocol ChatControlButtonsDataSourceFactory {
    
    /// Построить источник данных для командной клавиатуры в контроле в чате
    ///
    /// - Parameter buttons: информация о кнопках в клавиатуре
    /// - Returns: источник данных
    func buildDataSourceConfiguration(from buttons: [ChatCustomControlButton]) -> ChatControlButtonsDataSourceConfiguration
}

//
//  UserPinIconConfiguration.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 20.06.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

class UserPinIconConfiguration: NSObject, UserPinIconConfigurator, YMKUserLocationObjectListener {
    
    fileprivate let userPinIcon = #imageLiteral(resourceName: "geo_pin_g")
    fileprivate let userPinRadiusColor = UIColor(cgColor: UIColor.white.withAlphaComponent(0.5).cgColor)
    
    func configureUserPinIcon(with userLocationLayer: YMKUserLocationLayer?) {
        
        guard userLocationLayer != nil else { return }
        
        userLocationLayer!.isEnabled = true
        userLocationLayer!.isHeadingEnabled = false
        userLocationLayer!.setObjectListenerWith(self)
    }
    
    //MARK: - YMKUserLocationObjectListener
    
    func onObjectAdded(with view: YMKUserLocationView?) {
        
        guard view != nil else { return }
        
        view!.pin!.setIconWith(userPinIcon)
        view!.arrow!.setIconWith(userPinIcon)
        view!.accuracyCircle!.fillColor = userPinRadiusColor
    }
    
    func onObjectRemoved(with view: YMKUserLocationView?) {}
    
    func onObjectUpdated(with view: YMKUserLocationView?, event: YMKObjectEvent?) {}
}

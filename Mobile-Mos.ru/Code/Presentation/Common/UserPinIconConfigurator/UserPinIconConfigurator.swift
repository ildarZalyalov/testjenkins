//
//  UserPinIconConfigurator.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 20.06.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

protocol UserPinIconConfigurator {
    
    /// Сконфигурировать иконку пользователя
    ///
    /// - Parameter userLocationLayer: слой локации пользователя
    func configureUserPinIcon(with userLocationLayer: YMKUserLocationLayer?)
}

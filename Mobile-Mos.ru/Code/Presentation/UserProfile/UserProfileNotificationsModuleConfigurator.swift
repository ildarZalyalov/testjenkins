//
//  UserProfileNotificationsModuleConfigurator.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 07.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import UIKit

class UserProfileNotificationsModuleConfigurator: BaseModuleConfigurator {
    
    override func configureModule(for view: UIViewController) {
        
        let viewController = view as! UserProfileNotificationsViewController
        let presenter = UserProfileNotificationsPresenter()
        let interactor = UserProfileNotificationsInteractor()
        let router = UserProfileNotificationsRouter()
        let settingsDatasourceFactory = NotificationsSettingsDataSourceFactoryImplementation()
        
        viewController.output = presenter
        
        presenter.view = viewController
        presenter.interactor = interactor
        presenter.router = router
        presenter.userProfileNotificationFactory = settingsDatasourceFactory
        
        interactor.output = presenter
        
        router.transitionHandler = viewController
    }
}

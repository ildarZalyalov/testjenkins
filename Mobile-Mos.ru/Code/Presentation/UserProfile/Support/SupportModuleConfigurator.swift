//
//  SupportModuleConfigurator.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 14.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation
import UIKit

class SupportModuleConfigurator: BaseModuleConfigurator {
    
    override func configureModule(for view: UIViewController) {
        
        let viewController = view as! SupportViewController
        let presenter = SupportPresenter()
        let interactor = SupportInteractor()
        let router = SupportRouter()
       
        let feedbackService = UIApplication.shared.serviceBuilder.getFeedbackService()
        let supportsAlertsFactory = SupportAlertsFactoryImplementation()
        let commonAlertsFactory = CommonAlertsFactoryImplementation()
        let notificationCenter = NotificationCenter.default
        let stringValidator = StringValidator()
        let currentUserManager = CurrentUserManagerImplementation()
        let analyticsManager = AnalyticsManagerBuilder.getAnalyticsManager()
        
        viewController.output = presenter
        viewController.notificationCenter = notificationCenter
        
        presenter.view = viewController
        presenter.interactor = interactor
        presenter.router = router
        
        interactor.output = presenter
        interactor.feedbackService = feedbackService
        interactor.stringValidator = stringValidator
        interactor.currentUserManager = currentUserManager
        interactor.analyticsManager = analyticsManager
        
        router.transitionHandler = viewController
        router.supportAlertFactory = supportsAlertsFactory
        router.commonAlertsFactory = commonAlertsFactory
        
    }
}

//
//  SupportAlertsFactory.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 15.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Делегат для отлова нажатия "ок" в алерте
protocol SupportAlertsFactoryHandler: class {
    func didPressOk()
}

/// Фабрика алертов
protocol SupportAlertsFactory {
    func getSuccessAlert(with handler: SupportAlertsFactoryHandler?) -> UIViewController
}

//
//  SupportAlertsFactoryImplementation.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 15.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

class SupportAlertsFactoryImplementation: SupportAlertsFactory {
    
    func getSuccessAlert(with handler: SupportAlertsFactoryHandler?) -> UIViewController {
        
        let alert = UIAlertController(title: "🎉🎉🎉", message: "Спасибо, мы получили сообщение. Специалисты свяжутся с вами по email и попробуют решить проблему. Обычно на это нужно от 2 до 8 дней", preferredStyle: .alert)
        
        let okButton = UIAlertAction(title: "Закрыть", style: .default) { _ in
            handler?.didPressOk()
        }
        
        alert.addAction(okButton)
        
        return alert
    }
}

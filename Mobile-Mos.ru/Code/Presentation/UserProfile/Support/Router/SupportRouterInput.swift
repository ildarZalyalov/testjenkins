//
//  SupportRouterInput.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 14.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

protocol SupportRouterInput: class {
    
    func showAgreementsModule()
    
    func showFeedbackSuccessAlert(with handler: SupportAlertsFactoryHandler?)
    
    func showEmailValidatorErrorAlert()
    
    func showEmailEmptyErrorAlert()
    
    func showPhoneNumberNotValidErrorModule()
    
    func showSubjectEmptyErrorAlert()
    
    func showTextEmptyErrorAlert()
    
    func showFailedSendingSupportErrorAlert()
    
    func closeModule()
}

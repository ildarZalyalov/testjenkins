//
//  SupportRouter.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 14.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

class SupportRouter: SupportRouterInput {
    
    weak var transitionHandler: UIViewController!
    var supportAlertFactory: SupportAlertsFactory!
    var commonAlertsFactory: CommonAlertsFactory!
    
    let showAgreementsSegueIdentifier = "showAgreements"
    
    //MARK: - SupportRouterInput
    
    func showAgreementsModule() {
        transitionHandler.performSegue(withIdentifier: showAgreementsSegueIdentifier, sender: nil)
    }
    
    func showFeedbackSuccessAlert(with handler: SupportAlertsFactoryHandler?) {
        
        let alert = supportAlertFactory.getSuccessAlert(with: handler)
        transitionHandler.present(alert, animated: true, completion: nil)
    }
    
    func showEmailValidatorErrorAlert() {
        let alert = commonAlertsFactory.getErrorAlert(with: StringsHelper.emailValidateMessage)
        transitionHandler.present(alert, animated: true, completion: nil)
    }
    
    func showPhoneNumberNotValidErrorModule() {
        let alert = commonAlertsFactory.getErrorAlert(with: StringsHelper.minCountOfPhoneNumber)
        transitionHandler.present(alert, animated: true, completion: nil)
    }
    
    func showEmailEmptyErrorAlert() {
        let alert = commonAlertsFactory.getErrorAlert(with: StringsHelper.emailEmptyMessage)
        transitionHandler.present(alert, animated: true, completion: nil)
    }
    
    func showSubjectEmptyErrorAlert() {
        let alert = commonAlertsFactory.getErrorAlert(with: StringsHelper.subjectEmptyMessage)
        transitionHandler.present(alert, animated: true, completion: nil)
    }
    
    func showTextEmptyErrorAlert() {
        let alert = commonAlertsFactory.getErrorAlert(with: StringsHelper.textEmptyMessage)
        transitionHandler.present(alert, animated: true, completion: nil)
    }
    
    func showFailedSendingSupportErrorAlert() {
        let alert = commonAlertsFactory.getErrorAlert(with: StringsHelper.failedSendingSupportAboutAppErrorText)
        transitionHandler.present(alert, animated: true, completion: nil)
    }
    
    func closeModule() {
        transitionHandler.dismiss(animated: true, completion: nil)
    }
}

//
//  SupportViewOutput.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 14.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

enum SupportViewFieldType {
    case email
    case subject
    case description
}

typealias SupportViewFieldValue = (type: SupportViewFieldType, text: String?)

protocol SupportViewOutput: class {
    
    func setupInitialState()
    
    func didPressCancel()
    
    func didPressAgreements()
    
    func validateField(with value: SupportViewFieldValue, onFormSubmit: Bool) -> Bool
    
    func didPressSendFeedback(with email: String?, subject: String?, description: String?)
}

//
//  SupportViewInput.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 14.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Информация пользователя для полей
typealias UserFieldsInfo = (userPhone: String?, userEmail: String?)

protocol SupportViewInput: class {
    
    var subjectsDataSource:[SubjectModel] { get set }
    
    func hideActivityIndicator()
    
    func fillFields(from info: UserFieldsInfo)
}

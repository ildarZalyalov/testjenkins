//
//  SupportViewController.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 14.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import UIKit

class SupportViewController: BaseViewController, SupportViewInput, NavigationBarCustomizingController, UITextFieldDelegate, UITextViewDelegate, UIPickerViewDelegate {
    
    var output: SupportViewOutput!
    
    var notificationCenter: NotificationCenter!
    
    let textFieldsLabelAffineTransform = CGAffineTransform(scaleX: 0.8, y: 0.8)
    let textFieldsLabelAnimationDuration = 0.6
    let textFieldlabelTransformAnchorPoint = CGPoint(x: 0.625, y: 0.5)
    let textFieldlabelDefaultAnchorPoint = CGPoint(x: 0.5, y: 0.5)
    
    let textTypingFont = UIFont(customName: .graphikLCGMedium, size: 17)!
    let normalFont = UIFont(customName: .graphikLCGRegular, size: 17)!
    let emptyLineChar: Character = "\n"
    
    var isSendButtonConstraintUpdated = false
    var isKeyboardOpen = false
    
    var descriptionDefaultHeight: CGFloat = 0
    
    var lastScrollContentSizeHeight: CGFloat = 0
    var sendButtonText = ""
    
    var subjectsDataSource: [SubjectModel] = []
    
    @IBOutlet weak var cancelBarButtonItem: UIBarButtonItem!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var emailStackView: UIStackView!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var emailSeparator: UIView!
    @IBOutlet weak var emailWarningImage: UIImageView!
    
    @IBOutlet weak var subjectStackView: UIStackView!
    @IBOutlet weak var subjectLabel: UILabel!
    @IBOutlet weak var subjectTextField: UITextField!
    @IBOutlet weak var subjectSeparator: UIView!
    @IBOutlet weak var subjectWarningImage: UIImageView!
    
    @IBOutlet weak var descriptionStackView: UIStackView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var descriptionSeparator: UIView!
    
    @IBOutlet weak var sendButtonBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var agreementsBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var descriptionHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var descriptionWarningImage: UIImageView!
    
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var agreementsLabel: UILabel!
    
    @IBOutlet var textFields: [UITextField]!
    
    //MARK: - Методы
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        descriptionTextView.isScrollEnabled = false
        descriptionTextView.textContainer.lineFragmentPadding = 0
        descriptionDefaultHeight = descriptionHeightConstraint.constant
       
        configureCancelButton()
        
        setupKeyboardNotifications()
        setupGestures()
        
        output.setupInitialState()
        
        setupDataPicker(for: subjectTextField)
        setupInputAccessoryView()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        updateSendButtonBottomConstraintIfNeeded()
    }
    
    deinit {
        notificationCenter.removeObserver(self)
    }
    
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override func viewSafeAreaInsetsDidChange() {
        resetScrollViewInsets()
    }
    
    
    //MARK: - NavigationBarCustomizingController
    
    var navigationBarState: NavigationBarState? {
        
        var state = NavigationBarState()
        
        state.isHidden = false
        state.isTranslucent = false
        state.backgroundState = NavigationBarBackgroundState.regular
        state.tintColor = UIColor.black
        state.isShadowHidden = true
        
        let titleTextAttributes = [
            NSAttributedStringKey.font : UIFont(customName: .graphikLCGBold, size: 15) as Any,
            NSAttributedStringKey.foregroundColor : UIColor.black
        ]
        let largeTitleTextAttributes = [
            NSAttributedStringKey.font : UIFont(customName: .graphikLCGBold, size: 25) as Any,
            NSAttributedStringKey.foregroundColor : UIColorPalette.conversationsPageTitleColor
        ]
        
        state.titleTextAttributes = titleTextAttributes
        state.largeTitleTextAttributes = largeTitleTextAttributes
        
        return state
    }
    
    //MARK: - SupportViewInput
    
    func hideActivityIndicator() {
        
        activityIndicator.isHidden = true
        sendButton.setTitle(sendButtonText, for: .normal)
        
        textFields.forEach{ $0.isEnabled = true }
        descriptionTextView.isEditable = true
        descriptionTextView.isSelectable = true
        descriptionStackView.isUserInteractionEnabled = true
    }
    
    func showActivityIndicator() {
        
        guard let buttonText = sendButton.titleLabel?.text else { return }
        
        sendButtonText = buttonText
        
        sendButton.setTitle("", for: .normal)
        
        textFields.forEach{ $0.isEnabled = false }
        descriptionTextView.isEditable = false
        descriptionTextView.isSelectable = false
        descriptionStackView.isUserInteractionEnabled = false
        
        activityIndicator.isHidden = false
        activityIndicator.startAnimating()
    }
    
    func fillFields(from info: UserFieldsInfo) {
        
        emailTextField.text = info.userEmail
    }
    
    //MARK: - UITextFieldDelegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        textField.font = textTypingFont
        
        if textField == emailTextField {
            
            emailWarningImage.isHidden = true
            emailSeparator.backgroundColor = UIColorPalette.selectedTextFieldSeparatorColor
            
            animateTextFieldTitleState(with: emailLabel, view: emailTextField)
        }
        else if textField == subjectTextField {
            
            subjectWarningImage.isHidden = true
            subjectTextField.text = subjectsDataSource.first?.title
            subjectSeparator.backgroundColor = UIColorPalette.selectedTextFieldSeparatorColor
            
            animateTextFieldTitleState(with: subjectLabel, view: subjectTextField)
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        textField.font = normalFont
        
        if textField == emailTextField {
            
            revertTextFieldTitleState(with: emailLabel, view: emailTextField)
            emailTextField.text = emailTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        }
        else if textField == subjectTextField {
           
            revertTextFieldTitleState(with: subjectLabel, view: subjectTextField)
        }
        
        validateTextInputs(textField, onFormSubmit: false)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == emailTextField {
            subjectTextField.becomeFirstResponder()
        }
        
        return false
    }
    
    //MARK: - UITextViewDelegate
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        guard text.last != emptyLineChar else {
            textView.resignFirstResponder()
            return false
        }
        
        return true
    }
    
    func textViewDidChange(_ textView: UITextView) {
        updateHeight(for: textView, font: textTypingFont)
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        textView.font = textTypingFont
        
        descriptionDefaultHeight = descriptionHeightConstraint.constant
        
        descriptionSeparator.backgroundColor = UIColorPalette.selectedTextFieldSeparatorColor
        descriptionWarningImage.isHidden = true
        animateTextFieldTitleState(with: descriptionLabel, view: descriptionTextView)
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        textView.font = normalFont
        
        revertTextFieldTitleState(with: descriptionLabel, view: descriptionTextView)
        descriptionSeparator.backgroundColor = UIColorPalette.unselectedTextFieldSeparatorColor
        
        updateHeight(for: textView, font: normalFont)
        
        validateTextInputs(textView, onFormSubmit: false)
    }
    
    //MARK: - PickerViewDelegate
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return subjectsDataSource[row].title
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        subjectTextField.text = subjectsDataSource[row].title
    }
    
    //MARK: - Helper methods
    
    func setupGestures() {
        
        scrollView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(userDidTapOnView)))
        emailStackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(respondEmailTextField)))
        subjectStackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(respondSubjectTextField)))
        descriptionStackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(respondDescriptionStackView)))
        agreementsLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(respondAgreementsLabel)))
    }
    
    func setupKeyboardNotifications() {
        
        notificationCenter.addObserver(self,
                                       selector: #selector(self.keyboardWillShow(with:)),
                                       name: NSNotification.Name.UIKeyboardWillShow,
                                       object: nil)
        
        notificationCenter.addObserver(self,
                                       selector: #selector(self.keyboardWillHide(with:)),
                                       name: NSNotification.Name.UIKeyboardWillHide,
                                       object: nil)
    }
    
    func setupDataPicker(for textField: UITextField) {
        
        let inputView = DataPickerInputView(frame: CGRect.zero)
        inputView.dataPicker.delegate = self
        inputView.setDataSource(with: subjectsDataSource)
        
        textField.inputView = inputView
    }
    
    func configureCancelButton() {
        
        let attributes: [NSAttributedStringKey : Any] = [NSAttributedStringKey.font : UIFont(customName: .graphikLCGRegular, size: 17) as Any]
        cancelBarButtonItem.setTitleTextAttributes(attributes, for: .normal)
        cancelBarButtonItem.setTitleTextAttributes(attributes, for: .highlighted)
    }
    
    func setupInputAccessoryView() {
        
        let nextButtonToolbar = UIToolbar(frame: CGRect.zero)
        nextButtonToolbar.barStyle = .default
        
        let nextButton = UIBarButtonItem(title: StringsHelper.activateNextTextFieldButtonTitle, style: .plain, target: self, action: #selector(nextButtonPressed))
        nextButton.tintColor = UIColor.black
        
        nextButtonToolbar.items = [UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil), nextButton]
        nextButtonToolbar.sizeToFit()
        
        emailTextField.inputAccessoryView = nextButtonToolbar
        subjectTextField.inputAccessoryView = nextButtonToolbar
        
        let doneButtonToolbar = UIToolbar(frame: CGRect.zero)
        doneButtonToolbar.barStyle = .default
        
        let doneButton = UIBarButtonItem(title: StringsHelper.deactivateTextFieldButtonTitle, style: .plain, target: self, action: #selector(doneButtonPressed))
        doneButton.tintColor = UIColor.black
        
        doneButtonToolbar.items = [UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil), doneButton]
        doneButtonToolbar.sizeToFit()
        
        descriptionTextView.inputAccessoryView = doneButtonToolbar
    }
    
    func resetScrollViewInsets() {
        
        guard !isKeyboardOpen else { return }
        
        var insets = UIEdgeInsets.zero
        if #available(iOS 11.0, *) {
            insets.bottom = view.safeAreaInsets.bottom
        }
        
        scrollView.contentInset = insets
        scrollView.scrollIndicatorInsets = insets
    }
    
    func updateHeight(for textView: UITextView, font: UIFont) {
        let newSize = textView.sizeThatFits(CGSize(width:textView.width, height: textView.text.sizeOfString(with: font).height))
        descriptionHeightConstraint.constant = newSize.height
    }
    
    func animateTextFieldTitleState(with textLabel: UILabel, view: UIView) {
        
        UIView.animate(withDuration: textFieldsLabelAnimationDuration, animations: { [weak self] in
            
            guard let strongSelf = self else { return }
            
            if #available(iOS 11.0, *) {
                textLabel.layer.anchorPoint = strongSelf.textFieldlabelTransformAnchorPoint
            }
            
            textLabel.transform = strongSelf.textFieldsLabelAffineTransform
            textLabel.x = view.x
        })
    }
    
    func revertTextFieldTitleState(with textLabel: UILabel, view: UIView) {
        
        UIView.animate(withDuration: textFieldsLabelAnimationDuration, animations: { [weak self] in
            
            guard let strongSelf = self else { return }
            
            if #available(iOS 11.0, *) {
                textLabel.layer.anchorPoint = strongSelf.textFieldlabelDefaultAnchorPoint
            }
            
            textLabel.transform = .identity
            textLabel.x = view.x
        })
    }
    
    func updateSendButtonBottomConstraintIfNeeded() {
        
        guard !isSendButtonConstraintUpdated else { return }
        
        isSendButtonConstraintUpdated = true
        
        let insets = scrollView.contentInset.top + scrollView.contentInset.bottom
        let delta = (scrollView.height - insets - sendButton.frame.maxY)
        
        sendButtonBottomConstraint.constant = delta - agreementsLabel.height - agreementsBottomConstraint.constant
        view.layoutIfNeeded()
    }
    
    func updateFieldState(inFieldWith type: SupportViewFieldType, hasError: Bool) {
        
        let isWarningHidden = !hasError
        let separatorColor = hasError ? UIColorPalette.errorInUserDataColor : UIColorPalette.unselectedTextFieldSeparatorColor
        
        switch type {
        case .email:
            emailSeparator.backgroundColor = separatorColor
            emailWarningImage.isHidden = isWarningHidden
        case .subject:
            subjectSeparator.backgroundColor = separatorColor
            subjectWarningImage.isHidden = isWarningHidden
        case .description:
            descriptionSeparator.backgroundColor = separatorColor
            descriptionWarningImage.isHidden = isWarningHidden
        }
    }

    @discardableResult
    func validateTextInputs(_ view: UIView, onFormSubmit: Bool) -> Bool {
        
        var fieldType: SupportViewFieldType
        var text: String?
        
        switch view {
        case emailTextField:
            fieldType = .email
            text = emailTextField.text
        case subjectTextField:
            fieldType = .subject
            text = subjectTextField.text
        case descriptionTextView:
            fieldType = .description
            text = descriptionTextView.text
        default:
            return true
        }
        
        let valid = output.validateField(with: (fieldType, text), onFormSubmit: onFormSubmit)
        updateFieldState(inFieldWith: fieldType, hasError: !valid)
        
        return valid
    }
    
    //MARK: - Observed methods
    
    @objc
    open func userDidTapOnView() {
        self.view.endEditing(true)
    }
    
    @objc
    func respondEmailTextField() {
        emailTextField.becomeFirstResponder()
    }
    
    @objc
    func respondSubjectTextField() {
        subjectTextField.becomeFirstResponder()
    }
    
    @objc
    func respondDescriptionStackView() {
        descriptionTextView.becomeFirstResponder()
    }
    
    @objc
    func respondAgreementsLabel() {
        output.didPressAgreements()
    }
    
    @objc
    func keyboardWillShow(with notification: Notification) {
        
        guard let info = notification.userInfo,
            let keyboardEndSize = (info[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size
            else { return }
        
        isKeyboardOpen = true
        
        let insets = UIEdgeInsets(top: 0, left: 0, bottom: keyboardEndSize.height, right: 0)
        scrollView.contentInset = insets
        scrollView.scrollIndicatorInsets = insets
    }
    
    @objc
    func keyboardWillHide(with notification: Notification) {
        isKeyboardOpen = false
        resetScrollViewInsets()
    }
    
    @objc
    func nextButtonPressed() {
        
       
        if emailTextField.isFirstResponder {
            subjectTextField.becomeFirstResponder()
        }
        else if subjectTextField.isFirstResponder {
            descriptionTextView.becomeFirstResponder()
        }
    }
    
    @objc
    func doneButtonPressed() {
        descriptionTextView.resignFirstResponder()
    }
    
    //MARK: - Buttons action
    
    @IBAction func cancelPressed() {
        
        textFields.forEach { $0.resignFirstResponder() }
        descriptionTextView.resignFirstResponder()
        
        output.didPressCancel()
    }
    
    @IBAction func sendButtonPressed(_ sender: UIButton) {
        
        let fields: [UIView] = [emailTextField, subjectTextField, descriptionTextView]
        for field in fields {
            guard validateTextInputs(field, onFormSubmit: true) else { return }
        }
        
        showActivityIndicator()
        output.didPressSendFeedback(with: emailTextField.text, subject: subjectTextField.text, description: descriptionTextView.text)
    }
}

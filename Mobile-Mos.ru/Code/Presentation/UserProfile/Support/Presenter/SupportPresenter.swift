//
//  SupportPresenter.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 14.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

class SupportPresenter: SupportViewOutput, SupportInteractorOutput, SupportAlertsFactoryHandler {
    
    weak var view: SupportViewInput!
    var router: SupportRouterInput!
    var interactor: SupportInteractorInput!
    
    let minCountOfPhoneCharacters = 11
    let phonePrefix = "+7"
    var selectedSubject: String = ""
    
    //MARK: - SupportViewOutput
    
    func setupInitialState() {
        
        interactor.sendFeedbackEvent(event: .screenLoaded(withString: AnalyticsConstants.screenLoadedEventName), categoryName: nil, errorText: nil)
        
        let subjects = interactor.obtainSubjects()
        view.subjectsDataSource = subjects
        
        interactor.obtainCurrentUserInfo()
    }
    
    func didPressAgreements() {
        router.showAgreementsModule()
        interactor.sendFeedbackEvent(event: .agreementsPressed(withString: AnalyticsConstants.agreementsEventName), categoryName: nil, errorText: nil)
    }
    
    func didPressSendFeedback(with email: String?, subject: String?, description: String?) {
        
         guard email != nil, subject != nil, description != nil else { return }
        
        selectedSubject = subject!
        interactor.sendFeedback(with: email!,
                                subject: subject!,
                                description: description!)
    }
    
    func validateField(with value: SupportViewFieldValue, onFormSubmit: Bool) -> Bool {
        
        switch value.type {
        case .email:
            
            guard let emailString = value.text, !emailString.isEmpty else {
                
                if onFormSubmit {
                    router.showEmailEmptyErrorAlert()
                }
                
                return false
            }
            
            guard interactor.isEmailValid(emailString) else {
                
                if onFormSubmit {
                    router.showEmailValidatorErrorAlert()
                }
                return false
            }
        case .subject:
            
            guard  let subjectString = value.text, !subjectString.isEmpty else {
                
                if onFormSubmit {
                    router.showSubjectEmptyErrorAlert()
                }
                return false
            }
        case .description:
            
            guard let descriptionString = value.text, !descriptionString.isEmpty else {
                
                if onFormSubmit {
                    router.showTextEmptyErrorAlert()
                }
                return false
            }
        }
        
        return true
    }
    
    //MARK: - SupportInteractorOutput
    
    func didPressCancel() {
        router.closeModule()
    }
    
    func didFinishSendingFeedback(with result: FeedbackMultipleResult) {
        
        view.hideActivityIndicator()
        
        switch result {
        case .success():
            router.showFeedbackSuccessAlert(with: self)
            interactor.sendFeedbackEvent(event: .sendPressed(withString: AnalyticsConstants.feedbackSentEventName), categoryName: selectedSubject, errorText: nil)
        case .failure(let error):
            router.showFailedSendingSupportErrorAlert()
            interactor.sendFeedbackEvent(event: .sendPressed(withString: AnalyticsConstants.feedbackSentEventName), categoryName: selectedSubject, errorText: error.localizedDescription)
        }
    }
    
    func didFinishObtainingCurrentUserInfo(with info: UserInfo?) {
        
        view.fillFields(from: ("", info?.email))
    }
    
    //MARK: - SupportAlertsFactoryHandler
    
    func didPressOk() {
        router.closeModule()
    }
}

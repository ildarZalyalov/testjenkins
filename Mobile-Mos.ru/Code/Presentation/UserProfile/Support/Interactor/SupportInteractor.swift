//
//  SupportInteractor.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 14.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

class SupportInteractor: SupportInteractorInput {
    
    weak var output: SupportInteractorOutput!
    var feedbackService: FeedbackService!
    var stringValidator: StringValidator!
    var currentUserManager: CurrentUserManager!
    var analyticsManager: AnalyticsManager!
    
    lazy var subjects: [SubjectModel] = {
       return [SubjectModel(title: "Отзыв о работе"),
               SubjectModel(title: "Сообщение об ошибке"),
               SubjectModel(title: "Предложение авторам"),
               SubjectModel(title: "Другое")]
    }()
    
    //MARK: - SupportInteractorInput
    
    func isEmailValid(_ emailString: String) -> Bool {
        return stringValidator.validateEmail(emailString)
    }
    
    func obtainCurrentUserInfo() {
        output.didFinishObtainingCurrentUserInfo(with: currentUserManager.currentUser.userInfo)
    }
    
    func sendFeedback(with email: String, subject: String, description: String) {
        
        let userPhone = currentUserManager.currentUser.userInfo?.phone ?? ""
        
        let feedbackInfoModel = FeedbackInfo(title: subject, email: email, feedBackDescription: description, userPhone: userPhone)
        
        feedbackService.sendFeedback(with: feedbackInfoModel) {
            [weak self] (result) in
            self?.output.didFinishSendingFeedback(with: result)
        }
    }
    
    func obtainSubjects() -> [SubjectModel] {
        return subjects
    }
    
    func sendFeedbackEvent(event: FeedbackEvents, categoryName: String?, errorText: String?) {
        analyticsManager.sendFeedbackEvent(with: event, categoryName: categoryName, errorText: errorText)
    }
}

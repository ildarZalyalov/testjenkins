//
//  SupportInteractorInput.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 14.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

protocol SupportInteractorInput: class {
    
    /// Получить информацию о пользователе, если есть
    ///
    func obtainCurrentUserInfo()
    
    /// Провалидировать email строку
    ///
    /// - Parameter emailString: email строка
    /// - Returns: валидно/не валидно
    func isEmailValid(_ emailString: String) -> Bool
    
    /// Отправить фидбэк
    ///
    /// - Parameters:
    ///   - email: email пользователя
    ///   - phone: телефон пользователя
    ///   - subject: тема пользователя
    ///   - description: описание пользователя
    func sendFeedback(with email: String, subject: String, description: String)
    
    /// Получить темы
    ///
    /// - Returns: темы
    func obtainSubjects() -> [SubjectModel]
    
    /// Отправить событие
    ///
    /// - Parameters:
    ///   - event: событие
    ///   - categoryName: название категории обращения
    ///   - parameter: параметр
    ///   - value: значение
    func sendFeedbackEvent(event: FeedbackEvents, categoryName: String?, errorText: String?)
}

//
//  SupportInteractorOutput.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 14.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

protocol SupportInteractorOutput: class {
    
    func didFinishSendingFeedback(with result: FeedbackMultipleResult)
    
    func didFinishObtainingCurrentUserInfo(with info: UserInfo?)
}

//
//  SubjectModel.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 15.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Модель Темы
struct SubjectModel {
    var title: String
}

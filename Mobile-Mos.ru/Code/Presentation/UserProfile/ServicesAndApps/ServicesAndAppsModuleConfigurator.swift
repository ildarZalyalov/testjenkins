//
//  ServicesAndAppsModuleConfigurator.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 23.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import UIKit

class ServicesAndAppsModuleConfigurator: BaseModuleConfigurator {
    
    override func configureModule(for view: UIViewController) {
        
        let viewController = view as! ServicesAndAppsViewController
        let presenter = ServicesAndAppsPresenter()
        let interactor = ServicesAndAppsInteractor()
        let router = ServicesAndAppsRouter()
        let serviceAndAppsFactory = ServicesAndAppsDataSourceFactoryImplementation()
        
        let iTunesSearchService = UIApplication.shared.serviceBuilder.getiTunesSearchService()
        let analyticsManager = AnalyticsManagerBuilder.getAnalyticsManager()
        
        viewController.output = presenter
        
        presenter.view = viewController
        presenter.interactor = interactor
        presenter.router = router
        presenter.serviceAndAppsFactory = serviceAndAppsFactory
        
        interactor.output = presenter
        interactor.iTunesSearchService = iTunesSearchService
        interactor.analyticsManager = analyticsManager
        
        router.transitionHandler = viewController
    }
}

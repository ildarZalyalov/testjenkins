//
//  ServicesAndAppsRouter.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 23.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import SafariServices

class ServicesAndAppsRouter: ServicesAndAppsRouterInput {
    
    weak var transitionHandler: UIViewController!
    
    let showCTMosRuSendApplicationSegueIndentifier = "showCTMosRuSendApplication"
    
    //MARK: - ServicesAndAppsRouterInput
    
    func openUrl(with websiteString: String) {
        
        guard let websiteUrl = URL(string: websiteString) else {
            return
        }
        
        UIApplication.shared.open(url: websiteUrl)
    }
    
    func showSafariViewController(with websiteString: String) {
        
        guard let websiteUrl = URL(string: websiteString) else {
            return
        }
        
        let safariController = SFSafariViewController(url: websiteUrl, entersReaderIfAvailable: true)
        if #available(iOS 10.0, *) {
            safariController.preferredControlTintColor = UIColorPalette.browserControlsDefaultTintColor
        }
        
        transitionHandler.present(safariController, animated: true, completion: nil)
    }
    
    func showCTMosRuModule() {
        transitionHandler.performSegue(withIdentifier: showCTMosRuSendApplicationSegueIndentifier, sender: self)
    }
}

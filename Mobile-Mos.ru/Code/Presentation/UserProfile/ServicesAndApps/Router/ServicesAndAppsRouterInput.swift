//
//  ServicesAndAppsRouterInput.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 23.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol ServicesAndAppsRouterInput: class {
    
    func openUrl(with websiteString: String)
    
    func showSafariViewController(with websiteString: String)
    
    func showCTMosRuModule()
}

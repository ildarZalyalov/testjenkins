//
//  ServicesAndAppsViewController.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 23.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import UIKit

class ServicesAndAppsViewController: BaseViewController, ServicesAndAppsViewInput, UITableViewDelegate, NavigationBarCustomizingController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var loadingStatusIndicator: UIActivityIndicatorView!
    
    var output: ServicesAndAppsViewOutput!
    var tableViewDataDisplayManager: TableViewDataSource!
    
    //MARK: - Методы
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        setupTableView()
        output.setupInitialState()
    }
    
    override var shouldAutorotate: Bool {
        // для дочерних контроллеров, которые могут поменять ориентацию
        return true
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return .portrait
    }
    
    @IBAction func unwindToServicesAndApps(_: UIStoryboardSegue) {}
    
    //MARK: - NavigationBarCustomizingController
    
    var navigationBarState: NavigationBarState? {
        
        var state = NavigationBarState()
        
        state.isHidden = false
        state.isTranslucent = false
        state.backgroundState = NavigationBarBackgroundState.regular
        state.tintColor = UIColor.black
        state.isShadowHidden = true
        
        let titleTextAttributes = [
            NSAttributedStringKey.font : UIFont(customName: .graphikLCGBold, size: 15) as Any,
            NSAttributedStringKey.foregroundColor : UIColor.black
        ]
        let largeTitleTextAttributes = [
            NSAttributedStringKey.font : UIFont(customName: .graphikLCGBold, size: 25) as Any,
            NSAttributedStringKey.foregroundColor : UIColorPalette.conversationsPageTitleColor
        ]
        
        state.titleTextAttributes = titleTextAttributes
        state.largeTitleTextAttributes = largeTitleTextAttributes
        
        return state
    }
    
    // MARK: - Приватные методы
    
    func setupTableView() {
        
        tableView.assignEmptyTableHeaderToHideGroupedTableTopOffset()
        
        tableView.registerCellNib(for: ServiceCellObject.self)
        tableView.registerCellNib(for: AppsCellObject.self)
        tableView.registerHeaderNib(for: ServicesAndAppsHeaderObject.self)
        
        tableView.estimatedRowHeight = 75
        tableView.estimatedSectionHeaderHeight = 56
    }
    
    //MARK: - ServicesAndAppsViewInput
    
    func displayServicesAndAppsTable(with dataSource: TableViewDataSource) {
        
        tableViewDataDisplayManager = dataSource
        tableViewDataDisplayManager.delegate = self
        tableViewDataDisplayManager.assign(to: tableView)
        
        tableView.reloadData()
    }
    
    func showLoadingStatus() {
        tableView.isHidden = true
        loadingStatusIndicator.startAnimating()
    }
    
    func hideLoadingStatus() {
        loadingStatusIndicator.stopAnimating()
        tableView.isHidden = false
    }
    
    //MARK: - UITableView
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let dataStructure = tableViewDataDisplayManager.dataStructure
        guard let cellObject = dataStructure.cellObject(at: indexPath) as? CellObjectWithId else {
            return
        }
        
        output.didSelectRow(with: cellObject.itemId)
    }
}

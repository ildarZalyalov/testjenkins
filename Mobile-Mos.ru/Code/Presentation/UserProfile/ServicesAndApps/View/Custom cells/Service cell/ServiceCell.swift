//
//  ServiceCell.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 23.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import UIKit

class ServiceCell: UITableViewCell, ConfigurableView {
    
    @IBOutlet weak var insiteContentView: UIView!
    @IBOutlet weak var upperContentView: UIView!
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var websiteLabel: UILabel!
    @IBOutlet weak var secondTitleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var actionButton: UIButton!
    
    var itemId: String!
    weak var eventHandler: ServiceCellEventHandler?
    
    let secondTitleLabelLineHeightMultiple: CGFloat = 1.2
    let descriptionLabelLineHeightMultiple: CGFloat = 1.38
    
    func apply(text: String, to label: UILabel, with lineHeightMultiple: CGFloat) {
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineHeightMultiple = lineHeightMultiple
        paragraphStyle.alignment = .left
        paragraphStyle.lineBreakMode = .byTruncatingTail
        
        let attributes: [NSAttributedStringKey : Any] = [NSAttributedStringKey.font : label.font, NSAttributedStringKey.paragraphStyle : paragraphStyle]
        let valueString = NSAttributedString(string: text, attributes: attributes)
        
        label.attributedText = valueString
    }
    
    //MARK: - Actions
    
    @IBAction func actionButtonPressed() {
        eventHandler?.actionButtonPressed(for: itemId)
    }
    
    //MARK: - ConfigurableView
    
    func configure(with object: Any) {
        
        guard let cellObject = object as? ServiceCellObject else {
            return
        }
        
        itemId = cellObject.itemId
        eventHandler = cellObject.eventHandler

        iconImageView.image   = cellObject.icon
        titleLabel.text       = cellObject.title
        websiteLabel.text     = cellObject.subtitle
        
        apply(text: cellObject.descriptionHeader, to: secondTitleLabel, with: secondTitleLabelLineHeightMultiple)
        apply(text: cellObject.descriptionText, to: descriptionLabel, with: descriptionLabelLineHeightMultiple)
        
        actionButton.setTitle(cellObject.buttonTitle, for: .normal)
    }
}

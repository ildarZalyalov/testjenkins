//
//  ServiceCellObject.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 23.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol ServiceCellEventHandler: class {
    
    func actionButtonPressed(for itemId: String)
}

struct ServiceCellObject: CellObjectWithId {
    
    var itemId: String
    
    var icon: UIImage
    
    var title: String
    
    var subtitle: String
    
    var descriptionHeader: String
    
    var descriptionText: String
    
    var viewColor: UIColor
    
    var buttonTitle: String
    
    weak var eventHandler: ServiceCellEventHandler?
}

//
//  ServicesAndAppsDataSourceFactory.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 23.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

typealias ServicesAndAppsDataSourceConfiguration = (dataSource: TableViewDataSource, selectionItems: [String : ServicesAndApps])

protocol ServicesAndAppsDataSourceFactory {
    
    func buildDataSourceConfiguration(with servicesArray: [ServicesAndAppsModel], and appsArray: [AppSearchModel], servicesEventHandler: ServiceCellEventHandler?) -> ServicesAndAppsDataSourceConfiguration
}

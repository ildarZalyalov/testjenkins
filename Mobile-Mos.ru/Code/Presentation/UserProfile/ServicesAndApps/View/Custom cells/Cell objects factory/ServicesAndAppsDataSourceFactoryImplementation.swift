//
//  ServicesAndAppsDataSourceFactoryImplementation.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 23.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class ServicesAndAppsDataSourceFactoryImplementation: ServicesAndAppsDataSourceFactory {
    
    //MARK: - ServicesAndAppsDataSourceFactory
    
    func buildDataSourceConfiguration(with servicesArray: [ServicesAndAppsModel], and appsArray: [AppSearchModel], servicesEventHandler: ServiceCellEventHandler?) -> ServicesAndAppsDataSourceConfiguration {
        
        let dataStructure = TableViewDataSourceStructure()
        var selectionItems = [String : ServicesAndApps]()

        var servicesCellObjects = [ServiceCellObject]()
        
        for model in servicesArray {
            
            let cellObject = ServiceCellObject(itemId: model.title, icon: model.icon, title: model.title, subtitle: model.subtitle, descriptionHeader: model.descriptionHeader, descriptionText: model.description, viewColor: UIColor.gray, buttonTitle: model.buttonTitle, eventHandler: servicesEventHandler)
            
            selectionItems[cellObject.itemId] = model
            servicesCellObjects.append(cellObject)
        }
        
        var appsCellObjects = [AppsCellObject]()
        
        for model in appsArray {
            
            let cellObject = AppsCellObject(itemId: model.title, iconUrlString: model.imageLinkString, title: model.title, subtitle: model.description)
            
            selectionItems[cellObject.itemId] = model
            appsCellObjects.append(cellObject)
        }
        
        let servicesSection = ServicesAndAppsHeaderObject(title: StringsHelper.ourAppsAndServicesServicesHeader)
        dataStructure.appendSection(with: servicesSection, and: servicesCellObjects)
        
        let appsSection = ServicesAndAppsHeaderObject(title: StringsHelper.ourAppsAndServicesAppsHeader)
        dataStructure.appendSection(with: appsSection, and: appsCellObjects)
        
        let dataSource = TableViewDataSource(with: dataStructure)
        
        return (dataSource: dataSource, selectionItems: selectionItems)
    }
}


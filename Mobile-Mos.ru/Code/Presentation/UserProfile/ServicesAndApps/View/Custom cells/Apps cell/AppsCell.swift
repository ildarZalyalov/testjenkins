//
//  AppsCell.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 23.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import UIKit

class AppsCell: UITableViewCell, ConfigurableView {
    
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var bottomSeparatorView: UIView!
    
    let titleLabelLineHeightMultiple: CGFloat = 1.29
    let subtitleLabelLineHeightMultiple: CGFloat = 1.38
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        contentView.alpha = highlighted ? 0.5 : 1
    }
    
    func apply(text: String, to label: UILabel, with lineHeightMultiple: CGFloat) {
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineHeightMultiple = lineHeightMultiple
        paragraphStyle.alignment = .left
        paragraphStyle.lineBreakMode = .byTruncatingTail
        
        let attributes: [NSAttributedStringKey : Any] = [NSAttributedStringKey.font : label.font, NSAttributedStringKey.paragraphStyle : paragraphStyle]
        let valueString = NSAttributedString(string: text, attributes: attributes)
        
        label.attributedText = valueString
    }
    
    //MARK: - ConfigurableView
    
    func configure(with object: Any) {
        
        guard let cellObject = object as? AppsCellObject else {
            return
        }
        
        iconImageView.image = nil
        iconImageView.loadAndDisplayImage(from: URL(string: cellObject.iconUrlString))
        
        apply(text: cellObject.title, to: titleLabel, with: titleLabelLineHeightMultiple)
        apply(text: cellObject.subtitle, to: subtitleLabel, with: subtitleLabelLineHeightMultiple)
    }
}

//
//  AppsCellObject.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 23.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

struct AppsCellObject: CellObjectWithId {
    
    var itemId: String
    
    var iconUrlString: String
    
    var title: String
    
    var subtitle: String
}

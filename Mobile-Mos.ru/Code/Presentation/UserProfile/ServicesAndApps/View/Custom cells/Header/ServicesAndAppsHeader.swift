//
//  ServicesAndAppsHeader.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 24.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import UIKit

class ServicesAndAppsHeader: UITableViewHeaderFooterView, ConfigurableView {
    
    @IBOutlet weak var sectionTitleLabel: UILabel!
    
    func configure(with object: Any) {
        
        guard let cellObject = object as? ServicesAndAppsHeaderObject else {
            return
        }
        
        sectionTitleLabel.text = cellObject.title
    }
}

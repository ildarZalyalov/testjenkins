//
//  ServicesAndAppsHeaderObject.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 24.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

struct ServicesAndAppsHeaderObject: HeaderObject {
    
    var title: String
}

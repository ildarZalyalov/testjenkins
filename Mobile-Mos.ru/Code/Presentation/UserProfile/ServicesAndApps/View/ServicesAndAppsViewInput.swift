//
//  ServicesAndAppsViewInput.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 23.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol ServicesAndAppsViewInput: class {
    
    func displayServicesAndAppsTable(with dataSource: TableViewDataSource)
    
    func showLoadingStatus()
    
    func hideLoadingStatus()
}

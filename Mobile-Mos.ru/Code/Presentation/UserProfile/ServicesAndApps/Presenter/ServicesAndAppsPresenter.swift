//
//  ServicesAndAppsPresenter.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 23.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class ServicesAndAppsPresenter: ServicesAndAppsViewOutput, ServicesAndAppsInteractorOutput, ServiceCellEventHandler {
    
    weak var view: ServicesAndAppsViewInput!
    var router: ServicesAndAppsRouterInput!
    var interactor: ServicesAndAppsInteractorInput!
    
    var serviceAndAppsFactory: ServicesAndAppsDataSourceFactory!
    var selectionItemsByItem: [String : ServicesAndApps]!
    
    fileprivate let itunesPreffix = "itunes"
    fileprivate let services: [ServicesAndAppsModel] = [ctMosRuServiceViewModel]
    
    let retryObtainMoscowAppsDelay: TimeInterval = 3
    lazy var retryObtainMoscowAppsPerformer = DelayedWorkPerformer { [weak self] in
        guard let strongSelf = self else { return }
        strongSelf.interactor.obtainMoscowGovermentApps()
    }
    
    //MARK: - ServicesAndAppsViewOutput
    
    func setupInitialState() {
        
        view.showLoadingStatus()
        interactor.obtainMoscowGovermentApps()
        interactor.sendAppsAndServicesEvent(event: .screenLoaded(withString: AnalyticsConstants.screenLoadedEventName), selectedAppName: nil)
    }
    
    func didSelectRow(with objectId: String) {
        
        guard let object = selectionItemsByItem[objectId] else { return }
        
        let link = object.linkFromApp
        
        interactor.sendAppsAndServicesEvent(event: .selectedApp(withString: AnalyticsConstants.selectedAppNameEventName), selectedAppName: object.title)
        
        if link.contains(itunesPreffix) {
            router.openUrl(with: link)
        }
        else if link != ctMosRuServiceViewModel.linkFromApp {
            router.showSafariViewController(with: link)
        }
    }
    
    //MARK: - ServicesAndAppsInteractorOutput
    
    func didFinishObtainingMoscowGovermentApps(with result: iTunesSearchAppMultipleResult) {
        
        switch result {
            
        case .success(let items):
            
            let configuration = serviceAndAppsFactory.buildDataSourceConfiguration(with: services, and: items, servicesEventHandler: self)
            selectionItemsByItem = configuration.selectionItems
            
            view.hideLoadingStatus()
            view.displayServicesAndAppsTable(with: configuration.dataSource)
            
        case .failure:
            retryObtainMoscowAppsPerformer.performOnMain(afterDelay: retryObtainMoscowAppsDelay)
        }
    }
    
    //MARK: - ServiceCellEventHandler
    
    func actionButtonPressed(for itemId: String) {
        
        guard let object = selectionItemsByItem[itemId] else { return }
        
        if(object.linkFromApp == ctMosRuServiceViewModel.linkFromApp) {
            router.showCTMosRuModule()
        }
    }
}

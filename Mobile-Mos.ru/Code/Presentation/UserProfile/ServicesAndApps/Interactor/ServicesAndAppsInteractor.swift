//
//  ServicesAndAppsInteractor.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 23.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class ServicesAndAppsInteractor: ServicesAndAppsInteractorInput {
    
    weak var output: ServicesAndAppsInteractorOutput!
    var iTunesSearchService: iTunesSearchService!
    var analyticsManager: AnalyticsManager!
    
    //MARK: - ServicesAndAppsInteractorInput
    
    func obtainMoscowGovermentApps() {
        iTunesSearchService.getMoscowGovermentApps { [weak self] (result) in
            self?.output.didFinishObtainingMoscowGovermentApps(with: result)
        }
    }
    
    func sendAppsAndServicesEvent(event: AppsAndServecesEvents, selectedAppName: String?) {
        analyticsManager.sendAppsAndServicesEvent(with: event, appName: selectedAppName, error: nil)
    }
}

//
//  ServicesAndAppsInteractorInput.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 23.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol ServicesAndAppsInteractorInput: class {
    
    func obtainMoscowGovermentApps()
    
    func sendAppsAndServicesEvent(event: AppsAndServecesEvents, selectedAppName: String?)
}

//
//  UserProfileRowType.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 15.02.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Тип ячейки формы в разделе в ЛК
enum UserProfileRowType: String {
    
    // Главная страница
    
    // личные данные
    case userInfo
    // уведомления
    case notifications
    // о приложении
    case aboutApp
    // приложения и сервисы
    case appsAndServices
    // пользовательское соглашение
    case agreement
    // обратная связь
    case feedback
    // выход
    case exit
    
    // неизвестно
    case none
}

//
//  UserProfileRowViewModel.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 05.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Тип accessory в ячейке в разделе в ЛК
///
/// - disclosure: стрелка
/// - switcher: UISwitch
enum UserProfileRowAccessoryType {
    case disclosure
    case switcher
}

/// View model ячейки формы в разделе ЛК
struct UserProfileRowViewModel {
    
    /// Заголовок
    var title: String
    
    /// Подзаголовок
    var subtitle: String?
    
    /// Иконка
    var icon: UIImage?
    
    /// Состояние UISwitch, если accessoryType = switcher
    var switchState: Bool
    
    /// Тип accessory в ячейке
    var accessoryType: UserProfileRowAccessoryType
    
    /// Тип ячейки
    var type: UserProfileRowType
    
    init(with title: String,
         subtitle: String? = nil,
         icon: UIImage? = nil,
         switchState: Bool = false,
         accessoryType: UserProfileRowAccessoryType = .disclosure,
         type: UserProfileRowType = .none) {
        self.title = title
        self.subtitle = subtitle
        self.icon = icon
        self.switchState = switchState
        self.accessoryType = accessoryType
        self.type = type
    }
}

//
//  UserProfileSectionViewModel.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 06.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// View model секции ячеек формы в разделе ЛК
struct UserProfileSectionViewModel {
    
    /// Данные о ячейках
    var sectionInfo: [UserProfileRowViewModel]
    
    /// Заголовок секции
    var sectionTitle: String?
    
    /// Футер секции
    var sectionFooter: String?
    
    init(sectionInfo: [UserProfileRowViewModel],
         sectionTitle: String? = nil,
         sectionFooter: String? = nil) {
        self.sectionInfo = sectionInfo
        self.sectionTitle = sectionTitle
        self.sectionFooter = sectionFooter
    }
}

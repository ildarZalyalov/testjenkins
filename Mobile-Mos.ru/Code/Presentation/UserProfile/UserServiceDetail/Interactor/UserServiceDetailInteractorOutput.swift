//
//  UserServiceDetailInteractorOutput.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 16.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol UserServiceDetailInteractorOutput: class {
    
    func didFinishObtainingUserDataOptions(with result: UserDataOptionsObtainResult)
    
    func didFinishDeletingUserDataOption(with optionId: String, and result: UserDataOptionsOperationResult)
    
    func didFinishDeletingUserDataOptionGroup(with groupId: String, and result: UserDataOptionsOperationResult)
}

//
//  UserServiceDetailInteractor.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 16.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class UserServiceDetailInteractor: UserServiceDetailInteractorInput {
    
    weak var output: UserServiceDetailInteractorOutput!
    
    var userDataService: UserDataOptionsService!
    var analyticsManager: AnalyticsManager!
    
    //MARK: - UserServiceDetailInteractorInput
    
    var hasConnectionToServer: Bool {
        return userDataService.hasConnectionToServer
    }
    
    func obtainUserDataOptions(for slug: String) {
        userDataService.obtainUserDataOptions(for: slug) { [weak self] result in
            self?.output.didFinishObtainingUserDataOptions(with: result)
        }
    }
    
    func deleteUserDataOption(with optionId: String, optionType: String) {
        userDataService.deleteUserDataOption(with: optionId, optionType: optionType) { [weak self] result in
            self?.output.didFinishDeletingUserDataOption(with: optionId, and: result)
        }
    }
    
    func deleteUserDataOptionGroup(with groupId: String, groupType: String) {
        userDataService.deleteUserDataOptionGroup(with: groupId, groupType: groupType) { [weak self] result in
            self?.output.didFinishDeletingUserDataOptionGroup(with: groupId, and: result)
        }
    }
    
    func sentDidLoadEvent(with chatId: String?) {
        
        guard let chat = ConversationIdentificator.init(rawValue: chatId ?? "") else { return }
        
        let eventName = AnalyticsConstants.screenLoadedEventName
        analyticsManager.setChatIdetifier(with: chat.rawValue)
        
        switch chat {
        case .children:
            analyticsManager.sendServiceEvent(with: CategoryType.childrenService, event: .screenLoaded(withString: eventName), errorText: nil)
        case .transport:
            analyticsManager.sendServiceEvent(with: CategoryType.transportService, event: .screenLoaded(withString: eventName), errorText: nil)
        case .myHome:
            analyticsManager.sendServiceEvent(with: CategoryType.myHomeService, event: .screenLoaded(withString: eventName), errorText: nil)
        case .health:
            analyticsManager.sendServiceEvent(with: CategoryType.medecineInsuranceService, event: .screenLoaded(withString: eventName), errorText: nil)
        default:
            return
        }
    }
    
    func sendEditEvent(with chatId: String?, statsData: ServiceStatsData?) {
        
        guard let statsData = statsData else { return }
        
        guard let chat = ConversationIdentificator.init(rawValue: chatId ?? "") else { return }
        
        let eventName = statsData.editEventName
        
        switch chat {
        case .children:
            analyticsManager.sendServiceEvent(with: CategoryType.childrenService, event: .edit(withString: eventName), errorText: nil)
        case .transport:
            analyticsManager.sendServiceEvent(with: CategoryType.transportService, event: .edit(withString: eventName), errorText: nil)
        case .myHome:
            analyticsManager.sendServiceEvent(with: CategoryType.myHomeService, event: .edit(withString: eventName), errorText: nil)
        case .health:
            analyticsManager.sendServiceEvent(with: CategoryType.medecineInsuranceService, event: .edit(withString: eventName), errorText: nil)
        default:
            return
        }
    }
    
    func sendAddEvent(with chatId: String?, statsData: ServiceStatsData?) {
        
        guard let statsData = statsData else { return }
        
        guard let chat = ConversationIdentificator.init(rawValue: chatId ?? "") else { return }
        
        let eventName = statsData.addEventName
        
        switch chat {
        case .children:
            analyticsManager.sendServiceEvent(with: CategoryType.childrenService, event: .add(withString: eventName), errorText: nil)
        case .transport:
            analyticsManager.sendServiceEvent(with: CategoryType.transportService, event: .add(withString: eventName), errorText: nil)
        case .myHome:
            analyticsManager.sendServiceEvent(with: CategoryType.myHomeService, event: .add(withString: eventName), errorText: nil)
        case .health:
            analyticsManager.sendServiceEvent(with: CategoryType.medecineInsuranceService, event: .add(withString: eventName), errorText: nil)
        default:
            return
        }
    }
    
}

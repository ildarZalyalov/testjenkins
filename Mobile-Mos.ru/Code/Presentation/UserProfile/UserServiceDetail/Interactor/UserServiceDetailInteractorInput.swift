//
//  UserServiceDetailInteractorInput.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 16.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol UserServiceDetailInteractorInput: class {
    
    var hasConnectionToServer: Bool { get }
    
    func obtainUserDataOptions(for slug: String)
    
    func deleteUserDataOption(with optionId: String, optionType: String)
    
    func deleteUserDataOptionGroup(with groupId: String, groupType: String)
    
    func sentDidLoadEvent(with chatId: String?)
    
    func sendEditEvent(with chatId: String?, statsData: ServiceStatsData?)
    
    func sendAddEvent(with chatId: String?, statsData: ServiceStatsData?)
}

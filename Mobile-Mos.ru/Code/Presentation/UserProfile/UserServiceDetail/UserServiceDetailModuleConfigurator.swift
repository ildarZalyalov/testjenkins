//
//  UserServiceDetailModuleConfigurator.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 16.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import UIKit

class UserServiceDetailModuleConfigurator: BaseModuleConfigurator {
    
    override func configureModule(for view: UIViewController) {
        
        let viewController = view as! UserServiceDetailViewController
        let presenter = UserServiceDetailPresenter()
        let interactor = UserServiceDetailInteractor()
        let router = UserServiceDetailRouter()
        let serviceDetailDataSourceFactory = ServiceDetailDataSourceFactoryImplementation()
        let alertFactory = UserServiceDetailAlertFactoryImplementation()
        let commonAlertsFactory = CommonAlertsFactoryImplementation()
        let storyboardFactory = StoryboardFactoryImplementation()
        let userDataService = UIApplication.shared.serviceBuilder.getUserDataOptionsService()
        let analyticsManager = AnalyticsManagerBuilder.getAnalyticsManager()
        
        viewController.output = presenter
        
        presenter.view = viewController
        presenter.interactor = interactor
        presenter.router = router
        presenter.serviceDetailFactory = serviceDetailDataSourceFactory
        
        interactor.output = presenter
        interactor.userDataService = userDataService
        interactor.analyticsManager = analyticsManager
        
        router.transitionHandler = viewController
        router.alertTransitionHandler = viewController
        router.toOtherModulesTransitionHandler = viewController
        router.alertFactory = alertFactory
        router.commonAlertsFactory = commonAlertsFactory
        router.storyboardFactory = storyboardFactory
        router.analyticsManager = analyticsManager
    }
}

//
//  UserServiceDetailPresenter.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 16.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class UserServiceDetailPresenter: UserServiceDetailViewOutput, UserServiceDetailInteractorOutput, OptionGroupTypeSelectionEventHandler, UserServiceDetailEditModuleEventHandler, UserServiceDetailEmptyViewEventHandler {
    
    weak var view: UserServiceDetailViewInput!
    var router: UserServiceDetailRouterInput!
    var interactor: UserServiceDetailInteractorInput!
    
    var serviceDetailFactory: ServiceDetailDataSourceFactory!
    var serviceDetailConfiguration: ServiceDetailDataSourceConfiguration!
    
    var serviceInfo: UserServiceInfo!
    var userData: UserOptionsData!
    
    let retryObtainUserDataDelay: TimeInterval = 3
    lazy var retryObtainUserDataPerformer = DelayedWorkPerformer { [weak self] in
        guard let strongSelf = self else { return }
        strongSelf.interactor.obtainUserDataOptions(for: strongSelf.serviceInfo.slug)
    }
    
    var processingBeginTime = Date()
    let processingMinimumDuration: TimeInterval = 1
    
    let readonlyServiceDetailChatIds: Set<ConversationIdentificator> = [.children]
    let emptyViewsToConversationsMapping: [ConversationIdentificator : UserServiceDetailConversationType] = [
        .children: .children,
        .health : .health
    ]
    
    //MARK: - Методы
    
    func perform(after delay: TimeInterval, work: @escaping () -> ()) {
        let deadline: DispatchTime = .now() + delay
        DispatchQueue.main.asyncAfter(deadline: deadline, execute: work)
    }
    
    func reloadOptionsGroups() {
        view.showLoadingStatus()
        interactor.obtainUserDataOptions(for: serviceInfo.slug)
    }
    
    func refreshOptionsGroups() {
        
        serviceDetailConfiguration = serviceDetailFactory.buildDataSourceConfiguration(from: userData.options, with: userData.optionsMetadata, and: userData.groups, groupedWith: userData.grouping)
        view.displayServicesInfo(with: serviceDetailConfiguration.dataSource)
        
        guard cannotEditServiceDetail() else { return }
        view.setEditingServiceDetailDisabled()
    }
    
    func canAddOption(with metadata: UserOptionMetadata) -> Bool {
        guard !metadata.multiple else { return true }
        return userData.options.filter({ $0.type == metadata.type }).count == 0
    }
    
    func cannotEditServiceDetail() -> Bool {
        guard let conversationId = ConversationIdentificator(rawValueOptional: serviceInfo.chatId) else { return true }
        return readonlyServiceDetailChatIds.contains(conversationId)
    }
    
    func canHandleFinishProcessing(with completion: @escaping () -> ()) -> Bool {
        
        let durationDifference = processingBeginTime.timeIntervalSinceNow + processingMinimumDuration
        
        guard durationDifference <= 0 else {
            perform(after: durationDifference, work: completion)
            return false
        }
        
        return true
    }
    
    func handleFinishDeletingUserDataOption(with itemId: String,
                                            result: UserDataOptionsOperationResult,
                                            objectIdObtainer: (String) -> String?,
                                            onSuccess: () -> ()) {
        
        view.setNavigationItemsEnabled(true)
        router.hideProcessingModule()
        
        switch result {
        case .success:
            guard let objectId = objectIdObtainer(itemId) else { return }
            view.deleteObject(with: objectId)
            onSuccess()
        case .failure:
            router.showDeletingOptionsErrorModule()
        }
    }
    
    //MARK: - UserServiceDetailViewOutput
    
    func setupInitialState() {
        
        view.showLoadingStatus()
        
        interactor.sentDidLoadEvent(with: serviceInfo.chatId)
        
        guard let conversationId = ConversationIdentificator(rawValueOptional: serviceInfo.chatId) else { return }
        guard let conversationType = emptyViewsToConversationsMapping[conversationId] else { return }
        
        
        view.setupEmptyContentView(with: conversationType, eventHandler: self)
        view.setupBottomLinkView(with: conversationType, eventHandler: self)
    }
    
    func handleViewFirstDisplayed() {
        
        guard interactor.hasConnectionToServer else {
            router.showConnectionErrorAndCloseModule()
            return
        }
        
        reloadOptionsGroups()
    }
    
    func configure(with serviceInfo: UserServiceInfo) {
        self.serviceInfo = serviceInfo
    }
    
    func didSelectObject(with objectId: String) {
        
        guard !cannotEditServiceDetail() else { return }
        
        if let option = serviceDetailConfiguration.selectionOptions[objectId] {
            
            router.showAddOrEditOptionModule(with: userData, for: option.type, optionId: option.itemId, eventHandler: self)
            
            guard let element = userData.optionsMetadata.enumerated().first(where: { $0.element.type == option.type })?.element else { return }
            interactor.sendEditEvent(with: serviceInfo.chatId, statsData: element.statsData)
        }
        else if let group = serviceDetailConfiguration.selectionOptionGroups[objectId] {
            
            router.showAddOrEditOptionsGroupModule(with: userData, for: group.type, groupId: group.itemId, eventHandler: self)
            
            guard let element = userData.groupsMetadata.enumerated().first(where: { $0.element.type == group.type })?.element else { return }
            interactor.sendEditEvent(with: serviceInfo.chatId, statsData: element.statsData)
        }
    }
    
    func didPressToDeleteObject(with objectId: String) {
        
        let option = serviceDetailConfiguration.selectionOptions[objectId]
        let group = serviceDetailConfiguration.selectionOptionGroups[objectId]
        
        guard option != nil || group != nil else { return }

        processingBeginTime = Date()
        view.setNavigationItemsEnabled(false)
        router.showProcessingModule()

        if option != nil {
            interactor.deleteUserDataOption(with: option!.itemId, optionType: option!.type)
        }
        else {
            interactor.deleteUserDataOptionGroup(with: group!.itemId, groupType: group!.type)
        }
    }
    
    func didPressAddGroup() {
        
        let options = userData.optionsMetadata.filter({ canAddOption(with: $0) })
        let totalCount = userData.groupsMetadata.count + options.count
        
        guard totalCount == 1 else {
            router.showMetadataTypeSelectionModule(for: userData.optionsMetadata, groups: userData.groupsMetadata, eventHandler: self)
            return
        }
    
        if let optionMetadata = options.first {
            didSelect(option: optionMetadata)
        }
        else if let groupMetadata = userData.groupsMetadata.first {
            didSelect(group: groupMetadata)
        }
    }
    
    //MARK: - UserServiceDetailInteractorOutput
    
    func didFinishObtainingUserDataOptions(with result: UserDataOptionsObtainResult) {
        switch result {
        case .success(let userDataValue):
            userData = userDataValue
            view.hideLoadingStatus()
            refreshOptionsGroups()
        case .failure:
            retryObtainUserDataPerformer.performOnMain(afterDelay: retryObtainUserDataDelay)
        }
    }
    
    func didFinishDeletingUserDataOption(with optionId: String, and result: UserDataOptionsOperationResult) {
        
        guard canHandleFinishProcessing(with: {
            self.didFinishDeletingUserDataOption(with: optionId, and: result)
        }) else { return }
        
        handleFinishDeletingUserDataOption(with: optionId,
                                           result: result,
                                           objectIdObtainer: { itemId in
            guard let objectId = serviceDetailConfiguration.selectionOptions.first(where: { $0.value.itemId == itemId })?.key else { return nil }
            serviceDetailConfiguration.selectionOptions[objectId] = nil
            return objectId
        }, onSuccess: {
            userData.options = userData.options.filter { $0.itemId != optionId }
        })
    }
    
    func didFinishDeletingUserDataOptionGroup(with groupId: String, and result: UserDataOptionsOperationResult) {

        guard canHandleFinishProcessing(with: {
            self.didFinishDeletingUserDataOptionGroup(with: groupId, and: result)
        }) else { return }
        
        handleFinishDeletingUserDataOption(with: groupId,
                                           result: result,
                                           objectIdObtainer: { itemId in
            guard let objectId = serviceDetailConfiguration.selectionOptionGroups.first(where: { $0.value.itemId == itemId })?.key else { return nil }
            serviceDetailConfiguration.selectionOptionGroups[objectId] = nil
            return objectId
        }, onSuccess: {
            userData.groups = userData.groups.filter { $0.itemId != groupId }
        })
    }
    
    //MARK: - OptionGroupTypeSelectionEventHandler
    
    func didSelect(option: UserOptionMetadata) {
        router.showAddOrEditOptionModule(with: userData, for: option.type, optionId: nil, eventHandler: self)
    }
    
    func didSelect(group: UserOptionGroupMetadata) {
        
        router.showAddOrEditOptionsGroupModule(with: userData, for: group.type, groupId: nil, eventHandler: self)
        interactor.sendAddEvent(with: serviceInfo.chatId, statsData: group.statsData)
    }
    
    //MARK: - UserServiceDetailEditModuleEventHandler
    
    func didModifyUserOptionGroups() {
        reloadOptionsGroups()
    }
    
    //MARK: - UserServiceDetailEmptyViewEventHandler
    
    func didPressToOpen(url: URL) {
        router.showSafariViewController(with: url)
    }
}

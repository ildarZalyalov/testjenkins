//
//  ServiceDetailDataSourceFactory.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 16.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

struct ServiceDetailDataSourceConfiguration {
    
    var dataSource: TableViewDataSource
    
    var selectionOptions: [String : UserOption]
    
    var selectionOptionGroups: [String : UserOptionGroup]
}

protocol ServiceDetailDataSourceFactory {
    
    func buildDataSourceConfiguration(from options: [UserOption], with metadata: [UserOptionMetadata], and groups: [UserOptionGroup], groupedWith groupings: [UserOptionGroupGrouping]) -> ServiceDetailDataSourceConfiguration
}

//
//  ServiceDetailDataSourceFactoryImplementation.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 16.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class ServiceDetailDataSourceFactoryImplementation: ServiceDetailDataSourceFactory {
    
    let emptyFooterHeight: CGFloat = 32
    
    //MARK: - Методы
    
    fileprivate func append(groups: [UserOptionGroup], to dataStructure: inout TableViewDataSourceStructure, with selectionOptionGroups: inout [String : UserOptionGroup]) {
        
        for item in groups.enumerated() {
            
            let cellObject = ServiceDetailCellObject(itemId: UUID().uuidString, title: item.element.name, subtitle: item.element.description, isLastObject: item.offset == (groups.count - 1), isTitleUserProvided: item.element.alias != nil)
            selectionOptionGroups[cellObject.itemId] = item.element
            
            dataStructure.appendCellObject(cellObject)
        }
        
        dataStructure.appendFooterObject(EmptyTableFooterObject())
    }
    
    fileprivate func append(groups: [UserOptionGroup], with groupings: [UserOptionGroupGrouping], to dataStructure: inout TableViewDataSourceStructure, with selectionOptionGroups: inout [String : UserOptionGroup]) {
        
        let groupsById = groups.toDictionary { $0.itemId }
        
        for (index, grouping) in groupings.enumerated() {
            
            let section = max(dataStructure.numberOfSections() - 1, 0)
            dataStructure.addHeaderObject(ServiceDetailHeaderObject(title: grouping.name), to: section)
            dataStructure.addFooterObject(EmptyTableFooterObject(), to: section)
            
            for (index, groupId) in grouping.groupIds.enumerated() {
                
                guard let group = groupsById[groupId] else { continue }
                
                let cellObject = ServiceDetailCellObject(itemId: UUID().uuidString, title: group.name, subtitle: group.description, isLastObject: index == (grouping.groupIds.count - 1), isTitleUserProvided: group.alias != nil)
                selectionOptionGroups[cellObject.itemId] = group
                
                dataStructure.appendCellObject(cellObject)
            }
            
            guard index < groupings.count - 1 else { continue }
            dataStructure.appendSection()
        }
    }
    
    //MARK: - ServiceDetailDataSourceFactory
    
    func buildDataSourceConfiguration(from options: [UserOption], with metadata: [UserOptionMetadata], and groups: [UserOptionGroup], groupedWith groupings: [UserOptionGroupGrouping]) -> ServiceDetailDataSourceConfiguration {
        
        var dataStructure = TableViewDataSourceStructure()
        var selectionOptions = [String : UserOption]()
        var selectionOptionGroups = [String : UserOptionGroup]()
        
        let metadataByType = metadata.toDictionary { $0.type }
        let optionsWithMetadata = options.filter { metadataByType[$0.type] != nil }
        
        for item in optionsWithMetadata.enumerated() {
            
            let title = item.element.value
            let subtitle = metadataByType[item.element.type]?.name ?? String()
            let isLastObject = groups.isEmpty && item.offset == (optionsWithMetadata.count - 1)
            
            let cellObject = ServiceDetailCellObject(itemId: UUID().uuidString, title: title, subtitle: subtitle, isLastObject: isLastObject, isTitleUserProvided: true)
            selectionOptions[cellObject.itemId] = item.element
            
            dataStructure.appendCellObject(cellObject)
        }
        
        if !optionsWithMetadata.isEmpty {
            dataStructure.appendFooterObject(EmptyTableFooterObject(footerHeight: emptyFooterHeight))
            dataStructure.appendSection()
            dataStructure.appendHeaderObject(EmptyTableHeaderObject())
        }
        
        if groupings.isEmpty {
            append(groups: groups, to: &dataStructure, with: &selectionOptionGroups)
        }
        else {
            append(groups: groups, with: groupings, to: &dataStructure, with: &selectionOptionGroups)
        }
        
        let dataSource = TableViewDataSource(with: dataStructure)
        
        return ServiceDetailDataSourceConfiguration(dataSource: dataSource, selectionOptions: selectionOptions, selectionOptionGroups: selectionOptionGroups)
    }
}

//
//  UserServiceDetailViewInput.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 16.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

enum UserServiceDetailConversationType {
    case children
    case health
}

protocol UserServiceDetailViewInput: class {
    
    func setupEmptyContentView(with type: UserServiceDetailConversationType, eventHandler: UserServiceDetailEmptyViewEventHandler)
    
    func setupBottomLinkView(with type: UserServiceDetailConversationType, eventHandler: UserServiceDetailEmptyViewEventHandler)
    
    func displayServicesInfo(with dataSource: TableViewDataSource)
    
    func deleteObject(with objectId: String)
    
    func setNavigationItemsEnabled(_ enabled: Bool)
    
    func setEditingServiceDetailDisabled()
    
    func showLoadingStatus()
    
    func hideLoadingStatus()
}

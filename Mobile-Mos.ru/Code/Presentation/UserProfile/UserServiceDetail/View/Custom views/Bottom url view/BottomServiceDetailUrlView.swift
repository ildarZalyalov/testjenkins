//
//  BottomServiceDetailUrlView.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 30.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

class BottomServiceDetailUrlView: UIView {
    
    fileprivate static let nibFile = UINib(nibName: String(describing: BottomServiceDetailUrlView.self), bundle: nil)
    
    static func loadNib() -> BottomServiceDetailUrlView {
        return BottomServiceDetailUrlView.nibFile.instantiate(withOwner: nil, options: nil).first as! BottomServiceDetailUrlView
    }
    
    @IBOutlet fileprivate weak var linkButton: UIButton!

    fileprivate let linkButtonLineHeightMultiple: CGFloat = 1.38
    
    var title: String? {
        get { return linkButton.title(for: .normal) }
        set { setLinkButtonAttributedTitle(title: newValue) }
    }
    
    var linkUrl: URL?
    weak var eventHandler: UserServiceDetailEmptyViewEventHandler?
    
    override func awakeFromNib() {
        
        linkButton.titleLabel?.numberOfLines = 0
        
        guard let labelText = linkButton.titleLabel?.text else { return }
        setLinkButtonAttributedTitle(title: labelText)
    }
    
    fileprivate func setLinkButtonAttributedTitle(title: String?) {
        
        guard let labelText = title else { return }
        guard let label = linkButton.titleLabel else { return }
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineHeightMultiple = linkButtonLineHeightMultiple
        paragraphStyle.alignment = .left
        paragraphStyle.lineBreakMode = .byTruncatingTail
        
        let attributes: [NSAttributedStringKey : Any] = [NSAttributedStringKey.font : label.font, NSAttributedStringKey.paragraphStyle : paragraphStyle, NSAttributedStringKey.underlineStyle : NSNumber(value: NSUnderlineStyle.styleSingle.rawValue), NSAttributedStringKey.underlineColor : label.textColor]
        let attributextText = NSAttributedString(string: labelText, attributes: attributes)
        
        linkButton.setAttributedTitle(attributextText, for: .normal)
    }
    
    @IBAction fileprivate func linkPressed() {
        guard let url = linkUrl else { return }
        eventHandler?.didPressToOpen(url: url)
    }
}

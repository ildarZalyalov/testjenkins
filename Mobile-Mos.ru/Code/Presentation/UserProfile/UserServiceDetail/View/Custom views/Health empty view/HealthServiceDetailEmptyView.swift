//
//  HealthServiceDetailEmptyView.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 30.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

class HealthServiceDetailEmptyView: UIView {
    
    fileprivate static let nibFile = UINib(nibName: String(describing: HealthServiceDetailEmptyView.self), bundle: nil)
    
    static func loadNib() -> HealthServiceDetailEmptyView {
        return HealthServiceDetailEmptyView.nibFile.instantiate(withOwner: nil, options: nil).first as! HealthServiceDetailEmptyView
    }
    
    static let firstLinkTitle: String = "Проверка полиса ОМС"
    static let firstLinkUrl: URL = URL(string: "http://www.mgfoms.ru/chastnye-lica/proverka-polisa")!
    
    @IBOutlet fileprivate weak var noContentLabel: UILabel!
    @IBOutlet fileprivate weak var firstLinkButton: UIButton!
    
    fileprivate let noContentLabelLineHeightMultiple: CGFloat = 1.41
    fileprivate let firstLinkButtonLineHeightMultiple: CGFloat = 1.41
    
    weak var eventHandler: UserServiceDetailEmptyViewEventHandler?
    
    override func awakeFromNib() {
        
        firstLinkButton.setTitle(HealthServiceDetailEmptyView.firstLinkTitle, for: .normal)
        firstLinkButton.titleLabel?.numberOfLines = 0
        
        setup(lineHeightMultiple: noContentLabelLineHeightMultiple, for: noContentLabel)
        setup(lineHeightMultiple: firstLinkButtonLineHeightMultiple, for: firstLinkButton.titleLabel)
    }
    
    fileprivate func setup(lineHeightMultiple: CGFloat, for label: UILabel?) {
        
        guard label != nil else { return }
        guard let labelText = label!.text else { return }
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineHeightMultiple = lineHeightMultiple
        paragraphStyle.alignment = .left
        paragraphStyle.lineBreakMode = .byTruncatingTail
        
        let attributes: [NSAttributedStringKey : Any] = [NSAttributedStringKey.font : label!.font, NSAttributedStringKey.paragraphStyle : paragraphStyle]
        let attributextText = NSAttributedString(string: labelText, attributes: attributes)
        
        label!.attributedText = attributextText
    }
    
    @IBAction fileprivate func firstLinkPressed() {
        eventHandler?.didPressToOpen(url: HealthServiceDetailEmptyView.firstLinkUrl)
    }
}

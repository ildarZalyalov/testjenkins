//
//  ChildrenServiceDetailEmptyView.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 30.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

class ChildrenServiceDetailEmptyView: UIView {
    
    fileprivate static let nibFile = UINib(nibName: String(describing: ChildrenServiceDetailEmptyView.self), bundle: nil)
    
    static func loadNib() -> ChildrenServiceDetailEmptyView {
        return ChildrenServiceDetailEmptyView.nibFile.instantiate(withOwner: nil, options: nil).first as! ChildrenServiceDetailEmptyView
    }
    static let electronnyDnevnikURLString = "https://www.mos.ru/otvet-obrazovanie/kak-voyti-v-elektronnyy-dnevnik/"
    static let sppInstructionURLString = "https://www.mos.ru/otvet-obrazovanie/kak-polzovatsya-servisom-moskvyonok/#4"
    
    @IBOutlet fileprivate weak var firstLinkNoContentLabel: UILabel!
    @IBOutlet fileprivate weak var noContentLabel: UILabel!
    @IBOutlet fileprivate weak var firstLinkButton: UIButton!
    @IBOutlet fileprivate weak var secondLinkNoContentLabel: UILabel!
    @IBOutlet fileprivate weak var secondLinkButton: UIButton!
    
    fileprivate let noContentLabelLineHeightMultiple: CGFloat = 1.41
    fileprivate let firstLinkButtonLineHeightMultiple: CGFloat = 1.41
    fileprivate let secondLinkButtonLineHeightMultiple: CGFloat = 1.41
    
    weak var eventHandler: UserServiceDetailEmptyViewEventHandler?
    
    override func awakeFromNib() {
        
        firstLinkButton.titleLabel?.numberOfLines = 0
        secondLinkButton.titleLabel?.numberOfLines = 0
        
        setup(lineHeightMultiple: noContentLabelLineHeightMultiple, for: firstLinkNoContentLabel)
        setup(lineHeightMultiple: firstLinkButtonLineHeightMultiple, for: firstLinkButton.titleLabel)
        setup(lineHeightMultiple: noContentLabelLineHeightMultiple, for: secondLinkNoContentLabel)
        setup(lineHeightMultiple: secondLinkButtonLineHeightMultiple, for: secondLinkButton.titleLabel)
    }
    
    fileprivate func setup(lineHeightMultiple: CGFloat, for label: UILabel?) {
        
        guard label != nil else { return }
        guard let labelText = label!.text else { return }
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineHeightMultiple = lineHeightMultiple
        paragraphStyle.alignment = .left
        paragraphStyle.lineBreakMode = .byTruncatingTail
        
        let attributes: [NSAttributedStringKey : Any] = [NSAttributedStringKey.font : label!.font, NSAttributedStringKey.paragraphStyle : paragraphStyle]
        let attributextText = NSAttributedString(string: labelText, attributes: attributes)
        
        label!.attributedText = attributextText
    }
    
    @IBAction fileprivate func firstLinkPressed() {
        guard let url = URL(string: ChildrenServiceDetailEmptyView.electronnyDnevnikURLString) else { return }
        eventHandler?.didPressToOpen(url: url)
    }
    
    @IBAction fileprivate func secondLinkPressed() {
        guard let url = URL(string: ChildrenServiceDetailEmptyView.sppInstructionURLString) else { return }
        eventHandler?.didPressToOpen(url: url)
    }
}

//
//  UserServiceDetailViewOutput.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 16.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol UserServiceDetailViewOutput: class {
    
    func setupInitialState()
    
    func handleViewFirstDisplayed()
    
    func configure(with serviceInfo: UserServiceInfo)
    
    func didSelectObject(with objectId: String)
    
    func didPressToDeleteObject(with objectId: String)
    
    func didPressAddGroup()
}

//
//  ServiceDetailCell.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 16.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import UIKit

class ServiceDetailCell: UITableViewCell, ConfigurableView {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var bottomSeparatorView: UIView!
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        contentView.alpha = highlighted ? 0.5 : 1
    }
    
    //MARK: - ConfigurableView
    
    func configure(with object: Any) {
        
        guard let cellObject = object as? ServiceDetailCellObject else {
            return
        }
        
        titleLabel.text = cellObject.title
        subtitleLabel.text = cellObject.subtitle
        bottomSeparatorView.isHidden = cellObject.isLastObject
        
        titleLabel.font = cellObject.isTitleUserProvided ? UIFont(customName: .graphikLCGMedium, size: 17) : UIFont(customName: .graphikLCGRegular, size: 17)
    }
}

//
//  ServiceDetailCellObject.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 16.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

struct ServiceDetailCellObject: CellObjectWithId {
    
    var itemId: String
    
    var title: String
    
    var subtitle: String
    
    var isLastObject: Bool
    
    var isTitleUserProvided: Bool
}

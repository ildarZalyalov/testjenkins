//
//  ServiceDetailHeader.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 26.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

class ServiceDetailHeader: UITableViewHeaderFooterView, ConfigurableView {
    
    @IBOutlet weak var sectionTitleLabel: UILabel!
    @IBOutlet weak var sectionTitleLabelHeightConstraint: NSLayoutConstraint!
    
    let sectionTitleLabelDefaultHeight: CGFloat = 20
    
    func configure(with object: Any) {
        
        guard let cellObject = object as? ServiceDetailHeaderObject else {
            return
        }
        
        sectionTitleLabel.text = cellObject.title
        sectionTitleLabelHeightConstraint.constant = cellObject.title.isEmpty ? 0 : sectionTitleLabelDefaultHeight
    }
}

//
//  ServiceDetailHeaderObject.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 26.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

struct ServiceDetailHeaderObject: HeaderObject {
    
    /// Текст заголовка
    var title: String
}

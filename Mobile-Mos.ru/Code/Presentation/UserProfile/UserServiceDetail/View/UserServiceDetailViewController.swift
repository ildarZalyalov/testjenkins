//
//  UserServiceDetailViewController.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 16.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import UIKit

class UserServiceDetailViewController: BaseViewController, UserServiceDetailViewInput, ConfigurableModuleController, TableViewDataSourceDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var bottomViewWithButton: UIView!
    @IBOutlet weak var loadingIndicatorView: UIActivityIndicatorView!
    @IBOutlet weak var emptyContentLabel: UILabel!
    @IBOutlet weak var customEmptyContentViewContainer: UIView!
    @IBOutlet weak var bottomUrlViewContainer: UIView!
    
    @IBOutlet weak var bottomViewWithButtonBottomConstraint: NSLayoutConstraint!
    
    var output: UserServiceDetailViewOutput!
    var tableViewDataDisplayManager: TableViewDataSource!
    
    let tableViewTopInset: CGFloat = 34
    let emptyContentLabelLineHeightMultiple: CGFloat = 1.41
    
    var editingStyle: UITableViewCellEditingStyle = .delete
    
    var isInitialized = false
    
    //MARK: - Методы
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
    
        tableView.estimatedRowHeight = 72
        tableView.estimatedSectionHeaderHeight = 58
        tableView.estimatedSectionFooterHeight = 1
        
        tableView.registerCellNib(for: ServiceDetailCellObject.self)
        tableView.registerHeaderNib(for: ServiceDetailHeaderObject.self)
        tableView.registerEmptyHeader()
        tableView.registerEmptyFooter()
        
        tableView.assignEmptyTableFooterToHideEmptyCellsSeparators()
        
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.width, height: tableViewTopInset))
        headerView.backgroundColor = view.backgroundColor
        tableView.tableHeaderView = headerView
        
        let insets = UIEdgeInsets(top: 0, left: 0, bottom: bottomViewWithButton.height, right: 0)
        tableView.contentInset = insets
        tableView.scrollIndicatorInsets = insets
        
        if let text = emptyContentLabel.text {
            
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.lineHeightMultiple = emptyContentLabelLineHeightMultiple
            paragraphStyle.alignment = .left
            
            emptyContentLabel.attributedText = NSAttributedString(string: text, attributes: [NSAttributedStringKey.font : emptyContentLabel.font, NSAttributedStringKey.paragraphStyle : paragraphStyle])
        }
        
        output.setupInitialState()
    }
    
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        
        guard !isInitialized else { return }
        isInitialized = true
        
        output.handleViewFirstDisplayed()
    }
    
    func updateNavigationBar() {
        
        let tableIsEmpty = tableViewDataDisplayManager?.dataStructure.isEmpty() ?? true
        guard !tableIsEmpty else {
            navigationItem.rightBarButtonItem = nil
            return
        }
        
        var item: UIBarButtonItem
        var attributes: [NSAttributedStringKey : Any]
        
        if tableView.isEditing {
            item = UIBarButtonItem(title: StringsHelper.doneChangingUserDataListButtonText, style: .done, target: self, action: #selector(doneButtonPressed))
            attributes = [NSAttributedStringKey.font : UIFont(customName: .graphikLCGMedium, size: 17) as Any]
        }
        else {
            item = UIBarButtonItem(title: StringsHelper.changeUserDataListButtonText, style: .plain, target: self, action: #selector(changeButtonPressed))
            attributes = [NSAttributedStringKey.font : UIFont(customName: .graphikLCGRegular, size: 17) as Any]
        }
        
        item.tintColor = UIColorPalette.editBarButtonColor
        item.setTitleTextAttributes(attributes, for: .normal)
        item.setTitleTextAttributes(attributes, for: .highlighted)
        item.setTitleTextAttributes(attributes, for: .disabled)
        
        navigationItem.rightBarButtonItem = item
    }
    
    func updateEmptyContentView() {
        
        let tableIsEmpty = tableViewDataDisplayManager?.dataStructure.isEmpty() ?? false
        let hasCustomEmptyView = customEmptyContentViewContainer.subviews.count > 0
        
        if hasCustomEmptyView {
            customEmptyContentViewContainer.isHidden = !tableIsEmpty
        }
        else {
            emptyContentLabel.isHidden = !tableIsEmpty
        }
        
        bottomUrlViewContainer.isHidden = bottomUrlViewContainer.subviews.count == 0 || tableIsEmpty
        
        guard tableIsEmpty && !tableView.isHidden && tableView.isEditing else {
            tableView.isHidden = tableIsEmpty
            return
        }
        
        DispatchQueue.main.async {
            self.tableView.setEditing(false, animated: false)
            self.tableView.isHidden = true
        }
    }
    
    //MARK: - Actions
    
    @IBAction func unwindToUserServiceDetail(_: UIStoryboardSegue) {}
    
    @IBAction func addGroupButtonPressed(_ sender: Any) {
        output.didPressAddGroup()
    }
    
    @objc func changeButtonPressed() {
        
        if tableView.isEditing {
            tableView.setEditing(false, animated: false)
        }
        
        tableView.setEditing(true, animated: true)
        updateNavigationBar()
    }
    
    @objc func doneButtonPressed() {
        tableView.setEditing(false, animated: true)
        updateNavigationBar()
    }
    
    //MARK: - ConfigurableModuleController
    
    func configureModule(with object: Any) {
        if let serviceObject = object as? UserServiceInfo {
            navigationItem.title = serviceObject.title
            output.configure(with: serviceObject)
        }
    }
    
    //MARK: - UserServiceDetailViewInput
    
    func setupEmptyContentView(with type: UserServiceDetailConversationType, eventHandler: UserServiceDetailEmptyViewEventHandler) {
        
        var customEmptyView: UIView? = nil
        
        switch type {
        case .children:
            let childrenView = ChildrenServiceDetailEmptyView.loadNib()
            childrenView.eventHandler = eventHandler
            customEmptyView = childrenView
        case .health:
            let healthView = HealthServiceDetailEmptyView.loadNib()
            healthView.eventHandler = eventHandler
            customEmptyView = healthView
        }
        
        guard customEmptyView != nil else { return }
        
        customEmptyContentViewContainer.addSubview(customEmptyView!)
        customEmptyView!.translatesAutoresizingMaskIntoConstraints = false
       
        let constaints = NSLayoutConstraint.edgesConstraints(for: customEmptyView!)
        customEmptyContentViewContainer.addConstraints(constaints)
    }
    
    func setupBottomLinkView(with type: UserServiceDetailConversationType, eventHandler: UserServiceDetailEmptyViewEventHandler) {
        
        var title: String
        var url: URL
        
        switch type {
        case .health:
            title = HealthServiceDetailEmptyView.firstLinkTitle
            url = HealthServiceDetailEmptyView.firstLinkUrl
        default:
            return
        }
        
        let bottomView = BottomServiceDetailUrlView.loadNib()
        bottomView.title = title
        bottomView.linkUrl = url
        bottomView.eventHandler = eventHandler
        
        bottomUrlViewContainer.addSubview(bottomView)
        bottomView.translatesAutoresizingMaskIntoConstraints = false
        
        let constaints = NSLayoutConstraint.edgesConstraints(for: bottomView)
        bottomUrlViewContainer.addConstraints(constaints)
    }
    
    func displayServicesInfo(with dataSource: TableViewDataSource) {
        
        tableViewDataDisplayManager = dataSource
        tableViewDataDisplayManager.delegate = self
        tableViewDataDisplayManager.assign(to: tableView)
        
        tableView.reloadData()
        
        updateNavigationBar()
        updateEmptyContentView()
    }
    
    func deleteObject(with objectId: String) {
        
        let dataStructure = tableViewDataDisplayManager.dataStructure
        guard let indexPath = dataStructure.indexPathOfFirstObjectPassing({
            guard let cellObject = $0 as? CellObjectWithId else { return false }
            return cellObject.itemId == objectId
        }) else { return }
        
        dataStructure.removeCellObject(at: indexPath)
    
        if dataStructure.numberOfObjects(at: indexPath.section) == 0 {
            dataStructure.removeSections(at: [indexPath])
            tableView.deleteSections([indexPath.section], with: .left)
        }
        else {
            tableView.deleteRows(at: [indexPath], with: .left)
        }
        
        updateNavigationBar()
        updateEmptyContentView()
    }
    
    func setNavigationItemsEnabled(_ enabled: Bool) {
        navigationItem.rightBarButtonItem?.isEnabled = enabled
    }
    
    func setEditingServiceDetailDisabled() {
        
        bottomViewWithButtonBottomConstraint.constant -= bottomViewWithButton.height
        bottomViewWithButton.setNeedsLayout()
        bottomViewWithButton.layoutIfNeeded()
        
        navigationItem.rightBarButtonItem = nil
        
        let insets = UIEdgeInsets.zero
        tableView.contentInset = insets
        tableView.scrollIndicatorInsets = insets
        
        editingStyle = .none
    }
    
    func showLoadingStatus() {
        navigationItem.rightBarButtonItem = nil
        tableView.isHidden = true
        bottomViewWithButton.isHidden = true
        emptyContentLabel.isHidden = true
        loadingIndicatorView.startAnimating()
    }
    
    func hideLoadingStatus() {
        loadingIndicatorView.stopAnimating()
        updateNavigationBar()
        updateEmptyContentView()
        tableView.isHidden = false
        bottomViewWithButton.isHidden = false
    }
    
    //MARK: - TableViewDataSourceDelegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        let dataStructure = tableViewDataDisplayManager.dataStructure
        guard let cellObject = dataStructure.cellObject(at: indexPath) as? CellObjectWithId else {
            return
        }
        
        output.didSelectObject(with: cellObject.itemId)
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return editingStyle
    }
    
    func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
        return StringsHelper.deleteItemInUserDataListButtonText
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        let dataStructure = tableViewDataDisplayManager.dataStructure
        guard let cellObject = dataStructure.cellObject(at: indexPath) as? CellObjectWithId else {
            return
        }
        
        output.didPressToDeleteObject(with: cellObject.itemId)
    }
    
    func tableView(_ tableView: UITableView, willDisplayFooterView view: UIView, forSection section: Int) {
        if let emptyFooter = view as? EmptyTableFooterView {
            emptyFooter.contentView.backgroundColor = tableView.backgroundColor
        }
    }
}

//
//  UserServiceDetailAlertFactoryImplementation.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 16.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class UserServiceDetailAlertFactoryImplementation: UserServiceDetailAlertFactory {
    
    func getMetadataTypeSelectionActionSheet(from options: [UserOptionMetadata], groups: [UserOptionGroupMetadata], eventHandler: OptionGroupTypeSelectionEventHandler?) -> UIViewController {
        
        let alert = UIAlertController(title: nil, message:nil, preferredStyle: .actionSheet)
        
        for option in options {
            alert.addAction(UIAlertAction(title: option.name, style: .default, handler: { (alert: UIAlertAction!) in
                eventHandler?.didSelect(option: option)
            }))
        }
        
        for group in groups {
            alert.addAction(UIAlertAction(title: group.name, style: .default, handler: { (alert: UIAlertAction!) in
                eventHandler?.didSelect(group: group)
            }))
        }
        
        alert.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: nil))
        
        return alert
    }
}

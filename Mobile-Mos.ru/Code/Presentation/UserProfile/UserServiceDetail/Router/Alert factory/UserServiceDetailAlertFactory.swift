//
//  UserServiceDetailAlertFactory.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 16.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol OptionGroupTypeSelectionEventHandler {
    
    func didSelect(option: UserOptionMetadata)
    
    func didSelect(group: UserOptionGroupMetadata)
}

protocol UserServiceDetailAlertFactory {
    
    func getMetadataTypeSelectionActionSheet(from options: [UserOptionMetadata], groups: [UserOptionGroupMetadata], eventHandler: OptionGroupTypeSelectionEventHandler?) -> UIViewController
}

//
//  UserServiceDetailRouterInput.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 16.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol UserServiceDetailRouterInput: class {
    
    func closeCurrentModule()

    func showProcessingModule()
    
    func hideProcessingModule()
    
    func showConnectionErrorAndCloseModule()
    
    func showDeletingOptionsErrorModule()
    
    func showMetadataTypeSelectionModule(for options: [UserOptionMetadata], groups: [UserOptionGroupMetadata], eventHandler: OptionGroupTypeSelectionEventHandler?)
    
    func showAddOrEditOptionModule(with userData: UserOptionsData,
                                   for optionType: String,
                                   optionId: String?,
                                   eventHandler: UserServiceDetailEditModuleEventHandler?)
    
    func showAddOrEditOptionsGroupModule(with userData: UserOptionsData,
                                         for groupType: String,
                                         groupId: String?,
                                         eventHandler: UserServiceDetailEditModuleEventHandler?)
    
    func showSafariViewController(with url: URL)
}

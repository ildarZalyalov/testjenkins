//
//  UserServiceDetailRouter.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 16.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import SafariServices

class UserServiceDetailRouter: NSObject, UserServiceDetailRouterInput, SFSafariViewControllerDelegate {
    
    weak var transitionHandler: UIViewController!
    weak var alertTransitionHandler: UIViewController!
    weak var toOtherModulesTransitionHandler: DataTransferModuleController!
    
    var alertFactory: UserServiceDetailAlertFactory!
    var commonAlertsFactory: CommonAlertsFactory!
    var storyboardFactory: StoryboardFactory!
    var analyticsManager: AnalyticsManager!
    
    let showUserServiceDetailEditSegueIdentifier = "showUserServiceDetailEdit"
    let unwindToUserProfileSegueIdentifier = "unwindToUserProfileSegue"
    
    //MARK: - UserServiceDetailRouterInput
    
    func closeCurrentModule() {
        transitionHandler.performSegue(withIdentifier: unwindToUserProfileSegueIdentifier, sender: nil)
    }
    
    func showProcessingModule() {
        
        let storyboard = storyboardFactory.getStoryboard(with: .userProfile)
        let processingController = storyboard.instantiateViewController(for: .userDataProcessing)
        
        processingController.modalPresentationStyle = .overCurrentContext
        processingController.modalTransitionStyle = .crossDissolve
        processingController.modalPresentationCapturesStatusBarAppearance = false
        
        transitionHandler.definesPresentationContext = true
        transitionHandler.present(processingController, animated: true, completion: nil)
    }
    
    func hideProcessingModule() {
        transitionHandler.dismiss(animated: true, completion: nil)
        transitionHandler.definesPresentationContext = false
    }
    
    func showConnectionErrorAndCloseModule() {
        let alert = commonAlertsFactory.getErrorAlert(with: nil, body: StringsHelper.noConnectionErrorText) { [weak self] _ in
            self?.closeCurrentModule()
        }
        alertTransitionHandler.present(alert, animated: true, completion: nil)
    }
    
    func showDeletingOptionsErrorModule() {
        let alert = commonAlertsFactory.getErrorAlert(with: StringsHelper.deleteItemInUserDataErrorAlertText)
        alertTransitionHandler.present(alert, animated: true, completion: nil)
    }
    
    func showMetadataTypeSelectionModule(for options: [UserOptionMetadata], groups: [UserOptionGroupMetadata], eventHandler: OptionGroupTypeSelectionEventHandler?) {
        let actioSheet = alertFactory.getMetadataTypeSelectionActionSheet(from: options, groups: groups, eventHandler: eventHandler)
        alertTransitionHandler.present(actioSheet, animated: true, completion: nil)
    }
    
    func showAddOrEditOptionModule(with userData: UserOptionsData,
                                   for optionType: String,
                                   optionId: String?,
                                   eventHandler: UserServiceDetailEditModuleEventHandler?) {
        
        toOtherModulesTransitionHandler.performSegue(with: showUserServiceDetailEditSegueIdentifier, sender: self) { controller in
            
            var configuration = UserServiceDetailEditModuleConfiguration(userData: userData, metadataType: .option(optionType: optionType, optionId: optionId))
            configuration.eventHandler = eventHandler
            
            controller.configureModule(with: configuration)
        }
        
    }
    
    func showAddOrEditOptionsGroupModule(with userData: UserOptionsData,
                                         for groupType: String,
                                         groupId: String?,
                                         eventHandler: UserServiceDetailEditModuleEventHandler?) {
        
        toOtherModulesTransitionHandler.performSegue(with: showUserServiceDetailEditSegueIdentifier, sender: self) { controller in
            
            var configuration = UserServiceDetailEditModuleConfiguration(userData: userData, metadataType: .group(groupType: groupType, selectedGroupId: groupId))
            configuration.eventHandler = eventHandler
            
            controller.configureModule(with: configuration)
        }
    }
    
    func showSafariViewController(with url: URL) {
        
        let safariController = SFSafariViewController(url: url, entersReaderIfAvailable: true)
        safariController.delegate = self
        
        analyticsManager.setSelectedUrl(with: url)
        
        if #available(iOS 10.0, *) {
            safariController.preferredControlTintColor = UIColorPalette.browserControlsDefaultTintColor
        }
        
        transitionHandler.present(safariController, animated: true, completion: nil)
    }
    
    fileprivate func sendOpenLinkEvent(with chatId: String?, url: URL, errorText: String) {
        
        guard let chat = ConversationIdentificator.init(rawValue: chatId ?? "") else { return }
        
        var childrenEventName = AnalyticsConstants.screenLoadedEventName
        
        if url.absoluteString == ChildrenServiceDetailEmptyView.electronnyDnevnikURLString {
            childrenEventName = AnalyticsConstants.childESHDLinkEventName
        }
        else if url.absoluteString == ChildrenServiceDetailEmptyView.sppInstructionURLString {
            childrenEventName = AnalyticsConstants.childIsppLinkEventName
        }
        
        switch chat {
        case .children:
            analyticsManager.sendServiceEvent(with: CategoryType.childrenService, event:.openLinkInstrusction(withString: childrenEventName), errorText: errorText)
        case .health:
            analyticsManager.sendServiceEvent(with: CategoryType.medecineInsuranceService, event: .openLinkInstrusction(withString: AnalyticsConstants.checkMedicalInsuranceLinkEventName), errorText: errorText)
        default:
            return
        }
    }
    
    //MARK: - SFSafariViewControllerDelegate
    
    func safariViewController(_ controller: SFSafariViewController, didCompleteInitialLoad didLoadSuccessfully: Bool) {
        
        guard let chatId = analyticsManager.chatIdentifier, let selectedUrl = analyticsManager.selectedUrl else { return }
        let errorText = didLoadSuccessfully ? AnalyticsConstants.emptyValueName : AnalyticsConstants.notLoadedErrorValue
        
        sendOpenLinkEvent(with: chatId, url: selectedUrl, errorText: errorText)
    }
}

//
//  UserProfileNotificationsRouter.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 07.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class UserProfileNotificationsRouter: UserProfileNotificationsRouterInput {
    
    weak var transitionHandler: UIViewController!
    var alertsFactory: CommonAlertsFactory!
    
    //MARK: - UserProfileNotificationsRouterInput
    
    func showChangingNotificationSettingErrorModule() {
        let alert = alertsFactory.getErrorAlert(with: StringsHelper.changingNotificationSettingsErrorAlertText)
        transitionHandler.present(alert, animated: true, completion: nil)
    }
    
    func goToAppSettings() {
        if let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) {
            UIApplication.shared.open(url: settingsUrl)
        }
    }
}

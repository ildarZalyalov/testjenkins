//
//  UserProfileNotificationsViewInput.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 07.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol UserProfileNotificationsViewInput: class {
    
    func displayUserProfileNotifications(with dataSource: TableViewDataSource)
    
    func showLoadingStatus()
    
    func hideLoadingStatus()
    
    func showNotificationsDisabledStatus()
    
    func hideNotificationsDisabledStatus()
}

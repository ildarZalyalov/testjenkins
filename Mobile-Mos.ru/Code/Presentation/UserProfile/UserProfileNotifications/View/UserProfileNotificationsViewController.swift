//
//  UserProfileNotificationsViewController.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 07.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import UIKit

class UserProfileNotificationsViewController: BaseViewController, UserProfileNotificationsViewInput {
    
    var output: UserProfileNotificationsViewOutput!
    var tableViewDataDisplayManager: TableViewDataSource!

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var loadingIndicatorView: UIActivityIndicatorView!
    @IBOutlet weak var notificationsDisabledView: UIView!
    
    //MARK: - Методы
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        tableView.registerCellNib(for: NotificationSettingsCellObject.self)
        tableView.estimatedRowHeight = 50
        
        output.setupInitialState()
    }
    
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    @IBAction func pressedToGoToAppSettings() {
        output.didPressToGoToAppSettings()
    }

    //MARK: - UserProfileNotificationsViewInput
    
    func displayUserProfileNotifications(with dataSource: TableViewDataSource) {
        
        tableViewDataDisplayManager = dataSource
        tableViewDataDisplayManager.assign(to: tableView)
        
        tableView.reloadData()
    }
    
    func showLoadingStatus() {
        tableView.isHidden = true
        loadingIndicatorView.startAnimating()
    }
    
    func hideLoadingStatus() {
        loadingIndicatorView.stopAnimating()
        tableView.isHidden = false
    }
    
    func showNotificationsDisabledStatus() {
        tableView.isHidden = true
        notificationsDisabledView.isHidden = false
    }
    
    func hideNotificationsDisabledStatus() {
        notificationsDisabledView.isHidden = true
        tableView.isHidden = false
    }
}

//
//  NotificationSettingsCell.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 09.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import UIKit

class NotificationSettingsCell: UITableViewCell, ConfigurableView {
    
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var bottomSeparatorView: UIView!
    @IBOutlet weak var statusSwitch: UISwitch!
    @IBOutlet weak var loadingIndicatorView: UIActivityIndicatorView!
    
    var itemId: String!
    weak var eventHandler: NotificationsSettingsEventHandler?
    
    override func awakeFromNib() {
        statusSwitch.addTarget(self, action: #selector(switchValueChanged), for: .valueChanged)
    }
    
    @objc
    func switchValueChanged() {
        eventHandler?.didChangeSetting(for: itemId)
    }
    
    //MARK: - ConfigurableView
    
    func configure(with object: Any) {
        
        guard let cellObject = object as? NotificationSettingsCellObject else {
            return
        }
        
        itemId = cellObject.itemId
        eventHandler = cellObject.eventHandler
        
        iconImageView.image = cellObject.icon
        
        if iconImageView.image == nil {
            
            var placeholder: UIImage? = nil
            if let iconName = cellObject.iconName {
                placeholder = UIImage(named: iconName)
            }
            
            iconImageView.loadAndDisplayImage(from: cellObject.iconUrl, placeholder: placeholder)
        }
        
        titleLabel.text = cellObject.title
        bottomSeparatorView.isHidden = cellObject.isLastCellInSection
        
        statusSwitch.isOn = cellObject.isSwitchOn
        statusSwitch.isHidden = cellObject.isShowingLoadingStatus
        
        if cellObject.isShowingLoadingStatus {
            loadingIndicatorView.startAnimating()
        }
        else {
            loadingIndicatorView.stopAnimating()
        }
    }
}

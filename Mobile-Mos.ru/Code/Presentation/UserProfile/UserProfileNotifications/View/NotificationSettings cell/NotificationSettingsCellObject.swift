//
//  NotificationSettingsCellObject.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 09.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Cell object ячейки со статусом настройки уведомлений для чата
struct NotificationSettingsCellObject: CellObjectWithId {
    
    /// Идентификатор ячейки
    var itemId: String
    
    /// Иконка (если есть)
    var icon: UIImage?
    
    /// Ссылка на иконку (если есть)
    var iconUrl: URL?
    
    /// Название иконки (если есть)
    var iconName: String?
    
    /// Заголовок
    var title: String
    
    /// Последняя ли ячейка в секции
    var isLastCellInSection: Bool
    
    /// Включен ли свитч
    var isSwitchOn: Bool
    
    /// Отображается ли вместо свичта индикатор загрузки
    var isShowingLoadingStatus: Bool
    
    /// Обработчик изменения значения в свитче
    weak var eventHandler: NotificationsSettingsEventHandler?
}

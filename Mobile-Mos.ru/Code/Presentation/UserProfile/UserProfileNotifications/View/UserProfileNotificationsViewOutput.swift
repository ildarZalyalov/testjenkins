//
//  UserProfileNotificationsViewOutput.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 07.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol UserProfileNotificationsViewOutput: class {
    
    func setupInitialState()
    
    func didPressToGoToAppSettings()
}

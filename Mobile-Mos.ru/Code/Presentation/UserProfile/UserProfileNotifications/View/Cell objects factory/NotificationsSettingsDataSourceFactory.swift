//
//  NotificationsSettingsDataSourceFactory.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 09.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Обработчик изменения значения настройки уведомлений в ячейке
protocol NotificationsSettingsEventHandler: class {
    
    /// Изменилось значение настройки уведомлений в ячейке
    ///
    /// - Parameter itemId: идентификатор ячейки
    func didChangeSetting(for itemId: String)
}

/// Конфигурация источника данных экрана настройки уведомлений
struct NotificationsSettingsDataSourceConfiguration {
    
    /// Источник данных
    var dataSource: TableViewDataSource
    
    /// Словарь с содержимым секций по идентификатору
    var selectionItems: [String : Conversation]
}

/// Фабрика источника данных экрана настройки уведомлений
protocol NotificationsSettingsDataSourceFactory {
    
    /// Построить конфигурацию источника данных экрана настройки уведомлений
    ///
    /// - Parameters:
    ///   - settings: набор доступных настроек уведомлений
    ///   - conversations: список доступных чатов
    ///   - eventHandler: обработчик изменения значения настройки уведомлений в ячейке
    /// - Returns: конфигурацию источника данных
    func buildDataSourceConfiguration(from settings: [UserNotificationsSetting], for conversations: [Conversation], eventHandler: NotificationsSettingsEventHandler?) -> NotificationsSettingsDataSourceConfiguration
}

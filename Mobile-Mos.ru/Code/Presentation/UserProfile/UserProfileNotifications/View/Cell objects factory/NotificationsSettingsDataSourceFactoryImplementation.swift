//
//  NotificationsSettingsDataSourceFactoryImplementation.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 09.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class NotificationsSettingsDataSourceFactoryImplementation: NotificationsSettingsDataSourceFactory {
    
    //MARK: - NotificationsSettingsDataSourceFactory
    
    func buildDataSourceConfiguration(from settings: [UserNotificationsSetting], for conversations: [Conversation], eventHandler: NotificationsSettingsEventHandler?) -> NotificationsSettingsDataSourceConfiguration {
        
        let dataStructure = TableViewDataSourceStructure()
        var selectionItems = [String : Conversation]()
        var sectionItems = [NotificationSettingsCellObject]()
        
        let settingsByConversationId = settings.toDictionary { $0.chatId }
        
        for item in conversations.enumerated() {
            
            let isLastCellInSection = item.offset == (conversations.count - 1)
            let setting = settingsByConversationId[item.element.itemId]?.areNotificationsEnabled
            
            let cellObject = NotificationSettingsCellObject(itemId: UUID().uuidString, icon: nil, iconUrl: item.element.avatarMiniatureUrl, iconName: item.element.avatarPlaceholderMiniatureName, title: item.element.title, isLastCellInSection: isLastCellInSection, isSwitchOn: setting ?? false, isShowingLoadingStatus: setting == nil, eventHandler: eventHandler)
            
            selectionItems[cellObject.itemId] = item.element
            sectionItems.append(cellObject)
        }
        
        dataStructure.appendSection(with: sectionItems)
        let dataSource = TableViewDataSource(with: dataStructure)
        
        return NotificationsSettingsDataSourceConfiguration(dataSource: dataSource, selectionItems: selectionItems)
    }
}

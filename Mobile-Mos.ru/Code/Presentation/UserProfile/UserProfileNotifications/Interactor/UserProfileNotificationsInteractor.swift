//
//  UserProfileNotificationsInteractor.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 07.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class UserProfileNotificationsInteractor: UserProfileNotificationsInteractorInput {
    
    weak var output: UserProfileNotificationsInteractorOutput!
    
    var userSettingsService: UserInfoAndSettingsService!
    var conversationsService: ConversationsService!
    var analyticsManager: AnalyticsManager!
    
    //MARK: - UserProfileNotificationsInteractorInput
    
    func obtainNotificationsAuthorized() {
        userSettingsService.obtainNotificationsAuthorizationStatus { [weak self] status in
            switch status {
            case .denied, .notDetermined:
                self?.output.didFinishObtainingNotificationsAuthorized(with: false)
            case .authorized:
                self?.output.didFinishObtainingNotificationsAuthorized(with: true)
            }
        }
    }
    
    func obtainSettings() {
        
        let user = userSettingsService.currentUser
        
        conversationsService.obtainCachedConversations(for: user) { [weak self] conversationsResult in
            
            guard let strongSelf = self else { return }
            
            switch conversationsResult {
                
            case .success(var conversations):
                
                strongSelf.userSettingsService.refreshNotificationSettings(completion: { [weak self] settingsResult in
                    
                    guard let strongSelf = self else { return }
                    
                    switch settingsResult {
                        
                    case .success(let settings):
                        
                        let settingsByChatId = settings.toDictionary { $0.chatId }
                        conversations = conversations.filter { settingsByChatId[$0.itemId] != nil }
                        
                        strongSelf.output.didFinishObtainingSettings(with: settings, for: conversations)
                        
                    case .failure(let error):
                        strongSelf.output.didFinishObtainingSettings(with: error)
                    }
                })
            case .failure(let error):
                strongSelf.output.didFinishObtainingSettings(with: error)
            }
        }
    }
    
    func changeSetting(for conversation: Conversation, to value: Bool) {
        userSettingsService.changeNotificationSetting(for: conversation, to: value) { [weak self] result in
            self?.output.didFinishChangingSetting(for: conversation, to: value, successfully: result)
        }
    }
    
    func sentEvent(with event: NotificationServiceEvents, settings: [UserNotificationsSetting]) {
        
        var notificationData: NotificationEventData = (true,true,true,true,true,true)
        
        for setting in settings {
            
            switch setting.chatId {
            case ConversationIdentificator.qa.rawValue:
                notificationData.consultationNotificationEnabled = setting.areNotificationsEnabled
            case ConversationIdentificator.children.rawValue:
                notificationData.childrenNotificationEnabled = setting.areNotificationsEnabled
            case ConversationIdentificator.transport.rawValue:
                notificationData.transportNotificationEnabled = setting.areNotificationsEnabled
            case ConversationIdentificator.health.rawValue:
                notificationData.healthNotificationEnabled = setting.areNotificationsEnabled
            case ConversationIdentificator.cityNews.rawValue:
                notificationData.newsNotificationEnabled = setting.areNotificationsEnabled
            case ConversationIdentificator.myHome.rawValue:
                notificationData.myHomeNotificationEnabled = setting.areNotificationsEnabled
                
            default:
                continue
            }
        }
        
        analyticsManager.sendNotificationServiceEvent(with: event, data: notificationData)
    }
}

//
//  UserProfileNotificationsInteractorOutput.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 07.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol UserProfileNotificationsInteractorOutput: class {
    
    func didFinishObtainingNotificationsAuthorized(with authorized: Bool)
    
    func didFinishObtainingSettings(with settings: [UserNotificationsSetting], for conversations: [Conversation])
    
    func didFinishObtainingSettings(with error: Error)
    
    func didFinishChangingSetting(for conversation: Conversation, to value: Bool, successfully: Bool)
}

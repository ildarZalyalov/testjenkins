//
//  UserProfileNotificationsInteractorInput.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 07.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol UserProfileNotificationsInteractorInput: class {
    
    func obtainNotificationsAuthorized()
    
    func obtainSettings()
    
    func changeSetting(for conversation: Conversation, to value: Bool)
    
    func sentEvent(with event: NotificationServiceEvents, settings: [UserNotificationsSetting])
}

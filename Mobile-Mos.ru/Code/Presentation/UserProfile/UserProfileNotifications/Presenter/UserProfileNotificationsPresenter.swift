//
//  UserProfileNotificationsPresenter.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 07.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class UserProfileNotificationsPresenter: UserProfileNotificationsViewOutput, UserProfileNotificationsInteractorOutput, NotificationsSettingsEventHandler {
    
    weak var view: UserProfileNotificationsViewInput!
    var router: UserProfileNotificationsRouterInput!
    var interactor: UserProfileNotificationsInteractorInput!
    
    var notificationCenter: NotificationCenter!
    
    var userProfileNotificationFactory: NotificationsSettingsDataSourceFactory!
    var userProfileNotificationFactoryConfiguration: NotificationsSettingsDataSourceConfiguration!
    
    let retryObtainSettingsDelay: TimeInterval = 3
    lazy var retryObtainSettingsPerformer = DelayedWorkPerformer { [weak self] in
        self?.interactor.obtainSettings()
    }
    
    var currentConversations: [Conversation]!
    var currentSettings: [String : UserNotificationsSetting]!
    
    func refreshUserProfileNotificationsStatus() {
        userProfileNotificationFactoryConfiguration = userProfileNotificationFactory.buildDataSourceConfiguration(from: Array(currentSettings.values), for: currentConversations, eventHandler: self)
        view.displayUserProfileNotifications(with: userProfileNotificationFactoryConfiguration.dataSource)
    }
    
    //MARK: - UserProfileNotificationsViewOutput
    
    func setupInitialState() {
        
        view.showLoadingStatus()
        interactor.obtainNotificationsAuthorized()
        
        notificationCenter.addObserver(self, selector: #selector(applicationDidBecomeActive), name: .UIApplicationDidBecomeActive, object: nil)
    }
    
    func didPressToGoToAppSettings() {
        router.goToAppSettings()
    }
    
    //MARK: - UserProfileNotificationsInteractorOutput
    
    func didFinishObtainingNotificationsAuthorized(with authorized: Bool) {
        
        view.hideLoadingStatus()
        
        guard authorized else {
            view.showNotificationsDisabledStatus()
            return
        }
        
        view.hideNotificationsDisabledStatus()
        
        guard currentSettings == nil else { return }
        
        view.showLoadingStatus()
        interactor.obtainSettings()
    }
    
    func didFinishObtainingSettings(with settings: [UserNotificationsSetting], for conversations: [Conversation]) {
        
        view.hideLoadingStatus()
        
        currentConversations = conversations
        currentSettings = settings.toDictionary { $0.chatId }
        
        refreshUserProfileNotificationsStatus()
        
        interactor.sentEvent(with: .screenLoaded(withString: AnalyticsConstants.screenLoadedEventName), settings: Array(currentSettings.values))
    }
    
    func didFinishObtainingSettings(with error: Error) {
        switch error {
        case NotificationsSettingObtainErrors.notificationsDisabledOnDevice:
            view.hideLoadingStatus()
            view.showNotificationsDisabledStatus()
        default:
            retryObtainSettingsPerformer.performOnMain(afterDelay: retryObtainSettingsDelay)
        }
    }
    
    func didFinishChangingSetting(for conversation: Conversation, to value: Bool, successfully: Bool) {
        
        let newValue = successfully ? value : !value
        currentSettings[conversation.itemId] = UserNotificationsSetting(chatId: conversation.itemId, areNotificationsEnabled: newValue)
        
        refreshUserProfileNotificationsStatus()
        
        if !successfully {
            router.showChangingNotificationSettingErrorModule()
        }
    }
    
    //MARK: - NotificationsSettingsEventHandler
    
    func didChangeSetting(for itemId: String) {
        
        guard let conversation = userProfileNotificationFactoryConfiguration.selectionItems[itemId] else { return }
        
        let currentValue = currentSettings[conversation.itemId]?.areNotificationsEnabled ?? true
        
        currentSettings[conversation.itemId] = nil
        refreshUserProfileNotificationsStatus()
        
        interactor.changeSetting(for: conversation, to: !currentValue)
    }
    
    //MARK: - Нотификации
    
    @objc
    func applicationDidBecomeActive() {
        view.showLoadingStatus()
        interactor.obtainNotificationsAuthorized()
    }
}

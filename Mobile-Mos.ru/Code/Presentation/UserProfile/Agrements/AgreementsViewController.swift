//
//  AgreementsViewController.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 31.01.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import UIKit
import WebKit

class AgreementsViewController: UIViewController, WKNavigationDelegate, UIScrollViewDelegate, NavigationBarCustomizingController {
    
    @IBOutlet weak var webViewContainer: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var analyticsManager: AnalyticsManager = AnalyticsManagerBuilder.getAnalyticsManager()
    var didScrollToEnd = false
    
    let webView: WKWebView = {
        
        let hearderRemoveScript = "$(document).ready(function() { $('#mos-header').remove(); })"
        let footerRemoveScript = "$(document).ready(function() { $('#mos_footer').remove(); })"
        let fontsStyle = "document.body.style.fontFamily = \'-apple-system, BlinkMacSystemFont, \"Segoe UI\", \"Roboto\", \"Oxygen\", \"Ubuntu\", \"Cantarell\", \"Fira Sans\", \"Droid Sans\", \"Helvetica Neue\", sans-serif\'"
        
        let fontsScript = "$(document).ready(function() { eval(\(fontsStyle)); })"
        
        let userScript = WKUserScript(source: hearderRemoveScript, injectionTime: .atDocumentEnd, forMainFrameOnly: true)
        let userScript2 = WKUserScript(source: footerRemoveScript, injectionTime: .atDocumentEnd, forMainFrameOnly: true)
        let userScript3 = WKUserScript(source: fontsScript, injectionTime: .atDocumentEnd, forMainFrameOnly: true)
        
        let preferences = WKPreferences()
        preferences.javaScriptEnabled = false
        
        let wkController = WKUserContentController()
        wkController.addUserScript(userScript)
        wkController.addUserScript(userScript2)
        wkController.addUserScript(userScript3)
        
        let configuration = WKWebViewConfiguration()
        configuration.preferences = preferences
        configuration.suppressesIncrementalRendering = true
        configuration.selectionGranularity = .character
        configuration.userContentController = wkController
        
        return WKWebView(frame: CGRect.zero, configuration: configuration)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        add(view: webView, to: webViewContainer)
        
        // чтобы выключить зум при попытке его начать
        webView.scrollView.delegate = self
        webView.navigationDelegate = self
        
        // убираем серый фон при оверскролле
        webView.isOpaque = false
        
        automaticallyAdjustsScrollViewInsets = false
        if #available(iOS 11.0, *) {
            webView.scrollView.contentInsetAdjustmentBehavior = .never
        }
        
        view.bringSubview(toFront: activityIndicator)
        activityIndicator.startAnimating()
        loadAgreements(to: webView)
        
        analyticsManager.sendAgreementsEvent(with: .screenLoaded(withString: AnalyticsConstants.screenLoadedEventName))
    }
    
    deinit {
        webView.scrollView.delegate = nil
        webView.navigationDelegate = nil
    }
    
    @available(iOS 11.0, *)
    override func viewSafeAreaInsetsDidChange() {
        
        // контейнер webView сверху прикреплен к safe area
        var insets = view.safeAreaInsets
        insets.top = 0
        
        webView.scrollView.contentInset = insets
        webView.scrollView.scrollIndicatorInsets = insets
    }
    
    //MARK: - NavigationBarCustomizingController
    
    var navigationBarState: NavigationBarState? {
        
        var state = NavigationBarState()
        
        state.isHidden = false
        state.isTranslucent = false
        state.backgroundState = NavigationBarBackgroundState.regular
        state.tintColor = UIColor.black
        state.isShadowHidden = true
        
        let titleTextAttributes = [
            NSAttributedStringKey.font : UIFont(customName: .graphikLCGBold, size: 15) as Any,
            NSAttributedStringKey.foregroundColor : UIColor.black
        ]
        let largeTitleTextAttributes = [
            NSAttributedStringKey.font : UIFont(customName: .graphikLCGBold, size: 25) as Any,
            NSAttributedStringKey.foregroundColor : UIColorPalette.conversationsPageTitleColor
        ]
        
        state.titleTextAttributes = titleTextAttributes
        state.largeTitleTextAttributes = largeTitleTextAttributes
        
        return state
    }
    
    //MARK: Custom Methods
    
    func add(view: UIView, to container: UIView) {
        
        container.addSubview(view)
        view.translatesAutoresizingMaskIntoConstraints = false
        
        let viewConstraints = NSLayoutConstraint.edgesConstraints(for: view)
        container.addConstraints(viewConstraints)
    }
    
    func loadAgreements(to webView: WKWebView) {
        webView.load(URLRequest(url: legalRulesUrl))
    }
    
    //MARK: - WKNavigationDelegate
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        
        activityIndicator.stopAnimating()
        activityIndicator.isHidden = true
        
        webView.resignFirstResponder()
    }
    
    
    //MARK: - UIScrollViewDelegate
    
    func scrollViewWillBeginZooming(_ scrollView: UIScrollView, with view: UIView?) {
        scrollView.pinchGestureRecognizer?.isEnabled = false
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        guard !didScrollToEnd else { return }
        
        let height = scrollView.height
        let contentYoffset = scrollView.contentOffset.y
        let distanceFromBottom = scrollView.contentSize.height - contentYoffset
        
        if distanceFromBottom < height {
            analyticsManager.sendAgreementsEvent(with: .scrolledToBottom(withString: AnalyticsConstants.scrollToBottomEventName))
            didScrollToEnd = true
        }
    }
}

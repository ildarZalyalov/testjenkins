//
//  UserServiceDetailEditModuleConfigurator.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 22.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation
import UIKit

class UserServiceDetailEditModuleConfigurator: BaseModuleConfigurator {
    
    override func configureModule(for view: UIViewController) {
        
        let viewController = view as! UserServiceDetailEditViewController
        let presenter = UserServiceDetailEditPresenter()
        let interactor = UserServiceDetailEditInteractor()
        let router = UserServiceDetailEditRouter()
        let notificationCenter = NotificationCenter.default
        let userDataService = UIApplication.shared.serviceBuilder.getUserDataOptionsService()
        let alertFactory = UserServiceDetailEditAlertFactoryImplementation()
        let commonAlertsFactory = CommonAlertsFactoryImplementation()
        let storyboardFactory = StoryboardFactoryImplementation()
        
        viewController.output = presenter
        viewController.notificationCenter = notificationCenter
        
        presenter.view = viewController
        presenter.interactor = interactor
        presenter.router = router
        
        interactor.output = presenter
        interactor.userDataService = userDataService
        
        router.transitionHandler = viewController
        router.alertTransitionHandler = viewController
        router.toOtherModulesTransitionHandler = viewController
        router.alertFactory = alertFactory
        router.commonAlertsFactory = commonAlertsFactory
        router.storyboardFactory = storyboardFactory
    }
}

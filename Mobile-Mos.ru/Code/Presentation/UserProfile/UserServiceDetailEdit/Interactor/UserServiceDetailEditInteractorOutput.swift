//
//  UserServiceDetailEditInteractorOutput.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 22.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

protocol UserServiceDetailEditInteractorOutput: class {
    
    func didFinishAddingUserDataOption(with type: String, value: String, and result: UserDataOptionsOperationResult)
    
    func didFinishAddingUserDataOptionGroup(with type: String, alias: String?, values: [UserOption], and result: UserDataOptionsOperationResult)
    
    func didFinishEditingUserDataOption(with optionId: String, bySetting value: String, with result: UserDataOptionsOperationResult)
    
    func didFinishEditingUserDataOptionGroup(with groupId: String, groupType: String, with result: UserDataOptionsOperationResult)
    
    func didFinishDeletingUserDataOption(with optionId: String, and result: UserDataOptionsOperationResult)
    
    func didFinishDeletingUserDataOptionGroup(with groupId: String, and result: UserDataOptionsOperationResult)
}

//
//  UserServiceDetailEditInteractor.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 22.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

class UserServiceDetailEditInteractor: UserServiceDetailEditInteractorInput {
    
    weak var output: UserServiceDetailEditInteractorOutput!
    
    var userDataService: UserDataOptionsService!
    
    //MARK: - UserServiceDetailEditInteractorInput
    
    func addUserDataOption(with type: String, and value: String) {
        userDataService.addUserDataOption(with: type, and: value) { [weak self] result in
            self?.output.didFinishAddingUserDataOption(with: type, value: value, and: result)
        }
    }
    
    func addUserDataOptionGroup(with type: String, alias: String?, and values: [UserOption]) {
        userDataService.addUserDataOptionGroup(with: type, alias: alias, and: values) { [weak self] result in
            self?.output.didFinishAddingUserDataOptionGroup(with: type, alias: alias, values: values, and: result)
        }
    }
    
    func editUserDataOption(with optionId: String, optionType: String, bySetting value: String) {
        userDataService.editUserDataOption(with: optionId, optionType: optionType, bySetting: value) { [weak self] result in
            self?.output.didFinishEditingUserDataOption(with: optionId, bySetting: value, with: result)
        }
    }
    
    func editUserDataOptionGroup(with groupId: String, groupType: String, byChanging alias: UserOptionChange?, and options: [UserOptionChange]) {
        userDataService.editUserDataOptionGroup(with: groupId, groupType: groupType, byChanging: alias, and: options) { [weak self] result in
            self?.output.didFinishEditingUserDataOptionGroup(with: groupId, groupType: groupType, with: result)
        }
    }
    
    func deleteUserDataOption(with optionId: String, optionType: String) {
        userDataService.deleteUserDataOption(with: optionId, optionType: optionType) { [weak self] result in
            self?.output.didFinishDeletingUserDataOption(with: optionId, and: result)
        }
    }
    
    func deleteUserDataOptionGroup(with groupId: String, groupType: String) {
        userDataService.deleteUserDataOptionGroup(with: groupId, groupType: groupType) { [weak self] result in
            self?.output.didFinishDeletingUserDataOptionGroup(with: groupId, and: result)
        }
    }
}

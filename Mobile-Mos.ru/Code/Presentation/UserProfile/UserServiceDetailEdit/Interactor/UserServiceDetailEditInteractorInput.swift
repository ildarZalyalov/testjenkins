//
//  UserServiceDetailEditInteractorInput.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 22.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

protocol UserServiceDetailEditInteractorInput: class {
    
    func addUserDataOption(with type: String, and value: String)
    
    func addUserDataOptionGroup(with type: String, alias: String?, and values: [UserOption])
    
    func editUserDataOption(with optionId: String, optionType: String, bySetting value: String)
    
    func editUserDataOptionGroup(with groupId: String, groupType: String, byChanging alias: UserOptionChange?, and options: [UserOptionChange])
    
    func deleteUserDataOption(with optionId: String, optionType: String)
    
    func deleteUserDataOptionGroup(with groupId: String, groupType: String)
}

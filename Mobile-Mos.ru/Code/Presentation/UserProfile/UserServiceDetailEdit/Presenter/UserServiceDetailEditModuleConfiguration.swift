//
//  UserServiceDetailEditModuleConfiguration.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 22.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

protocol UserServiceDetailEditModuleEventHandler: class {
    
    func didModifyUserOptionGroups()
}

struct UserServiceDetailEditModuleConfiguration {
    
    enum MetadataType {
        case option(optionType: String, optionId: String?)
        case group(groupType: String, selectedGroupId: String?)
    }
    
    let userData: UserOptionsData
    
    let metadataType: MetadataType
    
    var eventHandler: UserServiceDetailEditModuleEventHandler?
    
    init(userData: UserOptionsData, metadataType: MetadataType) {
        self.userData = userData
        self.metadataType = metadataType
    }
}

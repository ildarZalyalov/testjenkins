//
//  UserServiceDetailEditPresenter.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 22.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

class UserServiceDetailEditPresenter: UserServiceDetailEditViewOutput, UserServiceDetailEditInteractorOutput, UserServiceDetailEditConfirmDeleteEventHandler, OptionsChangeSuccessEventHandler, UserServiceDetailAutocompleteEventHandler {
    
    weak var view: UserServiceDetailEditViewInput!
    var router: UserServiceDetailEditRouterInput!
    var interactor: UserServiceDetailEditInteractorInput!
    
    var configuration: UserServiceDetailEditModuleConfiguration!
    
    var processingBeginTime = Date()
    let processingMinimumDuration: TimeInterval = 1
    
    var initialFieldsState: UserServiceDetailEditFieldsState!
    var currentFieldsState: UserServiceDetailEditFieldsState!
    
    var autocompleteValuesByFieldIndex = [Int : UserOptionsAutocompleteValue]()
    
    var optionMetadata: UserOptionMetadata? {
        switch configuration.metadataType {
        case .option(let optionType, _):
            return configuration.userData.optionsMetadata.first(where: { $0.type == optionType })
        default:
            return nil
        }
    }
    
    var groupMetadata: UserOptionGroupMetadata? {
        switch configuration.metadataType {
        case .group(let groupType, _):
            return configuration.userData.groupsMetadata.first(where: { $0.type == groupType })
        default:
            return nil
        }
    }
    
    //MARK: - Приватные методы
    
    func perform(after delay: TimeInterval, work: @escaping () -> ()) {
        let deadline: DispatchTime = .now() + delay
        DispatchQueue.main.asyncAfter(deadline: deadline, execute: work)
    }
    
    func configureModule(optionType: String, optionId: String?) {
        
        view.hideAliasField()
        
        if let description = optionMetadata?.description {
            view.displayDescription(description)
        }
        else {
            view.hideDescription()
        }
        
        if optionId == nil {
            
            view.setCancelButtonTitle(StringsHelper.cancelAddingItemInUserDataButtonText)
            view.setSaveButtonTitle(StringsHelper.doneAddingItemInUserDataButtonText)
            
            view.hideDeleteButton()
        }
        else {
            
            view.setCancelButtonTitle(StringsHelper.closeItemInUserDataButtonText)
            view.setSaveButtonTitle(StringsHelper.saveItemInUserDataButtonText)
            
            view.hideSaveButton()
        }
        
        guard let metadata = optionMetadata else { return }
        view.displayFields(for: [metadata])
        
        guard optionId != nil else {
            setupFieldsState(for: 1)
            return
        }
        guard let selectedOption = configuration.userData.options.first(where: { $0.itemId == optionId! }) else {
            setupFieldsState(for: 1)
            return
        }
        
        view.updateField(at: 0, with: selectedOption.value)
        setupFieldsState(with: nil, and: [selectedOption.value])
    }
    
    func configureModule(groupType: String, groupId: String?) {
        
        if let description = groupMetadata?.description {
            view.displayDescription(description)
        }
        else {
            view.hideDescription()
        }
        
        if groupId == nil {
            
            view.setCancelButtonTitle(StringsHelper.cancelAddingItemInUserDataButtonText)
            view.setSaveButtonTitle(StringsHelper.doneAddingItemInUserDataButtonText)
            
            view.hideDeleteButton()
        }
        else {
            
            view.setCancelButtonTitle(StringsHelper.closeItemInUserDataButtonText)
            view.setSaveButtonTitle(StringsHelper.saveItemInUserDataButtonText)
            
            view.hideSaveButton()
        }
        
        guard let metadata = groupMetadata else { return }
        
        if metadata.isAliasAllowed {
            
            view.setAliasFieldTextMaxLength(metadata.aliasMaxLength)
            
            if let title = metadata.aliasName {
                view.updateAliasFieldTitle(with: title)
            }
        }
        else {
            view.hideAliasField()
        }
        
        view.displayFields(for: metadata.options)
        
        guard groupId != nil else {
            setupFieldsState(for: metadata.options.count)
            return
        }
        guard let selectedGroup = configuration.userData.groups.first(where: { $0.itemId == groupId! }) else {
            setupFieldsState(for: metadata.options.count)
            return
        }
        
        var aliasValue: String? = nil
        var fieldValues = [String?]()
        
        if let alias = selectedGroup.alias, metadata.isAliasAllowed {
            aliasValue = alias.value
            view.updateAliasField(with: alias.value)
        }
        
        let selectedGroupOptions = selectedGroup.options.toDictionary { $0.type }
        
        for (index, option) in metadata.options.enumerated() {
            
            guard let selectedOption = selectedGroupOptions[option.type] else {
                fieldValues.append(nil)
                continue
            }
            
            fieldValues.append(selectedOption.value)
            view.updateField(at: index, with: selectedOption.value)
        }
        
        setupFieldsState(with: aliasValue, and: fieldValues)
    }
    
    func setupFieldsState(for valuesCount: Int) {
        initialFieldsState = UserServiceDetailEditFieldsState(alias: nil, values: [String?](repeating: nil, count: valuesCount))
        currentFieldsState = UserServiceDetailEditFieldsState(alias: nil, values: [String?](repeating: nil, count: valuesCount))
    }
    
    func setupFieldsState(with alias: String?, and values: [String?]) {
        initialFieldsState = UserServiceDetailEditFieldsState(alias: alias, values: values)
        currentFieldsState = UserServiceDetailEditFieldsState(alias: alias, values: values)
    }
    
    func metadataForField(at index: Int) -> UserOptionMetadata? {
        switch configuration.metadataType {
        case .option:
            return optionMetadata
        case .group:
            guard let metadata = groupMetadata else { return nil }
            guard metadata.options.indices.contains(index) else { return nil }
            return metadata.options[index]
        }
    }
    
    func process(fieldChange: UserServiceDetailEditFieldsState.StateChange, for alias: UserOptionGroupAlias?) -> UserOptionChange? {
        switch fieldChange {
        case .noChange:
            return nil
        case .valueChange(let newValue):
            return UserOptionChange(itemId: alias?.itemId, type: String(), value: newValue)
        }
    }
    
    func process(fieldChange: UserServiceDetailEditFieldsState.StateChange, with optionMetadata: UserOptionMetadata?, for option: UserOption?) -> UserOptionChange? {
        switch fieldChange {
        case .noChange:
            return nil
        case .valueChange(let newValue):
            guard let type = optionMetadata?.type else { return nil }
            return UserOptionChange(itemId: option?.itemId, type: type, value: newValue)
        }
    }
    
    func userOptionChanges(forGroupWith groupId: String, and fieldStateDifference: UserServiceDetailEditFieldsState.StateChangeDifference) -> (aliasChange: UserOptionChange?, optionChanges: [UserOptionChange]) {
        
        var aliasChange: UserOptionChange? = nil
        var optionChanges = [UserOptionChange]()
        
        guard let selectedGroup = configuration.userData.groups.first(where: { $0.itemId == groupId }) else {
            return (aliasChange, optionChanges)
        }
        
        aliasChange = process(fieldChange: fieldStateDifference.aliasChange, for: selectedGroup.alias)
        for (index, fieldChange) in fieldStateDifference.valuesChanges.enumerated() {
            let metadata = metadataForField(at: index)
            let option = selectedGroup.options.first(where: { $0.type == metadata?.type })
            guard let change = process(fieldChange: fieldChange, with: metadata, for: option) else { continue }
            optionChanges.append(change)
        }
        
        return (aliasChange, optionChanges)
    }
    
    func canHandleFinishProcessing(with completion: @escaping () -> ()) -> Bool {
        
        let durationDifference = processingBeginTime.timeIntervalSinceNow + processingMinimumDuration
        
        guard durationDifference <= 0 else {
            perform(after: durationDifference, work: completion)
            return false
        }
        
        return true
    }
    
    func handleFinishAddingUserDataOptionItem(with result: UserDataOptionsOperationResult) {
        
        router.hideProcessingModule()
        
        switch result {
        case .success:
            router.showAddingOptionsSuccessModule(with: self)
        case .failure(let error):
            view.showSaveButton()
            router.showAddingOptionsErrorModule(with: error.localizedDescription)
        }
    }
    
    func handleFinishEditingUserDataOptionItem(with result: UserDataOptionsOperationResult) {
        
        router.hideProcessingModule()
        
        switch result {
        case .success:
            router.showEditingOptionsSuccessModule(with: self)
        case .failure(let error):
            view.showSaveButton()
            router.showAddingOptionsErrorModule(with: error.localizedDescription)
        }
    }
    
    func handleFinishDeletingUserDataOptionItem(with result: UserDataOptionsOperationResult) {
        
        router.hideProcessingModule()
        
        switch result {
        case .success:
            configuration.eventHandler?.didModifyUserOptionGroups()
            router.closeModule(completion: nil)
        case .failure:
            router.showDeletingOptionsErrorModule()
        }
    }
    
    func validateFieldValues(_ values: [String?]) -> Bool {
        
        typealias FieldNameAndIndex = (index: Int, fieldName: String)
        
        var emptyMandatoryFields = [FieldNameAndIndex]()
        var errorFields = [FieldNameAndIndex]()
        
        let optionTypesWithValues: Set<String> = Set(values.enumerated().compactMap({ $0.element != nil ? metadataForField(at: $0.offset)?.type : nil }))
        
        
        for (index, value) in values.enumerated() {
            
            guard let metadata = metadataForField(at: index) else { continue }
            
            if value != nil {
                guard !validate(text: value!, forFieldAt: index) else { continue }
                errorFields.append((index, metadata.name))
            }
            else {
                
                guard !metadata.mandatory else {
                    emptyMandatoryFields.append((index, metadata.name))
                    continue
                }
                
                guard !metadata.mandatoryUnless.isEmpty else { continue }
                
                let mandatoryUnlessTypes: Set<String> = Set(metadata.mandatoryUnless)
                let intersection = mandatoryUnlessTypes.intersection(optionTypesWithValues)
                
                if intersection.isEmpty {
                    emptyMandatoryFields.append((index, metadata.name))
                }
            }
        }
        
        guard emptyMandatoryFields.isEmpty else {
            
            emptyMandatoryFields.forEach { view.displayErrorInField(at: $0.index) }
            router.showMandatoryFieldsErrorModule(for: emptyMandatoryFields.map { $0.fieldName })
            
            return false
        }
        
        guard errorFields.isEmpty else {
            
            errorFields.forEach { view.displayErrorInField(at: $0.index) }
            router.showErrorInFieldsErrorModule(for: errorFields.map { $0.fieldName })
            
            return false
        }
        
        return true
    }
    
    func refreshSaveButtonHiddenStateIfNeeded() {
        
        switch configuration.metadataType {
        case .option(_, let optionId):
            guard optionId != nil else { return }
        case .group(_, let selectedGroupId):
            guard selectedGroupId != nil else { return }
        }
        
        let difference = currentFieldsState.difference(from: initialFieldsState)
        
        if difference.hasNoChanges {
            view.hideSaveButton()
        }
        else {
            view.showSaveButton()
        }
    }
    
    //MARK: - UserServiceDetailEditViewOutput
    
    func setupInitialState() {
        
        switch configuration.metadataType {
        case .option(let optionType, let optionId):
            configureModule(optionType: optionType, optionId: optionId)
            let title = optionId != nil ? StringsHelper.changeItemInUserDataTitleText : StringsHelper.addItemInUserDataTitleText
            view.displayTitle(title)
        case .group(let groupType, let selectedGroupId):
            configureModule(groupType: groupType, groupId: selectedGroupId)
            guard let title = groupMetadata?.name else { return }
            view.displayTitle(title)
        }
    }
    
    func configure(with configuration: UserServiceDetailEditModuleConfiguration) {
        self.configuration = configuration
    }
    
    func helpTextForAliasField() -> String? {
        return groupMetadata?.aliasHelpText
    }
    
    func helpTextForField(at index: Int) -> String? {
        
        switch configuration.metadataType {
        case .option:
            return optionMetadata?.helpText
        case .group:
            guard let metadata = groupMetadata else { return nil }
            guard metadata.options.indices.contains(index) else { return nil }
            return metadata.options[index].helpText
        }
    }
    
    func validate(text: String, forFieldAt index: Int) -> Bool {
        
        guard let regexPattern = metadataForField(at: index)?.regex else { return true }
        guard let regex = try? NSRegularExpression(pattern: regexPattern, options: [.dotMatchesLineSeparators]) else { return true }
        
        let string = text as NSString
        let range = NSMakeRange(0, string.length)
        let rangeOfFirstMatch = regex.rangeOfFirstMatch(in: text, options: [], range: range)
        
        return NSEqualRanges(rangeOfFirstMatch, range)
    }
    
    func didTapReadonlyField(at index: Int) {
        
        var fieldMetadata: UserOptionMetadata? = nil
        
        switch configuration.metadataType {
        case .option:
            fieldMetadata = optionMetadata
        case .group:
            guard let metadata = groupMetadata else { return }
            guard metadata.options.indices.contains(index) else { return }
            fieldMetadata = metadata.options[index]
        }
        
        guard fieldMetadata != nil else { return }
        guard fieldMetadata!.keyboard == .autocomplete else { return }
        
        router.showAutocompleteModule(forFieldAt: index, with: fieldMetadata!, eventHandler: self)
    }
    
    func didPressCancel() {
        router.closeModule(completion: nil)
    }
    
    func didPressDelete() {
        router.showConfirmDeletingOptionGroupModule(eventHandler: self)
    }
    
    func didChangeAliasFieldText(to aliasText: String?) {
        
        guard currentFieldsState != nil else { return }
        
        currentFieldsState.alias = aliasText
        refreshSaveButtonHiddenStateIfNeeded()
    }
    
    func didChangeTextForField(at index: Int, to fieldText: String?) {
        
        guard currentFieldsState.values.indices.contains(index) else { return }
        
        currentFieldsState.values[index] = fieldText
        refreshSaveButtonHiddenStateIfNeeded()
        
        guard fieldText == nil else { return }
        autocompleteValuesByFieldIndex[index] = nil
    }
    
    func didPressSave(with alias: String?, and values: [String?]) {
        
        guard validateFieldValues(values) else { return }
        
        let fieldsValues = values.enumerated().map { autocompleteValuesByFieldIndex[$0.offset]?.value ?? $0.element }
        currentFieldsState.values = currentFieldsState.values.enumerated().map { autocompleteValuesByFieldIndex[$0.offset]?.value ?? $0.element }
        
        let startProcessing: () -> () = {
            self.view.hideSaveButton()
            self.processingBeginTime = Date()
            self.router.showProcessingModule()
        }
        
        switch configuration.metadataType {
            
        case .option(let optionType, let optionId):
            
            guard let value = fieldsValues.first else { return }
            guard value != nil else { return }
            
            if optionId != nil {
                startProcessing()
                interactor.editUserDataOption(with: optionId!, optionType: optionType, bySetting: value!)
            }
            else {
                startProcessing()
                interactor.addUserDataOption(with: optionType, and: value!)
            }
            
        case .group(let groupType, let selectedGroupId):
            
            if selectedGroupId != nil {
                
                let difference = currentFieldsState.difference(from: initialFieldsState)
                guard !difference.hasNoChanges else { return }
                
                startProcessing()
                let changes = userOptionChanges(forGroupWith: selectedGroupId!, and: difference)
                interactor.editUserDataOptionGroup(with: selectedGroupId!, groupType: groupType, byChanging: changes.aliasChange, and: changes.optionChanges)
            }
            else {
                
                let options: [UserOption] = fieldsValues.enumerated().compactMap({
                    guard $0.element != nil else { return nil }
                    guard let type = metadataForField(at: $0.offset)?.type else { return nil }
                    return UserOption(itemId: String(), type: type, value: $0.element!)
                })
                
                startProcessing()
                interactor.addUserDataOptionGroup(with: groupType, alias: alias, and: options)
            }
        }
    }
    
    //MARK: - UserServiceDetailEditInteractorOutput
    
    func didFinishAddingUserDataOption(with type: String, value: String, and result: UserDataOptionsOperationResult) {
        
        guard canHandleFinishProcessing(with: {
            self.didFinishAddingUserDataOption(with: type, value: value, and: result)
        }) else { return }
        
        handleFinishAddingUserDataOptionItem(with: result)
    }
    
    func didFinishAddingUserDataOptionGroup(with type: String, alias: String?, values: [UserOption], and result: UserDataOptionsOperationResult) {
        
        guard canHandleFinishProcessing(with: {
            self.didFinishAddingUserDataOptionGroup(with: type, alias: alias, values: values, and: result)
        }) else { return }
        
        handleFinishAddingUserDataOptionItem(with: result)
    }
    
    func didFinishEditingUserDataOption(with optionId: String, bySetting value: String, with result: UserDataOptionsOperationResult) {
        
        guard canHandleFinishProcessing(with: {
            self.didFinishEditingUserDataOption(with: optionId, bySetting: value, with: result)
        }) else { return }
        
        handleFinishEditingUserDataOptionItem(with: result)
    }
    
    func didFinishEditingUserDataOptionGroup(with groupId: String, groupType: String, with result: UserDataOptionsOperationResult) {
        
        guard canHandleFinishProcessing(with: {
            self.didFinishEditingUserDataOptionGroup(with: groupId, groupType: groupType, with: result)
        }) else { return }
        
        handleFinishEditingUserDataOptionItem(with: result)
    }
    
    func didFinishDeletingUserDataOption(with optionId: String, and result: UserDataOptionsOperationResult) {
        
        guard canHandleFinishProcessing(with: {
            self.didFinishDeletingUserDataOption(with: optionId, and: result)
        }) else { return }
        
        handleFinishDeletingUserDataOptionItem(with: result)
    }
    
    func didFinishDeletingUserDataOptionGroup(with groupId: String, and result: UserDataOptionsOperationResult) {
        
        guard canHandleFinishProcessing(with: {
            self.didFinishDeletingUserDataOptionGroup(with: groupId, and: result)
        }) else { return }
        
        handleFinishDeletingUserDataOptionItem(with: result)
    }
    
    //MARK: - UserServiceDetailEditConfirmDeleteEventHandler
    
    func didConfirmDeletingOptionGroup() {
        
        let startProcessing: () -> () = {
            self.processingBeginTime = Date()
            self.router.showProcessingModule()
        }
        
        switch configuration.metadataType {
        case .option(let optionType, let optionId):
            guard let itemId = optionId else { return }
            startProcessing()
            interactor.deleteUserDataOption(with: itemId, optionType: optionType)
        case .group(let groupType, let selectedGroupId):
            guard let itemId = selectedGroupId else { return }
            startProcessing()
            interactor.deleteUserDataOptionGroup(with: itemId, groupType: groupType)
        }
    }
    
    //MARK: - OptionsChangeSuccessEventHandler
    
    func didPressToCloseSuccessModule() {
        configuration.eventHandler?.didModifyUserOptionGroups()
        router.closeModule(completion: nil)
    }
    
    //MARK: - UserServiceDetailAutocompleteEventHandler
    
    func didSelectAutocompleteValue(_ value: UserOptionsAutocompleteValue, forFieldAt index: Int, with metadata: UserOptionMetadata) {
        
        guard currentFieldsState.values.indices.contains(index) else { return }
        
        autocompleteValuesByFieldIndex[index] = value
        
        view.updateField(at: index, with: value.name)
        currentFieldsState.values[index] = value.name
        
        refreshSaveButtonHiddenStateIfNeeded()
    }
}

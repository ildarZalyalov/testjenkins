//
//  UserServiceDetailEditViewInput.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 22.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

protocol UserServiceDetailEditViewInput: class {
    
    func displayTitle(_ title: String)
    
    func displayFields(for options: [UserOptionMetadata])
    
    func displayDescription(_ description: String)
    
    func displayErrorInField(at index: Int)
    
    func updateAliasField(with value: String)
    
    func updateField(at index: Int, with value: String)
    
    func setSaveButtonTitle(_ title: String)
    
    func setCancelButtonTitle(_ title: String)
    
    func showSaveButton()
    
    func hideSaveButton()
    
    func updateAliasFieldTitle(with title: String)
    
    func setAliasFieldTextMaxLength(_ maxLength: Int?)
    
    func hideAliasField()
    
    func hideDeleteButton()
    
    func hideDescription()
}

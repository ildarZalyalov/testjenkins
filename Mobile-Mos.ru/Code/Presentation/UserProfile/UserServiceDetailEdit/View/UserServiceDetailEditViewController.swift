//
//  UserServiceDetailEditViewController.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 22.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import UIKit

/// Поле, в которое вводится название группы опций
class UserServiceDetailEditAliasField: UITextView {
    
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        guard super.canPerformAction(action, withSender: sender) else { return false }
        let selector = #selector(UIResponderStandardEditActions.paste(_:))
        return action != selector
    }
}

class UserServiceDetailEditViewController: BaseViewController, UserServiceDetailEditViewInput, ConfigurableModuleController, UITextViewDelegate, UserServiceDetailTextFieldDelegate, UIScrollViewDelegate {
    
    var output: UserServiceDetailEditViewOutput!
    var notificationCenter: NotificationCenter!
    
    @IBOutlet weak var cancelBarButtonItem: UIBarButtonItem!
    @IBOutlet var saveBarButtonItem: UIBarButtonItem!
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var stackView: UIStackView!
    
    @IBOutlet weak var topFieldContainer: UIView!
    @IBOutlet weak var topFieldPlaceholderLabel: UILabel!
    @IBOutlet weak var topFieldTextView: UserServiceDetailEditAliasField!
    @IBOutlet weak var topFieldActivationView: UIView!
    @IBOutlet weak var topFieldSeparatorView: UIView!
    
    @IBOutlet weak var fieldEnteringExplainingContainer: UIView!
    @IBOutlet weak var fieldEnteringExplainingLabel: UILabel!
    
    @IBOutlet weak var deleteButtonContainer: UIView!
    
    @IBOutlet weak var helpTextContainerView: UIView!
    @IBOutlet weak var helpTextBackgroudView: UIImageView!
    @IBOutlet weak var helpTextLabel: UILabel!
    
    @IBOutlet weak var topFieldHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var helpTextTopConstraint: NSLayoutConstraint!
    
    let topFieldMinimumHeight: CGFloat = 41
    let topFieldLineHeightMultiple: CGFloat = 1.28
    let explainingLabelLineHeightMultiple: CGFloat = 1.38
    let helpTextFieldBottomOutset: CGFloat = 5
    let helpTextShowHideAnimationDuration: TimeInterval = 0.25
    
    var textFieldsStartIndex: Int {
        return topFieldContainer.isHidden ? 0 : 1
    }
    
    var fields = [UserServiceDetailTextField]()
    var aliasFieldTextMaxLength: Int?
    
    //MARK: - Методы
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        let titleAttributes: [NSAttributedStringKey : Any] = [NSAttributedStringKey.font : UIFont(customName: .graphikLCGRegular, size: 17) as Any]
        cancelBarButtonItem.setTitleTextAttributes(titleAttributes, for: .normal)
        cancelBarButtonItem.setTitleTextAttributes(titleAttributes, for: .highlighted)
        
        let saveTitleAttributes: [NSAttributedStringKey : Any] = [NSAttributedStringKey.font : UIFont(customName: .graphikLCGMedium, size: 17) as Any]
        
        saveBarButtonItem.setTitleTextAttributes(saveTitleAttributes, for: .normal)
        saveBarButtonItem.setTitleTextAttributes(saveTitleAttributes, for: .highlighted)
        
        automaticallyAdjustsScrollViewInsets = false
        if #available(iOS 11.0, *) {
            scrollView.contentInsetAdjustmentBehavior = .never
        }
        
        resetScrollViewInsets()
        updateTopFieldParagraphStyle()
        updateExplainingLabelParagraphStyle()
        
        topFieldSeparatorView.backgroundColor = UIColorPalette.unselectedTextFieldSeparatorColor
        
        helpTextBackgroudView.image = #imageLiteral(resourceName: "editUserOptionsTooltipBackground").resizableImage(withCapInsets: UIEdgeInsets(top: 12, left: 52, bottom: 12, right: 17))
        
        notificationCenter.addObserver(self,
                                       selector: #selector(keyboardWillShow(with:)),
                                       name: NSNotification.Name.UIKeyboardWillShow,
                                       object: nil)
        
        notificationCenter.addObserver(self,
                                       selector: #selector(keyboardWillHide(with:)),
                                       name: NSNotification.Name.UIKeyboardWillHide,
                                       object: nil)
        
        output.setupInitialState()
    }
    
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override func viewSafeAreaInsetsDidChange() {
        resetScrollViewInsets()
    }
    
    //MARK: - Actions
    
    @IBAction func pressedToActivateTopField() {
        topFieldTextView.becomeFirstResponder()
    }
    
    @IBAction func pressedCancel() {
        
        topFieldTextView.resignFirstResponder()
        fields.forEach { let _ = $0.resignFirstResponder() }
        
        output.didPressCancel()
    }
    
    @IBAction func pressedDelete() {
        output.didPressDelete()
    }
    
    @IBAction func pressedSave() {
        
        topFieldTextView.resignFirstResponder()
        fields.forEach { let _ = $0.resignFirstResponder() }
        hideTooltip()
        
        let alias: String? = (topFieldTextView.text?.isEmpty ?? true) ? nil : topFieldTextView.text
        output.didPressSave(with: alias, and: fields.map({
            guard $0.text != nil else { return nil }
            guard !$0.text!.isEmpty else { return nil }
            return $0.text
        }))
    }
    
    @IBAction func pressedToHideTooltip() {
        hideTooltip()
    }
    
    @IBAction func unwindToUserServiceDetailEdit(_: UIStoryboardSegue) {}
    
    //MARK: - Хелперы
    
    func resetScrollViewInsets() {
        
        var insets: UIEdgeInsets
        
        if #available(iOS 11.0, *) {
            insets = view.safeAreaInsets
        }
        else {
            var topInset = navigationController?.navigationBar.height ?? 0
            topInset += UIApplication.shared.statusBarFrame.height
            insets = UIEdgeInsets(top: topInset, left: 0, bottom: 0, right: 0)
        }
        
        scrollView.contentInset = insets
        scrollView.scrollIndicatorInsets = insets
    }
    
    func updateTopFieldParagraphStyle() {
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineHeightMultiple = topFieldLineHeightMultiple
        paragraphStyle.alignment = .left
        paragraphStyle.lineBreakMode = .byWordWrapping
        
        topFieldTextView.textContainerInset = UIEdgeInsets.zero
        topFieldTextView.typingAttributes = [NSAttributedStringKey.paragraphStyle.rawValue : paragraphStyle, NSAttributedStringKey.font.rawValue : topFieldTextView.font as Any]
    }
    
    func updateExplainingLabelParagraphStyle() {
        
        if let text = fieldEnteringExplainingLabel.text {
            
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.lineHeightMultiple = explainingLabelLineHeightMultiple
            paragraphStyle.alignment = .left
            paragraphStyle.lineBreakMode = .byTruncatingTail
            
            fieldEnteringExplainingLabel.attributedText = NSAttributedString(string: text, attributes: [NSAttributedStringKey.font : fieldEnteringExplainingLabel.font, NSAttributedStringKey.paragraphStyle : paragraphStyle])
        }
    }
    
    func showNextToAlias(tooltip: String) {
        
        guard let fieldOrigin = topFieldActivationView.superview?.convert(topFieldActivationView.origin, to: view) else {
            hideTooltip()
            return
        }
        
        show(tooltip: tooltip, at: fieldOrigin.y)
    }
    
    func show(tooltip: String, nextTo field: UserServiceDetailTextField) {
        
        guard var tooltipOrigin = field.superview?.convert(field.origin, to: view) else {
            hideTooltip()
            return
        }
        
        tooltipOrigin.y += field.height - helpTextFieldBottomOutset
        
        show(tooltip: tooltip, at: tooltipOrigin.y)
    }
    
    func show(tooltip: String, at originY: CGFloat) {
        
        guard !tooltip.isEmpty else { return }

        let showTooltip: (Bool) -> () = { [weak self] _ in
            
            guard let strongSelf = self else { return }
            
            strongSelf.helpTextLabel.text = tooltip
            strongSelf.helpTextTopConstraint.constant = originY
            strongSelf.helpTextContainerView.setNeedsLayout()
            strongSelf.helpTextContainerView.layoutIfNeeded()
            
            UIView.transition(with: strongSelf.helpTextContainerView, duration: strongSelf.helpTextShowHideAnimationDuration, options: .transitionCrossDissolve, animations: {
                strongSelf.helpTextContainerView.isHidden = false
            }, completion: nil)
        }
        
        if helpTextContainerView.isHidden {
            showTooltip(true)
        }
        else {
            hideTooltip(completion: showTooltip)
        }
    }
    
    func hideTooltip(completion: ((Bool) -> ())? = nil) {
        
        guard !helpTextContainerView.isHidden else { return }
        
        UIView.animate(withDuration: helpTextShowHideAnimationDuration, animations: { [weak self] in
            self?.helpTextContainerView.alpha = 0
        }) { [weak self] finished in
            self?.helpTextContainerView.isHidden = true
            self?.helpTextContainerView.alpha = 1
            completion?(finished)
        }
    }
    
    //MARK: - ConfigurableModuleController
    
    func configureModule(with object: Any) {
        if let configuration = object as? UserServiceDetailEditModuleConfiguration {
            output.configure(with: configuration)
        }
    }
    
    //MARK: - UserServiceDetailEditViewInput
    
    func displayTitle(_ title: String) {
        navigationItem.title = title
    }
    
    func displayFields(for options: [UserOptionMetadata]) {
        
        fields = []
        var lastViewIndex = textFieldsStartIndex
        
        for (index, option) in options.enumerated() {
            
            let field = UserServiceDetailTextField.loadNib()
            field.tag = index
            field.title = option.name
            field.delegate = self
            
            switch option.keyboard {
            case .digits:
                field.keyboardType = .numberPad
            case .digitsAndPunctuation:
                field.keyboardType = .numbersAndPunctuation
            case .date:
                field.setupDatePickerInput(with: option.dateFormat)
            case .autocomplete:
                field.isReadonly = true
            default:
                break
            }
            
            stackView.insertArrangedSubview(field, at: lastViewIndex)
            
            lastViewIndex += 1
            fields.append(field)
        }
    }
    
    func displayDescription(_ description: String) {
        
        guard !description.isEmpty else {
            hideDescription()
            return
        }
        
        fieldEnteringExplainingContainer.isHidden = false
        fieldEnteringExplainingLabel.text = description
        
        updateExplainingLabelParagraphStyle()
    }
    
    func displayErrorInField(at index: Int) {
        guard fields.indices.contains(index) else { return }
        fields[index].showFieldError()
    }
    
    func updateAliasField(with value: String) {
        topFieldTextView.text = value
        topFieldPlaceholderLabel.isHidden = !value.isEmpty
        textViewDidChange(topFieldTextView)
    }
    
    func updateField(at index: Int, with value: String) {
        guard fields.indices.contains(index) else { return }
        fields[index].text = value
    }
    
    func setSaveButtonTitle(_ title: String) {
        saveBarButtonItem.title = title
    }
    
    func setCancelButtonTitle(_ title: String) {
        cancelBarButtonItem.title = title
    }
    
    func showSaveButton() {
        navigationItem.rightBarButtonItems = [saveBarButtonItem]
    }
    
    func hideSaveButton() {
        navigationItem.rightBarButtonItems = nil
    }
    
    func updateAliasFieldTitle(with title: String) {
        topFieldPlaceholderLabel.text = title
    }
    
    func setAliasFieldTextMaxLength(_ maxLength: Int?) {
        aliasFieldTextMaxLength = maxLength
    }
    
    func hideAliasField() {
        topFieldContainer.isHidden = true
    }
    
    func hideDeleteButton() {
        deleteButtonContainer.isHidden = true
    }
    
    func hideDescription() {
        fieldEnteringExplainingContainer.isHidden = true
    }
    
    //MARK: - UITextViewDelegate
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if let helpText = output.helpTextForAliasField() {
            showNextToAlias(tooltip: helpText)
        }
        else {
            hideTooltip()
        }
        
        topFieldSeparatorView.backgroundColor = UIColorPalette.selectedTextFieldSeparatorColor
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        topFieldSeparatorView.backgroundColor = UIColorPalette.unselectedTextFieldSeparatorColor
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        guard !text.isEmpty else { return true }
        guard !text.trimmingCharacters(in: CharacterSet.newlines).isEmpty else { return false }
        guard !(textView.text?.isEmpty ?? true) else { return true }
        
        if let maxLength = aliasFieldTextMaxLength {
            let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
            return newText.count <= maxLength
        }
        else {
            return true
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        
        let aliasText = (textView.text?.isEmpty ?? true) ? nil : textView.text
        output.didChangeAliasFieldText(to: aliasText)
        
        // форсим лейаут, чтобы у textView была актуальная ширина
        textView.superview?.setNeedsLayout()
        textView.superview?.layoutIfNeeded()
        
        topFieldPlaceholderLabel.isHidden = !textView.text.isEmpty
        
        let previousHeight = topFieldHeightConstraint.constant
        let size = textView.sizeThatFits(CGSize(width: textView.width, height: CGFloat.greatestFiniteMagnitude))
        let newHeight = max(topFieldMinimumHeight, size.height)

        guard previousHeight != newHeight else { return }

        topFieldHeightConstraint.constant = newHeight
        topFieldTextView.layoutIfNeeded()
        
        hideTooltip()
    }
    
    //MARK: - UserServiceDetailTextFieldDelegate
    
    func fieldShouldBeginEditing(_ field: UserServiceDetailTextField) -> Bool { return true }
    
    func fieldDidBeginEditing(_ field: UserServiceDetailTextField) {
        if let helpText = output.helpTextForField(at: field.tag) {
            show(tooltip: helpText, nextTo: field)
        }
        else {
            hideTooltip()
        }
    }
    
    func fieldDidEndEditing(_ field: UserServiceDetailTextField) {
        
        guard let text = field.text else { return }
        guard !text.isEmpty else { return }
        guard !output.validate(text: text, forFieldAt: field.tag) else { return }
        
        field.showFieldError()
    }
    
    func fieldTextDidChange(_ field: UserServiceDetailTextField) {
        let valueText = (field.text?.isEmpty ?? true) ? nil : field.text
        output.didChangeTextForField(at: field.tag, to: valueText)
    }
    
    func readonlyFieldWasTapped(_ field: UserServiceDetailTextField) {
        output.didTapReadonlyField(at: field.tag)
    }
    
    //MARK: - UIScrollViewDelegate
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        hideTooltip()
    }
    
    //MARK: - Нотификации
    
    @objc func keyboardWillShow(with notification: Notification) {
        
        guard let info = notification.userInfo,
            let keyboardEndSize = (info[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size
            else { return }
        
        var insets = scrollView.contentInset
        insets.bottom = max(insets.bottom, keyboardEndSize.height)
        
        scrollView.contentInset = insets
        scrollView.scrollIndicatorInsets = insets
    }
    
    @objc func keyboardWillHide(with notification: Notification) {
        resetScrollViewInsets()
    }
}

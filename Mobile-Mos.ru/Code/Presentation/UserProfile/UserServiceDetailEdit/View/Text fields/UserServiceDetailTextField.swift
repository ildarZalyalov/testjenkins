//
//  UserServiceDetailTextField.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 22.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Делегат UserServiceDetailTextField
protocol UserServiceDetailTextFieldDelegate: class {
    
    /// Можно ли начать редактировать поле
    ///
    /// - Parameter field: поле, которое хотят начать редактировать
    /// - Returns: можно ли начать редактировать поле
    func fieldShouldBeginEditing(_ field: UserServiceDetailTextField) -> Bool
    
    /// Начато редактирование поля
    ///
    /// - Parameter field: поле, которое начали редактировать
    func fieldDidBeginEditing(_ field: UserServiceDetailTextField)
    
    /// Закончено редактирование поля
    ///
    /// - Parameter field: поле, которое закончили редактировать
    func fieldDidEndEditing(_ field: UserServiceDetailTextField)
    
    /// Текст в поле был изменен
    ///
    /// - Parameter field: поле, текст в котором был изменен
    func fieldTextDidChange(_ field: UserServiceDetailTextField)
    
    /// Тапнули на поле, доступное только для чтения
    ///
    /// - Parameter field: поле, доступное только для чтени
    func readonlyFieldWasTapped(_ field: UserServiceDetailTextField)
}

/// Тектовое поле для формы добавления/редактирования опций данных пользователя
class UserServiceDetailTextField: UIView, UITextFieldDelegate {
    
    fileprivate let textFieldLabelAnimationDuration = 0.6
    fileprivate let textFieldLabelAffineTransform = CGAffineTransform(scaleX: 0.8, y: 0.8)
    fileprivate let textFieldlabelTransformAnchorPoint = CGPoint(x: 0.62, y: 0.5)
    fileprivate let textFieldlabelDefaultAnchorPoint = CGPoint(x: 0.5, y: 0.5)
    
    @IBOutlet fileprivate weak var stackView: UIStackView!
    @IBOutlet fileprivate weak var textField: UITextField!
    @IBOutlet fileprivate weak var readonlyFieldValueLabel: UILabel!
    @IBOutlet fileprivate weak var fieldTitleLabel: UILabel!
    @IBOutlet fileprivate weak var separatorView: UIView!
    @IBOutlet fileprivate weak var errorIconView: UIImageView!
    
    @IBOutlet fileprivate weak var textFieldBottomConstraint: NSLayoutConstraint!
    @IBOutlet fileprivate weak var readonlyLabelBottomConstraint: NSLayoutConstraint!
    
    fileprivate static let nibFile = UINib(nibName: String(describing: UserServiceDetailTextField.self), bundle: nil)
    
    /// Форматтер дат для полей с вводом через барабан дат
    fileprivate static let dateFormatter: DateFormatter = {
        
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "ru_RU")
        formatter.timeZone = TimeZone.autoupdatingCurrent
        formatter.dateFormat = "dd.MM.yyyy"
        
        return formatter
    }()
    
    static func loadNib() -> UserServiceDetailTextField {
        return UserServiceDetailTextField.nibFile.instantiate(withOwner: nil, options: nil).first as! UserServiceDetailTextField
    }
    
    fileprivate var fieldWasCleared = false
    fileprivate var dateStringFormat: String?
    
    /// Делегат
    weak var delegate: UserServiceDetailTextFieldDelegate?
    
    /// Заголовок поля
    var title: String? {
        get { return fieldTitleLabel.text }
        set { fieldTitleLabel.text = newValue }
    }
    
    /// Текст в поле
    var text: String? {
        
        get { return textField.text }
        
        set {
            
            textField.text = newValue
            readonlyFieldValueLabel.text = newValue
            
            let textIsEmpty = newValue?.isEmpty ?? true
            
            if textIsEmpty {
                fieldTitleLabel.layer.anchorPoint = textFieldlabelDefaultAnchorPoint
                revertTextFieldTitle()
            }
            else {
                fieldTitleLabel.layer.anchorPoint = textFieldlabelTransformAnchorPoint
                shrinkTextFieldTitle()
            }
            
            guard let datePickerInput = textField.inputView as? DatePickerInputView else { return }
            guard let text = newValue else { return }
            
            if let format = dateStringFormat {
                UserServiceDetailTextField.dateFormatter.dateFormat = format
            }
            
            guard let date = UserServiceDetailTextField.dateFormatter.date(from: text) else { return }
            datePickerInput.datePicker.date = date
        }
    }
    
    /// Тип клавиатуры поля
    var keyboardType: UIKeyboardType {
        get { return textField.keyboardType }
        set { textField.keyboardType = newValue }
    }
    
    /// Цвет разделителя поля
    var separatorColor: UIColor? {
        get { return separatorView.backgroundColor }
        set { separatorView.backgroundColor = newValue }
    }
    
    /// Поле доступно только для чтения
    var isReadonly: Bool {
        
        get { return textField.isHidden }
        
        set {
            
            guard newValue != isReadonly else { return }
            
            textFieldBottomConstraint.priority = newValue ? .defaultLow : .defaultHigh
            readonlyLabelBottomConstraint.priority = newValue ? .defaultHigh : .defaultLow
            
            textField.isHidden = newValue
            readonlyFieldValueLabel.isHidden = !newValue
            
            setNeedsLayout()
            layoutIfNeeded()
        }
    }
    
    override func resignFirstResponder() -> Bool {
        return textField.resignFirstResponder()
    }
    
    override func awakeFromNib() {
        
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(didTapTitleLabel(recognizer:)))
        fieldTitleLabel.addGestureRecognizer(tapRecognizer)
        fieldTitleLabel.isUserInteractionEnabled = true
        
        let tapReadonlyFieldRecognizer = UITapGestureRecognizer(target: self, action: #selector(didTapReadonlyFieldLabel(recognizer:)))
        readonlyFieldValueLabel.addGestureRecognizer(tapReadonlyFieldRecognizer)
        readonlyFieldValueLabel.isUserInteractionEnabled = true
        
        textField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        
        textField.autocapitalizationType = .none
        textField.autocorrectionType = .no
        textField.spellCheckingType = .no
        
        if #available(iOS 11.0, *) {
            textField.smartInsertDeleteType = .no
            textField.smartDashesType = .no
            textField.smartQuotesType = .no
        }
    }
    
    @objc
    fileprivate func didTapTitleLabel(recognizer: UIGestureRecognizer) {
        textField.becomeFirstResponder()
    }
    
    @objc
    fileprivate func didTapReadonlyFieldLabel(recognizer: UIGestureRecognizer) {
        delegate?.readonlyFieldWasTapped(self)
    }
    
    @objc
    fileprivate func updateTextFieldFromDatePicker(datePicker: UIDatePicker) {
        
        if let format = dateStringFormat {
            UserServiceDetailTextField.dateFormatter.dateFormat = format
        }
        
        textField.text = UserServiceDetailTextField.dateFormatter.string(from: datePicker.date)
        delegate?.fieldTextDidChange(self)
    }
    
    //MARK: - Открытые методы
    
    func showFieldError() {
        textField.clearButtonMode = .whileEditing
        errorIconView.isHidden = false
        separatorView.backgroundColor = UIColorPalette.errorInUserDataColor
    }
    
    func setupDatePickerInput(with dateFormat: String? = nil) {
        
        let input = DatePickerInputView(frame: CGRect.zero)
        input.datePicker.locale = Locale.autoupdatingCurrent
        input.datePicker.addTarget(self, action: #selector(updateTextFieldFromDatePicker(datePicker:)), for: .valueChanged)
        
        dateStringFormat = dateFormat
        textField.inputView = input
    }
    
    //MARK: - UITextFieldDelegate
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        guard !fieldWasCleared else {
            fieldWasCleared = false
            return false
        }
        
        return delegate?.fieldShouldBeginEditing(self) ?? true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        textField.clearButtonMode = .always
        errorIconView.isHidden = true
        separatorView.backgroundColor = UIColorPalette.selectedTextFieldSeparatorColor
        animateTextFieldTitleState()
        
        delegate?.fieldDidBeginEditing(self)
        
        guard textField.text?.isEmpty ?? true else { return }
        
        if let datePickerInput = textField.inputView as? DatePickerInputView {
            updateTextFieldFromDatePicker(datePicker: datePickerInput.datePicker)
            delegate?.fieldTextDidChange(self)
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        separatorView.backgroundColor = UIColorPalette.unselectedTextFieldSeparatorColor
        delegate?.fieldDidEndEditing(self)
        guard textField.text?.isEmpty ?? true else { return }
        revertTextFieldTitleState()
    }
    
    @objc
    fileprivate func textFieldDidChange() {
        delegate?.fieldTextDidChange(self)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        fieldWasCleared = true
        revertTextFieldTitleState()
        return true
    }
    
    //MARK: - Анимации текстовых полей
    
    fileprivate func shrinkTextFieldTitle() {
        fieldTitleLabel.transform = textFieldLabelAffineTransform
        fieldTitleLabel.x = textField.x
    }
    
    fileprivate func revertTextFieldTitle() {
        fieldTitleLabel.transform = .identity
        fieldTitleLabel.x = textField.x
    }
    
    fileprivate func animateTextFieldTitleState() {
        
        guard fieldTitleLabel.transform == CGAffineTransform.identity else { return }
        
        UIView.animate(withDuration: textFieldLabelAnimationDuration, animations: { [weak self] in
            self?.shrinkTextFieldTitle()
        })
    }
    
    fileprivate func revertTextFieldTitleState() {
        
        guard fieldTitleLabel.transform != CGAffineTransform.identity else { return }
        
        UIView.animate(withDuration: textFieldLabelAnimationDuration, animations: { [weak self] in
            self?.revertTextFieldTitle()
        })
    }
}

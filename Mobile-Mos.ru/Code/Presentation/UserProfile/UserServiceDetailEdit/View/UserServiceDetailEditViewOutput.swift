//
//  UserServiceDetailEditViewOutput.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 22.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

protocol UserServiceDetailEditViewOutput: class {
    
    func setupInitialState()
    
    func configure(with configuration: UserServiceDetailEditModuleConfiguration)
    
    func helpTextForAliasField() -> String?
    
    func helpTextForField(at index: Int) -> String?
    
    func validate(text: String, forFieldAt index: Int) -> Bool
    
    func didTapReadonlyField(at index: Int)
    
    func didPressCancel()
    
    func didPressDelete()
    
    func didChangeAliasFieldText(to aliasText: String?)
    
    func didChangeTextForField(at index: Int, to fieldText: String?)
    
    func didPressSave(with alias: String?, and values: [String?])
}

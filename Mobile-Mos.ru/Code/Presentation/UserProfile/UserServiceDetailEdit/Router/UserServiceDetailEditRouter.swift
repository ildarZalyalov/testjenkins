//
//  UserServiceDetailEditRouter.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 22.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

class UserServiceDetailEditRouter: UserServiceDetailEditRouterInput {
    
    weak var transitionHandler: UIViewController!
    weak var alertTransitionHandler: UIViewController!
    weak var toOtherModulesTransitionHandler: DataTransferModuleController!
    
    var alertFactory: UserServiceDetailEditAlertFactory!
    var commonAlertsFactory: CommonAlertsFactory!
    var storyboardFactory: StoryboardFactory!
    
    let showUserServiceAutocompleteSegueSegueIdentifier = "showUserServiceAutocompleteSegue"
    let unwindToChatSegueIdentifier = "unwindToUserServiceDetail"
    
    //MARK: - UserServiceDetailEditRouterInput
    
    func closeModule(completion: (() -> ())?) {
        transitionHandler.performSegue(withIdentifier: unwindToChatSegueIdentifier, sender: self)
        transitionHandler.transitionCoordinator?.animate(alongsideTransition: nil, completion: { _ in
            completion?()
        })
    }
    
    func showConfirmDeletingOptionGroupModule(eventHandler: UserServiceDetailEditConfirmDeleteEventHandler?) {
        let actionSheet = alertFactory.getActionSheetToConfirmDeletingOptionGroup(eventHandler: eventHandler)
        alertTransitionHandler.present(actionSheet, animated: true, completion: nil)
    }
    
    func showProcessingModule() {
        
        let storyboard = storyboardFactory.getStoryboard(with: .userProfile)
        let processingController = storyboard.instantiateViewController(for: .userDataProcessing)
        
        processingController.modalPresentationStyle = .overCurrentContext
        processingController.modalTransitionStyle = .crossDissolve
        processingController.modalPresentationCapturesStatusBarAppearance = false
        
        transitionHandler.definesPresentationContext = true
        transitionHandler.present(processingController, animated: true, completion: nil)
    }
    
    func hideProcessingModule() {
        transitionHandler.dismiss(animated: true, completion: nil)
        transitionHandler.definesPresentationContext = false
    }
    
    func showAutocompleteModule(forFieldAt index: Int, with metadata: UserOptionMetadata, eventHandler: UserServiceDetailAutocompleteEventHandler?) {
        
        toOtherModulesTransitionHandler.performSegue(with: showUserServiceAutocompleteSegueSegueIdentifier, sender: self) { controller in
            
            var configuration = UserServiceDetailAutocompleteModuleConfiguration(metadata: metadata, fieldIndex: index)
            configuration.eventHandler = eventHandler
            
            controller.configureModule(with: configuration)
        }
    }
    
    func showAddingOptionsErrorModule(with errorMessage: String) {
        let alert = commonAlertsFactory.getErrorAlert(with: errorMessage)
        alertTransitionHandler.present(alert, animated: true, completion: nil)
    }
    
    func showDeletingOptionsErrorModule() {
        let alert = commonAlertsFactory.getErrorAlert(with: StringsHelper.deleteItemInUserDataErrorAlertText)
        alertTransitionHandler.present(alert, animated: true, completion: nil)
    }
    
    func showMandatoryFieldsErrorModule(for fieldNames: [String]) {
        
        let separator = StringsHelper.fieldInUserDataSeparator
        let fieldsString = fieldNames.map({ "\"" + $0 + "\"" }).joined(separator: separator)
        let format = fieldNames.count == 1 ? StringsHelper.mandatoryFieldInUserDataErrorAlertTextFormat : StringsHelper.mandatoryMultipleFieldInUserDataErrorAlertTextFormat
        let errorMessage = String(format: format, fieldsString)
        
        let alert = commonAlertsFactory.getErrorAlert(with: errorMessage)
        alertTransitionHandler.present(alert, animated: true, completion: nil)
    }
    
    func showErrorInFieldsErrorModule(for fieldNames: [String]) {
        
        let separator = StringsHelper.fieldInUserDataSeparator
        let fieldsString = fieldNames.map({ "\"" + $0 + "\"" }).joined(separator: separator)
        let format = fieldNames.count == 1 ? StringsHelper.errorInFieldInUserDataErrorAlertTextFormat : StringsHelper.errorInMultipleFieldInUserDataErrorAlertTextFormat
        let errorMessage = String(format: format, fieldsString)
        
        let alert = commonAlertsFactory.getErrorAlert(with: errorMessage)
        alertTransitionHandler.present(alert, animated: true, completion: nil)
    }
    
    func showAddingOptionsSuccessModule(with eventHandler: OptionsChangeSuccessEventHandler?) {
        let alert = alertFactory.getAddingOptionsSuccessAlert(eventHandler: eventHandler)
        alertTransitionHandler.present(alert, animated: true, completion: nil)
    }
    
    func showEditingOptionsSuccessModule(with eventHandler: OptionsChangeSuccessEventHandler?) {
        let alert = alertFactory.getEditingOptionsSuccessAlert(eventHandler: eventHandler)
        alertTransitionHandler.present(alert, animated: true, completion: nil)
    }
}

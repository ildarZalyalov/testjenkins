//
//  UserServiceDetailEditAlertFactory.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 23.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

protocol UserServiceDetailEditConfirmDeleteEventHandler {
    
    func didConfirmDeletingOptionGroup()
}

protocol OptionsChangeSuccessEventHandler {
    
    func didPressToCloseSuccessModule()
}

protocol UserServiceDetailEditAlertFactory {
    
    func getActionSheetToConfirmDeletingOptionGroup(eventHandler: UserServiceDetailEditConfirmDeleteEventHandler?) -> UIViewController
    
    func getAddingOptionsSuccessAlert(eventHandler: OptionsChangeSuccessEventHandler?) -> UIViewController
    
    func getEditingOptionsSuccessAlert(eventHandler: OptionsChangeSuccessEventHandler?) -> UIViewController
}

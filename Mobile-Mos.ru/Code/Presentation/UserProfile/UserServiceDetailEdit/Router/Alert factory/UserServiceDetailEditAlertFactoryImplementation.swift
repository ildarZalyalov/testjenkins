//
//  UserServiceDetailEditAlertFactoryImplementation.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 23.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

class UserServiceDetailEditAlertFactoryImplementation: UserServiceDetailEditAlertFactory {
    
    func getActionSheetToConfirmDeletingOptionGroup(eventHandler: UserServiceDetailEditConfirmDeleteEventHandler?) -> UIViewController {
        
        let actionSheet = UIAlertController(title: "Вы уверены?", message: nil, preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Удалить", style: .destructive, handler: { _ in
            eventHandler?.didConfirmDeletingOptionGroup()
        }))
        actionSheet.addAction(UIAlertAction(title: "Отменить", style: .cancel, handler: nil))
        
        return actionSheet
    }
    
    func getAddingOptionsSuccessAlert(eventHandler: OptionsChangeSuccessEventHandler?) -> UIViewController {
        
        let alert = UIAlertController(title: "🎉🎉🎉", message: "Данные успешно сохранены!", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Закрыть", style: .cancel, handler: { _ in
            eventHandler?.didPressToCloseSuccessModule()
        }))
        
        return alert
    }
    
    func getEditingOptionsSuccessAlert(eventHandler: OptionsChangeSuccessEventHandler?) -> UIViewController {
        
        let alert = UIAlertController(title: "🎉🎉🎉", message: "Данные успешно изменены!", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Закрыть", style: .cancel, handler: { _ in
            eventHandler?.didPressToCloseSuccessModule()
        }))
        
        return alert
    }
}

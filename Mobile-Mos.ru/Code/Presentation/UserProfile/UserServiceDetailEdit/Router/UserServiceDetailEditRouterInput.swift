//
//  UserServiceDetailEditRouterInput.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 22.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

protocol UserServiceDetailEditRouterInput: class {

    func closeModule(completion: (() -> ())?)
    
    func showConfirmDeletingOptionGroupModule(eventHandler: UserServiceDetailEditConfirmDeleteEventHandler?)
    
    func showProcessingModule()
    
    func hideProcessingModule()
    
    func showAutocompleteModule(forFieldAt index: Int, with metadata: UserOptionMetadata, eventHandler: UserServiceDetailAutocompleteEventHandler?)
    
    func showAddingOptionsErrorModule(with errorMessage: String)
    
    func showDeletingOptionsErrorModule()
    
    func showMandatoryFieldsErrorModule(for fieldNames: [String])
    
    func showErrorInFieldsErrorModule(for fieldNames: [String])
    
    func showAddingOptionsSuccessModule(with eventHandler: OptionsChangeSuccessEventHandler?)
    
    func showEditingOptionsSuccessModule(with eventHandler: OptionsChangeSuccessEventHandler?)
}

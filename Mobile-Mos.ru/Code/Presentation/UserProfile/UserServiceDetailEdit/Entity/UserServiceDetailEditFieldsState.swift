//
//  UserServiceDetailEditFieldsState.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 28.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

struct UserServiceDetailEditFieldsState {
    
    enum StateChange {
        case noChange
        case valueChange(newValue: String?)
    }
    
    struct StateChangeDifference {
        
        let aliasChange: StateChange
        
        let valuesChanges: [StateChange]
        
        var hasNoChanges: Bool {
            
            let allChanges = valuesChanges + [aliasChange]
            
            for change in allChanges {
                switch change {
                case .noChange:
                    continue
                case .valueChange:
                    return false
                }
            }
            
            return true
        }
    }

    var alias: String?
    
    var values: [String?] = []
    
    func difference(from state: UserServiceDetailEditFieldsState) -> StateChangeDifference {
        
        var aliasChange = StateChange.noChange
        var valuesChanges = [StateChange]()
        
        if alias != state.alias {
            aliasChange = .valueChange(newValue: alias)
        }
        
        let maxCount = max(values.count, state.values.count)
        for index in 0 ..< maxCount {
            
            var currentValue: String? = nil
            var oldValue: String? = nil
            
            if let value = values[safe: index] {
                currentValue = value
            }
            if let value = state.values[safe: index] {
                oldValue = value
            }
            
            if currentValue != oldValue {
                valuesChanges.append(.valueChange(newValue: currentValue))
            }
            else {
                valuesChanges.append(.noChange)
            }
        }
        
        return StateChangeDifference(aliasChange: aliasChange, valuesChanges: valuesChanges)
    }
}

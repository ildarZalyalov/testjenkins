//
//  UserProfileInfoModuleConfigurator.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 10.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import UIKit

class UserProfileInfoModuleConfigurator: BaseModuleConfigurator {
    
    override func configureModule(for view: UIViewController) {
        
        let viewController = view as! UserProfileInfoViewController
        let presenter = UserProfileInfoPresenter()
        let interactor = UserProfileInfoInteractor()

        let userProfileInfoDataSourceFactory = UserProfileInfoDataSourceFactoryImplementation()
        let userInfoService = UIApplication.shared.serviceBuilder.getUserInfoAndSettingsService()
        let stringsBuilder = UserProfileStringBuilderImplementation()
        let analyticsManager = AnalyticsManagerBuilder.getAnalyticsManager()
        
        let userProfileViewModelBuilder = UserProfileInfoViewModelBuilderImplementation()
        userProfileViewModelBuilder.stringBuilder = stringsBuilder
        
        viewController.output = presenter
        
        presenter.view = viewController
        presenter.interactor = interactor
        presenter.userProfileFactory = userProfileInfoDataSourceFactory
        presenter.userProfileViewModelBuilder = userProfileViewModelBuilder
        
        interactor.output = presenter
        interactor.userInfoService = userInfoService
        interactor.analyticsManager = analyticsManager
    }
}

//
//  UserProfileInfoPresenter.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 10.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class UserProfileInfoPresenter: UserProfileInfoViewOutput, UserProfileInfoInteractorOutput {
    
    weak var view: UserProfileInfoViewInput!
    var interactor: UserProfileInfoInteractorInput!
    
    var userProfileViewModelBuilder: UserProfileInfoViewModelBuilder!
    var userProfileFactory: UserProfileInfoDataSourceFactory!
    
    //MARK: Приватные методы
    
    func redisplayUserProfileInfo(with user: User) {
        
        let models = userProfileViewModelBuilder.buildUserProfileInfoViewModel(for: user)
        let configuration = userProfileFactory.buildDataSourceConfiguration(from: models, lastUserInfoUpdateDate: interactor.lastUserInfoUpdateDate)
        
        view.hideLoadingStatus()
        view.displayUserProfileSections(with: configuration.dataSource)
    }
    
    //MARK: - UserProfileInfoViewOutput
    
    func setupInitialState() {
        view.showLoadingStatus()
        interactor.obtainCurrentUser()
        interactor.sentEvent(with: .screenLoaded(withString: AnalyticsConstants.screenLoadedEventName))
    }
    
    func didRequestUserInfoUpdate() {
        interactor.refreshCurrentUser()
    }
    
    //MARK: - UserProfileInfoInteractorOutput
    
    func didFinishObtainingCurrentUser(with user: User) {
        
        guard user.userInfo != nil else {
            interactor.refreshCurrentUser()
            return
        }
        
        redisplayUserProfileInfo(with: user)
    }
    
    func didFinishRefreshingCurrentUser(with user: User) {
        redisplayUserProfileInfo(with: user)
    }
    
}

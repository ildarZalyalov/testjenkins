//
//  UserProfileInfoInteractorInput.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 10.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol UserProfileInfoInteractorInput: class {
    
    var lastUserInfoUpdateDate: Date? { get }
    
    func obtainCurrentUser()
    
    func refreshCurrentUser()
    
    func sentEvent(with event: UserPersonalInfoEvents)
}

//
//  UserProfileInfoInteractorOutput.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 10.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol UserProfileInfoInteractorOutput: class {
    
    func didFinishObtainingCurrentUser(with user: User)
    
    func didFinishRefreshingCurrentUser(with user: User)
}

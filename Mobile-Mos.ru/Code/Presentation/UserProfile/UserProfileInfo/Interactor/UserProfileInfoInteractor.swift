//
//  UserProfileInfoInteractor.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 10.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class UserProfileInfoInteractor: UserProfileInfoInteractorInput {
    
    weak var output: UserProfileInfoInteractorOutput!
    var analyticsManager: AnalyticsManager!
    
    var userInfoService: UserInfoAndSettingsService! {
        
        didSet {
            
            guard userInfoService != nil else {
                return
            }
            
            userInfoService.userInfoHandler = { [weak self] result in
                
                guard let strongSelf = self else { return }
                
                switch result {
                case .success(_):
                    strongSelf.output.didFinishRefreshingCurrentUser(with: strongSelf.userInfoService.currentUser)
                case .failure(_):
                    DispatchQueue.main.asyncAfter(deadline: .now() + strongSelf.obtainUserInfoRetryDelay, execute: { [weak self] in
                        self?.refreshCurrentUser()
                    })
                }
            }
        }
    }
    
    let obtainUserInfoRetryDelay: TimeInterval = 5
    
    //MARK: - UserProfileInfoInteractorInput
    
    var lastUserInfoUpdateDate: Date? {
        return userInfoService.lastUserInfoObtainDate
    }
    
    func obtainCurrentUser() {
        output.didFinishObtainingCurrentUser(with: userInfoService.currentUser)
    }
    
    func refreshCurrentUser() {
        userInfoService.obtainCurrentUserInfo()
    }
    
    func sentEvent(with event: UserPersonalInfoEvents) {
        analyticsManager.sendUserPersonalInfoEvent(with: event)
    }
}

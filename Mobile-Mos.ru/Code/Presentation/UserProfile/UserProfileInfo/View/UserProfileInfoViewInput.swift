//
//  UserProfileInfoViewInput.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 10.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol UserProfileInfoViewInput: class {
    
    func displayUserProfileSections(with dataSource: TableViewDataSource)
    
    func showLoadingStatus()
    
    func hideLoadingStatus()
}

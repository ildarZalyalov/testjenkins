//
//  UserProfileInfoViewController.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 10.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import UIKit

class UserProfileInfoViewController: BaseViewController, UserProfileInfoViewInput {
    
    var output: UserProfileInfoViewOutput!
    
    var tableViewDataDisplayManager: TableViewDataSource!
    var tableViewController: UITableViewController?
    var refreshControl = UIRefreshControl()
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    
    @IBOutlet var tableViewToSafeAreaConstraint: NSLayoutConstraint!
    @IBOutlet var tableViewToSuperviewConstraint: NSLayoutConstraint!
    
    let refreshUserInfoDelay: TimeInterval = 1
    
    //MARK: - Методы
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        setupRefreshControl()
        
        if #available(iOS 10, *) {
            tableViewToSafeAreaConstraint.isActive = false
            tableViewToSuperviewConstraint.isActive = true
        }
        else {
            tableViewToSafeAreaConstraint.isActive = true
            tableViewToSuperviewConstraint.isActive = false
        }
        
        
        tableView.estimatedRowHeight = 55
        tableView.registerCellNib(for: UserProfileInfoCellObject.self)
        tableView.registerCellNib(for: LastDataUpdateCellObject.self)
        
        output.setupInitialState()
    }
    
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    //MARK: - Actions
    
    @objc
    func refreshUserInfo() {
        // делаем задержку вызова, чтобы контрол не схлопывался сразу же
        let delay = DispatchTime.now() + refreshUserInfoDelay
        DispatchQueue.main.asyncAfter(deadline: delay) { [weak self] in
            self?.output.didRequestUserInfoUpdate()
        }
    }
    
    //MARK: - Хелперы
    
    func setupRefreshControl() {
        
        refreshControl.addTarget(self, action: #selector(refreshUserInfo), for: .valueChanged)
        refreshControl.tintColor = UIColor.black
        
        if #available(iOS 10, *) {
            tableView.refreshControl = refreshControl
        }
        else {
            tableViewController = UITableViewController()
            tableViewController?.tableView = tableView
            tableViewController?.refreshControl = refreshControl
        }
    }
    
    //MARK: - UserProfileInfoViewInput
    
    func displayUserProfileSections(with dataSource: TableViewDataSource) {
        
        let reloadTable = { [weak self] in
            
            guard let strongSelf = self else { return }
            
            strongSelf.tableViewDataDisplayManager = dataSource
            strongSelf.tableViewDataDisplayManager.assign(to: strongSelf.tableView)
            
            strongSelf.tableView.reloadData()
        }
        
        if refreshControl.isRefreshing {
            
            refreshControl.endRefreshing()
            
            // делаем задержку вызова, чтобы контрол успел схлопнуться
            let delay = DispatchTime.now() + refreshUserInfoDelay
            DispatchQueue.main.asyncAfter(deadline: delay, execute: reloadTable)
        }
        else {
            reloadTable()
        }
    }
    
    func showLoadingStatus() {
        
        // чтобы хедер таблицы оставался виден, прячем содержимое, удаляя контент таблицы
        let emptyDataSource = TableViewDataSource(with: TableViewDataSourceStructure())
        tableViewDataDisplayManager = emptyDataSource
        tableViewDataDisplayManager.assign(to: tableView)
        
        tableView.isScrollEnabled = false
        tableView.reloadData()
        
        activityIndicatorView.startAnimating()
    }
    
    func hideLoadingStatus() {
        tableView.isScrollEnabled = true
        activityIndicatorView.stopAnimating()
    }
}

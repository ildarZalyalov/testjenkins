//
//  UserProfileInfoCell.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 10.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import UIKit

/// Ячейка с личными данными пользователя
class UserProfileInfoCell: UITableViewCell, ConfigurableView {
    
    //MARK: - ConfigurableView
    
    @IBOutlet weak var infoTitleLabel: UILabel!
    @IBOutlet weak var infoValueLabel: UILabel!
    
    let valueLabelLineHeightMultiple: CGFloat = 1.22
    
    func configure(with object: Any) {
        
        guard let cellObject = object as? UserProfileInfoCellObject else {
            return
        }
        
        infoTitleLabel.text = cellObject.title
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineHeightMultiple = valueLabelLineHeightMultiple
        paragraphStyle.alignment = .left
        paragraphStyle.lineBreakMode = .byTruncatingTail
        
        let attributes: [NSAttributedStringKey : Any] = [NSAttributedStringKey.font : infoValueLabel.font, NSAttributedStringKey.paragraphStyle : paragraphStyle]
        let valueString = NSAttributedString(string: cellObject.value, attributes: attributes)
        
        infoValueLabel.attributedText = valueString
    }
}

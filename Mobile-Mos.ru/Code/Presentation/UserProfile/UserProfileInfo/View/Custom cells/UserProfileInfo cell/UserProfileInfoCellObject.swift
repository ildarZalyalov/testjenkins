//
//  UserProfileInfoCellObject.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 10.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Cell object ячейки с личными данными пользователя
struct UserProfileInfoCellObject: CellObjectWithId {
    
    /// Индентификатор ячейки
    var itemId: String
    
    /// Название
    var title: String
    
    /// Значение
    var value: String
}

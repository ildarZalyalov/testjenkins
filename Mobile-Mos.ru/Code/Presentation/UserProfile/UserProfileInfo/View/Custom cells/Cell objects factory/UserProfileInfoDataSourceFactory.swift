//
//  UserProfileInfoDataSourceFactory.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 10.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Конфигурация источника данных для страницы личных данных пользователя
typealias UserProfileInfoDataSourceConfiguration = (dataSource: TableViewDataSource, selectionItems: [String : UserProfileRowViewModel])

/// Фабрика источника данных для страницы личных данных пользователя
protocol UserProfileInfoDataSourceFactory {
    
    /// Построить источник данных
    ///
    /// - Parameters:
    ///   - models: модели рядов ячеек
    ///   - lastUserInfoUpdateDate: дата последнего обновления личных данных пользователя (если есть)
    /// - Returns: источник данных
    func buildDataSourceConfiguration(from models: [UserProfileRowViewModel], lastUserInfoUpdateDate: Date?) -> UserProfileInfoDataSourceConfiguration
}

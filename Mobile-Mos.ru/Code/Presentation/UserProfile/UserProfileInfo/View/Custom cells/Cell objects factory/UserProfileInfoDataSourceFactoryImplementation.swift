//
//  UserProfileInfoDataSourceFactoryImplementation.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 10.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class UserProfileInfoDataSourceFactoryImplementation: UserProfileInfoDataSourceFactory {
    
    fileprivate lazy var dateFormatter: DateFormatter = {
        
        let localeIdentifier = "ru_RU"
        
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: localeIdentifier)
        formatter.timeZone = TimeZone.autoupdatingCurrent
        formatter.dateFormat = "dd MMMM HH:mm"
        
        return formatter
    }()
    
    //MARK: - UserProfileInfoDataSourceFactory
    
    func buildDataSourceConfiguration(from models: [UserProfileRowViewModel], lastUserInfoUpdateDate: Date?) -> UserProfileInfoDataSourceConfiguration {
        
        let dataStructure = TableViewDataSourceStructure()
        var selectionItems = [String : UserProfileRowViewModel]()
        
        for item in models.enumerated() {
            
            let cellObject = UserProfileInfoCellObject(itemId: String(item.offset), title: item.element.title, value: item.element.subtitle ?? "")
            selectionItems[cellObject.itemId] = item.element
    
            dataStructure.appendCellObject(cellObject)
        }
        
        dataStructure.appendCellObject(UserProfileEditDisabledCellObject())
        
        if let date = lastUserInfoUpdateDate {
            
            let dateString = dateFormatter.string(from: date)
            let dateCellObject = LastDataUpdateCellObject(itemId: UUID().uuidString, dateString: dateString)
            
            dataStructure.appendCellObject(dateCellObject)
        }
        
        let dataSource = TableViewDataSource(with: dataStructure)
        
        return (dataSource: dataSource, selectionItems: selectionItems)
    }
}

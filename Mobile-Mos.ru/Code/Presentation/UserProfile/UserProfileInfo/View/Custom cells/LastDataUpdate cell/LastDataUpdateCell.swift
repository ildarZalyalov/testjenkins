//
//  LastDataUpdateCell.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 07.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import UIKit

/// Ячейка с датой последнего обновления
class LastDataUpdateCell: UITableViewCell, ConfigurableView {
    
    @IBOutlet weak var lastDataUpdateLabel: UILabel!
    
    //MARK: - ConfigurableView
    
    func configure(with object: Any) {
        
        guard let cellObject = object as? LastDataUpdateCellObject else {
            return
        }
        
        lastDataUpdateLabel.text = StringsHelper.dataUpToDateString + " " + cellObject.dateString
    }
}

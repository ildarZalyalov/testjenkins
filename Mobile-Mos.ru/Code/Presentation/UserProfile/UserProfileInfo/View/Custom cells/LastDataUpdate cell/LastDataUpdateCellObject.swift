//
//  LastDataUpdateCellObject.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 07.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Cell object ячейки с датой последнего обновления
struct LastDataUpdateCellObject: CellObjectWithId {
    
    /// Индентификатор ячейки
    var itemId: String
    
    /// Строка с датой
    var dateString: String
}

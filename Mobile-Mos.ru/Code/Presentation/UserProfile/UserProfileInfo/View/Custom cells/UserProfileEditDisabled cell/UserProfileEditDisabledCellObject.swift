//
//  UserProfileEditDisabledCellObject.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 06.04.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Cell object ячейки с предупреждением о недоступности редактирования личных данных пользователя
struct UserProfileEditDisabledCellObject: CellObject {}

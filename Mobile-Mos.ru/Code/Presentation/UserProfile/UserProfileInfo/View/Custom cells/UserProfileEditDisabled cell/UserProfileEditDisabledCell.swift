//
//  UserProfileEditDisabledCell.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 06.04.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Ячейка с предупреждением о недоступности редактирования личных данных пользователя
class UserProfileEditDisabledCell: UITableViewCell, ConfigurableView {
    
    @IBOutlet weak var warningLabel: UILabel!
    
    let warningLabelLineHeightMultiple: CGFloat = 1.46
    
    override func awakeFromNib() {
        
        guard let warning = warningLabel.text else { return }
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineHeightMultiple = warningLabelLineHeightMultiple
        paragraphStyle.alignment = .left
        paragraphStyle.lineBreakMode = .byTruncatingTail
        
        let attributes: [NSAttributedStringKey : Any] = [NSAttributedStringKey.font : warningLabel.font, NSAttributedStringKey.paragraphStyle : paragraphStyle]
        let warningString = NSAttributedString(string: warning, attributes: attributes)
        
        warningLabel.attributedText = warningString
    }
    
    //MARK: - ConfigurableView
    
    func configure(with object: Any) {}
}

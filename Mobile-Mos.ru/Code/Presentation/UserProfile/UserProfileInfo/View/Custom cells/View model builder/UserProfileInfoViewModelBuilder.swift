//
//  UserProfileInfoViewModelBuilder.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 22.02.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Билдер view models ячеек с личными данными пользователя
protocol UserProfileInfoViewModelBuilder {
    
    /// Построить view models ячеек
    ///
    /// - Parameter user: текущий пользователь
    /// - Returns: view models ячеек
    func buildUserProfileInfoViewModel(for user: User) -> [UserProfileRowViewModel]
}

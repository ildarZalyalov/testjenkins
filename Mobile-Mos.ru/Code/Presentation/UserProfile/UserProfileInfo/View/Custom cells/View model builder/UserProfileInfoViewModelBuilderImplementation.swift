//
//  UserProfileInfoViewModelBuilderImplementation.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 22.02.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

class UserProfileInfoViewModelBuilderImplementation: UserProfileInfoViewModelBuilder {
    
    fileprivate lazy var dateFormatter: DateFormatter = {
        
        let localeIdentifier = "ru_RU"
        
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: localeIdentifier)
        formatter.timeZone = TimeZone.autoupdatingCurrent
        formatter.dateFormat = "dd MMMM yyyy"
        
        return formatter
    }()
    
    var stringBuilder: UserProfileStringBuilder!
    
    //MARK: - UserProfileInfoViewModelBuilder
    
    func buildUserProfileInfoViewModel(for user: User) -> [UserProfileRowViewModel] {
        
        let userName = UserProfileRowViewModel(with: StringsHelper.userNameInfoTitle, subtitle: stringBuilder.fullName(of: user))
        var items = [userName]
        
        if let birthday = user.userInfo?.birthDate {
            items += [UserProfileRowViewModel(with: StringsHelper.userAgeInfoTitle, subtitle: dateFormatter.string(from: birthday))]
        }
        
        if let snils = user.userInfo?.snils, !snils.isEmpty {
            items += [UserProfileRowViewModel(with: StringsHelper.userSnilsInfoTitle, subtitle: snils)]
        }
        
        if let mobilePhone = user.userInfo?.phone, !mobilePhone.isEmpty {
            items += [UserProfileRowViewModel(with: StringsHelper.userPhoneInfoTitle, subtitle: mobilePhone)]
        }
        
        if let email = user.userInfo?.email, !email.isEmpty {
            items += [UserProfileRowViewModel(with: StringsHelper.userEmailInfoTitle, subtitle: email)]
        }
        
        return items
    }
}

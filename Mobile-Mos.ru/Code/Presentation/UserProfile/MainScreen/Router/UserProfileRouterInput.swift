//
//  UserProfileRouterInput.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 05.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol UserProfileRouterInput: class {
   
    func closeModule()
    
    func showExitModule(with handler: UserProfileAlertFactoryActionEventHandler?)
    
    func showLogoutErrorModule()
    
    func showNotificationSettings()
    
    func showUserProfileInfo()
    
    func showAboutAppInfo()
    
    func showServicesDetails(with service: UserServiceInfo)
    
    func showAgreementModule()
    
    func showServicesAndAppsModule()
    
    func showSupportModule()
}

//
//  UserProfileRouter.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 05.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class UserProfileRouter: UserProfileRouterInput {
    
    let notificationsSettingsIdentifier = "notifications"
    let servicesAndAppsIdentifier = "servicesAndApps"
    let userProfileInfoIdentifier = "userInfo"
    let serviceDetailIdentifier = "serviceDetail"
    let aboutAppIdentifier = "aboutApp"
    let rulesIdentifier = "rules"
    let supportSegueIdentifier = "support"
    
    weak var transitionHandler: UIViewController!
    weak var alertTransitionHandler: UIViewController!
    weak var toOtherModulesTransitionHandler: DataTransferModuleController!
    
    var commonAlertsFactory: CommonAlertsFactory!
    var userProfileAlertFactory: UserProfileAlertFactory!
    
    //MARK: - UserProfileRouterInput
    
    func closeModule() {
        transitionHandler.navigationController?.popViewController(animated: true)
    }
    
    func showExitModule(with handler: UserProfileAlertFactoryActionEventHandler?) {
        let alert = userProfileAlertFactory.getExitAlert(with: handler)
        alertTransitionHandler.present(alert, animated: true, completion: nil)
    }
    
    func showLogoutErrorModule() {
        let alert = commonAlertsFactory.getErrorAlert(with: nil, body: StringsHelper.userExitErrorMessage)
        alertTransitionHandler.present(alert, animated: true, completion: nil)
    }
    
    func showNotificationSettings() {
        transitionHandler.performSegue(withIdentifier: notificationsSettingsIdentifier, sender: nil)
    }
    
    func showServicesDetails(with service: UserServiceInfo) {
        toOtherModulesTransitionHandler.performSegue(with: serviceDetailIdentifier, sender: nil) { (configurableController) in
            configurableController.configureModule(with: service)
        }
    }
    
    func showUserProfileInfo() {
        transitionHandler.performSegue(withIdentifier: userProfileInfoIdentifier, sender: nil)
    }
    
    func showAboutAppInfo() {
        transitionHandler.performSegue(withIdentifier: aboutAppIdentifier, sender: nil)
    }
    
    func showAgreementModule() {
        transitionHandler.performSegue(withIdentifier: rulesIdentifier, sender: nil)
    }
    
    func showServicesAndAppsModule() {
        transitionHandler.performSegue(withIdentifier: servicesAndAppsIdentifier, sender: nil)
    }
    
    func showSupportModule() {
        transitionHandler.performSegue(withIdentifier: supportSegueIdentifier, sender: nil)
    }
}

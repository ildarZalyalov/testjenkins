//
//  UserProfileAlertFactoryImplementation.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 07.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class UserProfileAlertFactoryImplementation: UserProfileAlertFactory {
    
    func getExitAlert(with eventHandler: UserProfileAlertFactoryActionEventHandler?) -> UIViewController {
        
        let alert = UIAlertController(title: "Выйти из профиля?", message:nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Выйти", style: .destructive, handler: { (alert: UIAlertAction!) in
            eventHandler?.didSelectExit()
        }))
        
        alert.addAction(UIAlertAction(title: "Отменить", style: .cancel, handler: nil))
        
        return alert
    }
}

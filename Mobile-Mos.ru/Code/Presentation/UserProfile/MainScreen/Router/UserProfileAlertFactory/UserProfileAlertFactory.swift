//
//  UserProfileAlertFactory.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 07.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Обработчик нажатий
protocol UserProfileAlertFactoryActionEventHandler {
    
    func didSelectExit()
}

/// Фабрика алертов данного модуля
protocol UserProfileAlertFactory {
    
    /// Получить алерт для выхода
    func getExitAlert(with eventHandler: UserProfileAlertFactoryActionEventHandler?) -> UIViewController
}

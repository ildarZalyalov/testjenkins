//
//  UserProfileModuleConfigurator.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 05.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import UIKit

class UserProfileModuleConfigurator: BaseModuleConfigurator {
    
    override func configureModule(for view: UIViewController) {
        
        let viewController = view as! UserProfileViewController
        let presenter = UserProfilePresenter()
        let interactor = UserProfileInteractor()
        let router = UserProfileRouter()
        let userService = UIApplication.shared.serviceBuilder.getUserService()
        let userInfoService = UIApplication.shared.serviceBuilder.getUserInfoAndSettingsService()
        let paymentService = UIApplication.shared.serviceBuilder.getPaymentService()
        let userProfileFactory = UserProfileDataSourceFactoryImplementation()
        let conversationsService = UIApplication.shared.serviceBuilder.getConversationsService()
        let messageHistoryService = UIApplication.shared.serviceBuilder.getChatMessageHistoryService()
        let commonAlertsFactory = CommonAlertsFactoryImplementation()
        let userProfileAlertFactory = UserProfileAlertFactoryImplementation()
        let stringsBuilder = UserProfileStringBuilderImplementation()
        let analyticsManager = AnalyticsManagerBuilder.getAnalyticsManager()
        
        let userProfileViewModelBuilder = UserProfileViewModelBuilderImplementation()
        userProfileViewModelBuilder.stringBuilder = stringsBuilder
        
        viewController.output = presenter
        
        presenter.view = viewController
        presenter.interactor = interactor
        presenter.router = router
        presenter.userProfileViewModelBuilder = userProfileViewModelBuilder
        presenter.userProfileFactory = userProfileFactory
        presenter.stringsBuilder = stringsBuilder
        
        interactor.output = presenter
        interactor.userService = userService
        interactor.userInfoService = userInfoService
        interactor.conversationsService = conversationsService
        interactor.messageHistoryService = messageHistoryService
        interactor.paymentService = paymentService
        interactor.analyticsManager = analyticsManager
        
        router.transitionHandler = viewController
        router.commonAlertsFactory = commonAlertsFactory
        router.userProfileAlertFactory = userProfileAlertFactory
        router.alertTransitionHandler = viewController
        router.toOtherModulesTransitionHandler = viewController
    }
}

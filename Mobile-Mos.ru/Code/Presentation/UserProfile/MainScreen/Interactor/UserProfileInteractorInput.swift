//
//  UserProfileInteractorInput.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 05.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol UserProfileInteractorInput: class {
    
    var hasConnectionToServer: Bool { get }
    
    func obtainCurrentUserAndConversations()
    
    func refreshCurrentUserAndConversations()
    
    func logout()
    
    func sentEvent(with event: UserAccountEvents, selectedServiceName: String?, errorText: String?)
}

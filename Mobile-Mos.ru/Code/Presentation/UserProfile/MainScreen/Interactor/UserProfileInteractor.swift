//
//  UserProfileInteractor.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 05.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class UserProfileInteractor: UserProfileInteractorInput {
    
    weak var output: UserProfileInteractorOutput!
    
    var conversationsService: ConversationsService!
    var messageHistoryService: ChatMessageHistoryService!
    var userService: UserService!
    var paymentService: PaymentService!
    var analyticsManager: AnalyticsManager!
    
    var userInfoService: UserInfoAndSettingsService! {
        
        didSet {
            
            guard userInfoService != nil else {
                return
            }
            
            userInfoService.userInfoHandler = { [weak self] _ in
                
                guard let strongSelf = self else { return }
                
                let conversations = strongSelf.conversationsCache ?? []
                strongSelf.output.didFinishRefreshingCurrentUserAndConversations(with: strongSelf.userService.currentUser, and: conversations)
            }
        }
    }
    
    var conversationsCache: [Conversation]?
    
    func obtainConversationsCache(completion: @escaping ([Conversation]) -> Void) {
        
        guard conversationsCache == nil else {
            completion(conversationsCache!)
            return
        }
        
        conversationsService.obtainCachedConversations(for: userService.currentUser) { [weak self] result in
            switch result {
            case .success(let conversations):
                self?.conversationsCache = conversations
                completion(conversations)
            case .failure(_):
                completion([])
            }
        }
    }
    
    //MARK: - UserProfileInteractorInput
    
    var hasConnectionToServer: Bool {
        return userService.hasConnectionToServer
    }
    
    func obtainCurrentUserAndConversations() {
        obtainConversationsCache { [weak self] conversations in
            guard let strongSelf = self else { return }
            strongSelf.output.didFinishObtainingCurrentUserAndConversations(with: strongSelf.userService.currentUser, and: conversations)
        }
    }
    
    func refreshCurrentUserAndConversations() {
        obtainConversationsCache { [weak self] conversations in
            self?.userInfoService.obtainCurrentUserInfo()
        }
    }
    
    func logout() {
        
        userService.logout(beforeSuccess: { [weak self] _ in
            
            guard let strongSelf = self else { return }
            
            strongSelf.conversationsService.clearCachedConversations()
            strongSelf.messageHistoryService.clearCachedHistory(for: strongSelf.userService.currentUser)
            strongSelf.paymentService.deleteCurrentUserPaymentInfo()
            
        }) { [weak self] success in
            self?.output.didFinishLoggingOut(with: success)
        }
    }
    
    func sentEvent(with event: UserAccountEvents, selectedServiceName: String?, errorText: String?) {
        analyticsManager.sendUserAccountEvent(with: event, selectedServiceName: selectedServiceName, errorText: errorText)
    }
}

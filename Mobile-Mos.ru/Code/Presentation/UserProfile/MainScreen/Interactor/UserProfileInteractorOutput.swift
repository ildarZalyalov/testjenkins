//
//  UserProfileInteractorOutput.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 05.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol UserProfileInteractorOutput: class {
    
    func didFinishObtainingCurrentUserAndConversations(with user: User, and conversations: [Conversation])
    
    func didFinishRefreshingCurrentUserAndConversations(with user: User, and conversations: [Conversation])
    
    func didFinishLoggingOut(with success: Bool)
}

//
//  UserProfileEventHandler.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 27.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Оброботчик действий из профиля пользователя
protocol UserProfileEventHandler: class {
    
    /// Пользователь произвел действие "Выход" из профиля
    func didExitFromProfile()
}

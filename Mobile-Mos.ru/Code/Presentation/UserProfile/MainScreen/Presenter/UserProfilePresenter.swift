//
//  UserProfilePresenter.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 05.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class UserProfilePresenter: UserProfileViewOutput, UserProfileInteractorOutput, UserProfileAlertFactoryActionEventHandler {
    
    weak var view: UserProfileViewInput!
    var router: UserProfileRouterInput!
    var interactor: UserProfileInteractorInput!
    
    var userProfileViewModelBuilder: UserProfileViewModelBuilder!
    var userProfileFactory: UserProfileDataSourceFactory!
    
    var userProfileConfiguration: UserProfileDataSourceConfiguration!
    var userProfileEventHandler: UserProfileEventHandler?
    
    var stringsBuilder: UserProfileStringBuilder!
    
    func redisplayUserProfile(with user: User, conversations: [Conversation], and serviceInfo: [UserServiceInfo]?) {
        
        let name = stringsBuilder.fullName(of: user)
        let title = name.isEmpty ? StringsHelper.userProfileNoDataTitle : name
        let models = userProfileViewModelBuilder.buildUserProfileViewModel(for: user)
        
        userProfileConfiguration = userProfileFactory.buildDataSourceConfiguration(with: title, from: models, services: serviceInfo, conversations: conversations)
        
        view.displayUserProfileTitle(title: title)
        view.displayUserProfile(with: userProfileConfiguration.dataSource, and: [])
    }
    
    //MARK: - UserProfileViewOutput
    
    func setupInitialState() {
        interactor.obtainCurrentUserAndConversations()
        interactor.sentEvent(with: .screenLoaded(withString: AnalyticsConstants.screenLoadedEventName), selectedServiceName: nil, errorText: nil)
    }
    
    func setupUserProfileEventHandler(with handler: UserProfileEventHandler?) {
        userProfileEventHandler = handler
    }
    
    func didSelectRow(with objectId: String) {
        
        let selectionItems = userProfileConfiguration.selectionItems
        let servicesSelectionItems = userProfileConfiguration.servicesSelectionItems
        
        if let object = selectionItems[objectId] {
            
            let parameterName = AnalyticsConstants.selectedServiceNameEventName
            switch object.type {
            case .userInfo:
                router.showUserProfileInfo()
                interactor.sentEvent(with: .selectedService(withString: parameterName), selectedServiceName: StringsHelper.userPersonalInfoTitle, errorText: nil)
            case .notifications:
                router.showNotificationSettings()
                interactor.sentEvent(with: .selectedService(withString: parameterName), selectedServiceName: StringsHelper.userNotificationsTitle, errorText: nil)
            case .exit:
                router.showExitModule(with: self)
                interactor.sentEvent(with: .selectedService(withString: parameterName), selectedServiceName: StringsHelper.userExitTitle, errorText: nil)
            case .aboutApp:
                router.showAboutAppInfo()
                interactor.sentEvent(with: .selectedService(withString: parameterName), selectedServiceName: StringsHelper.userAboutAppTitle, errorText: nil)
            case .agreement:
                router.showAgreementModule()
                interactor.sentEvent(with: .selectedService(withString: parameterName), selectedServiceName: StringsHelper.userAgrementTitle, errorText: nil)
            case .appsAndServices:
                router.showServicesAndAppsModule()
                interactor.sentEvent(with: .selectedService(withString: parameterName), selectedServiceName: StringsHelper.ourAppsAndServicesText, errorText: nil)
            case .feedback:
                router.showSupportModule()
                interactor.sentEvent(with: .selectedService(withString: parameterName), selectedServiceName: StringsHelper.supportText, errorText: nil)
            default: return
            }
        }
        else if let service = servicesSelectionItems[objectId] {
            router.showServicesDetails(with: service)
        }
    }
    
    //MARK: - UserProfileInteractorOutput
    
    func didFinishObtainingCurrentUserAndConversations(with user: User, and conversations: [Conversation]) {
        redisplayUserProfile(with: user, conversations: conversations, and: user.userInfo?.serviceInfo)
        guard user.userInfo == nil else { return }
        interactor.refreshCurrentUserAndConversations()
    }
    
    func didFinishRefreshingCurrentUserAndConversations(with user: User, and conversations: [Conversation]) {
        
        guard user.userInfo != nil else {
            redisplayUserProfile(with: user, conversations: conversations, and: [])
            return
        }

        redisplayUserProfile(with: user, conversations: conversations, and: nil)
        
        let updates = userProfileFactory.updateDataSource(with: user.userInfo!.serviceInfo, conversations: conversations, in: &userProfileConfiguration!)
        view.displayUserProfile(with: userProfileConfiguration.dataSource, and: updates)
    }
    
    func didFinishLoggingOut(with success: Bool) {
        if success {
            userProfileEventHandler?.didExitFromProfile()
            router.closeModule()
            interactor.sentEvent(with: .exit(withString: AnalyticsConstants.exitFromAppEventName), selectedServiceName: nil, errorText: nil)
        }
        else {
            interactor.sentEvent(with: .exit(withString: AnalyticsConstants.exitFromAppEventName), selectedServiceName: nil, errorText: StringsHelper.userExitErrorMessage)
            router.showLogoutErrorModule()
        }
    }
    
    //MARK: - UserProfileAlertFactoryActionEventHandler
    
    func didSelectExit() {
        
        guard interactor.hasConnectionToServer else {
            router.showLogoutErrorModule()
            return
        }
        
        interactor.logout()
    }
}

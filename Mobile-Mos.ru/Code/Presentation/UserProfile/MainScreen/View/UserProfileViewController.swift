//
//  UserProfileViewController.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 05.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import UIKit

class UserProfileViewController: BaseViewController, UserProfileViewInput, UITableViewDelegate, NavigationBarCustomizingController, ConfigurableModuleController {

    @IBOutlet weak var tableView: UITableView!
    
    var output: UserProfileViewOutput!
    
    var tableViewDataDisplayManager: TableViewDataSource!
    
    var hasViewAppeared = false
    
    let navigationTitleAnimationDuration: TimeInterval = 0.2
    
    lazy var navigationBarTitleLabel: UILabel = {
        
        let label = UILabel()
        label.font = UIFont(customName: .graphikLCGBold, size: 15)
        label.textColor = UIColor.black
        label.textAlignment = .center
        label.alpha = 0
        label.text = StringsHelper.userProfileNoDataTitle
        
        return label
    }()
    
    lazy var navigationBarTitleView: UIView = {
        
        let titleView = UIView(frame: CGRect.zero)
        titleView.backgroundColor = UIColor.clear
        titleView.addSubview(navigationBarTitleLabel)
        
        navigationBarTitleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleView.addConstraints(NSLayoutConstraint.edgesConstraints(for: navigationBarTitleLabel))
        
        return titleView
    }()
    
    //MARK: - NavigationBarCustomizingController
    
    var navigationBarState: NavigationBarState? {
        
        var state = NavigationBarState()
        
        state.isHidden = false
        state.isTranslucent = false
        state.backgroundState = NavigationBarBackgroundState.regular
        state.tintColor = UIColor.black
        state.isShadowHidden = true
        
        let titleTextAttributes = [
            NSAttributedStringKey.font : UIFont(customName: .graphikLCGBold, size: 15) as Any,
            NSAttributedStringKey.foregroundColor : UIColor.black
        ]
        let largeTitleTextAttributes = [
            NSAttributedStringKey.font : UIFont(customName: .graphikLCGBold, size: 25) as Any,
            NSAttributedStringKey.foregroundColor : UIColorPalette.conversationsPageTitleColor
        ]
        
        state.titleTextAttributes = titleTextAttributes
        state.largeTitleTextAttributes = largeTitleTextAttributes
        
        return state
    }
    
    //MARK: - Методы
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        tableView.registerEmptyHeader()
        tableView.registerHeaderNib(for: UserProfileSectionHeaderObject.self)
        tableView.registerEmptyFooter()
        
        tableView.registerCellNib(for: UserProfileLargeTitleCellObject.self)
        tableView.registerCellNib(for: UserProfileCellObject.self)
        tableView.registerCellNib(for: ExitUserProfileCellObject.self)
        
        tableView.estimatedRowHeight = 50
        tableView.estimatedSectionHeaderHeight = 75
        
        tableView.assignEmptyTableHeaderToHideGroupedTableTopOffset()
        tableView.assignEmptyTableFooterToHideEmptyCellsSeparators()
        
        output.setupInitialState()
    }
    
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        
        guard !hasViewAppeared else { return }
        
        hasViewAppeared = true
        refreshNavigationTitleLayout()
    }
    
    @IBAction func unwindToUserProfile(_: UIStoryboardSegue) {}
    
    //MARK: - Приватные методы
    
    func setNavigationTitleHidden(hidden: Bool) {
        
        let alpha: CGFloat = hidden ? 0 : 1
        guard alpha != navigationBarTitleLabel.alpha else { return }
        
        UIView.animate(withDuration: navigationTitleAnimationDuration) {
            self.navigationBarTitleLabel.alpha = alpha
        }
    }
    
    func refreshNavigationTitleLayout() {
        
        DispatchQueue.main.async { [weak self] in
            
            guard let strongSelf = self else { return }
            guard strongSelf.hasViewAppeared else { return }
            
            strongSelf.navigationBarTitleLabel.sizeToFit()
            strongSelf.navigationBarTitleView.bounds = strongSelf.navigationBarTitleLabel.bounds
            
            strongSelf.navigationItem.titleView = strongSelf.navigationBarTitleView
        }
    }
    
    //MARK: - ConfigurableModuleController
    
    func configureModule(with object: Any) {
        if let object = object as? UserProfileEventHandler {
            output.setupUserProfileEventHandler(with: object)
        }
    }
    
    //MARK: - UserProfileViewInput
    
    func displayUserProfileTitle(title: String) {
        navigationBarTitleLabel.text = title
        refreshNavigationTitleLayout()
    }
    
    func displayUserProfile(with dataSource: TableViewDataSource, and updates: [TableViewDataSourceUpdate]) {
        
        tableViewDataDisplayManager = dataSource
        tableViewDataDisplayManager.delegate = self
        tableViewDataDisplayManager.assign(to: tableView)
        
        if updates.isEmpty {
            
            tableView.reloadData()
            
            // гарантируем мгновенное обновление
            // тогда быстрый запрос на обновление с анимацией не
            // запустит анимацию для всех изменений
            tableView.setNeedsLayout()
            tableView.layoutIfNeeded()
        }
        else {
            tableView.apply(updates: updates)
        }
    }
    
    //MARK: - UITableView
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let dataStructure = tableViewDataDisplayManager.dataStructure
        guard let cellObject = dataStructure.cellObject(at: indexPath) as? CellObjectWithId else {
            return
        }
        
        output.didSelectRow(with: cellObject.itemId)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let dataStructure = tableViewDataDisplayManager.dataStructure
        guard dataStructure.cellObject(at: indexPath) is UserProfileLargeTitleCellObject else { return }
        guard tableView.isDragging || tableView.isDecelerating else { return }
        setNavigationTitleHidden(hidden: true)
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let dataStructure = tableViewDataDisplayManager.dataStructure
        guard dataStructure.cellObject(at: indexPath) is UserProfileLargeTitleCellObject else { return }
        guard tableView.isDragging || tableView.isDecelerating else { return }
        setNavigationTitleHidden(hidden: false)
    }
}

//
//  UserProfileViewOutput.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 05.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol UserProfileViewOutput: class {
    
    func setupInitialState()
    
    func setupUserProfileEventHandler(with handler: UserProfileEventHandler?)
    
    func didSelectRow(with objectId: String)
}

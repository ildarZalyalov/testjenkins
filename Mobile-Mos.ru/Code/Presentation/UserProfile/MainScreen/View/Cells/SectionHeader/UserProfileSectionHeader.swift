//
//  UserProfileSectionHeader.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 06.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class UserProfileSectionHeader: UITableViewHeaderFooterView, ConfigurableView {
    
    @IBOutlet weak var sectionTitleLabel: UILabel!
    @IBOutlet weak var loadingStatusIndicator: UIActivityIndicatorView!
    
    func configure(with object: Any) {
        
        guard let cellObject = object as? UserProfileSectionHeaderObject else {
            return
        }
        
        sectionTitleLabel.text = cellObject.title
        
        if cellObject.displaysLoadingStatus {
            loadingStatusIndicator.startAnimating()
        }
        else {
            loadingStatusIndicator.stopAnimating()
        }
    }
}

//
//  UserProfileSectionHeaderObject.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 06.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Header object заголовка цекции пунктов меню в ЛК
struct UserProfileSectionHeaderObject: HeaderObject {
    
    /// Текст заголовка
    var title: String
    
    /// Показывается ли индикатор загрузки в заголовке
    var displaysLoadingStatus: Bool
}

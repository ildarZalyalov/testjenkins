//
//  UserProfileCell.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 05.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import UIKit

class UserProfileCell: UITableViewCell, ConfigurableView {
    
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var bottomSeparatorView: UIView!
    
    @IBOutlet weak var titleToBottomConstraint: NSLayoutConstraint!
    
    let subtitleLabelLineHeightMultiple: CGFloat = 1.38
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        contentView.alpha = highlighted ? 0.5 : 1
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        separatorInset = UIEdgeInsets(top: 0, left: contentView.width, bottom: 0, right: 0)
    }
    
    //MARK: - ConfigurableView
    
    func configure(with object: Any) {
        
        guard let cellObject = object as? UserProfileCellObject else {
            return
        }
        
        iconImageView.image = cellObject.icon
        
        if iconImageView.image == nil {
            
            var placeholder: UIImage? = nil
            if let iconName = cellObject.iconName {
                placeholder = UIImage(named: iconName)
            }
            
            iconImageView.loadAndDisplayImage(from: cellObject.iconUrl, placeholder: placeholder)
        }
        
        titleLabel.text = cellObject.title
        bottomSeparatorView.isHidden = cellObject.isLastCellInSection
        
        if let subtitle = cellObject.subtitle {
            
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.lineHeightMultiple = subtitleLabelLineHeightMultiple
            paragraphStyle.alignment = .left
            paragraphStyle.lineBreakMode = .byTruncatingTail
            
            let attributes: [NSAttributedStringKey : Any] = [NSAttributedStringKey.font : subtitleLabel.font, NSAttributedStringKey.paragraphStyle : paragraphStyle]
            let subtitleString = NSAttributedString(string: subtitle, attributes: attributes)
            
            subtitleLabel.attributedText = subtitleString
        }
        else {
            subtitleLabel.text = cellObject.subtitle
        }
        
        titleToBottomConstraint.priority = cellObject.subtitle != nil ? .defaultLow : .defaultHigh
    }
}

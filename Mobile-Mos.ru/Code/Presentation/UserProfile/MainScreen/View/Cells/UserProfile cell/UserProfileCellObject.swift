//
//  UserProfileCellObject.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 05.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Cell object ячейки пункта меню на главной странице ЛК
struct UserProfileCellObject: CellObjectWithId {
    
    /// Идентификатор ячейки
    var itemId: String
    
    /// Иконка пункта меню (если есть)
    var icon: UIImage?
    
    /// Ссылка на иконку пункта меню (если есть)
    var iconUrl: URL?
    
    /// Название иконки пункта меню (если есть)
    var iconName: String?
    
    /// Заголовок пункта меню
    var title: String
    
    /// Подзаголовок пункта меню (если есть)
    var subtitle: String?
    
    /// Является ли пункт меню последним в секции
    var isLastCellInSection: Bool
}

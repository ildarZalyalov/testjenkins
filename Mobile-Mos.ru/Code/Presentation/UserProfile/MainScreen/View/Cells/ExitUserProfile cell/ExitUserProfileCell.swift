//
//  ExitUserProfileCell.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 19.02.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

class ExitUserProfileCell: UITableViewCell, ConfigurableView {
    
    @IBOutlet weak var exitTitleLabel: UILabel!
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        contentView.alpha = highlighted ? 0.5 : 1
    }
    
    //MARK: - ConfigurableView
    
    func configure(with object: Any) {
        
        guard let cellObject = object as? ExitUserProfileCellObject else {
            return
        }
        
        exitTitleLabel.text = cellObject.title
    }
}

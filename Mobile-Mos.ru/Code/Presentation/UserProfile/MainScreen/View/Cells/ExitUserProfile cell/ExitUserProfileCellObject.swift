//
//  ExitUserProfileCellObject.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 19.02.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Cell object ячейки выхода из аккаунта в ЛК
struct ExitUserProfileCellObject: CellObjectWithId {
    
    /// Идентификатор ячейки
    var itemId: String
    
    /// Заголовок пункта меню
    var title: String
}

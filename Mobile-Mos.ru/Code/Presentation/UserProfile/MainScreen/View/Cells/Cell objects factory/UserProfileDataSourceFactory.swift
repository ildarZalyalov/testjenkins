//
//  UserProfileDataSourceFactory.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 05.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Конфигурация источника данных для ЛК
struct UserProfileDataSourceConfiguration {
    
    /// Источник данных
    var dataSource: TableViewDataSource
    
    /// Словарь с view model ячейки по идентификатору
    var selectionItems: [String : UserProfileRowViewModel]
    
    /// Словарь с сервисами по идентификатору
    var servicesSelectionItems: [String : UserServiceInfo]
}

/// Фабрика источника данных для ЛК
protocol UserProfileDataSourceFactory {
    
    /// Построить источник данных для ЛК
    ///
    /// - Parameters:
    ///   - title: заголовок страницы ЛК
    ///   - models: view models секций ячеек
    ///   - services: сервисы (если данные о них есть)
    ///   - conversations: список текущих чатов
    /// - Returns: конфигурация источника данных
    func buildDataSourceConfiguration(with title: String, from models: [UserProfileSectionViewModel], services: [UserServiceInfo]?, conversations: [Conversation]) -> UserProfileDataSourceConfiguration
    
    /// Обновить таблицу данными сервисов
    ///
    /// - Parameters:
    ///   - services: сервисы
    ///   - conversations: список текущих чатов
    ///   - configuration: текущая конфигурация источник данных
    /// - Returns: данные об обновлениях источника данных
    @discardableResult func updateDataSource(with services: [UserServiceInfo], conversations: [Conversation], in configuration: inout UserProfileDataSourceConfiguration) -> [TableViewDataSourceUpdate]
}

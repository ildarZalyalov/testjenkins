//
//  UserProfileDataSourceFactoryImplementation.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 05.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class UserProfileDataSourceFactoryImplementation: UserProfileDataSourceFactory {
    
    let servicesSectionIndex = 3
    
    let exitRowHeaderHeight: CGFloat = 24
    let exitRowViewModel = UserProfileRowViewModel(with: StringsHelper.userExitTitle,
                                                   accessoryType: .disclosure,
                                                   type: .exit)
    
    //MARK: - UserProfileDataSourceFactory
    
    func buildDataSourceConfiguration(with title: String, from models: [UserProfileSectionViewModel], services: [UserServiceInfo]?, conversations: [Conversation]) -> UserProfileDataSourceConfiguration {
        
        let dataStructure = TableViewDataSourceStructure()
        let servicesSelectionItems = [String : UserServiceInfo]()
        var selectionItems = [String : UserProfileRowViewModel]()
        var sectionIndex = 0
        
        let titleCellObject = UserProfileLargeTitleCellObject(title: title)
        dataStructure.appendSection(with: EmptyTableHeaderObject(), and: [titleCellObject])
        dataStructure.appendFooterObject(EmptyTableFooterObject())
        
        sectionIndex += 1
        
        for section in models {
            
            if sectionIndex == servicesSectionIndex {
                let servicesHeaderObject = UserProfileSectionHeaderObject(title: StringsHelper.userServicesInfoTitle, displaysLoadingStatus: services == nil)
                dataStructure.appendSection(with: servicesHeaderObject)
            }
            
            var sectionsItems = [UserProfileCellObject]()
            
            for item in section.sectionInfo.enumerated() {
                
                let isLastCellInSection = item.offset == (section.sectionInfo.count - 1)
                let cellObject = UserProfileCellObject(itemId: UUID().uuidString, icon: item.element.icon, iconUrl: nil, iconName: nil, title: item.element.title, subtitle: item.element.subtitle, isLastCellInSection: isLastCellInSection)
                
                selectionItems[cellObject.itemId] = item.element
                sectionsItems.append(cellObject)
            }
            
            let headerObject: HeaderObject = section.sectionTitle != nil ? UserProfileSectionHeaderObject(title: section.sectionTitle!, displaysLoadingStatus: false) : EmptyTableHeaderObject()
            dataStructure.appendSection(with: headerObject, and: sectionsItems)
            dataStructure.appendFooterObject(EmptyTableFooterObject())
            
            sectionIndex += 1
        }
        
        let exitCellObject = ExitUserProfileCellObject(itemId: UUID().uuidString, title: exitRowViewModel.title)
        selectionItems[exitCellObject.itemId] = exitRowViewModel
        
        dataStructure.appendSection(with: EmptyTableHeaderObject(headerHeight: exitRowHeaderHeight), and: [exitCellObject])
        dataStructure.appendFooterObject(EmptyTableFooterObject())
        
        let dataSource = TableViewDataSource(with: dataStructure)
        var configuration = UserProfileDataSourceConfiguration(dataSource: dataSource, selectionItems: selectionItems, servicesSelectionItems: servicesSelectionItems)
        
        if services != nil {
            updateDataSource(with: services!, conversations: conversations, in: &configuration)
        }
        
        return configuration
    }
    
    @discardableResult func updateDataSource(with services: [UserServiceInfo], conversations: [Conversation], in configuration: inout UserProfileDataSourceConfiguration) -> [TableViewDataSourceUpdate] {
        
        let dataSource = configuration.dataSource
        
        let numberOfServices = dataSource.dataStructure.numberOfObjects(at: servicesSectionIndex)
        for row in 0 ..< numberOfServices {
            let indexPath = IndexPath(row: row, section: servicesSectionIndex)
            dataSource.dataStructure.removeCellObject(at: indexPath)
        }
        
        let servicesHeaderObject = UserProfileSectionHeaderObject(title: StringsHelper.userServicesInfoTitle, displaysLoadingStatus: false)
        dataSource.dataStructure.addHeaderObject(servicesHeaderObject, to: servicesSectionIndex)
        
        let conversationsById = conversations.toDictionary { $0.itemId }

        for item in services.enumerated() {
            
            var conversation: Conversation? = nil
            if let chatId = item.element.chatId {
                conversation = conversationsById[chatId]
            }
            
            let cellObject = UserProfileCellObject(itemId: UUID().uuidString, icon: nil, iconUrl: conversation?.avatarMiniatureUrl, iconName: conversation?.avatarPlaceholderMiniatureName, title: item.element.title, subtitle: nil, isLastCellInSection: item.offset == (services.count - 1))
            
            configuration.servicesSelectionItems[cellObject.itemId] = item.element
            dataSource.dataStructure.addCellObject(cellObject, to: IndexPath(row: item.offset, section: servicesSectionIndex))
        }
        
        return [.reloadSections(sections: [servicesSectionIndex], animation: .fade)]
    }
}

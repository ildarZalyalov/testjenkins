//
//  UserProfileLargeTitleCellObject.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 07.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Cell object ячейки с большим заголовком
struct UserProfileLargeTitleCellObject: CellObject {
    
    /// Текст заголовка
    var title: String
}

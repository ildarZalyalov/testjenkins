//
//  UserProfileLargeTitleCell.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 07.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import UIKit

class UserProfileLargeTitleCell: UITableViewCell, ConfigurableView {
    
    @IBOutlet weak var largeTitleLabel: UILabel!
    
    let titleLabelLineHeightMultiple: CGFloat = 1.28
    
    //MARK: - ConfigurableView
    
    func configure(with object: Any) {
        
        guard let cellObject = object as? UserProfileLargeTitleCellObject else {
            return
        }
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineHeightMultiple = titleLabelLineHeightMultiple
        paragraphStyle.alignment = .left
        paragraphStyle.lineBreakMode = .byTruncatingTail
        
        let attributes: [NSAttributedStringKey : Any] = [NSAttributedStringKey.font : largeTitleLabel.font, NSAttributedStringKey.paragraphStyle : paragraphStyle]
        let titleString = NSAttributedString(string: cellObject.title, attributes: attributes)
        
        largeTitleLabel.attributedText = titleString
    }
}

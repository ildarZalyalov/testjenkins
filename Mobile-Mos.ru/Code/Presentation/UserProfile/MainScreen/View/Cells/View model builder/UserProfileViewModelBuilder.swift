//
//  UserProfileViewModelBuilder.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 15.02.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Билдер view models секций ячеек на главной странице ЛК
protocol UserProfileViewModelBuilder {
    
    /// Построить view models секций ячеек
    ///
    /// - Parameter user: текущий пользователь
    /// - Returns: view models секций ячеек
    func buildUserProfileViewModel(for user: User) -> [UserProfileSectionViewModel]
}

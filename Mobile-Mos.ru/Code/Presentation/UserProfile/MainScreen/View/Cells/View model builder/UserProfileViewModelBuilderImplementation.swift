//
//  UserProfileViewModelBuilderImplementation.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 15.02.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

class UserProfileViewModelBuilderImplementation: UserProfileViewModelBuilder {
    
    var stringBuilder: UserProfileStringBuilder!
    
    //MARK: - UserProfileViewModelBuilder
    
    func buildUserProfileViewModel(for user: User) -> [UserProfileSectionViewModel] {
        
        var models = [UserProfileSectionViewModel]()
        
        if user.isLoggedIn {
            
            let userInfo = UserProfileRowViewModel(with: StringsHelper.userPersonalInfoTitle, subtitle: stringBuilder.basicUserInfo(for: user), icon: #imageLiteral(resourceName: "profileIco"), type: .userInfo)
            let userPersonalInfoSection = UserProfileSectionViewModel(sectionInfo: [userInfo])
            
            models.append(userPersonalInfoSection)
            
            let userSettingsNotifications = UserProfileRowViewModel(with: StringsHelper.userNotificationsTitle, icon: #imageLiteral(resourceName: "notificationsButtonIcon"), type: .notifications)
            let userSettingsSection = UserProfileSectionViewModel(sectionInfo: [userSettingsNotifications], sectionTitle: StringsHelper.userSettingsTitle)
            
            models.append(userSettingsSection)
        }
        
        let userAboutApp = UserProfileRowViewModel(with: StringsHelper.userAboutAppTitle, icon: #imageLiteral(resourceName: "infoIco"), type: .aboutApp)
        let servicesAndApps = UserProfileRowViewModel(with: StringsHelper.ourAppsAndServicesText, icon: #imageLiteral(resourceName: "appsIcon"), type: .appsAndServices)
        let userAgreement = UserProfileRowViewModel(with: StringsHelper.userAgrementTitle, icon: #imageLiteral(resourceName: "checkIco"), type: .agreement)
        let feedback = UserProfileRowViewModel(with: StringsHelper.supportText, icon: #imageLiteral(resourceName: "supportIcon"), type: .feedback)
        let userOtherSection = UserProfileSectionViewModel(sectionInfo: [userAboutApp, servicesAndApps, userAgreement, feedback], sectionTitle: StringsHelper.userOtherTitle)
        
        models.append(userOtherSection)
        
        return models
    }
}

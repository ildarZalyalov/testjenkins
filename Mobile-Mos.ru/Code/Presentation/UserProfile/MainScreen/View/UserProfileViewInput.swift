//
//  UserProfileViewInput.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 05.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol UserProfileViewInput: class {
    
    func displayUserProfileTitle(title: String)
    
    func displayUserProfile(with dataSource: TableViewDataSource, and updates: [TableViewDataSourceUpdate])
}

//
//  UserServiceDetailAutocompleteRouter.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 29.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

class UserServiceDetailAutocompleteRouter: UserServiceDetailAutocompleteRouterInput {
    
    weak var transitionHandler: UIViewController!
    
    let unwindToUserServiceDetailEditSegueIdentifier = "unwindToUserServiceDetailEdit"
    
    //MARK: - UserServiceDetailAutocompleteRouterInput
    
    func closeModule(completion: (() -> ())?) {
        transitionHandler.performSegue(withIdentifier: unwindToUserServiceDetailEditSegueIdentifier, sender: self)
        transitionHandler.transitionCoordinator?.animate(alongsideTransition: nil, completion: { _ in
            completion?()
        })
    }
}

//
//  UserServiceAutocompleteDataSourceFactory.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 29.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

typealias UserServiceAutocompleteDataSourceConfiguration = (dataSource: TableViewDataSource, selectionItems: [String : UserOptionsAutocompleteValue])

protocol UserServiceAutocompleteDataSourceFactory {
    
    func buildDataSourceConfiguration(from models: [UserOptionsAutocompleteValue], searchString: String) -> UserServiceAutocompleteDataSourceConfiguration
}

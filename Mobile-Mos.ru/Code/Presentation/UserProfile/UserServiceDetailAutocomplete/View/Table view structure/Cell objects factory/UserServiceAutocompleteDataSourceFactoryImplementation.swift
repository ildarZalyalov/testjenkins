//
//  UserServiceAutocompleteDataSourceFactoryImplementation.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 29.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

class UserServiceAutocompleteDataSourceFactoryImplementation: UserServiceAutocompleteDataSourceFactory {
    
    //MARK: - UserServiceAutocompleteDataSourceFactory
    
    func buildDataSourceConfiguration(from models: [UserOptionsAutocompleteValue], searchString: String) -> UserServiceAutocompleteDataSourceConfiguration {
        
        let dataStructure = TableViewDataSourceStructure()
        var selectionItems = [String : UserOptionsAutocompleteValue]()
        
        for model in models {
            
            let cellObject = UserServiceAutocompleteCellObject(itemId: UUID().uuidString,
                                                               name: model.name,
                                                               searchString: searchString)
            
            dataStructure.appendCellObject(cellObject)
            selectionItems[cellObject.itemId] = model
        }
        
        let dataSource = TableViewDataSource(with: dataStructure)
        
        return (dataSource, selectionItems)
    }
}

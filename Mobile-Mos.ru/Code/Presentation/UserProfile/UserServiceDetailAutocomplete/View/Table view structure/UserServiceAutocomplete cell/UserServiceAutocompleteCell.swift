//
//  UserServiceAutocompleteCell.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 29.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

class UserServiceAutocompleteCell: UITableViewCell, ConfigurableView {
    
    @IBOutlet weak var nameLabel: UILabel!
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        super.setHighlighted(highlighted, animated: animated)
        contentView.alpha = highlighted ? 0.5 : 1
    }
    
    let lineHeightMultiple: CGFloat = 1.31
    
    //MARK: - ConfigurableView
    
    func configure(with object: Any) {
        
        guard let cellObject = object as? UserServiceAutocompleteCellObject else {
            return
        }
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineHeightMultiple = lineHeightMultiple
        paragraphStyle.alignment = .left
        paragraphStyle.lineBreakMode = .byTruncatingTail
        
        let attributes: [NSAttributedStringKey : Any] = [NSAttributedStringKey.font : nameLabel.font, NSAttributedStringKey.paragraphStyle : paragraphStyle]
        let nameString = NSMutableAttributedString(string: cellObject.name, attributes: attributes)
        
        let range = (cellObject.name as NSString).range(of: cellObject.searchString, options: .caseInsensitive)
        if range.location != NSNotFound {
            nameString.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColorPalette.searchStringSubstringColor, range: range);
        }
        
        nameLabel.attributedText = nameString
    }
}

//
//  UserServiceAutocompleteCellObject.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 29.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

struct UserServiceAutocompleteCellObject: CellObjectWithId {
    
    var itemId: String
    
    var name: String
    
    var searchString: String
}

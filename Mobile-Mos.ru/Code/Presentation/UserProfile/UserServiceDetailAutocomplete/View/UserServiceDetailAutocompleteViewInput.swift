//
//  UserServiceDetailAutocompleteViewInput.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 29.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

protocol UserServiceDetailAutocompleteViewInput: class {
    
    func display(title: String)
    
    func displayAutocompleteResult(with dataSource: TableViewDataSource)
}

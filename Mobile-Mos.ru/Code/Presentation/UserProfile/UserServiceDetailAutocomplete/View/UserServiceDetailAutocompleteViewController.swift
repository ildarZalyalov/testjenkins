//
//  UserServiceDetailAutocompleteViewController.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 29.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import UIKit

class UserServiceDetailAutocompleteViewController: BaseViewController, UserServiceDetailAutocompleteViewInput, ConfigurableModuleController, UITextFieldDelegate, UITableViewDelegate {
    
    var output: UserServiceDetailAutocompleteViewOutput!
    
    @IBOutlet weak var cancelButton: UIBarButtonItem!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchField: UITextField!
    @IBOutlet weak var loadingProgressView: LoadingTimeoutProgressView!
    @IBOutlet weak var startTypingHelpTextLabel: UILabel!
    
    @IBOutlet weak var searchFieldBottomConstraint: NSLayoutConstraint!
    
    let navigationItemsFontSize: CGFloat = 15
    let searchRequestTimeout: TimeInterval = 0.5
    let timeOutIntervalForProgress: TimeInterval = 20
    
    var notificationCenter: NotificationCenter!
    var tableViewDataDisplayManager: TableViewDataSource!
    
    lazy var searchRequestPerformer: DelayedSelectorPerformer = {
        return DelayedSelectorPerformer(target: self, selector: #selector(beginSearching(with:)))
    }()
    
    //MARK: - Методы
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        let titleAttributes: [NSAttributedStringKey : Any] = [NSAttributedStringKey.font : UIFont(customName: .graphikLCGRegular, size: 15) as Any]
        cancelButton.setTitleTextAttributes(titleAttributes, for: .normal)
        cancelButton.setTitleTextAttributes(titleAttributes, for: .highlighted)
        
        startTypingHelpTextLabel.text = StringsHelper.chatAutocompleteStartHelpText
        
        tableView.assignEmptyTableFooterToHideEmptyCellsSeparators()
        
        searchField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        searchField.becomeFirstResponder()
        
        registerForKeyboardNotifications()
        
        output.setupInitialState()
    }
    
    deinit {
        notificationCenter.removeObserver(self)
    }
    
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    @IBAction func cancelButtonPressed(_ sender: Any) {
        searchField.resignFirstResponder()
        output.didPressCancel()
    }
    
    //MARK: - ConfigurableModuleController
    
    func configureModule(with object: Any) {
        if let configuration = object as? UserServiceDetailAutocompleteModuleConfiguration {
            output.configure(with: configuration)
        }
    }
    
    //MARK: - UserServiceDetailAutocompleteViewInput
    
    func display(title: String) {
        navigationItem.title = title
    }
    
    func displayAutocompleteResult(with dataSource: TableViewDataSource) {
        
        startTypingHelpTextLabel.isHidden = true
        
        tableViewDataDisplayManager = dataSource
        tableViewDataDisplayManager.delegate = self
        tableViewDataDisplayManager.assign(to: tableView)
        
        tableView.isHidden = dataSource.dataStructure.isEmpty()
        
        hideLoadingIndicator()
        tableView.reloadData()
    }
    
    //MARK: - KeyboardNotifications
    
    @objc func keyboardWillShow(with notification: Notification) {
        
        guard let info = notification.userInfo,
            let keyboardEndSize = (info[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size
            else { return }
        
        searchFieldBottomConstraint.constant = keyboardEndSize.height
        
        if #available(iOS 11.0, *) {
            searchFieldBottomConstraint.constant -= view.safeAreaInsets.bottom
        }
    }
    
    //MARK: - UITableViewDelegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: false)
        
        let dataStructure = tableViewDataDisplayManager.dataStructure
        guard let cellObject = dataStructure.cellObject(at: indexPath) as? CellObjectWithId else {
            return
        }
        
        output.didSelectObject(with: cellObject.itemId)
    }
    
    //MARK: - Custom methods
    
    func registerForKeyboardNotifications(){
        
        notificationCenter.addObserver(self,
                                       selector: #selector(keyboardWillShow(with:)),
                                       name: NSNotification.Name.UIKeyboardWillShow,
                                       object: nil)
    }
    
    @objc
    func textFieldDidChange(_ textField: UITextField) {
        
        searchRequestPerformer.cancelDelayedPerform()
        
        if let searchText = textField.text, !searchText.isEmpty {
            searchRequestPerformer.argument = searchText
            searchRequestPerformer.perform(afterDelay: searchRequestTimeout)
        }
    }
    
    @objc
    func beginSearching(with text: String) {
        
        guard output.canSearch(with: text) else { return }
        
        displayLoadingIndicator()
        output.didSearch(with: text)
    }
    
    func hideLoadingIndicator() {
        
        let hideLoadingHandler: () -> Void = { [weak self] in
            self?.loadingProgressView.completeProgressTimer()
        }
        
        DispatchQueue.main.async(execute: hideLoadingHandler)
    }
    
    func displayLoadingIndicator() {
        loadingProgressView.isHidden = false
        loadingProgressView.startProgressTimer(with: timeOutIntervalForProgress)
    }
}

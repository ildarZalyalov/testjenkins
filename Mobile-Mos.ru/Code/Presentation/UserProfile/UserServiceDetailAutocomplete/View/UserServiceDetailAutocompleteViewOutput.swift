//
//  UserServiceDetailAutocompleteViewOutput.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 29.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

protocol UserServiceDetailAutocompleteViewOutput: class {
    
    func setupInitialState()
    
    func configure(with configuration: UserServiceDetailAutocompleteModuleConfiguration)
    
    func didPressCancel()
    
    func canSearch(with text: String) -> Bool
    
    func didSelectObject(with valueId: String)
    
    func didSearch(with text: String)
}

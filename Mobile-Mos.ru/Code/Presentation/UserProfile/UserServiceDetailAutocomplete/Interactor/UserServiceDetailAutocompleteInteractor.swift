//
//  UserServiceDetailAutocompleteInteractor.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 29.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

class UserServiceDetailAutocompleteInteractor: UserServiceDetailAutocompleteInteractorInput {
    
    weak var output: UserServiceDetailAutocompleteInteractorOutput!
    var userDataOptionsService: UserDataOptionsService!
    
    weak var currentAutocompleteRequest: HTTPRequest?
    
    //MARK: - UserServiceDetailAutocompleteInteractorInput
    
    var searchStringMinimumLength: Int {
        return 3
    }
    
    func searchAutocomplete(for optionType: String, with string: String) {
        currentAutocompleteRequest?.cancel()
        currentAutocompleteRequest = userDataOptionsService.obtainUserDataOptionAutocomplete(for: optionType, and: string, completion: { [weak self] result in
            self?.output.didFinishSearchingAutocomplete(with: result)
        })
    }
}

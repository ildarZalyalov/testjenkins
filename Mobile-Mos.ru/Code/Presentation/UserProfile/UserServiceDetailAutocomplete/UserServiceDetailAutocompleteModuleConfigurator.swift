//
//  UserServiceDetailAutocompleteModuleConfigurator.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 29.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation
import UIKit

class UserServiceDetailAutocompleteModuleConfigurator: BaseModuleConfigurator {
    
    override func configureModule(for view: UIViewController) {
        
        let viewController = view as! UserServiceDetailAutocompleteViewController
        let presenter = UserServiceDetailAutocompletePresenter()
        let interactor = UserServiceDetailAutocompleteInteractor()
        let router = UserServiceDetailAutocompleteRouter()
        
        let userDataOptionsService = UIApplication.shared.serviceBuilder.getUserDataOptionsService()
        let notificationCenter = NotificationCenter.default
        let dataSourceFactory = UserServiceAutocompleteDataSourceFactoryImplementation()
        
        viewController.output = presenter
        viewController.notificationCenter = notificationCenter
        
        presenter.view = viewController
        presenter.interactor = interactor
        presenter.router = router
        presenter.dataSourceFactory = dataSourceFactory
        
        interactor.output = presenter
        interactor.userDataOptionsService = userDataOptionsService
        
        router.transitionHandler = viewController
    }
}

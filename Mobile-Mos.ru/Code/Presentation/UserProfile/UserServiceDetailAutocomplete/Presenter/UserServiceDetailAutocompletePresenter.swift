//
//  UserServiceDetailAutocompletePresenter.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 29.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

class UserServiceDetailAutocompletePresenter: UserServiceDetailAutocompleteViewOutput, UserServiceDetailAutocompleteInteractorOutput {
    
    weak var view: UserServiceDetailAutocompleteViewInput!
    var router: UserServiceDetailAutocompleteRouterInput!
    var interactor: UserServiceDetailAutocompleteInteractorInput!
    
    var configuration: UserServiceDetailAutocompleteModuleConfiguration!
    
    var dataSourceFactory: UserServiceAutocompleteDataSourceFactory!
    var selectionItems: [String : UserOptionsAutocompleteValue]!
    
    var searchText = String()
    
    //MARK: - UserServiceDetailAutocompleteViewOutput
    
    func setupInitialState() {
        view.display(title: configuration.metadata.name)
    }
    
    func configure(with configuration: UserServiceDetailAutocompleteModuleConfiguration) {
        self.configuration = configuration
    }
    
    func didPressCancel() {
        router.closeModule(completion: nil)
    }
    
    func canSearch(with text: String) -> Bool {
        return text.count >= interactor.searchStringMinimumLength
    }
    
    func didSelectObject(with valueId: String) {
        
        guard let value = selectionItems[valueId] else { return }
        
        configuration.eventHandler?.didSelectAutocompleteValue(value, forFieldAt: configuration.fieldIndex, with: configuration.metadata)
        router.closeModule(completion: nil)
    }
    
    func didSearch(with text: String) {
        
        guard canSearch(with: text) else { return }
        
        searchText = text
        interactor.searchAutocomplete(for: configuration.metadata.type, with: text)
    }
    
    //MARK: - UserServiceDetailAutocompleteInteractorOutput
    
    func didFinishSearchingAutocomplete(with result: UserDataOptionsAutocompleteResult) {
        
        switch result {
            
        case .success(let values):
            
            let configuration = dataSourceFactory.buildDataSourceConfiguration(from: values, searchString: searchText)
            
            selectionItems = configuration.selectionItems
            view.displayAutocompleteResult(with: configuration.dataSource)
            
        case .failure(let error):
            print("Ошибка автокомплита: \(error.localizedDescription)")
        }
    }
}

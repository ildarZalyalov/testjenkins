//
//  UserServiceDetailAutocompleteModuleConfiguration.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 29.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

protocol UserServiceDetailAutocompleteEventHandler: class {
    
    func didSelectAutocompleteValue(_ value: UserOptionsAutocompleteValue, forFieldAt index: Int, with metadata: UserOptionMetadata)
}

struct UserServiceDetailAutocompleteModuleConfiguration {

    let metadata: UserOptionMetadata
    
    let fieldIndex: Int
    
    var eventHandler: UserServiceDetailAutocompleteEventHandler?
    
    init(metadata: UserOptionMetadata, fieldIndex: Int) {
        self.metadata = metadata
        self.fieldIndex = fieldIndex
    }
}

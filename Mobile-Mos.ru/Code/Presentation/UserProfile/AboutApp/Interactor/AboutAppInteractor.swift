//
//  AboutAppInteractor.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 20.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class AboutAppInteractor: AboutAppInteractorInput {
    
    weak var output: AboutAppInteractorOutput!
    var appUpdateVerifier: AppUpdateNotificationVerifier!
    var analyticsManager: AnalyticsManager!
    
    //MARK: - AboutAppInteractorInput
    
    func refreshInfoAboutAppNewVersion() {
        
        appUpdateVerifier.checkForApplicationUpdates { [weak self] result in
            
            var newVersionInfo: iTunesVersionLookupState? = nil
            
            switch result {
            case .success(let state, _):
                newVersionInfo = state
            case .failure:
                return
            }
            
            self?.output.didFinishRefreshingInfoAboutAppNewVersion(with: newVersionInfo)
        }
    }
    
    func sendEvent(with event: AboutAppEvents, sharingData: SharingEventData?) {
        analyticsManager.sendAboutAppEvent(with: event, sharingData: sharingData)
    }
}

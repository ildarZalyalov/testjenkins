//
//  AboutAppInteractorInput.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 20.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol AboutAppInteractorInput: class {

    func refreshInfoAboutAppNewVersion()
    
    func sendEvent(with event: AboutAppEvents, sharingData: SharingEventData?)
}

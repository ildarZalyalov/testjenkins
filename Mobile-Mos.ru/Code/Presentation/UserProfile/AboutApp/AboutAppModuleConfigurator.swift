//
//  AboutAppModuleConfigurator.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 20.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import UIKit

class AboutAppModuleConfigurator: BaseModuleConfigurator {
    
    override func configureModule(for view: UIViewController) {
        
        let viewController = view as! AboutAppViewController
        let presenter = AboutAppPresenter()
        let interactor = AboutAppInteractor()
        let router = AboutAppRouter()
        
        let appUpdateVerifier = AppUpdateNotificationVerifier.getAppUpdateNotificationVerifier()
        let aboutAppDataSourceFactory = AboutAppDataSourceFactoryImplementation()
        let alertsFactory = CommonAlertsFactoryImplementation()
        let analyticsManager = AnalyticsManagerBuilder.getAnalyticsManager()
        
        viewController.output = presenter
        
        presenter.view = viewController
        presenter.interactor = interactor
        presenter.router = router
        presenter.aboutAppDataSourceFactory = aboutAppDataSourceFactory
        
        interactor.output = presenter
        interactor.appUpdateVerifier = appUpdateVerifier
        interactor.analyticsManager = analyticsManager
        
        router.transitionHandler = viewController
        router.commonAlertsFactory = alertsFactory
        router.analyticsManager = analyticsManager
    }
}

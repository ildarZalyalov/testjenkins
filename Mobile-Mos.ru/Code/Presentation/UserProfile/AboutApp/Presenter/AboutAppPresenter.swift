//
//  AboutAppPresenter.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 20.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class AboutAppPresenter: AboutAppViewOutput, AboutAppInteractorOutput, AppStoreButtonCellDelegate {
    
    weak var view: AboutAppViewInput!
    var router: AboutAppRouterInput!
    var interactor: AboutAppInteractorInput!
    
    var aboutAppDataSourceFactory: AboutAppDataSourceFactory!
    var selectionItemsById:[String : AboutAppModel]!
    
    var newVersionInfo: iTunesVersionLookupState?
    
    func redisplayAboutAppTable() {
        let datasource = aboutAppDataSourceFactory.buildDataSourceConfiguration(with: newVersionInfo, cellDelegate: self)
        view.displayAboutAppTable(with: datasource.dataSource)
        selectionItemsById = datasource.selectionItems
    }
    
    //MARK: - AboutAppViewOutput
    
    func setupInitialState() {
        
        redisplayAboutAppTable()
        interactor.refreshInfoAboutAppNewVersion()
        interactor.sendEvent(with: .screenLoaded(withString: AnalyticsConstants.screenLoadedEventName), sharingData: nil)
    }
    
    func didSelectRow(with objectId: String) {
        
        guard let object = selectionItemsById[objectId] else { return }
        
        switch object.title {
        case StringsHelper.shareToFriendText:
            router.showShareViewController()
        case StringsHelper.rateAppText:
            router.showAppInAppStoreAndOpenReview()
        default:
            return 
        }
    }
    
    func didPressMoreButton() {
        interactor.sendEvent(with: .more(withString: AnalyticsConstants.moreEventName), sharingData: nil)
    }
    
    //MARK: - AboutAppInteractorOutput
    
    func didFinishRefreshingInfoAboutAppNewVersion(with info: iTunesVersionLookupState?) {
        newVersionInfo = info
        redisplayAboutAppTable()
    }
    
    //MARK: - AppStoreButtonCellDelegate
    
    func didPressAppStoreButton() {
        if let version = newVersionInfo?.version, let releaseNotes = newVersionInfo?.releaseNotes {
            router.showAppNewVersionModule(with: version, and: releaseNotes)
        }
        else {
            router.showAppInAppStore()
            interactor.sendEvent(with: .openInAppstore(withString: AnalyticsConstants.openInAppStoreEventName), sharingData: nil)
        }
    }
}

//
//  AboutAppRouterInput.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 20.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol AboutAppRouterInput: class {
    
    func showAppInAppStore()
    
    func showAppInAppStoreAndOpenReview()
    
    func showAppNewVersionModule(with version: String, and releaseNotes: String)
    
    func showShareViewController()
}

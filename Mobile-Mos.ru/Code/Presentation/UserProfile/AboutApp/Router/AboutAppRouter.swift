//
//  AboutAppRouter.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 20.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import SafariServices

class AboutAppRouter: AboutAppRouterInput {
    
    weak var transitionHandler: UIViewController!
    var commonAlertsFactory: CommonAlertsFactory!
    var analyticsManager: AnalyticsManager!
    
    //MARK: - AboutAppRouterInput
    
    func showAppInAppStore() {
        if UIApplication.shared.canOpenURL(applicationUrl) {
            UIApplication.shared.open(url: applicationUrl)
        }
    }
    
    func showAppInAppStoreAndOpenReview() {
        
        if #available(iOS 10.3, *) {
            if UIApplication.shared.canOpenURL(applicationReviewInAppStoreUrl) {
                UIApplication.shared.open(url: applicationReviewInAppStoreUrl)
            }
        }
        else {
            showAppInAppStore()
        }
    }
    
    func showAppNewVersionModule(with version: String, and releaseNotes: String) {
        let alert = commonAlertsFactory.getNewApplicationVersionAlert(with: version, and: releaseNotes)
        transitionHandler.present(alert, animated: true, completion: nil)
    }
    
    func showShareViewController() {
        
        let stringToShare = StringsHelper.appUrlText + applicationUrl.absoluteString
        let activityController = UIActivityViewController(activityItems: [stringToShare], applicationActivities: nil)
        
        activityController.completionWithItemsHandler = { [weak self] activityType, completed, items, error  in
            
            let errorValue = error?.localizedDescription ?? AnalyticsConstants.emptyValueName
            
            if completed {
                
                let sharingData: SharingEventData = (activityType?.rawValue ?? AnalyticsConstants.emptyValueName, errorValue)
                self?.analyticsManager.sendAboutAppEvent(with: .sharing(withString: AnalyticsConstants.sharingToAppEventName), sharingData: sharingData)
            }
        }
        
        transitionHandler.present(activityController, animated: true, completion: nil)
    }
    
}

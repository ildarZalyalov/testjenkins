//
//  AboutAppViewOutput.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 20.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol AboutAppViewOutput: class {
    
    func setupInitialState()
    
    func didSelectRow(with objectId: String)
    
    func didPressMoreButton()
}

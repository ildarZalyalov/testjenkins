//
//  AboutAppCellObject.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 20.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

struct AboutAppCellObject: CellObjectWithId {
    
    var itemId: String
    var icon: UIImage?
    var title: String
    var isLastCellInSection: Bool
}

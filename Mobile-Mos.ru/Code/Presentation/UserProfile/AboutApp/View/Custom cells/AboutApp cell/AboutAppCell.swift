//
//  AboutAppCell.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 20.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import UIKit

class AboutAppCell: UITableViewCell, ConfigurableView {
    
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var bottomSeparatorView: UIView!
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        contentView.alpha = highlighted ? 0.5 : 1
    }
    
    //MARK: - ConfigurableView
    
    func configure(with object: Any) {
        
        guard let cellObject = object as? AboutAppCellObject else {
            return
        }
        
        iconImageView.image = cellObject.icon
        titleLabel.text = cellObject.title
        bottomSeparatorView.isHidden = cellObject.isLastCellInSection
    }
}

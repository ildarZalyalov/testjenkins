//
//  AboutAppDataSourceFactory.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 20.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

typealias AboutAppDataSourceConfiguration = (dataSource: TableViewDataSource, selectionItems: [String : AboutAppModel])

protocol AboutAppDataSourceFactory {
    
    func buildDataSourceConfiguration(with newVersionInfo: iTunesVersionLookupState?, cellDelegate: AppStoreButtonCellDelegate?) -> AboutAppDataSourceConfiguration
}

//
//  AboutAppDataSourceFactoryImplementation.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 20.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class AboutAppDataSourceFactoryImplementation: AboutAppDataSourceFactory {
    
    fileprivate lazy var aboutAppCellsData: [AboutAppModel] = {
        
        let shareToFriend = AboutAppModel(icon: #imageLiteral(resourceName: "shareButtonIcon"), title: StringsHelper.shareToFriendText)
        let rateApp = AboutAppModel(icon: #imageLiteral(resourceName: "favorites-ico"), title: StringsHelper.rateAppText)
        
        return [shareToFriend, rateApp]
    }()
    
    //MARK: - AboutAppDataSourceFactory
    
    func buildDataSourceConfiguration(with newVersionInfo: iTunesVersionLookupState?, cellDelegate: AppStoreButtonCellDelegate?) -> AboutAppDataSourceConfiguration {
        
        let dataStructure = TableViewDataSourceStructure()
        var selectionItems = [String : AboutAppModel]()
        
        for model in aboutAppCellsData.enumerated() {
            
            let cellObject = AboutAppCellObject(itemId: model.element.title, icon: model.element.icon, title: model.element.title, isLastCellInSection: model.offset == (aboutAppCellsData.count - 1))
            
            dataStructure.appendCellObject(cellObject)
            selectionItems[cellObject.itemId] = model.element
        }
        
        var appStoreButtonTitle = StringsHelper.openInAppStoreButtonTitle
        if newVersionInfo?.version != nil && newVersionInfo?.releaseNotes != nil {
            appStoreButtonTitle = StringsHelper.newAppVersionAvailableButtonTitle
        }
        
        let appStoreCellObject = AppStoreButtonCellObject(itemId: "0", title: appStoreButtonTitle, cellDelegate: cellDelegate)
        dataStructure.appendCellObject(appStoreCellObject)
        
        let dataSource = TableViewDataSource(with: dataStructure)
        
        return (dataSource: dataSource, selectionItems: selectionItems)
    }
}

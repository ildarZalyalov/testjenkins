//
//  AppStoreButtonCellObject.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 20.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

struct AppStoreButtonCellObject: CellObjectWithId {
    
    var itemId: String
    var title: String
    var cellDelegate: AppStoreButtonCellDelegate?
}

//
//  AppStoreButtonCell.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 20.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import UIKit

protocol AppStoreButtonCellDelegate: class {
    
    func didPressAppStoreButton()
}

class AppStoreButtonCell: UITableViewCell, ConfigurableView {
    
    @IBOutlet weak var appStoreButton: UIButton!
    
    var cellDelegate: AppStoreButtonCellDelegate?
    
    //MARK: - ConfigurableView
    
    func configure(with object: Any) {
        
        guard let cellObject = object as? AppStoreButtonCellObject else {
            return
        }
        
        cellDelegate = cellObject.cellDelegate
        appStoreButton.setTitle(cellObject.title, for: .normal)
    }
    
    @IBAction func appStoreButtonPressed(_ sender: Any) {
        cellDelegate?.didPressAppStoreButton()
    }
}

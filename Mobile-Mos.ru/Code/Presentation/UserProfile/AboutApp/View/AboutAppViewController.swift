//
//  AboutAppViewController.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 20.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import UIKit

class AboutAppViewController: BaseViewController, AboutAppViewInput, UITableViewDelegate, NavigationBarCustomizingController {
    
    var output: AboutAppViewOutput!
    var tableViewDataDisplayManager: TableViewDataSource!
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var moreInfoButton: UIButton!
    
    @IBOutlet weak var appTitleLabel: UILabel!
    @IBOutlet weak var appVersionLabel: UILabel!
    
    @IBOutlet var descriptionLabelHeightConstraint: NSLayoutConstraint!
    
    let descriptionLabelLineHeightMultiple: CGFloat = 1.53
    
    //MARK: - Методы
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        setupTableView()
        
        if let name = Bundle.main.appDisplayName {
            appTitleLabel.text = name
        }
        
        if let version = Bundle.main.releaseVersionNumber {
            appVersionLabel.text = StringsHelper.appVersionPrefix + " " + version
        }
        
        apply(text: StringsHelper.appDescription, to: descriptionLabel, with: descriptionLabelLineHeightMultiple)
        
        output.setupInitialState()
    }
    
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    //MARK: - Private
    
    fileprivate func setupTableView() {
        
        tableView.assignEmptyTableFooterToHideEmptyCellsSeparators()
        
        tableView.registerCellNib(for: AboutAppCellObject.self)
        tableView.registerCellNib(for: AppStoreButtonCellObject.self)
        
        tableView.estimatedRowHeight = 54
        tableView.estimatedSectionHeaderHeight = 1
    }
    
    func apply(text: String, to label: UILabel, with lineHeightMultiple: CGFloat) {
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineHeightMultiple = lineHeightMultiple
        paragraphStyle.alignment = .left
        paragraphStyle.lineBreakMode = .byTruncatingTail
        
        let attributes: [NSAttributedStringKey : Any] = [NSAttributedStringKey.font : label.font, NSAttributedStringKey.paragraphStyle : paragraphStyle]
        let valueString = NSAttributedString(string: text, attributes: attributes)
        
        label.attributedText = valueString
    }
    
    //MARK: - NavigationBarCustomizingController
    
    var navigationBarState: NavigationBarState? {
        
        var state = NavigationBarState()
        
        state.isHidden = false
        state.isTranslucent = false
        state.backgroundState = NavigationBarBackgroundState.regular
        state.tintColor = UIColor.black
        state.isShadowHidden = true
        
        let titleTextAttributes = [
            NSAttributedStringKey.font : UIFont(customName: .graphikLCGBold, size: 15) as Any,
            NSAttributedStringKey.foregroundColor : UIColor.black
        ]
        let largeTitleTextAttributes = [
            NSAttributedStringKey.font : UIFont(customName: .graphikLCGBold, size: 25) as Any,
            NSAttributedStringKey.foregroundColor : UIColorPalette.conversationsPageTitleColor
        ]
        
        state.titleTextAttributes = titleTextAttributes
        state.largeTitleTextAttributes = largeTitleTextAttributes
        
        return state
    }
    
    //MARK: - AboutAppViewInput
    
    func displayAboutAppTable(with dataSource: TableViewDataSource) {
        
        tableViewDataDisplayManager = dataSource
        tableViewDataDisplayManager.delegate = self
        tableViewDataDisplayManager.assign(to: tableView)
        
        tableView.reloadData()
    }
    
    //MARK: - UITableView
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let dataStructure = tableViewDataDisplayManager.dataStructure
        guard let cellObject = dataStructure.cellObject(at: indexPath) as? CellObjectWithId else {
            return
        }
        
        output.didSelectRow(with: cellObject.itemId)
    }
    
    @IBAction func moreButtonPressed(_ sender: UIButton) {
        
        sender.isSelected = !sender.isSelected
        descriptionLabelHeightConstraint.isActive = !sender.isSelected
        moreInfoButton.isHidden = true
        
        output.didPressMoreButton()
        
        view.layoutIfNeeded()
    }
}

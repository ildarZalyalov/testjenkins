//
//  AboutAppModel.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 20.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Модель элемента меню на экране "О приложении"
struct AboutAppModel {
    
    /// Иконка
    var icon: UIImage?
    
    /// Заголовок
    var title: String
}

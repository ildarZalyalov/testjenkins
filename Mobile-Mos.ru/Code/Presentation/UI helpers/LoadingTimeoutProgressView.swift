//
//  LoadingTimeoutProgressView.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 13.11.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import UIKit


/// Вьюха для отображения загрузки с таймаутом, по истечении таймаута считается что запрос/действие не выполнено, стандартный лоадер UIProgressView
class LoadingTimeoutProgressView: UIProgressView {

    /// Таймер обновления прогресса
    fileprivate var progressTimer: Timer?
    
    /// Частота вызова таймера обновления прогресса (как часто обновляется прогресс)
    fileprivate let progressTimerUpdateDuration: TimeInterval = 0.05
    
    /// Шаг прогресса обновления прогресса, рассчитывается из таймаута и progressTimerUpdateDuration
    fileprivate var progressTimerUpdateStep: Float = 0
    
    /// Длительность анимации заполнения индикатора прогресса до конца при успехе раньше таймаута
    fileprivate let completeProgressDuration: TimeInterval = 0.2
    
    
    /// Шаг обновления прогресса отправки сообщения (вызывается по таймеру)
    @objc
    fileprivate func updateProgress() {
        
        let progress = self.progress + progressTimerUpdateStep
        
        self.progress = progress
        UIView.animate(withDuration: progressTimerUpdateDuration,
                       delay: 0,
                       options: [.beginFromCurrentState, .curveLinear],
                       animations: { [weak self] in
                        self?.layoutIfNeeded()
            }, completion: nil)
        
        if progress > 1 {
            progressTimer?.invalidate()
            isHidden = true
        }
    }
    
    /// Запустить прогресс
    ///
    /// - Parameter timeout: таймаут успешной отправки (по его истечении отправка будет считаться проваленной)
    func startProgressTimer(with timeout: TimeInterval) {
        
        progressTimer?.invalidate()
        
        progress = 0
        layoutIfNeeded()
        isHidden = false
        
        progressTimerUpdateStep = Float(progressTimerUpdateDuration / timeout)
        updateProgress()
        progressTimer = Timer.scheduledTimer(timeInterval: progressTimerUpdateDuration,
                                             target: self,
                                             selector: #selector(updateProgress),
                                             userInfo: nil,
                                             repeats: true)
    }
    
    /// Заполнить прогресс до конца (прерывает текущую анимацию прогресса)
    func completeProgressTimer() {
        
        guard !isHidden else { return }
        
        progressTimer?.invalidate()
        progress = 1
        
        UIView.animate(withDuration: completeProgressDuration,
                       delay: 0,
                       options: .curveLinear,
                       animations: { [weak self] in
                        self?.layoutIfNeeded()
        }) { [weak self]  _ in
            self?.isHidden = true
        }
    }
}

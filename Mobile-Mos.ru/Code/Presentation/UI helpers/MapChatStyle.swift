//
//  MapChatStyle.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 08.08.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Чат с картами
let mapConversation: Conversation = {
    
    var conversation = Conversation(title: "Поиск на карте",
                                    avatarPlaceholderIconName: "chatAvatarMaps",
                                    descriptionText: "Находите ближайшую аптеку, бассейн или магазин за пару кликов")
    
    conversation.style.isStatusBarDark = true
    conversation.style.backButtonColor = UIColor.black
    conversation.style.titleColor = UIColor.black
    conversation.style.gradientColor = UIColor.fromHex(hex: 0xFFFFFF)
    conversation.style.headerColor = UIColor.fromHex(hex: 0xFFFFFF)
    conversation.style.headerShadowColor = UIColor.fromHex(hex: 0x5C544F)
    conversation.style.outgoingMessageTextColor = UIColor.fromHex(hex: 0xFFFFFF)
    conversation.style.outgoingMessageBackgroundColor = UIColor.fromHex(hex: 0xF9BE46)
    conversation.style.controlsTextColor = UIColor.fromHex(hex: 0xF9BE46)
    conversation.style.controlsShadowColor = UIColor.fromHex(hex: 0xFFAB00)
    
    return conversation
    
}()

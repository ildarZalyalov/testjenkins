//
//  ChatTextBubbleViewStyleDefault.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 26.05.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import ChattoAdditions

class ChatTextBubbleViewStyleDefault: ChatTextBubbleViewStyleProtocol {
    
    fileprivate typealias Class = ChatTextBubbleViewStyleDefault
    fileprivate typealias BaseStyle = BaseMessageCellStyleDefault
    
    fileprivate let bubbleImages: BubbleImages
    fileprivate let bubbleBorderImages: BubbleImages
    
    lazy fileprivate var images: [ImageKey: UIImage] = {
        return [
            .template(isIncoming: true, showsTail: true) : self.bubbleImages.incomingTail(),
            .template(isIncoming: true, showsTail: false) : self.bubbleImages.incomingNoTail(),
            .template(isIncoming: false, showsTail: true) : self.bubbleImages.outgoingTail(),
            .template(isIncoming: false, showsTail: false) : self.bubbleImages.outgoingNoTail()
        ]
    }()
    
    lazy fileprivate var borderImages: [ImageKey: UIImage] = {
        return [
            .template(isIncoming: true, showsTail: true) : self.bubbleBorderImages.incomingTail(),
            .template(isIncoming: true, showsTail: false) : self.bubbleBorderImages.incomingNoTail(),
            .template(isIncoming: false, showsTail: true) : self.bubbleBorderImages.outgoingTail(),
            .template(isIncoming: false, showsTail: false) : self.bubbleBorderImages.outgoingNoTail()
        ]
    }()
    
    fileprivate static let whiteColorHex: Int = UIColor.white.toHex()
    fileprivate static let transparentColorHex: Int = UIColor.clear.toHex()
    
    let baseStyle: BaseMessageCellStyleDefault
    let outgoingTextColor: UIColor
    let inlineTextColor: UIColor
    
    init(outgoingColor: UIColor? = nil,
         outgoingTextColor: UIColor? = nil,
         inlineTextColor: UIColor? = nil) {
        
        self.bubbleImages = Class.createBubbleImages()
        self.bubbleBorderImages = Class.createBubbleBorderImages()
        
        self.baseStyle = BaseStyle(outgoingColor: outgoingColor)
        self.outgoingTextColor = outgoingTextColor ?? Class.defaultOutgoingTextColor
        self.inlineTextColor = inlineTextColor ?? Class.defaultInlineTextColor
    }

    //MARK: - BaseBubbleViewStyleProtocol
    
    func bubbleImage(viewModel: BaseMessageViewModelProtocol, isSelected: Bool) -> UIImage? {
        
        guard let model = viewModel as? TextMessageViewModelProtocol else { return nil }
        
        let key = ImageKey.normal(isIncoming: model.isIncoming,
                                  status: model.status,
                                  showsTail: model.decorationAttributes.isShowingTail,
                                  isSelected: isSelected,
                                  customBubbleColor: viewModel.customBubbleColor)
        
        if let image = images[key] {
            return image
        }
        else {
            
            let templateKey = ImageKey.template(isIncoming: model.isIncoming, showsTail: model.decorationAttributes.isShowingTail)
            
            if let image = images[templateKey] {
                
                let image = createImage(templateImage: image,
                                        isIncoming: model.isIncoming,
                                        status: model.status,
                                        isSelected: isSelected,
                                        customBubbleColor: viewModel.customBubbleColor)
                images[key] = image
                
                return image
            }
        }
        
        return nil
    }

    func bubbleImageBorder(viewModel: BaseMessageViewModelProtocol, isSelected: Bool) -> UIImage? {
        
        guard shouldUseBubbleImageBorder(for: viewModel, isSelected: isSelected) else { return nil }
        guard let model = viewModel as? TextMessageViewModelProtocol else { return nil }
        
        let key = ImageKey.template(isIncoming: model.isIncoming, showsTail: model.decorationAttributes.isShowingTail)
        return borderImages[key]
    }
        
    func inlineButtonTextFont(viewModel: InlineKeyboardButtonViewModel, isSelected: Bool) -> UIFont {
        return Class.defaultTextFont
    }
    
    func inlineButtonTextColor(viewModel: InlineKeyboardButtonViewModel, isSelected: Bool) -> UIColor {
        return viewModel.isEnabled ? inlineTextColor : Class.defaultDisabledInlineTextColor
    }
    
    func inlineButtonBackgroundColor(viewModel: InlineKeyboardButtonViewModel, isSelected: Bool) -> UIColor {
        return viewModel.isEnabled ? Class.defaultInlineBackgroundColor : Class.defaultDisabledInlineBackgroundColor
    }
    
    func inlineButtonRowSize(viewModel: BaseMessageViewModelProtocol, isSelected: Bool) -> CGSize {
        return Class.defaultInlineButtonRowSize
    }
    
    func inlineButtonSeparatorColor(viewModel: BaseMessageViewModelProtocol, isSelected: Bool) -> UIColor {
        
        guard !shouldUseBubbleImageBorder(for: viewModel, isSelected: isSelected) else { return Class.defaultInlineSeparatorColor }
        guard let model = viewModel as? TextMessageViewModelProtocol else { return Class.defaultInlineSeparatorColor }
        
        return model.isIncoming ? baseStyle.baseColorIncoming : baseStyle.baseColorOutgoing
    }
    
    func optionButtonIconColor(viewModel: BaseMessageViewModelProtocol, isSelected: Bool) -> UIColor {
        return Class.defaultOptionButtonIconColor
    }
    
    func optionButtonOpenWeblinkIconColor(viewModel: BaseMessageViewModelProtocol, isSelected: Bool) -> UIColor {
        return baseStyle.baseColorOutgoing
    }
    
    func optionButtonIconHighlightedColor(viewModel: BaseMessageViewModelProtocol, isSelected: Bool) -> UIColor {
        return inlineTextColor
    }
    
    func contentInsets(viewModel: BaseMessageViewModelProtocol, isSelected: Bool) -> BubbleViewContentInsets {
        
        var insets = Class.defaultContentInsets
        
        if let model = viewModel as? TextMessageViewModelProtocol {
            if model.isIncoming {
                insets = (insets.right, insets.left)
            }
        }
        
        return insets
    }
    
    func contentCornerRadius(viewModel: BaseMessageViewModelProtocol, isSelected: Bool) -> CGFloat {
        return Class.defaultContentCornerRadius
    }
    
    //MARK: - ChatTextBubbleViewStyleProtocol
    
    func textFont(viewModel: ChatTextMessageViewModelProtocol, isSelected: Bool) -> UIFont {
        return Class.defaultTextFont
    }
    
    func mediumTextFont(viewModel: ChatTextMessageViewModelProtocol, isSelected: Bool) -> UIFont? {
        return Class.defaultMediumTextFont
    }
    
    func boldTextFont(viewModel: ChatTextMessageViewModelProtocol, isSelected: Bool) -> UIFont? {
        return Class.defaultBoldTextFont
    }
    
    func textParagraphStyle(viewModel: ChatTextMessageViewModelProtocol, isSelected: Bool) -> NSParagraphStyle? {
        return Class.defaultTextParagraphStyle
    }
    
    func textColor(viewModel: ChatTextMessageViewModelProtocol, isSelected: Bool) -> UIColor {
        return viewModel.isIncoming ? Class.defaultIncomingTextColor : outgoingTextColor
    }
    
    func textInsets(viewModel: ChatTextMessageViewModelProtocol, isSelected: Bool) -> UIEdgeInsets {
        return Class.defaultMessageTextInsets
    }
    
    //MARK: - Создание картинок для бабблов в зависимости от view model 
    
    fileprivate struct BubbleImages {
        
        let incomingTail: () -> UIImage
        let incomingNoTail: () -> UIImage
        let outgoingTail: () -> UIImage
        let outgoingNoTail: () -> UIImage
        
        public init(
            incomingTail: @autoclosure @escaping () -> UIImage,
            incomingNoTail: @autoclosure @escaping () -> UIImage,
            outgoingTail: @autoclosure @escaping () -> UIImage,
            outgoingNoTail: @autoclosure @escaping () -> UIImage) {
            
            self.incomingTail = incomingTail
            self.incomingNoTail = incomingNoTail
            self.outgoingTail = outgoingTail
            self.outgoingNoTail = outgoingNoTail
        }
    }
    
    fileprivate enum ImageKey: Hashable {
        
        case template(isIncoming: Bool, showsTail: Bool)
        case normal(isIncoming: Bool, status: MessageViewModelStatus, showsTail: Bool, isSelected: Bool, customBubbleColor: UIColor?)
        
        var hashValue: Int {
            switch self {
            case let .template(isIncoming: isIncoming, showsTail: showsTail):
                return [1 /*template*/, isIncoming.hashValue, showsTail.hashValue].combinedHashValue
            case let .normal(isIncoming: isIncoming, status: status, showsTail: showsTail, isSelected: isSelected, customBubbleColor: customBubbleColor):
                let customColorHashValue = customBubbleColor?.hashValue ?? 0
                return [2 /*normal*/, isIncoming.hashValue, status.hashValue, showsTail.hashValue, isSelected.hashValue, customColorHashValue].combinedHashValue
            }
        }
        
        static func == (lhs: ImageKey, rhs: ImageKey) -> Bool {
            switch (lhs, rhs) {
            case let (.template(lhsValues), .template(rhsValues)):
                return lhsValues == rhsValues
            case let (.normal(lhsValues), .normal(rhsValues)):
                return lhsValues.isIncoming == rhsValues.isIncoming &&
                       lhsValues.status == rhsValues.status &&
                       lhsValues.showsTail == rhsValues.showsTail &&
                       lhsValues.isSelected == rhsValues.isSelected &&
                       lhsValues.customBubbleColor == rhsValues.customBubbleColor
            default:
                return false
            }
        }
    }
    
    fileprivate func createImage(templateImage image: UIImage,
                                 isIncoming: Bool,
                                 status: MessageViewModelStatus,
                                 isSelected: Bool,
                                 customBubbleColor: UIColor?) -> UIImage {
        
        var color: UIColor
        
        if let customColor = customBubbleColor {
            color = customColor
        }
        else {
            color = isIncoming ? baseStyle.baseColorIncoming : baseStyle.baseColorOutgoing
        }
        
        switch status {
        case .sending:
            guard color != UIColor.clear else { break }
            color = color.bma_blendWithColor(UIColor.white.withAlphaComponent(0.70))
        default:
            break
        }
        
        if isSelected && color != UIColor.clear {
            color = color.bma_blendWithColor(UIColor.black.withAlphaComponent(0.10))
        }
        
        return image.bma_tintWithColor(color)
    }
    
    //MARK: - Приватные методы
    
    /// Надо ли использовать обводку для баббла
    ///
    /// - Parameters:
    ///   - viewModel: viewModel сообщения
    ///   - isSelected: выбрано ли сообщение
    /// - Returns: надо ли использовать обводку
    fileprivate func shouldUseBubbleImageBorder(for viewModel: BaseMessageViewModelProtocol, isSelected: Bool) -> Bool {
        
        guard let customBubbleColor = viewModel.customBubbleColor else { return false }
        
        let colorHex = customBubbleColor.toHex()
        guard colorHex == Class.whiteColorHex || colorHex == Class.transparentColorHex else { return false }
        
        guard viewModel is TextMessageViewModelProtocol else { return false }
        
        return true
    }
    
    //MARK: - Задаем значения по умолчанию
    
    fileprivate static func createBubbleImages() -> BubbleImages {
        return BubbleImages(
            incomingTail: #imageLiteral(resourceName: "bubbleIncomingTail").resizableImage(withCapInsets: UIEdgeInsets(top: 19, left: 30, bottom: 19, right: 19)),
            incomingNoTail: #imageLiteral(resourceName: "bubbleIncoming").resizableImage(withCapInsets: UIEdgeInsets(top: 19, left: 30, bottom: 19, right: 19)),
            outgoingTail: #imageLiteral(resourceName: "bubbleOutgoingTail").resizableImage(withCapInsets: UIEdgeInsets(top: 19, left: 19, bottom: 19, right: 30)),
            outgoingNoTail: #imageLiteral(resourceName: "bubbleOutgoing").resizableImage(withCapInsets: UIEdgeInsets(top: 19, left: 19, bottom: 19, right: 30))
        )
    }
    
    fileprivate static func createBubbleBorderImages() -> BubbleImages {
        return BubbleImages(
            incomingTail: #imageLiteral(resourceName: "bubbleBorderIncomingTail").resizableImage(withCapInsets: UIEdgeInsets(top: 19, left: 30, bottom: 19, right: 19)),
            incomingNoTail: #imageLiteral(resourceName: "bubbleBorderIncoming").resizableImage(withCapInsets: UIEdgeInsets(top: 19, left: 30, bottom: 19, right: 19)),
            outgoingTail: #imageLiteral(resourceName: "bubbleBorderOutgoingTail").resizableImage(withCapInsets: UIEdgeInsets(top: 19, left: 19, bottom: 19, right: 30)),
            outgoingNoTail: #imageLiteral(resourceName: "bubbleBorderOutgoing").resizableImage(withCapInsets: UIEdgeInsets(top: 19, left: 19, bottom: 19, right: 30))
        )
    }
    
    fileprivate static var defaultInlineTextColor = UIColor.fromHex(hex: 0x22AFF2)
    fileprivate static var defaultDisabledInlineTextColor = UIColor.fromRGB(red: 186, green: 186, blue: 189)
    
    fileprivate static var defaultInlineBackgroundColor = UIColor.white
    fileprivate static var defaultDisabledInlineBackgroundColor = UIColor.white
    
    fileprivate static var defaultInlineButtonRowSize = CGSize(width: 232, height: 42)
    
    fileprivate static var defaultInlineSeparatorColor = UIColor.fromRGB(red: 218, green: 218, blue: 220)
    
    fileprivate static var defaultOptionButtonIconColor = UIColor.black
    
    fileprivate static var defaultContentInsets: BubbleViewContentInsets = (0, 11)
    
    fileprivate static var defaultContentCornerRadius: CGFloat = 17.5
    
    fileprivate static var fallbackTextFont = UIFont.systemFont(ofSize: UIFont.systemFontSize, weight: UIFont.Weight.regular)
    fileprivate static var defaultTextFont = UIFont(customName: .graphikLCGRegular, size: 15) ?? fallbackTextFont
    
    fileprivate static var fallbackMediumTextFont = UIFont.systemFont(ofSize: UIFont.systemFontSize, weight: UIFont.Weight.medium)
    fileprivate static var defaultMediumTextFont = UIFont(customName: .graphikLCGMedium, size: 15) ?? fallbackMediumTextFont
    
    fileprivate static var fallbackBoldTextFont = UIFont.systemFont(ofSize: UIFont.systemFontSize, weight: UIFont.Weight.semibold)
    fileprivate static var defaultBoldTextFont = UIFont(customName: .graphikLCGSemibold, size: 15) ?? fallbackBoldTextFont
    
    fileprivate static var defaultTextLineSpacing: CGFloat = 9
    fileprivate static var defaultTextParagraphStyle: NSParagraphStyle = {
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .left
        paragraphStyle.lineSpacing = defaultTextLineSpacing
        
        return paragraphStyle
    }()
    
    fileprivate static var defaultIncomingTextColor = UIColor.fromHex(hex: 0x37373C)
    fileprivate static var defaultOutgoingTextColor = UIColor.white
    
    fileprivate static var defaultMessageTextTopBottomInset: CGFloat = 4.5
    fileprivate static var defaultMessageTextInsets = UIEdgeInsets(top: 8 + defaultMessageTextTopBottomInset,
                                                                   left: 12,
                                                                   bottom: 8 + defaultMessageTextTopBottomInset,
                                                                   right: 12)
}

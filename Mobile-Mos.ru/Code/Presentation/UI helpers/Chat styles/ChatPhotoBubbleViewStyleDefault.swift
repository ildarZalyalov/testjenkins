//
//  ChatPhotoBubbleViewStyleDefault.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 01.02.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation
import ChattoAdditions

class ChatPhotoBubbleViewStyleDefault: ChatTextBubbleViewStyleDefault, ChatPhotoBubbleViewStyleProtocol {
    
    fileprivate typealias Class = ChatPhotoBubbleViewStyleDefault
    
    //MARK: - ChatPhotoBubbleViewStyleProtocol
    
    func sizeForPhoto(viewModel: ChatPhotoMessageViewModelProtocol, isSelected: Bool) -> CGSize {
        return viewModel.isPhotoVertical ? Class.defaultVerticalPhotoSize : Class.defaultPhotoSize
    }
    
    //MARK: - Задаем значения по умолчанию
    
    fileprivate static var defaultPhotoSize: CGSize = CGSize(width: 234, height: 127)
    fileprivate static var defaultVerticalPhotoSize: CGSize = CGSize(width: 125, height: 234)
}

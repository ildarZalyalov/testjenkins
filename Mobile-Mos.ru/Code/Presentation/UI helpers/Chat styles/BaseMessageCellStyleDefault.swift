//
//  BaseMessageCellStyleDefault.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 28.07.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import ChattoAdditions

class BaseMessageCellStyleDefault: BaseMessageCollectionViewCellDefaultStyle {
    
    fileprivate typealias Class = BaseMessageCellStyleDefault
    
    init(outgoingColor: UIColor? = nil) {

        let outgoingColorValue = outgoingColor ?? Class.defaultOutgoingColor
        
        super.init(colors: Class.createBaseColors(with: outgoingColorValue),
                   bubbleBorderImages: nil,
                   failedIconImages: Class.createFailedImages(),
                   layoutConstants: Class.createLayoutConstants(),
                   dateTextStyle: Class.createDateTextStyle(),
                   avatarStyle: Class.createAvatarStyle(),
                   selectionIndicatorStyle: Class.createSelectionIndicatorStyle(outgoingColor: outgoingColorValue))
    }
    
    //MARK: - Задаем значения по умолчанию
    
    fileprivate static var defaultOutgoingColor = UIColor.fromHex(hex: 0x22AFF2)
    
    fileprivate static var fallbackTextFont = UIFont.systemFont(ofSize: UIFont.systemFontSize, weight: UIFont.Weight.medium)
    fileprivate static var dateTextFont = UIFont(customName: .graphikLCGRegular, size: 11) ?? fallbackTextFont
    
    fileprivate static var defaultMaxBubbleViewWidthPercentage: CGFloat {
        
        let deviceScreenType = UIDevice.current.deviceScreenType
        
        switch deviceScreenType {
        case .iPhone6Plus:
            return 0.79
        case .iPhone6, .iPhoneX:
            return 0.77
        default:
            return 0.73
        }
    }
    
    fileprivate static func createBaseColors(with outgoingColor: UIColor) -> Colors {
        return Colors(incoming: UIColor.fromRGB(red: 242, green: 242, blue: 242), outgoing: outgoingColor)
    }
    
    fileprivate static func createFailedImages() -> FailedIconImages {
        return FailedIconImages(normal: #imageLiteral(resourceName: "messageSendingFailedIcon"), highlighted: #imageLiteral(resourceName: "messageSendingFailedIconPressed"))
    }
    
    fileprivate static func createLayoutConstants() -> BaseMessageCollectionViewCellLayoutConstants {
        return BaseMessageCollectionViewCellLayoutConstants(horizontalMargin: 5,
                                                            horizontalInterspacing: 0,
                                                            horizontalTimestampMargin: 16,
                                                            maxContainerWidthPercentageForBubbleView: defaultMaxBubbleViewWidthPercentage)
    }
    
    fileprivate static func createDateTextStyle() -> DateTextStyle {
        return DateTextStyle(font: dateTextFont, color: UIColor.fromRGB(red: 171, green: 173, blue: 187))
    }
    
    fileprivate static func createAvatarStyle() -> AvatarStyle {
        return AvatarStyle(size: .zero, alignment: .bottom)
    }
    
    fileprivate static func createSelectionIndicatorStyle(outgoingColor: UIColor) -> SelectionIndicatorStyle {
        return SelectionIndicatorStyle(
            margins: UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10),
            selectedIcon: #imageLiteral(resourceName: "сheckedButton").bma_tintWithColor(outgoingColor),
            deselectedIcon: #imageLiteral(resourceName: "uncheckButton")
        )
    }
}

//
//  ChatMapBubbleViewStyleDefault.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 15.08.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class ChatMapBubbleViewStyleDefault: ChatTextBubbleViewStyleDefault, ChatMapBubbleViewStyleProtocol {
    
    fileprivate typealias Class = ChatMapBubbleViewStyleDefault
    
    //MARK: - ChatMapBubbleViewStyleProtocol
    
    func mapPreviewSize(viewModel: ChatMapMessageViewModelProtocol, isSelected: Bool) -> CGSize {
        
        let hasTitle = !(viewModel.title?.isEmpty ?? true)
        let hasDescription = !(viewModel.description?.isEmpty ?? true)
        let atLeastOneLabelHasText = hasTitle || hasDescription
        
        return atLeastOneLabelHasText ? Class.defaultTitledMapPreviewSize : Class.defaultMapPreviewSize
    }
    
    func locationNameFont(viewModel: ChatMapMessageViewModelProtocol, isSelected: Bool) -> UIFont {
        return Class.defaultLocationNameFont
    }
    
    func locationNameParagraphStyle(viewModel: ChatMapMessageViewModelProtocol, isSelected: Bool) -> NSParagraphStyle? {
        return Class.defaultLocationNameParagraphStyle
    }
    
    func locationNameTextColor(viewModel: ChatTextMessageViewModelProtocol, isSelected: Bool) -> UIColor {
        return Class.defaultLocationNameColor
    }
    
    func locationDescriptionFont(viewModel: ChatMapMessageViewModelProtocol, isSelected: Bool) -> UIFont {
        return Class.defaultLocationDescriptionFont
    }
    
    func locationDescriptionParagraphStyle(viewModel: ChatMapMessageViewModelProtocol, isSelected: Bool) -> NSParagraphStyle? {
        return Class.defaultLocationDescriptionParagraphStyle
    }
    
    func locationDescriptionTextColor(viewModel: ChatTextMessageViewModelProtocol, isSelected: Bool) -> UIColor {
        return Class.defaultLocationDescriptionColor
    }
    
    //MARK: - Задаем значения по умолчанию
    
    fileprivate static var defaultMapPreviewSize = CGSize(width: 232, height: 139)
    fileprivate static var defaultTitledMapPreviewSize = CGSize(width: 232, height: 89)
    
    fileprivate static var fallbackLocationNameFont = UIFont.systemFont(ofSize: UIFont.systemFontSize, weight: UIFont.Weight.regular)
    fileprivate static var defaultLocationNameFont = UIFont(customName: .graphikLCGMedium, size: 14) ?? fallbackLocationNameFont
    
    fileprivate static var defaultLocationNameLineSpacing: CGFloat = 4
    fileprivate static var defaultLocationNameParagraphStyle: NSParagraphStyle = {
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .left
        paragraphStyle.lineSpacing = defaultLocationNameLineSpacing
        
        return paragraphStyle
    }()
    
    fileprivate static var defaultLocationNameColor = UIColor.fromRGB(red: 73, green: 73, blue: 80)
    
    fileprivate static var fallbackLocationDescriptionFont = UIFont.systemFont(ofSize: UIFont.systemFontSize, weight: UIFont.Weight.regular)
    fileprivate static var defaultLocationDescriptionFont = UIFont(customName: .graphikLCGRegular, size: 14) ?? fallbackLocationDescriptionFont
    
    fileprivate static var defaultLocationDescriptionLineSpacing: CGFloat = 6
    fileprivate static var defaultLocationDescriptionParagraphStyle: NSParagraphStyle = {
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .left
        paragraphStyle.lineSpacing = defaultLocationDescriptionLineSpacing
        
        return paragraphStyle
    }()
    
    fileprivate static var defaultLocationDescriptionColor = UIColor.fromRGB(red: 73, green: 73, blue: 80)
}

//
//  OnboardingTextBubbleViewStyleDefault.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 12.04.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

class OnboardingTextBubbleViewStyleDefault: ChatTextBubbleViewStyleDefault {
    
    fileprivate typealias Class = OnboardingTextBubbleViewStyleDefault
    
    //MARK: - BaseBubbleViewStyleProtocol
    
    override func inlineButtonTextFont(viewModel: InlineKeyboardButtonViewModel, isSelected: Bool) -> UIFont {
        return Class.defaultOnboardingFont
    }
    
    //MARK: - ChatTextBubbleViewStyleProtocol
    
    override func textFont(viewModel: ChatTextMessageViewModelProtocol, isSelected: Bool) -> UIFont {
        return Class.defaultOnboardingFont
    }
    
    override func mediumTextFont(viewModel: ChatTextMessageViewModelProtocol, isSelected: Bool) -> UIFont? {
        return Class.defaultMediumOnboardingFont
    }
    
    override func boldTextFont(viewModel: ChatTextMessageViewModelProtocol, isSelected: Bool) -> UIFont? {
        return Class.defaultBoldOnboardingFont
    }
    
    //MARK: - Задаем значения по умолчанию
    
    fileprivate static var fallbackOnboardingFont = UIFont.systemFont(ofSize: UIFont.systemFontSize, weight: UIFont.Weight.regular)
    fileprivate static var defaultOnboardingFont = UIFont(customName: .graphikLCGRegular, size: 16) ?? fallbackOnboardingFont
    
    fileprivate static var fallbackMediumOnboardingFont = UIFont.systemFont(ofSize: UIFont.systemFontSize, weight: UIFont.Weight.medium)
    fileprivate static var defaultMediumOnboardingFont = UIFont(customName: .graphikLCGMedium, size: 16) ?? fallbackMediumOnboardingFont
    
    fileprivate static var fallbackBoldOnboardingFont = UIFont.systemFont(ofSize: UIFont.systemFontSize, weight: UIFont.Weight.semibold)
    fileprivate static var defaultBoldOnboardingFont = UIFont(customName: .graphikLCGSemibold, size: 16) ?? fallbackBoldOnboardingFont
}

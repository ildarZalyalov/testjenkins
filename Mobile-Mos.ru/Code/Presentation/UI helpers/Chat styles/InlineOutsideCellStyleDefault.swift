//
//  InlineOutsideCellStyleDefault.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 18.07.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Стиль для ячейки с внешней инлайн клавиатурой по умолчанию
class InlineOutsideCellStyleDefault: InlineOutsideCellStyleProtocol {
    
    fileprivate typealias Class = InlineOutsideCellStyleDefault
    
    fileprivate var inlineTextColor: UIColor
    
    init(inlineTextColor: UIColor? = nil) {
        self.inlineTextColor = inlineTextColor ?? Class.defaultInlineTextColor
    }
    
    //MARK: - InlineOutsideCellStyleProtocol
    
    func inlineButtonTextFont(viewModel: InlineKeyboardButtonViewModel, isSelected: Bool) -> UIFont {
        return Class.defaultTextFont
    }
    
    /// Цвет заголовков инлайн кнопок
    func inlineButtonTextColor(viewModel: InlineKeyboardButtonViewModel, isSelected: Bool) -> UIColor {
        return viewModel.isEnabled ? inlineTextColor : Class.defaultDisabledInlineTextColor
    }
    
    /// Цвет заливки инлайн кнопок
    func inlineButtonBackgroundColor(viewModel: InlineKeyboardButtonViewModel, isSelected: Bool) -> UIColor {
        return viewModel.isEnabled ? Class.defaultInlineBackgroundColor : Class.defaultDisabledInlineBackgroundColor
    }
    
    /// Размер ряда инлайн кнопок.
    func inlineButtonRowSize(viewModel: InlineOutsideViewModel, isSelected: Bool) -> CGSize {
        return Class.defaultInlineButtonRowSize
    }
    
    /// Максимальное отображаемое кол-во рядов инлайн кнопок до их разворачивания
    /// (если будет превышено, то отобразится кнопка разворачивания)
    func inlineButtonMaxCollapsedRowCount(viewModel: InlineOutsideViewModel, isSelected: Bool) -> Int {
        return Class.defaultInlineButtonMaxCollapsedRowCount
    }
    
    /// Цвет сепараторов инлайн кнопок
    func inlineButtonSeparatorColor(viewModel: InlineOutsideViewModel, isSelected: Bool) -> UIColor {
        return Class.defaultInlineSeparatorColor
    }
    
    /// Инсеты справа и слева инлайн кнопок
    func inlineButtonInsets(viewModel: InlineOutsideViewModel, isSelected: Bool) -> InlineKeyboardButtonInsets {
        
        var insets = Class.defaultInlineButtonsInsets
        
        if viewModel.isIncomingMessage {
            insets = (insets.right, insets.left)
        }
        
        return insets
    }
    
    func inlineButtonCornerRadius(viewModel: InlineOutsideViewModel, isSelected: Bool) -> CGFloat {
        return Class.defaultInlineButtonsCornerRadius
    }
    
    //MARK: - Задаем значения по умолчанию
    
    fileprivate static var defaultInlineTextColor = UIColor.fromHex(hex: 0x22AFF2)
    fileprivate static var defaultDisabledInlineTextColor = UIColor.fromRGB(red: 186, green: 186, blue: 189)
    fileprivate static var defaultInlineBackgroundColor = UIColor.white
    fileprivate static var defaultDisabledInlineBackgroundColor = UIColor.white
    fileprivate static var defaultInlineSeparatorColor = UIColor.fromRGB(red: 242, green: 242, blue: 242)
    
    fileprivate static var defaultInlineButtonRowSize = CGSize(width: 232, height: 42)
    fileprivate static var defaultInlineButtonMaxCollapsedRowCount = 3
    fileprivate static var defaultInlineButtonsCornerRadius: CGFloat = 16
    fileprivate static var defaultInlineButtonsInsets: InlineKeyboardButtonInsets = (0, 16)
    
    fileprivate static var fallbackTextFont = UIFont.systemFont(ofSize: UIFont.systemFontSize, weight: UIFont.Weight.medium)
    fileprivate static var defaultTextFont = UIFont(customName: .graphikLCGRegular, size: 15) ?? fallbackTextFont
}

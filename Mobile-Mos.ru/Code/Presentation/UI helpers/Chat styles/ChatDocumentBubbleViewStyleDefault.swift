//
//  ChatDocumentBubbleViewStyleDefault.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 07.11.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import ChattoAdditions

class ChatDocumentBubbleViewStyleDefault: ChatTextBubbleViewStyleDefault, ChatDocumentBubbleViewStyleProtocol {
    
    fileprivate typealias Class = ChatDocumentBubbleViewStyleDefault
    
    //MARK: - ChatDocumentBubbleViewStyleProtocol
    
    func iconTintColor(viewModel: ChatTextMessageViewModelProtocol, isSelected: Bool) -> UIColor {
        return viewModel.isIncoming ? textColor(viewModel: viewModel, isSelected: isSelected) : baseStyle.baseColorOutgoing
    }
    
    func fileNameFont(viewModel: ChatDocumentMessageViewModelProtocol, isSelected: Bool) -> UIFont {
        return Class.defaultFileNameFont
    }
    
    func fileSizeFont(viewModel: ChatDocumentMessageViewModelProtocol, isSelected: Bool) -> UIFont? {
        return Class.defaultFileSizeFont
    }
    
    //MARK: - Задаем значения по умолчанию
    
    fileprivate static var fallbackFileNameFont = UIFont.systemFont(ofSize: UIFont.systemFontSize, weight: UIFont.Weight.regular)
    fileprivate static var defaultFileNameFont = UIFont(customName: .graphikLCGMedium, size: 15) ?? fallbackFileNameFont
    
    fileprivate static var fallbackFileSizeFont = UIFont.systemFont(ofSize: UIFont.systemFontSize, weight: UIFont.Weight.regular)
    fileprivate static var defaultFileSizeFont = UIFont(customName: .graphikLCGRegular, size: 15) ?? fallbackFileSizeFont
}

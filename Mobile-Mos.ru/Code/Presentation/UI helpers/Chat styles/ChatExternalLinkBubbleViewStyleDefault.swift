//
//  ChatExternalLinkBubbleViewStyleDefault.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 14.09.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import ChattoAdditions

class ChatExternalLinkBubbleViewStyleDefault: ChatTextBubbleViewStyleDefault, ChatExternalLinkBubbleViewStyleProtocol {
    
    fileprivate typealias Class = ChatExternalLinkBubbleViewStyleDefault
    
    //MARK: - ChatMapBubbleViewStyleProtocol
    
    func bubbleWidth(viewModel: ChatExternalLinkMessageViewModelProtocol, isSelected: Bool) -> CGFloat {
        return Class.defaultBubbleWidth
    }
    
    func photoHeight(viewModel: ChatExternalLinkMessageViewModelProtocol, isSelected: Bool) -> CGFloat {
        return Class.defaultPhotoHeight
    }
    
    //MARK: - Задаем значения по умолчанию
    
    fileprivate static var defaultBubbleWidth: CGFloat {
        
        let deviceScreenType = UIDevice.current.deviceScreenType
        
        switch deviceScreenType {
        case .iPhone6Plus:
            return 328
        case .iPhone6, .iPhoneX:
            return 289
        default:
            return 234
        }
    }
    
    fileprivate static var defaultPhotoHeight: CGFloat = 122
}

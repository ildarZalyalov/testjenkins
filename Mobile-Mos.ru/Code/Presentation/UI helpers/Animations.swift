//
//  Animations.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 08.08.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import UIKit

/// Расширение позволяет запустить на любой view "анимацию загрузчика" - заставить ее то пропадать, то появляться
extension UIView {
    
    //MARK: - Loader animation
    
    /// Длительность появления view при "анимации загрузчика"
    var loadingAnimationAppearDuration: TimeInterval {
        return 0.75
    }
    
    /// Длительность пропадания view при "анимации загрузчика"
    var loadingAnimationDisappearDuration: TimeInterval {
        return 0.75
    }
    
    /// Запуск "анимации загрузчика"
    func startLoadingAnimation() {
        
        alpha = 0
        
        UIView.animate(withDuration: loadingAnimationAppearDuration, delay: 0, options: .curveEaseIn, animations: { [weak self] in
            
            self?.alpha = 1
            
        }) { [weak self] isFinished in
            
            guard isFinished else { return }
            guard let duration = self?.loadingAnimationDisappearDuration else { return }
            
            UIView.animate(withDuration: duration, delay: 0, options: .curveLinear, animations: { [weak self] in
                
                self?.alpha = 0
                
                }, completion: { [weak self] isFinished in
                    guard isFinished else { return }
                    self?.startLoadingAnimation()
            })
        }
    }
    
    /// Остановка "анимации загрузчика"
    func stopLoadingAnimation() {
        
        CATransaction.begin()
        layer.removeAllAnimations()
        CATransaction.commit()
        CATransaction.flush()
        
        alpha = 1
    }
    
    //MARK: - Bounce animation
    
    var bounceAnimationScaleBeforeStart: CGAffineTransform {
        return CGAffineTransform(scaleX: 0.5, y: 0.5)
    }
    
    var bounceAnimationDuration: TimeInterval {
        return 0.5
    }
    
    var bounceAnimationSpringDamping: CGFloat {
        return 0.3
    }
    
    var bounceAnimationSpringVelocity: CGFloat {
        return 0.1
    }
    
    func performBounceAnimation() {
        
        transform = bounceAnimationScaleBeforeStart
        
        UIView.animate(withDuration: bounceAnimationDuration,
                       delay: 0,
                       usingSpringWithDamping: bounceAnimationSpringDamping,
                       initialSpringVelocity: bounceAnimationSpringVelocity,
                       options: .beginFromCurrentState,
                       animations: {
            self.transform = CGAffineTransform.identity
        })
    }
}

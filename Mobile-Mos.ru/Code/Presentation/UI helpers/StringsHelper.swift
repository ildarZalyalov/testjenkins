//
//  StringsHelper.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 01.09.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

struct StringsHelper {
    
    //MARK: - Общее
    
    /// Строка для подстановки к описанию размера данных в килобайтах
    static let kilobytesString = "Кбайт"
    
    /// Заголовок кнопки активации следующего текстового поля в форме
    static let activateNextTextFieldButtonTitle = "Далее"
    
    /// Заголовок кнопки деактивации текстового поля в форме
    static let deactivateTextFieldButtonTitle = "Готово"
    
    /// Заголовок кнопки деактивации режима редактирования таблицы
    static let tableViewEndEditingButtonTitle = "Готово"
    
    /// Текст ошибки отсутствия подключения к серверу
    static let noConnectionErrorText = "Отсутствует подключение к серверу. Попробуйте позже"
    
    /// Текст ошибки отсутствия подключения к интернету
    static let noInternetConnectionErrorText = "Отсутствует подключение к интернету"
    
    //MARK: - Превью сообщений
    
    /// Строка превью для сообщений с картой
    static let chatMessagePreviewMapString = "Местоположение"
    
    /// Строка превью для сообщений с фото
    static let chatMessagePreviewPhotoString = "Фото"
    
    /// Строка превью для сообщений со ссылкой (веб-сайт или соц сети)
    static let chatMessagePreviewExternalLinkString = "Ссылка"
    
    /// Строка превью для сообщений с файлом
    static let chatMessagePreviewDocumentString = "Файл"
    
    /// Строка превью для неотправленного сообщения
    static let conversationMessageSendingFailedPreviewText = "Сообщение не отправлено"
    
    /// Строка, отображаемая в счетчике непрочитанных сообщений, если сообщение не отправлено
    static let conversationMessageSendingFailedUnreadText = "!"
    
    /// Текст для "кадров" анимации превью события "вам печатают"
    static let conversationIsTypingPreview = "печатает"
    
    /// "Кадры" анимации превью события "вам печатают"
    static let conversationIsTypingPreviewTexts: [String] = {
        return ["\(conversationIsTypingPreview)",
            "\(conversationIsTypingPreview).",
            "\(conversationIsTypingPreview)..",
            "\(conversationIsTypingPreview)..."]
    }()
    
    //MARK: -  Главная страница
    
    /// Заголовок на странице списка диалогов
    static let conversationsPageTitle = "Cообщения"
    
    /// Заголовок секции с диалогами с новыми сообщениями на странице списка диалогов
    static let conversationsSectionNewTitle = "Новое"
    
    /// Заголовок секции с остальными диалогами на странице списка диалогов
    static let conversationsSectionRecentTitle = "Недавнее"
    
    /// Текст для "кадров" анимации отсутствия подключения к серверу
    static let networkErrorMessage = "Подключение"
    
    /// "Кадры" анимации отсутствия подключения к серверу
    static let networkErrorTexts: [String] = {
        return ["\(networkErrorMessage)",
            "⚡\(networkErrorMessage)⚡",
            "⚡⚡\(networkErrorMessage)⚡⚡",
            "⚡\(networkErrorMessage)⚡"]
    }()
    
    //MARK: -  Чат
    
    /// Текст об ошибке ввода в одно из полей контрола с несколькими полями ввода
    static let textInputsControlErrorInUnknownFieldMessage = "В одном из полей — неверные данные"
    
    /// Текст об ошибке ввода в конкретное поле контрола с несколькими полями ввода
    static let textInputsControlErrorInFieldMessageFormat = "В поле \"%@\" введены неверные данные. Проверьте и введите снова"
    
    /// Текст об ошибке ввода в поле контрола с несколькими полями ввода с введенным значением
    static let textInputsControlErrorInValueMessageFormat = "Эти данные — неверные:\"%@\". Проверьте данные и введите снова"
    
    /// Текст с подсказкой при ошибке ввода в одно из полей контрола с несколькими полями ввода
    static let textInputsControlErrorHintMessageFormat = "Подсказка: \"%@\"."
    
    /// Текст заголовка информации о точке на карте в чате
    static let chatLocationOnMapTitle = "Местоположение"
    
    /// Текст заголовка информации о точке с местоположением пользователя на карте в чате
    static let chatUserLocationOnMapTitle = "Моё местоположение"
    
    /// Текст описания в информации о точке на карте, говорящий, что загружается ее адрес
    static let chatLocationOnMapLoadingAddress = "Загружаем адрес"
    
    /// Текст описания в информации о точке на карте, говорящий, что не удалось загрузить ее адрес
    static let chatLocationOnMapFailedToLoadAddress = "Адрес не загрузился. Попробуйте снова"
    
    /// Добавочный текст для указания расстояния от точки на карте до пользователя
    static let chatLocationOnMapDistanceStringSuffix = " от вас"
    
    /// Заголовок кнопки построения маршрута на экране точки на карте для компактных экранов
    static let chatLocationOnMapCompactRouteButtonTitle = "Маршрут"
    
    /// Текст кнопки отправки местоположения в чат с экрана выбора местоположения
    static let chatSendLocationSubmitButtonTitle = "Отправить 📍"
    
    /// Заголовок кнопки командной клавиатуры, по которой расхлопываются все команды
    static let commandKeyboardMoreActionsButtonTitle = "Еще…"
    
    /// Текст заголовка в ячейке выбор даты от в календаре выбора периода времени
    static let chatDateIntervalControlDateFromTitle = "От"
    
    /// Текст заголовка в ячейке выбор даты до в календаре выбора периода времени
    static let chatDateIntervalControlDateToTitle = "До"
    
    /// Текст подсказки для поиска адресов в чате
    static let chatAddressSearchStartHelpText = "Для начала поиска введите четыре символа"
    
    /// Текст подсказки для автокомплита в чате
    static let chatAutocompleteStartHelpText = "Для начала поиска введите три символа"
    
    
    //MARK: - Оплата
    
    /// Текст ошибки, если не введен email
    static let paymentEmailEmptyMessage = "Укажите ваш Email"
    
    /// Текст ошибки, если некорректно введен email
    static let paymentEmailValidateMessage = "Не корректно введен Email"
    
    /// Текст ошибки, если не введено имя плательщика
    static let paymentFirstNameEmptyMessage = "Укажите ваше имя"
    
    /// Текст ошибки, если не введена фамилия плательщика
    static let paymentSecondNameEmptyMessage = "Укажите вашу фамилию"
    
    /// Текст ошибки, если не введено отчество плательщика
    static let paymentMiddleNameEmptyMessage = "Укажите ваше отчество"
    
    /// Префикс для строки с отображением комиссии
    static let paymentComissionPrefix = "Комиссия "
    
    /// Cтрока с отображением комиссии для нулевой комиссии
    static let paymentNoComissionString = "Без комиссии"
    
    /// Строка, которой помечается платежный инструмент, который уже не валиден
    static let paymentToolNotValidString = "Срок истёк"
    
    /// Плейсхолдер для платежного инструмента, если у него нет названия
    static let paymentToolTitlePlaceholder = "Банковская карта"
    
    /// Заголовок кнопки закрытия экрана добавления нового платежного инструмента, если это первый платежный инструмент
    static let paymentAddFirstToolCloseButtonTitle = "Отменить"
    
    /// Заголовок кнопки закрытия экрана добавления нового платежного инструмента, если уже есть сохраненные платежные инструменты
    static let paymentAddAnotherToolCloseButtonTitle = "Закрыть"
    
    /// Заголовок страницы ввода Cvv кода для сохраненного платежного инструмента
    static let paymentEnterCvvPageTitle = "Введите CVV код"
    
    /// Суффикс информациии об оплате на экране успеха оплаты
    static let paymentSuccessSubjectSuffix = ", квитанция отправлена на ваш e-mail"
    
    /// Перфикс значения комиссии на экране успеха оплаты
    static let paymentSuccessComissionPrefix = "в том числе комиссия "
    
    /// Префикс для строки с отображением банка платежного инструмента, если это социальная карта
    static let paymentToolBankSocialCardPrefix = "Соц. карта / "
    
    /// Текст ошибки, если некорректно введен номер банковской карты
    static let paymentCardNumberValidateMessage = "Не корректно введен номер банковской карты"
    
    /// Текст ошибки, если некорректно введен срок действия банковской карты
    static let paymentCardExpiryValidateMessage = "Не корректно введен срок действия банковской карты"
    
    /// Текст ошибки, если некорректно введен код безопасности банковской карты
    static let paymentCardCvvValidateMessage = "Не корректно введен код безопасности банковской карты"
    
    /// Текст ошибки, причину которой не удалось определить
    static let paymentUnknownErrorMessage = "Непредвиденная ошибка, попробуйте повторить."
    
    //MARK: - О чате
    
    /// Текст рядом с переключателем уведомлений, когда они включены
    static let aboutConversationNotificationsEnabledTitle = "Уведомления включены"
    
    /// Текст рядом с переключателем уведомлений, когда они выключены
    static let aboutConversationNotificationsDisabledTitle = "Уведомления выключены"
    
    //MARK: -  Карта
    
    /// Название секции избранного на карте
    static let favoriteSectionTitle = "Мои места"
    
    /// Название секции истории на карте
    static let historySectionTitle = "Недавние"
    
    /// Название для action на секции "Сервисы" в глобальном поиске
    static let allServicesTitle = "Все сервисы"
    
    /// Название для action на секции "На карте" в глобальном поиске
    static let onMapDatasetsTitle = "Места москвы"
    
    /// Подзаголовок на модуле карт
    static let findedSubtitle = "Найдено"
    
    static let placesSubtitle = "мест"
    
    static let placeSubtitle = "место"
    
    static let placesFewSubtitle = "места"
    
    static let overlapsPlaceholderText = "Перекрытия"
    
    // Поиск на карте
    
    /// Плейсхолдер для поиска
    static let placeHolderText = "Введите для поиска"
    
    //MARK: - Карточка объекта
    
    /// Объект открыт круглосуточно
    static let conveniencePlaceName = "круглосуточно"
    
    /// Объект закрыт
    static let closedPlaceName = "закрыто"
    
    /// У объекта выходной
    static let weekendString = "выходной"
    
    /// Текст открытого объекта
    static let objectOpenedTitle = "Открыто до "
    
    /// Текст закрытого объекта
    static let objectClosedTitle = "Закрыто до "
    
    /// Название кнопки копировать из контекстного меню
    static let menuItemCopyTitle = "Копировать"
    
    /// Название кнопки произнести из контекстного меню
    static let menuItemSpeakTitle = "Произнести"
    
    /// Название кнопки отобразить полный текст
    static let showFullTextTitle = "Показать полностью"
    
    /// Название кнопки скрыть текст
    static let hideFullTextTitle = "Скрыть"
    
    /// Название кнопки проложить маршрут для формфакторов iPhone 5 <
    static let smallFactorsRoadText = "Маршрут"
    
    /// Название дней недели
    enum WeekDayNames: String {
        case monday = "Понедельник"
        case tuesday = "Вторник"
        case wednesday = "Среда"
        case thursday = "Четверг"
        case friday = "Пятница"
        case saturday = "Суббота"
        case sunday = "Воскресенье"
    }
    
    //MARK: - Глобальный поиск
    
    /// Заголовок секции сервисов
    static let servicesSectionName = "Сервисы"
    
    /// Заголовок секции датасетов с карты
    static let datasetsSectionName = "На карте"
    
    /// Заголовок секции с диалогами и сообщениями
    static let conversationSectionName = "Диалоги"
    
    /// Заголовок секции новостей
    static let newsSectionName = "Новости"
    
    /// Заголовок секции популярных запросов
    static let popularWords = "Популярное"
    
    /// Заголовок секции недавних запросов
    static let historySectionName = "Недавнее"
    
    static let todayStringPrefix = "Сегодня "
    static let yesterdayStringPrefix = "Вчера "
    
    // Окончание времен
    
    static let minuteOne = "минута"
    
    static let minutesFew = "минут"
    
    static let minutes = "минуты"
    
    static let hours = "часов"
    
    static let hour = "час"
    
    static let hoursFew = "часа"
    
    static let daysFew = "дня"
    
    static let day = "день"
    
    static let days = "дней"
    
    /// Название датасета с избранными пользователя
    static let myPlaces = "Мои места ⭐️ "
    
    //MARK: - Авторизация
    
    static let authorizationHelpText = "Введите номер телефона и пароль от приложения «Госуслуги Москвы»"
    
    static let authorizationHelpGosuslugiAppName = "«Госуслуги Москвы»"
    
    //MARK: - Восстановление пароля пользователя
    
    static let incorrectPhoneError = "Неверный формат номера"
    
    static let changePasswordSuccess = "Пароль изменен 🎉"
    
    //MARK: - Регистрация пользователя
    
    static let repeatPasswordIncorrect = "Повтор пароля не совпадает с паролем"
    
    static let minCountOfPassword = "Пароль должен быть не менее 5-ти символов"
    
    static let minCountOfPhoneNumber = "Телефон должен быть не менее 11-ти символов"
    
    static let fillPasswordField = "Введите пароль"
    
    static let fillCodeField = "Заполните поле кода"
    
    static let fillPhoneField = "Введите номер телефона"
    
    static let registerCodeExpirationPrefix = "через "
    
    static let registerCodeExpirationSuffix = " сек."
    
    static let warningAlertTitle = "Внимание"
    
    static let fourTimesRequestedCodeText = "Вы запросили код четыре раза. После пятой попытки вы можете запросить новый код только через 24 часа"
    
    static let fiveTimesRequestedCodeText = "Вы запросили код пять раз. Новый код вы можете запросить только через 24 часа. Проверьте SMS и введите последний код"
    
    //MARK: - Обратная связь
    
    static let failedSendingSupportAboutAppErrorText = "Не удалось отправить ваше сообщение. Попробуйте еще раз."
    
    static let emailEmptyMessage = "Заполните поле Email"
    
    static let emailValidateMessage = "Не корректно введен Email"
    
    static let subjectEmptyMessage = "Тема сообщения не выбрана"
    
    static let textEmptyMessage = "Заполните текст обращения"
    
    //MARK: - Чудо техники
    
    static let ctMosRuEmptyFieldsMessage = "Заполните все поля"
    
    static let ctMosRuPhoneErrorMessage = "Телефон должен быть не менее 11-ти символов"
    
    static let ctMosRuUnknownErrorMessage = "Непредвиденная ошибка, попробуйте повторить."
    
    //MARK: - Профиль пользователя
    
    static let loginErrorMessage = "Возможно неправильный логин или пароль"
    
    static let emptyFieldMessage = "Заполните все поля"

    
    // Название полей на главном экране "Профиль пользователя"
    
    /// Заголовок в ЛК, если нет данных о пользователе
    static let userProfileNoDataTitle = "Личный кабинет"
    
    /// Заголовок последнего обновления данных
    static let dataUpToDateString     = "Данные обновлены"
    
    /// Название ячейки личных данных пользователя
    static let userPersonalInfoTitle  = "Личные данные"
    
    /// Название секции настроек пользователя
    static let userSettingsTitle      = "Настройки"
    
    /// Заголовок настроек уведомлений пользователя
    static let userNotificationsTitle = "Уведомления"
    
    /// Заголовок настроек безопасности пользователя
    static let userSecurityTitle      = "Безопасность"
    
    /// Название секции данных для сервисов пользователя
    static let userServicesInfoTitle  = "Данные для сервисов"
    
    /// Название секции "другое" пользователя
    static let userOtherTitle         = "Другое"
    
    /// Заголовок раздела о пользовательском соглашении
    static let userAgrementTitle      = "Пользовательское соглашение"
    
    /// Заголовок раздела о приложении
    static let userAboutAppTitle      = "О приложении"
    
    /// Заголовок раздела о приложениях и сервисах ДИТ
    static let ourAppsAndServicesText = "Наши приложения и сервисы"
    
    /// Заголовок раздела деавторизации
    static let userExitTitle          = "Выйти из профиля"
    
    /// Сообщение об ошибке деавторизации
    static let userExitErrorMessage   = "Не получается выйти из аккаунта. Попробуйте позже"
    
    // Текст ошибки изменения настройки уведомлений для чата
    static let changingNotificationSettingsErrorAlertText = "Не удалось поменять настройки. Попробуйте снова"
    
    // Название заголовков секций на странице сервисов ДИТ
    static let ourAppsAndServicesServicesHeader = "Сервисы"
    static let ourAppsAndServicesAppsHeader = "Приложения"
    
    // Название полей на экране "информация о профиле пользователя"
    static let userNameInfoTitle  = "ФИО"
    static let userSexInfoTitle   = "Пол"
    static let userAgeInfoTitle   = "Дата рождения"
    static let userSnilsInfoTitle = "СНИЛС"
    static let userPhoneInfoTitle = "Телефон"
    static let userEmailInfoTitle = "Email"
    
    static let userSexMaleTitle = "Мужской"
    static let userSexFemaleTitle = "Женский"
    
    // Название полей про паспорт на экране "информация о профиле пользователя"
    static let passportSeries     = "Серия паспорта"
    static let passportNumber     = "Номер паспорта"
    static let passportIssuer     = "Кем выдан паспорт"
    static let passportIssuerCode = "Код подразделения"
    static let passportBirthPlace = "Место рождения"
    
    // Информация о сервисе
    static let changeUserDataListButtonText = "Изменить"
    static let doneChangingUserDataListButtonText = "Готово"
    static let deleteItemInUserDataListButtonText = "Удалить"
    static let saveItemInUserDataButtonText = "Сохранить"
    static let closeItemInUserDataButtonText = "Закрыть"
    static let cancelAddingItemInUserDataButtonText = "Отменить"
    static let doneAddingItemInUserDataButtonText = "Готово"
    static let addItemInUserDataTitleText = "Добавить"
    static let changeItemInUserDataTitleText = "Просмотр"
    static let fieldInUserDataSeparator = ", "
    static let deleteItemInUserDataErrorAlertText = "Не получилось удалить данные. Попробуйте снова"
    static let mandatoryFieldInUserDataErrorAlertTextFormat = "Заполните обязательное поле %@."
    static let mandatoryMultipleFieldInUserDataErrorAlertTextFormat = "Заполните обязательные поля %@."
    static let errorInFieldInUserDataErrorAlertTextFormat = "Неверные данные в поле %@."
    static let errorInMultipleFieldInUserDataErrorAlertTextFormat = "Неверные данные в полях %@."
    
    // Раздел безопасноcть
    static let securityWithCodeTitle = "Защита кодом"
    static let timeWithoutCodeText   = "5 минут входа без кода"
    static let changeCodeText        = "Сменить код входа"
    static let useCodeText           = "Использовать код"
    static let touchIdTite           = "Touch ID"
    static let useTouchIdText        = "Использовать Touch ID"
    static let codeSectionFooterText = "Данный текст является тестовым на данный момент и будет изменен в будущем."
    static let touchIdSectionFooterText = "Технология touch-ID позволяет авторизовываться без ввода дополнительного пароля."
    
    // Информация о приложении
    static let appVersionPrefix       = "Версия"
    static let shareToFriendText      = "Рекомендовать другу"
    static let rateAppText            = "Оценить приложение"
    static let supportText            = "Обратная связь"
    static let appUrlText             = "Ссылка на приложение: "
    
    static let openInAppStoreButtonTitle = "Открыть в App Store"
    static let newAppVersionAvailableButtonTitle = "Новое обновление"
    
    static let appDescription = "Все самые нужные городские сервисы, консультации с оператором контакт-центра и главные городские новости всегда под рукой — в официальном приложении mos.ru «Моя Москва».\n\nЗаписывайтесь к врачу, оплачивайте счета и штрафы, проверяйте, как дети ходят в школу — с помощью простых чатов.\n\nСо сложными вопросами поможет оператор городского контакт-центра в круглосуточном чате."
}

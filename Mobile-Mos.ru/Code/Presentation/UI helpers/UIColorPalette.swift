//
//  UIColorPalette.swift
//  Moscow-Toursim
//
//  Created by Ivan Erasov on 25.01.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import UIKit

struct UIColorPalette {
    
    //MARK: - Таб бар
    
    /// Цвет выделенного элемента таб бара What to do
    static var tabBarWhatToDoTintColor: UIColor {
        return UIColor.fromRGB(red: 140, green: 29, blue: 40)
    }
    
    /// Цвет выделенного элемента таб бара Map
    static var tabBarMapTintColor: UIColor {
        return UIColor.fromRGB(red: 65, green: 117, blue: 5)
    }
    
    /// Цвет выделенного элемента таб бара Tips & Info
    static var tabBarTipsInfoTintColor: UIColor {
        return UIColor.fromRGB(red: 14, green: 81, blue: 161)
    }
    
    /// Цвет выделенного элемента таб бара My Journey
    static var tabBarMyJourneyTintColor: UIColor {
        return UIColor.fromRGB(red: 112, green: 41, blue: 184)
    }
    
    //MARK: -  SFSafariViewController
    
    /// Цвет контролов во внутреннем браузере по умолчанию
    static var browserControlsDefaultTintColor: UIColor {
        return UIColor.fromHex(hex: 0xCD2D40)
    }
    
    //MARK: -  Главная страница
    
    /// Цвет текста на баннере днем
    static var bannerTextColorForDay: UIColor {
        return UIColor.black
    }
    
    /// Цвет текста на баннере ночью
    static var bannerTextColorForNight: UIColor {
        return UIColor.white
    }
    
    /// Цвет заголовка на странице списка диалогов
    static var conversationsPageTitleColor: UIColor {
        return UIColor.fromRGB(red: 40, green: 40, blue: 47)
    }
    
    //MARK: -  Сообщения
    
    /// Цвет текста ссылки в сообщении по умолчанию
    static var chatMessageDefaultLinkColor: UIColor {
        return UIColor.fromHex(hex: 0x1C5DAE)
    }
    
    /// Цвет текста "Сообщение не отправлено" в чате под неотправленным сообщением
    static var chatMessageSendingFailedStatusColor: UIColor {
        return UIColor.fromRGB(red: 234, green: 33, blue: 28)
    }
    
    /// Цвет текста, описывающего другие статусы, в чате под сообщением
    static var chatMessageOtherStatusColor: UIColor {
        return UIColor.fromRGB(red: 242, green: 242, blue: 242)
    }
    
    /// Цвет текста разделителя с временем сообщения, который появляется при большой паузе между сообщениями
    static var chatMessageTimeSeparatorColor: UIColor {
        return UIColor.fromRGB(red: 171, green: 173, blue: 187)
    }
    
    /// Цвет текста заголовка новости в ячейке сообщения, отображающей ссылку на внешний ресурс
    static var chatMessageExternalLinkTitleColor: UIColor {
        return UIColor.fromRGB(red: 73, green: 73, blue: 80)
    }
    
    /// Цвет текста описания новости в ячейке сообщения, отображающей ссылку на внешний ресурс
    static var chatMessageExternalLinkDescriptionColor: UIColor {
        return UIColor.fromRGB(red: 73, green: 73, blue: 80)
    }
    
    /// Цвет текста источника новости в ячейке сообщения, отображающей ссылку на внешний ресурс
    static var chatMessageExternalLinkSourceColor: UIColor {
        return UIColor.fromRGB(red: 73, green: 73, blue: 80).withAlphaComponent(0.8)
    }
    
    //MARK: - Командная клавиатура
    
    static var commandKeyboardTopSeparatorColor: UIColor {
        return UIColor.fromRGB(red: 221, green: 221, blue: 221, alpha: 0.5)
    }
    
    static var commandKeyboardButtonsTitleDefaultColor: UIColor {
        return UIColor.fromHex(hex: 0x22AFF2)
    }
    
    static var commandKeyboardButtonsHighlightedTitleDefaultColor: UIColor {
        return UIColor.white
    }
    
    static var commandKeyboardButtonsShadowDefaultColor: UIColor {
        return UIColor.fromHex(hex: 0x5B82A2)
    }
    
    //MARK: - Кастомные контролы
    
    /// Цвет текста в контроле текстового ввода в чате
    static var textInputsControlTextColor: UIColor {
        return UIColor.fromRGB(red: 3, green: 3, blue: 3)
    }
    
    /// Цвет сепараторов в контроле текстового ввода в чате
    static var textInputsControlSeparatorColor: UIColor {
        return UIColor.fromRGB(red: 224, green: 223, blue: 223)
    }
    
    /// Цвет затемнения позади модальной панели с контролом
    static var modalPanelDimmingColor: UIColor {
        return UIColor.fromRGB(red: 25, green: 25, blue: 25, alpha: 0.4)
    }
    
    //MARK: - Календарь
    
    /// Цвет текста с числом в контроле календаря
    static var calendarDayTextColor: UIColor {
        return UIColor.black
    }
    
    /// Цвет текста с недоступным числом в контроле календаря
    static var calendarDisabledDayTextColor: UIColor {
        return UIColor.black.withAlphaComponent(0.2)
    }
    
    /// Цвет текста с выбранным числом в контроле календаря
    static var calendarSelectedDayTextColor: UIColor {
        return UIColor.white
    }
    
    //MARK: - Календарь с периодом времени
    
    /// Цвет фона ячейки выбора даты от/до по умолчанию
    static var dateInIntervalDefaultBackgroundColor: UIColor {
        return UIColor.white
    }
    
    /// Цвет фона ячейки выбора даты от/до в выбранном состоянии по умолчанию
    static var dateInIntervalDefaultSelectedBackgroundColor: UIColor {
        return UIColor.black
    }
    
    //MARK: - Карта
    
    static var datasetButtonsShadowDefaultColor: UIColor {
        return UIColor.fromHex(hex: 0xFF6700)
    }
    
    static var searchStringSubstringColor: UIColor {
        return UIColor.fromHex(hex: 0x78808E)
    }
    //MARK: - Оплата
    
    /// Цвет подстветки выбора параметра при оплате для перехода к его настройке
    /// (например, для способа оплаты)
    static var paymentParameterSelectionColor: UIColor {
        return UIColor.lightGray.withAlphaComponent(0.2)
    }
    
    /// Цвет фона кнопки подтверждения оплаты
    static var submitPaymentButtonBackgroundColor: UIColor {
        return UIColor.white
    }
    
    /// Цвет подзаголовка способа оплаты
    static var paymentToolSubtitleColor: UIColor {
        return UIColor.fromRGB(red: 106, green: 110, blue: 114)
    }
    
    /// Цвет подзаголовка способа оплаты, если он не валиден
    static var paymentToolNotValidSubtitleColor: UIColor {
        return UIColor.fromRGB(red: 238, green: 43, blue: 35)
    }
    
    /// Цвет bar button items на экранах оплаты
    static var paymentBarButtonColor: UIColor {
        return UIColor.fromRGB(red: 45, green: 112, blue: 224)
    }
    
    /// Цвет disabled bar button items на экранах оплаты
    static var paymentDisabledBarButtonColor: UIColor {
        return UIColor.black.withAlphaComponent(0.2)
    }
    
    /// Цвет текстового поля на форме оплаты
    static var paymentFieldRegularColor: UIColor {
        return UIColor.fromRGB(red: 158, green: 164, blue: 172 )
    }
    
    /// Цвет подсвеченного текстового поля на форме оплаты
    static var paymentFieldHighlightedColor: UIColor {
        return UIColor.fromRGB(red: 201, green: 39, blue: 35)
    }
    
    //MARK: - Профиль пользователя
    
    static var editBarButtonColor: UIColor {
        return UIColor.fromRGB(red: 45, green: 112, blue: 224)
    }
    
    /// Цвет заголовка поля ввода данных пользователя в ЛК
    static var userDataTextFieldLabelColor: UIColor {
        return UIColor.fromHex(hex: 0x838396)
    }
    
    /// Цвет сепаратора и заголовка поля при ошибке ввода данных
    static var errorInUserDataColor: UIColor {
        return UIColor.fromRGB(red: 234, green: 33, blue: 28)
    }
    
    //MARK: - Авторизация/Регистрация
    
    /// цвет текст переотправки смс
    static var resendSmsTitleColor: UIColor {
        return UIColor.fromRGB(red: 153, green: 153, blue: 153)
    }
    
    /// цвет статических лейблов/текстов на странице авторизации/регистрации
    static var staticTextColor: UIColor {
        return UIColor.fromRGB(red: 131, green: 135, blue: 150)
    }
    
    /// Цвет элементов навигейшен бара для экрана регистрации
    static var registrationNavigationBarTintColor: UIColor {
        return UIColor.black
    }
    
    /// Цвет сепаратора при выборе текст. поля
    static var selectedTextFieldSeparatorColor: UIColor {
        return UIColor.fromHex(hex: 0x2D70E0)
    }
    
    /// Цвет сепаратора текст. поля в нормальном состоянии
    static var unselectedTextFieldSeparatorColor: UIColor {
        return UIColor.fromHex(hex: 0xDDDDDD)
    }
    
    /// Цвет ссылок на форме авторизации
    static var authorizationFormLinksColor: UIColor {
        return UIColor.fromHex(hex: 0x2D70E0)
    }
    
    //MARK: - Чудо техники
    
    /// Цвет заголовка типа техники
    static var ctMosRuDeviceTypeTitleColor: UIColor {
        return UIColor.fromRGB(red: 5, green: 78, blue: 147)
    }
    
    /// Цвет заголовка типа техники в выбранном состоянии
    static var ctMosRuSelectedDeviceTypeTitleColor: UIColor {
        return UIColor.white
    }
    
    /// Цвет фона заголовка типа техники в выбранном состоянии
    static var ctMosRuSelectedDeviceTypeTitleBackgroundColor: UIColor {
        return UIColor.fromRGB(red: 6, green: 78, blue: 147)
    }
}

//
//  UIFontPallete.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 27.07.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import UIKit

extension UIFont {
    
    /// Имена кастомных шрифтов
    enum CustomFontName: String {
        
        // Graphik LCG
        
        case graphikLCGSuperItalic = "GraphikLCG-SuperItalic"
        case graphikLCGExtralight = "GraphikLCG-Extralight"
        case graphikLCGBlackItalic = "GraphikLCG-BlackItalic"
        case graphikLCGMediumItalic = "GraphikLCG-MediumItalic"
        case graphikLCGMedium = "GraphikLCG-Medium"
        case graphikLCGThin = "GraphikLCG-Thin"
        case graphikLCGThinItalic = "GraphikLCG-ThinItalic"
        case graphikLCGSuper = "GraphikLCG-Super"
        case graphikLCGBlack = "GraphikLCG-Black"
        case graphikLCGLightItalic = "GraphikLCG-LightItalic"
        case graphikLCGBoldItalic = "GraphikLCG-BoldItalic"
        case graphikLCGRegular = "GraphikLCG-Regular"
        case graphikLCGExtralightItalic = "GraphikLCG-ExtralightItalic"
        case graphikLCGRegularItalic = "GraphikLCG-RegularItalic"
        case graphikLCGLight = "GraphikLCG-Light"
        case graphikLCGBold = "GraphikLCG-Bold"
        case graphikLCGSemiboldItalic = "GraphikLCG-SemiboldItalic"
        case graphikLCGSemibold = "GraphikLCG-Semibold"
    }
    
    convenience init?(customName fontName: CustomFontName, size fontSize: CGFloat) {
        self.init(name: fontName.rawValue, size: fontSize)
    }
}

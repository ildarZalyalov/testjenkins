//
//  OnboardingInteractorInput.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 09.04.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

typealias ChatMessagesConfiguration = (messages: [ChatMessage], buttonsById: [String : OnboardingDialogMessageButton])

protocol OnboardingInteractorInput: class {
    
    func buildChatMessages(from messages: [OnboardingDialogMessage], for conversation: Conversation) -> ChatMessagesConfiguration
    
    func presentChatMessages(messages: [ChatMessage], afterUserMessageWith userMessageText: String?, in conversation: Conversation)
}

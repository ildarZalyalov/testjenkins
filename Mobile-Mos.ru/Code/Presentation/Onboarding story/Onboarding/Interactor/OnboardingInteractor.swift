//
//  OnboardingInteractor.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 09.04.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

class OnboardingInteractor: OnboardingInteractorInput {
    
    weak var output: OnboardingInteractorOutput!
    
    var messageFactory: ChatMessageFactory!
    var formattingExtractor: ChatTextFormattingExtractor!
    var userService: UserService!
    
    var isPresentingMessages = false
    
    let willPresentMessageNotificationDelay: TimeInterval = 0.15
    let presentingMessageDelay: TimeInterval = 1.75
    
    //MARK: - Методы
    
    func processMessageTextFormatting(in message: inout ChatMessage) {
        message.content.text = message.content.text.trimmingCharacters(in: CharacterSet.whitespaces)
        guard let formatting = formattingExtractor.extractFormatting(from: message.content.text) else { return }
        message.content.textFormatting = ChatMessageTextFormatting(formatting: formatting)
    }
    
    func appendPhotoContentIfNeeded(with photoUrl: URL?, to message: inout ChatMessage) {
        
        guard photoUrl != nil else { return }
        
        let photoContent = ChatMessagePhotoObject(photoUrl: photoUrl!, photoSize: nil)
        
        message.content.photoObject = photoContent
        message.content.type = .photo
    }
    
    func appendReplyMarkup(with buttons: [OnboardingDialogMessageButton], to message: inout ChatMessage) -> [String : OnboardingDialogMessageButton] {
        
        var buttonsById = [String : OnboardingDialogMessageButton]()
        
        let commandButtons: [[ChatReplyKeyboardButton]] = buttons.map {
            
            var commandButton = ChatReplyKeyboardButton()
            commandButton.itemId = $0.buttonId
            commandButton.title = $0.title
            
            buttonsById[commandButton.itemId] = $0
            
            return [commandButton]
        }
        
        let markup = ChatReplyKeyboardMarkup(buttons: commandButtons, resizeKeyboard: true)
        message.replyMarkup.commandsMarkup = markup
        
        return buttonsById
    }
    
    func schedulePresentingMessages(_ messages: [ChatMessage]) {
        
        guard !messages.isEmpty else {
            isPresentingMessages = false
            return
        }
        
        let notificationDelay = DispatchTime.now() + willPresentMessageNotificationDelay
        let delay = DispatchTime.now() + presentingMessageDelay
        
        var expandingMessages = messages
        let messageToPresent = expandingMessages.removeFirst()
        let isLastMessage = expandingMessages.isEmpty
        
        DispatchQueue.main.asyncAfter(deadline: notificationDelay) { [weak self] in
            self?.output.willPresentNewMessage()
        }
        
        DispatchQueue.main.asyncAfter(deadline: delay, execute: { [weak self] in
            
            guard let strongSelf = self else { return }
            
            strongSelf.isPresentingMessages = !isLastMessage
            strongSelf.output.didObtainNewMessages(messages: [messageToPresent])
            
            guard !isLastMessage else { return }
            
            strongSelf.schedulePresentingMessages(expandingMessages)
        })
    }
    
    //MARK: - OnboardingInteractorInput
    
    func buildChatMessages(from messages: [OnboardingDialogMessage], for conversation: Conversation) -> ChatMessagesConfiguration{
        
        var buttonsById = [String : OnboardingDialogMessageButton]()
        
        let chatMessages: [ChatMessage] = messages.map {
            
            var chatMessage = messageFactory.buildFakeIncomingMessage(from: $0.text, for: conversation, and: userService.currentUser)
            processMessageTextFormatting(in: &chatMessage)
            appendPhotoContentIfNeeded(with: $0.photoUrl, to: &chatMessage)
            
            buttonsById += appendReplyMarkup(with: $0.buttons, to: &chatMessage)
            
            return chatMessage
        }
        
        return (chatMessages, buttonsById)
    }
    
    func presentChatMessages(messages: [ChatMessage], afterUserMessageWith userMessageText: String?, in conversation: Conversation) {
        
        guard !isPresentingMessages else { return }
        isPresentingMessages = true
        
        if let messageText = userMessageText {
            
            let context = ChatMessage.MessageContextValues.text.rawValue
            var userMessage = messageFactory.buildOutgoingMessage(from: messageText,
                                                                  messageContext: context,
                                                                  for: conversation,
                                                                  and: userService.currentUser)
            userMessage.status = .success
            
            output.didObtainNewMessages(messages: [userMessage])
        }
        
        var messagesToPresent = messages
        messagesToPresent.forEachMutate { $0.date = Date() }

        schedulePresentingMessages(messagesToPresent)
    }
}

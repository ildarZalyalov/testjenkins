//
//  OnboardingInteractorOutput.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 09.04.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

protocol OnboardingInteractorOutput: class {
    
    func didObtainNewMessages(messages: [ChatMessage])
    
    func willPresentNewMessage()
}

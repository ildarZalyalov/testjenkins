//
//  OnboardingModuleConfiguration.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 11.04.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

struct OnboardingModuleConfiguration {
    
    weak var onboardingBuilder: OnboardingBuilder?
    
    weak var remoteNotificationsConfigurator: RemoteNotificationsConfigurator?
}

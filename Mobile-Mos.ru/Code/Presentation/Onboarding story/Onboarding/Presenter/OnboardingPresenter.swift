//
//  OnboardingPresenter.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 09.04.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

class OnboardingPresenter: OnboardingViewOutput, OnboardingInteractorOutput {
    
    weak var view: OnboardingViewInput!
    var router: OnboardingRouterInput!
    var interactor: OnboardingInteractorInput!
    
    var dataSourceFactory: ChatDataSourceFactory!
    var dataSourceConfiguration: ChatDataSourceConfiguration!
    
    var commandsDataSourceFactory: CommandButtonsDataSourceFactory!
    var commandsById: [String : ChatReplyKeyboardButton]!
    var commandsModelsById: [String : OnboardingDialogMessageButton]!
    
    var hapticFeedManager: HapticFeedbackManager?
    var soundsManager: SoundsManager!
    var notificationCenter: NotificationCenter!
    
    var dialogBuilder: OnboardingDialogBuilder!
    
    var configuration: OnboardingModuleConfiguration!
    
    var isModuleInitialized = false
    var askEnablingNotificationsResultHandler: ((Bool) -> (String, [Int]))?
    
    var isChatEmpty: Bool {
        return dataSourceConfiguration == nil
    }
    
    var dataSource: ChatDataSource {
        return dataSourceConfiguration.dataSource
    }
    var messageById: [String : ChatMessage] {
        return dataSourceConfiguration.selectionItems
    }
    
    let conversation: Conversation = {
       
        var conversation = Conversation()
        conversation.style = ConversationStyle(isStatusBarDark: true,
                                               backButtonColor: UIColor.fromHex(hex: 0xCD2D40),
                                               titleColor: UIColor.black,
                                               gradientColor: UIColor.fromHex(hex: 0xFFFFFF),
                                               headerColor: UIColor.fromHex(hex: 0xFFFFFF),
                                               headerShadowColor: UIColor.fromHex(hex: 0xBA8D93),
                                               outgoingMessageTextColor: UIColor.white,
                                               outgoingMessageBackgroundColor: UIColor.fromHex(hex: 0xCD2D40),
                                               controlsTextColor: UIColor.fromHex(hex: 0xCE3144),
                                               controlsShadowColor: UIColor.fromHex(hex: 0x493235))
        
        return conversation
    }()
    
    deinit {
        notificationCenter.removeObserver(self)
    }
    
    //MARK: - Приватные методы
    
    func display(messages: [ChatMessage], completion: (() -> ())?) {
        
        let updateType: ChatViewUpdateType = isChatEmpty ? .reload : .regular
        dataSourceFactory.append(newMessages: messages, to: &dataSourceConfiguration!)
        
        if view.showsTypingIndicator {
            let lastIsIncoming = messages.last?.isIncoming ?? false
            view.showsTypingIndicator = !lastIsIncoming
        }
        
        view.display(dataSource: dataSource, for: updateType, completion: completion)
    }
    
    func updateUIWithLastIncomingMessage(for messages: [ChatMessage]) {
        
        guard let message = messages.last(where: { $0.isIncoming }) else {
            return
        }
        
        let commandsMarkup = message.replyMarkup.commandsMarkup
        let shouldShowCommands = !(commandsMarkup?.buttons.isEmpty ?? true)
        var configuration: CommandButtonsDataSourceConfiguration?
        
        if shouldShowCommands, commandsMarkup != nil {
            
            configuration = commandsDataSourceFactory.buildDataSourceConfiguration(from: commandsMarkup!)
            commandsById = configuration!.selectionItems
            
            view.showCommands(commands: configuration!.dataSource, animated: true)
        }
    }
    
    func tryFindButton(with buttonId: String, in message: ChatMessage) -> ChatReplyKeyboardButton? {
        
        let tryFindButton: ([[ChatReplyKeyboardButton]]?) -> ChatReplyKeyboardButton? = { buttons in
            
            if let button = buttons?.lazy.reduce([], +).first(where: { $0.itemId == buttonId }) {
                return button
            }
            
            return nil
        }
        
        if let button = tryFindButton(message.replyMarkup.commandsMarkup?.buttons) {
            return button
        }
        
        return nil
    }
    
    func triggerChatButtonPressedFeedback() {
        hapticFeedManager?.triggerSelectionFeedback()
    }
    
    func triggerChatMessageIncomingFeedback() {
        soundsManager.playCustomSound(.messageIn)
        hapticFeedManager?.triggerImpactFeedback(with: .light)
    }
    
    func triggerNotificationFeedback() {
        soundsManager.playCustomSound(.messageInOther)
        hapticFeedManager?.triggerNotificationFeedback(with: .warning)
    }
    
    func presentDialogStep(with userMessageText: String?, and answerMessagesIds: [Int]) {
        
        let messages = dialogBuilder.obtainMessages(with: answerMessagesIds)
        let configuration = interactor.buildChatMessages(from: messages, for: conversation)
        let chatMessages = configuration.messages
        
        commandsModelsById = configuration.buttonsById
        
        interactor.presentChatMessages(messages: chatMessages, afterUserMessageWith: userMessageText, in: conversation)
    }
    
    //MARK: - OnboardingViewOutput
    
    var conversationStyle: ConversationStyle {
        return conversation.style
    }
    
    func configure(with configuration: OnboardingModuleConfiguration) {
        self.configuration = configuration
        guard let builder = configuration.onboardingBuilder else { return }
        router.configure(with: builder)
    }
    
    func setupInitialState() {
        
        notificationCenter.addObserver(self, selector: #selector(remoteNotificationsStateDidChange), name: .RemoteNotificationsStateDidChange, object: nil)
        
        dataSourceConfiguration = dataSourceFactory.buildDataSourceConfiguration(from: [])
        view.configure(with: conversationStyle)
    }
    
    func viewDidAppear() {
    
        guard !isModuleInitialized else { return }
        isModuleInitialized = true
        
        let initialMessagesIds = dialogBuilder.obtainInitialMessagesIds()
        presentDialogStep(with: nil, and: initialMessagesIds)
    }
    
    func didSelectCommand(with itemId: String) {
        
        guard let command = commandsById[itemId] else { return }
        guard let commandModel = commandsModelsById[command.itemId] else { return }
        
        triggerChatButtonPressedFeedback()
        
        switch commandModel.action {
        case .sendMessage(let answerMessagesIds):
            view.hideNotifications(animated: true)
            view.hideCommands(animated: true)
            presentDialogStep(with: command.title, and: answerMessagesIds)
        case .demonstrateNotifications:
            view.showNotifications()
        case .askEnablingNotifications(let resultHandler):
            view.hideNotifications(animated: true)
            view.hideCommands(animated: true)
            askEnablingNotificationsResultHandler = resultHandler
            configuration.remoteNotificationsConfigurator?.registerForRemoteNotifications()
        case .finishOnboarding:
            router.hideModule()
        }
    }
    
    func didShowNotification() {
        triggerNotificationFeedback()
    }
    
    //MARK: - OnboardingInteractorOutput

    func didObtainNewMessages(messages: [ChatMessage]) {
        
        guard !messages.isEmpty else {
            return
        }

        display(messages: messages, completion: { [weak self] in
            
            guard let strongSelf = self else { return }
            guard messages.first(where: { $0.isIncoming }) != nil else { return }
            
            strongSelf.triggerChatMessageIncomingFeedback()
            strongSelf.updateUIWithLastIncomingMessage(for: messages)
        })
    }
    
    func willPresentNewMessage() {
        view.showsTypingIndicator = true
        view.display(dataSource: dataSource, for: .regular, completion: nil)
    }
    
    //MARK: - Нотификации
    
    @objc
    func remoteNotificationsStateDidChange() {
        
        guard let resultHandler = askEnablingNotificationsResultHandler else { return }
        askEnablingNotificationsResultHandler = nil
        
        let success = configuration.remoteNotificationsConfigurator?.notificationsAreAuthorized ?? false
        let userTextAndMessageIds = resultHandler(success)
        
        presentDialogStep(with: userTextAndMessageIds.0, and: userTextAndMessageIds.1)
    }
}

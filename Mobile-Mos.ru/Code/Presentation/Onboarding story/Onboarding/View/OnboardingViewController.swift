//
//  OnboardingViewController.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 09.04.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import UIKit
import Chatto
import ChattoAdditions

class OnboardingViewController: BaseChatViewController, OnboardingViewInput, ConfigurableModuleController, ChatDataSourceProtocol, ChatLayoutDelegate, BaseChatMessageHandler, CommandKeyboardViewDelegate {
    
    var output: OnboardingViewOutput!
    var textFormattingExtractor: ChatTextFormattingExtractor!
    
    @IBOutlet weak var notificationsOverlayView: UIVisualEffectView!
    @IBOutlet weak var notificationsStackView: UIStackView!
    @IBOutlet weak var firstNotificationView: UIView!
    @IBOutlet weak var secondNotificationView: UIView!
    @IBOutlet weak var thirdNotificationView: UIView!
    
    @IBOutlet weak var notificationsStackViewTopConstraint: NSLayoutConstraint!
    
    var chatItems = [ChatItemProtocol]()
    var hasMoreNext: Bool = false
    var hasMorePrevious: Bool = false
    
    weak var delegate: ChatDataSourceDelegateProtocol?
    weak var itemsDecorator: OnboardingChatItemsDecorator?
    
    var showsTypingIndicator: Bool {
        get { return itemsDecorator?.shouldShowTypingIndicator ?? false }
        set {
            itemsDecorator?.shouldShowTypingIndicator = newValue
            itemsDecorator?.shouldUseTypingIndicatorBottomMargin = false
        }
    }
    
    var chatContentInsets: UIEdgeInsets {
        get { return layoutConfiguration.contentInsets }
        set { layoutConfiguration = ChatLayoutConfiguration(contentInsets: newValue, scrollIndicatorInsets: layoutConfiguration.scrollIndicatorInsets) }
    }
    
    let commandKeyboard = CommandKeyboardView(frame: CGRect.zero)
    
    var originalInputContainerBottomConstraint: NSLayoutConstraint?
    var hiddenInputContainerBottomConstraint: NSLayoutConstraint?
    
    let chatTopInset: CGFloat = 26
    
    let chatUpdatesAnimationDuration: TimeInterval = 0.45
    let refreshCommandsAnimationDuration: TimeInterval = 0.23
    let hideOrShowCommandsAnimationDuration: TimeInterval = 0.3
    
    let notificationsBackgroundAlpha: CGFloat = 0.1
    
    let showNotificationAnimationDelay: TimeInterval = 1
    let showOrHideNotificationAnimationDuration: TimeInterval = 0.2
    
    let chatPanningMinimumVelocityToHideCommands: CGFloat = 100
    let chatPanningMinimumVelocityToShowCommands: CGFloat = 100
    
    var notificationsTopOffset: CGFloat = 0
    
    //MARK: - Методы
    
    override func loadView() {
        substitutesMainViewAutomatically = false
        super.loadView()
    }
    
    override func viewDidLoad() {
        
        constants.updatesAnimationDuration = chatUpdatesAnimationDuration
        
        super.viewDidLoad()
        
        var insets = chatContentInsets
        insets.top = chatTopInset
        chatContentInsets = insets
        
        chatDataSource = ChatItemsDataSourceProxy(with: self)
        
        let decorator = OnboardingChatItemsDecorator()
        chatItemsDecorator = decorator
        itemsDecorator = decorator
        
        view.bringSubview(toFront: notificationsOverlayView)
        view.bringSubview(toFront: notificationsStackView)
        notificationsTopOffset = notificationsStackViewTopConstraint.constant
        hideNotifications(animated: false)
        
        collectionView.showsVerticalScrollIndicator = false
        collectionView.delaysContentTouches = false
        
        commandKeyboard.buttonsTitleFont = UIFont(customName: .graphikLCGRegular, size: 16)
        commandKeyboard.delegate = self
        commandKeyboard.isBackgroundHidden = true
        
        tryFindInputViewBottomConstraints()
        
        // чтобы изначальная настройка прошла после первого лейаута
        DispatchQueue.main.async { [weak self] in
            self?.output.setupInitialState()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        output.viewDidAppear()
    }
    
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return .portrait
    }
    
    override var bottomLayoutGuide: UILayoutSupport {
        guard #available(iOS 11.0, *) else { return super.bottomLayoutGuide }
        return ChatViewControllerFakeBottomLayoutGuide(safeAreaLayoutGuide: view.safeAreaLayoutGuide)
    }
    
    //MARK: - ConfigurableModuleController
    
    func configureModule(with object: Any) {
        if let configuration = object as? OnboardingModuleConfiguration {
            output.configure(with: configuration)
        }
    }
    
    //MARK: - Хелперы
    
    func chattoUpdateType(from updateType: ChatViewUpdateType) -> UpdateType {
        switch updateType {
        case .pagination:
            return .pagination
        case .reload:
            return .reload
        default:
            return .normal
        }
    }
    
    func animateNotificationsStackView(completion: @escaping () -> ()) {
        
        guard notificationsStackView.isHidden else { return }
        let duration = showOrHideNotificationAnimationDuration
        
        notificationsStackView.alpha = 1
        notificationsStackView.isHidden = false
        
        notificationsStackViewTopConstraint.constant = notificationsTopOffset
        
        UIView.animate(withDuration: duration, delay: 0, options: [.beginFromCurrentState, .curveEaseInOut], animations: { [weak self] in
            self?.notificationsStackView.superview?.layoutIfNeeded()
        }) { [weak self] _ in
            self?.output.didShowNotification()
            completion()
        }
    }
    
    func animateNotificationView(_ view: UIView?, delay: TimeInterval, completion: @escaping () -> ()) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + delay) { [weak self] in
            
            guard let strongSelf = self else { return }
            guard !strongSelf.notificationsStackView.isHidden else { return }
            
            let duration = strongSelf.showOrHideNotificationAnimationDuration
            
            UIView.animate(withDuration: duration, delay: 0, options: [.beginFromCurrentState, .curveEaseInOut], animations: { [weak self] in
                view?.isHidden = false
                self?.notificationsStackView.layoutIfNeeded()
            }) { [weak self] _ in
                self?.output.didShowNotification()
                completion()
            }
        }
    }
    
    //MARK: - Chatto методы
    
    override func createChatInputView() -> UIView {
        
        let inputView = UIView(frame: CGRect.zero)
        inputView.translatesAutoresizingMaskIntoConstraints = false
        
        inputView.addSubview(commandKeyboard)
        commandKeyboard.translatesAutoresizingMaskIntoConstraints = false
        
        let viewConstraints = NSLayoutConstraint.edgesConstraints(for: commandKeyboard)
        inputView.addConstraints(viewConstraints)
        
        return inputView
    }
    
    override func createCollectionViewLayout() -> UICollectionViewLayout {
        let layout = ChatLayout()
        layout.delegate = self
        layout.chatLayoutDelegate = self
        return layout
    }
    
    //MARK: - Презентеры ячеек с сообщениями
    
    override func createPresenterBuilders() -> [ChatItemType: [ChatItemPresenterBuilderProtocol]] {
        
        return [
            ChatMessageContent.ContentType.text.rawValue: [
                createTextMessagePresenterBuilder()
            ],
            ChatMessageContent.ContentType.photo.rawValue: [
                createPhotoMessagePresenterBuilder()
            ],
            TypingIndicatorCellObject.chatItemType : [
                TypingIndicatorPresenterBuilder(textCellStyle: createTextMessageStyle(for: output.conversationStyle), cellStyle: createBaseMessageStyle(for: output.conversationStyle))
            ],
            OnboardingBeginCellObject.chatItemType : [
                OnboardingBeginPresenterBuilder()
            ]
        ]
    }
    
    func createBaseMessageStyle(for style: ConversationStyle) -> BaseMessageCollectionViewCellStyleProtocol {
        let outgoingMessageColor = style.outgoingMessageBackgroundColor
        return BaseMessageCellStyleDefault(outgoingColor: outgoingMessageColor)
    }
    
    func createTextMessageStyle(for style: ConversationStyle) -> ChatTextMessageCollectionViewCellStyleProtocol {
        
        let outgoingMessageColor = style.outgoingMessageBackgroundColor
        let outgoingMessageTextColor = style.outgoingMessageTextColor
        let inlineTextColor = style.controlsTextColor
        
        return OnboardingTextBubbleViewStyleDefault(outgoingColor: outgoingMessageColor, outgoingTextColor: outgoingMessageTextColor, inlineTextColor: inlineTextColor)
    }
    
    func createTextMessagePresenterBuilder() -> ChatTextMessagePresenterBuilder<ChatTextMessageViewModelBuilder, ChatTextMessageHandler> {
        
        let baseStyle = createBaseMessageStyle(for: output.conversationStyle)
        let textStyle = createTextMessageStyle(for: output.conversationStyle)
        
        let textMessagePresenterBuilder = ChatTextMessagePresenterBuilder(
            viewModelBuilder: ChatTextMessageViewModelBuilder(style: textStyle, formattingExtractor: textFormattingExtractor),
            interactionHandler: ChatTextMessageHandler(baseHandler: self)
        )
        
        textMessagePresenterBuilder.baseMessageStyle = baseStyle
        textMessagePresenterBuilder.textCellStyle = textStyle
        
        return textMessagePresenterBuilder
    }
    
    func createPhotoMessagePresenterBuilder() -> ChatPhotoMessagePresenterBuilder<ChatPhotoMessageViewModelBuilder, ChatPhotoMessageHandler> {
        
        let outgoingMessageColor = output.conversationStyle.outgoingMessageBackgroundColor
        let outgoingMessageTextColor = output.conversationStyle.outgoingMessageTextColor
        let inlineTextColor = output.conversationStyle.controlsTextColor
        
        let baseStyle = createBaseMessageStyle(for: output.conversationStyle)
        let photoStyle = ChatPhotoBubbleViewStyleDefault(outgoingColor: outgoingMessageColor, outgoingTextColor: outgoingMessageTextColor, inlineTextColor: inlineTextColor)
        
        let photoMessagePresenterBuilder = ChatPhotoMessagePresenterBuilder(
            viewModelBuilder: ChatPhotoMessageViewModelBuilder(style: photoStyle, formattingExtractor: textFormattingExtractor),
            interactionHandler: ChatPhotoMessageHandler(baseHandler: self)
        )
        
        photoMessagePresenterBuilder.baseMessageStyle = baseStyle
        photoMessagePresenterBuilder.photoCellStyle = photoStyle
        
        return photoMessagePresenterBuilder
    }
    
    //MARK: - OnboardingViewInput
    
    func configure(with style: ConversationStyle) {
        commandKeyboard.buttonsTitleColor = style.controlsTextColor
        commandKeyboard.buttonsShadowColor = style.controlsShadowColor
    }
    
    func display(dataSource: ChatDataSource, for updateType: ChatViewUpdateType, completion: (() -> ())?) {
        
        if completion != nil {
            
            let delay = constants.updatesAnimationDuration / 2
            
            // в очередь, чтобы гарантировать completion перед началом выполнения анимации обновления, т.е. одновременно с ней
            updateQueue.addTask { taskCompletion in
                DispatchQueue.main.asyncAfter(deadline: .now() + delay, execute: completion!)
                taskCompletion()
            }
        }
        
        chatItems = dataSource.chatItems
        delegate?.chatDataSourceDidUpdate(self, updateType: chattoUpdateType(from: updateType))
    }
    
    func showCommands(commands: CommandButtonsDataSource, animated: Bool) {
        
        commandKeyboard.fixateCurrentKeyboardHeight = true
        commandKeyboard.buttons = commands.buttons
        
        inputContainer.setNeedsLayout()
        inputContainer.layoutIfNeeded()
        
        commandKeyboard.fixateCurrentKeyboardHeight = false
        commandKeyboard.recalculateHeight()
        
        guard animated else {
            commandKeyboard.invalidateIntrinsicContentSize()
            return
        }
        
        UIView.animate(withDuration: refreshCommandsAnimationDuration) { [weak self] in
            
            guard let strongSelf = self else { return }
            
            strongSelf.commandKeyboard.alpha = 1
            strongSelf.commandKeyboard.invalidateIntrinsicContentSize()
            strongSelf.view.setNeedsLayout()
            strongSelf.view.layoutIfNeeded()
        }
    }
    
    func hideCommands(animated: Bool) {
        UIView.animate(withDuration: refreshCommandsAnimationDuration) { [weak self] in
            self?.commandKeyboard.alpha = 0
        }
    }
    
    func showNotifications() {
        
        guard notificationsStackView.isHidden else { return }

        let backgroundAlpha = notificationsBackgroundAlpha
        let duration = showOrHideNotificationAnimationDuration
        let delay = showNotificationAnimationDelay
        
        weak var secondNotification: UIView? = secondNotificationView
        weak var thirdNotification: UIView? = thirdNotificationView
        
        UIView.animate(withDuration: duration) { [weak self] in
            self?.collectionView.alpha = backgroundAlpha
        }
        
        animateNotificationsStackView { [weak self] in
            self?.animateNotificationView(secondNotification, delay: delay, completion: { [weak self] in
                self?.animateNotificationView(thirdNotification, delay: delay, completion: { [weak self] in
                    let finalDelay = DispatchTime.now() + delay
                    DispatchQueue.main.asyncAfter(deadline: finalDelay, execute: { [weak self] in
                        self?.hideNotifications(animated: true)
                    })
                })
            })
        }
    }
    
    func hideNotifications(animated: Bool) {
        
        guard !notificationsStackView.isHidden else { return }
        
        let hideViews: () -> () = { [weak self] in
            self?.notificationsStackView.alpha = 0
            self?.collectionView.alpha = 1
        }
        
        let topOffset = -firstNotificationView.height
        let layoutViews: (Bool) -> () = { [weak self] _ in
            
            self?.notificationsStackView.isHidden = true
            self?.notificationsStackView.arrangedSubviews.forEach { $0.isHidden = $0 != self?.firstNotificationView }
            
            self?.notificationsStackViewTopConstraint.constant = topOffset
            self?.notificationsStackView.layoutIfNeeded()
        }
        
        if animated {
            let duration = showOrHideNotificationAnimationDuration
            UIView.animate(withDuration: duration, animations: hideViews, completion: layoutViews)
        }
        else {
            notificationsOverlayView.isHidden = true
            hideViews()
            layoutViews(true)
        }
    }
    
    //MARK: - ChatDataSourceProtocol
    
    func loadNext() {}
    
    func loadPrevious() {}
    
    func adjustNumberOfMessages(preferredMaxCount: Int?, focusPosition: Double, completion:(_ didAdjust: Bool) -> Void) {
        completion(false)
    }
    
    //MARK: - ChatLayoutDelegate
    
    func isMessageItem(at indexPath: IndexPath) -> Bool {
        guard let item = chatItemCompanionCollection[safe: indexPath.row] else { return false }
        return item.chatItem is TypingIndicatorCellObject || item.chatItem is BaseMessageCellObjectProtocol
    }
    
    func isIncomingMessageItem(at indexPath: IndexPath) -> Bool {
        guard let item = chatItemCompanionCollection[safe: indexPath.row] else { return false }
        guard !(item.chatItem is TypingIndicatorCellObject) else { return true }
        guard let messageItem = item.chatItem as? BaseMessageCellObjectProtocol else { return false }
        return messageItem.isIncoming
    }
    
    //MARK: - BaseChatMessageHandler
    
    func userDidTapOnFailIcon(viewModel: BaseMessageViewModelProtocol, failIconView: UIView) {}
    func userDidTapOnAvatar(viewModel: BaseMessageViewModelProtocol) {}
    func userDidBeginLongPressOnBubble(viewModel: BaseMessageViewModelProtocol) {}
    func userDidEndLongPressOnBubble(viewModel: BaseMessageViewModelProtocol) {}
    func userDidSelectMessage(viewModel: BaseMessageViewModelProtocol) {}
    func userDidDeselectMessage(viewModel: BaseMessageViewModelProtocol) {}
    func userDidTapOnBubble(viewModel: BaseMessageViewModelProtocol) {}
    func userDidPressInlineKeyboardButton(viewModel: BaseMessageViewModelProtocol, with buttonId: String) {}
    func userDidPressExpandInlineKeyboardButton(viewModel: BaseMessageViewModelProtocol) {}
    func userDidPressToOpen(url: URL, inMessage viewModel: BaseMessageViewModelProtocol) {}
    func userDidPressOptionButton(viewModel: BaseMessageViewModelProtocol, with buttonId: String) {}
    func userDidPressDocumentIcon(viewModel: ChatDocumentMessageViewModelProtocol) {}
    func userDidPressOnPhoto(viewModel: ChatPhotoMessageViewModelProtocol) {}
    
    //MARK: - CommandKeyboardViewDelegate
    
    func didPressCommand(with itemId: String) {
        output.didSelectCommand(with: itemId)
    }
    
    //MARK: - UIScrollViewDelegate
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        UIMenuController.shared.setMenuVisible(false, animated: false)
        hideOrShowInputView()
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        let forceShowInputView = !inputContainer.isUserInteractionEnabled && isScrolledAtBottom()
        guard decelerate || forceShowInputView else { return }
        
        hideOrShowInputView()
    }
    
    //MARK: - Показ/скрытие командной клавиатуры при прокрутке
    
    var isInputViewHidden: Bool {
        return !inputContainer.isUserInteractionEnabled
    }
    
    func tryFindInputViewBottomConstraints() {
        
        // да-да, полухак 💩, но с ним можно реально сдвинуть любую input view вниз, а не просто спрятать.
        // если изменятся констрейны при обновлении библиотеки, то пострадает только кнопка скролла чата вниз,
        // которая при скрытии командной клавиатуры останется "висеть"
        originalInputContainerBottomConstraint = inputContainer.superview?.constraints.filter { $0.secondItem === inputContainer && $0.secondAttribute == .bottom }.first
        
        guard originalInputContainerBottomConstraint != nil else { return }
        
        hiddenInputContainerBottomConstraint = NSLayoutConstraint(item: view, attribute: .bottom, relatedBy: .equal, toItem: inputContainer, attribute: .bottom, multiplier: 1, constant: 0)
        view.addConstraint(hiddenInputContainerBottomConstraint!)
        hiddenInputContainerBottomConstraint?.isActive = false
        
        bottomSpaceView.removeFromSuperview()
    }
    
    func setInputViewHidden(hidden: Bool) {
        
        guard isInputViewHidden != hidden else { return }
        
        let inputAlpha: CGFloat = hidden ? 0 : 1
        
        if #available(iOS 11.0, *) {
            
            let bottomInsetAdjustment = hidden ? view.safeAreaInsets.bottom : -view.safeAreaInsets.bottom
            var insets = chatContentInsets
            insets.bottom += bottomInsetAdjustment
            
            chatContentInsets = insets
        }
        
        if hidden {
            hiddenInputContainerBottomConstraint?.constant = -inputContainer.height
        }
        
        // деактивируем сначала оба констрейна т.к. они конфликтные
        let allBottomConstraints: [NSLayoutConstraint] = [originalInputContainerBottomConstraint, hiddenInputContainerBottomConstraint].compactMap { $0 }
        NSLayoutConstraint.deactivate(allBottomConstraints)
        
        originalInputContainerBottomConstraint?.isActive = !hidden
        hiddenInputContainerBottomConstraint?.isActive = hidden
        
        inputContainer.isUserInteractionEnabled = !hidden
        inputContainer.alpha = inputAlpha
        inputContainer.superview?.layoutIfNeeded()
    }
    
    func hideOrShowInputView() {
        
        let visibleHeight = collectionView.height - commandKeyboard.height
        guard collectionView.contentSize.height > visibleHeight else { return }
        
        let velocity = collectionView.panGestureRecognizer.velocity(in: collectionView).y
        let isScrolledToBottomDirection = velocity < 0
        let maximumVelocity = isScrolledToBottomDirection ? chatPanningMinimumVelocityToShowCommands : chatPanningMinimumVelocityToHideCommands
        
        guard isScrolledToBottomDirection != inputContainer.isUserInteractionEnabled else { return }
        guard abs(velocity) >= maximumVelocity else { return }
        
        UIView.animate(withDuration: hideOrShowCommandsAnimationDuration) { [weak self] in
            self?.setInputViewHidden(hidden: !isScrolledToBottomDirection)
        }
    }
}

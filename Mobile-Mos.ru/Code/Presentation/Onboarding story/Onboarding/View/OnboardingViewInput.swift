//
//  OnboardingViewInput.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 09.04.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

protocol OnboardingViewInput: class {
    
    var showsTypingIndicator: Bool { get set }
    
    func configure(with style: ConversationStyle)
    
    func display(dataSource: ChatDataSource, for updateType: ChatViewUpdateType, completion: (() -> ())?)
    
    func showCommands(commands: CommandButtonsDataSource, animated: Bool)
    
    func hideCommands(animated: Bool)
    
    func showNotifications()
    
    func hideNotifications(animated: Bool)
}

//
//  OnboardingBeginPresenter.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 10.04.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation
import Chatto
import ChattoAdditions

class OnboardingBeginPresenter: ChatItemPresenterProtocol {
    
    let model: OnboardingBeginCellObject
    
    init(model: OnboardingBeginCellObject) {
        self.model = model
    }
    
    fileprivate static let cellReuseIdentifier = String(describing: OnboardingBeginCell.self)
    
    //MARK: - ChatItemPresenterProtocol
    
    var canCalculateHeightInBackground: Bool {
        return true
    }
    
    static func registerCells(_ collectionView: UICollectionView) {
        collectionView.register(UINib(nibName: String(describing: OnboardingBeginCell.self), bundle: nil), forCellWithReuseIdentifier: cellReuseIdentifier)
    }
    
    func dequeueCell(collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell {
        return collectionView.dequeueReusableCell(withReuseIdentifier: OnboardingBeginPresenter.cellReuseIdentifier, for: indexPath)
    }
    
    func configureCell(_ cell: UICollectionViewCell, decorationAttributes: ChatItemDecorationAttributesProtocol?) {}
    
    func heightForCell(maximumWidth width: CGFloat, decorationAttributes: ChatItemDecorationAttributesProtocol?) -> CGFloat {
        return 100
    }
}

class OnboardingBeginPresenterBuilder: ChatItemPresenterBuilderProtocol {
    
    public func canHandleChatItem(_ chatItem: ChatItemProtocol) -> Bool {
        return chatItem is OnboardingBeginCellObject
    }
    
    public func createPresenterWithChatItem(_ chatItem: ChatItemProtocol) -> ChatItemPresenterProtocol {
        precondition(canHandleChatItem(chatItem), "Попытка создать OnboardingBeginPresenter для \(type(of: chatItem))")
        return OnboardingBeginPresenter(model: chatItem as! OnboardingBeginCellObject)
    }
    
    public var presenterType: ChatItemPresenterProtocol.Type {
        return OnboardingBeginPresenter.self
    }
}

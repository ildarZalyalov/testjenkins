//
//  OnboardingBeginCellObject.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 10.04.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation
import Chatto
import ChattoAdditions

class OnboardingBeginCellObject: ChatItemProtocol {
    
    let uid: String = UUID().uuidString
    let type: ChatItemType = OnboardingBeginCellObject.chatItemType
    
    static var chatItemType: ChatItemType {
        return String(describing: OnboardingBeginCellObject.self)
    }
}

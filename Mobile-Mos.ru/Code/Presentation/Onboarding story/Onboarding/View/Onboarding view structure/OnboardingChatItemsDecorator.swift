//
//  OnboardingChatItemsDecorator.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 10.04.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation
import Chatto
import ChattoAdditions

/// Декоратор чата онбординга, добавляет к сообщениям ячейки с дополнительными элементами
final class OnboardingChatItemsDecorator: ChatItemsDecoratorProtocol {
    
    /// Маленький отступ
    let shortSeparation: CGFloat = 4
    
    /// Обычный отступ
    let normalSeparation: CGFloat = 12
    
    /// Отступ индикатора "вам отвечают" снизу
    let typingIndicatorBottomMargin: CGFloat = 40
    
    /// Отступ от ячейки "начала онбординга"
    let onboardingBeginSeparation: CGFloat = 16
    
    
    /// Отступ последней ячейки в чате
    var lastItemSeparation: CGFloat = 7
    
    /// Надо ли отображать ячейку с индикатором "вам отвечают"
    var shouldShowTypingIndicator: Bool = false {
        didSet {
            guard !oldValue && shouldShowTypingIndicator else { return }
            typingIdicatorCellObject = TypingIndicatorCellObject()
        }
    }
    
    /// Добавлять ли отступ снизу для индикатора "вам отвечают"
    var shouldUseTypingIndicatorBottomMargin = false
    
    fileprivate var typingIdicatorCellObject = TypingIndicatorCellObject()
    
    //MARK: - ChatItemsDecoratorProtocol
    
    func decorateItems(_ chatItems: [ChatItemProtocol]) -> [DecoratedChatItem] {
        
        var decoratedChatItems = [DecoratedChatItem]()
        
        decoratedChatItems.appendDecoratedItem(with: OnboardingBeginCellObject(), bottomMargin: onboardingBeginSeparation, isShowingTail: false)
        
        for (index, chatItem) in chatItems.enumerated() {
            
            let previous: ChatItemProtocol? = (index - 1 >= 0) ? chatItems[index - 1] : nil
            let next: ChatItemProtocol? = (index + 1 < chatItems.count) ? chatItems[index + 1] : nil
            
            var canShowTail = false
            
            // определяем показывать ли хвостик
            // хвостик показывается, если предыдущее сообщение было от другого пользователя или предыдущий элемент не сообщение
            if let currentMessage = chatItem as? MessageModelProtocol {
                if let previousMessage = previous as? MessageModelProtocol {
                    canShowTail = currentMessage.senderId != previousMessage.senderId
                } else {
                    canShowTail = true
                }
            }
            
            // если нам надо показать хвостик, убеждаемся, что можем это сделать
            // мы не можем показывать хвостики, если в сообщении нет текста
            if let messageItem = chatItem as? BaseMessageCellObjectProtocol, canShowTail {
                canShowTail = !messageItem.text.isEmpty
            }
            
            let bottomMargin = separationAfterItem(chatItem, next: next)
            decoratedChatItems.appendDecoratedItem(with: chatItem, bottomMargin: bottomMargin, canShowFailedIcon: true, isShowingTail: canShowTail, isShowingAvatar: false)
        }
        
        if shouldShowTypingIndicator {
            
            let bottomMargin = shouldUseTypingIndicatorBottomMargin ? typingIndicatorBottomMargin : 0
            let lastMessage = chatItems.last as? MessageModelProtocol
            let isShowingTail = !(lastMessage?.isIncoming ?? false)
            
            decoratedChatItems.appendDecoratedItem(with: typingIdicatorCellObject, bottomMargin: bottomMargin, isShowingTail: isShowingTail)
        }
        
        return decoratedChatItems
    }
    
    /// Считаем отступ для элемента
    ///
    /// - Parameters:
    ///   - current: элемент
    ///   - next: следующий элемент (если есть)
    /// - Returns: отступ для элемента снизу
    func separationAfterItem(_ current: ChatItemProtocol?, next: ChatItemProtocol?) -> CGFloat {
        
        guard let nextItem = next else {
            
            guard shouldShowTypingIndicator else { return lastItemSeparation }
            
            let isIncoming = (current as? MessageModelProtocol)?.isIncoming ?? false
            return isIncoming ? shortSeparation : normalSeparation
        }
        
        guard let currentMessage = current as? MessageModelProtocol else { return normalSeparation }
        guard let nextMessage = nextItem as? MessageModelProtocol else { return normalSeparation }
        
        if currentMessage.senderId != nextMessage.senderId {
            return normalSeparation
        } else {
            return shortSeparation
        }
    }
}

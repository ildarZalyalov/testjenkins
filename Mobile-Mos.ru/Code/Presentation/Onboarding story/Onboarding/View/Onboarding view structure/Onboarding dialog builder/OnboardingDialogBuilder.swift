//
//  OnboardingDialogBuilder.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 10.04.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Действия кнопок в онбординге
///
/// - sendMessage: отправить сообщение и получить ответы, связанное значение - идентификаторы сообщений с ответами
/// - demonstrateNotifications: показать уведомления
/// - askEnablingNotifications: запросить разрешение показывать уведомление, связанное значение - обработчик, который в зависимости от результата вернет текст пользовательского сообщения и идентификаторы сообщений с ответами
/// - finishOnboarding: завершить онбординг
enum OnboardingDialogMessageButtonAction {
    case sendMessage(answerMessagesIds: [Int])
    case demonstrateNotifications
    case askEnablingNotifications(resultHandler: (Bool) -> (String, [Int]))
    case finishOnboarding
}

/// Кнопка в онбординге
struct OnboardingDialogMessageButton {
    
    /// Идентификатор кнопки
    let buttonId: String
    
    /// Заголовок кнопки
    let title: String
    
    /// Действие кнопки
    let action: OnboardingDialogMessageButtonAction
}

/// Сообщение в онбординге
struct OnboardingDialogMessage {
    
    /// Идентификатор сообщения
    let messageId: Int

    /// Текст сообщения
    var text: String = String()
    
    /// Ссылка на фото в сообщении
    var photoUrl: URL?
    
    /// Кнопки командной клавиатуры
    var buttons: [OnboardingDialogMessageButton] = []
}

/// Билдер диалога в онбординге
protocol OnboardingDialogBuilder {
    
    /// Получить идентификаторы начальных сообщений
    func obtainInitialMessagesIds() -> [Int]
    
    /// Получить сообщения с идентификаторами
    ///
    /// - Parameter messageIds: идентификаторы сообщений
    /// - Returns: сообщения
    func obtainMessages(with messageIds: [Int]) -> [OnboardingDialogMessage]
}

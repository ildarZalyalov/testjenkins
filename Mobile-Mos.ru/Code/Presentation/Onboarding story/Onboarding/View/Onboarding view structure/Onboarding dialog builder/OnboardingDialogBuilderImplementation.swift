//
//  OnboardingDialogBuilderImplementation.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 10.04.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

class OnboardingDialogBuilderImplementation: OnboardingDialogBuilder {
    
    lazy var messagesById: [Int : OnboardingDialogMessage] = self.messages.toDictionary { $0.messageId }
    
    //MARK: - OnboardingDialogBuilder
    
    let demonstrateNotificationsButtonId: String = "demonstrateNotificationsButtonId"
    
    let askEnablingNotificationsButtonId: String = "askEnablingNotificationsButtonId"
    
    let finishOnboardingButtonId: String = "finishOnboardingButtonId"
    
    func obtainInitialMessagesIds() -> [Int] {
        return [1, 2]
    }
    
    func obtainMessages(with messageIds: [Int]) -> [OnboardingDialogMessage] {
        
        var messages = [OnboardingDialogMessage]()
        
        for messageId in messageIds {
            guard let message = messagesById[messageId] else { continue }
            messages.append(message)
        }
        
        return messages
    }
    
    //MARK: - Сообщения
    
    lazy var messages: [OnboardingDialogMessage] = [
        
        OnboardingDialogMessage(messageId: 1,
                                text: "Здравствуйте!<br/>Будущее уже здесь и все больше людей ежедневно пользуются роботами и виртуальными помощниками, такими как я 😉",
                                photoUrl: nil,
                                buttons: []),
        
        OnboardingDialogMessage(messageId: 2,
                                text: String(),
                                photoUrl: Bundle.main.url(forResource: "milkSpilling", withExtension: "gif"),
                                buttons: [
                                    OnboardingDialogMessageButton(buttonId: UUID().uuidString, title: "Дальше 👉", action: .sendMessage(answerMessagesIds: [3]))
            ]),
        
        OnboardingDialogMessage(messageId: 3,
                                text: "Сейчас я быстро расскажу вам о самых полезных и интересных сервисах, которые есть в приложении “Моя Москва”",
                                photoUrl: nil,
                                buttons: [
                                    OnboardingDialogMessageButton(buttonId: UUID().uuidString, title: "В другой раз", action: .sendMessage(answerMessagesIds: [8])),
                                    OnboardingDialogMessageButton(buttonId: UUID().uuidString, title: "Расскажите 😃", action: .sendMessage(answerMessagesIds: [4]))
            ]),
        
        OnboardingDialogMessage(messageId: 4,
                                text: "Я могу за пару минут записать вас к врачу, помогу подать показания счетчиков и сообщу, когда ваш ребенок придет в школу и какие оценки там получит",
                                photoUrl: nil,
                                buttons: [
                                    OnboardingDialogMessageButton(buttonId: UUID().uuidString, title: "А что еще?", action: .sendMessage(answerMessagesIds: [5, 6])),
                                    OnboardingDialogMessageButton(buttonId: UUID().uuidString, title: "Дальше 👉", action: .sendMessage(answerMessagesIds: [5, 6]))
            ]),
        
        OnboardingDialogMessage(messageId: 5,
                                text: "А еще, я буду рада сообщить вам о важных новостях города, и если нужно, переключу вас в чат с оператором городского контакт-центра, готовым вас проконсультировать",
                                photoUrl: nil,
                                buttons: []),
        
        OnboardingDialogMessage(messageId: 6,
                                text: String(),
                                photoUrl: Bundle.main.url(forResource: "catTyping", withExtension: "gif"),
                                buttons: [
                                    OnboardingDialogMessageButton(buttonId: UUID().uuidString, title: "И это всё?", action: .sendMessage(answerMessagesIds: [7])),
                                    OnboardingDialogMessageButton(buttonId: UUID().uuidString, title: "Понятно, спасибо", action: .sendMessage(answerMessagesIds: [8]))
            ]),
        
        OnboardingDialogMessage(messageId: 7,
                                text: "Конечно, нет. Еще я могу проверить автомобиль перед покупкой, сообщу, если машину эвакуируют, и пришлю уведомление, если вам выпишут штраф. Правда, я надеюсь, что у вас-то штрафов не будет",
                                photoUrl: nil,
                                buttons: [
                                    OnboardingDialogMessageButton(buttonId: UUID().uuidString, title: "Неплохо", action: .sendMessage(answerMessagesIds: [8])),
                                    OnboardingDialogMessageButton(buttonId: UUID().uuidString, title: "Давайте дальше 👉", action: .sendMessage(answerMessagesIds: [8]))
            ]),
        
        OnboardingDialogMessage(messageId: 8,
                                text: "Не забудьте включить уведомления для приложения. Обещаю, буду сообщать только о важных событиях, чтобы не отвлекать вас",
                                photoUrl: nil,
                                buttons: [
                                    OnboardingDialogMessageButton(buttonId: UUID().uuidString, title: "А как выглядят уведомления?", action: .demonstrateNotifications),
                                    OnboardingDialogMessageButton(buttonId: UUID().uuidString, title: "Дальше 👉", action: .askEnablingNotifications(resultHandler: { success in
                                        return success ? ("Хорошо, включай уведомления", [9, 11, 12]) : ("Нет, не включай уведомления", [10, 11, 12])
                                    }))
            ]),
        
        OnboardingDialogMessage(messageId: 9,
                                text: "Включила!️",
                                photoUrl: nil,
                                buttons: []),
        
        OnboardingDialogMessage(messageId: 10,
                                text: "Хорошо. Вы всегда можете включить уведомления в настройках.",
                                photoUrl: nil,
                                buttons: []),
        
        OnboardingDialogMessage(messageId: 11,
                                text: "Жду вас в приложении",
                                photoUrl: nil,
                                buttons: []),
        
        OnboardingDialogMessage(messageId: 12,
                                text: String(),
                                photoUrl: Bundle.main.url(forResource: "rocketStarting", withExtension: "gif"),
                                buttons: [
                                    OnboardingDialogMessageButton(buttonId: UUID().uuidString, title: "Войти в приложение 🚀", action: .finishOnboarding)
            ]),
        ]
}

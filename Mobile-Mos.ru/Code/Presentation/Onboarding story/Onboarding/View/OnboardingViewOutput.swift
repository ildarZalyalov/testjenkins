//
//  OnboardingViewOutput.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 09.04.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

protocol OnboardingViewOutput: class {
    
    var conversationStyle: ConversationStyle { get }
    
    func configure(with configuration: OnboardingModuleConfiguration)
    
    func setupInitialState()
    
    func viewDidAppear()
    
    func didSelectCommand(with itemId: String)
    
    func didShowNotification()
}

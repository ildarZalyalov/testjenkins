//
//  OnboardingModuleConfigurator.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 09.04.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation
import UIKit

class OnboardingModuleConfigurator: BaseModuleConfigurator {
    
    override func configureModule(for view: UIViewController) {
        
        let viewController = view as! OnboardingViewController
        let presenter = OnboardingPresenter()
        let interactor = OnboardingInteractor()
        let router = OnboardingRouter()
        
        let messageFactory = UIApplication.shared.serviceBuilder.getChatMessageFactory()
        let formattingExtractor = ChatTextFormattingExtractorImplementation()
        let userService = UIApplication.shared.serviceBuilder.getUserService()
        let dialogBuilder = OnboardingDialogBuilderImplementation()
        
        let dataSourceFactory = ChatDataSourceFactoryImplementation()
        let commandsDataSourceFactory = CommandButtonsDataSourceFactoryImplementation()
        let textFormattingExtractor = ChatTextFormattingExtractorImplementation()
        
        let soundsManager = SoundsManagerImplementation()
        let notificationCenter = NotificationCenter.default
        
        viewController.output = presenter
        viewController.textFormattingExtractor = textFormattingExtractor
        
        presenter.view = viewController
        presenter.interactor = interactor
        presenter.router = router
        presenter.dataSourceFactory = dataSourceFactory
        presenter.commandsDataSourceFactory = commandsDataSourceFactory
        presenter.dialogBuilder = dialogBuilder
        presenter.soundsManager = soundsManager
        presenter.notificationCenter = notificationCenter
        if #available(iOS 10.0, *) {
            let hapticFeedManager = HapticFeedbackManagerImplementation()
            presenter.hapticFeedManager = hapticFeedManager
        }
        
        interactor.output = presenter
        interactor.messageFactory = messageFactory
        interactor.formattingExtractor = formattingExtractor
        interactor.userService = userService
        
        router.transitionHandler = viewController
    }
}

//
//  OnboardingRouterInput.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 09.04.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

protocol OnboardingRouterInput: class {
    
    func configure(with builder: OnboardingBuilder)

    func hideModule()
}

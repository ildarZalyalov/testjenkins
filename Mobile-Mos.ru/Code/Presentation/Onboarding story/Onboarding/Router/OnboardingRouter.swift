//
//  OnboardingRouter.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 09.04.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

class OnboardingRouter: OnboardingRouterInput {
    
    weak var transitionHandler: UIViewController!
    
    weak var onboardingBuilder: OnboardingBuilder?
    
    //MARK: - OnboardingRouterInput
    
    func configure(with builder: OnboardingBuilder) {
        onboardingBuilder = builder
    }
    
    func hideModule() {
        onboardingBuilder?.hideOnboarding(animated: true)
    }
}

//
//  CTMosRuApplicationSendingSuccess.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 06.07.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

class CTMosRuApplicationSendingSuccess: UIViewController {
    
    @IBOutlet weak var messageLabel: UILabel!
    
    let messageLabelLineHeightMultiple: CGFloat = 1.3
    let unwindSegueIdentifier = "unwindToServicesAndApps"
    
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        applyMessageLabelStyle()
    }
    
    @IBAction func closeModule() {
        performSegue(withIdentifier: unwindSegueIdentifier, sender: self)
    }
    
    func applyMessageLabelStyle() {
        
        guard let message = messageLabel.text else { return }
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineHeightMultiple = messageLabelLineHeightMultiple
        paragraphStyle.alignment = .center
        
        let attributes: [NSAttributedStringKey : Any] = [NSAttributedStringKey.font : messageLabel.font, NSAttributedStringKey.paragraphStyle : paragraphStyle]
        let messageString = NSAttributedString(string: message, attributes: attributes)
        
        messageLabel.attributedText = messageString
    }
}

//
//  CTMosRuSendApplicationViewController.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 06.07.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import UIKit

class CTMosRuSendApplicationViewController: BaseViewController, CTMosRuSendApplicationViewInput, NavigationBarCustomizingController, ConfigurableModuleController, UITextFieldDelegate, UITextViewDelegate, UIPickerViewDelegate {
    
    var output: CTMosRuSendApplicationViewOutput!
    var notificationCenter: NotificationCenter!
    var phoneTextFieldFormatManager: TextFieldFormatManager!
    
    @IBOutlet weak var cancelBarButtonItem: UIBarButtonItem!
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var deviceTypeStackView: UIStackView!
    @IBOutlet weak var deviceTypeLabel: UILabel!
    @IBOutlet weak var deviceTypeTextField: UITextField!
    @IBOutlet weak var deviceTypeSeparator: UIView!
    @IBOutlet weak var deviceTypeWarningImage: UIImageView!
    
    @IBOutlet weak var deviceManufacturerStackView: UIStackView!
    @IBOutlet weak var deviceManufacturerLabel: UILabel!
    @IBOutlet weak var deviceManufacturerTextField: UITextField!
    @IBOutlet weak var deviceManufacturerSeparator: UIView!
    @IBOutlet weak var deviceManufacturerWarningImage: UIImageView!
    
    @IBOutlet weak var deviceModelStackView: UIStackView!
    @IBOutlet weak var deviceModelLabel: UILabel!
    @IBOutlet weak var deviceModelTextField: UITextField!
    @IBOutlet weak var deviceModelSeparator: UIView!
    @IBOutlet weak var deviceModelWarningImage: UIImageView!
    
    @IBOutlet weak var problemStackView: UIStackView!
    @IBOutlet weak var problemLabel: UILabel!
    @IBOutlet weak var problemTextView: UITextView!
    @IBOutlet weak var problemSeparator: UIView!
    @IBOutlet weak var problemWarningImage: UIView!
    
    @IBOutlet weak var usernameStackView: UIStackView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var usernameSeparator: UIView!
    @IBOutlet weak var usernameWarningImage: UIImageView!
    
    @IBOutlet weak var phoneStackView: UIStackView!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var phoneSeparator: UIView!
    @IBOutlet weak var phoneWarningImage: UIImageView!
    @IBOutlet weak var numberPlaceholderLabel: UILabel!
    
    @IBOutlet weak var addressStackView: UIStackView!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var addressTextView: UITextView!
    @IBOutlet weak var addressSeparator: UIView!
    @IBOutlet weak var addressWarningImage: UIView!
    
    @IBOutlet weak var problemHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var addressHeightConstraint: NSLayoutConstraint!
    
    let emptyLineChar: Character = "\n"
    let phonePrefix = "+7"
    
    let deviceTypePickerTag = 1
    let deviceManufacturerPickerTag = 2
    
    let textFieldsLabelAffineTransform = CGAffineTransform(scaleX: 0.8, y: 0.8)
    let textFieldsLabelAnimationDuration = 0.6
    let textFieldlabelTransformAnchorPoint = CGPoint(x: 0.625, y: 0.5)
    let textFieldlabelDefaultAnchorPoint = CGPoint(x: 0.5, y: 0.5)
    
    let textTypingFont = UIFont(customName: .graphikLCGMedium, size: 17)!
    let normalFont = UIFont(customName: .graphikLCGRegular, size: 17)!
    
    var isKeyboardOpen = false
    var problemDefaultHeight: CGFloat = 0
    var addressDefaultHeight: CGFloat = 0
    
    var pickerDeviceType: UIPickerView?
    var pickerDeviceTypeDataSource: [String]!
    
    var pickerDeviceManufacturer: UIPickerView?
    var pickerDeviceManufacturerDataSource: [String]!
    
    //MARK: - Методы
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        deviceTypeTextField.font = normalFont
        
        phoneTextFieldFormatManager.configure(with: phoneTextField, mask: nil)
        
        problemWarningImage.isHidden = true
        problemTextView.isScrollEnabled = false
        problemTextView.textContainer.lineFragmentPadding = 0
        problemDefaultHeight = problemHeightConstraint.constant
        
        addressWarningImage.isHidden = true
        addressTextView.isScrollEnabled = false
        addressTextView.textContainer.lineFragmentPadding = 0
        addressDefaultHeight = addressHeightConstraint.constant
        
        configureCancelButton()
        
        setupKeyboardNotifications()
        setupGestures()
        setupInputAccessoryView()
        
        output.setupInitialState()
    }
    
    deinit {
        notificationCenter.removeObserver(self)
    }
    
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override func viewSafeAreaInsetsDidChange() {
        resetScrollViewInsets()
    }
    
    @IBAction func cancelPressed() {
        view.endEditing(true)
        output.didPressCancel()
    }
    
    @IBAction func showRulesPressed() {
        output.didPressShowRules()
    }
    
    @IBAction func confirmPressed() {
        output.didPressConfirm(deviceModel: deviceModelTextField.text, problem: problemTextView.text, username: usernameTextField.text, phone: obtainPhoneNumberWithCorrectFormat(from: phoneTextField.text), address: addressTextView.text)
    }
    
    //MARK: - NavigationBarCustomizingController
    
    var navigationBarState: NavigationBarState? {
        
        var state = NavigationBarState()
        
        state.isHidden = false
        state.isTranslucent = false
        state.backgroundState = NavigationBarBackgroundState.regular
        state.tintColor = UIColor.black
        state.isShadowHidden = true
        
        let titleTextAttributes = [
            NSAttributedStringKey.font : UIFont(customName: .graphikLCGBold, size: 15) as Any,
            NSAttributedStringKey.foregroundColor : UIColor.black
        ]
        let largeTitleTextAttributes = [
            NSAttributedStringKey.font : UIFont(customName: .graphikLCGBold, size: 25) as Any,
            NSAttributedStringKey.foregroundColor : UIColorPalette.conversationsPageTitleColor
        ]
        
        state.titleTextAttributes = titleTextAttributes
        state.largeTitleTextAttributes = largeTitleTextAttributes
        
        return state
    }
    
    //MARK: - ConfigurableModuleController
    
    func configureModule(with object: Any) {
        if let configuration = object as? CTMosRuSendApplicationModuleConfiguration {
            output.configure(with: configuration)
        }
    }
    
    //MARK: - CTMosRuSendApplicationViewInput
    
    func displayDeviceTypeValue(value: String) {
        deviceTypeTextField.text = value
        guard let index = pickerDeviceTypeDataSource?.index(of: value) else { return }
        pickerDeviceType?.selectRow(index, inComponent: 0, animated: false)
    }
    
    func displayDeviceManufacturerValue(value: String?) {
        
        deviceManufacturerTextField.text = value
        
        guard value != nil else { return }
        guard let index = pickerDeviceManufacturerDataSource?.index(of: value!) else { return }
        
        pickerDeviceManufacturer?.selectRow(index, inComponent: 0, animated: false)
    }
    
    func highlightFieldsWithErrors() {
        validateField(with: deviceTypeTextField.text, separator: deviceTypeSeparator, warningView: deviceTypeWarningImage)
        validateField(with: deviceManufacturerTextField.text, separator: deviceManufacturerSeparator, warningView: deviceManufacturerWarningImage)
        validateField(with: deviceModelTextField.text, separator: deviceModelSeparator, warningView: deviceModelWarningImage)
        validateField(with: problemTextView.text, separator: problemSeparator, warningView: problemWarningImage)
        validateField(with: usernameTextField.text, separator: usernameSeparator, warningView: usernameWarningImage)
        validateField(with: phoneTextField.text, separator: phoneSeparator, warningView: phoneWarningImage, isPhone: true)
        validateField(with: addressTextView.text, separator: addressSeparator, warningView: addressWarningImage)
    }
    
    func setupPickerInputForDeviceType(with dataSource: [String]) {
        pickerDeviceTypeDataSource = dataSource
        pickerDeviceType = setupDataPicker(for: deviceTypeTextField, with: dataSource, and: deviceTypePickerTag)
    }
    
    func setupPickerInputForDeviceManufacturer(with dataSource: [String]) {
        pickerDeviceManufacturerDataSource = dataSource
        pickerDeviceManufacturer = setupDataPicker(for: deviceManufacturerTextField, with: dataSource, and: deviceManufacturerPickerTag)
    }
    
    //MARK: - UITextFieldDelegate
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == phoneTextField {
            return phoneTextFieldFormatManager.textField!(textField, shouldChangeCharactersIn: range, replacementString: string)
        }
        
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        textField.font = textTypingFont
        
        if textField == deviceTypeTextField {
            deviceTypeWarningImage.isHidden = true
            deviceTypeSeparator.backgroundColor = UIColorPalette.selectedTextFieldSeparatorColor
            animateTextFieldTitleState(with: deviceTypeLabel, view: deviceTypeTextField)
        }
        else if textField == deviceManufacturerTextField {
            deviceManufacturerWarningImage.isHidden = true
            deviceManufacturerSeparator.backgroundColor = UIColorPalette.selectedTextFieldSeparatorColor
            animateTextFieldTitleState(with: deviceManufacturerLabel, view: deviceManufacturerTextField)
        }
        else if textField == deviceModelTextField {
            deviceModelWarningImage.isHidden = true
            deviceModelSeparator.backgroundColor = UIColorPalette.selectedTextFieldSeparatorColor
            animateTextFieldTitleState(with: deviceModelLabel, view: deviceModelTextField)
        }
        else if textField == usernameTextField {
            usernameWarningImage.isHidden = true
            usernameSeparator.backgroundColor = UIColorPalette.selectedTextFieldSeparatorColor
            animateTextFieldTitleState(with: usernameLabel, view: usernameTextField)
        }
        else if textField == phoneTextField {
            
            numberPlaceholderLabel.isHidden = false
            phoneStackView.layoutIfNeeded()
            
            phoneWarningImage.isHidden = true
            phoneSeparator.backgroundColor = UIColorPalette.selectedTextFieldSeparatorColor
            animateTextFieldTitleState(with: phoneLabel, view: numberPlaceholderLabel)
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        textField.font = normalFont
        
        var separator: UIView? = nil
        var warningView: UIImageView? = nil
        
        if textField == deviceTypeTextField {
            separator = deviceTypeSeparator
            warningView = deviceTypeWarningImage
            revertTextFieldTitleState(with: deviceTypeLabel, view: deviceTypeTextField)
        }
        else if textField == deviceManufacturerTextField {
            separator = deviceManufacturerSeparator
            warningView = deviceManufacturerWarningImage
            revertTextFieldTitleState(with: deviceManufacturerLabel, view: deviceManufacturerTextField)
        }
        else if textField == deviceModelTextField {
            separator = deviceModelSeparator
            warningView = deviceModelWarningImage
            revertTextFieldTitleState(with: deviceModelLabel, view: deviceModelTextField)
        }
        else if textField == usernameTextField {
            separator = usernameSeparator
            warningView = usernameWarningImage
            revertTextFieldTitleState(with: usernameLabel, view: usernameTextField)
        }
        else if textField == phoneTextField {
            
            separator = phoneSeparator
            warningView = phoneWarningImage
            
            if let text = textField.text, text.isEmpty {
                numberPlaceholderLabel.isHidden = true
                phoneStackView.layoutIfNeeded()
            }
            
            let viewToPlaceLabelAgainst = numberPlaceholderLabel.isHidden ? phoneTextField : numberPlaceholderLabel
            revertTextFieldTitleState(with: phoneLabel, view: viewToPlaceLabelAgainst)
        }
        
        validateField(with: textField.text, separator: separator, warningView: warningView, isPhone: textField == phoneTextField)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == deviceTypeTextField {
            deviceManufacturerTextField.becomeFirstResponder()
        }
        else if textField == deviceManufacturerTextField {
            deviceModelTextField.becomeFirstResponder()
        }
        else if textField == deviceModelTextField {
            problemTextView.becomeFirstResponder()
        }
        else if textField == usernameTextField {
            phoneTextField.becomeFirstResponder()
        }
        else if textField == phoneTextField {
            addressTextView.becomeFirstResponder()
        }
        
        return false
    }
    
    //MARK: - UITextViewDelegate
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        guard text.last != emptyLineChar else {
            
            if textView == addressTextView {
                textView.resignFirstResponder()
            }
            else {
                usernameTextField.becomeFirstResponder()
            }
            
            return false
        }
        
        return true
    }
    
    func textViewDidChange(_ textView: UITextView) {
        updateHeight(for: textView, font: textTypingFont)
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        textView.font = textTypingFont
        
        if textView == problemTextView {
            
            problemDefaultHeight = problemHeightConstraint.constant
            
            problemSeparator.backgroundColor = UIColorPalette.selectedTextFieldSeparatorColor
            problemWarningImage.isHidden = true
            animateTextFieldTitleState(with: problemLabel, view: problemTextView)
        }
        else if textView == addressTextView {
            
            addressDefaultHeight = addressHeightConstraint.constant
            
            addressSeparator.backgroundColor = UIColorPalette.selectedTextFieldSeparatorColor
            addressWarningImage.isHidden = true
            animateTextFieldTitleState(with: addressLabel, view: addressTextView)
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        textView.font = normalFont
        
        var separator: UIView? = nil
        var warningView: UIView? = nil
        
        if textView == problemTextView {
            
            separator = problemSeparator
            warningView = problemWarningImage
            
            revertTextFieldTitleState(with: problemLabel, view: problemTextView)
        }
        else if textView == addressTextView {
            
            separator = addressSeparator
            warningView = addressWarningImage
            
            revertTextFieldTitleState(with: addressLabel, view: addressTextView)
        }
        
        updateHeight(for: textView, font: normalFont)
        validateField(with: textView.text, separator: separator, warningView: warningView)
    }
    
    //MARK: - PickerViewDelegate
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let pickerDataSource: [String]! = pickerView.tag == deviceManufacturerPickerTag ? pickerDeviceManufacturerDataSource : pickerDeviceTypeDataSource
        return pickerDataSource[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView.tag == deviceTypePickerTag {
            output.didSelectInPickerDeviceType(at: row)
        }
        else if pickerView.tag == deviceManufacturerPickerTag {
            output.didSelectInPickerDeviceManufacturer(at: row)
        }
    }
    
    //MARK: - Helper methods
    
    func validateField(with text: String?, separator: UIView?, warningView: UIView?, isPhone: Bool = false) {
        
        var textString: String
        
        if isPhone {
            textString = obtainPhoneNumberWithCorrectFormat(from: text) ?? String()
        }
        else {
            textString = text?.trimmingCharacters(in: .whitespaces) ?? String()
        }

        let valid = isPhone ? output.validatePhoneNumber(number: textString) : !textString.isEmpty
        
        separator?.backgroundColor = !valid ? UIColorPalette.errorInUserDataColor : UIColorPalette.selectedTextFieldSeparatorColor
        warningView?.isHidden = valid
    }
    
    func obtainPhoneNumberWithCorrectFormat(from phoneText: String?) -> String? {
        
        guard let phoneString = phoneText else { return nil }
        
        let phoneTextWithPrefix = phonePrefix + phoneString
        return phoneTextWithPrefix.trimAll(exeptFor: CharacterSet.decimalDigits.inverted)
    }
    
    func setupGestures() {
        scrollView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(userDidTapOnView)))
        deviceTypeStackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(respondDeviceTypeStackView)))
        deviceManufacturerStackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(respondDeviceManufacturerStackView)))
        deviceModelStackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(respondDeviceModelStackView)))
        problemStackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(respondProblemStackView)))
        usernameStackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(respondUsernameStackView)))
        phoneStackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(respondPhoneStackView)))
        addressStackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(respondAddressStackView)))
    }
    
    func setupKeyboardNotifications() {
        
        notificationCenter.addObserver(self,
                                       selector: #selector(self.keyboardWillShow(with:)),
                                       name: NSNotification.Name.UIKeyboardWillShow,
                                       object: nil)
        
        notificationCenter.addObserver(self,
                                       selector: #selector(self.keyboardWillHide(with:)),
                                       name: NSNotification.Name.UIKeyboardWillHide,
                                       object: nil)
    }
    
    func setupDataPicker(for textField: UITextField, with dataSource: [Any], and tag: Int) -> UIPickerView {
        
        let inputView = DataPickerInputView(frame: CGRect.zero)
        inputView.dataPicker.tag = tag
        inputView.dataPicker.delegate = self
        inputView.setDataSource(with: dataSource)
        
        textField.inputView = inputView
        
        return inputView.dataPicker
    }
    
    func configureCancelButton() {
        
        let attributes: [NSAttributedStringKey : Any] = [NSAttributedStringKey.font : UIFont(customName: .graphikLCGRegular, size: 17) as Any]
        cancelBarButtonItem.setTitleTextAttributes(attributes, for: .normal)
        cancelBarButtonItem.setTitleTextAttributes(attributes, for: .highlighted)
    }
    
    func setupInputAccessoryView() {
        
        let nextButtonToolbar = UIToolbar(frame: CGRect.zero)
        nextButtonToolbar.barStyle = .default
        
        let nextButton = UIBarButtonItem(title: StringsHelper.activateNextTextFieldButtonTitle, style: .plain, target: self, action: #selector(nextButtonPressed))
        nextButton.tintColor = UIColor.black
        
        nextButtonToolbar.items = [UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil), nextButton]
        nextButtonToolbar.sizeToFit()
        
        deviceTypeTextField.inputAccessoryView = nextButtonToolbar
        deviceManufacturerTextField.inputAccessoryView = nextButtonToolbar
        deviceModelTextField.inputAccessoryView = nextButtonToolbar
        problemTextView.inputAccessoryView = nextButtonToolbar
        usernameTextField.inputAccessoryView = nextButtonToolbar
        phoneTextField.inputAccessoryView = nextButtonToolbar
        
        let doneButtonToolbar = UIToolbar(frame: CGRect.zero)
        doneButtonToolbar.barStyle = .default
        
        let doneButton = UIBarButtonItem(title: StringsHelper.deactivateTextFieldButtonTitle, style: .plain, target: self, action: #selector(doneButtonPressed))
        doneButton.tintColor = UIColor.black
        
        doneButtonToolbar.items = [UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil), doneButton]
        doneButtonToolbar.sizeToFit()
        
        addressTextView.inputAccessoryView = doneButtonToolbar
    }
    
    func resetScrollViewInsets() {
        
        guard !isKeyboardOpen else { return }
        
        var insets = UIEdgeInsets.zero
        if #available(iOS 11.0, *) {
            insets.bottom = view.safeAreaInsets.bottom
        }
        
        scrollView.contentInset = insets
        scrollView.scrollIndicatorInsets = insets
    }
    
    func updateHeight(for textView: UITextView, font: UIFont) {
        
        let newSize = textView.sizeThatFits(CGSize(width:textView.width, height: textView.text.sizeOfString(with: font).height))
        
        if textView == problemTextView {
            problemHeightConstraint.constant = newSize.height
        }
        else if textView == addressTextView {
            addressHeightConstraint.constant = newSize.height
        }
    }
    
    func animateTextFieldTitleState(with textLabel: UILabel, view: UIView) {
        
        UIView.animate(withDuration: textFieldsLabelAnimationDuration, animations: { [weak self] in
            
            guard let strongSelf = self else { return }
            
            if #available(iOS 11.0, *) {
                textLabel.layer.anchorPoint = strongSelf.textFieldlabelTransformAnchorPoint
            }
            
            textLabel.transform = strongSelf.textFieldsLabelAffineTransform
            textLabel.x = view.x
        })
    }
    
    func revertTextFieldTitleState(with textLabel: UILabel, view: UIView) {
        
        UIView.animate(withDuration: textFieldsLabelAnimationDuration, animations: { [weak self] in
            
            guard let strongSelf = self else { return }
            
            if #available(iOS 11.0, *) {
                textLabel.layer.anchorPoint = strongSelf.textFieldlabelDefaultAnchorPoint
            }
            
            textLabel.transform = .identity
            textLabel.x = view.x
        })
    }
    
    //MARK: - Observed methods
    
    @objc
    open func userDidTapOnView() {
        self.view.endEditing(true)
    }
    
    @objc
    func respondDeviceTypeStackView() {
        deviceTypeTextField.becomeFirstResponder()
    }
    
    @objc
    func respondDeviceManufacturerStackView() {
        deviceManufacturerTextField.becomeFirstResponder()
    }
    
    @objc
    func respondDeviceModelStackView() {
        deviceModelTextField.becomeFirstResponder()
    }
    
    @objc
    func respondProblemStackView() {
        problemTextView.becomeFirstResponder()
    }
    
    @objc
    func respondUsernameStackView() {
        usernameTextField.becomeFirstResponder()
    }
    
    @objc
    func respondPhoneStackView() {
        phoneTextField.becomeFirstResponder()
    }
    
    @objc
    func respondAddressStackView() {
        addressTextView.becomeFirstResponder()
    }
    
    @objc
    func keyboardWillShow(with notification: Notification) {
        
        guard let info = notification.userInfo,
            let keyboardEndSize = (info[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size
            else { return }
        
        isKeyboardOpen = true
        
        let insets = UIEdgeInsets(top: 0, left: 0, bottom: keyboardEndSize.height, right: 0)
        scrollView.contentInset = insets
        scrollView.scrollIndicatorInsets = insets
    }
    
    @objc
    func keyboardWillHide(with notification: Notification) {
        isKeyboardOpen = false
        resetScrollViewInsets()
    }
    
    @objc
    func nextButtonPressed() {
        
        if deviceTypeTextField.isFirstResponder {
            deviceManufacturerTextField.becomeFirstResponder()
        }
        else if deviceManufacturerTextField.isFirstResponder {
            deviceModelTextField.becomeFirstResponder()
        }
        else if deviceModelTextField.isFirstResponder {
            problemTextView.becomeFirstResponder()
        }
        else if problemTextView.isFirstResponder {
            usernameTextField.becomeFirstResponder()
        }
        else if usernameTextField.isFirstResponder {
            phoneTextField.becomeFirstResponder()
        }
        else if phoneTextField.isFirstResponder {
            addressTextView.becomeFirstResponder()
        }
    }
    
    @objc
    func doneButtonPressed() {
        addressTextView.resignFirstResponder()
    }
}

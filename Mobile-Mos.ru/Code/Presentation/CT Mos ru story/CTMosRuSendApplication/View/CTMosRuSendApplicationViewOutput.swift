//
//  CTMosRuSendApplicationViewOutput.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 06.07.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

protocol CTMosRuSendApplicationViewOutput: class {
    
    func setupInitialState()
    
    func configure(with configuration: CTMosRuSendApplicationModuleConfiguration)
    
    func validatePhoneNumber(number: String) -> Bool
    
    func didSelectInPickerDeviceType(at index: Int)
    
    func didSelectInPickerDeviceManufacturer(at index: Int)
    
    func didPressCancel()
    
    func didPressShowRules()
    
    func didPressConfirm(deviceModel: String?, problem: String?, username: String?, phone: String?, address: String?)
}

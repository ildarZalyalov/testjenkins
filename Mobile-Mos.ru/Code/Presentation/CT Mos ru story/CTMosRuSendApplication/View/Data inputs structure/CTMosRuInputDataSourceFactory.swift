//
//  CTMosRuInputDataSourceFactory.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 06.07.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Конфигурация источника данных для пикера типов устройств
typealias CTMosRuDeviceTypeInputDataSourceConfiguration = (dataSource: [String], selectionItems: [Int : CTMosRuDeviceType])

/// Конфигурация источника данных для пикера производителей устройств
typealias CTMosRuDeviceManufacturerInputDataSourceConfiguration = (dataSource: [String], selectionItems: [Int : CTMosRuDeviceManufacturer])

/// Фабрика источников данных для пикеров
protocol CTMosRuInputDataSourceFactory {
    
    /// Построить источник данных для пикера типов устройств
    ///
    /// - Parameter model: типы устройств
    /// - Returns: конфигурация источника данных
    func buildDeviceTypeDataSource(from model: [CTMosRuDeviceType]) -> CTMosRuDeviceTypeInputDataSourceConfiguration
    
    /// Построить источник данных для пикера производителей устройств
    ///
    /// - Parameter model: производители устройств
    /// - Returns: конфигурация источника данных
    func buildDeviceManufacturerDataSource(from model: [CTMosRuDeviceManufacturer]) -> CTMosRuDeviceManufacturerInputDataSourceConfiguration
}

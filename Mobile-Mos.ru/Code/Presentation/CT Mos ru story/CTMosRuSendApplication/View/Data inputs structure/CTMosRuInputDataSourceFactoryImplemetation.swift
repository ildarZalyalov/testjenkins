//
//  CTMosRuInputDataSourceFactoryImplemetation.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 06.07.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

class CTMosRuInputDataSourceFactoryImplemetation: CTMosRuInputDataSourceFactory {
    
    func buildDeviceTypeDataSource(from model: [CTMosRuDeviceType]) -> CTMosRuDeviceTypeInputDataSourceConfiguration {
        
        var dataSource = [String]()
        var selectionItems = [Int : CTMosRuDeviceType]()
        
        for (index, deviceType) in model.enumerated() {
            dataSource.append(deviceType.title)
            selectionItems[index] = deviceType
        }
        
        return (dataSource, selectionItems)
    }
    
    func buildDeviceManufacturerDataSource(from model: [CTMosRuDeviceManufacturer]) -> CTMosRuDeviceManufacturerInputDataSourceConfiguration {
        
        var dataSource = [String]()
        var selectionItems = [Int : CTMosRuDeviceManufacturer]()
        
        for (index, manufacturer) in model.enumerated() {
            dataSource.append(manufacturer.title)
            selectionItems[index] = manufacturer
        }
        
        return (dataSource, selectionItems)
    }
}

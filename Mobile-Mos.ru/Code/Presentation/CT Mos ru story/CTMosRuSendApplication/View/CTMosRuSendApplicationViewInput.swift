//
//  CTMosRuSendApplicationViewInput.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 06.07.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

protocol CTMosRuSendApplicationViewInput: class {
    
    func displayDeviceTypeValue(value: String)
    
    func displayDeviceManufacturerValue(value: String?)
    
    func highlightFieldsWithErrors()
    
    func setupPickerInputForDeviceType(with dataSource: [String])
    
    func setupPickerInputForDeviceManufacturer(with dataSource: [String])
}

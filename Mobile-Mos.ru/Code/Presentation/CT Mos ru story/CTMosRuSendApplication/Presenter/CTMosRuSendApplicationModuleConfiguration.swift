//
//  CTMosRuSendApplicationModuleConfiguration.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 06.07.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Конфигурация модуля отправки заявки в сервис "Чудо Техники"
struct CTMosRuSendApplicationModuleConfiguration {
    
    /// Тип устройств
    let deviceTypes: [CTMosRuDeviceType]
    
    /// Выбранный тип устройств
    var selectedDeviceType: CTMosRuDeviceType?
}

//
//  CTMosRuSendApplicationPresenter.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 06.07.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

class CTMosRuSendApplicationPresenter: CTMosRuSendApplicationViewOutput, CTMosRuSendApplicationInteractorOutput {
    
    weak var view: CTMosRuSendApplicationViewInput!
    var router: CTMosRuSendApplicationRouterInput!
    var interactor: CTMosRuSendApplicationInteractorInput!
    
    var inputDataFactory: CTMosRuInputDataSourceFactory!
    var deviceTypesByIndex: [Int : CTMosRuDeviceType]!
    var deviceManufacturersByIndex: [Int : CTMosRuDeviceManufacturer]!
    
    var moduleConfiguration: CTMosRuSendApplicationModuleConfiguration!
    var deviceTypes: [CTMosRuDeviceType] {
        return moduleConfiguration.deviceTypes
    }
    
    var selectedDeviceType: CTMosRuDeviceType?
    var selectedDeviceManufaturer: CTMosRuDeviceManufacturer?
    
    var processingBeginTime = Date()
    let processingMinimumDuration: TimeInterval = 1
    
    let minCountOfPhoneCharacters = 11
    
    //MARK: - Хелперы
    
    /// Приводим телефон к формату 7 ### ###-##-##
    func formatPhoneForApplication(phone: String) -> String {
        
        guard phone.count == minCountOfPhoneCharacters else { return phone }
        
        var formatted = phone
        formatted.insert(" ", at: formatted.index(formatted.startIndex, offsetBy: 1))
        formatted.insert(" ", at: formatted.index(formatted.startIndex, offsetBy: 5))
        formatted.insert("-", at: formatted.index(formatted.startIndex, offsetBy: 9))
        formatted.insert("-", at: formatted.index(formatted.startIndex, offsetBy: 12))
        
        return formatted
    }
    
    func perform(after delay: TimeInterval, work: @escaping () -> ()) {
        let deadline: DispatchTime = .now() + delay
        DispatchQueue.main.asyncAfter(deadline: deadline, execute: work)
    }
    
    func canHandleFinishProcessing(with completion: @escaping () -> ()) -> Bool {
        
        let durationDifference = processingBeginTime.timeIntervalSinceNow + processingMinimumDuration
        
        guard durationDifference <= 0 else {
            perform(after: durationDifference, work: completion)
            return false
        }
        
        return true
    }
    
    //MARK: - CTMosRuSendApplicationViewOutput
    
    func setupInitialState() {
        
        let deviceTypeConfiguration = inputDataFactory.buildDeviceTypeDataSource(from: deviceTypes)
        view.setupPickerInputForDeviceType(with: deviceTypeConfiguration.dataSource)
        deviceTypesByIndex = deviceTypeConfiguration.selectionItems
        
        let selectedType = selectedDeviceType ?? deviceTypes.first
        selectedDeviceType = selectedType
        
        if let selectedType = selectedDeviceType {
            view.displayDeviceTypeValue(value: selectedType.title)
            interactor.obtainManufacturers(for: selectedType)
        }
        
        interactor.sendEvent(with: .selectedCTForm(withString: AnalyticsConstants.selectCTForm))
    }
    
    func configure(with configuration: CTMosRuSendApplicationModuleConfiguration) {
        self.moduleConfiguration = configuration
        selectedDeviceType = moduleConfiguration.selectedDeviceType
    }
    
    func validatePhoneNumber(number: String) -> Bool {
        
        guard !number.isEmpty else { return false }
        guard number.count == minCountOfPhoneCharacters else { return false }
        
        return true
    }
    
    func didSelectInPickerDeviceType(at index: Int) {
        
        guard let selectedType = deviceTypesByIndex[index] else { return }
        
        selectedDeviceType = selectedType
        selectedDeviceManufaturer = nil
        
        view.displayDeviceTypeValue(value: selectedType.title)
        view.displayDeviceManufacturerValue(value: nil)
        
        interactor.obtainManufacturers(for: selectedType)
    }
    
    func didSelectInPickerDeviceManufacturer(at index: Int) {
        
        guard let selectedManufacturer = deviceManufacturersByIndex[index] else { return }
        
        selectedDeviceManufaturer = selectedManufacturer
        view.displayDeviceManufacturerValue(value: selectedManufacturer.title)
    }
    
    func didPressCancel() {
        router.closeModule(completion: nil)
    }
    
    func didPressShowRules() {
        router.showRulesModule()
    }
    
    func didPressConfirm(deviceModel: String?, problem: String?, username: String?, phone: String?, address: String?) {
        
        let validateText: (String?) -> Bool = { text in
            guard text != nil else { return false }
            return !text!.isEmpty
        }
        
        guard selectedDeviceType != nil && selectedDeviceManufaturer != nil && validateText(deviceModel) && validateText(problem) && validateText(username) && validateText(phone) && validateText(address) else {
            view.highlightFieldsWithErrors()
            router.showErrorModule(with: StringsHelper.ctMosRuEmptyFieldsMessage)
            return
        }
        
        guard validatePhoneNumber(number: phone!) else {
            view.highlightFieldsWithErrors()
            router.showErrorModule(with: StringsHelper.ctMosRuPhoneErrorMessage)
            return
        }
        
        let formattedPhone = formatPhoneForApplication(phone: phone!)
        let application = CTMosRuRepairApplication(deviceType: selectedDeviceType!.value.rawValue, deviceManufacturer: selectedDeviceManufaturer!.value, deviceModel: deviceModel!, description: problem!, userName: username!, phone: formattedPhone, address: address!)
        
        processingBeginTime = Date()
        router.showProcessingModule()
        interactor.sendApplication(application)
    }
    
    //MARK: - CTMosRuSendApplicationInteractorOutput
    
    func didFinishObtainingManufacturers(with manufacturers: [CTMosRuDeviceManufacturer]) {
        
        let configuration = inputDataFactory.buildDeviceManufacturerDataSource(from: manufacturers)
        
        view.setupPickerInputForDeviceManufacturer(with: configuration.dataSource)
        deviceManufacturersByIndex = configuration.selectionItems
    }
    
    func didFinishSendingApplication(with result: CTMosRuSendRepairApplicationResult) {
        
        guard canHandleFinishProcessing(with: {
            self.didFinishSendingApplication(with: result)
        }) else { return }

        switch result {
        case .success:
            
            let routerReference = router
            
            router.closeModule {
                routerReference?.showSuccessModule()
            }
        case .failure(let error):
            
            router.hideProcessingModule(completion: nil)
            
            if error.isNoInternetConnectionError() {
                router.showErrorModule(with: StringsHelper.noInternetConnectionErrorText)
            }
            else {
                router.showErrorModule(with: StringsHelper.ctMosRuUnknownErrorMessage)
            }
        }
    }
}

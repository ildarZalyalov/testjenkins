//
//  CTMosRuSendApplicationModuleConfigurator.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 06.07.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation
import UIKit

class CTMosRuSendApplicationModuleConfigurator: BaseModuleConfigurator {
    
    override func configureModule(for view: UIViewController) {
        
        let viewController = view as! CTMosRuSendApplicationViewController
        let presenter = CTMosRuSendApplicationPresenter()
        let interactor = CTMosRuSendApplicationInteractor()
        let router = CTMosRuSendApplicationRouter()
        
        let service = UIApplication.shared.serviceBuilder.getCTMosRuService()
        
        let notificationCenter = NotificationCenter.default
        let inputDataFactory = CTMosRuInputDataSourceFactoryImplemetation()
        let textFieldFormatManager = TextFieldFormatManagerImplementation()
        let commonAlertsFactory = CommonAlertsFactoryImplementation()
        let storyboardFactory = StoryboardFactoryImplementation()
        let analyticsManager  = AnalyticsManagerBuilder.getAnalyticsManager()
        
        viewController.output = presenter
        viewController.notificationCenter = notificationCenter
        viewController.phoneTextFieldFormatManager = textFieldFormatManager
        
        presenter.view = viewController
        presenter.interactor = interactor
        presenter.router = router
        presenter.inputDataFactory = inputDataFactory
        
        interactor.output = presenter
        interactor.service = service
        interactor.analyticsManager = analyticsManager
        
        router.transitionHandler = viewController
        router.alertsTransitionHandler = viewController
        router.successModuleTransitionHandler = UIApplication.shared.delegate?.window??.rootViewController
        router.commonAlertsFactory = commonAlertsFactory
        router.storyboardFactory = storyboardFactory
    }
}

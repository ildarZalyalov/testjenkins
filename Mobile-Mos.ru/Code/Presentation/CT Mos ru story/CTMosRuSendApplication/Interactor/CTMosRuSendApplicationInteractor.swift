//
//  CTMosRuSendApplicationInteractor.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 06.07.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

class CTMosRuSendApplicationInteractor: CTMosRuSendApplicationInteractorInput {
    
    weak var output: CTMosRuSendApplicationInteractorOutput!
    var service: CTMosRuService!
    var analyticsManager: AnalyticsManager!
    var currentRequest: HTTPRequest?
    
    deinit {
        currentRequest?.cancel()
    }
    
    //MARK: - CTMosRuSendApplicationInteractorInput
    
    func obtainManufacturers(for deviceType: CTMosRuDeviceType) {
        
        typealias DeviceManufacturer = (CTMosRuDeviceManufacturer, Set<CTMosRuDeviceType.Value>)
        
        let manufacturers: [DeviceManufacturer] = [
            (CTMosRuDeviceManufacturer(title: "Acer", value: "Acer"), Set<CTMosRuDeviceType.Value>([.phone, .pad, .notebook, .computer, .unibodyComputer])),
            (CTMosRuDeviceManufacturer(title: "Alcatel", value: "Alcatel"), Set<CTMosRuDeviceType.Value>([.phone, .pad, .computer])),
            (CTMosRuDeviceManufacturer(title: "Apple", value: "Apple"), Set<CTMosRuDeviceType.Value>([.phone, .pad, .notebook, .computer, .unibodyComputer])),
            (CTMosRuDeviceManufacturer(title: "ASUS", value: "ASUS"), Set<CTMosRuDeviceType.Value>([.phone, .pad, .notebook, .computer, .unibodyComputer])),
            (CTMosRuDeviceManufacturer(title: "BlackBerry", value: "BlackBerry"), Set<CTMosRuDeviceType.Value>([.phone, .pad, .computer])),
            (CTMosRuDeviceManufacturer(title: "Canon", value: "Canon"), Set<CTMosRuDeviceType.Value>([.printer])),
            (CTMosRuDeviceManufacturer(title: "DELL", value: "DELL"), Set<CTMosRuDeviceType.Value>([.phone, .pad, .notebook, .computer, .unibodyComputer])),
            (CTMosRuDeviceManufacturer(title: "DIGMA", value: "DIGMA"), Set<CTMosRuDeviceType.Value>([.phone, .pad, .computer])),
            (CTMosRuDeviceManufacturer(title: "Emachines", value: "Emachines"), Set<CTMosRuDeviceType.Value>([.phone, .pad, .notebook, .computer, .unibodyComputer])),
            (CTMosRuDeviceManufacturer(title: "Fly", value: "Fly"), Set<CTMosRuDeviceType.Value>([.phone, .pad, .computer, .unibodyComputer])),
            (CTMosRuDeviceManufacturer(title: "Fujitsu", value: "Fujitsu"), Set<CTMosRuDeviceType.Value>([.notebook, .computer, .unibodyComputer])),
            (CTMosRuDeviceManufacturer(title: "HP", value: "HP"), Set<CTMosRuDeviceType.Value>([.phone, .pad, .notebook, .computer, .unibodyComputer, .printer])),
            (CTMosRuDeviceManufacturer(title: "HTC", value: "HTC"), Set<CTMosRuDeviceType.Value>([.phone, .pad, .computer])),
            (CTMosRuDeviceManufacturer(title: "Huawei", value: "Huawei"), Set<CTMosRuDeviceType.Value>([.phone, .pad, .computer])),
            (CTMosRuDeviceManufacturer(title: "KonicaMinolta", value: "KonicaMinolta"), Set<CTMosRuDeviceType.Value>([.printer])),
            (CTMosRuDeviceManufacturer(title: "Kyocera", value: "Kyocera"), Set<CTMosRuDeviceType.Value>([.printer])),
            (CTMosRuDeviceManufacturer(title: "Lenovo", value: "Lenovo"), Set<CTMosRuDeviceType.Value>([.phone, .pad, .notebook, .computer, .unibodyComputer])),
            (CTMosRuDeviceManufacturer(title: "LG", value: "LG"), Set<CTMosRuDeviceType.Value>([.phone, .pad, .notebook, .computer, .unibodyComputer])),
            (CTMosRuDeviceManufacturer(title: "Meizu", value: "Meizu"), Set<CTMosRuDeviceType.Value>([.phone, .pad, .computer])),
            (CTMosRuDeviceManufacturer(title: "Microsoft", value: "Microsoft"), Set<CTMosRuDeviceType.Value>([.phone])),
            (CTMosRuDeviceManufacturer(title: "Motorola", value: "Motorola"), Set<CTMosRuDeviceType.Value>([.phone, .pad])),
            (CTMosRuDeviceManufacturer(title: "MSI", value: "MSI"), Set<CTMosRuDeviceType.Value>([.pad, .notebook, .computer, .unibodyComputer])),
            (CTMosRuDeviceManufacturer(title: "Nokia", value: "Nokia"), Set<CTMosRuDeviceType.Value>([.phone, .pad])),
            (CTMosRuDeviceManufacturer(title: "Oki", value: "Oki"), Set<CTMosRuDeviceType.Value>([.printer])),
            (CTMosRuDeviceManufacturer(title: "Packard Bell", value: "Packard Bell"), Set<CTMosRuDeviceType.Value>([.phone, .pad, .notebook, .computer, .unibodyComputer])),
            (CTMosRuDeviceManufacturer(title: "Philips", value: "Philips"), Set<CTMosRuDeviceType.Value>([.phone])),
            (CTMosRuDeviceManufacturer(title: "Ricoh", value: "Ricoh"), Set<CTMosRuDeviceType.Value>([.printer])),
            (CTMosRuDeviceManufacturer(title: "Samsung", value: "Samsung"), Set<CTMosRuDeviceType.Value>([.phone, .pad, .notebook, .computer, .unibodyComputer, .printer])),
            (CTMosRuDeviceManufacturer(title: "SONY", value: "SONY"), Set<CTMosRuDeviceType.Value>([.phone, .pad, .notebook, .computer, .unibodyComputer])),
            (CTMosRuDeviceManufacturer(title: "Sony Xperia", value: "Sony Xperia"), Set<CTMosRuDeviceType.Value>([.phone, .pad])),
            (CTMosRuDeviceManufacturer(title: "Toshiba", value: "Toshiba"), Set<CTMosRuDeviceType.Value>([.notebook, .computer, .unibodyComputer, .printer])),
            (CTMosRuDeviceManufacturer(title: "Xerox", value: "Xerox"), Set<CTMosRuDeviceType.Value>([.printer])),
            (CTMosRuDeviceManufacturer(title: "Xiaomi", value: "Xiaomi"), Set<CTMosRuDeviceType.Value>([.phone, .pad, .notebook, .computer])),
            (CTMosRuDeviceManufacturer(title: "YotaPhone", value: "YotaPhone"), Set<CTMosRuDeviceType.Value>([.phone])),
            (CTMosRuDeviceManufacturer(title: "ZTE", value: "ZTE"), Set<CTMosRuDeviceType.Value>([.phone, .pad])),
            (CTMosRuDeviceManufacturer(title: "4Good", value: "4Good"), Set<CTMosRuDeviceType.Value>([.notebook, .computer, .unibodyComputer])),
            (CTMosRuDeviceManufacturer(title: "Другое", value: "Другое"), Set<CTMosRuDeviceType.Value>([.phone, .pad, .notebook, .computer, .unibodyComputer, .printer]))
        ]
        
        let filteredManufacturers = manufacturers.lazy.filter({ $0.1.contains(deviceType.value) }).map({ $0.0 })
        output.didFinishObtainingManufacturers(with: Array(filteredManufacturers))
    }
    
    func sendEvent(with event: AppsAndServecesEvents) {
        analyticsManager.sendAppsAndServicesEvent(with: event, appName: nil, error: nil)
    }
    
    func sendApplication(_ application: CTMosRuRepairApplication) {
        currentRequest = service.sendRepairApplication(application: application) { [weak self] result in
            
            switch result {
            case .success:
                self?.analyticsManager.sendAppsAndServicesEvent(with: .sendCTForm(withString: AnalyticsConstants.sendCTForm), appName: nil, error: nil)
            case .failure(let error):
                if error.isNoInternetConnectionError() {
                    self?.analyticsManager.sendAppsAndServicesEvent(with: .sendCTForm(withString: AnalyticsConstants.sendCTForm), appName: nil, error: StringsHelper.noInternetConnectionErrorText)
                }
                else {
                    self?.analyticsManager.sendAppsAndServicesEvent(with: .sendCTForm(withString: AnalyticsConstants.sendCTForm), appName: nil, error: StringsHelper.ctMosRuUnknownErrorMessage)
                }
            }
            
            self?.output.didFinishSendingApplication(with: result)
        }
    }
}

//
//  CTMosRuSendApplicationInteractorOutput.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 06.07.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

protocol CTMosRuSendApplicationInteractorOutput: class {
    
    func didFinishObtainingManufacturers(with manufacturers: [CTMosRuDeviceManufacturer])
    
    func didFinishSendingApplication(with result: CTMosRuSendRepairApplicationResult)
}

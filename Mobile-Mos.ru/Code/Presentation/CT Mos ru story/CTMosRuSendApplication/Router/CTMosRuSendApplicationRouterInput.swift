//
//  CTMosRuSendApplicationRouterInput.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 06.07.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

protocol CTMosRuSendApplicationRouterInput: class {

    func closeModule(completion: (() -> ())?)
    
    func showRulesModule()
    
    func showErrorModule(with message: String)
    
    func showProcessingModule()
    
    func hideProcessingModule(completion: (() -> ())?)
    
    func showSuccessModule()
}

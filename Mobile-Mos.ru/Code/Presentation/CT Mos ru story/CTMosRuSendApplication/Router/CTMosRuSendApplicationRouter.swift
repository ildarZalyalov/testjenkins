//
//  CTMosRuSendApplicationRouter.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 06.07.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation
import SafariServices

class CTMosRuSendApplicationRouter: CTMosRuSendApplicationRouterInput {
    
    weak var transitionHandler: UIViewController!
    weak var alertsTransitionHandler: UIViewController!
    weak var successModuleTransitionHandler: UIViewController!
    
    var commonAlertsFactory: CommonAlertsFactory!
    var storyboardFactory: StoryboardFactory!
    
    let unwindToServicesAndAppsSegueIdentifier = "unwindToServicesAndApps"
    
    //MARK: - CTMosRuSendApplicationRouterInput
    
    func closeModule(completion: (() -> ())?) {
        transitionHandler.performSegue(withIdentifier: unwindToServicesAndAppsSegueIdentifier, sender: nil)
        transitionHandler.transitionCoordinator?.animate(alongsideTransition: nil, completion: { _ in
            completion?()
        })
    }
    
    func showRulesModule() {
        
        let safariController = SFSafariViewController(url: ctMosRuRulesUrl, entersReaderIfAvailable: true)
        if #available(iOS 10.0, *) {
            safariController.preferredControlTintColor = UIColorPalette.browserControlsDefaultTintColor
        }
        
        transitionHandler.present(safariController, animated: true, completion: nil)
    }
    
    func showErrorModule(with message: String) {
        let alert = commonAlertsFactory.getErrorAlert(with: message)
        alertsTransitionHandler.present(alert, animated: true, completion: nil)
    }
    
    func showProcessingModule() {
        
        let storyboard = storyboardFactory.getStoryboard(with: .ctMosRu)
        let processingController = storyboard.instantiateViewController(for: .ctMosRuApplicationProcessing)
        
        processingController.modalPresentationStyle = .overCurrentContext
        processingController.modalTransitionStyle = .crossDissolve
        processingController.modalPresentationCapturesStatusBarAppearance = false
        
        transitionHandler.definesPresentationContext = true
        transitionHandler.present(processingController, animated: true, completion: nil)
    }
    
    func hideProcessingModule(completion: (() -> ())?) {
        transitionHandler.dismiss(animated: true, completion: completion)
        transitionHandler.definesPresentationContext = false
    }
    
    func showSuccessModule() {
        
        let storyboard = storyboardFactory.getStoryboard(with: .ctMosRu)
        let successController = storyboard.instantiateViewController(for: .ctMosRuApplicationSendSuccess)
        
        successController.modalPresentationStyle = .overFullScreen
        successController.modalTransitionStyle = .crossDissolve
        successController.modalPresentationCapturesStatusBarAppearance = false
        
        successModuleTransitionHandler.present(successController, animated: true, completion: nil)
    }
}

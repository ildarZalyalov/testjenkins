//
//  CTMosRuApplicationProcessing.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 06.07.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

class CTMosRuApplicationProcessing: UIViewController {
    
    @IBOutlet weak var animationLabel: UILabel!
    
    /// Длительность шага анимации (сколько отображается один из текстов)
    let animationStepDuration: TimeInterval = 0.3
    
    /// Тексты для анимации
    let animationTexts: [String] = ["🕛", "🕐", "🕑", "🕒", "🕓", "🕔", "🕕", "🕖", "🕗", "🕘", "🕙", "🕚"]
    
    /// Аниматор
    fileprivate lazy var animationPerformer: DelayedSelectorPerformer = {
        return DelayedSelectorPerformer(target: self, selector: #selector(updateAnimationText))
    }()
    
    /// Показывается ли анимация
    var isShowingAnimation: Bool = false
    
    /// Текущий индекс в массиве текстов для анимации
    var animationTextIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        startAnimation()
    }
    
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    //MARK: - Приватные методы
    
    func startAnimation() {
        
        guard !isShowingAnimation else { return }
        
        isShowingAnimation = true
        animationTextIndex = 0
        
        updateAnimationText()
    }
    
    func stopAnimation() {
        
        guard isShowingAnimation else { return }
        
        isShowingAnimation = false
        animationPerformer.cancelDelayedPerform()
    }
    
    @objc
    fileprivate func updateAnimationText() {
        
        guard isShowingAnimation else { return }
        
        animationLabel.text = animationTexts[animationTextIndex]
        
        animationTextIndex += 1
        if animationTextIndex == animationTexts.count {
            animationTextIndex = 0
        }
        
        animationPerformer.perform(afterDelay: animationStepDuration)
    }
}

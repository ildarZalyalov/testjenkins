//
//  CTMosRuChooseDeviceTypePresenter.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 06.07.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

class CTMosRuChooseDeviceTypePresenter: CTMosRuChooseDeviceTypeViewOutput, CTMosRuChooseDeviceTypeInteractorOutput {
    
    weak var view: CTMosRuChooseDeviceTypeViewInput!
    var router: CTMosRuChooseDeviceTypeRouterInput!
    var interactor: CTMosRuChooseDeviceTypeInteractorInput!
    
    var deviceTypeFactory: CTMosRuDeviceTypeDataSourceFactory!
    var deviceTypesByItem: [String : CTMosRuDeviceType]!
    
    var deviceTypes: [CTMosRuDeviceType]!
    var selectedDeviceType: CTMosRuDeviceType?
    
    //MARK: - CTMosRuChooseDeviceTypeViewOutput
    
    func setupInitialState() {
        interactor.obtainDeviceTypes()
    }
    
    func didPressCancel() {
        router.closeModule()
    }
    
    func didPressMoreInfo() {
        router.showSafariViewController(with: ctMosRuServiceViewModel.linkFromApp)
    }
    
    func didPressConfirm() {
        guard selectedDeviceType != nil else { return }
        router.showSendApplicationModule(with: deviceTypes, selectedType: selectedDeviceType)
    }
    
    func didSelectDeviceType(with itemId: String) {
        selectedDeviceType = deviceTypesByItem[itemId]
        view.displayConfirmView()
    }
    
    //MARK: - CTMosRuChooseDeviceTypeInteractorOutput
    
    func didFinishObtainingDeviceTypes(with types: [CTMosRuDeviceType]) {
        
        deviceTypes = types
        
        let configuration = deviceTypeFactory.buildDataSourceConfiguration(from: types)
        
        deviceTypesByItem = configuration.selectionItems
        view.displayDeviceTypes(with: configuration.dataSource)
    }
}

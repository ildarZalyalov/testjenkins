//
//  CTMosRuChooseDeviceTypeRouterInput.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 06.07.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

protocol CTMosRuChooseDeviceTypeRouterInput: class {

    func closeModule()
    
    func showSafariViewController(with websiteString: String)
    
    func showSendApplicationModule(with deviceTypes: [CTMosRuDeviceType], selectedType: CTMosRuDeviceType?)
}

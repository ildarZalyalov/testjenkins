//
//  CTMosRuChooseDeviceTypeRouter.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 06.07.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation
import SafariServices

class CTMosRuChooseDeviceTypeRouter: CTMosRuChooseDeviceTypeRouterInput {
    
    weak var transitionHandler: UIViewController!
    weak var toOtherModulesTransitionHandler: DataTransferModuleController!
    
    //MARK: - CTMosRuChooseDeviceTypeRouterInput
    
    let unwindToServicesAndAppsSegueIdentifier = "unwindToServicesAndApps"
    let showSendApplicationSegueIdentifier = "showSendApplication"
    
    func closeModule() {
        transitionHandler.performSegue(withIdentifier: unwindToServicesAndAppsSegueIdentifier, sender: nil)
    }
    
    func showSafariViewController(with websiteString: String) {
        
        guard let websiteUrl = URL(string: websiteString) else {
            return
        }
        
        let safariController = SFSafariViewController(url: websiteUrl, entersReaderIfAvailable: true)
        if #available(iOS 10.0, *) {
            safariController.preferredControlTintColor = UIColorPalette.browserControlsDefaultTintColor
        }
        
        transitionHandler.present(safariController, animated: true, completion: nil)
    }
    
    func showSendApplicationModule(with deviceTypes: [CTMosRuDeviceType], selectedType: CTMosRuDeviceType?) {
        toOtherModulesTransitionHandler.performSegue(with: showSendApplicationSegueIdentifier, sender: nil) { controller in
            let configuration = CTMosRuSendApplicationModuleConfiguration(deviceTypes: deviceTypes, selectedDeviceType: selectedType)
            controller.configureModule(with: configuration)
        }
    }
}

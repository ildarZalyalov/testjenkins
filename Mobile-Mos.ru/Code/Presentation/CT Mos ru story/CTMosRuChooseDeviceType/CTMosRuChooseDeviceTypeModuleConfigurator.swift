//
//  CTMosRuChooseDeviceTypeModuleConfigurator.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 06.07.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation
import UIKit

class CTMosRuChooseDeviceTypeModuleConfigurator: BaseModuleConfigurator {
    
    override func configureModule(for view: UIViewController) {
        
        let viewController = view as! CTMosRuChooseDeviceTypeViewController
        let presenter = CTMosRuChooseDeviceTypePresenter()
        let interactor = CTMosRuChooseDeviceTypeInteractor()
        let router = CTMosRuChooseDeviceTypeRouter()
        
        let deviceTypeFactory = CTMosRuDeviceTypeDataSourceFactoryImplementation()
        
        viewController.output = presenter
        
        presenter.view = viewController
        presenter.interactor = interactor
        presenter.router = router
        presenter.deviceTypeFactory = deviceTypeFactory
        
        interactor.output = presenter
        
        router.transitionHandler = viewController
        router.toOtherModulesTransitionHandler = viewController
    }
}

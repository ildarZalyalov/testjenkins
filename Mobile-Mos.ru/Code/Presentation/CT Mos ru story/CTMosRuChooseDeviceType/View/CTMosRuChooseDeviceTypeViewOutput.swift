//
//  CTMosRuChooseDeviceTypeViewOutput.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 06.07.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

protocol CTMosRuChooseDeviceTypeViewOutput: class {
    
    func setupInitialState()
    
    func didPressCancel()
    
    func didPressMoreInfo()
    
    func didPressConfirm()
    
    func didSelectDeviceType(with itemId: String)
}

//
//  CTMosRuChooseDeviceTypeViewController.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 06.07.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import UIKit

class CTMosRuChooseDeviceTypeViewController: BaseViewController, CTMosRuChooseDeviceTypeViewInput, NavigationBarCustomizingController, UICollectionViewDelegate {
    
    var output: CTMosRuChooseDeviceTypeViewOutput!
    
    @IBOutlet weak var cancelBarButtonItem: UIBarButtonItem!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var deviceTypesCollectionView: UICollectionView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var confirmView: UIView!
    
    @IBOutlet weak var confirmViewBottomConstraint: NSLayoutConstraint!
    
    var collectionViewDataDisplayManager: CollectionViewDataSource!
    
    let confirmViewAnimationDuration: TimeInterval = 0.3
    let descriptionLabelLineHeightMultiple: CGFloat = 1.41
    
    //MARK: - Методы
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        configureCancelButton()
        apply(lineHeightMultiple: descriptionLabelLineHeightMultiple, to: descriptionLabel)
        
        confirmView.isHidden = true
        confirmViewBottomConstraint.constant = -confirmView.height
        
        output.setupInitialState()
    }
    
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    @IBAction func cancelPressed() {
        output.didPressCancel()
    }
    
    @IBAction func moreInfoPressed() {
        output.didPressMoreInfo()
    }
    
    @IBAction func confirmPressed() {
        output.didPressConfirm()
    }
    
    @available(iOS 11.0, *)
    override func viewSafeAreaInsetsDidChange() {
        
        guard confirmView.isHidden else { return }
        
        confirmViewBottomConstraint.constant -= view.safeAreaInsets.bottom
        
        let insets = UIEdgeInsets(top: 0, left: 0, bottom: view.safeAreaInsets.bottom, right: 0)
        scrollView.contentInset = insets
        scrollView.scrollIndicatorInsets = insets
    }
    
    //MARK: - NavigationBarCustomizingController
    
    var navigationBarState: NavigationBarState? {
        
        var state = NavigationBarState()
        
        state.isHidden = false
        state.isTranslucent = false
        state.backgroundState = NavigationBarBackgroundState.regular
        state.tintColor = UIColor.black
        state.isShadowHidden = true
        
        let titleTextAttributes = [
            NSAttributedStringKey.font : UIFont(customName: .graphikLCGBold, size: 15) as Any,
            NSAttributedStringKey.foregroundColor : UIColor.black
        ]
        let largeTitleTextAttributes = [
            NSAttributedStringKey.font : UIFont(customName: .graphikLCGBold, size: 25) as Any,
            NSAttributedStringKey.foregroundColor : UIColorPalette.conversationsPageTitleColor
        ]
        
        state.titleTextAttributes = titleTextAttributes
        state.largeTitleTextAttributes = largeTitleTextAttributes
        
        return state
    }
    
    //MARK: - Хелперы
    
    func configureCancelButton() {
        
        let attributes: [NSAttributedStringKey : Any] = [NSAttributedStringKey.font : UIFont(customName: .graphikLCGRegular, size: 17) as Any]
        
        cancelBarButtonItem.setTitleTextAttributes(attributes, for: .normal)
        cancelBarButtonItem.setTitleTextAttributes(attributes, for: .highlighted)
    }
    
    func apply(lineHeightMultiple: CGFloat, to label: UILabel) {
        
        guard let text = label.text else { return }
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineHeightMultiple = lineHeightMultiple
        paragraphStyle.alignment = .left
        paragraphStyle.lineBreakMode = .byTruncatingTail
        
        let attributes: [NSAttributedStringKey : Any] = [NSAttributedStringKey.font : label.font, NSAttributedStringKey.paragraphStyle : paragraphStyle]
        let valueString = NSAttributedString(string: text, attributes: attributes)
        
        label.attributedText = valueString
    }
    
    //MARK: - CTMosRuChooseDeviceTypeViewInput
    
    func displayDeviceTypes(with dataSource: CollectionViewDataSource) {
        
        collectionViewDataDisplayManager = dataSource
        collectionViewDataDisplayManager.delegate = self
        collectionViewDataDisplayManager.assign(to: deviceTypesCollectionView)
        
        deviceTypesCollectionView.reloadData()
    }
    
    func displayConfirmView() {
        
        confirmView.isHidden = false
        confirmViewBottomConstraint.constant = 0
        
        UIView.animate(withDuration: confirmViewAnimationDuration, animations: { [weak self] in
            self?.view.layoutIfNeeded()
        }) { [weak self] _ in
            
            guard let strongSelf = self else { return }
            
            let bottomInset = strongSelf.confirmView.height
            let insets = UIEdgeInsets(top: 0, left: 0, bottom: bottomInset, right: 0)
            
            strongSelf.scrollView.contentInset = insets
            strongSelf.scrollView.scrollIndicatorInsets = insets
        }
    }
    
    //MARK: - UICollectionViewDelegate
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        guard let cellObject = collectionViewDataDisplayManager.dataStructure.cellObject(at: indexPath) as? CTMosRuDeviceTypeCellObject else { return }
        
        output.didSelectDeviceType(with: cellObject.itemId)
    }
}

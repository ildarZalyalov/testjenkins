//
//  CTMosRuChooseDeviceTypeViewInput.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 06.07.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

protocol CTMosRuChooseDeviceTypeViewInput: class {
    
    func displayDeviceTypes(with dataSource: CollectionViewDataSource)
    
    func displayConfirmView()
}

//
//  CTMosRuDeviceTypeCell.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 06.07.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import UIKit

class CTMosRuDeviceTypeCell: UICollectionViewCell, ConfigurableView {
    
    @IBOutlet weak var iconView: UIImageView!
    @IBOutlet weak var itemTitleLabel: UILabel!
    @IBOutlet weak var itemTitleLabelBackgroundView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        itemTitleLabel.textColor = UIColorPalette.ctMosRuDeviceTypeTitleColor
    }
    
    override var isSelected: Bool {
        didSet {
            itemTitleLabel.textColor = isSelected ? UIColorPalette.ctMosRuSelectedDeviceTypeTitleColor : UIColorPalette.ctMosRuDeviceTypeTitleColor
            itemTitleLabelBackgroundView.backgroundColor = isSelected ? UIColorPalette.ctMosRuSelectedDeviceTypeTitleBackgroundColor : UIColor.clear
        }
    }
    
    //MARK: - ConfigurableView
    
    func configure(with object: Any) {
        
        guard let cellObject = object as? CTMosRuDeviceTypeCellObject else {
            return
        }
        
        iconView.image = cellObject.icon
        itemTitleLabel.text = cellObject.title
    }
}

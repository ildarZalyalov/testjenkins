//
//  CTMosRuDeviceTypeCellObject.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 06.07.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

struct CTMosRuDeviceTypeCellObject: CellObjectWithId {
    
    var itemId: String
    
    var icon: UIImage?
    
    var title: String
}

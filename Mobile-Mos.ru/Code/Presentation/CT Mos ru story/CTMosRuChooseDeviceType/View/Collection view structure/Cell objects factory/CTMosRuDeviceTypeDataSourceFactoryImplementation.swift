//
//  CTMosRuDeviceTypeDataSourceFactoryImplementation.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 06.07.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

class CTMosRuDeviceTypeDataSourceFactoryImplementation: CTMosRuDeviceTypeDataSourceFactory {
    
    //MARK: - CTMosRuDeviceTypeDataSourceFactory
    
    func buildDataSourceConfiguration(from model: [CTMosRuDeviceType]) -> CTMosRuDeviceTypeDataSourceConfiguration {
        
        let dataStructure = CollectionViewDataSourceStructure()
        var selectionItems = [String : CTMosRuDeviceType]()
        
        dataStructure.appendSection()
        
        for (index, deviceType) in model.enumerated() {
            
            let cellObject = CTMosRuDeviceTypeCellObject(itemId: String(index), icon: deviceType.icon, title: deviceType.title)
            
            dataStructure.appendCellObject(cellObject)
            selectionItems[cellObject.itemId] = deviceType
        }
        
        let dataSource = CollectionViewDataSource(with: dataStructure)
        
        return (dataSource, selectionItems)
    }
}

//
//  CTMosRuDeviceTypeDataSourceFactory.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 06.07.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

typealias CTMosRuDeviceTypeDataSourceConfiguration = (dataSource: CollectionViewDataSource, selectionItems: [String : CTMosRuDeviceType])

protocol CTMosRuDeviceTypeDataSourceFactory {
    
    func buildDataSourceConfiguration(from model: [CTMosRuDeviceType]) -> CTMosRuDeviceTypeDataSourceConfiguration
}

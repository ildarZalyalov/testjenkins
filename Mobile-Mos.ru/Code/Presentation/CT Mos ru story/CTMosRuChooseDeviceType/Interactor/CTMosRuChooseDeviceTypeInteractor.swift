//
//  CTMosRuChooseDeviceTypeInteractor.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 06.07.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

class CTMosRuChooseDeviceTypeInteractor: CTMosRuChooseDeviceTypeInteractorInput {
    
    weak var output: CTMosRuChooseDeviceTypeInteractorOutput!
    
    //MARK: - CTMosRuChooseDeviceTypeInteractorInput
    
    func obtainDeviceTypes() {
        
        let deviceTypes: [CTMosRuDeviceType] = [
            CTMosRuDeviceType(icon: #imageLiteral(resourceName: "ctMosRuIconPhone"), title: "Смартфон", value: .phone),
            CTMosRuDeviceType(icon: #imageLiteral(resourceName: "ctMosRuIconPad"), title: "Планшет", value: .pad),
            CTMosRuDeviceType(icon: #imageLiteral(resourceName: "ctMosRuIconNotebook"), title: "Ноутбук", value: .notebook),
            CTMosRuDeviceType(icon: #imageLiteral(resourceName: "ctMosRuIconUnibodyPC"), title: "Моноблок", value: .unibodyComputer),
            CTMosRuDeviceType(icon: #imageLiteral(resourceName: "ctMosRuIconPC"), title: "Компьютер", value: .computer),
            CTMosRuDeviceType(icon: #imageLiteral(resourceName: "ctMosRuIconPrinter"), title: "Принтер/МФУ", value: .printer)
        ]
        
        output.didFinishObtainingDeviceTypes(with: deviceTypes)
    }
}

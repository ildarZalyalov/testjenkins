//
//  ParallaxScrollHeaderController.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 16.02.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import UIKit

/// Делегат контроллера "растягивающегося" заголовка
protocol ParallaxScrollHeaderControllerDelegate: class {
    
    /// Заголовок был прокручен хотя бы на 1 px
    ///
    /// - Parameter headerController: контроллер заголовка, который был прокручен
    func parallaxHeaderDidScroll(_ headerController: ParallaxScrollHeaderController)
}

/// Контроллер "растягивающегося" заголовка
class ParallaxScrollHeaderController: NSObject {
    
    /// Режим работы заголовка, определяет как тянется содержимое при скролле
    ///
    /// - fill: содержимое заполняет заголовок
    /// - topFill: содержимое заполянет заголовок, но верх не тянется
    /// - top: содержимое "магнитится" к верху
    /// - center: содержимое постоянно центруется
    /// - bottom: содержимое "магнитится" к низу
    enum HeaderMode {
        case fill
        case topFill
        case top
        case center
        case bottom
    }
    
    /// Значение процента скроллинга (progress), когда заголовок открыт на высоту defaultHeight
    let progressDefaultValue: CGFloat = 1

    /// UIScrollView, в которой расположен заголовок
    weak var scrollView: UIScrollView? {
        
        willSet {
            
            if newValue == nil {
                stopKVO()
            }
        }
        
        didSet {
            
            guard let scrollView = scrollView else {
                return
            }
            
            if #available(iOS 11.0, *) {
                scrollView.contentInsetAdjustmentBehavior = .never
            }
            
            adjustScrollViewTopInset(scrollView.contentInset.top + defaultHeight)
            
            scrollView.addSubview(headerViewSuperview)
            stopKVO()
            startKVO()
            
            layoutHeaderSuperview()
            refreshHeaderView()
        }
    }
    
    /// Содержимое заголовка
    var headerView: UIView? {
        didSet {
            refreshHeaderView()
        }
    }
    
    /// Высота заголовка
    var defaultHeight: CGFloat = 0 {
        
        willSet(newHeight) {
            
            guard let scrollView = scrollView else {
                return
            }
            
            let topInset = scrollView.contentInset.top - defaultHeight + newHeight
            adjustScrollViewTopInset(topInset)
        }
        
        didSet {
            refreshHeaderView()
            layoutHeaderSuperview()
        }
    }
    
    /// Минимальная высота заголовка, если больше 0, то он не схлопывается полностью
    var minimumHeight: CGFloat = 0 {
        didSet {
            layoutHeaderSuperview()
        }
    }
    
    /// Текущий режим работы заголовка
    var mode: HeaderMode = .fill {
        didSet {
            refreshHeaderView()
        }
    }

    /// Текущий процент скроллинга
    var progress: CGFloat {
        
        var height = defaultHeight - minimumHeight
        
        if height <= 0 {
            height = defaultHeight
        }
        
        return (headerViewSuperview.frame.size.height - minimumHeight) / height
    }
    
    /// Closure, возвращающий замкнутый UIBezierPath, описывающий маску для содержимого заголовка
    var headerViewMaskingPathCalculator: ((CGRect) -> UIBezierPath)? = nil {
        didSet {
            headerViewSuperview.maskingPathCalculator = headerViewMaskingPathCalculator
        }
    }
    
    /// Делегат
    weak var delegate: ParallaxScrollHeaderControllerDelegate?
    
    fileprivate var headerViewSuperview = BezierPathMaskedView()
    fileprivate let kvoKeyPath = #keyPath(UIScrollView.contentOffset)
    fileprivate var kvoContext = 0
    fileprivate var isKvoActive = false
    
    override init() {
        super.init()
        headerViewSuperview.clipsToBounds = true
    }
    
    //MARK: - Приватные методы
    
    fileprivate func refreshHeaderView() {
        
        guard let view = headerView else {
            return
        }
        
        view.removeFromSuperview()
        headerViewSuperview.addSubview(view)
        
        view.translatesAutoresizingMaskIntoConstraints = false
        
        switch mode {
        case .fill:
            setFillModeConstraints()
        case .topFill:
            setTopFillModeConstraints()
        case .top:
            setTopModeConstraints()
        case .bottom:
            setBottomModeConstraints()
        default:
            setCenterModeConstraints()
        }
    }
    
    fileprivate func layoutHeaderSuperview() {
        
        guard let scrollView = scrollView else {
            return
        }
        
        let minHeight = min(minimumHeight, defaultHeight)
        let relativeYOffset = scrollView.contentOffset.y + scrollView.contentInset.top - defaultHeight
        let relativeHeight = -relativeYOffset
        
        headerViewSuperview.frame = CGRect(
            x: 0,
            y: relativeYOffset,
            width: scrollView.frame.size.width,
            height: max(relativeHeight, minHeight))
    }
    
    fileprivate func adjustScrollViewTopInset(_ topInset: CGFloat) {
        
        guard let scrollView = scrollView else {
            return
        }
        
        var inset = scrollView.contentInset
        
        var offset = scrollView.contentOffset
        offset.y += inset.top - topInset
        scrollView.contentOffset = offset
        
        inset.top = topInset
        scrollView.contentInset = inset
    }
    
    //MARK: - KVO
    
    fileprivate func startKVO() {
        isKvoActive = true
        scrollView?.addObserver(self, forKeyPath: kvoKeyPath, options: .new, context: &kvoContext)
    }
    
    fileprivate func stopKVO() {
        
        guard isKvoActive else {
            return
        }
        
        isKvoActive = false
        scrollView?.removeObserver(self, forKeyPath: kvoKeyPath, context: &kvoContext)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        guard context == &kvoContext && keyPath == kvoKeyPath else {
            super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
            return
        }
        
        layoutHeaderSuperview()
        delegate?.parallaxHeaderDidScroll(self)
    }
    
    //MARK: - Создаем constraints для каждого режима работы заголовка
    
    fileprivate func setCenterModeConstraints() {
        
        guard let view = headerView else {
            return
        }
        
        headerViewSuperview.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[v]|", options: [], metrics: nil, views: ["v" : view]))
        headerViewSuperview.addConstraint(NSLayoutConstraint(item: view, attribute: .centerY, relatedBy: .equal, toItem: headerViewSuperview, attribute: .centerY, multiplier: 1, constant: 0))
        headerViewSuperview.addConstraint(NSLayoutConstraint(item: view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: defaultHeight))
    }
    
    fileprivate func setFillModeConstraints() {
        
        guard let view = headerView else {
            return
        }
        
        let binding = ["v" : view]
        headerViewSuperview.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[v]|", options: [], metrics: nil, views: binding))
        headerViewSuperview.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[v]|", options: [], metrics: nil, views: binding))
    }
    
    fileprivate func setTopFillModeConstraints() {
        
        guard let view = headerView else {
            return
        }
        
        let binding = ["v" : view]
        let metrics: [String : Any] = ["highPriority" : UILayoutPriority.defaultHigh,
                                       "height" : defaultHeight]
        
        headerViewSuperview.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[v]|", options: [], metrics: nil, views: binding))
        headerViewSuperview.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[v(>=height)]-0.0@highPriority-|", options: [], metrics: metrics, views: binding))
    }
    
    fileprivate func setTopModeConstraints() {
        
        guard let view = headerView else {
            return
        }
        
        let binding = ["v" : view]
        let metrics = ["height" : defaultHeight]
        
        headerViewSuperview.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[v]|", options: [], metrics: nil, views: binding))
        headerViewSuperview.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[v(==height)]", options: [], metrics: metrics, views: binding))
    }
    
    fileprivate func setBottomModeConstraints() {
        
        guard let view = headerView else {
            return
        }
        
        let binding = ["v" : view]
        let metrics = ["height" : defaultHeight]
        
        headerViewSuperview.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[v]|", options: [], metrics: nil, views: binding))
        headerViewSuperview.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[v(==height)]|", options: [], metrics: metrics, views: binding))
    }
}

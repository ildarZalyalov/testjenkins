//
//  DatePickerInputView.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 11.01.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// UIInputView с барабаном для выбора даты
class DatePickerInputView: UIInputView {
    
    let datePicker: UIDatePicker = {
        
        let picker = UIDatePicker(frame: CGRect.zero)
        picker.backgroundColor = UIColor.white
        picker.datePickerMode = .date
        
        return picker
    }()
    
    override init(frame: CGRect, inputViewStyle: UIInputViewStyle) {
        
        datePicker.sizeToFit()
        
        var inputFrame = frame
        inputFrame.size.height += datePicker.height
        
        super.init(frame: inputFrame, inputViewStyle: .default)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    fileprivate func commonInit() {
        
        allowsSelfSizing = true
        backgroundColor = UIColor.white
        
        addSubview(datePicker)
        datePicker.translatesAutoresizingMaskIntoConstraints = false
        
        addConstraints(NSLayoutConstraint.leadingTrailingEdgesConstraints(for: datePicker))
        addConstraint(NSLayoutConstraint(item: datePicker, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: 0))
        
        var item: Any?
        if #available(iOS 11, *) {
            item = safeAreaLayoutGuide
        }
        else {
            item = self
        }
        
        addConstraint(NSLayoutConstraint(item: datePicker, attribute: .bottom, relatedBy: .equal, toItem: item, attribute: .bottom, multiplier: 1, constant: 0))
    }
}

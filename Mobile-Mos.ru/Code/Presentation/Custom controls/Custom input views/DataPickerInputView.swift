//
//  DataPickerInputView.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 15.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// UIInputView с барабаном для выбора данных
class DataPickerInputView: UIInputView, UIPickerViewDataSource {
    
    fileprivate var dataSource:[Any]!
    
    let dataPicker: UIPickerView = {
        
        let picker = UIPickerView(frame: CGRect.zero)
        picker.backgroundColor = UIColor.white
        
        return picker
    }()
    
    override init(frame: CGRect, inputViewStyle: UIInputViewStyle) {
        
        dataPicker.sizeToFit()
        
        var inputFrame = frame
        inputFrame.size.height += dataPicker.height
        
        super.init(frame: inputFrame, inputViewStyle: .default)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    fileprivate func commonInit() {
        
        allowsSelfSizing = true
        backgroundColor = UIColor.white
        
        addSubview(dataPicker)
        dataPicker.translatesAutoresizingMaskIntoConstraints = false
        
        addConstraints(NSLayoutConstraint.leadingTrailingEdgesConstraints(for: dataPicker))
        addConstraint(NSLayoutConstraint(item: dataPicker, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: 0))
        
        var item: Any?
        if #available(iOS 11, *) {
            item = safeAreaLayoutGuide
        }
        else {
            item = self
        }
        
        addConstraint(NSLayoutConstraint(item: dataPicker, attribute: .bottom, relatedBy: .equal, toItem: item, attribute: .bottom, multiplier: 1, constant: 0))
    }
    
    func setDataSource(with dataArray: [Any]) {
        dataSource = dataArray
        dataPicker.dataSource = self
    }
    
    //MARK: - Datasource
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return dataSource.count
    }

}

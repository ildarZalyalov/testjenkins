//
//  HorizontalPickerElementCell.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 09.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import UIKit

/// Ячейка для HorizontalPickerView
class HorizontalPickerElementCell: UICollectionViewCell, ConfigurableView {
    
    /// Размер шрифта для элемента пикера
    static let defaultElementsTitleFontSize: CGFloat = 22
    
    /// Размер шрифта для элемента пикера при компактном отображении
    static let compactElementsTitleFontSize: CGFloat = 20
    
    /// Получаем шрифт для элемента пикера
    ///
    /// - Parameter forCompactSize: применять ли шрифт для компактного отображения
    static func elementsTitleFont(forCompactSize: Bool) -> UIFont {
        
        typealias Class = HorizontalPickerElementCell
        
        let fontSize = forCompactSize ? Class.compactElementsTitleFontSize : Class.defaultElementsTitleFontSize
        return UIFont(customName: .graphikLCGRegular, size: fontSize) ?? UIFont.systemFont(ofSize: fontSize)
    }
    
    @IBOutlet weak var elementTitleLabel: UILabel!
    
    override func awakeFromNib() {
        elementTitleLabel.font = HorizontalPickerElementCell.elementsTitleFont(forCompactSize: false)
    }
    
    override func prepareForReuse() {
        elementTitleLabel.font = HorizontalPickerElementCell.elementsTitleFont(forCompactSize: false)
    }
    
    /// Применить шрифт для компактного отображения
    func applyCompactFontSize() {
        elementTitleLabel.font = HorizontalPickerElementCell.elementsTitleFont(forCompactSize: true)
    }
    
    //MARK: - ConfigurableView
    
    func configure(with object: Any) {
        
        guard let cellObject = object as? HorizontalPickerElementCellObject else {
            return
        }
        
        if let attributes = cellObject.titleAttributes {
            let attributedTitle = NSAttributedString(string: cellObject.title, attributes: attributes)
            elementTitleLabel.attributedText = attributedTitle
        }
        else {
            elementTitleLabel.text = cellObject.title
        }
    }
}

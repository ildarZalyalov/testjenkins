//
//  HorizontalPickerElementCellObject.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 09.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Cell object ячейки для HorizontalPickerView
struct HorizontalPickerElementCellObject: CellObjectWithId {
    
    /// Идентификатор элемента
    var itemId: String
    
    /// Заголовок элемента
    var title: String
    
    /// Атрибуты отображения заголовка элемента
    var titleAttributes: [NSAttributedStringKey : Any]? = nil
    
    init(elementId: String, title: String) {
        self.itemId = elementId
        self.title = title
    }
}

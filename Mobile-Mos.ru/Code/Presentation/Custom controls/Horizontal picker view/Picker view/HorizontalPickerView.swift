//
//  HorizontalPickerView.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 09.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import UIKit

/// Делегат пикера с горизонтальной прокруткой
protocol HorizontalPickerViewDelegate: class {
    
    /// В пикере было выбрано значение
    ///
    /// - Parameters:
    ///   - pickerView: пикер, в котором было выбрано значение
    ///   - elementId: идентификатор значения
    func pickerView(_ pickerView: HorizontalPickerView, didSelectElementWithId elementId: String)
}

/// Picker view с горизонтальной прокруткой
class HorizontalPickerView: UIView, UICollectionViewDelegate {
    
    /// Настройка размера пикера (управляет размером текста в пикере, простая алтернатива аттрибутным заголовкам)
    ///
    /// - regular: обычный размер
    /// - compact: компактный размер
    enum ControlSizing {
        case regular
        case compact
    }
    
    fileprivate static let nibFile = UINib(nibName: String(describing: HorizontalPickerView.self), bundle: nil)
    
    /// Загрузить горизонтальный пикер
    static func loadNib() -> HorizontalPickerView {
        return HorizontalPickerView.nibFile.instantiate(withOwner: nil, options: nil).first as! HorizontalPickerView
    }
    
    /// Scroll view, которая обрабатывает touch input
    @IBOutlet fileprivate weak var interactionScrollView: UIScrollView!
    
    /// Лейаут пикера
    @IBOutlet fileprivate weak var collectionViewLayout: HorizontalPickerViewLayout!
    
    /// Collection view с элементами пикера
    @IBOutlet fileprivate weak var collectionView: UICollectionView!
    
    /// Кнопка перехода к предыдущему элементу
    @IBOutlet fileprivate weak var previousButton: UIButton!
    
    /// Кнопка перехода к следующему элементу
    @IBOutlet fileprivate weak var nextButton: UIButton!
    
    /// Ширина scroll view, которая обрабатывает touch input
    @IBOutlet fileprivate weak var interactionScrollViewWidthConstraint: NSLayoutConstraint!
    
    /// Слой фона кнопки перехода к предыдущему элементу
    fileprivate var previousButtonBackgroundLayer: CAGradientLayer?
    
    /// Слой фона кнопки перехода к следующему элементу
    fileprivate var nextButtonBackgroundLayer: CAGradientLayer?
    
    /// Доля ширины фона кнопки, при достижении которой происходит смена цвета градиента
    fileprivate let buttonBackgroundLayerGradientLocation: Double = 0.8
    
    /// Начальная точка градиента фона кнопки (определяет направление градиента)
    fileprivate let buttonBackgroundLayerGradientStartPoint = CGPoint(x: 0, y: 0.5)
    
    /// Конечная точка градиента фона кнопки (определяет направление градиента)
    fileprivate let buttonBackgroundLayerGradientEndPoint = CGPoint(x: 1, y: 0.5)
    
    /// Источник данных для сollection view пикера
    fileprivate var dataDisplayManager: CollectionViewDataSource!
    
    /// Кол-во элементов в пикере
    fileprivate var elementsTotalCount = 0
    
    /// Флаг равен true, если взаимодействие с пикером осуществил пользователь
    fileprivate var collectionViewWasDragged = false
    
    /// Флаг указывает, что не нужно оповещать делегат о взаимодействии с пикером
    fileprivate var doNotAlertDelegateAboutScrollStopped = false
    
    /// Ширина элемента пикера
    fileprivate var itemWidth: CGFloat {
        
        get {
            return collectionViewLayout.itemSize.width
        }
        set {
            
            let elementToSelectId = selectedElementId
            
            var itemSize = collectionViewLayout.itemSize
            itemSize.width = newValue
            collectionViewLayout.itemSize = itemSize
            
            interactionScrollViewWidthConstraint.constant = newValue
            setNeedsLayout()
            layoutIfNeeded()
            
            resetSelectionOffsetAndContentSize()
            recalculateSelectionInsets()
            collectionViewLayout.invalidateLayout()
            
            if let elementId = elementToSelectId {
                selectElement(with: elementId, triggerDelegate: false, animated: false)
                refreshSelectionButtons()
            }
        }
    }
    
    /// Примерная ширина пикера, основанная на текущей ширине пикера
    fileprivate var itemWidthBasedOnPickerWidth: CGFloat {
        
        // добавляем побольше ширины за счет кнопок, чтобы крайние элементы стояли ближе к ним
        // для компактного режима добавляем меньше т.к. не хватает ширины
        let buttonsWidthMultiplier: CGFloat = controlSizing == .compact ? 1 / 4 : 1 / 2
        let availableWidth = collectionView.width + previousButton.width * buttonsWidthMultiplier + nextButton.width * buttonsWidthMultiplier
        
        return availableWidth / 3
    }
    
    /// Index path выбраного элемента пикера
    fileprivate var selectedElementIndexPath: IndexPath? {
        let center = CGPoint(x: collectionView.bounds.midX, y: collectionView.bounds.midY)
        return collectionView.indexPathForItem(at: center)
    }
    
    override func awakeFromNib() {
        
        addPreviousNextButtonsBackground()
        
        // чтобы убрать варнинги о плохом лейауте до показа
        
        var itemSize = collectionViewLayout.itemSize
        itemSize.height = height
        collectionViewLayout.itemSize = itemSize
        
        recalculateSelectionInsets()
        
        collectionView.registerCellNib(for: HorizontalPickerElementCellObject.self)
        addGestureRecognizer(interactionScrollView.panGestureRecognizer)
    }
    
    /// Идентификатор выбранного элемента пикера
    var selectedElementId: String? {
        
        get {
            
            guard let selectedIndexPath = selectedElementIndexPath else { return nil }
            guard let cellObject = dataDisplayManager.dataStructure.cellObject(at: selectedIndexPath) as? HorizontalPickerElementCellObject else { return nil }
            
            return cellObject.itemId
        }
        set {
            guard let elementId = newValue else { return }
            selectElement(with: elementId, animated: false)
        }
    }
    
    /// Индекс выбранного элемента пикера
    var selectedElementIndex: Int? {
        
        get {
            guard let selectedIndexPath = selectedElementIndexPath else { return nil }
            return selectedIndexPath.row
        }
        set {
            
            guard let selectedIndexPath = selectedElementIndexPath else { return }
            guard let index = newValue?.clamped(to: 0 ... elementsTotalCount - 1) else { return }
            
            let newIndexPath = IndexPath(row: index, section: selectedIndexPath.section)
            scrollToElement(at: newIndexPath, animated: false)
        }
    }
    
    /// Настройка размера пикера
    var controlSizing = ControlSizing.regular {
        
        didSet {
            
            guard controlSizing != oldValue else { return }
            
            let minimumElementScale = controlSizing == .compact ? collectionViewLayout.compactElementScale : collectionViewLayout.defaultElementScale
            collectionViewLayout.minimumElementScale = minimumElementScale
            
            collectionView.reloadData()
        }
    }
    
    /// Делегат пикера
    weak var delegate: HorizontalPickerViewDelegate?
    
    override func layoutSubviews() {

        super.layoutSubviews()
        
        previousButtonBackgroundLayer?.frame = previousButton.frame
        nextButtonBackgroundLayer?.frame = nextButton.frame
        
        recalculateSelectionInsets()
        
        if collectionViewLayout.itemSize.height != height {
            
            var itemSize = collectionViewLayout.itemSize
            itemSize.height = height
            collectionViewLayout.itemSize = itemSize
            
            collectionViewLayout.invalidateLayout()
        }
        
        let expectedItemWidth = itemWidthBasedOnPickerWidth
        
        if itemWidth != expectedItemWidth {
            itemWidth = expectedItemWidth
        }
    }
    
    override func didMoveToWindow() {
        
        guard window != nil else { return }
        
        collectionView.reloadData()
        
        recalculateSelectionInsets()
        refreshSelectionButtons()
    }
    
    //MARK: - Actions
    
    /// Выбрать предыдущий элемент
    @IBAction func scrollToPreviousElement() {
        
        guard let selectedIndexPath = selectedElementIndexPath else { return }
        guard selectedIndexPath.row > 0 else { return }
        
        var previousElementIndexPath = selectedIndexPath
        previousElementIndexPath.row -= 1
        
        scrollToElement(at: previousElementIndexPath, animated: true)
    }
    
    /// Выбрать следующий элемент
    @IBAction func scrollToNextElement() {
        
        guard let selectedIndexPath = selectedElementIndexPath else { return }
        guard selectedIndexPath.row < elementsTotalCount - 1 else { return }
        
        var nextItemIndexPath = selectedIndexPath
        nextItemIndexPath.row += 1
        
        scrollToElement(at: nextItemIndexPath, animated: true)
    }
    
    //MARK: - Приватные методы
    
    fileprivate func addPreviousNextButtonsBackground() {
        
        guard let previousButtonSuperlayer = previousButton.layer.superlayer else { return }
        guard let nextButtonSuperlayer = nextButton.layer.superlayer else { return }
        
        guard let color = backgroundColor else { return }
        
        let almostTransparentColor = color.withAlphaComponent(CGFloat.leastNormalMagnitude)
        let gradientLocation = buttonBackgroundLayerGradientLocation
        let startPoint = buttonBackgroundLayerGradientStartPoint
        let endPoint = buttonBackgroundLayerGradientEndPoint
        
        typealias BackgroundLayerConfigurator = (CALayer, CALayer, Bool) -> CAGradientLayer
        let configureBackgroundLayer: BackgroundLayerConfigurator = { superlayer, buttonLayer, inverted in
            
            let backgroundLayer = CAGradientLayer()
            backgroundLayer.colors = [color.cgColor, almostTransparentColor.cgColor]
            backgroundLayer.locations = [NSNumber(value: gradientLocation)]
            backgroundLayer.startPoint = inverted ? endPoint : startPoint
            backgroundLayer.endPoint = inverted ? startPoint : endPoint
            
            superlayer.insertSublayer(backgroundLayer, below: buttonLayer)
            
            return backgroundLayer
        }
        
        previousButtonBackgroundLayer = configureBackgroundLayer(previousButtonSuperlayer,
                                                                 previousButton.layer,
                                                                 false)
        nextButtonBackgroundLayer = configureBackgroundLayer(nextButtonSuperlayer,
                                                                 nextButton.layer,
                                                                 true)
    }
    
    fileprivate func resetSelectionOffsetAndContentSize() {
        
        interactionScrollView.contentSize.width = (collectionViewLayout.itemSize.width + collectionViewLayout.minimumInteritemSpacing) * CGFloat(elementsTotalCount)
        interactionScrollView.setContentOffset(CGPoint.zero, animated: false)
        
        refreshSelectionButtons()
    }
    
    fileprivate func recalculateSelectionInsets() {
        
        let difference = collectionView.width - interactionScrollView.width
        let inset = difference / 2 + collectionViewLayout.minimumInteritemSpacing / 2
        
        collectionViewLayout.sectionInset = UIEdgeInsets(top: 0, left: inset, bottom: 0, right: inset)
    }
    
    fileprivate func refreshSelectionButtons() {
        
        guard let selectedIndexPath = selectedElementIndexPath else { return }
        
        previousButton.isHidden = selectedIndexPath.row == 0
        nextButton.isHidden = selectedIndexPath.row == elementsTotalCount - 1
    }
    
    fileprivate func handleScrollingStopped() {
        
        refreshSelectionButtons()
        
        guard !doNotAlertDelegateAboutScrollStopped else {
            doNotAlertDelegateAboutScrollStopped = false
            return
        }
        
        if let elementId = selectedElementId {
            delegate?.pickerView(self, didSelectElementWithId: elementId)
        }
    }
    
    //MARK: - Открытые методы
    
    /// Отобразить элементы
    ///
    /// - Parameter elements: набор cell objects элементов пикера
    func display(elements: [HorizontalPickerElementCellObject]) {
    
        elementsTotalCount = elements.count
        
        collectionViewLayout.elementsTitleWidthCache.removeAll()
        for (index, element) in elements.enumerated() {
            
            let indexPath = IndexPath(row: index, section: 0)
            let isCompact = controlSizing == .compact
            let attributes = element.titleAttributes ?? [NSAttributedStringKey.font : HorizontalPickerElementCell.elementsTitleFont(forCompactSize: isCompact)]
            let attributedTitle = NSAttributedString(string: element.title, attributes: attributes)
            
            collectionViewLayout.elementsTitleWidthCache[indexPath] = attributedTitle.size().width
        }
        
        let dataSourceStructure = CollectionViewDataSourceStructure()
        dataSourceStructure.appendSection(with: elements)
        
        dataDisplayManager = CollectionViewDataSource(with: dataSourceStructure)
        dataDisplayManager.delegate = self
        dataDisplayManager.assign(to: collectionView)
        
        resetSelectionOffsetAndContentSize()
        
        guard window != nil else { return }
        
        collectionView.reloadData()
    }
    
    /// Выбрать элемент по index path
    ///
    /// - Parameters:
    ///   - indexPath: index path элемента
    ///   - animated: применять ли анимацию при выборе элемента
    func scrollToElement(at indexPath: IndexPath, animated: Bool) {
        guard var contentOffset = collectionViewLayout.layoutAttributesForItem(at: indexPath)?.center else { return }
        contentOffset.x -= collectionView.width / 2
        interactionScrollView.setContentOffset(contentOffset, animated: animated)
    }
    
    /// Выбрать элемент по идентификатору
    ///
    /// - Parameters:
    ///   - elementId: идентификатор элемента
    ///   - triggerDelegate: вызывать ли метод делегата при выполнении
    ///   - animated: применять ли анимацию при выборе элемента
    func selectElement(with elementId: String, triggerDelegate: Bool = true, animated: Bool = true) {
        
        guard let newIndexPath = dataDisplayManager.dataStructure.indexPathOfFirstObjectPassing({ cellObject in
            guard let cellObjectWithId = cellObject as? HorizontalPickerElementCellObject else { return false }
            return cellObjectWithId.itemId == elementId
        }) else { return }
        
        guard newIndexPath != selectedElementIndexPath else { return }
        
        doNotAlertDelegateAboutScrollStopped = animated && !triggerDelegate
        
        scrollToElement(at: newIndexPath, animated: animated)
        
        guard !animated else { return }
        
        // если в doNotAlertDelegateAboutScrollStopped == false сыграл роль флаг animated
        // делаем проверку руками
        if triggerDelegate {
            handleScrollingStopped()
        }
        else {
            refreshSelectionButtons()
        }
    }
    
    //MARK: - UICollectionViewDelegate
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        guard controlSizing == .compact else { return }
        
        if let pickerCell = cell as? HorizontalPickerElementCell {
            pickerCell.applyCompactFontSize()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: false)
        guard indexPath != selectedElementIndexPath else { return }
        scrollToElement(at: indexPath, animated: true)
    }
    
    //MARK: - UIScrollViewDelegate
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        collectionView.contentOffset.x = interactionScrollView.contentOffset.x
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        doNotAlertDelegateAboutScrollStopped = false
        collectionViewWasDragged = true
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        guard !decelerate else { return }
        
        collectionViewWasDragged = false
        handleScrollingStopped()
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        guard collectionViewWasDragged else { return }
        
        collectionViewWasDragged = false
        handleScrollingStopped()
    }
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        handleScrollingStopped()
    }
}

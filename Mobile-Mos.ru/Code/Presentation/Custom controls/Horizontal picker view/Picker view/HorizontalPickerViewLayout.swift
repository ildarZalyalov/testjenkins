//
//  HorizontalPickerViewLayout.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 09.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Лейату для HorizontalPickerView
class HorizontalPickerViewLayout: UICollectionViewFlowLayout {
    
    /// Минимальный scale элемента при его прокрутке
    lazy var minimumElementScale: CGFloat = {
        return self.defaultElementScale
    }()
    
    /// Минимальный scale элемента при обычном отображении пикера
    let defaultElementScale: CGFloat = 8 / 11
    
    /// Минимальный scale элемента при компактном отображении пикера
    let compactElementScale: CGFloat = 4 / 5
    
    /// Допустимый интервал значений для относительного расстояния элементов пикера от центра пикера
    let distancePortionInterval: ClosedRange<CGFloat> = 0 ... 1
    
    /// Допустимый интервал значений для alpha элементов пикера
    let allowedAlphaInterval: ClosedRange<CGFloat> = 0.5 ... 1
    
    /// Кеш ширины элементов пикера по index path
    var elementsTitleWidthCache = [IndexPath : CGFloat]()
    
    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        
        guard let layoutCollectionView = collectionView else { return nil }
        guard let attribute = super.layoutAttributesForItem(at: indexPath) else { return nil }
        
        guard attribute.representedElementCategory == .cell else {
            return attribute
        }
        
        guard let newAttribute = attribute.copy() as? UICollectionViewLayoutAttributes else {
            return attribute
        }
        
        let halfOfCollectionViewWidth = layoutCollectionView.width / 2
        let collectionViewVisibleCenter = layoutCollectionView.contentOffset.x + halfOfCollectionViewWidth
        let distanse = attribute.center.x - collectionViewVisibleCenter
        let distancePortion = (abs(distanse) / halfOfCollectionViewWidth).clamped(to: distancePortionInterval)
        let invertedDistancePortion = 1 - distancePortion
        
        newAttribute.alpha = invertedDistancePortion.clamped(to: allowedAlphaInterval)
        
        let scale = max(minimumElementScale, invertedDistancePortion)
        var transform = CGAffineTransform(scaleX: scale, y: scale)
        
        if let elementTitleWidth = elementsTitleWidthCache[attribute.indexPath] {
            
            let widthOffset = abs(itemSize.width - elementTitleWidth) / 2
            var translate = widthOffset * distancePortion
            
            if distanse < 0 {
                translate *= -1
            }
            
            transform = transform.translatedBy(x: translate, y: 0)
        }
        
        newAttribute.transform = transform
        
        return newAttribute
    }
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        return super.layoutAttributesForElements(in: rect)?.map { attribute in
            return layoutAttributesForItem(at: attribute.indexPath) ?? attribute
        }
    }
    
    override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
        return true
    }
}

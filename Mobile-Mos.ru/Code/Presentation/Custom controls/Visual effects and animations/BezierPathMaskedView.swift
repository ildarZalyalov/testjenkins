//
//  BezierPathMaskedView.swift
//  Portable
//
//  Created by Ivan Erasov on 30.03.17.
//  Copyright © 2017. All rights reserved.
//

import Foundation
import UIKit

/// UIView, содержимое которой может отображаться с маской, описываемой замкнутой UIBezierPath
class BezierPathMaskedView: UIView {
    
    /// Слой, который представляет собой маску
    fileprivate let maskLayer = CAShapeLayer()
    
    /// Closure, возвращающий замкнутый UIBezierPath, описывающий маску
    var maskingPathCalculator: ((CGRect) -> UIBezierPath)? = nil {
        didSet {
            layer.mask = maskingPathCalculator != nil ? maskLayer : nil
        }
    }
    
    fileprivate func calculateMaskingPath(for frame: CGRect) -> UIBezierPath? {
        let path = maskingPathCalculator?(frame)
        path?.close()
        return path
    }
    
    override func layoutSubviews() {
        
        super.layoutSubviews()
        
        guard layer.mask != nil else {
            return
        }
        
        let frame = bounds
        maskLayer.path = calculateMaskingPath(for: frame)?.cgPath
        maskLayer.frame = frame
    }
}

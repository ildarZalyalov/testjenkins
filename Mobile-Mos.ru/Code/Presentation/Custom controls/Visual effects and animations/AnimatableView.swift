//
//  AnimatableView.swift
//  Portable
//
//  Created by Ivan Erasov on 30.03.17.
//  Copyright © 2017. All rights reserved.
//

import Foundation

/// View, поддерживающая различные возможности анимации
protocol AnimatableView: class {
    
    /// Анимировать интерактивный переход, учитывая прогресс
    ///
    /// - Parameter progress: прогресс перехода, число от 0 до 1
    func animateInteractiveTransition(with progress: CGFloat)
}

//
//  ContentObtainErrorView.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 13.12.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Вьюха для отображения ошибки загрузки контента
class ContentObtainErrorView: UIView {
    
    fileprivate static let nibFile = UINib(nibName: String(describing: ContentObtainErrorView.self), bundle: nil)
    
    static func loadNib() -> ContentObtainErrorView {
        return ContentObtainErrorView.nibFile.instantiate(withOwner: nil, options: nil).first as! ContentObtainErrorView
    }
    
    @IBOutlet fileprivate weak var exclaimerLabel: UILabel!
    @IBOutlet fileprivate weak var errorTitleLabel: UILabel!
    @IBOutlet fileprivate weak var errorMessageLabel: UILabel!
    
    @IBOutlet fileprivate weak var buttonBackgroundImageView: UIImageView!
    @IBOutlet fileprivate weak var buttonShadowImageView: UIImageView!
    @IBOutlet fileprivate weak var retryButton: UIButton!
    
    override func awakeFromNib() {
        
        /// Растягиваемая картинка с фоном кнопки
        let buttonBackgroundImage = #imageLiteral(resourceName: "roundedButtonBackground").resizableImage(withCapInsets: UIEdgeInsets(top: 0, left: 17, bottom: 0, right: 17))
        buttonBackgroundImageView.image = buttonBackgroundImage
        
        let buttonShadowImage = #imageLiteral(resourceName: "roundedButtonShadow").resizableImage(withCapInsets: UIEdgeInsets(top: 0, left: 27, bottom: 0, right: 27))
        buttonShadowImageView.image = buttonShadowImage
    }
    
    /// Текст для привлечения внимания (например, можно поставить эмоджи, установлен большой размер текста)
    var exclaimerText: String? {
        get { return exclaimerLabel.text }
        set { exclaimerLabel.text = newValue }
    }
    
    /// Заголовок ошибки, отображается под стекстом для привлечения внимания
    var errorTitleText: String? {
        get { return errorTitleLabel.text }
        set { errorTitleLabel.text = newValue }
    }
    
    /// Текст ошибки, отображается под заголовокм ошибки
    var errorMessageText: String? {
        get { return errorMessageLabel.text }
        set { errorMessageLabel.text = newValue }
    }
    
    /// Заголовок кнопки повтора запроса загрузки контента
    var retryButtonTitle: String? {
        get { return retryButton.title(for: .normal) }
        set { retryButton.setTitle(newValue, for: .normal) }
    }
    
    override var tintColor: UIColor! {
        didSet {
            retryButton.setTitleColor(tintColor, for: .normal)
        }
    }
    
    /// Добавить action повтора запроса загрузки контента
    ///
    /// - Parameters:
    ///   - retryAction: action повтора запроса загрузки контента
    ///   - target: объект, у которого определен передаваемый action
    func add(retryAction: Selector, with target: Any?) {
        retryButton.addTarget(target, action: retryAction, for: .touchUpInside)
    }
}

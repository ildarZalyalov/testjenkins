//
//  CommandKeyboardView.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 17.05.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import UIKit

/// Делегат командной клавиатуры
protocol CommandKeyboardViewDelegate: class {
    
    /// Пользователь нажал на кнопку
    ///
    /// - Parameter itemId: идентификатор кнопки
    func didPressCommand(with itemId: String)
}

/// Командная клавиатура
class CommandKeyboardView: UIView {
    
    /// View model для кнопки командной клавиатуры
    struct KeyboardButtonViewModel {
        
        /// Идентификатор кнопки
        var itemId: String
        
        /// Заголовок кнопки
        var title: String
    }
    
    /// UIScrollView для командной клавиатуры, которая может отменять жесты любых дочерних вьюх, когда обнаруживает, что ее скроллят
    fileprivate class CommandKeyboardScrollView: UIScrollView {
        
        override func touchesShouldCancel(in view: UIView) -> Bool {
            return true
        }
    }
    
    /// Цвет заголовков кнопок
    var buttonsTitleColor: UIColor = UIColorPalette.commandKeyboardButtonsTitleDefaultColor
    
    /// Цвет заголовков кнопок при фокусе на них
    var buttonsHighlightedTitleColor: UIColor = UIColorPalette.commandKeyboardButtonsHighlightedTitleDefaultColor
    
    /// Цвет теней кнопок
    var buttonsShadowColor: UIColor = UIColorPalette.commandKeyboardButtonsShadowDefaultColor
    
    /// Шрифт заголовков кнопок
    var buttonsTitleFont = UIFont(customName: .graphikLCGRegular, size: 15)
    
    /// Инсеты заголовка кнопки относительно ее границ
    let buttonsTitleEdgeInsets = UIEdgeInsets(top: 9, left: 16, bottom: 9, right: 16)
    
    /// Значение определяет на сколько точек по каждой оси тень кнопки выходит за ее границы
    let buttonsShadowOutset = CGPoint(x: 10, y: 10)
    
    
    /// Расстояние между рядами кнопок
    let rowSpace: CGFloat = 5
    
    /// Расстояние между кнопками в одном ряду
    let columnSpace: CGFloat = 5
    
    /// Инсеты рядов кнопок относительно краев клавиатуры
    let edgeInsets = UIEdgeInsets(top: 19, left: 14, bottom: 20, right: 13)
    
    /// Высота каждой кнопки
    let buttonHeight: CGFloat = 40
    
    
    /// Растягиваемая картинка с фоном кнопки
    let buttonBackgroundImage = #imageLiteral(resourceName: "roundedButtonBackground").resizableImage(withCapInsets: UIEdgeInsets(top: 0, left: 17, bottom: 0, right: 17))
    
    /// Растягиваемая картинка с тенью кнопки
    let buttonShadowImage = #imageLiteral(resourceName: "roundedButtonShadow").resizableImage(withCapInsets: UIEdgeInsets(top: 0, left: 27, bottom: 0, right: 27))
    
    
    /// Максимально возможная высота клавиатуры
    let maxKeyboardHeight: CGFloat = 259
    
    /// Максимально возможное кол-во рядов кнопок при свернутой клавиатуре
    let collapsedKeyboardMaxButtonRowsCount = 4
    
    /// Высота разделителя сверху клавиатуры
    let topSeparatorHeight: CGFloat = UIScreen.main.isRetina ? 0.5 : 1
    
    /// Скрыт ли фон клавиатуры
    var isBackgroundHidden: Bool {
        get { return backgroundBlurView.isHidden }
        set {
            topSeparator.isHidden = newValue
            backgroundBlurView.isHidden = newValue
        }
    }
    
    /// Зафиксировать текущую высоту клавиатуры
    var fixateCurrentKeyboardHeight: Bool = false
    
    /// Использовать ли для высоты клавиатуры ее максимально возможную высоту несмотря на содержимое
    /// (пока спрятано т.к. нет нобходимости)
    fileprivate var forceMaxKeyboardHeight: Bool = false {
        didSet {
            guard forceMaxKeyboardHeight != oldValue else { return }
            recalculateHeight()
        }
    }
    
    /// Делегат
    weak var delegate: CommandKeyboardViewDelegate?
    
    /// Схлопунта ли клавиатура
    fileprivate(set) var isCollapsed: Bool = false {
        didSet {
            recalculateHeight()
            recalculateButtonsLayout()
        }
    }
    
    /// Отображаемое кол-во рядов кнопок
    fileprivate var displayedButtonRowsCount: Int {
        return isCollapsed ? collapsedKeyboardMaxButtonRowsCount : buttons.count
    }
    
    /// Идентификатор кнопки "расхлопывания"
    fileprivate let expandButtonId = UUID().uuidString
    
    /// View model ряда с кнопкой "расхлопывания"
    fileprivate var expandButtonRowModel: [KeyboardButtonViewModel] {
        return [KeyboardButtonViewModel(itemId: expandButtonId,
                                        title: StringsHelper.commandKeyboardMoreActionsButtonTitle)]
    }
    
    fileprivate var scrollView = CommandKeyboardScrollView()
    fileprivate let backgroundBlurView = UIVisualEffectView(effect: UIBlurEffect(style: .extraLight))
    fileprivate let topSeparator = UIView()
    
    fileprivate var buttonControls = [UIButton]()
    fileprivate var buttonBackgroundViews = [UIImageView]()
    fileprivate var buttonShadowViews = [UIImageView]()
    
    /// Текущая высота клавиатуры
    fileprivate(set) var currentHeight: CGFloat = 0
    
    /// Ширина, для которой расчитан текущий layout кнопок
    fileprivate var currentButtonsLayoutWidth: CGFloat = 0
    
    /// Набор view model кнопок клавиатуры
    /// Присвоение нового значения пересчитает layout кнопок и высоту клавиатуры
    var buttons: [[KeyboardButtonViewModel]] = [] {
        didSet {
            isCollapsed = buttons.count > collapsedKeyboardMaxButtonRowsCount
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    fileprivate func commonInit() {
        
        backgroundColor = UIColor.clear

        backgroundBlurView.frame = bounds
        backgroundBlurView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(backgroundBlurView)
        
        scrollView.frame = bounds
        scrollView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        scrollView.alwaysBounceVertical = false
        scrollView.delaysContentTouches = false
        scrollView.canCancelContentTouches = true
        addSubview(scrollView)
        
        topSeparator.frame = CGRect(x: 0, y: 0, width: bounds.width, height: topSeparatorHeight)
        topSeparator.autoresizingMask = [.flexibleWidth, .flexibleBottomMargin]
        topSeparator.backgroundColor = UIColorPalette.commandKeyboardTopSeparatorColor
        addSubview(topSeparator)
    }
    
    override var intrinsicContentSize: CGSize {
        return CGSize(width: CGFloat.greatestFiniteMagnitude, height: currentHeight)
    }
    
    override func layoutSubviews() {
        
        super.layoutSubviews()
        
        guard width != currentButtonsLayoutWidth else { return }
        
        currentButtonsLayoutWidth = width
        recalculateButtonsLayout()
    }
    
    override var canBecomeFirstResponder: Bool {
        return true
    }
    
    override var canResignFirstResponder: Bool {
        return true
    }
    
    override func safeAreaInsetsDidChange() {
        guard #available(iOS 11.0, *) else { return }
        super.safeAreaInsetsDidChange()
        recalculateHeight()
    }
    
    //MARK: - Открытые методы
    
    /// Пересчитать высоту и intrinsicContentSize клавиатуры
    func recalculateHeight() {
        
        guard !fixateCurrentKeyboardHeight else {
            return
        }
        
        defer {
            
            if #available(iOS 11.0, *), currentHeight > 0 {
                currentHeight += safeAreaInsets.top + safeAreaInsets.bottom
            }
            
            invalidateIntrinsicContentSize()
        }
        
        guard !forceMaxKeyboardHeight else {
            currentHeight = maxKeyboardHeight
            return
        }
        
        let count = CGFloat(displayedButtonRowsCount)
        
        guard count > 0 else {
            currentHeight = 0
            return
        }
        
        currentHeight = count * buttonHeight
        currentHeight += (count - 1) * rowSpace
        currentHeight += edgeInsets.top + edgeInsets.bottom
        
        scrollView.contentSize = CGSize(width: scrollView.contentSize.width, height: currentHeight)
        scrollView.isScrollEnabled = currentHeight > maxKeyboardHeight
        
        if currentHeight > maxKeyboardHeight {
            currentHeight = maxKeyboardHeight
        }
    }
    
    //MARK: - Приватные методы
    
    fileprivate func recalculateButtonsLayout() {
        
        removeAllButtonControls()
        
        guard buttons.count > 0 else {
            return
        }
        
        var currentY = edgeInsets.top
        
        for (rowIndex, var row) in buttons.enumerated() {
            
            if isCollapsed {
                
                guard rowIndex < collapsedKeyboardMaxButtonRowsCount else { return }
                
                if rowIndex == collapsedKeyboardMaxButtonRowsCount - 1 {
                    row = expandButtonRowModel
                }
            }
            
            let count = CGFloat(row.count)
            var buttonWidth = frame.width - edgeInsets.left - edgeInsets.right
            buttonWidth -= (count - 1) * columnSpace
            buttonWidth /= count
            
            var currentX = edgeInsets.left
            
            for button in row {
                
                let buttonControl = createButton(with: button)
                buttonControl.frame = CGRect(x: currentX, y: currentY, width: buttonWidth, height: buttonHeight)
                
                let buttonShadowView = createButtonShadow(with: buttonControl)
                let buttonBackgroundView = createButtonBackground(with: buttonControl)
                
                scrollView.addSubview(buttonShadowView)
                scrollView.addSubview(buttonBackgroundView)
                scrollView.addSubview(buttonControl)
                
                currentX += columnSpace + buttonWidth
            }
            
            currentY += buttonHeight + rowSpace
        }
    }
    
    fileprivate func removeAllButtonControls() {
        
        for shadow in buttonShadowViews {
            shadow.removeFromSuperview()
        }
        for background in buttonBackgroundViews {
            background.removeFromSuperview()
        }
        for button in buttonControls {
            button.removeFromSuperview()
        }
        
        buttonShadowViews.removeAll()
        buttonBackgroundViews.removeAll()
        buttonControls.removeAll()
    }
    
    fileprivate func createButton(with model: KeyboardButtonViewModel) -> KeyboardButtonControl {
        
        let button = KeyboardButtonControl(type: .custom)
        button.buttonId = model.itemId
        
        button.backgroundColor = UIColor.clear
        
        button.contentHorizontalAlignment = .center
        button.contentVerticalAlignment = .fill
        
        button.setTitle(model.title, for: .normal)
        
        button.setTitleColor(buttonsTitleColor, for: .normal)
        button.setTitleColor(buttonsHighlightedTitleColor, for: .highlighted)
        button.titleLabel?.font = buttonsTitleFont
        
        button.addTarget(self, action: #selector(buttonPressed(button:)), for: .touchUpInside)
        button.titleEdgeInsets = buttonsTitleEdgeInsets
        button.isExclusiveTouch = true
        
        buttonControls.append(button)
        
        return button
    }
    
    fileprivate func createButtonShadow(with buttonControl: KeyboardButtonControl) -> UIView {
        
        let buttonShadowView = UIImageView(image: buttonShadowImage)
        buttonShadowView.tintColor = buttonsShadowColor
        buttonShadowView.frame = buttonControl.frame.insetBy(dx: -buttonsShadowOutset.x, dy: -buttonsShadowOutset.y)
        
        buttonShadowViews.append(buttonShadowView)
        
        return buttonShadowView
    }
    
    fileprivate func createButtonBackground(with buttonControl: KeyboardButtonControl) -> UIView {
        
        let buttonBackgroundView = UIImageView(image: buttonBackgroundImage)
        buttonBackgroundView.tintColor = buttonsHighlightedTitleColor
        buttonControl.isHighlightedChanged = { [weak self] _, isHighlighted in
            guard let strongSelf = self else { return }
            buttonBackgroundView.tintColor = isHighlighted ? strongSelf.buttonsTitleColor : strongSelf.buttonsHighlightedTitleColor
        }
        buttonBackgroundView.frame = buttonControl.frame
        
        buttonBackgroundViews.append(buttonBackgroundView)
        
        return buttonBackgroundView
    }
    
    @objc
    fileprivate func buttonPressed(button: UIButton) {
        
        guard let keyboardControl = button as? KeyboardButtonControl,
              let buttonId = keyboardControl.buttonId else {
            return
        }
        
        if buttonId == expandButtonId {
            isCollapsed = false
        }
        else {
            delegate?.didPressCommand(with: buttonId)
        }
    }
}

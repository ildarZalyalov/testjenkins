//
//  KeyboardButtonControl.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 27.07.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import UIKit

/// Кнопка для командной или инлайн клавиатуры
class KeyboardButtonControl: UIButton {
    
    /// Идентификатор кнопки
    var buttonId: String?
    
    /// Основной tintColor
    var normalTintColor: UIColor? {
        didSet {
            guard normalTintColor != nil else { return }
            tintColor = normalTintColor
        }
    }
    
    /// tintColor при фокусе на контроле
    var highlightedTintColor: UIColor?
    
    /// Блок обработки изменения состояния фокуса (isHighlighted)
    var isHighlightedChanged: ((KeyboardButtonControl, Bool) -> ())?
    
    override var isHighlighted: Bool {
        
        get {
            return super.isHighlighted
        }
        
        set {
            super.isHighlighted = newValue
            isHighlightedChanged?(self, newValue)
            tintColor = newValue ? highlightedTintColor : normalTintColor
        }
    }
}

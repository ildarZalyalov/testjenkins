//
//  SubtitledNavigationTitleView.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 18.08.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import UIKit

/// Вьюха с заголовком и подзаголовком, который может прятаться
class SubtitledNavigationTitleView: UIView {
    
    fileprivate static let nibFile = UINib(nibName: String(describing: SubtitledNavigationTitleView.self), bundle: nil)
    
    static func loadNib() -> SubtitledNavigationTitleView {
        return SubtitledNavigationTitleView.nibFile.instantiate(withOwner: nil, options: nil).first as! SubtitledNavigationTitleView
    }
    
    @IBOutlet fileprivate weak var titleLabel: UILabel!
    @IBOutlet fileprivate weak var subtitleLabel: UILabel!
    
    @IBOutlet fileprivate var titleLabelTopConstraint: NSLayoutConstraint!
    @IBOutlet fileprivate var titleLabelTopToSuperviewConstraint: NSLayoutConstraint!
    @IBOutlet fileprivate var titleLabelVerticalCenterConstraint: NSLayoutConstraint!
    @IBOutlet fileprivate var titleLabelBottomToSubtitleConstraint: NSLayoutConstraint!
    @IBOutlet fileprivate var titleLabelBottomToSuperviewConstraint: NSLayoutConstraint!
    @IBOutlet fileprivate var subtitleLabelBottomRegularConstraint: NSLayoutConstraint!
    @IBOutlet fileprivate var subtitleLabelBottomHiddenConstraint: NSLayoutConstraint!
    
    /// Длительность анимации показа/скрытия подзаголовка
    let subtitleAnimationDuration: TimeInterval = 0.2
    
    /// Надо ли анимировать показ/скрытие подзаголовка
    var shouldAnimateSubtitleAppearance = false
    
    /// Длительность шага анимации подзаголовка (сколько отображается один из текстов)
    let subtitleAnimationStepDuration: TimeInterval = 0.3
    
    /// Анимируется ли подзаголовок
    fileprivate(set) var isShowingSubtitleAnimation = false
    
    /// Сюда кладем значение подзаголовка на время анимации
    fileprivate var realSubtitleValue: String?
    
    /// Аниматор подзаголовка
    fileprivate lazy var subtitleAnimationPerformer: DelayedSelectorPerformer = {
        return DelayedSelectorPerformer(target: self, selector: #selector(updateSubtitleAnimationText))
    }()
    
    /// Текущий индекс в массиве текстов для анимации подзаголовка
    fileprivate var subtitleAnimationTextIndex = 0
    
    /// Тексты для анимации подзаголовка
    fileprivate var subtitleAnimationTexts: [String]?
    
    /// Атрибуты отрисовки заголовка
    var titleTextAttributes: [NSAttributedStringKey : Any]? {
        didSet {
            refresh(label: titleLabel, with: title, and: titleTextAttributes)
        }
    }
    
    /// Атрибуты отрисовки подзаголовка
    var subtitleTextAttributes: [NSAttributedStringKey : Any]? {
        didSet {
            refresh(label: subtitleLabel, with: subtitle, and: subtitleTextAttributes)
        }
    }
    
    /// Текст заголовка
    var title: String? {
        didSet {
            refresh(label: titleLabel, with: title, and: titleTextAttributes)
        }
    }
    
    /// Текст подзаголовка
    var subtitle: String? {
        didSet {
            updateLayout()
            refresh(label: subtitleLabel, with: subtitle, and: subtitleTextAttributes)
        }
    }
    
    override func awakeFromNib() {
        updateLayout()
    }
    
    /// Запустить анимацию подзаголовка - тексты из набора будут зацикленно сменять друг друга
    ///
    /// - Parameter texts: набор текстов
    func startSubtitleAnimation(with texts: [String]) {
        
        guard !isShowingSubtitleAnimation else { return }
        
        isShowingSubtitleAnimation = true
        
        realSubtitleValue = subtitle
        subtitleAnimationTexts = texts
        subtitleAnimationTextIndex = 0
        
        updateSubtitleAnimationText()
    }
    
    /// Остановить анимацию подзаголовка
    func stopSubtitleAnimation() {
        
        guard isShowingSubtitleAnimation else { return }
        
        isShowingSubtitleAnimation = false
        
        subtitleAnimationPerformer.cancelDelayedPerform()
        subtitleAnimationTexts = nil
        updateSubtitleAnimated(with: realSubtitleValue)
    }
    
    //MARK: - Приватные методы
    
    /// Обновить подзаголовок с анимацией (анимация применяется для этого обноления даже если отключена)
    ///
    /// - Parameter text: текст для подзаголовка
    fileprivate func updateSubtitleAnimated(with text: String?) {
        
        let shouldAnimateSubtitle = shouldAnimateSubtitleAppearance
        shouldAnimateSubtitleAppearance = true
        
        subtitle = text
        
        shouldAnimateSubtitleAppearance = shouldAnimateSubtitle
    }
    
    /// Обновить "кадр" в анимации подзаголовка
    @objc
    fileprivate func updateSubtitleAnimationText() {
        
        guard isShowingSubtitleAnimation else { return }
        guard let texts = subtitleAnimationTexts else { return }
        
        updateSubtitleAnimated(with: texts[subtitleAnimationTextIndex])
        
        subtitleAnimationTextIndex += 1
        if subtitleAnimationTextIndex == texts.count {
            subtitleAnimationTextIndex = 0
        }
        
        subtitleAnimationPerformer.perform(afterDelay: subtitleAnimationStepDuration)
    }
    
    /// Обновить текст в лейбле
    ///
    /// - Parameters:
    ///   - label: лейбл
    ///   - text: текст
    ///   - textAttributes: атрибуты текста
    fileprivate func refresh(label: UILabel, with text: String?, and textAttributes: [NSAttributedStringKey : Any]?) {
        
        guard let string = text else { return }
        
        var attributes = textAttributes
        
        if attributes != nil {
            
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.alignment = .center
            
            attributes![NSAttributedStringKey.paragraphStyle] = paragraphStyle
        }
        
        label.attributedText = NSAttributedString(string: string, attributes: attributes)
    }
    
    /// Обновить лейаут лейблов в соответствии с текущими параметрами
    fileprivate func updateLayout() {
        
        let shouldHideSubtitle = subtitle?.isEmpty ?? true
        
        guard shouldHideSubtitle != subtitleLabel.isHidden else { return }
        
        let layoutBlock = {
            
            let regularConstraints: [NSLayoutConstraint] = [self.titleLabelTopConstraint,
                                                            self.titleLabelBottomToSubtitleConstraint,
                                                            self.subtitleLabelBottomRegularConstraint]
            
            let subtitleHiddenConstraints: [NSLayoutConstraint] = [self.titleLabelTopToSuperviewConstraint,
                                                                   self.titleLabelVerticalCenterConstraint,
                                                                   self.titleLabelBottomToSuperviewConstraint,
                                                                   self.subtitleLabelBottomHiddenConstraint]
            
            let constraintsToActivate = shouldHideSubtitle ? subtitleHiddenConstraints : regularConstraints
            let constraintsToDeactivate = shouldHideSubtitle ? regularConstraints : subtitleHiddenConstraints
            
            NSLayoutConstraint.deactivate(constraintsToDeactivate)
            NSLayoutConstraint.activate(constraintsToActivate)
            
            self.subtitleLabel.alpha = shouldHideSubtitle ? 0 : 1
            
            self.layoutIfNeeded()
        }
        
        if shouldAnimateSubtitleAppearance {
            
            if !shouldHideSubtitle {
                subtitleLabel.isHidden = false
            }
            
            UIView.animate(withDuration: subtitleAnimationDuration,
                           delay: 0,
                           options: [.beginFromCurrentState, .curveEaseInOut],
                           animations: layoutBlock,
                           completion: { _ in
                                if shouldHideSubtitle {
                                    self.subtitleLabel.isHidden = true
                                }
            })
        }
        else {
            subtitleLabel.isHidden = shouldHideSubtitle
            layoutBlock()
        }
    }
}

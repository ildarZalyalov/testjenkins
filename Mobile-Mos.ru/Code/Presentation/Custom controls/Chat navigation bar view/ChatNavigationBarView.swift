//
//  ChatNavigationBarView.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 31.07.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import UIKit

/// Вьюха с фоном UINavigationBar, используемая в чате
class ChatNavigationBarView: UIView {
    
    fileprivate static let nibFile = UINib(nibName: String(describing: ChatNavigationBarView.self), bundle: nil)
    
    static func loadNib() -> ChatNavigationBarView {
        return ChatNavigationBarView.nibFile.instantiate(withOwner: nil, options: nil).first as! ChatNavigationBarView
    }
    
    @IBOutlet fileprivate weak var shadowView: UIImageView!
    @IBOutlet fileprivate weak var backgroundView: UIImageView!
    @IBOutlet fileprivate weak var gradientView: UIImageView!
    @IBOutlet fileprivate weak var avatarView: UIImageView!
    
    /// Цвет тени
    override var shadowColor: UIColor {
        didSet {
            guard shadowView != nil else { return }
            shadowView.tintColor = shadowColor
        }
    }
    
    /// Цвет фона
    override var backgroundColor: UIColor? {
        didSet {
            guard backgroundView != nil else { return }
            backgroundView.tintColor = backgroundColor
        }
    }
    
    /// Цвет градиента, накладываемого поверх фона
    var gradientColor: UIColor? {
        didSet {
            guard gradientView != nil else { return }
            gradientView.tintColor = gradientColor
        }
    }
    
    /// Аватарка
    var avatarImage: UIImage? {
        didSet {
            avatarView.image = avatarImage
        }
    }
    
    /// Веб-ссылка на аватарку
    var avatarImageUrl: URL? {
        didSet {
            guard avatarImageUrl != nil else { return }
            avatarView.loadAndDisplayImage(from: avatarImageUrl)
        }
    }
    
    /// Размер аватарки
    var avatarSize: CGSize {
        return avatarView.size
    }
    
    override func awakeFromNib() {
        
        tintAdjustmentMode = .normal
        
        let shadowImage = UIImage(contentsOfFileWithName: "headerShadow", ofType: "png")?.resizableImage(withCapInsets: UIEdgeInsets(top: 44, left: 1, bottom: 44, right: 70))
        let backgroundImage = UIImage(contentsOfFileWithName: "headerBackground", ofType: "png")?.resizableImage(withCapInsets: UIEdgeInsets(top: 38, left: 1, bottom: 38, right: 70))
        let gradientImage = UIImage(contentsOfFileWithName: "headerGradient", ofType: "png")
        
        shadowView.image = shadowImage?.withRenderingMode(.alwaysTemplate)
        backgroundView.image = backgroundImage?.withRenderingMode(.alwaysTemplate)
        gradientView.image = gradientImage?.withRenderingMode(.alwaysTemplate)
        
        // Значения по умолчанию
        
        shadowColor = UIColor.fromHex(hex: 0x2194F3)
        backgroundColor = UIColor.fromHex(hex: 0x22C3F1)
        gradientColor = UIColor.fromHex(hex: 0x2194F3)
    }
    
}

//
//  TextInputToolbar.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 26.07.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import UIKit

/// Панель с полем ввода для текста и кнопкой отправки + отключаемая кнопка переключения на командную клавиатуру справа
class TextInputToolbar: UIView, UITextViewDelegate {
    
    fileprivate static let nibFile = UINib(nibName: String(describing: TextInputToolbar.self), bundle: nil)
    
    static func loadNib() -> TextInputToolbar {
        return TextInputToolbar.nibFile.instantiate(withOwner: nil, options: nil).first as! TextInputToolbar
    }
    
    @IBOutlet fileprivate weak var textView: UITextView!
    @IBOutlet fileprivate weak var placeholderLabel: UILabel!
    @IBOutlet fileprivate(set) weak var commandsButton: UIButton!
    @IBOutlet fileprivate(set) weak var sendButton: UIButton!
    
    @IBOutlet fileprivate weak var textViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet fileprivate var textViewLeftToEdgeConstraint: NSLayoutConstraint!
    @IBOutlet fileprivate var textViewLeftToCommandsButtonConstraint: NSLayoutConstraint!
    
    let textContainerInset = UIEdgeInsets(top: 14, left: 11, bottom: 12, right: 11)
    
    /// Максимальное кол-во символов, которое можно ввести в поле
    var maxTextSymbolsCount: Int?
    
    /// Текст в поле ввода текста
    var text: String? {
        
        get {
            return textView.text
        }
        
        set {
            
            guard newValue != textView.text else {
                return
            }
            
            textView.text = newValue
            textViewDidChange(textView)
        }
    }
    
    /// Плейсхолдер в поле ввода текста
    var placeholder: String? {
        get { return placeholderLabel.text }
        set { placeholderLabel.text = newValue }
    }
    
    /// Присутствует ли кнопка переключения на командную клавиатуру
    var isCommandsButtonHidden: Bool {
        
        get {
            return commandsButton.isHidden
        }
        set {
            
            guard newValue != commandsButton.isHidden else { return }
            commandsButton.isHidden = newValue
            
            textViewLeftToEdgeConstraint.isActive = commandsButton.isHidden
            textViewLeftToCommandsButtonConstraint.isActive = !commandsButton.isHidden
        }
    }
    
    /// Отображается ли системная клавиатура
    var isSystemKeyboardDisplayed: Bool {
        return textView.isFirstResponder
    }
    
    /// Блок обработки изменения текста в поле ввода
    var textDidChange: ((String) -> ())?
    
    override func awakeFromNib() {
        
        placeholderLabel.isHidden = !textView.text.isEmpty
        
        var insets = textContainerInset
        insets.right += sendButton.frame.size.width
        textView.textContainerInset = insets
    }
    
    @discardableResult override func becomeFirstResponder() -> Bool {
        return textView.becomeFirstResponder()
    }
    
    @discardableResult override func resignFirstResponder() -> Bool {
        return textView.resignFirstResponder()
    }
    
    //MARK: - Приватные методы
    
    fileprivate func adjustTextViewSize() {
        let newHeight = textView.sizeThatFits(CGSize(width: textView.width, height: CGFloat.greatestFiniteMagnitude)).height
        textViewHeightConstraint.constant = newHeight
        layoutIfNeeded()
    }
    
    //MARK: - UITextViewDelegate
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        guard let maxCount = maxTextSymbolsCount else { return true }
        guard let currentText = textView.text as NSString? else { return true }
        
        return currentText.replacingCharacters(in: range, with: text).count <= maxCount
    }
    
    func textViewDidChange(_ textView: UITextView) {
        
        placeholderLabel.isHidden = !textView.text.isEmpty
        sendButton.isEnabled = !textView.text.isEmpty
        
        adjustTextViewSize()
        
        textDidChange?(textView.text)
    }
}

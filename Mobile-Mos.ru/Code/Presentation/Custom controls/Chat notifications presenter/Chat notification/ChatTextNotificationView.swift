//
//  ChatTextNotificationView.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 07.09.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Вьюха текстовой нотификации из чата
class ChatTextNotificationView: UIView, ChatNotificationView {
    
    fileprivate static let nibFile = UINib(nibName: String(describing: ChatTextNotificationView.self), bundle: nil)
    
    static func loadNib() -> ChatTextNotificationView {
        return ChatTextNotificationView.nibFile.instantiate(withOwner: nil, options: nil).first as! ChatTextNotificationView
    }
    
    @IBOutlet fileprivate weak var textLabel: UILabel!
    @IBOutlet fileprivate weak var textLabelLeftConstraint: NSLayoutConstraint!
    @IBOutlet fileprivate weak var textLabelRightConstraint: NSLayoutConstraint!
    
    let textLineSpacing: CGFloat = 2.5
    
    var tapHandler: (() -> ())?
    
    /// Максимальная ширина вьюхи для подсчета лейаута
    var preferredMaxLayoutWidth: CGFloat = UIScreen.main.bounds.width {
        didSet {
            textLabel.preferredMaxLayoutWidth = preferredMaxLayoutWidth - textLabelLeftConstraint.constant - textLabelRightConstraint.constant
        }
    }
    
    /// Текст в нотификации
    var text: String? {
        
        didSet {
            
            guard let string = text else { return }
            
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.alignment = .center
            paragraphStyle.lineSpacing = textLineSpacing
            paragraphStyle.lineBreakMode = .byTruncatingTail
            
            let attributes: [NSAttributedStringKey : Any] = [
                NSAttributedStringKey.foregroundColor : textLabel.textColor,
                NSAttributedStringKey.font : textLabel.font,
                NSAttributedStringKey.paragraphStyle: paragraphStyle
            ]
            
            textLabel.attributedText = NSAttributedString(string: string, attributes: attributes)
        }
    }
}

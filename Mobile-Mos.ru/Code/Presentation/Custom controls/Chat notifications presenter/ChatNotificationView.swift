//
//  ChatNotificationView.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 11.09.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Обработчик нажатия на нотификацию
typealias ChatNotificationViewTapHandler = () -> ()

/// Протокол view, которые отображают нотификации
protocol ChatNotificationView {
    
    /// Обработчик нажатия на нотификацию
    var tapHandler: ChatNotificationViewTapHandler? { get set }
}

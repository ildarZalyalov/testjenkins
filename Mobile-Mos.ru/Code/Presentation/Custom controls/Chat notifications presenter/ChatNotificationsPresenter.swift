//
//  ChatNotificationsPresenter.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 07.09.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Контроллер показа нотификаций из чата
class ChatNotificationsPresenter: NSObject {
    
    /// Высота тени сверху экрана при показе нотификации
    let shadowViewHeight: CGFloat = 93
    
    /// Сдвиг нотификации относительно верха экрана
    let notificationsTopOffset: CGFloat = 5
    
    /// Инсет нотификаций слева и справа от краев экрана
    let notificationsLeftRightInset: CGFloat = 5
    
    
    /// Длительность анимаций показа/скрытия нотификаций
    let notificationAnimationDuration: TimeInterval = 0.2
    
    /// Длительность отображения нотификации на экране до автоматического скрытия
    let notificationDisplayDuration: TimeInterval = 3
    
    /// Вьюха нотификации, отображаемой сейчас на экране
    fileprivate weak var currentNotificationView: UIView?
    
    /// Контроллер автоматического скрытия текущей нотификации
    fileprivate lazy var hidePerformer: DelayedSelectorPerformer = {
        return DelayedSelectorPerformer(target: self, selector: #selector(hideCurrentNotificationView))
    }()
    
    /// Окно оверлея для нотификаций
    fileprivate lazy var overlayWindow: HitTestableOverlayWindow = {
        
        let overlay = HitTestableOverlayWindow(frame: UIScreen.main.bounds)
        overlay.windowLevel = UIWindowLevelStatusBar
        
        self.shadowView.frame = CGRect(x: 0, y: 0, width: overlay.width, height: self.shadowViewHeight)
        overlay.addSubview(self.shadowView)
        
        return overlay
        
    }()
    
    /// Вьюха с тенью
    fileprivate lazy var shadowView: UIImageView = {
        
        let shadowImage = #imageLiteral(resourceName: "notificationTopShadow").resizableImage(withCapInsets: UIEdgeInsets(top: 0, left: 1, bottom: 0, right: 1))
        
        let shadowView = UIImageView(image: shadowImage)
        shadowView.autoresizingMask = [.flexibleWidth, .flexibleBottomMargin]
        
        return shadowView
        
    }()
    
    fileprivate lazy var notificationWidth: CGFloat = {
        return self.overlayWindow.bounds.insetBy(dx: self.notificationsLeftRightInset, dy: 0).width
    }()
    
    //MARK: - Приватные методы
    
    fileprivate func addHandleTap(to notificationView: UIView) {
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleTapOnNotification(_:)))
        notificationView.addGestureRecognizer(gestureRecognizer)
    }
    
    fileprivate func addSwipeToHide(to notificationView: UIView) {
        
        let gestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(hideNotificationViewOnSwipe(_:)))
        notificationView.addGestureRecognizer(gestureRecognizer)
        
        guard let gestureRecognizers = notificationView.gestureRecognizers else { return }
        
        for recognizer in gestureRecognizers {
            guard recognizer !== gestureRecognizer else { continue }
            recognizer.require(toFail: gestureRecognizer)
        }
    }
    
    @objc
    fileprivate func handleTapOnNotification(_ sender: UITapGestureRecognizer) {
        
        guard let notificationView = sender.view else { return }
        
        if let chatNotificationView = notificationView as? ChatNotificationView {
            chatNotificationView.tapHandler?()
        }
        
        hideNotificationView(notificationView: notificationView)
    }
    
    @objc
    fileprivate func hideNotificationViewOnSwipe(_ sender: UIPanGestureRecognizer) {
        
        guard let notificationView = sender.view else { return }
        
        let velocity = sender.velocity(in: notificationView)
        
        guard abs(velocity.y) > abs(velocity.x) else { return }
        guard velocity.y < 0 else { return }
        
        hideNotificationView(notificationView: notificationView)
    }
    
    @objc
    fileprivate func hideCurrentNotificationView() {
        guard let notificationView = currentNotificationView else { return }
        hideNotificationView(notificationView: notificationView)
    }
    
    /// Показать вьюху нотификации с анимацией
    ///
    /// - Parameter notificationView: вьюха нотификации
    fileprivate func showNotificationView(notificationView: UIView) {
        
        overlayWindow.isHidden = false
        hidePerformer.cancelDelayedPerform()
        
        let sizeToFit = CGSize(width: notificationWidth, height: overlayWindow.height)
        let notificationHeight = notificationView.systemLayoutSizeFitting(sizeToFit).height
        
        notificationView.frame = CGRect(x: notificationsLeftRightInset,
                                        y: -notificationHeight,
                                        width: notificationWidth,
                                        height: notificationHeight)
        overlayWindow.addHitTestableSubview(notificationView)
        
        let onScreenOffset = notificationHeight + notificationsTopOffset
        var onScreenFrame = notificationView.frame.offsetBy(dx: 0, dy: onScreenOffset)
        
        if #available(iOS 11.0, *) {
            onScreenFrame = onScreenFrame.offsetBy(dx: 0, dy: overlayWindow.safeAreaInsets.top)
        }
        
        UIView.animate(withDuration: notificationAnimationDuration,
                       delay: 0.0,
                       options: [.beginFromCurrentState, .curveEaseInOut],
                       animations: {
            self.shadowView.alpha = 1
            self.currentNotificationView?.alpha = 0
            notificationView.frame = onScreenFrame
        }) { _ in
            self.currentNotificationView?.removeFromSuperview()
            self.currentNotificationView = notificationView
            self.hidePerformer.perform(afterDelay: self.notificationDisplayDuration)
        }
    }
    
    /// Спрятать вьюху нотификации с анимацией
    ///
    /// - Parameter notificationView: вьюха нотификации
    fileprivate func hideNotificationView(notificationView: UIView) {
        
        hidePerformer.cancelDelayedPerform()
        
        let offScreenOffset = -(notificationView.height + notificationsTopOffset)
        let offScreenFrame = notificationView.frame.offsetBy(dx: 0, dy: offScreenOffset)
        
        UIView.animate(withDuration: notificationAnimationDuration,
                       delay: 0.0,
                       options: [.beginFromCurrentState, .curveEaseInOut],
                       animations: {
            self.shadowView.alpha = 0
            notificationView.frame = offScreenFrame
        }) { _ in
            notificationView.removeFromSuperview()
            if self.currentNotificationView == notificationView {
                self.overlayWindow.isHidden = true
            }
        }
    }
    
    //MARK: - Открытые методы
    
    /// Показать нотификацию с текстом
    ///
    /// - Parameters:
    ///   - text: текст
    ///   - conversation: диалог, в который пришло сообщение
    ///   - tapHandler: обработчик тапа по нотификации
    func showNotification(with text: String, in conversation: Conversation, tapHandler: ChatNotificationViewTapHandler? = nil) {
        
        let notificationView = ChatTextNotificationView.loadNib()
        notificationView.autoresizingMask = [.flexibleWidth, .flexibleBottomMargin]
        notificationView.preferredMaxLayoutWidth = notificationWidth
        notificationView.text = text
        notificationView.tapHandler = tapHandler
        
        addHandleTap(to: notificationView)
        addSwipeToHide(to: notificationView)
        showNotificationView(notificationView: notificationView)
    }
    
    /// Показать нотификацию о новом сообщении
    ///
    /// - Parameters:
    ///   - message: сообщение
    ///   - conversation: диалог, в который пришло сообщение
    ///   - tapHandler: обработчик тапа по нотификации
    func showNotification(for message: ChatMessage, in conversation: Conversation, tapHandler: ChatNotificationViewTapHandler? = nil) {
        
        let notificationView = ChatMessageNotificationView.loadNib()
        notificationView.autoresizingMask = [.flexibleWidth, .flexibleBottomMargin]
        
        if let placeholderName = conversation.avatarPlaceholderIconName {
            notificationView.avatar = UIImage(named: placeholderName)
        }
        if let avatarUrl = conversation.avatarIconUrl {
            notificationView.setAvatar(with: avatarUrl)
        }
        
        notificationView.recipient = conversation.title
        notificationView.message = message.content.previewString
        notificationView.tapHandler = tapHandler
        
        addHandleTap(to: notificationView)
        addSwipeToHide(to: notificationView)
        showNotificationView(notificationView: notificationView)
    }
}

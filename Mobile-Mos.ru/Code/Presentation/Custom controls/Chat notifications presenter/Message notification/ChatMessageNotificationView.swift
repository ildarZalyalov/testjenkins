//
//  ChatMessageNotificationView.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 08.09.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Вьюха нотификации о сообщении из чата
class ChatMessageNotificationView: UIView, ChatNotificationView {
    
    fileprivate static let nibFile = UINib(nibName: String(describing: ChatMessageNotificationView.self), bundle: nil)
    
    static func loadNib() -> ChatMessageNotificationView {
        return ChatMessageNotificationView.nibFile.instantiate(withOwner: nil, options: nil).first as! ChatMessageNotificationView
    }
    
    @IBOutlet fileprivate weak var avatarView: UIImageView!
    @IBOutlet fileprivate weak var recipientLabel: UILabel!
    @IBOutlet fileprivate weak var messageLabel: UILabel!
    
    var tapHandler: (() -> ())?
    
    /// Картинка с аватаркой
    var avatar: UIImage? {
        get { return avatarView.image }
        set { avatarView.image = newValue }
    }
    
    /// Имя отправителя сообщения (название диалога)
    var recipient: String? {
        didSet {
            recipientLabel.text = recipient
        }
    }
    
    /// Текстовое превью сообщения
    var message: String? {
        didSet {
            messageLabel.text = message
        }
    }
    
    /// Загрузить аватар по url
    ///
    /// - Parameter url: url аватара
    func setAvatar(with url: URL) {
        avatarView.loadAndDisplayImage(from: url)
    }
}

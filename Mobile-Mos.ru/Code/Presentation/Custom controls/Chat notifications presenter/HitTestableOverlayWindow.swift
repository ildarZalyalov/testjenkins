//
//  HitTestableOverlayWindow.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 07.09.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Окно, способное форвардить события главному окну приложения
class HitTestableOverlayWindow: UIWindow {
    
    /// Главный контроллер для нашего окна, который форвардит настройки из главного контроллера главного окна приложения
    fileprivate class RootViewController: UIViewController {
        
        var topViewController: UIViewController? {
            
            guard let window = UIApplication.shared.delegate?.window else {
                return nil
            }
            
            return window?.rootViewController
        }
        
        override var shouldAutorotate: Bool {
            return topViewController?.shouldAutorotate ?? super.shouldAutorotate
        }
        
        override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
            return topViewController?.supportedInterfaceOrientations ?? super.supportedInterfaceOrientations
        }
        
        override var preferredStatusBarStyle: UIStatusBarStyle {
            return topViewController?.preferredStatusBarStyle ?? super.preferredStatusBarStyle
        }
        
        override var prefersStatusBarHidden: Bool {
            return topViewController?.prefersStatusBarHidden ?? super.prefersStatusBarHidden
        }
    }
    
    fileprivate var hitTestableViewsHashes = Set<Int>()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    fileprivate func commonInit() {
        rootViewController = RootViewController()
    }
    
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        
        if let hitView = super.hitTest(point, with: event),
            hitTestableViewsHashes.contains(hitView.hashValue) {
            return hitView
        }
        else {
            
            guard let window = UIApplication.shared.delegate?.window else {
                return nil
            }
            
            return window?.hitTest(point, with: event)
        }
    }
    
    /// Добавить вьюху, которая будет принимать события (нажатия), а не пропускать их
    ///
    /// - Parameter view: вьюха
    func addHitTestableSubview(_ view: UIView) {
        addSubview(view)
        hitTestableViewsHashes.insert(view.hashValue)
    }
    
    override func willRemoveSubview(_ subview: UIView) {
        super.willRemoveSubview(subview)
        if hitTestableViewsHashes.contains(subview.hashValue) {
            hitTestableViewsHashes.remove(subview.hashValue)
        }
    }
}

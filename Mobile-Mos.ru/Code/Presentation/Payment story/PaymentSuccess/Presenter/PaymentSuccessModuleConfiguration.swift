//
//  PaymentSuccessModuleConfiguration.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 26.01.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Конфигурация модуля успеха оплаты 
struct PaymentSuccessModuleConfiguration {
    
    /// Информация о платеже
    var paymentConfiguration: PaymentProcessConfiguration
    
    /// Запрос на старт платежа
    var paymentRequest: PaymentRequest?
    
    /// Выбранный способ платежа
    var selectedProvider: PaymentProvider?
    
    init(configuration: PaymentProcessConfiguration) {
        paymentConfiguration = configuration
    }
}

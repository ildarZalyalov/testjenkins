//
//  PaymentSuccessPresenter.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 26.01.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

class PaymentSuccessPresenter: PaymentSuccessViewOutput {
    
    weak var view: PaymentSuccessViewInput!
    var router: PaymentSuccessRouterInput!
    
    var moduleConfiguration: PaymentSuccessModuleConfiguration!
    var control: ChatMessagePaymentControl {
        return moduleConfiguration.paymentConfiguration.control
    }
    
    var priceFormatter: NumberFormatter!
    
    let comissionToRublesConvertMultiplier: Double = 100
    
    //MARK: - Приватный метод
    
    func comission(for cardType: PaymentCardType?) -> Int64 {
        let paymentConfiguration = moduleConfiguration.paymentConfiguration
        guard let payment = paymentConfiguration.paymentBatch?.payments.first else { return 0 }
        return payment.comission(for: cardType) ?? 0
    }
    
    //MARK: - PaymentSuccessViewOutput
    
    func setupInitialState() {
        
        let conversation = moduleConfiguration.paymentConfiguration.conversation
        
        view.displayConversationAvatar(with: conversation.avatarIconUrl, and: conversation.avatarPlaceholderIconName)
        view.setup(with: conversation.style)
        
        if let paymentMethod = moduleConfiguration.selectedProvider {
            
            if paymentMethod.bankIconName == nil, let logoUrl = paymentMethod.bankIconUrl {
                view.displayPaymentMethod(with: paymentMethod.title, and: logoUrl)
            }
            else {
                view.displayPaymentMethod(with: paymentMethod.title, and: paymentMethod.bankIconName)
            }
        }
        
        let subject = control.subject + StringsHelper.paymentSuccessSubjectSuffix
        let paymentCard = moduleConfiguration.paymentRequest?.bankCard
        let comissionValue = comission(for: paymentCard?.type)
        let comissionInRubles = Double(comissionValue) / comissionToRublesConvertMultiplier
        let sum = Double(control.sum) + comissionInRubles
        
        guard let sumString = priceFormatter.string(from: NSNumber(value: sum)) else { return }
        
        var comissionString = String()
        if comissionValue == 0 {
            comissionString = StringsHelper.paymentNoComissionString.lowercased()
        }
        else if let formattedComission = priceFormatter.string(from: NSNumber(value: comissionInRubles)) {
            comissionString = StringsHelper.paymentSuccessComissionPrefix + formattedComission
        }
        
        view.displayPaymentInfo(with: subject, sum: sumString, comission: comissionString)
    }
    
    func configure(with configuration: PaymentSuccessModuleConfiguration) {
        self.moduleConfiguration = configuration
    }
    
    func didPressConfirm() {
        router.closeModule(completion: nil)
    }
}

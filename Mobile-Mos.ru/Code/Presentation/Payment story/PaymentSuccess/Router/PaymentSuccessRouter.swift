//
//  PaymentSuccessRouter.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 26.01.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

class PaymentSuccessRouter: PaymentSuccessRouterInput {
    
    weak var transitionHandler: UIViewController!
    
    let unwindToChatSegueIdentifier = "unwindToChat"
    
    //MARK: - PaymentSuccessRouterInput
    
    func closeModule(completion: (() -> ())?) {
        transitionHandler.performSegue(withIdentifier: unwindToChatSegueIdentifier, sender: self)
        transitionHandler.transitionCoordinator?.animate(alongsideTransition: nil, completion: { _ in
            completion?()
        })
    }
}

//
//  PaymentSuccessRouterInput.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 26.01.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

protocol PaymentSuccessRouterInput: class {

    func closeModule(completion: (() -> ())?)
}

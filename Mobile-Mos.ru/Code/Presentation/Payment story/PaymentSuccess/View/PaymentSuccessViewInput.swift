//
//  PaymentSuccessViewInput.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 26.01.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

protocol PaymentSuccessViewInput: class {
    
    func setup(with style: ConversationStyle)
    
    func displayConversationAvatar(with avatarUrl: URL?, and avatarPlaceholderName: String?)
    
    func displayPaymentInfo(with subjectTitle: String, sum: String, comission: String)
    
    func displayPaymentMethod(with title: String, and iconName: String?)
    
    func displayPaymentMethod(with title: String, and iconUrl: URL?)
}

//
//  PaymentSuccessViewController.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 26.01.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import UIKit

class PaymentSuccessViewController: BaseViewController, PaymentSuccessViewInput, ConfigurableModuleController {
    
    var output: PaymentSuccessViewOutput!
    
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var paymentSubjectLabel: UILabel!
    @IBOutlet weak var paymentMethodIconView: UIImageView!
    @IBOutlet weak var paymentMethodTitleLabel: UILabel!
    @IBOutlet weak var paymentSumLabel: UILabel!
    @IBOutlet weak var paymentComissionLabel: UILabel!
    @IBOutlet weak var closeButton: UIButton!
    
    let subjectLabelLineHeightMultiple: CGFloat = 1.3
    
    let unknownBankLogo: UIImage = #imageLiteral(resourceName: "paymentUnknownBankLogo")
    
    //MARK: - Методы
    
    override func viewDidLoad() {
        super.viewDidLoad()
        output.setupInitialState()
    }
    
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    @IBAction func pressedConfirm() {
        output.didPressConfirm()
    }
    
    //MARK: - ConfigurableModuleController
    
    func configureModule(with object: Any) {
        if let configuration = object as? PaymentSuccessModuleConfiguration {
            output.configure(with: configuration)
        }
    }
    
    //MARK: - PaymentSuccessViewInput
    
    func setup(with style: ConversationStyle) {
        closeButton.tintColor = style.controlsTextColor
    }
    
    func displayConversationAvatar(with avatarUrl: URL?, and avatarPlaceholderName: String?) {
        
        avatarImageView.image = nil
        
        var placeholderImage: UIImage?
        if let placeholderName = avatarPlaceholderName {
            placeholderImage = UIImage(named: placeholderName)
        }
        
        avatarImageView.loadAndDisplayImage(from: avatarUrl, placeholder: placeholderImage)
    }
    
    func displayPaymentInfo(with subjectTitle: String, sum: String, comission: String) {
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineHeightMultiple = subjectLabelLineHeightMultiple
        paragraphStyle.alignment = .center
        
        let attributes: [NSAttributedStringKey : Any] = [NSAttributedStringKey.font : paymentSubjectLabel.font, NSAttributedStringKey.paragraphStyle : paragraphStyle]
        let subjectString = NSAttributedString(string: subjectTitle, attributes: attributes)
        
        paymentSubjectLabel.attributedText = subjectString
        paymentSumLabel.text = sum
        paymentComissionLabel.text = comission
    }
    
    func displayPaymentMethod(with title: String, and iconName: String?) {
        
        paymentMethodTitleLabel.text = title
        
        if let name = iconName {
            paymentMethodIconView.image = UIImage(named: name)
        }
        else {
            paymentMethodIconView.image = unknownBankLogo
        }
    }
    
    func displayPaymentMethod(with title: String, and iconUrl: URL?) {
        
        paymentMethodTitleLabel.text = title
        
        if let url = iconUrl {
            paymentMethodIconView.loadAndDisplayImage(from: url)
        }
        else {
            paymentMethodIconView.image = unknownBankLogo
        }
    }
}

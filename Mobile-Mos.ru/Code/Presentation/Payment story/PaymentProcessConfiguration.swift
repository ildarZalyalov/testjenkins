//
//  PaymentProcessConfiguration.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 23.05.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Конфигурация процесса обработки платежа
struct PaymentProcessConfiguration {
    
    /// Информация об оплате
    var control: ChatMessagePaymentControl
    
    /// Текущий чат
    var conversation: Conversation
    
    /// Данные для коллбэк запроса (если есть)
    var callbackData: String?
    
    /// Сообщение, с которым связан контрол (если есть)
    var message: ChatMessage?
    
    /// Вложенное сообщение, если сообщение является каруселью
    var embeddedMessage: ChatMessage?
    
    /// Способ оплаты
    var paymentMethod: ChatPaymentMethod = .bankCard
    
    /// Пакет платежей
    var paymentBatch: PaymentBatch?
    
    /// Набор платежных инструментов
    var paymentTools: [PaymentCard] = []
    
    /// Фамилия, имя и отчество плательщика
    var clientName: PaymentClientName?
    
    /// Адрес электронной почты клиента
    var clientEmail: String = String()
    
    /// Обработчик результата работы контрола оплаты
    weak var controlResultHandler: ChatCustomControlResultHandler?
    
    /// Делегат процесса обработки платежа
    weak var paymentDelegate: PaymentProcessDelegate?
    
    init(control: ChatMessagePaymentControl,
         conversation: Conversation,
         callbackData: String? = nil) {
        
        self.control = control
        self.conversation = conversation
        self.callbackData = callbackData
    }
}

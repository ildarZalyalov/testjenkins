//
//  PaymentMethodSelectionInteractorInput.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 23.01.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

protocol PaymentMethodSelectionInteractorInput: class {
    
    func renamePaymentCard(with cardId: Int64, batchId: String, newName: String)
    
    func deletePaymentCard(with cardId: Int64, batchId: String)
}

//
//  PaymentMethodSelectionInteractorOutput.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 23.01.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

protocol PaymentMethodSelectionInteractorOutput: class {
    
    func didFinishRenamingPaymentCard(with cardId: Int64, result: PaymentOperationRequestResult)
    
    func didFinishDeletingPaymentCard(with cardId: Int64, result: PaymentOperationRequestResult)
}

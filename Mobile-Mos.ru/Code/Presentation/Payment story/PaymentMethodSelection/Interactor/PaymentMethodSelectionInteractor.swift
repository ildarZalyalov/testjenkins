//
//  PaymentMethodSelectionInteractor.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 23.01.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

class PaymentMethodSelectionInteractor: PaymentMethodSelectionInteractorInput {
    
    weak var output: PaymentMethodSelectionInteractorOutput!
    
    var paymentService: PaymentService!
    
    //MARK: - PaymentMethodSelectionInteractorInput
    
    func renamePaymentCard(with cardId: Int64, batchId: String, newName: String) {
        let request = PaymentToolRenameRequest(newTitle: newName)
        paymentService.renamePaymentCard(with: cardId, batchId: batchId, request: request) { [weak self] result in
            self?.output.didFinishRenamingPaymentCard(with: cardId, result: result)
        }
    }
    
    func deletePaymentCard(with cardId: Int64, batchId: String) {
        paymentService.deletePaymentCard(with: cardId, batchId: batchId) { [weak self] result in
            self?.output.didFinishDeletingPaymentCard(with: cardId, result: result)
        }
    }
}

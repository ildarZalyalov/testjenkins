//
//  PaymentMethodSelectionModuleConfigurator.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 23.01.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation
import UIKit

class PaymentMethodSelectionModuleConfigurator: BaseModuleConfigurator {
    
    override func configureModule(for view: UIViewController) {
        
        let viewController = view as! PaymentMethodSelectionViewController
        let presenter = PaymentMethodSelectionPresenter()
        let interactor = PaymentMethodSelectionInteractor()
        let router = PaymentMethodSelectionRouter()
        
        let priceFormatter = NumberFormatter()
        priceFormatter.usesGroupingSeparator = true
        priceFormatter.numberStyle = .currency
        priceFormatter.minimumFractionDigits = 0
        priceFormatter.maximumFractionDigits = 2
        priceFormatter.locale = Locale(identifier: "ru_RU")
        
        let validator = BankCardValidatorImplementation()
        let dataSourceFactory = PaymentMethodDataSourceFactoryImplementation()
        dataSourceFactory.validator = validator
        dataSourceFactory.priceFormatter = priceFormatter
        
        let storyboardFactory = StoryboardFactoryImplementation()
        let commonAlertFactory = CommonAlertsFactoryImplementation()
        let alertsFactory = PaymentMethodSelectionAlertFactoryImplementation()
        
        let paymentService = UIApplication.shared.serviceBuilder.getPaymentService()
        
        viewController.output = presenter
        
        presenter.view = viewController
        presenter.interactor = interactor
        presenter.router = router
        presenter.dataSourceFactory = dataSourceFactory
        
        interactor.output = presenter
        interactor.paymentService = paymentService
        
        router.transitionHandler = viewController
        router.alertTransitionHandler = viewController
        router.toOtherModulesTransitionHandler = viewController
        router.storyboardFactory = storyboardFactory
        router.commonAlertsFactory = commonAlertFactory
        router.alertsFactory = alertsFactory
    }
}

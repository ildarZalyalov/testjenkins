//
//  PaymentMethodSelectionPresenter.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 23.01.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

class PaymentMethodSelectionPresenter: PaymentMethodSelectionViewOutput, PaymentMethodSelectionInteractorOutput, PaymentMethodSelectionAlertFactoryActionEventHandler {
    
    weak var view: PaymentMethodSelectionViewInput!
    var router: PaymentMethodSelectionRouterInput!
    var interactor: PaymentMethodSelectionInteractorInput!
    
    var moduleConfiguration: PaymentProcessConfiguration!
    
    var dataSourceFactory: PaymentMethodDataSourceFactory!
    var selectionItems: [String : PaymentCard]!
    
    var deletedToolId: String?
    var renamedToolId: String?
    
    var processingBeginTime = Date()
    let processingMinimumDuration: TimeInterval = 1
    
    //MARK: - Приватные методы
    
    func display(error: Error) {
        if error.isNoInternetConnectionError() {
            router.showNoInternetConnectionAlert()
        }
        else {
            router.showUnknownErrorAlert()
        }
    }
    
    func perform(after delay: TimeInterval, work: @escaping () -> ()) {
        let deadline: DispatchTime = .now() + delay
        DispatchQueue.main.asyncAfter(deadline: deadline, execute: work)
    }
    
    func canHandleFinishProcessing(with completion: @escaping () -> ()) -> Bool {
        
        let durationDifference = processingBeginTime.timeIntervalSinceNow + processingMinimumDuration
        
        guard durationDifference <= 0 else {
            perform(after: durationDifference, work: completion)
            return false
        }
        
        return true
    }
    
    func refreshPaymentTools() {
        
        let configuration = dataSourceFactory.buildDataSourceConfiguration(from: moduleConfiguration.paymentTools, for: moduleConfiguration.paymentBatch)
        selectionItems = configuration.selectionItems
        
        view.displayPaymentMethods(with: configuration.dataSource)
    }
    
    //MARK: - PaymentMethodSelectionViewOutput
    
    func setupInitialState() {
        refreshPaymentTools()
    }
    
    func configure(with configuration: PaymentProcessConfiguration) {
        self.moduleConfiguration = configuration
    }
    
    func didPressCancel() {
        router.closeModule(completion: nil)
    }
    
    func didSelectPaymentTool(with itemId: String) {
        guard let paymentTool = selectionItems[itemId] else { return }
        router.showDisplayPaymentToolModule(paymentTool, for: moduleConfiguration)
    }
    
    func didExitEditingMode() {}
    
    func didPressDeletePaymentTool(with itemId: String) {
        deletedToolId = itemId
        router.showConfirmDeletePaymentToolModule(eventHandler: self)
    }
    
    func didPressRenamePaymentTool(with itemId: String) {
        renamedToolId = itemId
        router.showRenamePaymentToolModule(eventHandler: self)
    }
    
    func didPressAddPaymentTool() {
        router.showAddPaymentToolModule(for: moduleConfiguration)
    }
    
    //MARK: - PaymentMethodSelectionInteractorOutput
    
    func didFinishRenamingPaymentCard(with cardId: Int64, result: PaymentOperationRequestResult) {
        
        guard canHandleFinishProcessing(with: {
            self.didFinishRenamingPaymentCard(with: cardId, result: result)
        }) else { return }
        
        view.setExitEditingModeEnabled(true)
        router.hideProcessingModule { [weak self] in
            
            switch result {
            case .success(let operationResult):
                switch operationResult.status {
                case .success:
                    guard let strongSelf = self else { break }
                    let tools = strongSelf.moduleConfiguration.paymentTools
                    guard let toolIndex = tools.index(where: { $0.cardId == cardId }) else { break }
                    var tool = tools[toolIndex]
                    tool.title = operationResult.newTitle
                    strongSelf.moduleConfiguration.paymentTools[toolIndex] = tool
                    strongSelf.refreshPaymentTools()
                default:
                    self?.router.showUnknownErrorAlert()
                }
            case .failure(let error):
                self?.display(error: error)
            }
        }
    }
    
    func didFinishDeletingPaymentCard(with cardId: Int64, result: PaymentOperationRequestResult) {
        
        guard canHandleFinishProcessing(with: {
            self.didFinishDeletingPaymentCard(with: cardId, result: result)
        }) else { return }
        
        view.setExitEditingModeEnabled(true)
        router.hideProcessingModule { [weak self] in
            
            switch result {
            case .success(let operationResult):
                switch operationResult.status {
                case .success:
                    guard let strongSelf = self else { break }
                    guard let toolId = strongSelf.deletedToolId else { break }
                    strongSelf.view.deletePaymentTool(with: toolId)
                default:
                    self?.router.showUnknownErrorAlert()
                }
            case .failure(let error):
                self?.display(error: error)
            }
        }
    }
    
    //MARK: - PaymentMethodSelectionAlertFactoryActionEventHandler
    
    func didSelectToRenamePaymentMethod(to newName: String) {
        
        guard let toolId = renamedToolId else { return }
        guard let cardId = selectionItems[toolId]?.cardId else { return }
        guard let batchId = moduleConfiguration.paymentBatch?.batchId else { return }
        
        view.setExitEditingModeEnabled(false)
        processingBeginTime = Date()
        router.showProcessingModule()
        
        interactor.renamePaymentCard(with: cardId, batchId: batchId, newName: newName)
    }
    
    func didConfirmDeletingPaymentMethod() {
        
        guard let toolId = deletedToolId else { return }
        guard let cardId = selectionItems[toolId]?.cardId else { return }
        guard let batchId = moduleConfiguration.paymentBatch?.batchId else { return }
        
        view.setExitEditingModeEnabled(false)
        processingBeginTime = Date()
        router.showProcessingModule()
        
        interactor.deletePaymentCard(with: cardId, batchId: batchId)
    }
}

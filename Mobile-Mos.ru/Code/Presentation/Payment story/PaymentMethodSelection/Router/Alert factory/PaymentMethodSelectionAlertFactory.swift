//
//  PaymentMethodSelectionAlertFactory.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 30.05.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

protocol PaymentMethodSelectionAlertFactoryActionEventHandler {
    
    func didSelectToRenamePaymentMethod(to newName: String)
    
    func didConfirmDeletingPaymentMethod()
}

protocol PaymentMethodSelectionAlertFactory {
    
    func getRenamePaymentMethodAlert(with eventHandler: PaymentMethodSelectionAlertFactoryActionEventHandler?) -> UIViewController
    
    func getDeletePaymentMethodConfirmActionSheet(with eventHandler: PaymentMethodSelectionAlertFactoryActionEventHandler?) -> UIViewController
}

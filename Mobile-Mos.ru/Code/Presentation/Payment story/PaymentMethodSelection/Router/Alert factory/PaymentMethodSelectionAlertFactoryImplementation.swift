//
//  PaymentMethodSelectionAlertFactoryImplementation.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 30.05.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

class PaymentMethodSelectionAlertFactoryImplementation: PaymentMethodSelectionAlertFactory {
    
    class TextFieldDependantAction: UIAlertAction {
        
        @objc
        func textDidChange(field: UITextField) {
            isEnabled = !(field.text?.isEmpty ?? true)
        }
    }
    
    let textFieldFontSize: CGFloat = 17
    
    //MARK: - PaymentMethodSelectionAlertFactory
    
    func getRenamePaymentMethodAlert(with eventHandler: PaymentMethodSelectionAlertFactoryActionEventHandler?) -> UIViewController {
        
        let alert = UIAlertController(title: "Введите новое название для карты", message: nil, preferredStyle: .alert)
        
        let saveAction = TextFieldDependantAction(title: "Переименовать", style: .default, handler: { (action: UIAlertAction!) in
            guard let newName = alert.textFields?.first?.text else { return }
            eventHandler?.didSelectToRenamePaymentMethod(to: newName)
        })
        saveAction.isEnabled = false
        
        let fontSize = textFieldFontSize
        alert.addTextField { field in
            
            field.font = UIFont(customName: .graphikLCGRegular, size: fontSize)
            field.placeholder = "Название карты"
            field.autocapitalizationType = .sentences
            
            field.addTarget(saveAction, action: #selector(TextFieldDependantAction.textDidChange(field:)), for: .editingChanged)
        }
        
        alert.addAction(saveAction)
        alert.addAction(UIAlertAction(title: "Отменить", style: .cancel, handler: nil))
        
        return alert
    }
    
    func getDeletePaymentMethodConfirmActionSheet(with eventHandler: PaymentMethodSelectionAlertFactoryActionEventHandler?) -> UIViewController {
        
        let alert = UIAlertController(title: "Вы уверены?", message: nil, preferredStyle: .actionSheet)

        alert.addAction(UIAlertAction(title: "Удалить", style: .destructive, handler: { (alert: UIAlertAction!) in
            eventHandler?.didConfirmDeletingPaymentMethod()
        }))
        alert.addAction(UIAlertAction(title: "Отменить", style: .cancel, handler: nil))
        
        return alert
    }
}

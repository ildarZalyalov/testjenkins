//
//  PaymentMethodSelectionRouterInput.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 23.01.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

protocol PaymentMethodSelectionRouterInput: class {

    func closeModule(completion: (() -> ())?)
    
    func showUnknownErrorAlert()
    
    func showNoInternetConnectionAlert()
    
    func showErrorAlert(with message: String)
    
    func showAddPaymentToolModule(for configuration: PaymentProcessConfiguration)
    
    func showDisplayPaymentToolModule(_ paymentTool: PaymentCard, for configuration: PaymentProcessConfiguration)
    
    func showRenamePaymentToolModule(eventHandler: PaymentMethodSelectionAlertFactoryActionEventHandler?)
    
    func showConfirmDeletePaymentToolModule(eventHandler: PaymentMethodSelectionAlertFactoryActionEventHandler?)
    
    func showProcessingModule()
    
    func hideProcessingModule(completion: (() -> ())?)
}

//
//  PaymentMethodSelectionRouter.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 23.01.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

class PaymentMethodSelectionRouter: PaymentMethodSelectionRouterInput {
    
    weak var transitionHandler: UIViewController!
    weak var alertTransitionHandler: UIViewController!
    weak var toOtherModulesTransitionHandler: DataTransferModuleController!
    
    var commonAlertsFactory: CommonAlertsFactory!
    var alertsFactory: PaymentMethodSelectionAlertFactory!
    var storyboardFactory: StoryboardFactory!
    
    let unwindToChatSegueIdentifier = "unwindToChat"
    let showAddPaymentToolSegueIdentifier = "showAddPaymentTool"
    let showPaymentToolSegueIdentifier = "showPaymentTool"
    
    //MARK: - PaymentMethodSelectionRouterInput
    
    func closeModule(completion: (() -> ())?) {
        transitionHandler.performSegue(withIdentifier: unwindToChatSegueIdentifier, sender: self)
        transitionHandler.transitionCoordinator?.animate(alongsideTransition: nil, completion: { _ in
            completion?()
        })
    }
    
    func showUnknownErrorAlert() {
        let alert = commonAlertsFactory.getErrorAlert(with: StringsHelper.paymentUnknownErrorMessage)
        transitionHandler.present(alert, animated: true, completion: nil)
    }
    
    func showNoInternetConnectionAlert() {
        let alert = commonAlertsFactory.getErrorAlert(with: StringsHelper.noInternetConnectionErrorText)
        transitionHandler.present(alert, animated: true, completion: nil)
    }
    
    func showErrorAlert(with message: String) {
        let alert = commonAlertsFactory.getErrorAlert(with: message)
        transitionHandler.present(alert, animated: true, completion: nil)
    }
    
    func showAddPaymentToolModule(for configuration: PaymentProcessConfiguration) {
        toOtherModulesTransitionHandler.performSegue(with: showAddPaymentToolSegueIdentifier, sender: self) { controller in
            let moduleConfiguration = AddPaymentMethodModuleConfiguration(paymentConfiguration: configuration, isAddingFirstPaymentTool: false)
            controller.configureModule(with: moduleConfiguration)
        }
    }
    
    func showDisplayPaymentToolModule(_ paymentTool: PaymentCard, for configuration: PaymentProcessConfiguration) {
        
        toOtherModulesTransitionHandler.performSegue(with: showPaymentToolSegueIdentifier, sender: self) { controller in
            
            var moduleConfiguration = AddPaymentMethodModuleConfiguration(paymentConfiguration: configuration, isAddingFirstPaymentTool: false)
            moduleConfiguration.selectedPaymentTool = paymentTool
            
            controller.configureModule(with: moduleConfiguration)
        }
    }
    
    func showRenamePaymentToolModule(eventHandler: PaymentMethodSelectionAlertFactoryActionEventHandler?) {
        let alert = alertsFactory.getRenamePaymentMethodAlert(with: eventHandler)
        alertTransitionHandler.present(alert, animated: true, completion: nil)
    }
    
    func showConfirmDeletePaymentToolModule(eventHandler: PaymentMethodSelectionAlertFactoryActionEventHandler?) {
        let alert = alertsFactory.getDeletePaymentMethodConfirmActionSheet(with: eventHandler)
        alertTransitionHandler.present(alert, animated: true, completion: nil)
    }
    
    func showProcessingModule() {
        
        let storyboard = storyboardFactory.getStoryboard(with: .payment)
        let processingController = storyboard.instantiateViewController(for: .paymentMethodsProcessing)
        
        processingController.modalPresentationStyle = .overCurrentContext
        processingController.modalTransitionStyle = .crossDissolve
        processingController.modalPresentationCapturesStatusBarAppearance = false
        
        transitionHandler.definesPresentationContext = true
        transitionHandler.present(processingController, animated: true, completion: nil)
    }
    
    func hideProcessingModule(completion: (() -> ())?) {
        transitionHandler.dismiss(animated: true, completion: completion)
        transitionHandler.definesPresentationContext = false
    }
}

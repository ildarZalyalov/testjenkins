//
//  PaymentMethodSelectionViewController.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 23.01.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import UIKit

class PaymentMethodSelectionViewController: BaseViewController, PaymentMethodSelectionViewInput, ConfigurableModuleController, UITableViewDelegate {
    
    var output: PaymentMethodSelectionViewOutput!
    
    var tableViewDataDisplayManager: TableViewDataSource!
    
    @IBOutlet var cancelBarButtonItem: UIBarButtonItem!
    @IBOutlet var editModeBarButtonItem: UIBarButtonItem!
    var endEditingBarButtonItem: UIBarButtonItem!
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var addPaymentToolButtonContainer: UIView!
    @IBOutlet weak var addPaymentToolButton: UIButton!
    @IBOutlet weak var editModeButtonsContainer: UIView!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var renameButton: UIButton!
    @IBOutlet weak var emptyContentView: UIView!
    
    let navigationItemsFontSize: CGFloat = 17
    
    //MARK: - Методы
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        endEditingBarButtonItem = UIBarButtonItem(title: StringsHelper.tableViewEndEditingButtonTitle, style: .done, target: self, action: #selector(cancelEditMode))
        
        let attributes: [NSAttributedStringKey : Any] = [NSAttributedStringKey.font : UIFont(customName: .graphikLCGRegular, size: navigationItemsFontSize) as Any, NSAttributedStringKey.foregroundColor : UIColorPalette.paymentBarButtonColor]
        let disabledAttributes: [NSAttributedStringKey : Any] = [NSAttributedStringKey.font : UIFont(customName: .graphikLCGRegular, size: navigationItemsFontSize) as Any, NSAttributedStringKey.foregroundColor : UIColorPalette.paymentDisabledBarButtonColor]
        
        let updateStyle: (UIBarButtonItem) -> () = { item in
            item.setTitleTextAttributes(attributes, for: .normal)
            item.setTitleTextAttributes(attributes, for: .highlighted)
            item.setTitleTextAttributes(disabledAttributes, for: .disabled)
        }
        
        if let items = navigationItem.leftBarButtonItems {
            items.forEach(updateStyle)
        }
        if let items = navigationItem.rightBarButtonItems {
            items.forEach(updateStyle)
        }
        
        let doneAttributes: [NSAttributedStringKey : Any] = [NSAttributedStringKey.font : UIFont(customName: .graphikLCGMedium, size: navigationItemsFontSize) as Any, NSAttributedStringKey.foregroundColor : UIColorPalette.paymentBarButtonColor]
        let disabledDoneAttributes: [NSAttributedStringKey : Any] = [NSAttributedStringKey.font : UIFont(customName: .graphikLCGMedium, size: navigationItemsFontSize) as Any, NSAttributedStringKey.foregroundColor : UIColorPalette.paymentDisabledBarButtonColor]
        endEditingBarButtonItem.setTitleTextAttributes(doneAttributes, for: .normal)
        endEditingBarButtonItem.setTitleTextAttributes(doneAttributes, for: .highlighted)
        endEditingBarButtonItem.setTitleTextAttributes(disabledDoneAttributes, for: .disabled)
        
        tableView.estimatedRowHeight = 66
        tableView.estimatedSectionHeaderHeight = 1
        tableView.estimatedSectionFooterHeight = 1
        
        tableView.assignEmptyTableFooterToHideEmptyCellsSeparators()
        
        let insets = UIEdgeInsets(top: 0, left: 0, bottom: addPaymentToolButtonContainer.height, right: 0)
        tableView.contentInset = insets
        tableView.scrollIndicatorInsets = insets
        
        output.setupInitialState()
    }
    
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    //MARK: - Helpers
    
    func deselectAllRows() {
        
        guard let selectedRows = tableView.indexPathsForSelectedRows else { return }
        
        for rowIndexPath in selectedRows {
            tableView.deselectRow(at: rowIndexPath, animated: false)
        }
    }
    
    //MARK: - Actions
    
    @IBAction func pressedCancel() {
        output.didPressCancel()
    }
    
    @IBAction func switchToEditMode() {
        
        cancelBarButtonItem.isEnabled = false
        navigationItem.setRightBarButtonItems([endEditingBarButtonItem], animated: true)
        
        deleteButton.isEnabled = false
        renameButton.isEnabled = false
        
        addPaymentToolButtonContainer.isHidden = true
        editModeButtonsContainer.isHidden = false
        
        tableView.setEditing(true, animated: true)
        deselectAllRows()
    }
    
    @IBAction func cancelEditMode() {
        
        let barButtonItems: [UIBarButtonItem] = tableViewDataDisplayManager.dataStructure.isEmpty() ? [] : [editModeBarButtonItem]
        navigationItem.setRightBarButtonItems(barButtonItems, animated: true)
        cancelBarButtonItem.isEnabled = true
        
        addPaymentToolButtonContainer.isHidden = false
        editModeButtonsContainer.isHidden = true
        
        tableView.setEditing(false, animated: true)
        deselectAllRows()
        
        output.didExitEditingMode()
    }
    
    @IBAction func pressedDeleteSelectedPaymentMethod() {
        
        guard let indexPath = tableView.indexPathsForSelectedRows?.first else { return }
        guard let selectedToolCellObject = tableViewDataDisplayManager.dataStructure.cellObject(at: indexPath) as? PaymentMethodCellObject else { return }
        
        output.didPressDeletePaymentTool(with: selectedToolCellObject.itemId)
    }
    
    @IBAction func pressedRenameSelectedPaymentMethod() {
        
        guard let indexPath = tableView.indexPathsForSelectedRows?.first else { return }
        guard let selectedToolCellObject = tableViewDataDisplayManager.dataStructure.cellObject(at: indexPath) as? PaymentMethodCellObject else { return }
        
        output.didPressRenamePaymentTool(with: selectedToolCellObject.itemId)
    }
    
    @IBAction func pressedAddPaymentTool() {
        output.didPressAddPaymentTool()
    }
    
    @IBAction func unwindToPaymentMethodsSelection(_: UIStoryboardSegue) {}
    
    //MARK: - ConfigurableModuleController
    
    func configureModule(with object: Any) {
        if let configuration = object as? PaymentProcessConfiguration {
            output.configure(with: configuration)
        }
    }
    
    //MARK: - NavigationBarCustomizingController
    
    var navigationBarState: NavigationBarState? {
        
        var state = NavigationBarState()
        
        state.isHidden = false
        state.isTranslucent = false
        state.backgroundState = NavigationBarBackgroundState.regular
        state.tintColor = UIColor.black
        state.isShadowHidden = true
        
        let titleTextAttributes = [
            NSAttributedStringKey.font : UIFont(customName: .graphikLCGBold, size: 15) as Any,
            NSAttributedStringKey.foregroundColor : UIColor.black
        ]
        let largeTitleTextAttributes = [
            NSAttributedStringKey.font : UIFont(customName: .graphikLCGBold, size: 25) as Any,
            NSAttributedStringKey.foregroundColor : UIColor.black
        ]
        
        state.titleTextAttributes = titleTextAttributes
        state.largeTitleTextAttributes = largeTitleTextAttributes
        
        return state
    }
    
    //MARK: - PaymentMethodSelectionViewInput

    func displayPaymentMethods(with dataSource: TableViewDataSource) {
        
        tableViewDataDisplayManager = dataSource
        tableViewDataDisplayManager.delegate = self
        tableViewDataDisplayManager.assign(to: tableView)
        
        tableView.reloadData()
    }
    
    func deletePaymentTool(with itemId: String) {
        
        guard let indexPath = tableViewDataDisplayManager.dataStructure.indexPathOfFirstObjectPassing({ ($0 as? PaymentMethodCellObject)?.itemId == itemId }) else { return }
        
        tableViewDataDisplayManager.dataStructure.removeCellObject(at: indexPath)
        tableView.deleteRows(at: [indexPath], with: .right)
        
        if tableViewDataDisplayManager.dataStructure.isEmpty() {
            cancelEditMode()
            emptyContentView.isHidden = false
        }
    }
    
    func setExitEditingModeEnabled(_ enabled: Bool) {
        endEditingBarButtonItem.isEnabled = enabled
    }
    
    //MARK: - UITableViewDelegate
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        
        guard tableView.isEditing else { return indexPath }
        
        deselectAllRows()
        deleteButton.isEnabled = true
        renameButton.isEnabled = true
        
        return indexPath
    }
    
    func tableView(_ tableView: UITableView, willDeselectRowAt indexPath: IndexPath) -> IndexPath? {
        
        guard tableView.isEditing else { return indexPath }
        
        deleteButton.isEnabled = false
        renameButton.isEnabled = false
        
        return indexPath
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard !tableView.isEditing else { return }
        
        tableView.deselectRow(at: indexPath, animated: false)
        
        let dataStructure = tableViewDataDisplayManager.dataStructure
        
        guard let cellObject = dataStructure.cellObject(at: indexPath) as? PaymentMethodCellObject else { return }
        guard cellObject.isValid else { return }
        
        output.didSelectPaymentTool(with: cellObject.itemId)
    }
}

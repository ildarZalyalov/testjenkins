//
//  PaymentMethodSelectionViewInput.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 23.01.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

protocol PaymentMethodSelectionViewInput: class {

    func displayPaymentMethods(with dataSource: TableViewDataSource)
    
    func deletePaymentTool(with itemId: String)
    
    func setExitEditingModeEnabled(_ enabled: Bool)
}

//
//  PaymentMethodCell.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 24.01.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import UIKit

class PaymentMethodCell: UITableViewCell, ConfigurableView {
    
    @IBOutlet weak var methodIconView: UIImageView!
    @IBOutlet weak var methodTitleLabel: UILabel!
    @IBOutlet weak var methodSubtitleLabel: UILabel!
    @IBOutlet weak var disclosureView: UIImageView!
    
    @IBOutlet var titleLabelRegularConstraint: NSLayoutConstraint!
    @IBOutlet var titleLabelNoSubtitleConstraint: NSLayoutConstraint!
    
    let disclosureSize = CGSize(width: 7, height: 12)
    let unknownBankLogo: UIImage = #imageLiteral(resourceName: "paymentUnknownBankLogo")
    let notValidAlpha: CGFloat = 0.5
    let validAlpha: CGFloat = 1
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        contentView.alpha = highlighted ? 0.5 : 1
    }
    
    override func setEditing(_ editing: Bool, animated: Bool) {
        selectionStyle = editing ? .default : .none
        super.setEditing(editing, animated: animated)
        disclosureView.isHidden = editing
    }
    
    //MARK: - ConfigurableView
    
    func configure(with object: Any) {
        
        guard let cellObject = object as? PaymentMethodCellObject else {
            return
        }
        
        if cellObject.iconName == nil, let url = cellObject.iconUrl {
            methodIconView.loadAndDisplayImage(from: url)
        }
        else if let iconName = cellObject.iconName {
            methodIconView.image = UIImage(named: iconName)
        }
        else {
            methodIconView.image = unknownBankLogo
        }
        
        methodTitleLabel.text = cellObject.title
        methodSubtitleLabel.text = cellObject.subtitle
        
        methodSubtitleLabel.isHidden = methodSubtitleLabel.text?.isEmpty ?? true
        methodSubtitleLabel.textColor = cellObject.isValid ? UIColorPalette.paymentToolSubtitleColor : UIColorPalette.paymentToolNotValidSubtitleColor
        methodIconView.alpha = cellObject.isValid ? validAlpha : notValidAlpha
        methodTitleLabel.alpha = cellObject.isValid ? validAlpha : notValidAlpha
        
        titleLabelRegularConstraint.isActive = !methodSubtitleLabel.isHidden
        titleLabelNoSubtitleConstraint.isActive = methodSubtitleLabel.isHidden
    }
}

//
//  PaymentMethodCellObject.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 24.01.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

struct PaymentMethodCellObject: CellObjectWithId {
    
    var itemId: String
    
    var title: String
    
    var subtitle: String?
    
    var iconName: String?
    
    var iconUrl: URL?
    
    var isValid: Bool = true
    
    init(itemId: String, title: String) {
        self.itemId = itemId
        self.title = title
    }
}

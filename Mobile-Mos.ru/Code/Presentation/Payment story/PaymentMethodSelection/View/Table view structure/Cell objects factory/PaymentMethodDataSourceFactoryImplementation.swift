//
//  PaymentMethodDataSourceFactoryImplementation.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 24.01.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

class PaymentMethodDataSourceFactoryImplementation: PaymentMethodDataSourceFactory {
    
    var validator: BankCardValidator!
    var priceFormatter: NumberFormatter!
    
    let comissionToRublesConvertMultiplier: Double = 100
    
    //MARK: - Приватные методы
    
    func comission(for card: PaymentCard, and paymentBatch: PaymentBatch?) -> Int64 {
        guard let payment = paymentBatch?.payments.first else { return 0 }
        return payment.comission(for: card.type) ?? 0
    }
    
    func comissionString(for comissionValue: Int64) -> String? {
        
        var comissionString: String?
        let comissionInRubles = Double(comissionValue) / comissionToRublesConvertMultiplier
        
        if comissionInRubles > 0 {
            comissionString = priceFormatter.string(from: NSNumber(value: comissionInRubles))
            if comissionString != nil {
                comissionString = StringsHelper.paymentComissionPrefix + comissionString!
            }
        }
        else {
            comissionString = StringsHelper.paymentNoComissionString
        }
        
        return comissionString
    }
    
    //MARK: - PaymentMethodDataSourceFactory
    
    func buildDataSourceConfiguration(from paymentTools: [PaymentCard], for paymentBatch: PaymentBatch?) -> PaymentMethodDataSourceConfiguration {
        
        let dataStructure = TableViewDataSourceStructure()
        var selectionItems = [String : PaymentCard]()
        
        dataStructure.appendSection()
        
        for (index, tool) in paymentTools.enumerated() {
            
            let provider = PaymentProvider(card: tool)
            var isValid = false
            if let expiry = tool.cardInfo?.expiryDate {
                isValid = validator.validate(expiry: expiry)
            }
            
            var cellObject = PaymentMethodCellObject(itemId: String(index), title: provider.title)
            cellObject.iconName = provider.bankIconName
            cellObject.iconUrl = provider.bankIconUrl
            cellObject.isValid = isValid
            
            let comissionValue = comission(for: tool, and: paymentBatch)
            cellObject.subtitle = comissionString(for: comissionValue)
            
            dataStructure.appendCellObject(cellObject)
            selectionItems[cellObject.itemId] = tool
        }
        
        let dataSource = TableViewDataSource(with: dataStructure)
        
        return (dataSource, selectionItems)
    }
}

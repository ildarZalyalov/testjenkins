//
//  PaymentMethodDataSourceFactory.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 24.01.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

typealias PaymentMethodDataSourceConfiguration = (dataSource: TableViewDataSource, selectionItems: [String : PaymentCard])

protocol PaymentMethodDataSourceFactory {
    
    func buildDataSourceConfiguration(from paymentTools: [PaymentCard], for paymentBatch: PaymentBatch?) -> PaymentMethodDataSourceConfiguration
}

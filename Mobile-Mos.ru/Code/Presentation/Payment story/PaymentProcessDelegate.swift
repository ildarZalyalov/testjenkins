//
//  PaymentProcessDelegate.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 23.05.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Делегат процесса обработки платежа
protocol PaymentProcessDelegate: class {
    
    /// Был выбран способ оплаты
    ///
    /// - Parameters:
    ///   - paymentMethod: способ оплаты
    ///   - configuration: информация о платеже
    func didSelect(paymentMethod: ChatPaymentMethod, for configuration: PaymentProcessConfiguration)
    
    /// Платеж успешно обработан
    ///
    /// - Parameters:
    ///   - request: запрос на начало платежа
    ///   - configuration: информация о платеже
    ///   - paymentProvider: информация о способе платежа
    func didProcess(request: PaymentRequest, for configuration: PaymentProcessConfiguration, with paymentProvider: PaymentProvider)
}

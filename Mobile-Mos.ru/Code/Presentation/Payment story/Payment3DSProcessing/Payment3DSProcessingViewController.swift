//
//  Payment3DSProcessingViewController.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 31.05.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import UIKit
import WebKit

class Payment3DSProcessingViewController: UIViewController, ConfigurableModuleController, WKNavigationDelegate {
    
    @IBOutlet weak var cancelBarButtonItem: UIBarButtonItem!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var requestBuilder: HTTPRequestBuilder!
    var configuration: Payment3DSModuleConfiguration!
    
    var redirectInfo: PaymentRedirectInfo {
        return configuration.redirectInfo
    }
    
    let navigationItemsFontSize: CGFloat = 17
    let unwindToAddPaymentMethodSegueIdentifier = "unwindToAddPaymentMethod"
    
    var shouldExpectRedirect = false
    var redirectNavigation: WKNavigation?
    
    let webView: WKWebView = {
        
        let preferences = WKPreferences()
        preferences.javaScriptEnabled = true
        
        let scaleScript = "var meta = document.createElement('meta'); meta.setAttribute('name', 'viewport'); meta.setAttribute('content', 'width=device-width, initial-scale=1.0'); document.getElementsByTagName('head')[0].appendChild(meta);"
        let userScript = WKUserScript(source: scaleScript, injectionTime: .atDocumentEnd, forMainFrameOnly: true)
        let userContentController = WKUserContentController()
        userContentController.addUserScript(userScript)
        
        let configuration = WKWebViewConfiguration()
        configuration.preferences = preferences
        configuration.suppressesIncrementalRendering = true
        configuration.selectionGranularity = .character
        configuration.userContentController = userContentController
        
        return WKWebView(frame: CGRect.zero, configuration: configuration)
    }()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        let attributes: [NSAttributedStringKey : Any] = [NSAttributedStringKey.font : UIFont(customName: .graphikLCGRegular, size: navigationItemsFontSize) as Any]
        
        let updateStyle: (UIBarButtonItem) -> () = { item in
            item.setTitleTextAttributes(attributes, for: .normal)
            item.setTitleTextAttributes(attributes, for: .highlighted)
            item.setTitleTextAttributes(attributes, for: .disabled)
        }
        
        updateStyle(cancelBarButtonItem)
        
        webView.translatesAutoresizingMaskIntoConstraints = false
        webView.navigationDelegate = self
        
        view.insertSubview(webView, at: 0)
        view.addConstraints(NSLayoutConstraint.edgesConstraints(for: webView))
        
        loadRedirectUrl()
    }
    
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    @IBAction func didPressCancel() {
        closeModule(completion: nil)
    }
    
    //MARK: - ConfigurableModuleController
    
    func configureModule(with object: Any) {
        if let configuration = object as? Payment3DSModuleConfiguration {
            self.configuration = configuration
        }
    }
    
    //MARK: - Приватные методы
    
    func closeModule(completion: (() -> ())?) {
        performSegue(withIdentifier: unwindToAddPaymentMethodSegueIdentifier, sender: self)
        transitionCoordinator?.animate(alongsideTransition: nil, completion: { _ in
            completion?()
        })
    }
    
    func loadRedirectUrl() {
        
        guard let urlString = redirectInfo.redirectUrl?.absoluteString else { return }
        
        var requestConfiguration = HTTPRequestConfiguration(withMethod: HTTPMethod.POST.rawValue, andUrlString: urlString)
        requestConfiguration.queryStringParameters = redirectInfo.serializeToJson() as? [String : Any]
        
        let request = requestBuilder.request(with: requestConfiguration)
        
        activityIndicator.startAnimating()
        webView.load(request)
    }
    
    //MARK: - WKNavigationDelegate
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        guard shouldExpectRedirect else { return }
        redirectNavigation = navigation
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        activityIndicator.stopAnimating()
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        shouldExpectRedirect = navigationAction.request.httpMethod == HTTPMethod.POST.rawValue &&  navigationAction.request.url == redirectInfo.terminationUrl
        decisionHandler(.allow)
    }
    
    func webView(_ webView: WKWebView, didReceiveServerRedirectForProvisionalNavigation navigation: WKNavigation!) {
        
        guard navigation == redirectNavigation else { return }
        
        let eventHandler = configuration.eventHandler
        
        closeModule {
            eventHandler?.didConfirmPayment()
        }
    }
}

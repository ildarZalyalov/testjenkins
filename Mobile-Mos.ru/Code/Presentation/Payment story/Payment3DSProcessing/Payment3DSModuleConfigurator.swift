//
//  Payment3DSModuleConfigurator.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 31.05.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation
import UIKit

class Payment3DSModuleConfigurator: BaseModuleConfigurator {
    
    override func configureModule(for view: UIViewController) {
        
        let viewController = view as! Payment3DSProcessingViewController
        
        let requestBuilder = HTTPRequestBuilderDefault()
        requestBuilder.parametersEncoder = HTTPRequestParameterEncoderDefault()
        
        viewController.requestBuilder = requestBuilder
    }
}

//
//  Payment3DSModuleConfiguration.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 31.05.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Обработчик событий в модуле 3DS авторизации платежа
protocol Payment3DSModuleEventHandler: class {
    
    /// Пользователь прошел 3DS авторизацию
    func didConfirmPayment()
}

/// Конфигурация модуля 3DS авторизации платежа
struct Payment3DSModuleConfiguration {
    
    /// Информация по редиректу на страницу 3DS авторизации платежа
    let redirectInfo: PaymentRedirectInfo
    
    /// Обработчик событий в модуле
    weak var eventHandler: Payment3DSModuleEventHandler?
    
    init(info: PaymentRedirectInfo) {
        redirectInfo = info
    }
}

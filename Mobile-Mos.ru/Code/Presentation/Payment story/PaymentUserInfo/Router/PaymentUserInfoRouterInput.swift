//
//  PaymentUserInfoRouterInput.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 23.05.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

protocol PaymentUserInfoRouterInput: class {

    func closeModule(completion: (() -> ())?)
    
    func showEmailValidatorErrorAlert()
    
    func showEmailEmptyErrorAlert()
    
    func showFirstNameEmptyErrorAlert()
    
    func showSecondNameEmptyErrorAlert()
    
    func showMiddleNameEmptyErrorAlert()
    
    func showUnknownErrorAlert()
    
    func showNoInternetConnectionAlert()
    
    func showErrorAlert(with message: String)
    
    func showAgreementsModule()
    
    func showPaymentToolsModule(for configuration: PaymentProcessConfiguration)
    
    func showAddPaymentToolModule(for configuration: PaymentProcessConfiguration)
}

//
//  PaymentUserInfoRouter.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 23.05.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation
import SafariServices

class PaymentUserInfoRouter: PaymentUserInfoRouterInput {
    
    weak var transitionHandler: UIViewController!
    weak var toOtherModulesTransitionHandler: DataTransferModuleController!
    
    var commonAlertsFactory: CommonAlertsFactory!
    
    let unwindToChatSegueIdentifier = "unwindToChat"
    
    let showPaymentToolsSegueIdentifier = "showPaymentTools"
    let showAddPaymentToolSegueIdentifier = "showAddPaymentTool"
    
    //MARK: - PaymentUserInfoRouterInput
    
    func closeModule(completion: (() -> ())?) {
        transitionHandler.performSegue(withIdentifier: unwindToChatSegueIdentifier, sender: self)
        transitionHandler.transitionCoordinator?.animate(alongsideTransition: nil, completion: { _ in
            completion?()
        })
    }
    
    func showEmailValidatorErrorAlert() {
        let alert = commonAlertsFactory.getErrorAlert(with: StringsHelper.paymentEmailValidateMessage)
        transitionHandler.present(alert, animated: true, completion: nil)
    }
    
    func showEmailEmptyErrorAlert() {
        let alert = commonAlertsFactory.getErrorAlert(with: StringsHelper.paymentEmailEmptyMessage)
        transitionHandler.present(alert, animated: true, completion: nil)
    }
    
    func showFirstNameEmptyErrorAlert() {
        let alert = commonAlertsFactory.getErrorAlert(with: StringsHelper.paymentFirstNameEmptyMessage)
        transitionHandler.present(alert, animated: true, completion: nil)
    }
    
    func showSecondNameEmptyErrorAlert() {
        let alert = commonAlertsFactory.getErrorAlert(with: StringsHelper.paymentSecondNameEmptyMessage)
        transitionHandler.present(alert, animated: true, completion: nil)
    }
    
    func showMiddleNameEmptyErrorAlert() {
        let alert = commonAlertsFactory.getErrorAlert(with: StringsHelper.paymentMiddleNameEmptyMessage)
        transitionHandler.present(alert, animated: true, completion: nil)
    }
    
    func showUnknownErrorAlert() {
        let alert = commonAlertsFactory.getErrorAlert(with: StringsHelper.paymentUnknownErrorMessage)
        transitionHandler.present(alert, animated: true, completion: nil)
    }
    
    func showNoInternetConnectionAlert() {
        let alert = commonAlertsFactory.getErrorAlert(with: StringsHelper.noInternetConnectionErrorText)
        transitionHandler.present(alert, animated: true, completion: nil)
    }
    
    func showErrorAlert(with message: String) {
        let alert = commonAlertsFactory.getErrorAlert(with: message)
        transitionHandler.present(alert, animated: true, completion: nil)
    }
    
    func showAgreementsModule() {
        
        let safariController = SFSafariViewController(url: paymentRulesUrl, entersReaderIfAvailable: true)
        if #available(iOS 10.0, *) {
            safariController.preferredControlTintColor = UIColorPalette.browserControlsDefaultTintColor
        }
        
        transitionHandler.present(safariController, animated: true, completion: nil)
    }
    
    func showPaymentToolsModule(for configuration: PaymentProcessConfiguration) {
        toOtherModulesTransitionHandler.performSegue(with: showPaymentToolsSegueIdentifier, sender: self) { controller in
            controller.configureModule(with: configuration)
        }
    }
    
    func showAddPaymentToolModule(for configuration: PaymentProcessConfiguration) {
        toOtherModulesTransitionHandler.performSegue(with: showAddPaymentToolSegueIdentifier, sender: self) { controller in
            let moduleConfiguration = AddPaymentMethodModuleConfiguration(paymentConfiguration: configuration, isAddingFirstPaymentTool: true)
            controller.configureModule(with: moduleConfiguration)
        }
    }
}

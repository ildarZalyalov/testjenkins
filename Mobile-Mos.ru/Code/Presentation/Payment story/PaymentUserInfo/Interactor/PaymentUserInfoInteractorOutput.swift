//
//  PaymentUserInfoInteractorOutput.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 23.05.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

protocol PaymentUserInfoInteractorOutput: class {
    
    func didFinishObtainingCurrentUserPaymentInfo(email: String?, firstName: String?, lastName: String?, middleName: String?)
    
    func didFinishRegisteringPayment(with result: PaymentRegistrationResult)
    
    func didFinishObtainingPayment(with result: PaymentRequestResult)
    
    func didFinishObtainingPaymentCards(with result: PaymentCardRequestResult)
}

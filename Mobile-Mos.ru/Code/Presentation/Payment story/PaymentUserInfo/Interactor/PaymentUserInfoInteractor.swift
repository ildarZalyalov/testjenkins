//
//  PaymentUserInfoInteractor.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 23.05.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

class PaymentUserInfoInteractor: PaymentUserInfoInteractorInput {
    
    weak var output: PaymentUserInfoInteractorOutput!
    
    var paymentService: PaymentService!
    var userService: UserService!
    
    weak var currentRegisterRequest: HTTPRequest?
    
    //MARK: - PaymentUserInfoInteractorInput
    
    func obtainCurrentUserPaymentInfo() {
        
        paymentService.obtainCurrentUserPaymentInfo { [weak self] result in
            
            guard let strongSelf = self else { return }
            
            var userInfo: PaymentUserInfo? = nil
            
            switch result {
            case .success(let info):
                userInfo = info
            case .failure:
                break
            }
            
            if let info = userInfo {
                strongSelf.output.didFinishObtainingCurrentUserPaymentInfo(email: info.email, firstName: info.firstName, lastName: info.lastName, middleName: info.middleName)
            }
            else {
                let userProfileInfo = strongSelf.userService.currentUser.userInfo
                strongSelf.output.didFinishObtainingCurrentUserPaymentInfo(email: userProfileInfo?.email, firstName: userProfileInfo?.firstName, lastName: userProfileInfo?.lastName, middleName: userProfileInfo?.patronymicName)
            }
        }
    }
    
    func cancelCurrentRegisterPaymentRequest() {
        currentRegisterRequest?.cancel()
    }
    
    func registerPayment(for configuration: PaymentProcessConfiguration,
                         email: String,
                         firstName: String,
                         secondName: String,
                         middleName: String) {
        
        var request = ChatPaymentRequest(params: configuration.control.parameters,
                                         method: configuration.paymentMethod,
                                         email: email,
                                         firstName: firstName,
                                         lastName: secondName,
                                         middleName: middleName)
        
        request.userId = userService.currentUser.userId
        request.callbackData = configuration.callbackData
        
        currentRegisterRequest?.cancel()
        currentRegisterRequest = paymentService.registerPayment(with: request, completion: { [weak self] result in
            self?.output.didFinishRegisteringPayment(with: result)
        })
    }
    
    func obtainPayment(for paymentBatchId: String) {
        paymentService.obtainPayment(for: paymentBatchId) { [weak self] result in
            self?.output.didFinishObtainingPayment(with: result)
        }
    }
    
    func obtainPaymentCards(for paymentBatchId: String) {
        paymentService.obtainPaymentCards(for: paymentBatchId) { [weak self] result in
            self?.output.didFinishObtainingPaymentCards(with: result)
        }
    }
}

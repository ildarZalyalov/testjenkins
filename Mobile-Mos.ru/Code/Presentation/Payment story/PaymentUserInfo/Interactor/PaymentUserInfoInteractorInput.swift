//
//  PaymentUserInfoInteractorInput.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 23.05.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

protocol PaymentUserInfoInteractorInput: class {
    
    func obtainCurrentUserPaymentInfo()
    
    func cancelCurrentRegisterPaymentRequest()
    
    func registerPayment(for configuration: PaymentProcessConfiguration, email: String, firstName: String, secondName: String, middleName: String)
    
    func obtainPayment(for paymentBatchId: String)
    
    func obtainPaymentCards(for paymentBatchId: String)
}

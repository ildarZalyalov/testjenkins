//
//  PaymentUserInfoPresenter.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 23.05.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

class PaymentUserInfoPresenter: PaymentUserInfoViewOutput, PaymentUserInfoInteractorOutput {
    
    weak var view: PaymentUserInfoViewInput!
    var router: PaymentUserInfoRouterInput!
    var interactor: PaymentUserInfoInteractorInput!
    
    var stringValidator: StringValidator!
    
    var moduleConfiguration: PaymentProcessConfiguration!
    var control: ChatMessagePaymentControl {
        return moduleConfiguration.control
    }
    
    var paymentBatchId: String!
    
    //MARK: - Приватные методы
    
    func display(error: Error) {
        if error.isNoInternetConnectionError() {
            router.showNoInternetConnectionAlert()
        }
        else {
            router.showUnknownErrorAlert()
        }
    }
    
    //MARK: - PaymentUserInfoViewOutput
    
    func setupInitialState() {
        interactor.obtainCurrentUserPaymentInfo()
    }
    
    func configure(with configuration: PaymentProcessConfiguration) {
        self.moduleConfiguration = configuration
    }
    
    func didPressCancel() {
        interactor.cancelCurrentRegisterPaymentRequest()
        router.closeModule(completion: nil)
    }
    
    func didPressShowAgreement() {
        router.showAgreementsModule()
    }
    
    func validateField(with value: PaymentUserInfoViewFieldValue, onFormSubmit: Bool) -> Bool {
        
        switch value.type {
            
        case .email:
            
            guard let emailString = value.text, !emailString.isEmpty else {
                
                if onFormSubmit {
                    router.showEmailEmptyErrorAlert()
                }
                
                return false
            }
            
            guard stringValidator.validateEmail(emailString) else {
                
                if onFormSubmit {
                    router.showEmailValidatorErrorAlert()
                }
                
                return false
            }
            
        case .firstName:
            
            guard let firstNameString = value.text, !firstNameString.isEmpty else {
                
                if onFormSubmit {
                    router.showFirstNameEmptyErrorAlert()
                }
                
                return false
            }
            
        case .secondName:
            
            guard let secondNameString = value.text, !secondNameString.isEmpty else {
                
                if onFormSubmit {
                    router.showSecondNameEmptyErrorAlert()
                }
                
                return false
            }
            
        case .middleName:
            
            guard let middleNameString = value.text, !middleNameString.isEmpty else {
                
                if onFormSubmit {
                    router.showMiddleNameEmptyErrorAlert()
                }
                
                return false
            }
        }
        
        return true
    }
    
    func didPressSubmit(email: String?, firstName: String?, secondName: String?, middleName: String?) {
        
        guard email != nil &&
            firstName != nil &&
            secondName != nil &&
            middleName != nil else { return }
        
        moduleConfiguration.clientEmail = email!
        moduleConfiguration.clientName = PaymentClientName(firstName: firstName, lastName: secondName, middleName: middleName)
        
        view.showActivityIndicator()
        
        guard paymentBatchId != nil else {
            interactor.registerPayment(for: moduleConfiguration, email: email!, firstName: firstName!, secondName: secondName!, middleName: middleName!)
            return
        }
        
        guard moduleConfiguration.paymentBatch != nil else {
            interactor.obtainPayment(for: paymentBatchId)
            return
        }
        
        interactor.obtainPaymentCards(for: paymentBatchId)
    }
    
    //MARK: - PaymentUserInfoInteractorOutput
    
    func didFinishObtainingCurrentUserPaymentInfo(email: String?, firstName: String?, lastName: String?, middleName: String?) {
        view.setUserEmail(email)
        view.setUserFirstName(firstName)
        view.setUserLastName(lastName)
        view.setUserMiddleName(middleName)
    }
    
    func didFinishRegisteringPayment(with result: PaymentRegistrationResult) {
        switch result {
        case .success(let batchId):
            paymentBatchId = batchId
            interactor.obtainPayment(for: batchId)
        case .failure(let error):
            view.hideActivityIndicator()
            display(error: error)
        }
    }
    
    func didFinishObtainingPayment(with result: PaymentRequestResult) {
        switch result {
        case .success(let batch):
            moduleConfiguration.paymentBatch = batch
            interactor.obtainPaymentCards(for: paymentBatchId)
        case .failure(let error):
            view.hideActivityIndicator()
            display(error: error)
        }
    }
    
    func didFinishObtainingPaymentCards(with result: PaymentCardRequestResult) {
        
        view.hideActivityIndicator()
        
        switch result {
            
        case .success(let cards):
            
            moduleConfiguration.paymentTools = cards
            
            if cards.isEmpty {
                router.showAddPaymentToolModule(for: moduleConfiguration)
            }
            else {
                router.showPaymentToolsModule(for: moduleConfiguration)
            }
        
        case .failure(let error):
            display(error: error)
        }
    }
}

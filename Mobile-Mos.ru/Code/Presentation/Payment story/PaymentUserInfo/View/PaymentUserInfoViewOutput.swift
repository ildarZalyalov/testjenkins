//
//  PaymentUserInfoViewOutput.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 23.05.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

enum PaymentUserInfoViewFieldType {
    case email
    case firstName
    case secondName
    case middleName
}

typealias PaymentUserInfoViewFieldValue = (type: PaymentUserInfoViewFieldType, text: String?)

protocol PaymentUserInfoViewOutput: class {
    
    func setupInitialState()
    
    func configure(with configuration: PaymentProcessConfiguration)
    
    func didPressCancel()
    
    func didPressShowAgreement()
    
    func validateField(with value: PaymentUserInfoViewFieldValue, onFormSubmit: Bool) -> Bool
    
    func didPressSubmit(email: String?, firstName: String?, secondName: String?, middleName: String?)
}

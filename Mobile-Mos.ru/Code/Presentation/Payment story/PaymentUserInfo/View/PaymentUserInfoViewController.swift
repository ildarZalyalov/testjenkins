//
//  PaymentUserInfoViewController.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 23.05.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import UIKit

class PaymentUserInfoViewController: BaseViewController, PaymentUserInfoViewInput, ConfigurableModuleController, NavigationBarCustomizingController, UITextFieldDelegate {
    
    var output: PaymentUserInfoViewOutput!
    var notificationCenter: NotificationCenter!
    
    @IBOutlet weak var cancelBarButtonItem: UIBarButtonItem!
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var emailStackView: UIStackView!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var emailSeparator: UIView!
    @IBOutlet weak var emailWarningImage: UIImageView!
    
    @IBOutlet weak var firstNameStackView: UIStackView!
    @IBOutlet weak var firstNameLabel: UILabel!
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var firstNameSeparator: UIView!
    @IBOutlet weak var firstNameWarningImage: UIImageView!
    
    @IBOutlet weak var secondNameStackView: UIStackView!
    @IBOutlet weak var secondNameLabel: UILabel!
    @IBOutlet weak var secondNameTextField: UITextField!
    @IBOutlet weak var secondNameSeparator: UIView!
    @IBOutlet weak var secondNameWarningImage: UIImageView!
    
    @IBOutlet weak var middleNameStackView: UIStackView!
    @IBOutlet weak var middleNameLabel: UILabel!
    @IBOutlet weak var middleNameTextField: UITextField!
    @IBOutlet weak var middleNameSeparator: UIView!
    @IBOutlet weak var middleNameWarningImage: UIImageView!
    
    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var bottomLogosViewTopConstraint: NSLayoutConstraint!
    
    let navigationItemsFontSize: CGFloat = 17
    let largeScreenbottomLogosViewTopOffset: CGFloat = 40
    
    let textFieldsLabelAffineTransform = CGAffineTransform(scaleX: 0.8, y: 0.8)
    let textFieldsLabelAnimationDuration = 0.6
    let textFieldlabelTransformAnchorPoint = CGPoint(x: 0.625, y: 0.5)
    let textFieldlabelDefaultAnchorPoint = CGPoint(x: 0.5, y: 0.5)
    
    typealias TextFieldViews = (label: UILabel, field: UITextField, separator: UIView, warning: UIImageView)
    
    lazy var fieldViewsByField: [UITextField : TextFieldViews] = [
        emailTextField : (emailLabel, emailTextField, emailSeparator, emailWarningImage),
        firstNameTextField : (firstNameLabel, firstNameTextField, firstNameSeparator, firstNameWarningImage),
        secondNameTextField : (secondNameLabel, secondNameTextField, secondNameSeparator, secondNameWarningImage),
        middleNameTextField : (middleNameLabel, middleNameTextField, middleNameSeparator, middleNameWarningImage)
    ]
    
    var isKeyboardOpen = false
    var confirmButtonText = ""
    
    //MARK: - Методы
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        let attributes: [NSAttributedStringKey : Any] = [NSAttributedStringKey.font : UIFont(customName: .graphikLCGRegular, size: navigationItemsFontSize) as Any]
        
        cancelBarButtonItem.setTitleTextAttributes(attributes, for: .normal)
        cancelBarButtonItem.setTitleTextAttributes(attributes, for: .highlighted)
        
        setupGestures()
        setupInputAccessoryView()
        setupKeyboardNotifications()
        
        if #available(iOS 11.0, *), UIDevice.current.deviceScreenType == .iPhone6Plus {
            bottomLogosViewTopConstraint.constant = largeScreenbottomLogosViewTopOffset
        }
        
        output.setupInitialState()
    }
    
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override func viewSafeAreaInsetsDidChange() {
        resetScrollViewInsets()
    }
    
    func resetScrollViewInsets() {
        
        guard !isKeyboardOpen else { return }
        
        var insets = UIEdgeInsets.zero
        if #available(iOS 11.0, *) {
            insets.bottom = view.safeAreaInsets.bottom
        }
        
        scrollView.contentInset = insets
        scrollView.scrollIndicatorInsets = insets
    }
    
    //MARK: - ConfigurableModuleController
    
    func configureModule(with object: Any) {
        if let configuration = object as? PaymentProcessConfiguration {
            output.configure(with: configuration)
        }
    }
    
    //MARK: - NavigationBarCustomizingController
    
    var navigationBarState: NavigationBarState? {
        
        var state = NavigationBarState()
        state.isHidden = true
        state.isTranslucent = false
        state.backgroundState = NavigationBarBackgroundState.regular
        state.isShadowHidden = true
        
        let titleTextAttributes = [
            NSAttributedStringKey.font : UIFont(customName: .graphikLCGBold, size: 15) as Any,
            NSAttributedStringKey.foregroundColor : UIColor.black
        ]
        let largeTitleTextAttributes = [
            NSAttributedStringKey.font : UIFont(customName: .graphikLCGBold, size: 25) as Any,
            NSAttributedStringKey.foregroundColor : UIColorPalette.conversationsPageTitleColor
        ]
        
        state.titleTextAttributes = titleTextAttributes
        state.largeTitleTextAttributes = largeTitleTextAttributes
        
        return state
    }
    
    //MARK: - Actions
    
    @IBAction func pressedCancel() {
        output.didPressCancel()
    }
    
    @IBAction func pressedShowAgreement() {
        output.didPressShowAgreement()
    }
    
    @IBAction func pressedConfirm() {
        
        let fields: [UIView] = [emailTextField, firstNameTextField, secondNameTextField, middleNameTextField]
        for field in fields {
            guard validateTextInputs(field, onFormSubmit: true) else { return }
        }
        
        let email = emailTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        let firstName = firstNameTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        let secondName = secondNameTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        let middleName = middleNameTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        
        output.didPressSubmit(email: email, firstName: firstName, secondName: secondName, middleName: middleName)
    }

    @objc
    func nextButtonPressed() {
        
        if emailTextField.isFirstResponder {
            secondNameTextField.becomeFirstResponder()
        }
        else if secondNameTextField.isFirstResponder {
            firstNameTextField.becomeFirstResponder()
        }
        else if firstNameTextField.isFirstResponder {
            middleNameTextField.becomeFirstResponder()
        }
    }
    
    @objc
    func doneButtonPressed() {
        middleNameTextField.resignFirstResponder()
        pressedConfirm()
    }
    
    //MARK: - PaymentUserInfoViewInput
    
    func setUserEmail(_ email: String?) {
        emailTextField.text = email
    }
    
    func setUserFirstName(_ firstName: String?) {
        firstNameTextField.text = firstName
    }
    
    func setUserLastName(_ lastName: String?) {
        secondNameTextField.text = lastName
    }
    
    func setUserMiddleName(_ middleName: String?) {
        middleNameTextField.text = middleName
    }
    
    func showActivityIndicator() {
        
        guard let buttonText = confirmButton.titleLabel?.text else { return }
        confirmButtonText = buttonText
        
        confirmButton.setTitle(String(), for: .normal)
        
        emailTextField.isEnabled = false
        firstNameTextField.isEnabled = false
        secondNameTextField.isEnabled = false
        middleNameTextField.isEnabled = false
        
        confirmButton.isEnabled = false
    
        activityIndicator.startAnimating()
    }
    
    func hideActivityIndicator() {
        
        activityIndicator.stopAnimating()
        confirmButton.setTitle(confirmButtonText, for: .normal)
        
        emailTextField.isEnabled = true
        firstNameTextField.isEnabled = true
        secondNameTextField.isEnabled = true
        middleNameTextField.isEnabled = true
        
        confirmButton.isEnabled = true
    }
    
    //MARK: - UITextFieldDelegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        guard let views = fieldViewsByField[textField] else { return }
        
        views.warning.isHidden = true
        views.separator.backgroundColor = UIColorPalette.selectedTextFieldSeparatorColor
        
        animateTextFieldTitleState(with: views.label, view: views.field)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        guard let views = fieldViewsByField[textField] else { return }
        
        revertTextFieldTitleState(with: views.label, view: views.field)
        views.field.text = views.field.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        
        validateTextInputs(views.field, onFormSubmit: false)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == middleNameTextField {
            doneButtonPressed()
        }
        else {
            nextButtonPressed()
        }
        
        return false
    }
    
    //MARK: - Helper methods
    
    func setupGestures() {
        scrollView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(userDidTapOnView)))
        emailStackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(respondEmailTextField)))
        firstNameStackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(respondFirstNameTextField)))
        secondNameStackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(respondSecondNameTextField)))
        middleNameStackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(respondMiddleNameTextField)))
    }
    
    func setupKeyboardNotifications() {
        
        notificationCenter.addObserver(self,
                                       selector: #selector(self.keyboardWillShow(with:)),
                                       name: NSNotification.Name.UIKeyboardWillShow,
                                       object: nil)
        
        notificationCenter.addObserver(self,
                                       selector: #selector(self.keyboardWillHide(with:)),
                                       name: NSNotification.Name.UIKeyboardWillHide,
                                       object: nil)
    }
    
    func setupInputAccessoryView() {
        
        let nextButtonToolbar = UIToolbar(frame: CGRect.zero)
        nextButtonToolbar.barStyle = .default
        
        let nextButton = UIBarButtonItem(title: StringsHelper.activateNextTextFieldButtonTitle, style: .plain, target: self, action: #selector(nextButtonPressed))
        nextButton.tintColor = UIColor.black
        
        nextButtonToolbar.items = [UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil), nextButton]
        nextButtonToolbar.sizeToFit()
        
        emailTextField.inputAccessoryView = nextButtonToolbar
        firstNameTextField.inputAccessoryView = nextButtonToolbar
        secondNameTextField.inputAccessoryView = nextButtonToolbar
        
        let doneButtonToolbar = UIToolbar(frame: CGRect.zero)
        doneButtonToolbar.barStyle = .default
        
        let doneButton = UIBarButtonItem(title: StringsHelper.deactivateTextFieldButtonTitle, style: .plain, target: self, action: #selector(doneButtonPressed))
        doneButton.tintColor = UIColor.black
        
        doneButtonToolbar.items = [UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil), doneButton]
        doneButtonToolbar.sizeToFit()
        
        middleNameTextField.inputAccessoryView = doneButtonToolbar
    }
    
    func animateTextFieldTitleState(with textLabel: UILabel, view: UIView) {
        
        UIView.animate(withDuration: textFieldsLabelAnimationDuration, animations: { [weak self] in
            
            guard let strongSelf = self else { return }
            
            if #available(iOS 11.0, *) {
                textLabel.layer.anchorPoint = strongSelf.textFieldlabelTransformAnchorPoint
            }
            
            textLabel.transform = strongSelf.textFieldsLabelAffineTransform
            textLabel.x = view.x
        })
    }
    
    func revertTextFieldTitleState(with textLabel: UILabel, view: UIView) {
        
        UIView.animate(withDuration: textFieldsLabelAnimationDuration, animations: { [weak self] in
            
            guard let strongSelf = self else { return }
            
            if #available(iOS 11.0, *) {
                textLabel.layer.anchorPoint = strongSelf.textFieldlabelDefaultAnchorPoint
            }
            
            textLabel.transform = .identity
            textLabel.x = view.x
        })
    }
    
    func updateFieldState(inFieldWith type: PaymentUserInfoViewFieldType, hasError: Bool) {
        
        let isWarningHidden = !hasError
        let separatorColor = hasError ? UIColorPalette.errorInUserDataColor : UIColorPalette.unselectedTextFieldSeparatorColor
        
        switch type {
        case .email:
            emailSeparator.backgroundColor = separatorColor
            emailWarningImage.isHidden = isWarningHidden
        case .firstName:
            firstNameSeparator.backgroundColor = separatorColor
            firstNameWarningImage.isHidden = isWarningHidden
        case .secondName:
            secondNameSeparator.backgroundColor = separatorColor
            secondNameWarningImage.isHidden = isWarningHidden
        case .middleName:
            middleNameSeparator.backgroundColor = separatorColor
            middleNameWarningImage.isHidden = isWarningHidden
        }
    }
    
    @discardableResult
    func validateTextInputs(_ view: UIView, onFormSubmit: Bool) -> Bool {
        
        var fieldType: PaymentUserInfoViewFieldType
        var text: String?
        
        switch view {
        case emailTextField:
            fieldType = .email
            text = emailTextField.text
        case firstNameTextField:
            fieldType = .firstName
            text = firstNameTextField.text
        case secondNameTextField:
            fieldType = .secondName
            text = secondNameTextField.text
        case middleNameTextField:
            fieldType = .middleName
            text = middleNameTextField.text
        default:
            return true
        }
        
        text = text?.trimmingCharacters(in: .whitespacesAndNewlines)
        
        let valid = output.validateField(with: (fieldType, text), onFormSubmit: onFormSubmit)
        updateFieldState(inFieldWith: fieldType, hasError: !valid)
        
        return valid
    }
    
    //MARK: - Observed methods
    
    @objc
    open func userDidTapOnView() {
        self.view.endEditing(true)
    }
    
    @objc
    func respondEmailTextField() {
        emailTextField.becomeFirstResponder()
    }
    
    @objc
    func respondFirstNameTextField() {
        firstNameTextField.becomeFirstResponder()
    }
    
    @objc
    func respondSecondNameTextField() {
        secondNameTextField.becomeFirstResponder()
    }
    
    @objc
    func respondMiddleNameTextField() {
        middleNameTextField.becomeFirstResponder()
    }
    
    @objc
    func keyboardWillShow(with notification: Notification) {
        
        guard let info = notification.userInfo,
            let keyboardEndSize = (info[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size
            else { return }
        
        isKeyboardOpen = true
        
        let insets = UIEdgeInsets(top: 0, left: 0, bottom: keyboardEndSize.height, right: 0)
        scrollView.contentInset = insets
        scrollView.scrollIndicatorInsets = insets
    }
    
    @objc
    func keyboardWillHide(with notification: Notification) {
        isKeyboardOpen = false
        resetScrollViewInsets()
    }
}

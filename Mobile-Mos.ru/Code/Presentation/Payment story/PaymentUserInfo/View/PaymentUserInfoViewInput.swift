//
//  PaymentUserInfoViewInput.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 23.05.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

protocol PaymentUserInfoViewInput: class {
    
    func setUserEmail(_ email: String?)
    
    func setUserFirstName(_ firstName: String?)
    
    func setUserLastName(_ lastName: String?)
    
    func setUserMiddleName(_ middleName: String?)
    
    func showActivityIndicator()
    
    func hideActivityIndicator()
}

//
//  PaymentUserInfoModuleConfigurator.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 23.05.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation
import UIKit

class PaymentUserInfoModuleConfigurator: BaseModuleConfigurator {
    
    override func configureModule(for view: UIViewController) {
        
        let viewController = view as! PaymentUserInfoViewController
        let presenter = PaymentUserInfoPresenter()
        let interactor = PaymentUserInfoInteractor()
        let router = PaymentUserInfoRouter()
        
        let notificationCenter = NotificationCenter.default
        let stringValidator = StringValidator()
        let commonAlertsFactory = CommonAlertsFactoryImplementation()
        let paymentService = UIApplication.shared.serviceBuilder.getPaymentService()
        let userService = UIApplication.shared.serviceBuilder.getUserService()
        
        viewController.output = presenter
        viewController.notificationCenter = notificationCenter
        
        presenter.view = viewController
        presenter.interactor = interactor
        presenter.router = router
        presenter.stringValidator = stringValidator
        
        interactor.output = presenter
        interactor.paymentService = paymentService
        interactor.userService = userService
        
        router.transitionHandler = viewController
        router.toOtherModulesTransitionHandler = viewController
        router.commonAlertsFactory = commonAlertsFactory
    }
}

//
//  AddPaymentMethodModuleConfigurator.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 24.01.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation
import UIKit

class AddPaymentMethodModuleConfigurator: BaseModuleConfigurator {
    
    override func configureModule(for view: UIViewController) {
        
        let viewController = view as! AddPaymentMethodViewController
        let presenter = AddPaymentMethodPresenter()
        let interactor = AddPaymentMethodInteractor()
        let router = AddPaymentMethodRouter()
        
        let notificationCenter = NotificationCenter.default
        let paymentService = UIApplication.shared.serviceBuilder.getPaymentService()
        let userService = UIApplication.shared.serviceBuilder.getUserService()
        let cardNumberTextFormatter = BankCardFormatManagerImplementation()
        let cardDateTextFormatter = TextFieldFormatManagerImplementation()
        let cardCodeTextFormatter = TextFieldFormatManagerImplementation()
        let validator = BankCardValidatorImplementation()
        let paymentManager = SDKDIT()
        
        let storyboardFactory = StoryboardFactoryImplementation()
        let applicationRouter = ApplicationRouterBuilder.getApplicationRouter()
        let commonAlertFactory = CommonAlertsFactoryImplementation()
        
        let priceFormatter = NumberFormatter()
        priceFormatter.usesGroupingSeparator = true
        priceFormatter.numberStyle = .currency
        priceFormatter.minimumFractionDigits = 0
        priceFormatter.maximumFractionDigits = 2
        priceFormatter.locale = Locale(identifier: "ru_RU")
        
        viewController.output = presenter
        viewController.notificationCenter = notificationCenter
        viewController.cardNumberTextFormatter = cardNumberTextFormatter
        viewController.cardDateTextFormatter = cardDateTextFormatter
        viewController.cardCodeTextFormatter = cardCodeTextFormatter
        
        presenter.view = viewController
        presenter.interactor = interactor
        presenter.router = router
        presenter.validator = validator
        presenter.priceFormatter = priceFormatter
        
        interactor.output = presenter
        interactor.paymentService = paymentService
        interactor.userService = userService
        
        router.transitionHandler = viewController
        router.alertTransitionHandler = viewController
        router.toOtherModulesTransitionHandler = viewController
        router.commonAlertsFactory = commonAlertFactory
        router.paymentManager = paymentManager
        router.storyboardFactory = storyboardFactory
        router.applicationRouter = applicationRouter
    }
}

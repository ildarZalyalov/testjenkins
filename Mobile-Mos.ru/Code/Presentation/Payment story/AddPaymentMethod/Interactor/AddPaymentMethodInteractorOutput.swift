//
//  AddPaymentMethodInteractorOutput.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 24.01.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

protocol AddPaymentMethodInteractorOutput: class {
    
    func didFinishObtainingCardInfo(with result: PaymentCardInfoRequestResult)
    
    func didFinishBeginningPayment(with result: PaymentRequestResult)
    
    func didObtainRedirectRequest(with redirectInfo: PaymentRedirectInfo)
    
    func didFinishObtainingPaymentStatus(with result: PaymentRequestResult)
}

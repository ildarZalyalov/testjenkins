//
//  AddPaymentMethodInteractor.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 24.01.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

class AddPaymentMethodInteractor: AddPaymentMethodInteractorInput {
    
    weak var output: AddPaymentMethodInteractorOutput!
    
    var paymentService: PaymentService!
    var userService: UserService!
    
    //MARK: - AddPaymentMethodInteractorInput
    
    func buildCallbackData(for paymentConfiguration: PaymentProcessConfiguration) -> String? {
        
        guard let data = paymentConfiguration.callbackData else { return nil }
        let paymentControl = paymentConfiguration.control
        
        var controlValueJson: [String : String] = [
            "email" : paymentConfiguration.clientEmail,
            "payment_sum" : String(paymentControl.sum),
            "payment_params" : paymentControl.parameters,
            "payment_method" : paymentConfiguration.paymentMethod.rawValue,
            ]
        
        if let userId = userService.currentUser.userId {
            controlValueJson["user_id"] = userId
        }
        
        let jsonDictionary: [String : Any] = [
            "callback" : data,
            "control_value" : controlValueJson
        ]
        
        guard let jsonData = try? JSONSerialization.data(withJSONObject: jsonDictionary, options: []) else { return nil }
        
        return String(data: jsonData, encoding: .utf8)
    }
    
    func savePaymentInfo(_ userPaymentInfo: PaymentUserInfo) {
        paymentService.saveUserPaymentInfo(userPaymentInfo)
    }
    
    func obtainCardInfo(for bin: String) {
        paymentService.obtainPaymentCardInfo(for: bin) { [weak self] result in
            self?.output.didFinishObtainingCardInfo(with: result)
        }
    }
    
    func beginPayment(for batchId: String, with request: PaymentRequest) {
        paymentService.beginPayment(for: batchId, with: request) { [weak self] result in
            self?.output.didFinishBeginningPayment(with: result)
        }
    }
    
    func obtainPaymentStatus(for batchId: String) {
        paymentService.obtainPaymentStatus(for: batchId, redirect: { [weak self] info in
            self?.output.didObtainRedirectRequest(with: info)
        }) { [weak self] result in
            self?.output.didFinishObtainingPaymentStatus(with: result)
        }
    }
}

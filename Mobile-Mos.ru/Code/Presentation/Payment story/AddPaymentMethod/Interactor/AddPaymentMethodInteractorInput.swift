//
//  AddPaymentMethodInteractorInput.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 24.01.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

protocol AddPaymentMethodInteractorInput: class {
    
    func buildCallbackData(for paymentConfiguration: PaymentProcessConfiguration) -> String?
    
    func savePaymentInfo(_ userPaymentInfo: PaymentUserInfo)
    
    func obtainCardInfo(for bin: String)
    
    func beginPayment(for batchId: String, with request: PaymentRequest)
    
    func obtainPaymentStatus(for batchId: String)
}

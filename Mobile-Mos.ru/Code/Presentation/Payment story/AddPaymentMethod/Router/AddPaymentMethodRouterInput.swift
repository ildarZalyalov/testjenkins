//
//  AddPaymentMethodRouterInput.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 24.01.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

protocol AddPaymentMethodRouterInput: class {

    func closeModule(goingBackToChat: Bool, completion: (() -> ())?)
    
    func showScanCardModule(completion: @escaping (String?, UInt, UInt) -> ())
    
    func showCardNumberValidatorErrorAlert()
    
    func showCardExpiryValidatorErrorAlert()
    
    func showCardCvvValidatorErrorAlert()
    
    func showUnknownErrorAlert()
    
    func showNoInternetConnectionAlert()
    
    func showErrorAlert(with message: String)
    
    func showPayment3DSProcessingModule(with redirectInfo: PaymentRedirectInfo, eventHandler: Payment3DSModuleEventHandler?)
    
    func showPaymentProcessingModule()
    
    func hidePaymentProcessingModule(completion: (() -> ())?)
}

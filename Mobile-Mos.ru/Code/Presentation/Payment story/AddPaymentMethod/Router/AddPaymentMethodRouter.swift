//
//  AddPaymentMethodRouter.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 24.01.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

class AddPaymentMethodRouter: AddPaymentMethodRouterInput {
    
    weak var transitionHandler: BaseViewController!
    weak var alertTransitionHandler: UIViewController!
    weak var toOtherModulesTransitionHandler: DataTransferModuleController!
    
    var commonAlertsFactory: CommonAlertsFactory!
    
    var paymentManager: SDKDIT!
    var storyboardFactory: StoryboardFactory!
    var applicationRouter: ApplicationRouter!
    
    let showPayment3DSProcessingSegueIdentifier = "show3DSProcessing"
    let unwindToPaymentMethodSelectionSegueIdentifier = "unwindToPaymentMethodSelection"
    let unwindToChatSegueIdentifier = "unwindToChat"
    
    //MARK: - AddPaymentMethodRouterInput
    
    func closeModule(goingBackToChat: Bool, completion: (() -> ())?) {
        
        if goingBackToChat {
            transitionHandler.performSegue(withIdentifier: unwindToChatSegueIdentifier, sender: self)
        }
        else {
            transitionHandler.performSegue(withIdentifier: unwindToPaymentMethodSelectionSegueIdentifier, sender: self)
        }
        
        let destination = transitionHandler.currentSegueDestination
        destination?.transitionCoordinator?.animate(alongsideTransition: nil, completion: { _ in
            completion?()
        })
    }
    
    func showScanCardModule(completion: @escaping (String?, UInt, UInt) -> ()) {
        paymentManager.scanCard(with: transitionHandler) { cardNumber, expiryYear, expiryMonth  in
            completion(cardNumber, expiryYear, expiryMonth)
        }
    }
    
    func showCardNumberValidatorErrorAlert() {
        let alert = commonAlertsFactory.getErrorAlert(with: StringsHelper.paymentCardNumberValidateMessage)
        alertTransitionHandler.present(alert, animated: true, completion: nil)
    }
    
    func showCardExpiryValidatorErrorAlert() {
        let alert = commonAlertsFactory.getErrorAlert(with: StringsHelper.paymentCardExpiryValidateMessage)
        alertTransitionHandler.present(alert, animated: true, completion: nil)
    }
    
    func showCardCvvValidatorErrorAlert() {
        let alert = commonAlertsFactory.getErrorAlert(with: StringsHelper.paymentCardCvvValidateMessage)
        alertTransitionHandler.present(alert, animated: true, completion: nil)
    }
    
    func showUnknownErrorAlert() {
        let alert = commonAlertsFactory.getErrorAlert(with: StringsHelper.paymentUnknownErrorMessage)
        transitionHandler.present(alert, animated: true, completion: nil)
    }
    
    func showNoInternetConnectionAlert() {
        let alert = commonAlertsFactory.getErrorAlert(with: StringsHelper.noInternetConnectionErrorText)
        transitionHandler.present(alert, animated: true, completion: nil)
    }
    
    func showErrorAlert(with message: String) {
        let alert = commonAlertsFactory.getErrorAlert(with: message)
        transitionHandler.present(alert, animated: true, completion: nil)
    }
    
    func showPayment3DSProcessingModule(with redirectInfo: PaymentRedirectInfo, eventHandler: Payment3DSModuleEventHandler?) {
        
        toOtherModulesTransitionHandler.performSegue(with: showPayment3DSProcessingSegueIdentifier, sender: self) { controller in
            
            var configuration = Payment3DSModuleConfiguration(info: redirectInfo)
            configuration.eventHandler = eventHandler
            
            controller.configureModule(with: configuration)
        }
    }
    
    func showPaymentProcessingModule() {
        
        let processingController = storyboardFactory.getStoryboard(with: .payment).instantiateViewController(for: .paymentProcessing)
        processingController.modalPresentationStyle = .overFullScreen
        processingController.modalTransitionStyle = .crossDissolve
        
        applicationRouter.blockTransitions(whileOnScreen: processingController)
        transitionHandler.present(processingController, animated: true, completion: nil)
    }
    
    func hidePaymentProcessingModule(completion: (() -> ())?) {
        transitionHandler.dismiss(animated: true, completion: completion)
    }
}

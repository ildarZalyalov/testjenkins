//
//  AddPaymentMethodViewController.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 24.01.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import UIKit
import WebKit

class AddPaymentMethodViewController: BaseViewController, AddPaymentMethodViewInput, ConfigurableModuleController, NavigationBarCustomizingController, UITextFieldDelegate {
    
    var output: AddPaymentMethodViewOutput!
    
    var notificationCenter: NotificationCenter!
    var cardNumberTextFormatter: BankCardFormatManager!
    var cardDateTextFormatter: TextFieldFormatManager!
    var cardCodeTextFormatter: TextFieldFormatManager!
    
    @IBOutlet var textFields: [UITextField]!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var cardNumberLabel: UILabel!
    @IBOutlet weak var cardNumberTextField: UITextField!
    @IBOutlet weak var cardNumberSeparator: UIView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var dateTextField: UITextField!
    @IBOutlet weak var dateSeparator: UIView!
    @IBOutlet weak var cvvLabel: UILabel!
    @IBOutlet weak var cvvTextField: UITextField!
    @IBOutlet weak var cvvSeparator: UIView!
    
    @IBOutlet weak var bankLogoView: UIImageView!
    @IBOutlet weak var bankNameLabel: UILabel!
    @IBOutlet weak var bankColorView: UIView!
    @IBOutlet weak var paymentSystemView: UIImageView!
    
    @IBOutlet weak var comissionLabel: UILabel!
    @IBOutlet weak var scanCardButton: UIButton!
    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet var comissionLabelTopConstraint: NSLayoutConstraint!
    
    @IBOutlet var frontCardCenterXConstraint: NSLayoutConstraint!
    @IBOutlet var backCardCenterXConstraint: NSLayoutConstraint!
    
    let navigationItemsFontSize: CGFloat = 17
    let textFieldsLabelAffineTransform = CGAffineTransform(scaleX: 0.8, y: 0.8).translatedBy(x: -18, y: 0)
    let textFieldsLabelAnimationDuration: TimeInterval = 0.3
    let panChangeNotificationTimeout: TimeInterval = 0.5
    let compactScreenComissionLabelTopOffset: CGFloat = 15
    
    let unknownBankLogo: UIImage = #imageLiteral(resourceName: "paymentUnknownBankLogo")
    
    /// Префикс названия банка, который нужно удалять
    let cardInfoBankPrefixToRemove = "Bank "
    
    var isKeyboardOpen = false
    var doneButtonText = ""
    
    lazy var notifyAboutPanChangePerformer: DelayedSelectorPerformer = {
        return DelayedSelectorPerformer(target: self, selector: #selector(notifyAboutPanChange))
    }()
    
    //MARK: - Методы
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        let regularAttributes: [NSAttributedStringKey : Any] = [NSAttributedStringKey.font : UIFont(customName: .graphikLCGRegular, size: navigationItemsFontSize) as Any]
        
        let updateStyle: (UIBarButtonItem, [NSAttributedStringKey : Any]) -> () = { item, attributes in
            item.setTitleTextAttributes(attributes, for: .normal)
            item.setTitleTextAttributes(attributes, for: .highlighted)
            item.setTitleTextAttributes(attributes, for: .disabled)
        }
        
        if let items = navigationItem.leftBarButtonItems {
            items.forEach({ updateStyle($0, regularAttributes) })
        }
        
        cardNumberTextField.addTarget(self, action: #selector(textFieldDidChange(sender:)), for: .editingChanged)
        dateTextField.addTarget(self, action: #selector(textFieldDidChange(sender:)), for: .editingChanged)
        cvvTextField.addTarget(self, action: #selector(textFieldDidChange(sender:)), for: .editingChanged)
        
        setupInputAccessoryView()
        setupKeyboardNotifications()
        
        cardNumberTextFormatter.configure(with: cardNumberTextField)
        cardNumberTextField.delegate = cardNumberTextFormatter
        cardNumberTextFormatter.proxyDelegate = self
        
        cardDateTextFormatter.configure(with: dateTextField, mask: .bankCardDate)
        dateTextField.delegate = cardDateTextFormatter
        cardDateTextFormatter.proxyDelegate = self
        
        cardCodeTextFormatter.configure(with: cvvTextField, mask: .bankCvv)
        cvvTextField.delegate = cardCodeTextFormatter
        cardCodeTextFormatter.proxyDelegate = self
        
        if UIDevice.current.deviceScreenType != .iPhone6Plus {
            frontCardCenterXConstraint.isActive = false
            backCardCenterXConstraint.isActive = false
        }
        
        if UIDevice.current.hasConfinedScreen {
            comissionLabelTopConstraint.constant = compactScreenComissionLabelTopOffset
        }
        
        output.setupInitialState()
    }
    
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override func viewSafeAreaInsetsDidChange() {
        resetScrollViewInsets()
    }
    
    func resetScrollViewInsets() {
        
        guard !isKeyboardOpen else { return }
        
        var insets = UIEdgeInsets.zero
        if #available(iOS 11.0, *) {
            insets.bottom = view.safeAreaInsets.bottom
        }
        
        scrollView.contentInset = insets
        scrollView.scrollIndicatorInsets = insets
    }
    
    //MARK: - Actions
    
    @IBAction func pressedCancel() {
        textFields.forEach { $0.resignFirstResponder() }
        output.didPressCancel()
    }
    
    @IBAction func pressedScanCard() {
        textFields.forEach { $0.resignFirstResponder() }
        output.didPressScanCard()
    }
    
    @IBAction func pressedDone() {
        
        textFields.forEach { $0.resignFirstResponder() }
        
        let nonDigits = CharacterSet.decimalDigits.inverted
        let expiry = dateTextField.text?.components(separatedBy: nonDigits).joined()
        
        output.didPressDone(cardNumber: cardNumberTextFormatter.cardNumber, cardExpiry: expiry, cardCvv: cvvTextField.text)
    }
    
    @IBAction func showCardNumberTextField() {
        cardNumberTextField.becomeFirstResponder()
    }
    
    @IBAction func showDateTextField() {
        dateTextField.becomeFirstResponder()
    }
    
    @IBAction func showCvvTextField() {
        cvvTextField.becomeFirstResponder()
    }
    
    @objc
    func nextButtonPressed() {
        if cardNumberTextField.isFirstResponder {
            dateTextField.becomeFirstResponder()
        }
        else if dateTextField.isFirstResponder {
            cvvTextField.becomeFirstResponder()
        }
    }
    
    @objc
    func hideKeyboardButtonPressed() {
        cvvTextField.resignFirstResponder()
    }
    
    @objc
    func notifyAboutPanChange() {
        output.didEnterPanString(panString: cardNumberTextFormatter.cardNumber)
    }
    
    @IBAction func unwindToAddPaymentMethod(_: UIStoryboardSegue) {}
    
    //MARK: - ConfigurableModuleController
    
    func configureModule(with object: Any) {
        if let configuration = object as? AddPaymentMethodModuleConfiguration {
            output.configure(with: configuration)
        }
    }
    
    //MARK: - NavigationBarCustomizingController
    
    var navigationBarState: NavigationBarState? {
        
        var state = NavigationBarState()
        
        state.isHidden = false
        state.isTranslucent = false
        state.backgroundState = NavigationBarBackgroundState.regular
        state.tintColor = UIColor.black
        state.isShadowHidden = true
        
        let titleTextAttributes = [
            NSAttributedStringKey.font : UIFont(customName: .graphikLCGBold, size: 15) as Any,
            NSAttributedStringKey.foregroundColor : UIColor.black
        ]
        let largeTitleTextAttributes = [
            NSAttributedStringKey.font : UIFont(customName: .graphikLCGBold, size: 25) as Any,
            NSAttributedStringKey.foregroundColor : UIColor.black
        ]
        
        state.titleTextAttributes = titleTextAttributes
        state.largeTitleTextAttributes = largeTitleTextAttributes
        
        return state
    }
    
    //MARK: - Приватные методы
    
    func label(for textField: UITextField) -> UILabel? {
        switch textField {
        case cardNumberTextField:
            return cardNumberLabel
        case dateTextField:
            return dateLabel
        case cvvTextField:
            return cvvLabel
        default:
            return nil
        }
    }
    
    func animateState(for textField: UITextField, completion: (() -> ())? = nil) {
        
        guard let label = label(for: textField) else { return }
        let transform = textFieldsLabelAffineTransform
        
        UIView.animate(withDuration: textFieldsLabelAnimationDuration, animations: {
            textField.isHidden = false
            label.transform = transform
        }) { _ in
            completion?()
        }
    }
    
    func revertTextFieldTitleState(for textField: UITextField) {
        
        guard let label = label(for: textField) else { return }
        
        UIView.animate(withDuration: textFieldsLabelAnimationDuration, animations: {
            textField.isHidden = true
            label.transform = .identity
        })
    }
    
    func setupKeyboardNotifications() {
        
        notificationCenter.addObserver(self,
                                       selector: #selector(self.keyboardWillShow(with:)),
                                       name: NSNotification.Name.UIKeyboardWillShow,
                                       object: nil)
        
        notificationCenter.addObserver(self,
                                       selector: #selector(self.keyboardWillHide(with:)),
                                       name: NSNotification.Name.UIKeyboardWillHide,
                                       object: nil)
    }
    
    func setupInputAccessoryView() {
        
        let nextButtonToolbar = UIToolbar(frame: CGRect.zero)
        nextButtonToolbar.barStyle = .default
        
        let nextButton = UIBarButtonItem(title: StringsHelper.activateNextTextFieldButtonTitle, style: .plain, target: self, action: #selector(nextButtonPressed))
        nextButton.tintColor = UIColor.black
        
        nextButtonToolbar.items = [UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil), nextButton]
        nextButtonToolbar.sizeToFit()
        
        cardNumberTextField.inputAccessoryView = nextButtonToolbar
        dateTextField.inputAccessoryView = nextButtonToolbar
        
        let doneButtonToolbar = UIToolbar(frame: CGRect.zero)
        doneButtonToolbar.barStyle = .default
        
        let doneButton = UIBarButtonItem(title: StringsHelper.deactivateTextFieldButtonTitle, style: .plain, target: self, action: #selector(hideKeyboardButtonPressed))
        doneButton.tintColor = UIColor.black
        
        doneButtonToolbar.items = [UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil), doneButton]
        doneButtonToolbar.sizeToFit()
        
        cvvTextField.inputAccessoryView = doneButtonToolbar
    }
    
    //MARK: - AddPaymentMethodViewInput
    
    func assignAcceptableCardNumberLengths(range: CountableClosedRange<Int>) {
        cardNumberTextFormatter.acceptableCardNumberLengths = range
    }
    
    func setViewTitle(_ title: String) {
        navigationItem.title = title
    }
    
    func setCancelButtonTitle(_ title: String) {
        let cancelBarButtonItem = UIBarButtonItem(title: title, style: .plain, target: self, action: #selector(pressedCancel))
        cancelBarButtonItem.tintColor = UIColorPalette.paymentBarButtonColor
        navigationItem.leftBarButtonItems = [cancelBarButtonItem]
    }
    
    func hideCancelButton() {
        navigationItem.leftBarButtonItems = []
        navigationItem.leftBarButtonItem = nil
    }
    
    func hideScanCardButton() {
        
        scanCardButton.isHidden = true
        
        var shouldUseCompactConstraints = false
        
        if #available(iOS 11.0, *) { shouldUseCompactConstraints = UIDevice.current.hasConfinedScreen }
        else { shouldUseCompactConstraints = UIDevice.current.hasCompactScreen }
        
        if shouldUseCompactConstraints {
            comissionLabelTopConstraint.isActive = false
        }
    }
    
    func displayBankName(_ bankName: String?) {
        bankNameLabel.isHidden = bankName?.isEmpty ?? true
        bankNameLabel.text = bankName
    }
    
    func displayBankLogo(with bankLogoName: String?) {
        
        guard let logoName = bankLogoName else {
            bankLogoView.image = unknownBankLogo
            return
        }

        bankLogoView.image = UIImage(named: logoName)
        guard bankLogoView.image == nil else { return }
        bankLogoView.image = unknownBankLogo
    }
    
    func displayBankLogo(with bankLogoUrl: URL?) {
        bankLogoView.isHidden = bankLogoUrl == nil
        bankLogoView.loadAndDisplayImage(from: bankLogoUrl)
    }
    
    func displayBankColor(bankColor: UIColor?) {
        bankColorView.isHidden = bankColor == nil
        bankColorView.backgroundColor = bankColor
    }
    
    func displayPaymentSystem(with systemIconName: String?) {
        
        guard let iconName = systemIconName else {
            paymentSystemView.isHidden = true
            return
        }
        
        paymentSystemView.image = UIImage(named: iconName)
        paymentSystemView.isHidden = paymentSystemView.image == nil
    }
    
    func displayComission(_ comission: String?) {
        comissionLabel.isHidden = comission?.isEmpty ?? true
        comissionLabel.text = comission
    }
    
    func hideAllBankInfo() {
        bankNameLabel.isHidden = true
        bankLogoView.image = unknownBankLogo
        bankColorView.isHidden = true
        paymentSystemView.isHidden = true
        comissionLabel.isHidden = true
    }
    
    func setCardNumber(_ cardNumber: String?) {
        
        guard let number = cardNumber else { return }
        
        animateState(for: cardNumberTextField) { [weak self] in
            
            guard let strongSelf = self else { return }
            
            let numberText = strongSelf.cardNumberTextFormatter.format(cardNumber: number).formattedNumber
            strongSelf.cardNumberTextField.text = numberText
            strongSelf.textFieldDidChange(sender: strongSelf.cardNumberTextField)
        }
    }
    
    func setCardTitle(_ cardTitle: String?) {
        
        guard let title = cardTitle else { return }
        
        animateState(for: cardNumberTextField) { [weak self] in
            self?.cardNumberTextField.text = title
        }
    }
    
    func setCardExpiry(_ cardExpiry: String?) {
        
        guard let expiry = cardExpiry else { return }
        
        animateState(for: dateTextField) { [weak self] in
            
            guard let strongSelf = self else { return }
            
            strongSelf.dateTextField.text = expiry
            strongSelf.dateTextField.sendActions(for: .editingChanged)
        }
    }
    
    func setCardNumberFieldEnabled(_ enabled: Bool) {
        cardNumberTextField.isEnabled = enabled
    }
    
    func setCardExpiryFieldEnabled(_ enabled: Bool) {
        dateTextField.isEnabled = enabled
    }
    
    func setCardNumberHighlighted(_ highlighted: Bool) {
        let color = highlighted ? UIColorPalette.paymentFieldHighlightedColor : UIColorPalette.paymentFieldRegularColor
        cardNumberLabel.textColor = color
        cardNumberSeparator.backgroundColor = color
    }
    
    func setCardExpiryHighlighted(_ highlighted: Bool) {
        let color = highlighted ? UIColorPalette.paymentFieldHighlightedColor : UIColorPalette.paymentFieldRegularColor
        dateLabel.textColor = color
        dateSeparator.backgroundColor = color
    }
    
    func setCardCodeHighlighted(_ highlighted: Bool) {
        let color = highlighted ? UIColorPalette.paymentFieldHighlightedColor : UIColorPalette.paymentFieldRegularColor
        cvvLabel.textColor = color
        cvvSeparator.backgroundColor = color
    }
    
    func showActivityIndicator() {
        
        guard let buttonText = doneButton.titleLabel?.text else { return }
        doneButtonText = buttonText
        
        doneButton.setTitle(String(), for: .normal)
        
        cardNumberTextField.isEnabled = false
        dateTextField.isEnabled = false
        cvvTextField.isEnabled = false
        
        scanCardButton.isEnabled = false
        doneButton.isEnabled = false
        
        activityIndicator.startAnimating()
    }
    
    func hideActivityIndicator() {
        
        activityIndicator.stopAnimating()
        doneButton.setTitle(doneButtonText, for: .normal)
        
        cardNumberTextField.isEnabled = true
        dateTextField.isEnabled = true
        cvvTextField.isEnabled = true
        
        scanCardButton.isEnabled = true
        doneButton.isEnabled = true
    }
    
    //MARK: - UITextFieldDelegate
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        switch textField {
        case cardNumberTextField:
            setCardNumberHighlighted(false)
        case dateTextField:
            setCardExpiryHighlighted(false)
        case cvvTextField:
            setCardCodeHighlighted(false)
        default:
            break
        }
        
        let shouldAnimate = textField.text?.isEmpty ?? true
        guard shouldAnimate else { return true }
        
        animateState(for: textField)
        
        return true
    }
    
    @objc
    func textFieldDidChange(sender: UITextField) {
        
        let cardNumberIsEmpty = cardNumberTextField.text?.isEmpty ?? true
        let cardExpiryIsEmpty = dateTextField.text?.isEmpty ?? true
        let cardCvvIsEmpty = cvvTextField.text?.isEmpty ?? true
        
        doneButton.isEnabled = !cardNumberIsEmpty && !cardExpiryIsEmpty && !cardCvvIsEmpty
        
        guard sender == cardNumberTextField else { return }
        
        notifyAboutPanChangePerformer.cancelDelayedPerform()
        notifyAboutPanChangePerformer.perform(afterDelay: panChangeNotificationTimeout)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        let shouldAnimate = textField.text?.isEmpty ?? true
        guard shouldAnimate else { return }
        
        revertTextFieldTitleState(for: textField)
    }
    
    @available(iOS 10.0, *)
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextFieldDidEndEditingReason) {
        textFieldDidEndEditing(textField)
    }
    
    //MARK: - Нотификации
    
    @objc
    func keyboardWillShow(with notification: Notification) {
        
        guard let info = notification.userInfo,
            let keyboardEndSize = (info[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size
            else { return }
        
        isKeyboardOpen = true
        
        let insets = UIEdgeInsets(top: 0, left: 0, bottom: keyboardEndSize.height, right: 0)
        scrollView.contentInset = insets
        scrollView.scrollIndicatorInsets = insets
    }
    
    @objc
    func keyboardWillHide(with notification: Notification) {
        isKeyboardOpen = false
        resetScrollViewInsets()
    }
}

//
//  AddPaymentMethodViewOutput.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 24.01.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

protocol AddPaymentMethodViewOutput: class {
    
    func setupInitialState()
    
    func configure(with configuration: AddPaymentMethodModuleConfiguration)
    
    func didPressCancel()
    
    func didPressScanCard()
    
    func didPressDone(cardNumber: String?, cardExpiry: String?, cardCvv: String?)
    
    func didEnterPanString(panString: String?)
}

//
//  AddPaymentMethodViewInput.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 24.01.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

protocol AddPaymentMethodViewInput: class {
    
    func assignAcceptableCardNumberLengths(range: CountableClosedRange<Int>)
    
    func setViewTitle(_ title: String)
    
    func setCancelButtonTitle(_ title: String)
    
    func hideCancelButton()
    
    func hideScanCardButton()
    
    func displayBankName(_ bankName: String?)
    
    func displayBankLogo(with bankLogoName: String?)
    
    func displayBankLogo(with bankLogoUrl: URL?)
    
    func displayBankColor(bankColor: UIColor?)
    
    func displayPaymentSystem(with systemIconName: String?)
    
    func displayComission(_ comission: String?)
    
    func hideAllBankInfo()
    
    func setCardNumber(_ cardNumber: String?)
    
    func setCardTitle(_ cardTitle: String?)
    
    func setCardExpiry(_ cardExpiry: String?)
    
    func setCardNumberFieldEnabled(_ enabled: Bool)
    
    func setCardExpiryFieldEnabled(_ enabled: Bool)
    
    func setCardNumberHighlighted(_ highlighted: Bool)
    
    func setCardExpiryHighlighted(_ highlighted: Bool)
    
    func setCardCodeHighlighted(_ highlighted: Bool)
    
    func showActivityIndicator()
    
    func hideActivityIndicator()
}

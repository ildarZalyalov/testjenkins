//
//  AddPaymentMethodPresenter.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 24.01.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

class AddPaymentMethodPresenter: AddPaymentMethodViewOutput, AddPaymentMethodInteractorOutput, Payment3DSModuleEventHandler {
    
    weak var view: AddPaymentMethodViewInput!
    var router: AddPaymentMethodRouterInput!
    var interactor: AddPaymentMethodInteractorInput!
    
    var validator: BankCardValidator!
    var moduleConfiguration: AddPaymentMethodModuleConfiguration!
    var priceFormatter: NumberFormatter!
    
    var paymentConfiguration: PaymentProcessConfiguration {
        return moduleConfiguration.paymentConfiguration
    }
    
    var selectedPaymentTool: PaymentCard? {
        return moduleConfiguration.selectedPaymentTool
    }
    
    let minimumPanLengthToDetermineBank = 6
    let maximumPanLengthToDetermineBank = 8
    let panSuffixLengthForProviderTitle = 4
    let expiryValidLength = 4
    let expiryFirstPartLength = 2
    let comissionToRublesConvertMultiplier: Double = 100
    
    let panProviderTitlePrefix = "*"
    
    var lastSearchPan: String?
    var bankCardInfo: PaymentCardExtendedInfo?
    var paymentProvider: PaymentProvider?
    var comissionValue: Int64 = 0
    
    var bankCardNumber: String?
    var bankCardExpiry: String?
    var bankCardCvv: String?
    
    var paymentRequest: PaymentRequest?
    
    var processingBeginTime = Date()
    let processingMinimumDuration: TimeInterval = 1
    
    let retryRefreshPaymentStatusDelay: TimeInterval = 3
    lazy var retryRefreshPaymentStatusPerformer = DelayedWorkPerformer { [weak self] in
        self?.refreshPaymentStatus()
    }
    
    //MARK: - Приватные методы
    
    func display(error: Error) {
        if error.isNoInternetConnectionError() {
            router.showNoInternetConnectionAlert()
        }
        else {
            router.showUnknownErrorAlert()
        }
    }
    
    func refreshBankInfo() {
        view.displayBankName(bankCardInfo?.bankTitle)
        view.displayBankColor(bankColor: paymentProvider?.bankColor)
        view.displayPaymentSystem(with: paymentProvider?.typeIconName)
        view.displayBankLogo(with: paymentProvider?.bankIconName)
    }
    
    func refreshComission() {
        
        var comissionString: String?
        let comissionInRubles = Double(comissionValue) / comissionToRublesConvertMultiplier
        
        if comissionInRubles > 0 {
            comissionString = priceFormatter.string(from: NSNumber(value: comissionInRubles))
            if comissionString != nil {
                comissionString = StringsHelper.paymentComissionPrefix + comissionString!
            }
        }
        else {
            comissionString = StringsHelper.paymentNoComissionString
        }
        
        view.displayComission(comissionString)
    }
    
    func invertExpiryString(_ expiryString: String?) -> String? {
        
        guard expiryString != nil else { return nil }
        guard expiryValidLength > expiryFirstPartLength  else { return nil }
        guard expiryString!.count == expiryValidLength else { return nil }
        
        let secondPartLength = expiryValidLength - expiryFirstPartLength
        
        let firstPart = String(expiryString!.prefix(expiryFirstPartLength))
        let secondPart = String(expiryString!.suffix(secondPartLength))
        
        return secondPart + firstPart
    }
    
    func expiryString(from year: UInt, and month: UInt) -> String? {
        
        guard expiryValidLength > expiryFirstPartLength  else { return nil }
        
        let secondPartLength = expiryValidLength - expiryFirstPartLength
        let yearString = String(year).suffix(secondPartLength)
        let monthString = String(format: "%02d", month)
        let expiryString = monthString + yearString
        
        guard expiryString.count == expiryValidLength else { return nil }
        
        return expiryString
    }
    
    func paymentProviderTitle(from pan: String?) -> String {
        
        var panNumber = pan ?? String()
        if !panNumber.isEmpty {
            panNumber = panProviderTitlePrefix + String(panNumber.suffix(panSuffixLengthForProviderTitle))
        }
        
        return panNumber
    }
    
    func paymentProvider(from cardInfo: PaymentCardExtendedInfo, for pan: String?) -> PaymentProvider {
        
        var brand = PaymentProvider.PaymentSystem.otherBankCard
        var bank = PaymentProvider.Bank.otherBank
        
        if let brandValue = cardInfo.brand {
            switch brandValue {
            case .visa:
                brand = .visa
            case .mastercard:
                brand = .mastercard
            case .mir:
                brand = .mir
            case .maestro:
                brand = .maestro
            default:
                break
            }
        }
        
        if cardInfo.type == .scm {
            bank = .socialCard
        }
        else if let bankIdString = cardInfo.bankId {
            bank = PaymentProvider.Bank(rawValueOptional: Int(bankIdString)) ?? .otherBank
        }
        
        let title = (cardInfo.bankTitle ?? String()) + paymentProviderTitle(from: pan)
        
        var provider = PaymentProvider(type: brand, bank: bank, title: title)
        provider.bankColor = cardInfo.bankFillingColor
        provider.assignDefaultValues()
        
        return provider
    }
    
    func comission(for cardType: PaymentCardType?) -> Int64 {
        guard let payment = paymentConfiguration.paymentBatch?.payments.first else { return 0 }
        return payment.comission(for: cardType) ?? 0
    }
    
    func paymentRequestTool(from paymentMethod: ChatPaymentMethod) -> PaymentRequest.ToolType {
        switch paymentMethod {
        case .bankCard:
            return .card
        case .mobilePhone:
            return .account
        }
    }
    
    func perform(after delay: TimeInterval, work: @escaping () -> ()) {
        let deadline: DispatchTime = .now() + delay
        DispatchQueue.main.asyncAfter(deadline: deadline, execute: work)
    }
    
    func canHandleFinishProcessing(with completion: @escaping () -> ()) -> Bool {
        
        let durationDifference = processingBeginTime.timeIntervalSinceNow + processingMinimumDuration
        
        guard durationDifference <= 0 else {
            perform(after: durationDifference, work: completion)
            return false
        }
        
        return true
    }
    
    func beginPaymentProcessing(savingCard: Bool) {
        
        guard let paymentBatch = paymentConfiguration.paymentBatch else { return }
        
        processingBeginTime = Date()
        router.showPaymentProcessingModule()
        
        let paymentsInfo: [PaymentBaseInfo] = paymentBatch.payments.map { PaymentBaseInfo(payment: $0) }
        let paymentTool = paymentRequestTool(from: paymentConfiguration.paymentMethod)
        let amount = paymentConfiguration.control.sumInMinorUnits
        let fullAmount = amount + comissionValue
        
        let cardType: PaymentCardType = bankCardInfo?.type ?? .other
        let cardTitle: String? = paymentProvider?.title ?? nil
        let paymentCardId = moduleConfiguration.selectedPaymentTool?.cardId
        let paymentCardInfo = PaymentCardInfo(expiryDate: bankCardExpiry, cardholderName: nil, pan: bankCardNumber, cvvc: bankCardCvv)
        let paymentCard = PaymentCard(cardId: paymentCardId, type: cardType, title: cardTitle, needRegister: savingCard, cardInfo: paymentCardInfo, extendedCardInfo: bankCardInfo)
        
        let request = PaymentRequest(payments: paymentsInfo, toolType: paymentTool, clientName: paymentConfiguration.clientName, clientEmail: paymentConfiguration.clientEmail, portalId: paymentSystemIdentifier, amount: amount, fullAmount: fullAmount, bankCard: paymentCard)
        paymentRequest = request
        
        interactor.beginPayment(for: paymentBatch.batchId, with: request)
    }
    
    func refreshPaymentStatus() {
        guard let paymentBatch = paymentConfiguration.paymentBatch else { return }
        retryRefreshPaymentStatusPerformer.cancelDelayedPerform()
        interactor.obtainPaymentStatus(for: paymentBatch.batchId)
    }
    
    func handleDidFinishPayment(for paymentBatch: PaymentBatch) {
        
        if let failedPayment = paymentBatch.payments.first(where: { $0.status == .failure }) {
            
            if let failMessage = failedPayment.failReasonDescription {
                router.showErrorAlert(with: failMessage)
            }
            else {
                router.showUnknownErrorAlert()
            }
        }
        else {
            
            if let callbackData = interactor.buildCallbackData(for: paymentConfiguration) {
                
                let controlResultHandler = paymentConfiguration.controlResultHandler
                let message = paymentConfiguration.message
                let embeddedMessage = paymentConfiguration.embeddedMessage
                
                controlResultHandler?.handleSendCallback(with: callbackData, for: message, embeddedMessage: embeddedMessage)
            }
            
            if let clientName = moduleConfiguration.paymentConfiguration.clientName {
                
                guard clientName.firstName != nil &&
                    clientName.lastName != nil &&
                    clientName.middleName != nil else { return }
                
                let email = moduleConfiguration.paymentConfiguration.clientEmail
                let userInfo = PaymentUserInfo(email: email, firstName: clientName.firstName!, lastName: clientName.lastName!, middleName: clientName.middleName!)
                
                interactor.savePaymentInfo(userInfo)
            }
            
            let request: PaymentRequest? = paymentRequest
            let configuration: PaymentProcessConfiguration = paymentConfiguration
            let provider: PaymentProvider? = paymentProvider
            let delegate = paymentConfiguration.paymentDelegate
            
            router.closeModule(goingBackToChat: true) {
                
                guard request != nil else { return }
                guard provider != nil else { return }
                
                delegate?.didProcess(request: request!, for: configuration, with: provider!)
            }
        }
    }
    
    //MARK: - AddPaymentMethodViewOutput
    
    func setupInitialState() {
        
        view.assignAcceptableCardNumberLengths(range: validator.acceptableCardNumberLengths)
        
        if let tool = selectedPaymentTool {
            
            view.setViewTitle(StringsHelper.paymentEnterCvvPageTitle)
            view.hideCancelButton()
            
            bankCardInfo = tool.extendedCardInfo
            paymentProvider = PaymentProvider(card: tool)
            
            refreshBankInfo()
            
            if let title = paymentProvider?.title {
                view.setCardTitle(title)
                view.setCardNumberFieldEnabled(false)
            }
            
            if let expiry = invertExpiryString(tool.cardInfo?.expiryDate) {
                view.setCardExpiry(expiry)
                view.setCardExpiryFieldEnabled(false)
            }
            
            view.setCardCodeHighlighted(true)
            view.hideScanCardButton()
            
            comissionValue = comission(for: tool.type)
            refreshComission()
        }
        else {
            let title = moduleConfiguration.isAddingFirstPaymentTool ? StringsHelper.paymentAddFirstToolCloseButtonTitle : StringsHelper.paymentAddAnotherToolCloseButtonTitle
            view.setCancelButtonTitle(title)
        }
    }
    
    func configure(with configuration: AddPaymentMethodModuleConfiguration) {
        self.moduleConfiguration = configuration
    }
    
    func didPressCancel() {
        let shouldGoBackToChat = paymentConfiguration.paymentTools.isEmpty
        router.closeModule(goingBackToChat: shouldGoBackToChat, completion: nil)
    }
    
    func didPressScanCard() {
        router.showScanCardModule { [weak self] cardNumber, expiryYear, expiryMonth in
            self?.view.setCardNumber(cardNumber)
            let expiry = self?.expiryString(from: expiryYear, and: expiryMonth)
            self?.view.setCardExpiry(expiry)
        }
    }
    
    func didPressDone(cardNumber: String?, cardExpiry: String?, cardCvv: String?) {
        
        if moduleConfiguration.selectedPaymentTool == nil {
            
            guard cardNumber != nil else { return }
            
            guard validator.validate(cardNumber: cardNumber!) else {
                view.setCardNumberHighlighted(true)
                router.showCardNumberValidatorErrorAlert()
                return
            }
            
            bankCardNumber = cardNumber
        }
        
        guard cardExpiry != nil && cardCvv != nil else { return }
        
        guard let expiryString = invertExpiryString(cardExpiry!) else {
            view.setCardExpiryHighlighted(true)
            router.showCardExpiryValidatorErrorAlert()
            return
        }
        guard validator.validate(expiry: expiryString) else {
            view.setCardExpiryHighlighted(true)
            router.showCardExpiryValidatorErrorAlert()
            return
        }
        guard validator.validate(cvvc: cardCvv!) else {
            view.setCardCodeHighlighted(true)
            router.showCardCvvValidatorErrorAlert()
            return
        }
        
        bankCardExpiry = expiryString
        bankCardCvv = cardCvv
        
        beginPaymentProcessing(savingCard: true)
    }
    
    func didEnterPanString(panString: String?) {
        
        let lastSearch = lastSearchPan
        lastSearchPan = panString
        
        guard var pan = panString else { return }
        
        var anotherPanSearch = true
        if lastSearch != nil {
            anotherPanSearch = !pan.hasPrefix(lastSearch!)
        }
        
        guard pan.count >= minimumPanLengthToDetermineBank else {
            bankCardInfo = nil
            view.hideAllBankInfo()
            return
        }
        guard bankCardInfo == nil || anotherPanSearch else { return }
        
        if pan.count > maximumPanLengthToDetermineBank {
            pan = String(pan.prefix(maximumPanLengthToDetermineBank))
        }
        
        interactor.obtainCardInfo(for: pan)
    }
    
    //MARK: - AddPaymentMethodInteractorOutput
    
    func didFinishObtainingCardInfo(with result: PaymentCardInfoRequestResult) {
        
        var cardType: PaymentCardType? = nil
        
        switch result {
            
        case .success(let cardInfo):
            
            let provider = paymentProvider(from: cardInfo, for: lastSearchPan)
            
            bankCardInfo = cardInfo
            paymentProvider = provider
            
            cardType = cardInfo.type
            
            refreshBankInfo()
            
        case .failure:
            let title = paymentProviderTitle(from: lastSearchPan)
            paymentProvider = PaymentProvider(type: .otherBankCard, bank: .otherBank, title: title)
        }
        
        comissionValue = comission(for: cardType)
        refreshComission()
    }
    
    func didFinishBeginningPayment(with result: PaymentRequestResult) {
        
        guard canHandleFinishProcessing(with: {
            self.didFinishBeginningPayment(with: result)
        }) else { return }
        
        switch result {
        case .success(let batch):
            interactor.obtainPaymentStatus(for: batch.batchId)
        case .failure(let error):
            router.hidePaymentProcessingModule { [weak self] in
                self?.display(error: error)
            }
        }
    }
    
    func didObtainRedirectRequest(with redirectInfo: PaymentRedirectInfo) {
        
        router.hidePaymentProcessingModule { [weak self] in
            
            guard redirectInfo.isValid else {
                self?.router.showUnknownErrorAlert()
                return
            }
            
            self?.router.showPayment3DSProcessingModule(with: redirectInfo, eventHandler: self)
        }
    }
    
    func didFinishObtainingPaymentStatus(with result: PaymentRequestResult) {
        
        guard canHandleFinishProcessing(with: {
            self.didFinishObtainingPaymentStatus(with: result)
        }) else { return }
        
        switch result {
        case .success(let batch):
            switch batch.status {
            case .some(.processing):
                retryRefreshPaymentStatusPerformer.performOnMain(afterDelay: retryRefreshPaymentStatusDelay)
            case .some(.finish):
                router.hidePaymentProcessingModule { [weak self] in
                    self?.handleDidFinishPayment(for: batch)
                }
            default:
                router.hidePaymentProcessingModule { [weak self] in
                    self?.router.showUnknownErrorAlert()
                }
            }
        case .failure(let error):
            router.hidePaymentProcessingModule { [weak self] in
                self?.display(error: error)
            }
        }
    }
    
    //MARK: - Payment3DSModuleEventHandler
    
    func didConfirmPayment() {
        
        processingBeginTime = Date()
        router.showPaymentProcessingModule()
        
        refreshPaymentStatus()
    }
}

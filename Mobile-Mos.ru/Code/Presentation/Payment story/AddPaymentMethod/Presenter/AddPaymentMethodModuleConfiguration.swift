//
//  AddPaymentMethodModuleConfiguration.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 01.06.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Конфигурация модуля добавления платежного инструмента
struct AddPaymentMethodModuleConfiguration {
    
    /// Конфигурация платежа
    let paymentConfiguration: PaymentProcessConfiguration
    
    /// Добавляется ли первый платежный инструмент
    var isAddingFirstPaymentTool: Bool = false
    
    /// Выбранный платежный инструмент
    var selectedPaymentTool: PaymentCard?
    
    init(paymentConfiguration: PaymentProcessConfiguration, isAddingFirstPaymentTool: Bool) {
        self.paymentConfiguration = paymentConfiguration
        self.isAddingFirstPaymentTool = isAddingFirstPaymentTool
    }
}

//
//  PaymentPreviewRouterInput.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 22.01.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

protocol PaymentPreviewRouterInput: class {

    func closeModule(completion: (() -> ())?)
}

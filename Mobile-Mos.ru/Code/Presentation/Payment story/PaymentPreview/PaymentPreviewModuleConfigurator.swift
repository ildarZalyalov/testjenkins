//
//  PaymentPreviewModuleConfigurator.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 22.01.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation
import UIKit

class PaymentPreviewModuleConfigurator: BaseModuleConfigurator {
    
    override func configureModule(for view: UIViewController) {
        
        let viewController = view as! PaymentPreviewViewController
        let presenter = PaymentPreviewPresenter()
        let router = PaymentPreviewRouter()
        
        let priceFormatter = NumberFormatter()
        priceFormatter.usesGroupingSeparator = true
        priceFormatter.numberStyle = .currency
        priceFormatter.minimumFractionDigits = 0
        priceFormatter.maximumFractionDigits = 2
        priceFormatter.locale = Locale(identifier: "ru_RU")
        
        viewController.output = presenter
        
        presenter.view = viewController
        presenter.router = router
        presenter.priceFormatter = priceFormatter
        
        router.transitionHandler = viewController
        router.toOtherModulesTransitionHandler = viewController
    }
}

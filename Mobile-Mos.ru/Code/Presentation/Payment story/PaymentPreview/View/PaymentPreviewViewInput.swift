//
//  PaymentPreviewViewInput.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 22.01.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

protocol PaymentPreviewViewInput: class {
    
    func setup(with style: ConversationStyle)
    
    func displayPayment(with subject: String, and sumString: String)
}

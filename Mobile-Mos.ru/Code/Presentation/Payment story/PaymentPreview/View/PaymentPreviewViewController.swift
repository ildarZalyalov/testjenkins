//
//  PaymentPreviewViewController.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 22.01.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import UIKit

class PaymentPreviewViewController: BaseViewController, PaymentPreviewViewInput, ConfigurableModuleController {
    
    var output: PaymentPreviewViewOutput!
    
    @IBOutlet weak var cancelBarButtonItem: UIBarButtonItem!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var contentStackView: UIStackView!
    @IBOutlet weak var rootStackView: UIStackView!
    @IBOutlet weak var submitContainerView: UIView!
    
    @IBOutlet weak var submitButton: KeyboardButtonControl!
    @IBOutlet weak var submitButtonShadowView: UIImageView!
    @IBOutlet weak var submitButtonBackgroundView: UIImageView!
    
    @IBOutlet weak var subjectLabel: UILabel!
    @IBOutlet weak var sumLabel: UILabel!
    
    let navigationItemSpaceWidth: CGFloat = 9
    let navigationItemsFontSize: CGFloat = 15
    let subjectLabelLineHeightMultiple: CGFloat = 1.33
    
    var statusBarStyle = UIStatusBarStyle.default
    
    //MARK: - Методы
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // ограничение языка на #available
        if #available(iOS 10.0, *) {}
        else {
            
            if var rightBarButtonItems = navigationItem.rightBarButtonItems {
                
                let spaceItem = UIBarButtonItem(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
                spaceItem.width = navigationItemSpaceWidth
                
                rightBarButtonItems = [spaceItem] + rightBarButtonItems + [spaceItem]
                navigationItem.rightBarButtonItems = rightBarButtonItems
            }
            
            let falseSpaceOnLeft = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
            navigationItem.leftBarButtonItems = [falseSpaceOnLeft]
        }
        
        submitButtonShadowView.tintColor = UIColorPalette.commandKeyboardButtonsShadowDefaultColor
        submitButtonShadowView.image = #imageLiteral(resourceName: "chatActivationButtonShadow").resizableImage(withCapInsets: UIEdgeInsets(top: 34, left: 39, bottom: 34, right: 39))
        submitButtonBackgroundView.image = #imageLiteral(resourceName: "chatActivationButtonBackground").resizableImage(withCapInsets: UIEdgeInsets(top: 24, left: 29, bottom: 24, right: 29))
        
        output.setupInitialState()
        
        subjectLabel.preferredMaxLayoutWidth = subjectLabel.width
        preferredContentSize = rootStackView.systemLayoutSizeFitting(UILayoutFittingCompressedSize)
    }
    
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return statusBarStyle
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    //MARK: - Actions
    
    @IBAction func pressedCancel() {
        output.didPressCancel()
    }
    
    @IBAction func pressedConfirm() {
        output.didPressConfirm()
    }
    
    //MARK: - ConfigurableModuleController
    
    func configureModule(with object: Any) {
        if let configuration = object as? PaymentProcessConfiguration {
            output.configure(with: configuration)
        }
    }
    
    //MARK: - PaymentPreviewViewInput
    
    func setup(with style: ConversationStyle) {
        
        statusBarStyle = style.isStatusBarDark ? .default : .lightContent
        setNeedsStatusBarAppearanceUpdate()
        
        let attributes: [NSAttributedStringKey : Any] = [
            NSAttributedStringKey.font : UIFont(customName: .graphikLCGRegular, size: navigationItemsFontSize) as Any,
            NSAttributedStringKey.foregroundColor : style.controlsTextColor
        ]
        
        cancelBarButtonItem.setTitleTextAttributes(attributes, for: .normal)
        cancelBarButtonItem.setTitleTextAttributes(attributes, for: .highlighted)
        
        submitButton.setTitleColor(style.controlsTextColor, for: .normal)
        submitButton.normalTintColor = style.controlsTextColor
        
        submitButton.setTitleColor(UIColorPalette.submitPaymentButtonBackgroundColor, for: .highlighted)
        submitButton.highlightedTintColor = UIColorPalette.submitPaymentButtonBackgroundColor
        
        submitButton.isHighlightedChanged = { [weak self] _, isHighlighted in
            guard let strongSelf = self else { return }
            strongSelf.submitButtonBackgroundView.tintColor = isHighlighted ? strongSelf.submitButton.normalTintColor : strongSelf.submitButton.highlightedTintColor
        }
    }
    
    func displayPayment(with subject: String, and sumString: String) {
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineHeightMultiple = subjectLabelLineHeightMultiple
        
        let attributes: [NSAttributedStringKey : Any] = [NSAttributedStringKey.font : subjectLabel.font, NSAttributedStringKey.paragraphStyle : paragraphStyle]
        let subjectString = NSAttributedString(string: subject, attributes: attributes)
    
        subjectLabel.attributedText = subjectString
        sumLabel.text = sumString
    }
}

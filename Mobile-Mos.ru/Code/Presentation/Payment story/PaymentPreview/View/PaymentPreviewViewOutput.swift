//
//  PaymentPreviewViewOutput.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 22.01.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

protocol PaymentPreviewViewOutput: class {
    
    func setupInitialState()
    
    func configure(with configuration: PaymentProcessConfiguration)
    
    func didPressCancel()
    
    func didPressConfirm()
}

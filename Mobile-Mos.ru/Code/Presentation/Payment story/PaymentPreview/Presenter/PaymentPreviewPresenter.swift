//
//  PaymentPreviewPresenter.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 22.01.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

class PaymentPreviewPresenter: PaymentPreviewViewOutput {
    
    weak var view: PaymentPreviewViewInput!
    var router: PaymentPreviewRouterInput!
    
    var moduleConfiguration: PaymentProcessConfiguration!
    var control: ChatMessagePaymentControl {
        return moduleConfiguration.control
    }
    
    var priceFormatter: NumberFormatter!
    
    //MARK: - PaymentPreviewViewOutput
    
    func setupInitialState() {
        
        view.setup(with: moduleConfiguration.conversation.style)
        
        let sum = control.sum
        guard let priceString = priceFormatter.string(from: NSNumber(value: sum)) else { return }
        view.displayPayment(with: control.subject, and: priceString)
    }
    
    func configure(with configuration: PaymentProcessConfiguration) {
        self.moduleConfiguration = configuration
    }
    
    func didPressCancel() {
        router.closeModule(completion: nil)
    }
    
    func didPressConfirm() {
        
        let paymentMethod: ChatPaymentMethod = .bankCard
        var configuration: PaymentProcessConfiguration = moduleConfiguration
        let delegate = moduleConfiguration.paymentDelegate
        
        configuration.paymentMethod = paymentMethod
        
        router.closeModule {
            delegate?.didSelect(paymentMethod: paymentMethod, for: configuration)
        }
    }
}

//
//  AuthorizationWarningTextView.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 26.04.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// UITextView с костылями для отключения выделения текста и других фич
class AuthorizationWarningTextView: UITextView, UITextViewDelegate {
    
    func setup() {
    
        delegate = self
        
        gestureRecognizers?.forEach { $0.isEnabled = type(of: $0) == UILongPressGestureRecognizer.self && $0.delaysTouchesEnded }
    }
    
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        return false
    }
    
    override var selectedRange: NSRange {
        get { return NSRange(location: 0, length: 0) }
        set { }
    }
    
    override var contentOffset: CGPoint {
        get { return .zero }
        set { }
    }
    
    //MARK: - UITextViewDelegate
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool {
        
        UIApplication.shared.open(url: URL)
        
        if URL == gosuslugiApplicationUrl {
            let analyticsManager: AnalyticsManager = AnalyticsManagerBuilder.getAnalyticsManager()
            analyticsManager.sendAuthorizationSignInEvent(with: .gosUslugiPressed(withString: AnalyticsConstants.gosuslugiLinkEventName), errorText: nil)
        }
        
        return false
    }
}

//
//  AuthorizationViewInput.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 21.12.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol AuthorizationViewInput: class {
    
    func hideActivityIndicator()
    
    func hideKeyboard()
}

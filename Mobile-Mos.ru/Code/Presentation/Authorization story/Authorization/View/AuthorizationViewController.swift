//
//  AuthorizationViewController.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 21.12.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import UIKit

class AuthorizationViewController: BaseViewController, ConfigurableModuleController, AuthorizationViewInput, UITextFieldDelegate {
    
    var output: AuthorizationViewOutput!
    var notificationCenter: NotificationCenter!
    var phoneTextFieldFormatManager: TextFieldFormatManager!
    
    let textFieldsLabelAffineTransform = CGAffineTransform(scaleX: 0.8, y: 0.8)
    let textFieldsLabelAnimationDuration = 0.6
    let phonePrefix = "+7"
    
    var isContentSizeHeightChangedWithKeyboard = false
    var sendButtonText = ""
    
    let warningTextLineSpacing: CGFloat = 3
    let warningTextFontSize: CGFloat = 17
    
    @IBOutlet weak var warningTextView: AuthorizationWarningTextView!
    
    @IBOutlet weak var loginSeparatorView: UIView!
    @IBOutlet weak var passwordSeparatorView: UIView!
    @IBOutlet weak var loginWarningImage: UIImageView!
    @IBOutlet weak var passwordWarningImage: UIImageView!
    
    @IBOutlet var textFields: [UITextField]!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var passwordLabel: UILabel!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var forgotPasswordButton: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var sendButton: UIButton!
    
    @IBOutlet weak var singUpStackView: UIStackView!
    @IBOutlet weak var loginStackView: UIStackView!
    @IBOutlet weak var passwordStackView: UIStackView!
    @IBOutlet weak var numberPlaceholderLabel: UILabel!
    
    @IBOutlet var constraintsToCollapseOnSmallScreen: [NSLayoutConstraint]!
    
    //MARK: - Методы
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        phoneTextFieldFormatManager.configure(with: phoneTextField, mask: nil)
        
        setupKeyboardNotifications()
        setupGestures()
        setupWarningText()
        
        if UIDevice.current.hasCompactScreen {
            
            for constraint in constraintsToCollapseOnSmallScreen {
                constraint.constant /= 2
            }
            
            view.setNeedsLayout()
        }
        
        output.setupInitialState()
    }
    
    deinit {
        notificationCenter.removeObserver(self)
    }
    
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    //MARK: - AuthorizationViewInput
    
    func hideActivityIndicator() {
        activityIndicator.stopAnimating()
        sendButton.setTitle(sendButtonText, for: .normal)
        textFields.forEach{ $0.isEnabled = true }
    }
    
    func showActivityIndicator() {
        
        guard let buttonText = sendButton.titleLabel?.text else { return }
        
        sendButtonText = buttonText
        
        sendButton.setTitle("", for: .normal)
        
        textFields.forEach{ $0.isEnabled = false }
        
        activityIndicator.startAnimating()
    }
    
    func hideKeyboard() {
        userDidTapOnView()
    }
    
    //MARK: - ConfigurableModuleController
    
    func configureModule(with object: Any) {
        if let object = object as? AuthorizationProcessHandler {
            output.configureModule(with: object)
        }
    }
    
    //MARK: - UITextFieldDelegate
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == phoneTextField {
           return phoneTextFieldFormatManager.textField!(textField, shouldChangeCharactersIn: range, replacementString: string)
        }
        
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == phoneTextField {
            
            numberPlaceholderLabel.isHidden = false
            loginWarningImage.isHidden = true
            
            loginSeparatorView.backgroundColor = UIColorPalette.selectedTextFieldSeparatorColor
        }
        else {
            
            passwordWarningImage.isHidden = true
            passwordSeparatorView.backgroundColor = UIColorPalette.selectedTextFieldSeparatorColor
        }
        
        animateTextFieldTitleState(with: textField)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField == phoneTextField, let text = textField.text, text.isEmpty {
            numberPlaceholderLabel.isHidden = true
        }
        
        revertTextFieldTitleState(with: textField)
        validateTextField(textField, onFormSubmit: false)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        userDidTapOnView()
        logInButtonPressed(self)
        
        return true 
    }
    
    //MARK: - Custom methods
    
    func setupKeyboardNotifications() {
        
        guard UIDevice.current.hasConfinedScreen else { return }
        
        notificationCenter.addObserver(self,
                                       selector: #selector(self.keyboardWillShow(with:)),
                                       name: NSNotification.Name.UIKeyboardWillShow,
                                       object: nil)
        
        notificationCenter.addObserver(self,
                                       selector: #selector(self.keyboardWillHide(with:)),
                                       name: NSNotification.Name.UIKeyboardWillHide,
                                       object: nil)
    }
    
    func setupGestures() {
        
        scrollView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(userDidTapOnView)))
        loginStackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(showPhoneTextField)))
        passwordStackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(showPasswordTextField)))
        singUpStackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(AuthorizationViewController.createNewProfileButtonPressed)))
    }
    
    func setupWarningText() {
        
        warningTextView.setup()
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .left
        paragraphStyle.lineSpacing = warningTextLineSpacing
        
        let attributes: [NSAttributedStringKey : Any] = [
            NSAttributedStringKey.foregroundColor : UIColor.black,
            NSAttributedStringKey.font : UIFont.systemFont(ofSize: warningTextFontSize),
            NSAttributedStringKey.paragraphStyle : paragraphStyle
        ]
        
        let linkAttributes: [NSAttributedStringKey : Any] = [
            NSAttributedStringKey.foregroundColor : UIColorPalette.authorizationFormLinksColor,
            NSAttributedStringKey.underlineColor : UIColorPalette.authorizationFormLinksColor,
            NSAttributedStringKey.underlineStyle : NSNumber(value: NSUnderlineStyle.styleSingle.rawValue)
        ]
        
        let warning = StringsHelper.authorizationHelpText
        let gosuslugi = StringsHelper.authorizationHelpGosuslugiAppName
        
        let gosuslugiRange = (warning as NSString).range(of: gosuslugi)
        let attributedWarning = NSMutableAttributedString(string: warning, attributes: attributes)
        
        if gosuslugiRange.location != NSNotFound {
            attributedWarning.addAttributes(linkAttributes, range: gosuslugiRange)
            attributedWarning.addAttribute(.link, value: gosuslugiApplicationUrl, range: gosuslugiRange)
        }
        
        warningTextView.attributedText = attributedWarning
    }
    
    func animateTextFieldTitleState(with textField: UITextField) {
        
        switch textField {
        case phoneTextField:
            UIView.animate(withDuration: textFieldsLabelAnimationDuration, animations: { [weak self] in
                
                guard let strongSelf = self else { return }
                
                strongSelf.phoneNumberLabel.transform = strongSelf.textFieldsLabelAffineTransform
                strongSelf.phoneNumberLabel.x = strongSelf.numberPlaceholderLabel.x
            })
        case passwordTextField:
            
            UIView.animate(withDuration: textFieldsLabelAnimationDuration, animations: { [weak self] in
                
                guard let strongSelf = self else { return }
                
                strongSelf.passwordLabel.transform = strongSelf.textFieldsLabelAffineTransform
                strongSelf.passwordLabel.x = textField.x
            })
        default:
            return
        }
    }
    
    func revertTextFieldTitleState(with textField: UITextField) {
        
        switch textField {
        case phoneTextField:
            UIView.animate(withDuration: textFieldsLabelAnimationDuration, animations: { [weak self] in
                guard let strongSelf = self else { return }
                
                self?.phoneNumberLabel.transform = .identity
                self?.phoneNumberLabel.x = strongSelf.numberPlaceholderLabel.x
            })
        case passwordTextField:
            
            UIView.animate(withDuration: textFieldsLabelAnimationDuration, animations: { [weak self] in
                
                self?.passwordLabel.transform = .identity
                self?.passwordLabel.x = textField.x
            })
            
        default:
            return
        }
    }
    
    func obtainPhoneNumberWithCorrectFormat(from phoneText: String?) -> String? {
        
        guard let phoneString = phoneText else { return nil }
        
        let phoneTextWithPrefix = phonePrefix + phoneString
        return phoneTextWithPrefix.trimAll(exeptFor: CharacterSet.decimalDigits.inverted)
    }
    
    func updateFieldState(inFieldWith type: AuthorizationViewFieldType, hasError: Bool) {
        
        let isWarningHidden = !hasError
        let separatorColor = hasError ? UIColorPalette.errorInUserDataColor : UIColorPalette.unselectedTextFieldSeparatorColor
        
        switch type {
        case .phone:
            loginWarningImage.isHidden = isWarningHidden
            loginSeparatorView.backgroundColor = separatorColor
        case .password:
            passwordWarningImage.isHidden = isWarningHidden
            passwordSeparatorView.backgroundColor = separatorColor
        }
    }
    
    @discardableResult
    func validateTextField(_ textField: UITextField, onFormSubmit: Bool) -> Bool {
        
        var fieldType: AuthorizationViewFieldType
        var text: String?
        
        switch textField {
        case phoneTextField:
            fieldType = .phone
            text = obtainPhoneNumberWithCorrectFormat(from: textField.text)
        case passwordTextField:
            fieldType = .password
            text = textField.text
        default:
            return true
        }
        
        let valid = output.validateField(with: (fieldType, text), onFormSubmit: onFormSubmit)
        updateFieldState(inFieldWith: fieldType, hasError: !valid)
        
        return valid
    }
    
    //MARK: - Observed methods
    
    @objc
    open func userDidTapOnView() {
        self.view.endEditing(true)
    }
    
    @objc
    func showPhoneTextField() {
        phoneTextField.becomeFirstResponder()
    }
    
    @objc
    func showPasswordTextField() {
        passwordTextField.becomeFirstResponder()
    }
    
    @objc
    func keyboardWillShow(with notification: Notification) {
        
        guard let info = notification.userInfo,
            let keyboardEndSize = (info[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size
            else { return }
        
        guard !isContentSizeHeightChangedWithKeyboard else { return }
        
        isContentSizeHeightChangedWithKeyboard = true
        
        scrollView.contentSize.height = scrollView.height + keyboardEndSize.height
        
        var insets = scrollView.scrollIndicatorInsets
        insets.bottom += keyboardEndSize.height
        scrollView.scrollIndicatorInsets = insets
    }
    
    @objc
    func keyboardWillHide(with notification: Notification) {
        
        guard let info = notification.userInfo,
            let keyboardEndSize = (info[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size
            else { return }
        
        isContentSizeHeightChangedWithKeyboard = false 
        scrollView.contentSize.height -= keyboardEndSize.height
        
        var insets = scrollView.scrollIndicatorInsets
        insets.bottom -= keyboardEndSize.height
        scrollView.scrollIndicatorInsets = insets
    }
    
    //MARK: - Buttons action
    
    @IBAction func unwindToAuthorization(_: UIStoryboardSegue) {}
    
    @IBAction func cancelButtonPressed(_ sender: Any) {
        
        hideKeyboard()
        output.didPressCancel()
    }
    
    @IBAction func logInButtonPressed(_ sender: Any) {
        
        let fields: [UITextField] = [phoneTextField, passwordTextField]
        for field in fields {
            guard validateTextField(field, onFormSubmit: true) else { return }
        }
        
        showActivityIndicator()
        output.didPressLogin(withPhone: obtainPhoneNumberWithCorrectFormat(from: phoneTextField.text), password: passwordTextField.text)
    }
    
    @IBAction func createNewProfileButtonPressed(_ sender: Any) {
        output.didPressRegistration()
    }
    
    @IBAction func forgotPasswordPressed(_ sender: Any) {
        output.didPressNewPassword()
    }
}

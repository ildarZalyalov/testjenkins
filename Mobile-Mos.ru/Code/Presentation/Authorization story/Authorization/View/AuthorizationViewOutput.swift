//
//  AuthorizationViewOutput.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 21.12.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

enum AuthorizationViewFieldType {
    case phone
    case password
}

typealias AuthorizationViewFieldValue = (type: AuthorizationViewFieldType, text: String?)

protocol AuthorizationViewOutput: class {
    
    func setupInitialState()
    
    func didPressCancel()
    
    func validateField(with value: AuthorizationViewFieldValue, onFormSubmit: Bool) -> Bool
    
    func didPressLogin(withPhone phoneNumber: String?, password: String?)
    
    func didPressRegistration()
    
    func didPressNewPassword()
    
    func configureModule(with handlerObject: AuthorizationProcessHandler?)
}

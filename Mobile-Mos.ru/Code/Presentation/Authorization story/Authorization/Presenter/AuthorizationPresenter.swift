//
//  AuthorizationPresenter.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 21.12.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class AuthorizationPresenter: AuthorizationViewOutput, AuthorizationInteractorOutput, RegistrationProcessHandler {
    
    weak var view: AuthorizationViewInput!
    var router: AuthorizationRouterInput!
    var interactor: AuthorizationInteractorInput!
    
    weak var authorizationProcessHandler: AuthorizationProcessHandler?
    
    let minCountOfPassword = 5
    let minCountOfPhoneCharacters = 11
    
    //MARK: - Приватные методы
    
    func closeModule() {
        
        let cancelHandled = authorizationProcessHandler?.handleCancelAuthorization() ?? false
        guard !cancelHandled else { return }
        
        router.closeCurrentModule()
    }
    
    func finishAuthorization() {
        view.hideKeyboard()
        authorizationProcessHandler?.handleFinishAuthorization()
        closeModule()
    }
    
    //MARK: - AuthorizationViewOutput
    
    func setupInitialState() {
        interactor.sendEvent(with: .screenLoaded(withString: AnalyticsConstants.screenLoadedEventName), errorText: nil)
    }
    
    func configureModule(with handlerObject: AuthorizationProcessHandler?) {
        authorizationProcessHandler = handlerObject
    }
    
    func didPressCancel() {
        closeModule()
    }
    
    func validateField(with value: AuthorizationViewFieldValue, onFormSubmit: Bool) -> Bool {
        
        switch value.type {
        case .phone:
            guard let phoneNumber = value.text, !phoneNumber.isEmpty && phoneNumber.count == minCountOfPhoneCharacters else {
                if onFormSubmit {
                    router.showPhoneNumberNotValidErrorModule()
                }
                return false
            }
        case .password:
            guard let password = value.text, !password.isEmpty else {
                if onFormSubmit {
                    router.showPasswordIsEmptyErrorModule()
                }
                return false
            }
            
            guard password.count >= minCountOfPassword else {
                if onFormSubmit {
                    router.showPasswordTooSmallErrorModule()
                }
                return false
            }
        }
        
        return true
    }
    
    func didPressLogin(withPhone phoneNumber: String?, password: String?) {
        
        guard phoneNumber != nil && password != nil else { return }
        guard interactor.hasConnectionToServer else {
            view.hideActivityIndicator()
            router.showLoginErrorModule(with: StringsHelper.noConnectionErrorText)
            return
        }
        
        interactor.login(with: phoneNumber!, and: password!)
    }
    
    func didPressRegistration() {
        interactor.sendEvent(with: .createProfile(withString: AnalyticsConstants.createProfileEventName), errorText: nil)
        router.showRegistrationModule(with: self)
    }
    
    func didPressNewPassword() {
        interactor.sendEvent(with: .forgotPassword(withString: AnalyticsConstants.forgotPasswordEventName), errorText: nil)
    }
    
    //MARK: - AuthorizationInteractorOutput
    
    func didFinishLoggingIn(result: UserLoginResult) {
        
        switch result {
        case .success(_):
            interactor.sendEvent(with: .logInPressed(withString: AnalyticsConstants.loginInEventName), errorText: nil)
            finishAuthorization()
        case .failure(let error):
            router.showLoginErrorModule(with: error.localizedDescription.firstLetterCapitalized())
            interactor.sendEvent(with: .logInPressed(withString: AnalyticsConstants.loginInEventName), errorText: error.localizedDescription)
        }
        
        view.hideActivityIndicator()
    }
    
    //MARK: - AuthorizationHandler
    
    func handleCancelRegistration() -> Bool {
        closeModule()
        return true
    }
    
    func handleWillFinishRegistration() {
        interactor.handleBeforeSuccessfulRegistration()
    }
    
    func handleFinishRegistration(with model: UserRegisterRequest) {
        finishAuthorization()
    }
}

//
//  AuthorizationInteractorInput.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 21.12.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol AuthorizationInteractorInput: class {
    
    var hasConnectionToServer: Bool { get }
    
    /// Произвести авторизацию пользователя используя логин и пароль
    ///
    /// - Parameters:
    ///   - login: логин пользователя
    ///   - password: пароль пользователя
    func login(with login: String, and password: String)
    
    func handleBeforeSuccessfulRegistration()
    
    /// Отправить событие связанное с авторизацией
    ///
    /// - Parameters:
    ///   - eventType: тип события
    ///   - parameter: параметр события если не связан со статусом авторизации пользователя
    ///   - value: значение параметра
    func sendEvent(with eventType: AuthorizationSignInEvents, errorText: String?)
}

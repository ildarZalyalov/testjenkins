//
//  AuthorizationInteractor.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 21.12.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class AuthorizationInteractor: AuthorizationInteractorInput {
    
    weak var output: AuthorizationInteractorOutput!
    
    var userService: UserService!
    var userInfoService: UserInfoAndSettingsService!
    var messageHistoryService: ChatMessageHistoryService!
    var conversationsService: ConversationsService!
    var analyticsManager: AnalyticsManager!
    
    //MARK: - Приватные методы
    
    func beforeSuccessfulLoginHandler() {
        conversationsService.clearCachedConversations()
        messageHistoryService.clearCachedHistory(for: userService.currentUser)
    }
    
    //MARK: - AuthorizationInteractorInput
    
    var hasConnectionToServer: Bool {
        return userService.hasConnectionToServer
    }
    
    func login(with login: String, and password: String) {
        userService.login(with: login, and: password, beforeSuccess: { [weak self] _ in
            self?.beforeSuccessfulLoginHandler()
        }) { [weak self] result in
            self?.userInfoService.obtainCurrentUserInfo()
            self?.userInfoService.refreshNotificationSettings(completion: nil)
            self?.output.didFinishLoggingIn(result: result)
        }
    }
    
    func handleBeforeSuccessfulRegistration() {
        beforeSuccessfulLoginHandler()
    }
    
    func sendEvent(with eventType: AuthorizationSignInEvents, errorText: String?) {
        analyticsManager.sendAuthorizationSignInEvent(with: eventType, errorText: errorText)
    }
}

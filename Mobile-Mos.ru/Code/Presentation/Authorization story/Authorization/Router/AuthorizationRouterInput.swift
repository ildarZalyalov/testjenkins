//
//  AuthorizationRouterInput.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 21.12.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol AuthorizationRouterInput: class {
    
    func closeCurrentModule()
    
    func showLoginErrorModule(with message: String)
    
    func showPasswordIsEmptyErrorModule()
    
    func showPasswordTooSmallErrorModule()
    
    func showPhoneNumberNotValidErrorModule()
    
    func showRegistrationModule(with handler: RegistrationProcessHandler?)
}

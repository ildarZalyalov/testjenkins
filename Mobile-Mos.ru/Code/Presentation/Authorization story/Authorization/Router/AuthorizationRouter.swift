//
//  AuthorizationRouter.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 21.12.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class AuthorizationRouter: AuthorizationRouterInput {
    
    weak var transitionHandler: UIViewController!
    weak var alertTransitionHandler: UIViewController!
    weak var dataTransitionHandler: DataTransferModuleController!
    
    var alertsFactory: CommonAlertsFactory!
    
    let registrationSegueIdentifier = "registrationSegue"
    let unwindToConversationsSegueIdentifier = "unwindToConversationsSegue"
    
    //MARK: - AuthorizationRouterInput
    
    func closeCurrentModule() {
        transitionHandler.performSegue(withIdentifier: unwindToConversationsSegueIdentifier, sender: nil)
    }
    
    func showLoginErrorModule(with message: String) {
        
        let alert = alertsFactory.getErrorAlert(with: message)
        alertTransitionHandler.present(alert, animated: true, completion: nil)
    }
    
    func showPasswordIsEmptyErrorModule() {
        let alert =  alertsFactory.getErrorAlert(with: StringsHelper.fillPasswordField)
        transitionHandler.present(alert, animated: true, completion: nil)
    }
    
    func showPasswordTooSmallErrorModule() {
        let alert =  alertsFactory.getErrorAlert(with: StringsHelper.minCountOfPassword)
        transitionHandler.present(alert, animated: true, completion: nil)
    }
    
    func showPhoneNumberNotValidErrorModule() {
        let alert =  alertsFactory.getErrorAlert(with: StringsHelper.minCountOfPhoneNumber)
        transitionHandler.present(alert, animated: true, completion: nil)
    }
    
    func showRegistrationModule(with handler: RegistrationProcessHandler?) {
        dataTransitionHandler.performSegue(with: registrationSegueIdentifier, sender: nil) { (configurableController) in
            configurableController.configureModule(with: handler as Any)
        }
    }

}

//
//  AuthorizationModuleConfigurator.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 21.12.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import UIKit

class AuthorizationModuleConfigurator: BaseModuleConfigurator {
    
    override func configureModule(for view: UIViewController) {
        
        let viewController = view as! AuthorizationViewController
        let presenter = AuthorizationPresenter()
        let interactor = AuthorizationInteractor()
        let router = AuthorizationRouter()
        
        let alertsFactory = CommonAlertsFactoryImplementation()
        let conversationsService = UIApplication.shared.serviceBuilder.getConversationsService()
        let userService = UIApplication.shared.serviceBuilder.getUserService()
        let userInfoService = UIApplication.shared.serviceBuilder.getUserInfoAndSettingsService()
        let messageHistoryService = UIApplication.shared.serviceBuilder.getChatMessageHistoryService()
        let notificationCenter = NotificationCenter.default
        let textFieldFormatManager = TextFieldFormatManagerImplementation()
        let analyticsManager = AnalyticsManagerBuilder.getAnalyticsManager()
        
        viewController.output = presenter
        viewController.notificationCenter = notificationCenter
        viewController.phoneTextFieldFormatManager = textFieldFormatManager
        
        presenter.view = viewController
        presenter.interactor = interactor
        presenter.router = router
        
        interactor.output = presenter
        interactor.userService = userService
        interactor.userInfoService = userInfoService
        interactor.messageHistoryService = messageHistoryService
        interactor.conversationsService = conversationsService
        interactor.analyticsManager = analyticsManager
        
        router.transitionHandler = viewController
        router.alertsFactory = alertsFactory
        router.alertTransitionHandler = viewController
        router.dataTransitionHandler = viewController
    }
}

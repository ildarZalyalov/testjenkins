//
//  NewPasswordDataInput.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 09.04.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Данные для передачи в модуль нового пароля
typealias NewPasswordDataInput = (phone: String, timeOut: Int, delegate: ForgotPasswordDelegate?)

//
//  NewPasswordInteractorInput.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 01.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

protocol NewPasswordInteractorInput: class {
    
    func setNewPassword(with code: String, password: String)
    
    func getUserRecoverySmsCode(with phone: String)
    
    /// Кол-во запросов смс кода
    var codeRequestTimes: Int { get }
    
    var hasConnectionToServer: Bool { get }
    
    func sendEvent(with eventType: AuthorizationNewPasswordEvents, errorText: String?)
}

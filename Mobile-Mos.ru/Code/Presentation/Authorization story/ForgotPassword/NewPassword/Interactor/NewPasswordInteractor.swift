//
//  NewPasswordInteractor.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 01.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

class NewPasswordInteractor: NewPasswordInteractorInput {
    
    weak var output: NewPasswordInteractorOutput!
    
    var userSerice: UserService!
    var userDefaults: UserDefaults!
    var analyticsManager: AnalyticsManager!
    
    var codeRequestTimes: Int {
        get {
            return userDefaults.integer(forKey: ApplicationUserDefaultsKey.forgotPasswordCodeRequestTimes.rawValue)
        }
        set {
            userDefaults.set(newValue, forKey: ApplicationUserDefaultsKey.forgotPasswordCodeRequestTimes.rawValue)
            userDefaults.set(Date(), forKey: ApplicationUserDefaultsKey.forgotPasswordCacheDate.rawValue)
            userDefaults.synchronize()
        }
    }
    
    //MARK: - NewPasswordInteractorInput
    
    var hasConnectionToServer: Bool {
        return userSerice.hasConnectionToServer
    }
    
    func setNewPassword(with code: String, password: String) {
        
        userSerice.setNewPassword(with: code, newPassword: password) { [weak self] (result) in
            
            self?.output.didFinishChangeUserPassword(with: result)
        }
    }
    
    func getUserRecoverySmsCode(with phone: String) {
        
        userSerice.obtainUserRecoveryCode(with: phone) { [weak self] (result) in
            
            switch result {
            case .success(_):
                self?.codeRequestTimes += 1
                self?.output.didFinishObtainSmsCode(with: result)
            default: return
            }
            
        }
    }
    
    func sendEvent(with eventType: AuthorizationNewPasswordEvents, errorText: String?) {
        analyticsManager.sendNewPasswordEvent(with: eventType, errorText: errorText)
    }
}

//
//  NewPasswordInteractorOutput.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 01.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

protocol NewPasswordInteractorOutput: class {
    
    func didFinishChangeUserPassword(with result: UserNewPasswordResult)
    
    func didFinishObtainSmsCode(with result: UserObtainSmsCodeResult)
}

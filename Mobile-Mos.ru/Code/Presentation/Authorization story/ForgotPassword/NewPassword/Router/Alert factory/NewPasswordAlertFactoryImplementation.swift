//
//  NewPasswordAlertFactoryImplementation.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 02.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

class NewPasswordAlertFactoryImplementation: NewPasswordAlertFactory {
    
    func getOkAlert(with eventHandler: NewPasswordAlertFactoryActionEventHandler?) -> UIViewController {
        
        let alert = UIAlertController(title: StringsHelper.changePasswordSuccess, message:nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (alert: UIAlertAction!) in
            eventHandler?.didSelectOk()
        }))
        
        return alert
    }
}

//
//  NewPasswordAlertFactory.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 02.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Обработчик нажатий
protocol NewPasswordAlertFactoryActionEventHandler {
    
    func didSelectOk()
}

/// Фабрика алертов данного модуля
protocol NewPasswordAlertFactory {
    
    /// Получить алерт для выхода
    func getOkAlert(with eventHandler: NewPasswordAlertFactoryActionEventHandler?) -> UIViewController
}

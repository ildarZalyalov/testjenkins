//
//  NewPasswordRouterInput.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 01.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

protocol NewPasswordRouterInput: class {
    
    func closeForgotPasswordModules()
    
    func showCodeIsEmptyErrorModule()
    
    func showPasswordIsEmptyErrorModule()
    
    func showPasswordTooSmallErrorModule()
    
    func showChangePasswordSuccessAlert(with handler: NewPasswordAlertFactoryActionEventHandler?)
    
    func showCodeRequestWarningAlert(with requestTimes: Int)
    
    func showErrorAlert(with message: String)
}

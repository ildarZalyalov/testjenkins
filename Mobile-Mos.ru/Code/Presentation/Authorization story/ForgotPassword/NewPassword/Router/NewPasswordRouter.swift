//
//  NewPasswordRouter.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 01.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

class NewPasswordRouter: NewPasswordRouterInput {
    
    weak var transitionHandler: UIViewController!
    var commonAlertsFactory: CommonAlertsFactory!
    var alertsFactory: NewPasswordAlertFactory!
    var minTimeRequestCode = 3
    var maxTimeRequestCode = 5
    
    let unwindToAuthorizationSegueIdentifier = "unwindToAuthorization"
    
    //MARK: - NewPasswordRouterInput
    
    func closeForgotPasswordModules() {
        transitionHandler.performSegue(withIdentifier: unwindToAuthorizationSegueIdentifier, sender: nil)
    }
    
    func showCodeIsEmptyErrorModule() {
        let alert = commonAlertsFactory.getErrorAlert(with: StringsHelper.fillCodeField)
        transitionHandler.present(alert, animated: true, completion: nil)
    }
    
    func showPasswordIsEmptyErrorModule() {
        let alert = commonAlertsFactory.getErrorAlert(with: StringsHelper.fillPasswordField)
        transitionHandler.present(alert, animated: true, completion: nil)
    }
    
    func showPasswordTooSmallErrorModule() {
        let alert = commonAlertsFactory.getErrorAlert(with: StringsHelper.minCountOfPassword)
        transitionHandler.present(alert, animated: true, completion: nil)
    }
    
    func showChangePasswordSuccessAlert(with handler: NewPasswordAlertFactoryActionEventHandler?) {
        
        let alert = alertsFactory.getOkAlert(with: handler)
        transitionHandler.present(alert, animated: true, completion: nil)
    }
    
    func showErrorAlert(with message: String) {
        let alert = commonAlertsFactory.getErrorAlert(with: message)
        transitionHandler.present(alert, animated: true, completion: nil)
    }
    
    func showCodeRequestWarningAlert(with requestTimes: Int) {
        
        guard requestTimes > minTimeRequestCode else { return }
        
        var alert: UIViewController
        
        if requestTimes > minTimeRequestCode && requestTimes < maxTimeRequestCode {
            
            alert = commonAlertsFactory.getErrorAlert(with: StringsHelper.warningAlertTitle, body: StringsHelper.fourTimesRequestedCodeText)
            transitionHandler.present(alert, animated: true, completion: nil)
        }
        else if requestTimes >= maxTimeRequestCode {
            
            alert = commonAlertsFactory.getErrorAlert(with: StringsHelper.warningAlertTitle, body: StringsHelper.fiveTimesRequestedCodeText)
            transitionHandler.present(alert, animated: true, completion: nil)
        }
    }
}

//
//  NewPasswordViewOutput.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 01.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

enum NewPasswordViewFieldType {
    case code
    case password
}

typealias NewPasswordViewFieldValue = (type: NewPasswordViewFieldType, text: String?)

protocol NewPasswordViewOutput: class {
    
    func validateField(with value: NewPasswordViewFieldValue, onFormSubmit: Bool) -> Bool
    
    func updateUserPassword(with code: String?, newPassword: String?)
    
    func resendSmsCode(with userPhone: String)
    
    func showWarningAlertIfNeeded()
    
    func setupInitialState() 
}

//
//  NewPasswordViewController.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 01.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import UIKit

class NewPasswordViewController: BaseViewController, NewPasswordViewInput, ConfigurableModuleController, NavigationBarCustomizingController, UITextFieldDelegate {
    
    var output: NewPasswordViewOutput!
    var notificationCenter: NotificationCenter!
    
    let textFieldsLabelAffineTransform = CGAffineTransform(scaleX: 0.8, y: 0.8)
    let textFieldsLabelAnimationDuration = 0.6
    
    var timer: Timer = Timer()
    var timerCount: Int = 0
    var timeInterval = 1
    var underlineStringAttributes:  [NSAttributedStringKey: Any] = [NSAttributedStringKey.underlineStyle : NSUnderlineStyle.styleSingle.rawValue,
                                                                    NSAttributedStringKey.foregroundColor: UIColorPalette.resendSmsTitleColor,
                                                                    NSAttributedStringKey.font           : UIFont.init(customName: UIFont.CustomFontName.graphikLCGRegular, size: 15)!,
                                                                    NSAttributedStringKey.underlineColor : UIColorPalette.resendSmsTitleColor]
    
    var resendButtonAttributedString: NSAttributedString = NSAttributedString(string: "Отправить повторно",
                                                                              attributes: [NSAttributedStringKey.foregroundColor: UIColorPalette.resendSmsTitleColor,
                                                                                           NSAttributedStringKey.font : UIFont.init(customName: UIFont.CustomFontName.graphikLCGRegular, size: 15)!,])
    
    var isContentSizeHeightChangedWithKeyboard = false 
    var userPhone: String!
    
    var sendButtonText = ""
    
    @IBOutlet var textFields: [UITextField]!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var checkSmsLabel: UILabel!
    @IBOutlet weak var codeTextField: UITextField!
    @IBOutlet weak var codeSeparatorView: UIView!
    @IBOutlet weak var codeWarningImage: UIImageView!
    @IBOutlet weak var codeStackView: UIStackView!
    
    @IBOutlet weak var newPasswordLabel: UILabel!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var passwordStackView: UIStackView!
    
    @IBOutlet weak var eyeButton: UIButton!
    @IBOutlet weak var passwordSeparatorView: UIView!
    
    @IBOutlet weak var sendSmsAgainStackView: UIStackView!
    @IBOutlet weak var resendSmsButton: UIButton!
    @IBOutlet weak var afterTimeLabel: UILabel!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var sendButton: UIButton!
    
    weak var forgotPasswordDelegate: ForgotPasswordDelegate?
    
    //MARK: - NavigationBarCustomizingController
    
    var navigationBarState: NavigationBarState? {
        
        var state = NavigationBarState()
        
        state.isHidden = false
        state.isTranslucent = false
        state.backgroundState = NavigationBarBackgroundState.regular
        state.tintColor = UIColor.black
        state.isShadowHidden = true
        
        return state
    }
    
    //MARK: - Методы
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        output.setupInitialState() 
        setupKeyboardNotifications()
        setupGestures()
        
        setupTimer(with: timerCount)
        output.showWarningAlertIfNeeded()
            
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    deinit {
        notificationCenter.removeObserver(self)
    }
    
    
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
   
    //MARK: - ConfigurableModuleController
    
    func configureModule(with object: Any) {
        
        if let object = object as? NewPasswordDataInput {

           userPhone = object.phone
           timerCount = object.timeOut
           forgotPasswordDelegate = object.delegate
        }
    }
    
    //MARK: - SmsConfirmationViewInput
    
    func hideActivityIndicator() {
        activityIndicator.stopAnimating()
        sendButton.setTitle(sendButtonText, for: .normal)
        textFields.forEach{ $0.isEnabled = true }
    }
    
    func showActivityIndicator() {
        
        guard let buttonText = sendButton.titleLabel?.text else { return }
        
        sendButtonText = buttonText
        
        sendButton.setTitle("", for: .normal)
        
        textFields.forEach{ $0.isEnabled = false }
        activityIndicator.startAnimating()
    }
    
    func activateSmsTimer(with timeOut: Int) {
        
        timerCount = timeOut
        setupTimer(with: timerCount)
    }
    
    //MARK: - Notifications
    
    @objc
    func keyboardWillShow(with notification: Notification) {
        
        guard let info = notification.userInfo,
            let keyboardEndSize = (info[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size
            else { return }
        
        guard !isContentSizeHeightChangedWithKeyboard else { return }
        
        isContentSizeHeightChangedWithKeyboard = true
        
        scrollView.contentSize.height += keyboardEndSize.height
        
        var insets = scrollView.scrollIndicatorInsets
        insets.bottom += keyboardEndSize.height
        scrollView.scrollIndicatorInsets = insets
    }
    
    @objc
    func keyboardWillHide(with notification: Notification) {
        
        guard let info = notification.userInfo,
            let keyboardEndSize = (info[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size
            else { return }
        
        isContentSizeHeightChangedWithKeyboard = false
        
        scrollView.contentSize.height -= keyboardEndSize.height
        
        var insets = scrollView.scrollIndicatorInsets
        insets.bottom -= keyboardEndSize.height
        scrollView.scrollIndicatorInsets = insets
    }
    
    @objc func updateTimer() {
        
        guard timerCount > 0 else {
            
            forgotPasswordDelegate?.updateSmsCodeLiveTime(with: 0)
            
            timer.invalidate()
            resendSmsButton.isEnabled = true
            afterTimeLabel.text = nil
            
            let attributedTitle = NSAttributedString(string: resendButtonAttributedString.string, attributes: underlineStringAttributes)
            
            resendSmsButton.setAttributedTitle(attributedTitle, for: .normal)
            
            return
        }
        
        afterTimeLabel.text = StringsHelper.registerCodeExpirationPrefix + String(timerCount) + StringsHelper.registerCodeExpirationSuffix
        
        timerCount -= timeInterval
        
        forgotPasswordDelegate?.updateSmsCodeLiveTime(with: timerCount)
    }
    
    //MARK: - Custom methods
    
    func setupKeyboardNotifications() {
        
        notificationCenter.addObserver(self,
                                       selector: #selector(self.keyboardWillShow(with:)),
                                       name: NSNotification.Name.UIKeyboardWillShow,
                                       object: nil)
        
        notificationCenter.addObserver(self,
                                       selector: #selector(self.keyboardWillHide(with:)),
                                       name: NSNotification.Name.UIKeyboardWillHide,
                                       object: nil)
    }
    
    func setupTimer(with timeCount: Int) {
        
        resendSmsButton.isEnabled = false
        resendSmsButton.setAttributedTitle(resendButtonAttributedString, for: .normal)
        
        afterTimeLabel.text = StringsHelper.registerCodeExpirationPrefix + String(timerCount) + StringsHelper.registerCodeExpirationSuffix
        
        timerCount = timeCount
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.updateTimer), userInfo: nil, repeats: true)
    }
    
    func setupGestures() {
        passwordStackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(showPasswordTextField)))
        codeStackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(showCodeTextField)))
    }
    
    func animateTextFieldTitleState(with textField: UITextField) {
        
        switch textField {
        case passwordTextField:
            UIView.animate(withDuration: textFieldsLabelAnimationDuration, animations: { [weak self] in
                
                guard let strongSelf = self else { return }
                
                strongSelf.newPasswordLabel.transform = strongSelf.textFieldsLabelAffineTransform
                strongSelf.newPasswordLabel.x = strongSelf.passwordTextField.x
            })
            
        case codeTextField:
            
            UIView.animate(withDuration: textFieldsLabelAnimationDuration, animations: { [weak self] in
                
                guard let strongSelf = self else { return }
                
                strongSelf.checkSmsLabel.transform = strongSelf.textFieldsLabelAffineTransform
                strongSelf.checkSmsLabel.x = strongSelf.codeTextField.x
            })
        default:
            return
        }
    }
    
    func revertTextFieldTitleState(with textField: UITextField) {
        
        switch textField {
        case passwordTextField:
            UIView.animate(withDuration: textFieldsLabelAnimationDuration, animations: { [weak self] in
                
                guard let strongSelf = self else { return }
                
                strongSelf.newPasswordLabel.transform = .identity
                strongSelf.newPasswordLabel.x = strongSelf.passwordTextField.x
            })
        case codeTextField:
            
            UIView.animate(withDuration: textFieldsLabelAnimationDuration, animations: { [weak self] in
                
                guard let strongSelf = self else { return }
                
                strongSelf.checkSmsLabel.transform = .identity
                strongSelf.checkSmsLabel.x = strongSelf.codeTextField.x
            })
        default:
            return
        }
    }
    
    @objc
    func showPasswordTextField() {
        passwordTextField.becomeFirstResponder()
    }
    
    @objc
    func showCodeTextField() {
        codeTextField.becomeFirstResponder()
    }

    func updateFieldState(inFieldWith type: NewPasswordViewFieldType, hasError: Bool) {
        
        let isWarningHidden = !hasError
        let separatorColor = hasError ? UIColorPalette.errorInUserDataColor : UIColorPalette.unselectedTextFieldSeparatorColor
        
        switch type {
        case .code:
            codeWarningImage.isHidden = isWarningHidden
            codeSeparatorView.backgroundColor = separatorColor
        case .password:
            passwordSeparatorView.backgroundColor = separatorColor
        }
    }
    
    @discardableResult
    func validateTextField(_ textField: UITextField, onFormSubmit: Bool) -> Bool {
        
        var fieldType: NewPasswordViewFieldType
        
        switch textField {
        case codeTextField:
            fieldType = .code
        case passwordTextField:
            fieldType = .password
        default:
            return true
        }
        
        let valid = output.validateField(with: (fieldType, textField.text), onFormSubmit: onFormSubmit)
        updateFieldState(inFieldWith: fieldType, hasError: !valid)
        
        return valid
    }
    
    //MARK: - UITextFieldDelegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == passwordTextField {
            passwordSeparatorView.backgroundColor = UIColorPalette.selectedTextFieldSeparatorColor
        }
        else if textField == codeTextField {
            codeWarningImage.isHidden = true
            codeSeparatorView.backgroundColor = UIColorPalette.selectedTextFieldSeparatorColor
        }
        
        animateTextFieldTitleState(with: textField)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        revertTextFieldTitleState(with: textField)
        validateTextField(textField, onFormSubmit: false)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        nextButtonPressed(self)
        return true
    }
    
    //MARK: - NewPasswordViewInput
    
    @IBAction func changePasswordSecurityPressed(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        passwordTextField.isSecureTextEntry = !sender.isSelected
    }
    
    @IBAction func nextButtonPressed(_ sender: Any) {
        
        textFields.forEach{ $0.resignFirstResponder() }
        
        let fields: [UITextField] = [codeTextField, passwordTextField]
        for field in fields {
            guard validateTextField(field, onFormSubmit: true) else { return }
        }
        
        output.updateUserPassword(with: codeTextField.text, newPassword: passwordTextField.text)
    }
    
    @IBAction func didPressSendSmsAgain() {
        output.resendSmsCode(with: userPhone)
    }
}

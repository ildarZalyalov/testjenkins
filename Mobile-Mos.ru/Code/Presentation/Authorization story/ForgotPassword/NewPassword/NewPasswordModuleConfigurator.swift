//
//  NewPasswordModuleConfigurator.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 01.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation
import UIKit

class NewPasswordModuleConfigurator: BaseModuleConfigurator {
    
    override func configureModule(for view: UIViewController) {
        
        let viewController = view as! NewPasswordViewController
        let presenter = NewPasswordPresenter()
        let interactor = NewPasswordInteractor()
        let router = NewPasswordRouter()
        
        let notificationCenter = NotificationCenter.default
        let userSerice = UIApplication.shared.serviceBuilder.getUserService()
        let alertsFactory = NewPasswordAlertFactoryImplementation()
        let commonAlertsFactory = CommonAlertsFactoryImplementation()
        let userDefaults = UserDefaults.standard
        let analyticsManager = AnalyticsManagerBuilder.getAnalyticsManager()
        
        viewController.output = presenter
        viewController.notificationCenter = notificationCenter
        
        presenter.view = viewController
        presenter.interactor = interactor
        presenter.router = router
        
        interactor.output = presenter
        interactor.userSerice = userSerice
        interactor.userDefaults = userDefaults
        interactor.analyticsManager = analyticsManager
        
        router.transitionHandler = viewController
        router.alertsFactory = alertsFactory
        router.commonAlertsFactory = commonAlertsFactory
    }
}

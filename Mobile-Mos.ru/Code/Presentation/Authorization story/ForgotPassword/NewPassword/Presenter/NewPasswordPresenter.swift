//
//  NewPasswordPresenter.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 01.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

class NewPasswordPresenter: NewPasswordViewOutput, NewPasswordInteractorOutput, NewPasswordAlertFactoryActionEventHandler {
    
    weak var view: NewPasswordViewInput!
    var router: NewPasswordRouterInput!
    var interactor: NewPasswordInteractorInput!
    
    let minCountOfPassword = 5
    var maxCodeRequestTime = 5
    
    //MARK: - NewPasswordViewOutput
    
    func setupInitialState() {
        interactor.sendEvent(with: .screenLoaded(withString: AnalyticsConstants.screenLoadedEventName), errorText: nil)
    }
    
    func resendSmsCode(with userPhone: String) {
        
        interactor.sendEvent(with: .repeatAgain(withString: AnalyticsConstants.repeatAgainEventName), errorText: nil)
        
        guard interactor.hasConnectionToServer else {
            router.showErrorAlert(with: StringsHelper.noConnectionErrorText)
            return
        }
        
        guard interactor.codeRequestTimes <= maxCodeRequestTime else {
            showWarningAlertIfNeeded()
            return
        }
        
        interactor.getUserRecoverySmsCode(with: userPhone)
    }
    
    func updateUserPassword(with code: String?, newPassword: String?) {
        
        guard code != nil && newPassword != nil else { return }
        
        guard interactor.hasConnectionToServer else {
            router.showErrorAlert(with: StringsHelper.noConnectionErrorText)
            return
        }
        
        view.showActivityIndicator()
        interactor.setNewPassword(with: code!, password: newPassword!)
    }
    
    func showWarningAlertIfNeeded() {
        router.showCodeRequestWarningAlert(with: interactor.codeRequestTimes)
    }
    
    //MARK: - NewPasswordInteractorOutput
    
    func validateField(with value: NewPasswordViewFieldValue, onFormSubmit: Bool) -> Bool {
        
        switch value.type {
        case .code:
            guard let code = value.text, !code.isEmpty else {
                if onFormSubmit {
                    router.showCodeIsEmptyErrorModule()
                }
                return false
            }
        case .password:
            guard let password = value.text, !password.isEmpty else {
                if onFormSubmit {
                    router.showPasswordIsEmptyErrorModule()
                }
                return false
            }
            guard password.count >= minCountOfPassword else {
                if onFormSubmit {
                    router.showPasswordTooSmallErrorModule()
                }
                return false
            }
        }
        return true
    }
    
    func didFinishChangeUserPassword(with result: UserNewPasswordResult) {
        
        view.hideActivityIndicator()
        
        switch result {
        case .success():
            router.showChangePasswordSuccessAlert(with: self)
            interactor.sendEvent(with: .donePressed(withString: AnalyticsConstants.doneEventName), errorText: nil)
        case .failure(let error):
            router.showErrorAlert(with: error.localizedDescription.firstLetterCapitalized())
            interactor.sendEvent(with: .donePressed(withString: AnalyticsConstants.doneEventName), errorText: error.localizedDescription)
        }
    }
    
    func didFinishObtainSmsCode(with result: UserObtainSmsCodeResult) {
        switch result {
        case .success(let timeOut):
            view.activateSmsTimer(with: timeOut)
        case .failure(let error):
            router.showErrorAlert(with: error.localizedDescription.firstLetterCapitalized())
        }
    }
    
    //MARK: - NewPasswordAlertFactoryActionEventHandler
    
    func didSelectOk() {
        router.closeForgotPasswordModules()
    }
}

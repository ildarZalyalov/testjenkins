//
//  ForgotPasswordDelegate.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 21.05.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

protocol ForgotPasswordDelegate: class {
    
    /// Обновить время жизни смс кода
    ///
    /// - Parameter time: время жизни кода
    func updateSmsCodeLiveTime(with time: Int)
}

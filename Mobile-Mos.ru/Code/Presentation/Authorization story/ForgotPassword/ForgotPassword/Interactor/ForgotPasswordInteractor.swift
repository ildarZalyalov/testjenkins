//
//  ForgotPasswordInteractor.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 01.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

class ForgotPasswordInteractor: ForgotPasswordInteractorInput {
    
    weak var output: ForgotPasswordInteractorOutput!
    var userSerice: UserService!
    var userDefaults: UserDefaults!
    var analyticsManager: AnalyticsManager!
    
    var userPhone: String!
    
    var codeRequestTimes: Int {
        get {
            return userDefaults.integer(forKey: ApplicationUserDefaultsKey.forgotPasswordCodeRequestTimes.rawValue)
        }
        set {
            userDefaults.set(newValue, forKey: ApplicationUserDefaultsKey.forgotPasswordCodeRequestTimes.rawValue)
            userDefaults.set(Date(), forKey: ApplicationUserDefaultsKey.forgotPasswordCacheDate.rawValue)
            userDefaults.synchronize()
        }
    }
    
    var smsCodeLiveTime: Int = 0
    
    //MARK: - ForgotPasswordInteractorInput
    
    var hasConnectionToServer: Bool {
        return userSerice.hasConnectionToServer
    }
    
    func getUserRecoverySmsCode(with phone: String?) {
        
        if phone != nil {
             userPhone = phone
        }
        
        userSerice.obtainUserRecoveryCode(with: userPhone) { [weak self] (result) in
            
            self?.output.didFinishObtainSmsCode(with: result)
            
            switch result {
            case .success:
                self?.codeRequestTimes += 1
            default:
                return
            }
        }
    }
    
    func sendEvent(with eventType: AuthorizationForgotPasswordEvents, errorText: String?) {
        analyticsManager.sendForgotPasswordEvent(with: eventType, errorText: errorText)
    }
}


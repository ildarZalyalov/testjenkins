//
//  ForgotPasswordInteractorInput.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 01.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

protocol ForgotPasswordInteractorInput: class {
    
    /// Обновляемое время жизни смс кода
    var smsCodeLiveTime: Int { get set }

    var hasConnectionToServer: Bool { get }
    
    /// Получить код для восстановления пароля
    ///
    /// - Parameter userPhone: телефон пользователя
    func getUserRecoverySmsCode(with userPhone: String?)
    
    /// Отправить событие связанное с авторизацией
    ///
    /// - Parameters:
    ///   - eventType: тип события
    ///   - errorText: текст ошибки, если есть
    func sendEvent(with eventType: AuthorizationForgotPasswordEvents, errorText: String?)
}

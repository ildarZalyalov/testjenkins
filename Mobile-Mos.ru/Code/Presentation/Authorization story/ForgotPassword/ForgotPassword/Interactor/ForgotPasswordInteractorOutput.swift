//
//  ForgotPasswordInteractorOutput.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 01.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

protocol ForgotPasswordInteractorOutput: class {
    func didFinishObtainSmsCode(with result: UserObtainSmsCodeResult)
}

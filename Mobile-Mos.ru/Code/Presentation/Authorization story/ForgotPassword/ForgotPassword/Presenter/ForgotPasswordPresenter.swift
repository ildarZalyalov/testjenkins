//
//  ForgotPasswordPresenter.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 01.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

class ForgotPasswordPresenter: ForgotPasswordViewOutput, ForgotPasswordInteractorOutput, ForgotPasswordHandler, ForgotPasswordDelegate, ForgotPasswordAlertFactoryActionEventHandler {
    
    weak var view: ForgotPasswordViewInput!
    var router: ForgotPasswordRouterInput!
    var interactor: ForgotPasswordInteractorInput!
    
    let phoneNumberCount = 11
    var currentUserPhone: String = ""
    
    //MARK: - ForgotPasswordViewOutput
    
    func setupInitialState() {
        interactor.sendEvent(with: .screenLoaded(withString: AnalyticsConstants.screenLoadedEventName), errorText: nil)
    }
    
    func validate(phone: String?, onFormSubmit: Bool) -> Bool {
        
        guard phone?.count == phoneNumberCount else {
            if onFormSubmit {
                router.showPhoneIncorrectAlert()
            }
            return false
        }
        
        return true
    }
    
    func showNextModule(with userPhone: String?) {
        
        guard let phone = userPhone else { return }
        
        guard interactor.hasConnectionToServer else {
            router.showErrorAlert(with: StringsHelper.noConnectionErrorText)
            return
        }
        
        currentUserPhone = phone
        
        view.showActivityIndicator()
        interactor.getUserRecoverySmsCode(with: phone)
    }
    
    func closeModule() {
        router.closeCurrentModule()
    }
    
    //MARK: - ForgotPasswordInteractorOutput
    
    func didFinishObtainSmsCode(with result: UserObtainSmsCodeResult) {
        
        view.hideActivityIndicator()
        
        switch result {
        case .success(let timeout):
            interactor.sendEvent(with: .continuePressed(withString: AnalyticsConstants.continueEventName), errorText: nil)
            router.showNewPasswordModule(with: currentUserPhone, smsTimeOut: timeout, delegate: self)
        case .failure(let error as NSError):
            
            interactor.sendEvent(with: .continuePressed(withString: AnalyticsConstants.continueEventName), errorText: error.localizedDescription)
            
            if error.code == UserPasswordRecoverySmsCodeErrors.limitForSmsCode.code {
                router.showNewPasswordModule(with: currentUserPhone, smsTimeOut: 0, delegate: self)
            }
            else if error.code == UserPasswordRecoverySmsCodeErrors.recoveryPasswordTimeNotCompleted.code {
                router.showSmsCodeNotExpieredAlert(with: error.localizedDescription.firstLetterCapitalized(), handler:self)
            }else {
                router.showErrorAlert(with: error.localizedDescription.firstLetterCapitalized())
            }
        }
    }
    
    //MARK: - ForgotPasswordHandler
    
    func obtainNewSmsCode() {
        interactor.getUserRecoverySmsCode(with: nil)
    }
    
    //MARK: - ForgotPasswordDelegate
    
    func updateSmsCodeLiveTime(with time: Int) {
        interactor.smsCodeLiveTime = time
    }
    
    //MARK: - ForgotPasswordAlertFactoryActionEventHandler
    
    func didSelectOk() {
        
        router.showNewPasswordModule(with: currentUserPhone, smsTimeOut: interactor.smsCodeLiveTime, delegate: self)
    }
}

//
//  ForgotPasswordViewOutput.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 01.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

protocol ForgotPasswordViewOutput: class {
    
    func setupInitialState()
    
    func validate(phone: String?, onFormSubmit: Bool) -> Bool
    
    func showNextModule(with userPhone: String?)
    
    func closeModule()
}

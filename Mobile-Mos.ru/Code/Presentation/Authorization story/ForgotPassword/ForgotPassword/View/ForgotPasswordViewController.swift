//
//  ForgotPasswordViewController.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 01.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import UIKit

class ForgotPasswordViewController: BaseViewController, ForgotPasswordViewInput, NavigationBarCustomizingController, UITextFieldDelegate {
    
    
    var output: ForgotPasswordViewOutput!
    var phoneTextFieldFormatManager: TextFieldFormatManager!
    var notificationCenter: NotificationCenter!
    
    let textFieldsLabelAffineTransform = CGAffineTransform(scaleX: 0.8, y: 0.8)
    let textFieldsLabelAnimationDuration = 0.6
    let phonePrefix = "+7 "
    
    var sendButtonText = ""
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var phoneTitleLabel: UILabel!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var phoneSeparatorView: UIView!
    @IBOutlet weak var phoneStackView: UIStackView!
    @IBOutlet weak var numberPlaceholder: UILabel!
    @IBOutlet weak var phoneWarningImage: UIImageView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var sendButton: UIButton!
    
    //MARK: - NavigationBarCustomizingController
    
    var navigationBarState: NavigationBarState? {
        
        var state = NavigationBarState()
        state.isHidden = true
        state.backgroundState = NavigationBarBackgroundState.regular
        state.tintColor = UIColorPalette.registrationNavigationBarTintColor
        
        return state
    }
    
    //MARK: - Методы
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        output.setupInitialState()
        setupGestures()
        setupKeyboardNotifications()
        
        phoneTextFieldFormatManager.configure(with: phoneTextField, mask: nil)
    }
    
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    @IBAction func nextButtonPressed(_ sender: Any) {
        
        guard validatePhoneField(onFormSubmit: true) else { return }
        guard let finalPhoneText = obtainPhoneNumberWithCorrectFormat(from: phoneTextField.text) else { return }
        
        output.showNextModule(with: finalPhoneText)
    }
    
    @IBAction func cancelButtonPressed(_ sender: Any) {
        phoneTextField.resignFirstResponder()
        output.closeModule()
    }
    
    //MARK: - UITextFieldDelegate
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == phoneTextField {
            return phoneTextFieldFormatManager.textField!(textField, shouldChangeCharactersIn: range, replacementString: string)
        }
        
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == phoneTextField {
            
            phoneWarningImage.isHidden = true
            numberPlaceholder.isHidden = false
            
            phoneSeparatorView.backgroundColor = UIColorPalette.selectedTextFieldSeparatorColor
        }
        
        animateTextFieldTitleState(with: textField)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        revertTextFieldTitleState(with: textField)
        validatePhoneField(onFormSubmit: false)
    }
    
    //MARK: - Custom methods
    
    func setupGestures() {
        
        scrollView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(userDidTapOnView)))
        phoneStackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(showPhoneTextField)))
    }
    
    func animateTextFieldTitleState(with textField: UITextField) {
        
        switch textField {
        case phoneTextField:
            UIView.animate(withDuration: textFieldsLabelAnimationDuration, animations: { [weak self] in
                
                guard let strongSelf = self else { return }
                
                strongSelf.phoneTitleLabel.transform = strongSelf.textFieldsLabelAffineTransform
                strongSelf.phoneTitleLabel.x = strongSelf.numberPlaceholder.x
            })
        default:
            return
        }
    }
    
    func revertTextFieldTitleState(with textField: UITextField) {
        
        switch textField {
        case phoneTextField:
            UIView.animate(withDuration: textFieldsLabelAnimationDuration, animations: { [weak self] in
                
                guard let strongSelf = self else { return }
                
                strongSelf.phoneTitleLabel.transform = .identity
                strongSelf.phoneTitleLabel.x = strongSelf.numberPlaceholder.x
            })
        default:
            return
        }
    }
    
    func setupKeyboardNotifications() {
        
        guard UIDevice.current.hasCompactScreen else { return }
        
        scrollView.isScrollEnabled = true
        
        notificationCenter.addObserver(self,
                                       selector: #selector(self.keyboardWillShow(with:)),
                                       name: NSNotification.Name.UIKeyboardWillShow,
                                       object: nil)
        
        notificationCenter.addObserver(self,
                                       selector: #selector(self.keyboardWillHide(with:)),
                                       name: NSNotification.Name.UIKeyboardWillHide,
                                       object: nil)
    }
    
    func obtainPhoneNumberWithCorrectFormat(from phoneText: String?) -> String? {
        
        guard let phone = phoneText else { return nil }
        
        let phoneTextWithPrefix = phonePrefix + phone
        return phoneTextWithPrefix.trimAll(exeptFor: CharacterSet.decimalDigits.inverted)
    }
    
    @discardableResult
    func validatePhoneField(onFormSubmit: Bool) -> Bool {
        
        let phone = obtainPhoneNumberWithCorrectFormat(from: phoneTextField.text)
        let valid = output.validate(phone: phone, onFormSubmit: onFormSubmit)
        let hideWarning = valid
        let separatorColor = valid ? UIColorPalette.unselectedTextFieldSeparatorColor : UIColorPalette.errorInUserDataColor
        
        phoneWarningImage.isHidden = hideWarning
        phoneSeparatorView.backgroundColor = separatorColor
        
        return valid
    }
    
    //MARK: - Observed methods
    
    @objc
    open func userDidTapOnView() {
        self.view.endEditing(true)
    }
    
    @objc
    func showPhoneTextField() {
        phoneTextField.becomeFirstResponder()
    }
    
    @objc
    func keyboardWillShow(with notification: Notification) {
        
        guard let info = notification.userInfo,
            let keyboardEndSize = (info[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size
            else { return }
        
        let insets = UIEdgeInsets(top: 0, left: 0, bottom: keyboardEndSize.height, right: 0)
        scrollView.contentInset = insets
        scrollView.scrollIndicatorInsets = insets
    }
    
    @objc
    func keyboardWillHide(with notification: Notification) {
        
        scrollView.contentInset = UIEdgeInsets.zero
        scrollView.scrollIndicatorInsets = UIEdgeInsets.zero
    }
    
    //MARK: - ForgotPasswordViewInput
    
    func hideActivityIndicator() {
        activityIndicator.stopAnimating()
        sendButton.setTitle(sendButtonText, for: .normal)
        phoneTextField.isEnabled = true
    }
    
    func showActivityIndicator() {
        
        guard let buttonText = sendButton.titleLabel?.text else { return }
        
        sendButtonText = buttonText
        
        sendButton.setTitle("", for: .normal)
        
        phoneTextField.isEnabled = false
        activityIndicator.startAnimating()
    }
}

//
//  ForgotPasswordModuleConfigurator.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 01.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation
import UIKit

class ForgotPasswordModuleConfigurator: BaseModuleConfigurator {
    
    override func configureModule(for view: UIViewController) {
        
        let viewController = view as! ForgotPasswordViewController
        let presenter = ForgotPasswordPresenter()
        let interactor = ForgotPasswordInteractor()
        let router = ForgotPasswordRouter()
        
        let phoneFormatManager = TextFieldFormatManagerImplementation()
        let notificationCenter = NotificationCenter.default
        let userSerice = UIApplication.shared.serviceBuilder.getUserService()
        let commonAlertsFactory = CommonAlertsFactoryImplementation()
        let forgotPasswordAlertsFactory = ForgotPasswordAlertFactoryImplementation()
        let userDefaults = UserDefaults.standard
        let analyticsManager = AnalyticsManagerBuilder.getAnalyticsManager()
        
        viewController.output = presenter
        viewController.phoneTextFieldFormatManager = phoneFormatManager
        viewController.notificationCenter = notificationCenter
        
        presenter.view = viewController
        presenter.interactor = interactor
        presenter.router = router
        
        interactor.output = presenter
        interactor.userSerice = userSerice
        interactor.userDefaults = userDefaults
        interactor.analyticsManager = analyticsManager
        
        router.transitionHandler = viewController
        router.commonAlertsFactory = commonAlertsFactory
        router.forgotPasswordAlertFactory = forgotPasswordAlertsFactory
        router.dataTransitionHandler = viewController
    }
}

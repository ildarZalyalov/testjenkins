//
//  ForgotPasswordRouter.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 01.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

class ForgotPasswordRouter: ForgotPasswordRouterInput {
    
    weak var transitionHandler: UIViewController!
    weak var dataTransitionHandler: DataTransferModuleController!
    
    var commonAlertsFactory: CommonAlertsFactory!
    var forgotPasswordAlertFactory: ForgotPasswordAlertFactory!
    
    let newPasswordSegueIdentifier = "newPassword"
    let unwindToAuthorizationSegueIdentifier = "unwindToAuthorization"
    
    //MARK: - ForgotPasswordRouterInput
    
    func showNewPasswordModule(with userPhone: String, smsTimeOut: Int, delegate: ForgotPasswordDelegate?) {
        dataTransitionHandler.performSegue(with: newPasswordSegueIdentifier, sender: nil) { (configurableModule) in
            let data: NewPasswordDataInput = (phone: userPhone, timeOut: smsTimeOut, delegate: delegate)
            configurableModule.configureModule(with: data)
        }
    }
    
    func showPhoneIncorrectAlert() {
        let alert = commonAlertsFactory.getErrorAlert(with: StringsHelper.incorrectPhoneError)
        transitionHandler.present(alert, animated: true, completion: nil)
    }
    
    func showErrorAlert(with message: String) {
        let alert = commonAlertsFactory.getErrorAlert(with: message)
        transitionHandler.present(alert, animated: true, completion: nil)
    }
    
    func showSmsCodeNotExpieredAlert(with message: String, handler: ForgotPasswordAlertFactoryActionEventHandler?) {
        let alert = forgotPasswordAlertFactory.getSmsCodeNotExpiredAlert(with: message, eventHandler: handler)
        transitionHandler.present(alert, animated: true, completion: nil)
    }
    
    func closeCurrentModule() {
        transitionHandler.performSegue(withIdentifier: unwindToAuthorizationSegueIdentifier, sender: nil)
    }
}

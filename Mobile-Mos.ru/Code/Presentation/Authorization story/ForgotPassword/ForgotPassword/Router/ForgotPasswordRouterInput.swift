//
//  ForgotPasswordRouterInput.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 01.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

protocol ForgotPasswordRouterInput: class {
   
    func showNewPasswordModule(with userPhone: String, smsTimeOut: Int, delegate: ForgotPasswordDelegate?)
    
    func showSmsCodeNotExpieredAlert(with message: String, handler: ForgotPasswordAlertFactoryActionEventHandler?)
    
    func showPhoneIncorrectAlert()
    
    func showErrorAlert(with message: String)
    
    func closeCurrentModule()
}

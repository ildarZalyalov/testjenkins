//
//  ForgotPasswordFactoryImplementation.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 21.05.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

class ForgotPasswordAlertFactoryImplementation: ForgotPasswordAlertFactory {
    
    func getSmsCodeNotExpiredAlert(with message: String, eventHandler: ForgotPasswordAlertFactoryActionEventHandler?) -> UIViewController {
        
        let alert = UIAlertController(title: "Ошибка 😕", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (alert: UIAlertAction!) in
            eventHandler?.didSelectOk()
        }))
        
        return alert
    }
}

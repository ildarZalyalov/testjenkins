//
//  ForgotPasswordFactory.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 21.05.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation


/// Обработчик нажатий
protocol ForgotPasswordAlertFactoryActionEventHandler {
    
    func didSelectOk()
}

protocol ForgotPasswordAlertFactory {
    
    /// Получить алерт для отображения ошибки лимита
    ///
    /// - Parameter eventHandler: хендлер фабрики алерта
    /// - Returns: контроллер алерта
    func getSmsCodeNotExpiredAlert(with message: String, eventHandler: ForgotPasswordAlertFactoryActionEventHandler?) -> UIViewController
}

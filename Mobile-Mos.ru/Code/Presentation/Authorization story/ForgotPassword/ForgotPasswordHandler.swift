//
//  ForgotPasswordHandler.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 02.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Хэндлер для обработки нажатий
protocol ForgotPasswordHandler: class {
    
    /// запросить новый смс код
    func obtainNewSmsCode()
}

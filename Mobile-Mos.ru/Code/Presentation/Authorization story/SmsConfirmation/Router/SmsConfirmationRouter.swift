//
//  SmsConfirmationRouter.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 25.12.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class SmsConfirmationRouter: SmsConfirmationRouterInput {
    
    weak var transitionHandler: UIViewController!
    var commonAlertsFactory: CommonAlertsFactory!
    
    //MARK: - SmsConfirmationRouterInput
    
    func showNoConnectionErrorModule() {
        let alert = commonAlertsFactory.getErrorAlert(with: StringsHelper.noConnectionErrorText)
        transitionHandler.present(alert, animated: true, completion: nil)
    }
    
    func showErrorAlert(with message: String) {
        let alert = commonAlertsFactory.getErrorAlert(with: message)
        transitionHandler.present(alert, animated: true, completion: nil)
    }
}

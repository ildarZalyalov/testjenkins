//
//  SmsConfirmationViewController.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 25.12.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import UIKit

class SmsConfirmationViewController: BaseViewController, SmsConfirmationViewInput, UITextFieldDelegate, ConfigurableModuleController, NavigationBarCustomizingController {
    
    var output: SmsConfirmationViewOutput!
    var phoneTextFieldFormatManager: TextFieldFormatManager!
    var notificationCenter: NotificationCenter!
    
    let textFieldsLabelAffineTransform = CGAffineTransform(scaleX: 0.8, y: 0.8)
    let textFieldsLabelAnimationDuration = 0.6
    let codeMaxCount = 5
    var timerCount: Int = 0
    let timeInterval = 1
    var timer: Timer = Timer()
    var phoneText: String = ""
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var codeTextField: UITextField!
    @IBOutlet weak var afterTimeLabel: UILabel!
    @IBOutlet weak var checkCodeLabel: UILabel!
    @IBOutlet weak var codeSeparatorView: UIView!
    @IBOutlet weak var resendCodeButton: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    //MARK: - NavigationBarCustomizingController
    
    var navigationBarState: NavigationBarState? {
        
        var state = NavigationBarState()
        
        state.isHidden = false
        state.isTranslucent = false
        state.backgroundState = NavigationBarBackgroundState.regular
        state.tintColor = UIColor.black
        state.isShadowHidden = true
        
        return state
    }
    
    //MARK: - Методы
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        output.setupInitialState()
        setupKeyboardNotifications()
        
        phoneTextFieldFormatManager.configure(with: phoneTextField, mask: .phoneWithoutBrackets)
        
        navigationController?.setNavigationBarHidden(false, animated: false)
        codeTextField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        
        phoneTextField.text = String(phoneText.dropFirst())
        setupTimer(with: timerCount)
    }
    
    deinit {
        notificationCenter.removeObserver(self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        codeTextField.becomeFirstResponder()
    }
    
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    @IBAction func pressedResendCode() {
        output.didPressResendCode()
    }
    
    @objc func updateTimer() {
        
        guard timerCount > 0 else {
            
            timer.invalidate()
            resendCodeButton.isEnabled = true
            afterTimeLabel.text = nil
            
            return
        }
        
        afterTimeLabel.text = StringsHelper.registerCodeExpirationPrefix + String(timerCount) + StringsHelper.registerCodeExpirationSuffix
        timerCount -= timeInterval
        
        output.codeLifeTimeDidChange(with: timerCount)
    }
    
    //MARK: - ConfigurableModuleController
    
    func configureModule(with object: Any) {
        
        if let typle = object as? (UserRegisterRequest, Int, RegistrationHandler) {
            
            phoneText = typle.0.phone
            timerCount = typle.1
            output.configureRegistrationHandler(with: typle.2, confirmModel: typle.0)
        }
    }
    
    //MARK: - SmsConfirmationViewInput
    
    func showActivityIndicator() {
        codeTextField.isEnabled = false
        activityIndicator.startAnimating()
    }
    
    func hideActivityIndicator() {
        codeTextField.isEnabled = true
        activityIndicator.stopAnimating()
    }
    
    func displayCodeCountdown(with timeout: Int) {
        
        resendCodeButton.isEnabled = false
        
        timerCount = timeout
        setupTimer(with: timerCount)
    }
    
    //MARK: - UITextFieldDelegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == codeTextField {
            codeSeparatorView.backgroundColor = UIColorPalette.selectedTextFieldSeparatorColor
        }
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField == codeTextField {
            codeSeparatorView.backgroundColor = UIColorPalette.unselectedTextFieldSeparatorColor
        }
    }
    
    @objc
    func textFieldDidChange() {
        
        guard let codeText = codeTextField.text else {
            return
        }
        
        if codeText.count >= codeMaxCount {
            output.didFinishInputConfirmationCode(with: codeText)
        }
    }
    
    //MARK: - Notifications
    
    @objc
    func keyboardWillShow(with notification: Notification) {
        
        guard let info = notification.userInfo,
            let keyboardEndSize = (info[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size
            else { return }
        
        let insets = UIEdgeInsets(top: 0, left: 0, bottom: keyboardEndSize.height, right: 0)
        scrollView.contentInset = insets
        scrollView.scrollIndicatorInsets = insets
    }
    
    @objc
    func keyboardWillHide(with notification: Notification) {
        
        scrollView.contentInset = UIEdgeInsets.zero
        scrollView.scrollIndicatorInsets = UIEdgeInsets.zero
    }
    
    //MARK: - Custom methods
    
    func setupKeyboardNotifications() {
        
        guard UIDevice.current.hasCompactScreen else { return }
        
        scrollView.isScrollEnabled = true
        
        notificationCenter.addObserver(self,
                                       selector: #selector(self.keyboardWillShow(with:)),
                                       name: NSNotification.Name.UIKeyboardWillShow,
                                       object: nil)
        
        notificationCenter.addObserver(self,
                                       selector: #selector(self.keyboardWillHide(with:)),
                                       name: NSNotification.Name.UIKeyboardWillHide,
                                       object: nil)
    }
    
    func setupTimer(with timeCount: Int) {
        
        afterTimeLabel.text = StringsHelper.registerCodeExpirationPrefix + String(timerCount) + StringsHelper.registerCodeExpirationSuffix
        
        timerCount = timeCount
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.updateTimer), userInfo: nil, repeats: true)
    }
}

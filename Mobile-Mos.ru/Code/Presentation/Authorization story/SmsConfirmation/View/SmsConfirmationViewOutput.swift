//
//  SmsConfirmationViewOutput.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 25.12.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol SmsConfirmationViewOutput: class {
    
    func setupInitialState()
    
    func configureRegistrationHandler(with object: RegistrationHandler?, confirmModel: UserRegisterRequest)
    
    func didPressResendCode()
    
    func didFinishInputConfirmationCode(with code: String)
    
    func codeLifeTimeDidChange(with time: Int)
}

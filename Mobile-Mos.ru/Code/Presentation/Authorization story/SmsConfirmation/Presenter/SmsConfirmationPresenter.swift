//
//  SmsConfirmationPresenter.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 25.12.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class SmsConfirmationPresenter: SmsConfirmationViewOutput, SmsConfirmationInteractorOutput {
    
    weak var view: SmsConfirmationViewInput!
    weak var registrationHandler: RegistrationHandler?
    
    var router: SmsConfirmationRouterInput!
    var interactor: SmsConfirmationInteractorInput!
    var userRequestModel: UserRegisterRequest!
    
    //MARK: - SmsConfirmationViewOutput
    
    func configureRegistrationHandler(with object: RegistrationHandler?, confirmModel: UserRegisterRequest) {
        
        registrationHandler = object
        userRequestModel = confirmModel
    }
    
    func setupInitialState() {
        interactor.sendEvent(with: .screenLoaded(withString: AnalyticsConstants.screenLoadedEventName),errorText: nil)
    }
    
    func didPressResendCode() {
        
        interactor.sendEvent(with: .sendSmsAgain(withString: AnalyticsConstants.repeatAgainEventName), errorText: nil)
        
        guard interactor.hasConnectionToServer else {
            router.showNoConnectionErrorModule()
            return
        }
        
        interactor.getUserSmsCode(with: userRequestModel)
    }
    
    func didFinishInputConfirmationCode(with code: String) {
        
        guard let codeDigits = Int(code) else { return }
        
        view.showActivityIndicator()
        interactor.confirmUserCode(with: codeDigits,
                                   userRegistrationModel: userRequestModel,
                                   beforeSuccess: { [weak self] in
            self?.registrationHandler?.willFinishCodeConfirmation()
        })
    }
    
    func codeLifeTimeDidChange(with time: Int) {
        registrationHandler?.updateSmsCodeLiveTime(with: time)
    }
    
    //MARK: - SmsConfirmationInteractorOutput
    
    func didFinishObtainUserSmsCode(with result: UserObtainSmsCodeResult) {
        
        switch result {
            
        case .success(let timeOutCount):
            view.displayCodeCountdown(with: timeOutCount)
        case .failure(let error):
            router.showErrorAlert(with: error.localizedDescription.firstLetterCapitalized())
        }
    }
    
    func didFinishSmsCodeConfirmation(with result: UserRegistrationResult) {
        
        view.hideActivityIndicator()
        
        switch result {
        case .success:
            interactor.sendEvent(with: .donePressed(withString: AnalyticsConstants.doneEventName), errorText: nil)
            registrationHandler?.didFinishCodeConfirmation()
        case .failure(let error):
            router.showErrorAlert(with: error.localizedDescription.firstLetterCapitalized())
            interactor.sendEvent(with: .donePressed(withString: AnalyticsConstants.doneEventName), errorText: error.localizedDescription)
        }
    }

}

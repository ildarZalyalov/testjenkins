//
//  SmsConfirmationInteractor.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 25.12.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class SmsConfirmationInteractor: SmsConfirmationInteractorInput {
    
    weak var output: SmsConfirmationInteractorOutput!
    var userService: UserService!
    var userInfoService: UserInfoAndSettingsService!
    var analyticsManager: AnalyticsManager!
    
    //MARK: - SmsConfirmationInteractorInput
    
    var hasConnectionToServer: Bool {
        return userService.hasConnectionToServer
    }
    
    func getUserSmsCode(with userRegistrationModel: UserRegisterRequest) {
        userService.obtainUserRegisterCode(with: userRegistrationModel.phone) { [weak self] result in
            self?.output.didFinishObtainUserSmsCode(with: result)
        }
    }
    
    func confirmUserCode(with code: Int, userRegistrationModel: UserRegisterRequest, beforeSuccess: @escaping () -> ()) {
        
        userService.registerNewUser(with: userRegistrationModel.phone,
                                    password: userRegistrationModel.password,
                                    code: code,
                                    beforeSuccess: { _ in
                                        beforeSuccess()
                                    },
                                    completion: { [weak self] result in
                                        self?.userInfoService.obtainCurrentUserInfo()
                                        self?.userInfoService.refreshNotificationSettings(completion: nil)
                                        self?.output.didFinishSmsCodeConfirmation(with: result)
                                    })
    }
    
    func sendEvent(with eventType: AuthorizationAddSmsCodeEvents, errorText: String?) {
        analyticsManager.sendAddSmsCodeEvent(with: eventType, errorText: errorText)
    }
}

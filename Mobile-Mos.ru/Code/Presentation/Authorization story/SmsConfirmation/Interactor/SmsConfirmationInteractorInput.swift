//
//  SmsConfirmationInteractorInput.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 25.12.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol SmsConfirmationInteractorInput: class {
    
    var hasConnectionToServer: Bool { get }
    
    func getUserSmsCode(with userRegistrationModel: UserRegisterRequest)
    
    func confirmUserCode(with code: Int, userRegistrationModel: UserRegisterRequest, beforeSuccess: @escaping () -> ())
    
    func sendEvent(with eventType: AuthorizationAddSmsCodeEvents, errorText: String?)
}

//
//  SmsConfirmationModuleConfigurator.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 25.12.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import UIKit

class SmsConfirmationModuleConfigurator: BaseModuleConfigurator {
    
    override func configureModule(for view: UIViewController) {
        
        let viewController = view as! SmsConfirmationViewController
        let presenter = SmsConfirmationPresenter()
        let interactor = SmsConfirmationInteractor()
        let router = SmsConfirmationRouter()
        let userService = UIApplication.shared.serviceBuilder.getUserService()
        let userInfoService = UIApplication.shared.serviceBuilder.getUserInfoAndSettingsService()
        let textFieldFormatManager = TextFieldFormatManagerImplementation()
        let commonAlertsFactory = CommonAlertsFactoryImplementation()
        let notificationCenter = NotificationCenter.default
        let analyticsManager = AnalyticsManagerBuilder.getAnalyticsManager()
        
        viewController.output = presenter
        viewController.phoneTextFieldFormatManager = textFieldFormatManager
        viewController.notificationCenter = notificationCenter
        
        presenter.view = viewController
        presenter.interactor = interactor
        presenter.router = router
        
        interactor.output = presenter
        interactor.userService = userService
        interactor.userInfoService = userInfoService
        interactor.analyticsManager = analyticsManager
        
        router.transitionHandler = viewController
        router.commonAlertsFactory = commonAlertsFactory
    }
}

//
//  RegistrationInteractorInput.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 25.12.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol RegistrationInteractorInput: class {
    
    /// Обновляемое время жизни смс кода
    var smsCodeLiveTime: Int { get set }
    
    var hasConnectionToServer: Bool { get }
    
    var userRegistrationModel: UserRegisterRequest! { get set }
    
    func getUserSmsCode(with userRegistrationModel: UserRegisterRequest)
    
    func sendEvent(with eventType: AuthorizationSignUpEvents, errorText: String?)
}

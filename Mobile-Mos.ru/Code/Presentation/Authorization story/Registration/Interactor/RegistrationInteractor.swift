//
//  RegistrationInteractor.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 25.12.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class RegistrationInteractor: RegistrationInteractorInput {
    
    weak var output: RegistrationInteractorOutput!
    var userSerice: UserService!
    var userRegistrationModel: UserRegisterRequest!
    var analyticsManager: AnalyticsManager!
    
    var smsCodeLiveTime: Int = 0
    
    //MARK: - RegistrationInteractorInput
    
    var hasConnectionToServer: Bool {
        return userSerice.hasConnectionToServer
    }
    
    func getUserSmsCode(with userRegistrationModel: UserRegisterRequest) {
        
        self.userRegistrationModel = userRegistrationModel
        
        userSerice.obtainUserRegisterCode(with: userRegistrationModel.phone) {[weak self] result in
            self?.output.didFinishObtainUserSmsCode(with: result)
        }
    }
    
    func sendEvent(with eventType: AuthorizationSignUpEvents, errorText: String?) {
        analyticsManager.sendSignUpEvent(with: eventType, errorText: errorText)
    }
}

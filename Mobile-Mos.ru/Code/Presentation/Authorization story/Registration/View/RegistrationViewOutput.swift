//
//  RegistrationViewOutput.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 25.12.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

enum RegistrationViewFieldType {
    case phone
    case password
}

typealias RegistrationViewFieldValue = (type: RegistrationViewFieldType, text: String?)

protocol RegistrationViewOutput: class {
    
    func setupInitialState()
    
    func configureAuthorizationHandler(with object: RegistrationProcessHandler?)
    
    func validateField(with value: RegistrationViewFieldValue, onFormSubmit: Bool) -> Bool
    
    func didPressContinue(with userRegistrationModel: UserRegisterRequest)
    
    func didPressCancel()
    
    func didPressAuthorization()
    
    func didPressAgreements()
}

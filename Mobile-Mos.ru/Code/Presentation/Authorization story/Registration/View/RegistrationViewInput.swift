//
//  RegistrationViewInput.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 25.12.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol RegistrationViewInput: class {
    func hideActivityIndicator()
}

//
//  RegistrationViewController.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 25.12.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import UIKit

class RegistrationViewController: BaseViewController, RegistrationViewInput, UITextFieldDelegate, ConfigurableModuleController, NavigationBarCustomizingController {
    
    var output: RegistrationViewOutput!
    var notificationCenter: NotificationCenter!
    var phoneTextFieldFormatManager: TextFieldFormatManager!
    
    let textFieldsLabelAffineTransform = CGAffineTransform(scaleX: 0.8, y: 0.8)
    let minCorrectPhoneFieldCharCount = 15
    let minCorrectPasswordCharCount = 6
    let phonePrefix = "+7 "
    
    let textFieldsLabelAnimationDuration = 0.6
    let stopCharacterRemovingCount = 2
    let loginButtonToFooterConstraintConfinedConstant: CGFloat = 60
    
    var isContentSizeHeightChangedWithKeyboard = false
    var nextButtonText = ""
    
    @IBOutlet var textFields: [UITextField]!
    
    @IBOutlet weak var phoneStackView: UIStackView!
    @IBOutlet weak var passwordStackView: UIStackView!
    @IBOutlet weak var loginStackView: UIStackView!
    
    @IBOutlet weak var phoneSeparatorView: UIView!
    @IBOutlet weak var passwordSeparatorView: UIView!
    @IBOutlet weak var phoneWarningImage: UIImageView!
    
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var phoneNumberLabel: UILabel!
    @IBOutlet weak var passwordLabel: UILabel!
    @IBOutlet weak var phonePrefixLabel: UILabel!
    
    @IBOutlet weak var loginButtonToFooterConstraint: NSLayoutConstraint!
    @IBOutlet var constraintsToCollapseOnSmallScreen: [NSLayoutConstraint]!
    
    //MARK: - Методы
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        output.setupInitialState()
        setupKeyboardNotifications()
        
        phoneTextFieldFormatManager.configure(with: phoneTextField, mask: nil)
        
        setupGestures()
        
        if UIDevice.current.hasConfinedScreen {
            loginButtonToFooterConstraint.constant = loginButtonToFooterConstraintConfinedConstant
            view.setNeedsLayout()
        }
        
        if UIDevice.current.hasCompactScreen {
            
            for constraint in constraintsToCollapseOnSmallScreen {
                constraint.constant /= 2
            }
            
            view.setNeedsLayout()
        }
    }
    
    deinit {
        notificationCenter.removeObserver(self)
    }
    
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    //MARK: - ConfigurableModuleController
    
    func configureModule(with object: Any) {
        if let object = object as? RegistrationProcessHandler {
            output.configureAuthorizationHandler(with: object)
        }
    }
    
    //MARK: - NavigationBarCustomizingController
    
    var navigationBarState: NavigationBarState? {
    
        var state = NavigationBarState()
        state.isHidden = true
        state.backgroundState = NavigationBarBackgroundState.regular
        state.tintColor = UIColorPalette.registrationNavigationBarTintColor
        
        return state
    }
    
    //MARK: - UITextFieldDelegate
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == phoneTextField {
            return phoneTextFieldFormatManager.textField!(textField, shouldChangeCharactersIn: range, replacementString: string)
        }
        
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == phoneTextField {
            
            phonePrefixLabel.isHidden = false
            phoneWarningImage.isHidden = true
            phoneSeparatorView.backgroundColor = UIColorPalette.selectedTextFieldSeparatorColor
        }
        else {
            passwordSeparatorView.backgroundColor = UIColorPalette.selectedTextFieldSeparatorColor
        }
        
        animateTextFieldTitleState(with: textField)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField == phoneTextField, let text = textField.text, text.count == phonePrefix.count {
             phonePrefixLabel.isHidden = true
        }
        
        revertTextFieldTitleState(with: textField)
        validateTextField(textField, onFormSubmit: false)
    }
    
    //MARK: - Custom methods
    
    func setupKeyboardNotifications() {
        
        notificationCenter.addObserver(self,
                                       selector: #selector(self.keyboardWillShow(with:)),
                                       name: NSNotification.Name.UIKeyboardWillShow,
                                       object: nil)
        
        notificationCenter.addObserver(self,
                                       selector: #selector(self.keyboardWillHide(with:)),
                                       name: NSNotification.Name.UIKeyboardWillHide,
                                       object: nil)
    }
    
    func animateTextFieldTitleState(with textField: UITextField) {
        
        switch textField {
        case phoneTextField:
            UIView.animate(withDuration: textFieldsLabelAnimationDuration, animations: { [weak self] in
                
                guard let strongSelf = self else { return }
                
                strongSelf.phoneNumberLabel.transform = strongSelf.textFieldsLabelAffineTransform
                strongSelf.phoneNumberLabel.x = strongSelf.phonePrefixLabel.x
            })
        case passwordTextField:
            
            UIView.animate(withDuration: textFieldsLabelAnimationDuration, animations: { [weak self] in
                
                guard let strongSelf = self else { return }
                
                strongSelf.passwordLabel.transform = strongSelf.textFieldsLabelAffineTransform
                strongSelf.passwordLabel.x = textField.x
            })

        default:
            return
        }
    }
    
    func revertTextFieldTitleState(with textField: UITextField) {
        
        
        switch textField {
        case phoneTextField:
            UIView.animate(withDuration: textFieldsLabelAnimationDuration, animations: { [weak self] in
                
                guard let strongSelf = self else { return }
                
                self?.phoneNumberLabel.transform = .identity
                self?.phoneNumberLabel.x = strongSelf.phonePrefixLabel.x
            })
        case passwordTextField:
            
            UIView.animate(withDuration: textFieldsLabelAnimationDuration, animations: { [weak self] in
                
                self?.passwordLabel.transform = .identity
                self?.passwordLabel.x = textField.x
            })
            
        default:
            return
        }
    }
    
    func setupGestures() {
        
        scrollView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(userDidTapOnView)))
        phoneStackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(showPhoneTextField)))
        passwordStackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(showPasswordTextField)))
        loginStackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(RegistrationViewController.authorizationButtonPressed)))
    }
    
    func obtainPhoneNumberWithCorrectFormat(from phoneText: String?) -> String? {
        
        guard let phoneString = phoneText else { return nil }
        
        let phoneTextWithPrefix = phonePrefix + phoneString
        return phoneTextWithPrefix.trimAll(exeptFor: CharacterSet.decimalDigits.inverted)
    }
    
    func updateFieldState(inFieldWith type: RegistrationViewFieldType, hasError: Bool) {
        
        let isWarningHidden = !hasError
        let separatorColor = hasError ? UIColorPalette.errorInUserDataColor : UIColorPalette.unselectedTextFieldSeparatorColor
        
        switch type {
        case .phone:
            phoneWarningImage.isHidden = isWarningHidden
            phoneSeparatorView.backgroundColor = separatorColor
        case .password:
            passwordSeparatorView.backgroundColor = separatorColor
        }
    }
    
    @discardableResult
    func validateTextField(_ textField: UITextField, onFormSubmit: Bool) -> Bool {
        
        var fieldType: RegistrationViewFieldType
        var text: String?
        
        switch textField {
        case phoneTextField:
            fieldType = .phone
            text = obtainPhoneNumberWithCorrectFormat(from: textField.text)
        case passwordTextField:
            fieldType = .password
            text = textField.text
        default:
            return true
        }
        
        let valid = output.validateField(with: (fieldType, text), onFormSubmit: onFormSubmit)
        updateFieldState(inFieldWith: fieldType, hasError: !valid)
        
        return valid
    }
    
    //MARK: - Observed methods
    
    @objc
    open func userDidTapOnView() {
        self.view.endEditing(true)
    }
    
    @objc
    func showPhoneTextField() {
        phoneTextField.becomeFirstResponder()
    }
    
    @objc
    func showPasswordTextField() {
        passwordTextField.becomeFirstResponder()
    }
    
    @objc func keyboardWillShow(with notification: Notification) {
        
        guard let info = notification.userInfo,
            let keyboardEndSize = (info[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size
            else { return }
        
        guard !isContentSizeHeightChangedWithKeyboard else { return }
        
        isContentSizeHeightChangedWithKeyboard = true
        
        scrollView.contentSize.height += keyboardEndSize.height
        
        var insets = scrollView.scrollIndicatorInsets
        insets.bottom += keyboardEndSize.height
        scrollView.scrollIndicatorInsets = insets
    }
    
    @objc func keyboardWillHide(with notification: Notification) {
        
        guard let info = notification.userInfo,
            let keyboardEndSize = (info[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size
            else { return }
        
        isContentSizeHeightChangedWithKeyboard = false
        
        scrollView.contentSize.height -= keyboardEndSize.height
        
        var insets = scrollView.scrollIndicatorInsets
        insets.bottom -= keyboardEndSize.height
        scrollView.scrollIndicatorInsets = insets
    }
    
    //MARK: - RegistrationViewInput
    
    func hideActivityIndicator() {
        
        activityIndicator.stopAnimating()
        nextButton.setTitle(nextButtonText, for: .normal)
        
        textFields.forEach{ $0.isEnabled = true }
    }
    
    func showActivityIndicator() {
        
        guard let buttonText = nextButton.titleLabel?.text else { return }
        
        nextButtonText = buttonText
        
        nextButton.setTitle("", for: .normal)
        
        textFields.forEach{ $0.isEnabled = false }
        
        activityIndicator.startAnimating()
    }
    
    //MARK: - Buttons action
    
    @IBAction func continueButtonPressed(_ sender: Any) {
        
        let fields: [UITextField] = [phoneTextField, passwordTextField]
        for field in fields {
            guard validateTextField(field, onFormSubmit: true) else { return }
        }
        
        guard let phone = obtainPhoneNumberWithCorrectFormat(from: phoneTextField.text) else { return }
        guard let password = passwordTextField.text else { return }
        
        let registrationModel = UserRegisterRequest(sessionId: String(), phone: phone, password: password, code: 0)
        
        showActivityIndicator()
        output.didPressContinue(with: registrationModel)
    }
    
    @IBAction func authorizationButtonPressed(_ sender: Any) {
        output.didPressAuthorization()
    }
    
    @IBAction func agreementsPressed(_ sender: Any) {
        output.didPressAgreements()
    }
    
    @IBAction func closeButtonPressed(_ sender: Any) {
        userDidTapOnView()
        output.didPressCancel()
    }
    
    @IBAction func changePasswordSecurityPressed(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        passwordTextField.isSecureTextEntry = !sender.isSelected
    }
}

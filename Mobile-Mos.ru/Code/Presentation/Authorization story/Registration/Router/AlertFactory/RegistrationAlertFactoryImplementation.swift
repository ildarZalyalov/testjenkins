//
//  RegistrationAlertFactoryImplementation.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 31.05.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

class RegistrationAlertFactoryImplementation: RegistrationAlertFactory {
    
    func getSmsCodeNotExpiredAlert(with message: String, eventHandler: RegistrationAlertFactoryActionEventHandler?) -> UIViewController {
        
        let alert = UIAlertController(title: "Ошибка 😕", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (alert: UIAlertAction!) in
            eventHandler?.didSelectOk()
        }))
        
        return alert
    }
}

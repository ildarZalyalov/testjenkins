//
//  RegistrationRouter.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 25.12.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class RegistrationRouter: RegistrationRouterInput {
    
    weak var transitionHandler: UIViewController!
    weak var dataTransitionHandler: DataTransferModuleController!
    var commonAlertsFactory: CommonAlertsFactory!
    var registrationAlertFactory: RegistrationAlertFactory!
    
    let showAgreementsSegueIdentifier = "showAgreements"
    let smsConfirmationSegueIdentifier = "smsConfirmation"
    let unwindToAuthorizationSegueIdentifier = "unwindToAuthorizationSegue"
    let unwindToConversationsSegueIdentifier = "unwindToConversationsSegue"
    
    //MARK: - RegistrationRouterInput
    
    func closeCurrentModule() {
        transitionHandler.performSegue(withIdentifier: unwindToConversationsSegueIdentifier, sender: nil)
    }
    
    func showAuthorizationModule() {
        transitionHandler.performSegue(withIdentifier: unwindToAuthorizationSegueIdentifier, sender: nil)
    }
    
    func showAgreementsModule() {
        transitionHandler.performSegue(withIdentifier: showAgreementsSegueIdentifier, sender: nil)
    }
    
    func showSmsConfirmationModule(with userRegistrationModel: UserRegisterRequest, codeTimeCount: Int, and handler: RegistrationHandler?) {
        dataTransitionHandler.performSegue(with: smsConfirmationSegueIdentifier, sender: nil) { configurableController in
            configurableController.configureModule(with: (userRegistrationModel, codeTimeCount, handler))
        }
    }
    
    func showErrorAlert(with message: String) {
        let alert = commonAlertsFactory.getErrorAlert(with: message)
        transitionHandler.present(alert, animated: true, completion: nil)
    }
    
    func showSmsCodeNotExpieredAlert(with message: String, handler: RegistrationAlertFactoryActionEventHandler?) {
        
        let alert = registrationAlertFactory.getSmsCodeNotExpiredAlert(with: message, eventHandler: handler)
        transitionHandler.present(alert, animated: true, completion: nil)
    }
    
    func showNoConnectionErrorModule() {
        let alert = commonAlertsFactory.getErrorAlert(with: StringsHelper.noConnectionErrorText)
        transitionHandler.present(alert, animated: true, completion: nil)
    }
    
    func showPhoneIsEmptyErrorModule() {
        
        let alert = commonAlertsFactory.getErrorAlert(with: StringsHelper.fillPhoneField)
        transitionHandler.present(alert, animated: true, completion: nil)
    }
    
    func showPhoneNumberNotValidErrorModule() {
        let alert =  commonAlertsFactory.getErrorAlert(with: StringsHelper.minCountOfPhoneNumber)
        transitionHandler.present(alert, animated: true, completion: nil)
    }
    
    func showPasswordIsEmptyErrorModule() {
        let alert =  commonAlertsFactory.getErrorAlert(with: StringsHelper.fillPasswordField)
        transitionHandler.present(alert, animated: true, completion: nil)
    }
    
    func showPasswordTooSmallErrorModule() {
        let alert =  commonAlertsFactory.getErrorAlert(with: StringsHelper.minCountOfPassword)
        transitionHandler.present(alert, animated: true, completion: nil)
    }
    
    func showRepeatPasswordIncorrectErrorModule() {
        let alert = commonAlertsFactory.getErrorAlert(with: StringsHelper.repeatPasswordIncorrect)
        transitionHandler.present(alert, animated: true, completion: nil)
    }
}

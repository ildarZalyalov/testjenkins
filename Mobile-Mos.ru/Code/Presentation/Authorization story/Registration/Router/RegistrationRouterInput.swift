//
//  RegistrationRouterInput.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 25.12.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol RegistrationRouterInput: class {
    
    func closeCurrentModule()
    
    func showAuthorizationModule()
    
    func showAgreementsModule()
    
    func showSmsConfirmationModule(with userRegistrationModel: UserRegisterRequest, codeTimeCount: Int, and handler: RegistrationHandler?)
    
    func showNoConnectionErrorModule()
    
    func showErrorAlert(with message: String)
    
    func showSmsCodeNotExpieredAlert(with message: String, handler: RegistrationAlertFactoryActionEventHandler?)
    
    func showPhoneIsEmptyErrorModule()
    
    func showPhoneNumberNotValidErrorModule()
    
    func showPasswordIsEmptyErrorModule()
    
    func showPasswordTooSmallErrorModule()
    
    func showRepeatPasswordIncorrectErrorModule()
}

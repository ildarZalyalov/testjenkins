//
//  RegistrationModuleConfigurator.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 25.12.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import UIKit

class RegistrationModuleConfigurator: BaseModuleConfigurator {
    
    override func configureModule(for view: UIViewController) {
        
        let viewController = view as! RegistrationViewController
        let presenter = RegistrationPresenter()
        let interactor = RegistrationInteractor()
        let router = RegistrationRouter()
        
        let notificationCenter = NotificationCenter.default
        let textFieldFormatManager = TextFieldFormatManagerImplementation()
        let userSerice = UIApplication.shared.serviceBuilder.getUserService()
        let commonAlertsFactory = CommonAlertsFactoryImplementation()
        let registrationAlertFactory = RegistrationAlertFactoryImplementation()
        let analyticsManager = AnalyticsManagerBuilder.getAnalyticsManager()
        
        viewController.output = presenter
        viewController.notificationCenter = notificationCenter
        viewController.phoneTextFieldFormatManager = textFieldFormatManager
        
        presenter.view = viewController
        presenter.interactor = interactor
        presenter.router = router
        
        interactor.output = presenter
        interactor.userSerice = userSerice
        interactor.analyticsManager = analyticsManager
        
        router.transitionHandler = viewController
        router.commonAlertsFactory = commonAlertsFactory
        router.dataTransitionHandler = viewController
        router.registrationAlertFactory = registrationAlertFactory
    }
}

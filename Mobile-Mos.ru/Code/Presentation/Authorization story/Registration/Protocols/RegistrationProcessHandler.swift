//
//  AuthorizationHandler.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 25.12.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Обработчик процесса регистрации
protocol RegistrationProcessHandler: class {
    
    /// Обработать отмену регистрации. Если вернется false, то модуль обработает отмену сам путем возврата на главный экран
    ///
    /// - Returns: обработана ли отмена
    func handleCancelRegistration() -> Bool
    
    /// Выполнить подготовку к завершению регистрации
    func handleWillFinishRegistration()
    
    /// Обработать завершение регистрации
    func handleFinishRegistration(with model: UserRegisterRequest)
}

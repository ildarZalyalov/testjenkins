//
//  CodeVerificationDelegate.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 06.12.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol CodeVerificationDelegate: class {
    
    func didFinishObtainCode(with timeout: Int)
    
}

//
//  СonversationsHandlerProtocol.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 06.12.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Обработчик процесса авторизации
protocol AuthorizationProcessHandler: class {
    
    /// Обработать отмену авторизации. Если вернется false, то модуль обработает отмену сам путем возврата на главный экран
    ///
    /// - Returns: обработана ли отмена
    func handleCancelAuthorization() -> Bool
    
    /// Обработать завершение авторизации
    func handleFinishAuthorization()
}

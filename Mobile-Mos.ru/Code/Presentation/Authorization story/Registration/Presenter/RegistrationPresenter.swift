//
//  RegistrationPresenter.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 25.12.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class RegistrationPresenter: RegistrationViewOutput, RegistrationInteractorOutput, RegistrationHandler, RegistrationAlertFactoryActionEventHandler {
    
    weak var view: RegistrationViewInput!
    var router: RegistrationRouterInput!
    var interactor: RegistrationInteractorInput!
    
    weak var registrationProcessHandler: RegistrationProcessHandler?
    
    let minCountOfPassword = 5
    let minCountOfPhoneCharacters = 11
    
    //MARK: -
    
    func closeModule() {
        
        let cancelHandled = registrationProcessHandler?.handleCancelRegistration() ?? false
        guard !cancelHandled else { return }
        
        router.closeCurrentModule()
    }
    
    //MARK: - RegistrationViewOutput
    
    func setupInitialState() {
        interactor.sendEvent(with: .screenLoaded(withString: AnalyticsConstants.screenLoadedEventName), errorText: nil)
    }
    
    func configureAuthorizationHandler(with object: RegistrationProcessHandler?) {
        registrationProcessHandler = object
    }
    
    func validateField(with value: RegistrationViewFieldValue, onFormSubmit: Bool) -> Bool {
        
        switch value.type {
        case .phone:
            guard let phoneNumber = value.text, !phoneNumber.isEmpty else {
                if onFormSubmit {
                    router.showPhoneIsEmptyErrorModule()
                }
                return false
            }
            guard phoneNumber.count == minCountOfPhoneCharacters else {
                if onFormSubmit {
                    router.showPhoneNumberNotValidErrorModule()
                }
                return false
            }
        case .password:
            guard let password = value.text, !password.isEmpty else {
                if onFormSubmit {
                    router.showPasswordIsEmptyErrorModule()
                }
                return false
            }
            
            guard password.count >= minCountOfPassword else {
                if onFormSubmit {
                    router.showPasswordTooSmallErrorModule()
                }
                return false
            }
        }
        
        return true
    }
    
    func didPressContinue(with userRegistrationModel: UserRegisterRequest) {
        
        guard interactor.hasConnectionToServer else {
            view.hideActivityIndicator()
            router.showNoConnectionErrorModule()
            return
        }
        
        interactor.getUserSmsCode(with: userRegistrationModel)
    }
    
    func didPressCancel() {
        closeModule()
    }
    
    func didPressAuthorization() {
        router.showAuthorizationModule()
        interactor.sendEvent(with: .signInLinkPressed(withString: AnalyticsConstants.linkToLogInEventName), errorText: nil)
    }
    
    func didPressAgreements() {
        router.showAgreementsModule()
        interactor.sendEvent(with: .agreementsPressed(withString: AnalyticsConstants.agreementsEventName), errorText: nil)
    }
    
    //MARK: - RegistrationInteractorOutput
    
    func didFinishObtainUserSmsCode(with result: UserObtainSmsCodeResult) {
        
        view.hideActivityIndicator()
        
        switch result {
        
        case .success(let timeOutCount):
            router.showSmsConfirmationModule(with: interactor.userRegistrationModel, codeTimeCount: timeOutCount, and: self)
            interactor.sendEvent(with: .continuePressed(withString: AnalyticsConstants.continueEventName), errorText: nil)
        case .failure (let error as NSError):
            
            interactor.sendEvent(with: .continuePressed(withString: AnalyticsConstants.continueEventName), errorText: error.localizedDescription)
            
            if error.code == UserPasswordRecoverySmsCodeErrors.recoveryPasswordTimeNotCompleted.code {
                router.showSmsCodeNotExpieredAlert(with: error.localizedDescription.firstLetterCapitalized(), handler:self)
            }
            else {
                router.showErrorAlert(with: error.localizedDescription.firstLetterCapitalized())
            }
        }
    }
    
    //MARK: - RegistrationHandler
    
    func updateSmsCodeLiveTime(with time: Int) {
        interactor.smsCodeLiveTime = time
    }
    
    func willFinishCodeConfirmation() {
        registrationProcessHandler?.handleWillFinishRegistration()
    }
    
    func didFinishCodeConfirmation() {
        registrationProcessHandler?.handleFinishRegistration(with: interactor.userRegistrationModel)
    }
    
    //MARK: - RegistrationAlertFactoryActionEventHandler
    
    func didSelectOk() {
        router.showSmsConfirmationModule(with: interactor.userRegistrationModel, codeTimeCount: interactor.smsCodeLiveTime, and: self)
    }
}

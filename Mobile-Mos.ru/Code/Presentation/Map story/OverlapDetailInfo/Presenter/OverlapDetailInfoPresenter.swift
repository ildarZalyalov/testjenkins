//
//  OverlapDetailInfoPresenter.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 23.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

class OverlapDetailInfoPresenter: OverlapDetailInfoViewOutput, OverlapDetailInfoInteractorOutput, MapActionsDelegate {
    
    weak var view: OverlapDetailInfoViewInput!
    var router: OverlapDetailInfoRouterInput!
    var interactor: OverlapDetailInfoInteractorInput!
    var itemDatasource: OverlapItemDetailDataSource!
    
    //MARK: - OverlapDetailInfoViewOutput
    
    func setupInitialState(with object: OverlapInfoEventDataInput) {
        
        interactor.obtainOverlap(with: object.overlapId)
        object.eventHandler?.setupPulleyViewController(to: router as? PulleyConfigurator)
        object.eventHandler?.setupMapActionsDelegate(with: self)
    }
    
    func didPressClose() {
        router.closeCurrenModule()
    }
    
    func didPressCall() {
        router.makeCall(to: itemDatasource.cellPhone.text)
    }
    
    func didPressShare() {
        router.showShareViewController(with: itemDatasource.shareString)
    }
    
    //MARK: - OverlapDetailInfoInteractorOutput
    
    func didFinishObtainOverlap(with result: MapOverlapObtainResult) {
        switch result {
        case .success(let overlap):
            
            interactor.buildItemDetailsDataSource(withItem: overlap)
            interactor.sendOverlapViewEvent(with: overlap, errorText: AnalyticsConstants.emptyValueName)
            
        case .failure(let error):
            
            view.stopLoadingIndicator(showErrorLable: true)
            interactor.sendOverlapViewEvent(with: Overlap(), errorText: error.localizedDescription)
        }
    }
    
    func didFinishBuildDetailDatasource(with datasource: OverlapItemDetailDataSource) {
        itemDatasource = datasource
        view.configureOverlapDetails(from: datasource)
    }
    
    //MARK: - MapActionsDelegate
    
    func mapCameraDidMove() {
        router.closeCurrenModule()
    }
}

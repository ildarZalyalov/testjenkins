//
//  OverlapDetailInfoInteractorOutput.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 23.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

protocol OverlapDetailInfoInteractorOutput: class {
    
    func didFinishObtainOverlap(with result: MapOverlapObtainResult)
    
    func didFinishBuildDetailDatasource(with datasource: OverlapItemDetailDataSource)
}

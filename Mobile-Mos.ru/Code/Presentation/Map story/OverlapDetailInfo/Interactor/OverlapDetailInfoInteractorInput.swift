//
//  OverlapDetailInfoInteractorInput.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 23.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

protocol OverlapDetailInfoInteractorInput: class {
    
    func obtainOverlap(with overlapId: String)
    
    func buildItemDetailsDataSource(withItem overlap: Overlap)
    
    func sendOverlapViewEvent(with overlap: Overlap, errorText: String)
}

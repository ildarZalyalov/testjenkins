//
//  OverlapDetailInfoInteractor.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 23.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

class OverlapDetailInfoInteractor: OverlapDetailInfoInteractorInput {
    
    weak var output: OverlapDetailInfoInteractorOutput!
    var overlapsService: OverlapsService!
    var itemDetailsBuilder: ItemDetailsBuilder!
    var analyticsManager: AnalyticsManager!
    
    var request: HTTPRequest? {
        willSet{
            request?.cancel()
        }
    }
    
    //MARK: - OverlapDetailInfoInteractorInput
    
    func obtainOverlap(with overlapId: String) {
        
      request = overlapsService.obtainOverlap(with: overlapId) { [weak self] (result) in
            self?.output.didFinishObtainOverlap(with: result)
        }
    }
    
    func buildItemDetailsDataSource(withItem overlap: Overlap) {
        
        let datasource = itemDetailsBuilder.createItemDetails(from: overlap)
        output.didFinishBuildDetailDatasource(with: datasource)
    }
    
    func sendOverlapViewEvent(with overlap: Overlap, errorText: String) {
        
        var overlapObjectInfoEventData: OverlapObjectInfoEventData = (AnalyticsConstants.emptyValueName, AnalyticsConstants.emptyValueName, AnalyticsConstants.emptyValueName, errorText)
        
        if !overlap.address.isEmpty {
            overlapObjectInfoEventData.address = overlap.address
        }
        
        if !overlap.okrugName.isEmpty {
            overlapObjectInfoEventData.okrug = overlap.okrugName
        }
        
        analyticsManager.sendMapConversationEvent(with: .overlapMapObjectInfo(withString: AnalyticsConstants.viewOverlapObjectEventName), aboutAppData: nil, appSharingSource: nil, datasetData: nil, mapObjectSharingData: nil, overlapData: overlapObjectInfoEventData)
    }
}

//
//  OverlapDetailInfoRouter.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 23.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation
import Pulley

class OverlapDetailInfoRouter: OverlapDetailInfoRouterInput, PulleyConfigurator {
    
    weak var transitionHandler: UIViewController!
    
    fileprivate weak var pulley: PulleyViewController!
    
    //MARK: - OverlapDetailInfoRouterInput
    
    func setUpPulleyViewController(with object: Any?) {
        
        guard let controller = object as? PulleyViewController else { return }
        pulley = controller
    }
    
    func closeCurrenModule() {
        pulley.setDrawerPosition(position: .collapsed, animated: true)
    }
    
    func showShareViewController(with shareString: String) {
        
        let activityController = UIActivityViewController(activityItems: [shareString], applicationActivities: nil)
        transitionHandler.present(activityController, animated: true, completion: nil)
    }
    
    func makeCall(to numberString: String) {
        
        let numberToCall = numberString.components(separatedBy: CharacterSet.decimalDigits.inverted).joined(separator: "")
        
        UIApplication.shared.attempt(calling: numberToCall)
    }
}

//
//  OverlapDetailInfoRouterInput.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 23.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

protocol OverlapDetailInfoRouterInput: class {
    
    func closeCurrenModule()
    
    func makeCall(to numberString: String)
    
    func showShareViewController(with shareString: String)
}

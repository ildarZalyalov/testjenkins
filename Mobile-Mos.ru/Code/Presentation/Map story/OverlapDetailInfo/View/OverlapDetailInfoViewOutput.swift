//
//  OverlapDetailInfoViewOutput.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 23.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

protocol OverlapDetailInfoViewOutput: class {
    
    func setupInitialState(with object: OverlapInfoEventDataInput)
    
    func didPressClose()
    
    func didPressCall()
    
    func didPressShare()
}

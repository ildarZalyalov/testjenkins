//
//  OverlapDetailInfoViewInput.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 23.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

protocol OverlapDetailInfoViewInput: class {
    
    func configureOverlapDetails(from datasource: OverlapItemDetailDataSource)
    
    func showLoadingIndicator()
    
    func stopLoadingIndicator(showErrorLable: Bool)
}

//
//  OverlapDetailInfoViewController.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 23.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import UIKit
import Pulley

class OverlapDetailInfoViewController: BaseViewController,ConfigurableModuleController, PulleyDrawerViewControllerDelegate, UIScrollViewDelegate, OverlapDetailInfoViewInput {
    
    /// Первоначальная высота открытия карточки
    let beginPartialRevealDrawerHeight: CGFloat = 262
    
    /// Диапазон допустимых значений альфы навигейшен бара
    let navigationBarAlphaRange: ClosedRange<CGFloat> = 0 ... 1
    let pulleyTopConstant: CGFloat = 0
    
    var bottomInset: CGFloat {
        return self.beginPartialRevealDrawerHeight
    }
    
    @IBOutlet weak var overlapDateTitleLabel: UILabel!
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var headerVisualEffectView: UIVisualEffectView!
    @IBOutlet weak var dateFromDateToLabel: UILabel!
    @IBOutlet weak var detailInfoContainer: UIStackView!
    @IBOutlet weak var roadLinesStackView: UIStackView!
    @IBOutlet weak var linesLabel: UILabel!
    @IBOutlet weak var nameStackView: UIStackView!
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var phoneStackView: UIStackView!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var requesterStackView: UIStackView!
    @IBOutlet weak var requesterLabel: UILabel!
    @IBOutlet weak var customerStackView: UIStackView!
    @IBOutlet weak var customerLabel: UILabel!
    @IBOutlet weak var overlapTypeStackView: UIStackView!
    @IBOutlet weak var overlapTypeLabel: UILabel!
    @IBOutlet weak var addressStackView: UIStackView!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var okrugStackView: UIStackView!
    @IBOutlet weak var okrugLabel: UILabel!
    @IBOutlet weak var lastUpdateDateLabel: UILabel!
    
    @IBOutlet weak var shareStackView: UIStackView!
    @IBOutlet weak var loaderStackView: UIStackView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var errorLabel: UILabel!
    
    @IBOutlet var allLabels: [UILabel]!
    
    var output: OverlapDetailInfoViewOutput!
    
   
    //MARK: - Методы
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupGestureRecognizers()
        showLoadingIndicator()
    }
    
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override func didMove(toParentViewController parent: UIViewController?) {
        
        super.didMove(toParentViewController: parent)
        
        guard let pulley = parent as? PulleyViewController else { return }
        pulley.topInset = pulleyTopConstant
    }
    
    //MARK: - ConfigurableModuleController
    
    func configureModule(with object: Any) {
        if let object = object as? OverlapInfoEventDataInput {
            output.setupInitialState(with: object)
        }
    }
    
    //MARK: - OverlapDetailInfoViewInput
    
    
    func configureOverlapDetails(from datasource: OverlapItemDetailDataSource) {
        
        stopLoadingIndicator(showErrorLable: false)
        
        let dateToDateFrom = datasource.dateToDateFrom.text
        let roadLines = datasource.roadLines.text
        let name = datasource.name.text
        let address = datasource.address.text
        let phone = datasource.cellPhone.text
        let customer = datasource.customer.text
        let requester = datasource.requester.text
        let okrug = datasource.okrug.text
        let overlapType = datasource.overlapType.text
        let lastUpdateDate = datasource.lastUpdateDate.text
        
        dateFromDateToLabel.text = dateToDateFrom
        linesLabel.text          = roadLines
        nameLabel.text           = name
        phoneLabel.text          = phone
        customerLabel.text       = customer
        requesterLabel.text      = requester
        okrugLabel.text          = okrug
        overlapTypeLabel.text    = overlapType
        addressLabel.text        = address
        lastUpdateDateLabel.text = lastUpdateDate
        
        roadLinesStackView.isHidden   = datasource.roadLines.isHidden
        nameStackView.isHidden        = datasource.name.isHidden
        phoneStackView.isHidden       = datasource.cellPhone.isHidden
        customerStackView.isHidden    = datasource.customer.isHidden
        requesterStackView.isHidden   = datasource.requester.isHidden
        okrugStackView.isHidden       = datasource.okrug.isHidden
        overlapTypeStackView.isHidden = datasource.overlapType.isHidden
        addressStackView.isHidden     = datasource.address.isHidden
        
        view.layoutIfNeeded()
    }
    
    func showLoadingIndicator() {
        
        detailInfoContainer.isHidden = true
        allLabels.forEach{ $0.isHidden = true }

        loaderStackView.isHidden = false
        activityIndicator.isHidden = false
        activityIndicator.startAnimating()
    }
    
    func stopLoadingIndicator(showErrorLable: Bool) {
        
        activityIndicator.stopAnimating()
        activityIndicator.isHidden = true
        
        detailInfoContainer.isHidden = showErrorLable
        allLabels.forEach{ $0.isHidden = showErrorLable }
        
        errorLabel.isHidden = !showErrorLable
        loaderStackView.isHidden = !showErrorLable
    }
    
    //MARK: - ScrollViewDelegate
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        headerVisualEffectView.masksToBounds = !(scrollView.contentOffset.y > 0)
    }
    
    //MARK: - PulleyDrawerViewControllerDelegate
    
    func drawerChangedDistanceFromBottom(drawer: PulleyViewController, distance: CGFloat, bottomSafeArea: CGFloat) {
        
        let safeAreaInsets = drawer.backwardCompatibleSafeAreaInsets
        let viewHeight = drawer.view.height - safeAreaInsets.top
        let revealDrawerHeight = beginPartialRevealDrawerHeight + bottomSafeArea
        let totalScroll = viewHeight - (pulleyTopConstant + revealDrawerHeight)
        let offset = distance - revealDrawerHeight
        let percentage = offset / totalScroll
        let alpha = (1 - percentage).clamped(to: navigationBarAlphaRange)
        
        let navigationBar = navigationController?.navigationBar
        
        if let tintColor = navigationBar?.tintColor {
            navigationBar?.tintColor = tintColor.withAlphaComponent(alpha)
        }
        
        if let titleView = navigationBar?.topItem?.titleView {
            titleView.alpha = alpha
        }
        
        navigationController?.stateManager?.currentNavigationBarBackgroundView.alpha = alpha
    }
    
    func drawerPositionDidChange(drawer: PulleyViewController, bottomSafeArea: CGFloat) {
        
        setNeedsStatusBarAppearanceUpdate()
        
        switch drawer.drawerPosition {
        case .open:
            scrollView.isScrollEnabled = true
            navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        case .partiallyRevealed:
            scrollView.isScrollEnabled = false
            navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        case .collapsed:
            scrollView.scrollToTop(animated: false)
            navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        default:
            return
        }
    }
    
    func collapsedDrawerHeight(bottomSafeArea: CGFloat) -> CGFloat {
        return 0
    }
    
    func partialRevealDrawerHeight(bottomSafeArea: CGFloat) -> CGFloat {
        return beginPartialRevealDrawerHeight + bottomSafeArea
    }
    
    func supportedDrawerPositions() -> [PulleyPosition] {
        return [.open, .collapsed, .partiallyRevealed]
    }
    
    //MARK: - Helpers methods
    
    func setupGestureRecognizers() {
        
        let callToNumberTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.phoneButtonPressed(_:)))
        let shareTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.shareButtonPressed(_:)))
        
        phoneStackView.addGestureRecognizer(callToNumberTapGesture)
        shareStackView.addGestureRecognizer(shareTapGesture)
    }
    
    //MARK: - Buttons action
    
    @IBAction func closeButtonPressed(_ sender: Any) {
        output.didPressClose()
    }
    
    @IBAction func shareButtonPressed(_ sender: Any) {
        output.didPressShare()
    }
    
    @IBAction func phoneButtonPressed(_ sender: Any) {
        output.didPressCall()
    }
}

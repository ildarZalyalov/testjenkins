//
//  OverlapDetailInfoModuleConfigurator.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 23.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation
import UIKit

class OverlapDetailInfoModuleConfigurator: BaseModuleConfigurator {
    
    override func configureModule(for view: UIViewController) {
        
        let viewController = view as! OverlapDetailInfoViewController
        let presenter = OverlapDetailInfoPresenter()
        let interactor = OverlapDetailInfoInteractor()
        let router = OverlapDetailInfoRouter()
        
        let overlapsService = UIApplication.shared.serviceBuilder.getOverlapsService()
        let itemDetailBuilder = MapItemDetailsBuilderImplementation()
        let analyticsManager = AnalyticsManagerBuilder.getAnalyticsManager()
        
        viewController.output = presenter
        
        presenter.view = viewController
        presenter.interactor = interactor
        presenter.router = router
        
        interactor.output = presenter
        interactor.overlapsService = overlapsService
        interactor.itemDetailsBuilder = itemDetailBuilder
        interactor.analyticsManager = analyticsManager
        
        router.transitionHandler = viewController
    }
}

//
//  MapItemPanoramaViewController.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 30.05.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import UIKit

class MapItemPanoramaViewController: BaseViewController, MapItemPanoramaViewInput, ConfigurableModuleController {
    
    var output: MapItemPanoramaViewOutput!
    var currentPoint: YMKPoint!
    
    @IBOutlet weak var cancelButton: UIButton!
    
    lazy var panoView: YMKPanoView? = {
        let panoView = YMKPanoView(frame: self.view.frame)
        panoView?.player.enableMove()
        panoView?.player.enableZoom()
        panoView?.player.enableRotation()
        panoView?.player.enableMarkers()
        panoView?.player.enableLoadingWheel()
        
        return panoView
    }()
    
    //MARK: - Методы
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let lPanoView = panoView else { return }
        
        view.addSubview(lPanoView)
        view.bringSubview(toFront: cancelButton)
    }
    
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    //MARK: ConfigurableModuleController
    
    func configureModule(with object: Any) {
        guard let point = object as? YMKPoint else { return }
        currentPoint = point
        output.setupInitialState(with: point)
    }
    
    //MARK: - MapItemPanoramaViewInput
    
    func showPanorama(with panoramaId: String) {
        guard let lPanoView = panoView else { return }
        
        lPanoView.player.lookAt(withPosition: currentPoint)
        lPanoView.player.openPanorama(withPanoramaId: panoramaId)
    }
    
    
    //MARK: Buttons action
    @IBAction func cancelButtonPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}

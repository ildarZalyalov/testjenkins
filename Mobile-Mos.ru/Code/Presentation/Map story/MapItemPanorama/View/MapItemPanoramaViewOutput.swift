//
//  MapItemPanoramaViewOutput.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 30.05.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol MapItemPanoramaViewOutput: class {
    
     func setupInitialState(with point: YMKPoint)
}

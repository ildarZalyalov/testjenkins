//
//  MapItemPanoramaModuleConfigurator.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 30.05.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import UIKit

class MapItemPanoramaModuleConfigurator: BaseModuleConfigurator {
    
    override func configureModule(for view: UIViewController) {
        
        let viewController = view as! MapItemPanoramaViewController
        let presenter = MapItemPanoramaPresenter()
        let interactor = MapItemPanoramaInteractor()
        let router = MapItemPanoramaRouter()
        
        viewController.output = presenter
        
        presenter.view = viewController
        presenter.interactor = interactor
        presenter.router = router
        
        interactor.output = presenter
        interactor.panoramaService = YMKMapKit.sharedInstance().createPanoramaService()
        
        router.transitionHandler = viewController
    }
}

//
//  MapItemPanoramaPresenter.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 30.05.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class MapItemPanoramaPresenter: MapItemPanoramaViewOutput, MapItemPanoramaInteractorOutput {
    
    weak var view: MapItemPanoramaViewInput!
    var router: MapItemPanoramaRouterInput!
    var interactor: MapItemPanoramaInteractorInput!
    
    //MARK: - MapItemPanoramaViewOutput
    
    func setupInitialState(with point: YMKPoint) {
        interactor.findNearestPanorama(with: point)
    }
    
    //MARK: - MapItemPanoramaInteractorOutput
    
    func didFinishObtainPanorama(with panoramaId: String) {
        view.showPanorama(with: panoramaId)
    }
    
}

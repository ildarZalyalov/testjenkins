//
//  MapItemPanoramaInteractor.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 30.05.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class MapItemPanoramaInteractor: MapItemPanoramaInteractorInput {
    
    weak var output: MapItemPanoramaInteractorOutput!
    var session: YMKPanoramaServiceSearchSession?
    
    //MARK: - MapItemPanoramaInteractorInput
    
    var panoramaService: YMKPanoramaService!
    
    func findNearestPanorama(with chosenPoint: YMKPoint) {
        
     session = panoramaService?.findNearest(withPosition: chosenPoint, searchHandler: {[weak self] (panoramaId, error) in
        
            if error == nil, let panoId = panoramaId{
                self?.output.didFinishObtainPanorama(with: panoId)
            }
            else {
                print(error!)
            }
        })
        
    }
}

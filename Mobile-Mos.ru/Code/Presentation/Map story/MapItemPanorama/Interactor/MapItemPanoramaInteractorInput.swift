//
//  MapItemPanoramaInteractorInput.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 30.05.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol MapItemPanoramaInteractorInput: class {
    func findNearestPanorama(with chosenPoint: YMKPoint)
}

//
//  CityMapViewController.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 08.08.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class CityMapViewController: BasePulleyViewController, NavigationBarCustomizingController, ConfigurableModuleController {
    
    /// Идентификатор перехода к информации о чате
    /// Корневой контроллер не является модулем т.к. не обладает функционалом, только настройками UI
    /// Поэтому логика перехода реализована прямо в контроллере
    let aboutChatSegueIdentifier = "showChatInfo"
    
    /// Идентификатор перехода к сложному модулю карт
    let mapViewSegueIdentifier = "mapView"
    
    var cityMapModuleDelegate: CityMapModuleDelegate?
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        
        let superStyle = super.preferredStatusBarStyle
        
        if drawerPosition == .open {
            return drawerContentViewController?.preferredStatusBarStyle ?? superStyle
        }
        else {
            return primaryContentViewController?.preferredStatusBarStyle ?? superStyle
        }
    }
    
    //MARK: - Обработчики нажатий
    
    @objc
    func didPressChatAvatar() {
        performSegue(with: aboutChatSegueIdentifier, sender: nil) { controller in
            controller.configureModule(with: mapConversation)
        }
    }
    
    //MARK: - Методы
    
    func addAvatarBarButtonItem(with avatarSize: CGSize) {
        let button = UIButton(type: .system)
        button.frame = CGRect(x: 0, y: 0, width: avatarSize.width, height: avatarSize.height)
        button.addTarget(self, action: #selector(didPressChatAvatar), for: .touchUpInside)
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: button)
    }
    
    //MARK: - ConfigurableModuleController
    
    func configureModule(with object: Any) {
        cityMapModuleDelegate?.didReceivedConfigurationObject(object)
    }
    
    //MARK: - NavigationBarCustomizingController
    
    var navigationBarState: NavigationBarState? {
        
        let style = mapConversation.style
        
        let navigationBarBackgroundView = ChatNavigationBarView.loadNib()
        navigationBarBackgroundView.shadowColor = style.headerShadowColor
        navigationBarBackgroundView.backgroundColor = style.headerColor
        navigationBarBackgroundView.gradientColor = style.gradientColor
        navigationBarBackgroundView.avatarImage = #imageLiteral(resourceName: "chatAvatarMapsRegular")
        
        addAvatarBarButtonItem(with: navigationBarBackgroundView.avatarSize)
        
        var state = NavigationBarState()
        state.backgroundState = NavigationBarBackgroundState.custom(backgroundView: navigationBarBackgroundView)
        state.tintColor = style.backButtonColor
        state.isShadowHidden = true
        state.isHidden = false
        state.titleTextAttributes = [
            NSAttributedStringKey.font : UIFont(customName: .graphikLCGBold, size: 15) as Any,
            NSAttributedStringKey.foregroundColor : style.titleColor
        ]
        
        return state
    }
}

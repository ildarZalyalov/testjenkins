//
//  CategorizedTagsTrigger.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 15.12.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Протокол отзывающийся на определенные события из модуля категоризованных тегов
protocol CategorizedTagsTrigger: class {
    
    /// кнопка выбора категории нажата
    func didSelectCategoryPressed()
    
    /// экшен скролла к определенной категории закончился
    func didFinishScrollToCategory()
    
    /// тэг выбран
    func didPressTag()
}

//
//  CategorizedTagsModuleInteractor.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 19.07.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//


import Foundation

/// Структура описывающая картинку категории
struct CategoryImage {
    var image: UIImage
    var id: String
}

class CategorizedTagsModuleInteractor: CategorizedTagsModuleInteractorInput {
    
    weak var output: CategorizedTagsModuleInteractorOutput!
    var mapItemsService: MapItemsService!
    var request: HTTPRequest?
    var analyticsManager: AnalyticsManager!
    
    var allCategoriesImages: [UIImage] = [#imageLiteral(resourceName: "fav"),#imageLiteral(resourceName: "history"), #imageLiteral(resourceName: "culture"), #imageLiteral(resourceName: "med"), #imageLiteral(resourceName: "sport"), #imageLiteral(resourceName: "social"), #imageLiteral(resourceName: "car"), #imageLiteral(resourceName: "family"), #imageLiteral(resourceName: "fun"), #imageLiteral(resourceName: "security"), #imageLiteral(resourceName: "education"), #imageLiteral(resourceName: "ecology"), #imageLiteral(resourceName: "city"), #imageLiteral(resourceName: "business")]
    
    var categoriesImagesWithId: [CategoryImage] = [CategoryImage(image: #imageLiteral(resourceName: "fav"), id: "0"),
                                                   CategoryImage(image: #imageLiteral(resourceName: "history"), id: "1"),
                                                   CategoryImage(image: #imageLiteral(resourceName: "culture"), id: "37651"),
                                                   CategoryImage(image: #imageLiteral(resourceName: "med"), id: "34651"),
                                                   CategoryImage(image: #imageLiteral(resourceName: "sport"), id: "36651"),
                                                   CategoryImage(image: #imageLiteral(resourceName: "car"), id: "38651"),
                                                   CategoryImage(image: #imageLiteral(resourceName: "fun"), id: "33651"),
                                                   CategoryImage(image: #imageLiteral(resourceName: "education"), id: "35651"),
                                                   CategoryImage(image: #imageLiteral(resourceName: "social"), id: "42651"),
                                                   CategoryImage(image: #imageLiteral(resourceName: "city"), id: "40651"),
                                                   CategoryImage(image: #imageLiteral(resourceName: "security"), id: "43651"),
                                                   CategoryImage(image: #imageLiteral(resourceName: "business"), id: "41651")]
    
    //MARK: - CategorizedTagsModuleInteractorInput
    
    func obtainCategoryImages(selectedIndex: Int) {
        
        output.didFinishObtainingCategoryImages(images: allCategoriesImages, selectedIndex: selectedIndex)
    }
    
    func obtainCategoriesItems() {
      request =  mapItemsService.obtainMapItemsCategories { [weak self] (result) in
            self?.output.didFinishObtainingCategoryItems(with: result)
        }
    }
    
    func refreshCategoryImages(for items:[MapItemCategory], selectedIndex: Int) {
        var finalCategoryImages = [CategoryImage]()
        
        product(items, categoriesImagesWithId).forEach { (category, categoryImage) in
            if String(category.id) == categoryImage.id {
                finalCategoryImages.append(categoryImage)
            }
        }
        
        allCategoriesImages = finalCategoryImages.map({ $0.image })
        output.didFinishObtainingCategoryImages(images: allCategoriesImages, selectedIndex: selectedIndex)
    }
    
    func save(dataset: MapItemDataset) {
        
        mapItemsService.save(mapItemsDataset: dataset) {[weak self] (result) in
            
            switch result {
                
            case .success:
               self?.output.didFinishSavingHistoryItems(with: true)
            case .error(let error):
                self?.output.didFinishSavingHistoryItems(with: false)
                print("Ошибка при сохранении: \(error)")
            }
        }
    }
    
    func obtainFavoritesItems() {
        
        let favoritedItems = mapItemsService.obtainFavoritedMapItemsWithDataset()
        output.didFinishObtainingFavorites(favorites: favoritedItems)
    }
    
    func obtainHistoryItems() {
        
        let savedDatasets =  mapItemsService.obtainSavedMapItemsDatasets()
        output.didFinishObtainingHistory(historyItems: savedDatasets)
    }
    
    func sendSelectDatasetEvent(with datasetName: String, categoryName: String) {
        
        let datasetEvent: DatasetEventData = (datasetName, categoryName, AnalyticsConstants.emptyValueName, AnalyticsConstants.emptyValueName)
        
        analyticsManager.setMapObjectCategoryName(with: categoryName)
        analyticsManager.sendMapConversationEvent(with: .selectDataset(withString: AnalyticsConstants.selectDatasetEventName), aboutAppData: nil, appSharingSource: nil, datasetData: datasetEvent, mapObjectSharingData: nil, overlapData: nil)
    }
}

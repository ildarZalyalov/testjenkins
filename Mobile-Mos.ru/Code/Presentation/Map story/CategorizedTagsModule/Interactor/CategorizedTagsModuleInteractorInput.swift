//
//  CategorizedTagsModuleInteractorInput.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 19.07.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol CategorizedTagsModuleInteractorInput: class {
    
    /// Получить данные категории
    func obtainCategoryImages(selectedIndex: Int)
    
    /// Обновить картинки категорий от самих категорий
    ///
    /// - Parameter items: массив категорий
    func refreshCategoryImages(for items:[MapItemCategory], selectedIndex: Int)
    
    /// Сохранить датасет
    ///
    /// - Parameter dataset: датасет для сохранения
    func save(dataset: MapItemDataset)
    
    /// Получить категории
    func obtainCategoriesItems()
    
    /// Получить избранные датасеты
    func obtainFavoritesItems()
    
    /// Получить историю
    func obtainHistoryItems()
    
    func sendSelectDatasetEvent(with datasetName: String, categoryName: String)
}

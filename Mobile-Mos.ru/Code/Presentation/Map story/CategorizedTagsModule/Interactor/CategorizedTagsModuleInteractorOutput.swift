//
//  CategorizedTagsModuleInteractorOutput.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 19.07.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol CategorizedTagsModuleInteractorOutput: class {
    
    /// Получение массива картинок категорий
    ///
    /// - Parameter images: массив картинок
    func didFinishObtainingCategoryImages(images: [UIImage], selectedIndex: Int)
   
    /// Получение ответа на сохранение датасета
    ///
    /// - Parameter success: статус сохранения
    func didFinishSavingHistoryItems(with success: Bool)
    
    /// Получение результата запроса на категории
    ///
    /// - Parameter result: результат категории
    func didFinishObtainingCategoryItems(with result: MapItemCategoryObtainMultipleResult)
    
    /// Получение избранных датасетов
    ///
    /// - Parameter favorites: избранные датасеты
    func didFinishObtainingFavorites(favorites: [FavoritedDataset])
    
    /// Получение истории датасетов
    ///
    /// - Parameter historyItems: история датасетов
    func didFinishObtainingHistory(historyItems: [MapItemDataset])
}

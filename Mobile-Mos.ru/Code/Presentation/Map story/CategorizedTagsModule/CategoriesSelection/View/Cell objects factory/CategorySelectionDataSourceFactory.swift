//
//  CategorySelectionDataSourceFactory.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 27.07.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

typealias CategorySelectionDataSourceConfiguration = (CollectionViewDataSource)

protocol CategorySelectionDataSourceFactory {
    
    func buildDataSourceConfiguration(from imagesArray:[UIImage], selectedIndex: Int) -> CategorySelectionDataSourceConfiguration
}

//
//  CategorySelectionDataSourceFactoryImplementation.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 27.07.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class CategorySelectionDataSourceFactoryImplementation: CategorySelectionDataSourceFactory {
    
    fileprivate let favoriteIconHash = #imageLiteral(resourceName: "fav").hash
    fileprivate let historyIconHash = #imageLiteral(resourceName: "history").hash
    
    var cellObject: CellObject!
    
    //MARK: - CategorySelectionDataSourceFactory
    
    func buildDataSourceConfiguration(from imagesArray:[UIImage], selectedIndex: Int) -> CategorySelectionDataSourceConfiguration {
        
        let dataStructure = CollectionViewDataSourceStructure()
        
        for item in 0..<imagesArray.count{
            
            let isSeparatorNeeded = imagesArray[item].hash == favoriteIconHash ||
                                    imagesArray[item].hash == historyIconHash
            
            if isSeparatorNeeded {
               cellObject = CategorySelectionWithSeparatorCellObject(itemId: String(item), categoryImage: imagesArray[item], isFocused: item == selectedIndex)
            }
            else {
               cellObject = CategorySelectionCellObject(itemId: String(item), categoryImage: imagesArray[item], isFocused: item == selectedIndex)
            }
        
            dataStructure.appendCellObject(cellObject)
        }
       
        let dataSource = CollectionViewDataSource(with: dataStructure)
        
        return (dataSource)
    }
}

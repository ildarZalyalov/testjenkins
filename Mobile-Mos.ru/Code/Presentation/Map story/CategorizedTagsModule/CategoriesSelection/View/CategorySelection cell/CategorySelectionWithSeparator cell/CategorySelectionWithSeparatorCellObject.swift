//
//  CategorySelectionWithSeparatorCellObject.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 18.08.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Структура, описывающая объект ячейки с сепаратором
struct CategorySelectionWithSeparatorCellObject: CellObjectWithId {
    
    /// id объекта
    var itemId: String
    
    /// Изображение категории
    var categoryImage: UIImage
    
    /// Выделена ли ячейка в данный момент
    var isFocused: Bool
}

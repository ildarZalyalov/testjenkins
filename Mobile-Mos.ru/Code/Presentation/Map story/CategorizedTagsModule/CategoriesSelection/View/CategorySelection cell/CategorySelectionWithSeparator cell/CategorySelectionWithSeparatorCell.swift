//
//  CategorySelectionWithSeparatorCell.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 18.08.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import UIKit

class CategorySelectionWithSeparatorCell: UICollectionViewCell, ConfigurableView {
    
    @IBOutlet weak var selectorImageView: UIImageView!
    @IBOutlet weak var iconImageView: UIImageView!
    
    let focusedAlpha: CGFloat = 1.0
    let unfocusedAlpha: CGFloat = 0.2
    let selectorImageUnfocusedAlpha: CGFloat = 0.0
    let selectorImageViewTransform =  CGAffineTransform(scaleX: 0.0, y: 0.0)
    
    fileprivate let selectorAnimationDuration = 0.3
    
    //MARK: - ConfigurableView
    
    override func prepareForReuse() {
        super.prepareForReuse()
        iconImageView.alpha = unfocusedAlpha
    }
    
    func configure(with object: Any) {
        
        guard let cellObject = object as? CategorySelectionWithSeparatorCellObject else {
            return
        }
        
        iconImageView.image = cellObject.categoryImage
        iconImageView.alpha = cellObject.isFocused ? focusedAlpha : unfocusedAlpha
        
        selectorImageView.transform = selectorImageViewTransform
        selectorImageView.alpha = 0
        
        UIView.animate(withDuration: selectorAnimationDuration, delay: 0, options: .curveEaseOut, animations: { [weak self] in
            
            guard let strongSelf = self else { return }
            
            strongSelf.selectorImageView.transform = .identity
            strongSelf.selectorImageView.alpha = cellObject.isFocused ? strongSelf.focusedAlpha : strongSelf.selectorImageUnfocusedAlpha
            }, completion: nil)
        
    }
}

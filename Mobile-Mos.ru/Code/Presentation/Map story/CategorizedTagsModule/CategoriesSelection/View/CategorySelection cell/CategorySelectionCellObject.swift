//
//  CategorySelectionCellObject.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 27.07.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

struct CategorySelectionCellObject: CellObjectWithId {
    
    var itemId: String
    var categoryImage: UIImage
    var isFocused: Bool
    
}

//
//  CategorizedTagsModuleConfigurator.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 19.07.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import UIKit

class CategorizedTagsModuleConfigurator: BaseModuleConfigurator {
    
    override func configureModule(for view: UIViewController) {
        
        let viewController = view as! CategorizedTagsModuleViewController
        let presenter = CategorizedTagsModulePresenter()
        let interactor = CategorizedTagsModuleInteractor()
        let router = CategorizedTagsModuleRouter()
        
        let mapItemsService = UIApplication.shared.serviceBuilder.getMapItemsService()
        let tagsDataSourceFactory = TagItemsDataSourceFactoryImplementation()
        let categoriesDataSourceFactory = CategorySelectionDataSourceFactoryImplementation()
        let analyticsManager = AnalyticsManagerBuilder.getAnalyticsManager()
        
        viewController.output = presenter
        
        presenter.view = viewController
        presenter.interactor = interactor
        presenter.router = router
        presenter.tagsDataSourceFactory = tagsDataSourceFactory
        presenter.categoriesDataSourceFactory = categoriesDataSourceFactory
        
        interactor.output = presenter
        interactor.mapItemsService = mapItemsService
        interactor.analyticsManager = analyticsManager
        
        presenter.tagsDataSourceFactory = tagsDataSourceFactory
        presenter.categoriesDataSourceFactory = categoriesDataSourceFactory
        interactor.output = presenter
        
        router.transitionHandler = viewController

    }
}

//
//  TagItemsDataSourceFactory.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 19.07.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

typealias TagItemsDataSourceFactoryConfiguration = (dataSource: CollectionViewDataSource, selectionItems: [String : MapItemDataset])

protocol TagItemsDataSourceFactory {
    
    /// Получить конфигурацию с данными для тегов из категорий
    ///
    /// - Parameter categories: категории
    /// - Returns: конфигурация источника данных
    func buildDataSourceConfiguration(from categories: [MapItemCategory]) -> TagItemsDataSourceFactoryConfiguration
    
    /// Вставить новую секцию с данными в имеющуюся конфигурацию с данными
    ///
    /// - Parameters:
    ///   - objects: объекты новой секции
    ///   - dataStructure: структура с данными
    func insertNewSectionToTop(with newObjects:[MapItemCategory], to dataStructure: inout TagItemsDataSourceFactoryConfiguration)
    
    /// Обновить секцию с новыми данными
    ///
    /// - Parameters:
    ///   - newObjects: новые объекты
    ///   - index: индекс обновляемой секции
    ///   - dataStructure: структура с данными
    func updateSection(with newObjects:[MapItemCategory], at index: Int, of dataStructure: inout TagItemsDataSourceFactoryConfiguration)
}

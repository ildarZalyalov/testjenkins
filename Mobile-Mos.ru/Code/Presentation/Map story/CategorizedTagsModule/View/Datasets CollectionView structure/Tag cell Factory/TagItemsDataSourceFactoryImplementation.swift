//
//  TagItemsDataSourceFactoryImplementation.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 19.07.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class TagItemsDataSourceFactoryImplementation: TagItemsDataSourceFactory {
    
    func buildDataSourceConfiguration(from categories: [MapItemCategory]) -> TagItemsDataSourceFactoryConfiguration {
        
        let dataStructure = CollectionViewDataSourceStructure()
        
        var selectionItems = [String : MapItemDataset]()
        
        for category in categories {
            
            var cellObjectsArray = [TagCellObject]()
            var cellObject: TagCellObject
            let headerObject = SectionHeaderObject(title: category.name)
            
            if !category.datasets.isEmpty {
                
                for dataset in category.datasets {
                    
                    cellObject = TagCellObject(itemId: String(dataset.itemId),
                                                title: dataset.name,
                                                isFavoriteSection: category.name == StringsHelper.favoriteSectionTitle,
                                                favoritesCount: 0)
                    
                    append(cellObject: cellObject,
                           to: &cellObjectsArray,
                           and: dataset,
                           to: &selectionItems)
                }
            }
            else if !category.favoritedDatasets.isEmpty {
                
                for (i,favoritedDatset) in category.favoritedDatasets.enumerated() {
                    
                    cellObject = TagCellObject(itemId: String(favoritedDatset.itemId) + "\(i)",
                                               title: favoritedDatset.name,
                                               isFavoriteSection: category.name == StringsHelper.favoriteSectionTitle,
                                               favoritesCount: favoritedDatset.favoritedItems.count)
                    append(cellObject: cellObject,
                           to: &cellObjectsArray,
                           and: favoritedDatset,
                           to: &selectionItems)
                }
            }
            
            
            dataStructure.appendSection(with: headerObject, and: cellObjectsArray)
        }
        
        
        let dataSource = CollectionViewDataSource(with: dataStructure)
        
        return (dataSource, selectionItems)
    }
    
    func insertNewSectionToTop(with newObjects:[MapItemCategory], to dataStructure: inout TagItemsDataSourceFactoryConfiguration) {
        
        let configurationForObjects = buildDataSourceConfiguration(from: newObjects)
        
        dataStructure.dataSource.insertNewSection(with: configurationForObjects.dataSource.dataStructure, to: .top)
        dataStructure.selectionItems += configurationForObjects.selectionItems
    }
    
    func updateSection(with newObjects:[MapItemCategory], at index: Int, of dataStructure: inout TagItemsDataSourceFactoryConfiguration) {
        
        let configurationForObjects = buildDataSourceConfiguration(from: newObjects)
        
        dataStructure.dataSource.updateData(with: configurationForObjects.dataSource.dataStructure, at: index)
        
        dataStructure.selectionItems += configurationForObjects.selectionItems
    }
    
    fileprivate func append(cellObject: TagCellObject,
                            to cellObjectsArray: inout [TagCellObject],
                            and dataset: MapItemDataset,
                            to selectionItems: inout [String: MapItemDataset]) {
        
        cellObjectsArray.append(cellObject)
        selectionItems[cellObject.itemId] = dataset
    }
}

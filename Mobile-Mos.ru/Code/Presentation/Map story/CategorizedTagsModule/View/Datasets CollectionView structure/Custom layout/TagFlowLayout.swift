//
//  TagFlowLayout.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 20.07.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

//«Lasciate ogni speranza, voi ch'entrate»

import Foundation

@objc
protocol TagLayoutDelegate: class {
    
    /// Вернуть длину тэга по indexPath тега
    ///
    /// - Parameters:
    ///   - collectionView: коллекция отвечающая за тэги
    ///   - indexPath: индекс текущего тэга
    /// - Returns: длина тэга
    func collectionView(_ collectionView: UICollectionView, widthForTagAtIndexPath indexPath: IndexPath) -> CGFloat
    
    /// Коллекция тегов изменила секцию
    ///
    /// - Parameter sectionIndex: индекс секции на которую перешли
    @objc
    optional func collectionViewDidChangeSection(with sectionIndex: Int)
    
    @objc
    /// Позиции хидеров секций относительно contentSize.width коллекции
    ///
    /// - Parameter arrayOfPositions: массив всех позиций хидеров
    optional func collectionViewHeadersPositions(with arrayOfPositions:[CGFloat])
    
    @objc
    /// Получить позицию кнопки поиска, которая находится поверх коллекшена
    ///
    /// - Returns: позиция кнопки относительно collectionView
    optional func getSearchButtonPositionOnTagsCollection() -> CGPoint
}

class TagFlowLayout: UICollectionViewFlowLayout {

    fileprivate var contentWidth: CGFloat = 0
    fileprivate var contentHeight: CGFloat {
        let insets = collectionView!.contentInset
        return collectionView!.bounds.height - (insets.top + insets.bottom)
    }
    
    fileprivate var cache = [UICollectionViewLayoutAttributes]()
    fileprivate var arrayOfHeadersPositions:[CGFloat] = []
    fileprivate var elementsBySections = [Int:[UICollectionViewLayoutAttributes]]()
    fileprivate var farestPositionBySections = [Int:CGFloat]()
    fileprivate var lastDisplayedHeader: UICollectionViewLayoutAttributes!
    fileprivate var lastHeaderPosition: CGFloat = 0
    fileprivate var frameDxPosition: CGFloat = 4.0
    fileprivate var frameDyPosition: CGFloat = 3.0
    fileprivate var sectionHeaderAlpha: CGFloat = 1.0
    fileprivate var spaceBetweenCategories: CGFloat = 30
    fileprivate(set) var heightBetweenEachRowDecrementerDefault: CGFloat = 28
    fileprivate var currentSectionIndex: Int = 0 {
        didSet{
            delegate?.collectionViewDidChangeSection?(with: self.currentSectionIndex)
            stopScrollView()
        }
    }
    
    fileprivate var scrollOffsetCounter: Int = 0
    fileprivate var lastScrollOffsetXPosition: CGFloat = 0
    fileprivate var lastCollectionContentWith: CGFloat = 0
    fileprivate var isStoppingScrollViewActive = true
    fileprivate var isSelectTag = false
    
    weak var delegate: TagLayoutDelegate?
    var numberOfRows = 3
    var tagsCollectionYPosition: CGFloat = 14
    var cellPadding: CGFloat = 5.0
    var cellHeight: CGFloat = 44.0
    var headerHeight: CGFloat = 44
    var heightBetweenEachRowDecrementer: CGFloat = 28
    var sectionLeftMargin: CGFloat = 8
    var isLeftSideSectionMangettingActive = true
    
    override var collectionViewContentSize: CGSize {
        return CGSize(width: contentWidth, height: contentHeight)
    }
    
    override init() {
        super.init()
        sectionHeadersPinToVisibleBounds = true
        scrollDirection = .horizontal
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        sectionHeadersPinToVisibleBounds = true
        scrollDirection = .horizontal
    }
    
    override func prepare() {
        
        if cache.isEmpty {
            
            guard let currentCollectionView = collectionView,
                  let dataSource = currentCollectionView.dataSource,
                  let numberOfSections = dataSource.numberOfSections?(in: currentCollectionView),
                  let tagLayoutDelegate = delegate else {
                    
                    return
            }
            
            // Предварительно подсчитываем расстояние между каждой строкой
            let heightBetweenEachRow = (contentHeight / CGFloat(numberOfRows)) - heightBetweenEachRowDecrementer
            
            // Предварительно подсчитываем Y позицию ячеек
            var yOffset = [CGFloat]()
            for row in 0 ..< numberOfRows {
                yOffset.append(CGFloat(row) * heightBetweenEachRow + headerHeight + tagsCollectionYPosition)
            }
            
            var xOffsets = [CGFloat](repeatElement(sectionLeftMargin, count: numberOfRows))
            var rowWidth = [CGFloat](repeatElement(0, count: numberOfRows))
            
            // Расставляем элементы подсчитывая размеры с паддингами
            for section in 0 ..< numberOfSections {
                
                var row = 0
                var sectionElements = [UICollectionViewLayoutAttributes]()
                
                //Добавляем Заголовок (хидер)
                let nextHeaderAttributes = UICollectionViewLayoutAttributes(forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, with: IndexPath(item: 0, section: section))
                nextHeaderAttributes.frame = CGRect(x: xOffsets[0], y: 0, width: currentCollectionView.bounds.size.width, height: headerHeight)
                
                cache.append(nextHeaderAttributes)
                
                arrayOfHeadersPositions.append(nextHeaderAttributes.frame.origin.x - sectionLeftMargin)
                
                //Добавляем ячейки
                for item in 0 ..< dataSource.collectionView(currentCollectionView, numberOfItemsInSection: section) {
                    
                    let indexPath = IndexPath(item: item, section: section)
                    
                    let tagWidth = cellPadding + tagLayoutDelegate.collectionView(currentCollectionView, widthForTagAtIndexPath: indexPath) + cellPadding
                    
                    let frame = CGRect(x: section > 0 ? xOffsets[row] + sectionLeftMargin : xOffsets[row], y: yOffset[row], width: tagWidth, height: cellHeight)
                    
                    let insetFrame = frame.insetBy(dx:frameDxPosition, dy: frameDyPosition)
                    
                    let attributes = UICollectionViewLayoutAttributes(forCellWith: indexPath)
                    attributes.frame = insetFrame
                    
                    cache.append(attributes)
                    sectionElements.append(attributes)
                    
                    rowWidth[row] = rowWidth[row] + tagWidth
                    xOffsets[row] = xOffsets[row] + tagWidth
                    
                    row = row >= (numberOfRows - 1) ? 0 : 1 + row
                }
                
                //Находим самый крайний элемент в секции (он не обязательно последний)
                var xPos: CGFloat = 0
                var farestXPos: CGFloat = 0
                
                for element in sectionElements {
                    if element.frame.maxX > xPos {
                        xPos = element.frame.maxX
                        farestXPos = element.frame.origin.x
                    }
                }
                
                //Обновляем размер контента от самого крайнего положения тега + отступ
                contentWidth = xPos + sectionLeftMargin
                elementsBySections[section] = sectionElements
                farestPositionBySections[section] = farestXPos
                
                //для последующих секций сдвигаем все с отступом для красивого отображения
                xOffsets = [CGFloat](repeatElement(contentWidth + spaceBetweenCategories, count: numberOfRows))
            }
            
            contentWidth += currentCollectionView.width / 2
            delegate?.collectionViewHeadersPositions?(with: arrayOfHeadersPositions)
            
            if isSelectTag {
                let currentOffset = currentCollectionView.contentOffset.x
                let delta = currentOffset > 0 ? contentWidth - lastCollectionContentWith : currentOffset
                currentCollectionView.setContentOffset(CGPoint(x: currentOffset + delta, y: 0), animated: false)
                lastCollectionContentWith = contentWidth
                isSelectTag = false
            }
        }
    }
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        
        guard let currentCollectionView = collectionView else { return nil }
        
        var newLayoutAttributes = [UICollectionViewLayoutAttributes]()
        
        for attributes in cache {
            if attributes.frame.intersects(rect) {
                
                if attributes.representedElementCategory == .cell {
                    newLayoutAttributes.append(attributes)
                }
                else if attributes.representedElementCategory == .supplementaryView {
                    
                    guard let scopeOfBounds = boundaries(forSection: attributes.indexPath.section) else { continue }
                    
                    if lastDisplayedHeader == nil {
                        lastDisplayedHeader = attributes
                    }
                    
                    let contentOffsetX = currentCollectionView.contentOffset.x + sectionLeftMargin
                    
                    var frameForSupplementaryView = attributes.frame
                    
                    let minimum = scopeOfBounds.minimum
                    let maximum = scopeOfBounds.maximum
    
                    attributes.alpha = 0
                    
                    if contentOffsetX < minimum {
                        
                        if let position = delegate?.getSearchButtonPositionOnTagsCollection?() {
                            //если заголовок вышел из под кнопки поиска
                            if attributes.frame.maxX < position.x {
                                
                                let totalScroll = (minimum - contentOffsetX)
                                let offset = contentOffsetX - currentCollectionView.contentOffset.x
                                let percentage = offset / totalScroll
                                
                                attributes.alpha = percentage
                            }
                        }
                        
                        frameForSupplementaryView.origin.x = minimum
                    }
                    else if contentOffsetX > maximum {

                        attributes.alpha = sectionHeaderAlpha
                        frameForSupplementaryView.origin.x = maximum
                    }
                    else {
                        
                        if currentSectionIndex != attributes.indexPath.section {
                            currentSectionIndex = attributes.indexPath.section
                        }
                        attributes.alpha = sectionHeaderAlpha
                        frameForSupplementaryView.origin.x = contentOffsetX
                        
                        if lastDisplayedHeader != attributes {
                            
                            scrollOffsetCounter = currentSectionIndex
                            lastDisplayedHeader = attributes
                            lastHeaderPosition = arrayOfHeadersPositions[scrollOffsetCounter]
                        }
                    }
                    
                    attributes.frame = frameForSupplementaryView
                    
                    newLayoutAttributes.append(attributes)
                }
                
            }
        }
        
        return newLayoutAttributes
    }

    
    override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
        return true
    }
    
    override func targetContentOffset(forProposedContentOffset proposedContentOffset: CGPoint, withScrollingVelocity velocity: CGPoint) -> CGPoint {
        
        guard isLeftSideSectionMangettingActive else { return proposedContentOffset }
        
        guard let layoutCollectionView = collectionView else { return proposedContentOffset }
        
        checkScrollDirection(with: proposedContentOffset.x)

        let targetRect = CGRect(x: proposedContentOffset.x,
                                y: 0,
                                width: layoutCollectionView.bounds.width,
                                height: layoutCollectionView.bounds.height)
        
        guard let attributes = layoutAttributesForElements(in: targetRect) else { return proposedContentOffset }
        
        var offsetAdjustment = CGFloat.greatestFiniteMagnitude
        let horizontalOffset = proposedContentOffset.x
        
        for attribute in attributes {
            
            let itemOffset = attribute.frame.origin.x
            
            if abs(itemOffset - horizontalOffset) < abs(offsetAdjustment) && (itemOffset - horizontalOffset) > 0 {
                
                //выставляем положение для выравнивание по левому краю, если это первая секция, то учитываем отступ слева, иначе не учитываем
                if itemOffset < sectionInset.left {
                    offsetAdjustment = itemOffset - horizontalOffset - sectionInset.left
                }
                else {
                    offsetAdjustment = itemOffset - horizontalOffset
                }
            }
        }
        return proposedContentOffset.bma_offsetBy(dx: offsetAdjustment, dy: 0)
    }
    
    
    // MARK: - Helper Methods
    
    /// Остановить скролл
    fileprivate func stopScrollView() {
        
        guard isStoppingScrollViewActive else { return }
        
        guard let currentCollectionView = collectionView else { return }
        
        currentCollectionView.setContentOffset(currentCollectionView.contentOffset, animated: false)
    }
    
    /// Проверить направление скролла по X координате
    ///
    /// - Parameter offsetX: текущее положение офсета скролла по X координате
    fileprivate func checkScrollDirection(with offsetX: CGFloat) {
        
        if lastScrollOffsetXPosition > offsetX {
            isStoppingScrollViewActive = false
        }
        else {
            isStoppingScrollViewActive = true
        }
        lastScrollOffsetXPosition = offsetX
    }
    
    func boundaries(forSection section: Int) -> (minimum: CGFloat, maximum: CGFloat)? {
        
        var result = (minimum: CGFloat(0.0), maximum: CGFloat(0.0))
        
        guard let collectionView = collectionView else { return result }
        
        let numberOfItems = collectionView.numberOfItems(inSection: section)
        
        guard numberOfItems > 0 else { return result }
        
        if let firstItem = elementsBySections[section]?[0],
            let farestItem = farestPositionBySections[section] {
            
            result.minimum = firstItem.frame.minX
            result.maximum = farestItem
            
            result.minimum -= sectionInset.left
        }
        
        return result
    }
    
    func reloadLayout() {
        cache.removeAll()
        arrayOfHeadersPositions.removeAll()
        elementsBySections.removeAll()
        invalidateLayout()
    }
}

extension TagFlowLayout: CategorizedTagsTrigger {
    
    func configure(with categorizedTagsTrigger: inout CategorizedTagsTrigger?) {
        categorizedTagsTrigger = self
    }
    
    func didSelectCategoryPressed() {
        isStoppingScrollViewActive = false
    }
    
    func didFinishScrollToCategory() {
        isStoppingScrollViewActive = true 
    }
    
    func didPressTag() {
        isSelectTag = true
        lastCollectionContentWith = contentWidth
    }
}

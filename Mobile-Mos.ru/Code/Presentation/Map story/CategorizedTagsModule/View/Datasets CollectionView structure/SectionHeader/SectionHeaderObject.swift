//
//  SectionHeaderObject.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 26.07.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

struct SectionHeaderObject: HeaderObject {

    var title:  String
}

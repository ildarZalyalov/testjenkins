//
//  SectionHeader.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 25.07.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import UIKit

class SectionHeader: UICollectionReusableView, ConfigurableView {
    @IBOutlet weak var sectionTitleLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        sectionTitleLabel.text = ""
    }
    
    func configure(with object: Any) {
        guard let cellObject = object as? SectionHeaderObject else {
            return
        }
        
        sectionTitleLabel.text = cellObject.title
    }
    
}

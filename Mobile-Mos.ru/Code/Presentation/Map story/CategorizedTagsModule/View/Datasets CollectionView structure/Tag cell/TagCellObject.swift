//
//  TagCellObject.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 19.07.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Перечисления для выбра типа тени у ячейки
enum TagShadow {
    case orangeShadow
    case grayShadow
    
    var color: UIColor {
        switch self{
        case .orangeShadow  : return UIColor.fromRGB(red: 255, green: 103, blue: 0, alpha: 0.13)
        case .grayShadow    : return UIColor.fromRGB(red: 90, green: 87, blue: 85, alpha: 0.13)
        }
    }
}
struct TagCellObject: CellObjectWithId {
    
    var itemId: String
    var title:  String
    var isFavoriteSection: Bool
    var favoritesCount: Int
    var shadowColor: UIColor
    var leftIcon: UIImage?
    var rightIcon: UIImage?
    
    init(itemId: String,
         title: String,
         isFavoriteSection: Bool,
         favoritesCount: Int,
         tagShadow: TagShadow = .orangeShadow,
         leftIcon: UIImage? = nil,
         rightIcon: UIImage? = nil) {
        
        self.itemId = itemId
        self.title = title
        self.isFavoriteSection = isFavoriteSection
        self.favoritesCount = favoritesCount
        self.shadowColor = tagShadow.color
        self.leftIcon = leftIcon
        self.rightIcon = rightIcon
    }
}

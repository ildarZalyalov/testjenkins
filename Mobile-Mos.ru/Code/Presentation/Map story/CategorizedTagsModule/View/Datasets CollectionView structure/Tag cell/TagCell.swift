//
//  TagCell.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 19.07.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import UIKit

class TagCell: UICollectionViewCell, ConfigurableView {
    
    @IBOutlet weak var favoritesCountView: UIView!
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var favoritesCount: UILabel!
    
    let selectedColor = UIColor.fromRGB(red: 249, green: 190, blue: 70)
    let defaultBackgroundColor = UIColor.white
    let selectedTextColor = UIColor.white
    let defaultTextColor = UIColor.black
    
    override var isSelected: Bool {
        didSet {
            backgroundColor = isSelected ? selectedColor : defaultBackgroundColor
            textLabel.textColor = isSelected ? selectedTextColor : defaultTextColor
        }
    }
    
    override var isHighlighted: Bool {
        didSet {
            
            backgroundColor = isHighlighted ? selectedColor : defaultBackgroundColor
            textLabel.textColor = isHighlighted ? selectedTextColor : defaultTextColor
        }
    }
    
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        textLabel.text = nil
        favoritesCount.text = nil
        favoritesCountView.isHidden = true
    }
    
    //MARK: - ConfigurableView
    
    func configure(with object: Any) {
        
        contentView.translatesAutoresizingMaskIntoConstraints = false
        
        guard let cellObject = object as? TagCellObject else {
            return
        }
        
        shadowColor = cellObject.shadowColor
        
        textLabel.text = cellObject.title
        
        guard cellObject.isFavoriteSection,
              cellObject.favoritesCount > 0
            
            else { return }
        
        favoritesCountView.isHidden = false
        favoritesCount.text = String(cellObject.favoritesCount)
    }
}

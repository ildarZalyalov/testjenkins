//
//  CategorizedTagsModuleViewController.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 19.07.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import UIKit

class CategorizedTagsModuleViewController: BaseViewController,  UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, TagLayoutDelegate, CategorizedTagsModuleViewInput, ConfigurableModuleController, CategorizedTagsDelegate{
    
    @IBOutlet weak var tagFlowLayout: TagFlowLayout!
    @IBOutlet weak var tagsCollectionView: UICollectionView!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var categoriesFlowLayout: UICollectionViewFlowLayout!
    @IBOutlet weak var categoriesCollectionView: UICollectionView!
    @IBOutlet weak var tagsCollectionHeightConstraint: NSLayoutConstraint!
    
    fileprivate var arrayHeadersXPositions = [CGFloat]()
    fileprivate var isInitialized = false
    fileprivate let tagFlowLayoutInsets = UIEdgeInsetsMake(0, 14, 0, 0)
    fileprivate let tagTextPadding: CGFloat = 22
    fileprivate let font = UIFont(customName: UIFont.CustomFontName.graphikLCGRegular, size: 15) ?? UIFont.systemFont(ofSize: 15)
    fileprivate let iPhone4RowsCount = 2
    fileprivate let iPhone4BetweenRowsDecrementer: CGFloat = 5
    fileprivate let categoryItemWithSeparatorSize = CGSize(width: 58, height: 50)
    fileprivate let categoryItemSize = CGSize(width: 50, height: 50)
    
    var output: CategorizedTagsModuleViewOutput!
    var tagsCollectionViewDataDisplayManager: CollectionViewDataSource!
    var categoriesCollectionViewDataDisplayManager: CollectionViewDataSource!
    weak var categorizedTagsTrigger: CategorizedTagsTrigger?
    
    //MARK: - Методы
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupTagFlowLayout()
        registerCells()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if !isInitialized {
           output.setupInitialState()
           isInitialized = true
        }
    }
    
    override func viewWillLayoutSubviews() {
        tagsCollectionView.collectionViewLayout.invalidateLayout()
    }
    
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    func configureModule(with object: Any) {
        
        if let object = object as? MapDataPresenter, let handler = object as? MapUIEventHandler {
            
            output.setupMapDataPresenter(with: object, mapUIEventHandler: handler)
        }
    }
    
    //MARK: - CategorizedTagsDelegate
    
    func updateHistoryItems(with tagId: String) {
        output.updateHistoryItems(with: tagId)
    }
    
    func updateFavoritedItems() {
        output.updateFavoritedItems()
    }
    
    func saveLastSelectedDataset(_ dataset: MapItemDataset) {
        output.saveDataset(dataset)
    }
    
    func reloadTagsLayout() {
        reloadTags()
    }
    
    //MARK: - CategorizedTagsModuleViewInput 
    
    func displayCategoriesItems(with dataSource: CollectionViewDataSource) {
        
        categoriesCollectionViewDataDisplayManager = dataSource
        
        categoriesCollectionViewDataDisplayManager.delegate = self
        categoriesCollectionViewDataDisplayManager.assign(to: categoriesCollectionView)
        
        categoriesCollectionView.reloadData()
    }
    
    func displayCategorizedTagsItems(with dataSource: CollectionViewDataSource) {
        
        tagsCollectionViewDataDisplayManager = dataSource

        tagsCollectionViewDataDisplayManager.delegate = self
        tagsCollectionViewDataDisplayManager.assign(to: tagsCollectionView)
        
        tagsCollectionView.reloadData()
    }
    
    
    func scrollTagsCollectionView(to xPosition: CGFloat) {
        
        var tagsContentOffset = tagsCollectionView.contentOffset
        tagsContentOffset.x = xPosition
        tagsCollectionView.setContentOffset(tagsContentOffset, animated: true)
    }
    
    func scrollCategoriesCollectionViewToItem(at itemIndex: Int) {
        let indexPath = IndexPath(item: itemIndex, section: 0)
        categoriesCollectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
    }
    
    func reloadTags() {
        
        categorizedTagsTrigger?.didPressTag()
        tagFlowLayout.reloadLayout()
        tagsCollectionView.reloadData()
    }
    
    //MARK: - UICollectionViewDelegate 
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == tagsCollectionView {
            let dataStructure = tagsCollectionViewDataDisplayManager.dataStructure
            guard let cellObject = dataStructure.cellObject(at: indexPath) as? CellObjectWithId else {
                return
            }
            
            output.didSelectTag(with: cellObject.itemId)
        }
        else {
            guard arrayHeadersXPositions[safe: indexPath.item] != nil else { return }
           
            categorizedTagsTrigger?.didSelectCategoryPressed()
            output.didSelectCategory(with: arrayHeadersXPositions[indexPath.item])
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == categoriesCollectionView {
            
            let dataStructure = categoriesCollectionViewDataDisplayManager.dataStructure
            
            if let _ = dataStructure.cellObject(at: indexPath) as? CategorySelectionWithSeparatorCellObject {
                
                return categoryItemWithSeparatorSize
            }
            else {
                return categoryItemSize
            }
        }
        else {
           return tagFlowLayout.itemSize
        }
    }
    
    //MARK: - TagFlowLayoutDelegate 
    
    func collectionView(_ collection: UICollectionView, widthForTagAtIndexPath indexPath: IndexPath) -> CGFloat {
        
        let cellObject = tagsCollectionViewDataDisplayManager.dataStructure.cellObject(at: indexPath) as! TagCellObject
        let text = cellObject.title
        let tagWidth = text.sizeOfString(with: font).width
        
        let finalWidth = tagTextPadding + tagWidth + tagTextPadding
        
        return finalWidth
    }
    
    func collectionViewDidChangeSection(with sectionIndex: Int) {
        output.changeSelectedCategory(with: sectionIndex)
    }
    
    func collectionViewHeadersPositions(with arrayOfPositions: [CGFloat]) {
        arrayHeadersXPositions = arrayOfPositions
    }
    
    func getSearchButtonPositionOnTagsCollection() -> CGPoint {
        return searchButton.convert(searchButton.origin, to: tagsCollectionView)
    }
    
    //MARK: - UIScrollViewDelegate
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        categorizedTagsTrigger?.didFinishScrollToCategory()
    }
    
    //MARK: Buttons action
    
    @IBAction func searchButtonPressed(_ sender: Any) {
        output.didSelectSearch()
    }
    
    //MARK: Helpers 
    
    func setupTagFlowLayout() {
        
        tagFlowLayout.delegate = self
        tagFlowLayout.sectionInset = tagFlowLayoutInsets
        tagFlowLayout.configure(with: &categorizedTagsTrigger)
        
        if UIDevice.current.hasCompactScreen {
            
            tagFlowLayout.numberOfRows = iPhone4RowsCount
            tagFlowLayout.heightBetweenEachRowDecrementer += tagFlowLayout.heightBetweenEachRowDecrementer/2 - iPhone4BetweenRowsDecrementer
            
            tagsCollectionHeightConstraint.constant -= tagFlowLayout.cellHeight
            
            output.didChangeCategorizedTagsHeight(with: tagsCollectionHeightConstraint.constant + categoriesCollectionView.height)
        }
    }
    
    func registerCells() {
        
        tagsCollectionView.registerCellNib(for: TagCellObject.self)
        tagsCollectionView.registerHeaderNib(for: SectionHeaderObject.self)
        categoriesCollectionView.registerCellNib(for: CategorySelectionCellObject.self)
        categoriesCollectionView.registerCellNib(for: CategorySelectionWithSeparatorCellObject.self)
    }

}

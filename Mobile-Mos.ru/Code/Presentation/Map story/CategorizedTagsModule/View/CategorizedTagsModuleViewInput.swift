//
//  CategorizedTagsModuleViewInput.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 19.07.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol CategorizedTagsModuleViewInput: class {
    
    func displayCategorizedTagsItems(with dataSource: CollectionViewDataSource)
    
    func displayCategoriesItems(with dataSource: CollectionViewDataSource)
    
    func reloadTags()
    
    func scrollTagsCollectionView(to xPosition: CGFloat)
    
    func scrollCategoriesCollectionViewToItem(at itemIndex: Int)
}

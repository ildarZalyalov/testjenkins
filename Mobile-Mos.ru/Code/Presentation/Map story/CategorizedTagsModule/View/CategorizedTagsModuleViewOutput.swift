//
//  CategorizedTagsModuleViewOutput.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 19.07.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol CategorizedTagsModuleViewOutput: class {
    
    func setupInitialState()
    
    func setupMapDataPresenter(with mapDataPresenter: MapDataPresenter?, mapUIEventHandler: MapUIEventHandler?)
    
    func changeSelectedCategory(with index: Int)
    
    func didChangeCategorizedTagsHeight(with height: CGFloat)
    
    func didSelectCategory(with categoryHeaderXPosition: CGFloat)
    
    func didSelectTag(with tagId: String)
    
    func didSelectSearch()
    
    func updateHistoryItems(with tagId: String)
    
    func updateFavoritedItems()
    
    func saveDataset(_ dataset: MapItemDataset)
}

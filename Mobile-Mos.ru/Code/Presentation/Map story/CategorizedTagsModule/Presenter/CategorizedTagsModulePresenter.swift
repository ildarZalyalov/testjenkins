//
//  CategorizedTagsModulePresenter.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 19.07.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class CategorizedTagsModulePresenter: CategorizedTagsModuleViewOutput, CategorizedTagsModuleInteractorOutput {
    
    let historySectionId    = 1
    let favoriteSectionId   = 0
    
    weak var view: CategorizedTagsModuleViewInput!
    weak var mapDataPresenter: MapDataPresenter?
    weak var mapUIEventHandler: MapUIEventHandler?
    
    var router: CategorizedTagsModuleRouterInput!
    var interactor: CategorizedTagsModuleInteractorInput!
    
    var tagsDataSourceFactory: TagItemsDataSourceFactory!
    var tagsDataSourceConfiguration: TagItemsDataSourceFactoryConfiguration!
    var categoriesDataSourceFactory: CategorySelectionDataSourceFactory!
    
    var datasetItemById:  [String : MapItemDataset]!
    var favoritedDatasetsById = [String : MapItemDataset]()
    
    var lastCategories = [MapItemCategory]()
    var historyCategory: MapItemCategory!
    
    /// индекс местоположения ячейки "история"
    var historySectionCurrentIndex = 0
    
    /// Текущий выбранный индекс категории
    var currentSelectedIndex = 0 {
        willSet{
            previousSelectedIndex = previousSelectedIndex == currentSelectedIndex ? 1 : currentSelectedIndex
        }
    }
    
    /// Предыдущий выбранны индекс категрии
    var previousSelectedIndex = 1
    
    var historySectionExists = false
    var isUpdatingFavorites = false
    var isInitialized = false
    
    
    //MARK: - CategorizedTagsModuleViewOutput
    
    func setupInitialState() {
        
        mapUIEventHandler?.showCategorizedTagsPlaceholders()
        interactor.obtainCategoryImages(selectedIndex: 0)
        interactor.obtainFavoritesItems()
        interactor.obtainHistoryItems()
        interactor.obtainCategoriesItems()
        isInitialized = true
    }
    
    func setupMapDataPresenter(with mapDataPresenter: MapDataPresenter?, mapUIEventHandler: MapUIEventHandler?) {
        
        self.mapDataPresenter = mapDataPresenter
        self.mapUIEventHandler = mapUIEventHandler
    }
    
    func changeSelectedCategory(with index: Int) {
        
        if currentSelectedIndex == favoriteSectionId && previousSelectedIndex != currentSelectedIndex {
            mapDataPresenter?.clearMap()
        }
        
        interactor.obtainCategoryImages(selectedIndex: index)
        view.scrollCategoriesCollectionViewToItem(at: index)
    }
    
    func didSelectCategory(with categoryHeaderXPosition: CGFloat) {
        view.scrollTagsCollectionView(to: categoryHeaderXPosition)
    }
    
    func didSelectTag(with tagId: String) {
        
        datasetItemById += favoritedDatasetsById
        
        guard let selectedDataset = datasetItemById[tagId] else { return }
        
        if let favoritedDataset = selectedDataset as? FavoritedDataset, currentSelectedIndex == favoriteSectionId {
            
            mapDataPresenter?.updateMap(with: favoritedDataset)
        }
        else {
            mapDataPresenter?.updateMap(with: selectedDataset)
        }
    }
    
    func saveDataset(_ dataset: MapItemDataset) {
        
        if historyCategory != nil { historySectionExists = true }
        
        interactor.save(dataset: dataset)
        
        var categoryName: String = ""
        for category in lastCategories {
            if category.datasets.contains(where: { return $0.itemId == dataset.itemId }) {
                categoryName = category.name
            }
        }
        
        interactor.sendSelectDatasetEvent(with: dataset.name, categoryName: categoryName)
    }
    
    func didChangeCategorizedTagsHeight(with height: CGFloat) {
        mapUIEventHandler?.updateCategorizedTagsLayout(with: height)
    }
    
    func didSelectSearch() {
        mapUIEventHandler?.showSearchModule()
    }
    
    func updateHistoryItems(with tagId: String) {
        didSelectTag(with: tagId)
    }
    
    func updateFavoritedItems() {
        isUpdatingFavorites = true
        interactor.obtainFavoritesItems()
    }
    
    //MARK: - CategorizedTagsModuleInteractorOutput
    
    func didFinishObtainingCategoryImages(images: [UIImage], selectedIndex: Int) {
        
        let dataSource = categoriesDataSourceFactory.buildDataSourceConfiguration(from: images, selectedIndex: selectedIndex)
        currentSelectedIndex = selectedIndex
        view.displayCategoriesItems(with: dataSource)
    }
    
    func didFinishObtainingCategoryItems(with result: MapItemCategoryObtainMultipleResult) {
        
        switch result {
        case .success(categories: let items):
            
            lastCategories += items
            
            mapUIEventHandler?.hideCategorizedTagsPlaceholders()
            show(items: lastCategories)
            
        case .failure(let error):
            interactor.obtainCategoriesItems()
            print(error.localizedDescription)
        }
    }
   
    func didFinishObtainingFavorites(favorites: [FavoritedDataset]) {
        
        guard !favorites.isEmpty else {
            
            removeFavoriteSectionIfExists()
            return
        }
        
        var allFavorites = [MapItem]()
        
        favorites.enumerated().forEach {
            allFavorites.append(contentsOf: $0.element.favoritedItems)
            favoritedDatasetsById[String($0.element.itemId) + "\($0.offset)"] = $0.element
        }
        
        if !isInitialized {
            
            mapDataPresenter?.updateMap(with: allFavorites)
        }
        
        let favoriteCategory = MapItemCategory(id: favoriteSectionId, name: StringsHelper.favoriteSectionTitle, datasetsCount: favorites.count, datasets: [], favoritedDatasets: favorites, isSaved: false)
        
        updateCategories(with: favoriteCategory)
    }
    
    func didFinishObtainingHistory(historyItems: [MapItemDataset]) {
        
        guard !historyItems.isEmpty else { return }
        
        let historyCategory = MapItemCategory(id: historySectionId, name: StringsHelper.historySectionTitle, datasetsCount: historyItems.count, datasets: historyItems as! [Dataset], favoritedDatasets: [], isSaved: false)
        self.historyCategory = historyCategory
        
        updateCategories(with: historyCategory)
    }
    
    func didFinishSavingHistoryItems(with success: Bool) {
        
        guard success else { return }
        
        interactor.obtainHistoryItems()
        
        if !historySectionExists{
            insertNewTags(with: [historyCategory])
        }
        else {
            updateTags(with: historyCategory, at: historySectionCurrentIndex)
        }
    }
    
    //MARK: - Helpers
    
    func updateCategories(with category: MapItemCategory) {
        
        if let index = lastCategories.index(where: { $0.id == category.id}) {
            
            lastCategories[index] = category
            historySectionCurrentIndex = index
            
            if category.id == favoriteSectionId {
                
                updateTags(with: category, at: favoriteSectionId)
                historySectionCurrentIndex = 1
            }
        }
        else if lastCategories.contains(where: { $0.id == favoriteSectionId}){
            lastCategories.insert(category, at: 1)
            historySectionCurrentIndex = 1
        }
        else {
            
            lastCategories.insert(category, at: 0)
            historySectionCurrentIndex = 0
            
            if category.id == favoriteSectionId && isUpdatingFavorites {
                insertNewTags(with: [category])
                historySectionCurrentIndex = 1
            }
        }
    }
    
    func updateTags(with items: MapItemCategory..., at sectionIndex: Int) {
        
        tagsDataSourceFactory.updateSection(with: items, at: sectionIndex, of: &tagsDataSourceConfiguration!)
        
        datasetItemById = tagsDataSourceConfiguration.selectionItems
        view.reloadTags()
    }
    
    func insertNewTags(with categories: [MapItemCategory]) {
        
        tagsDataSourceFactory.insertNewSectionToTop(with: categories, to: &tagsDataSourceConfiguration!)
        
        datasetItemById = tagsDataSourceConfiguration.selectionItems
        view.reloadTags()
        interactor.refreshCategoryImages(for: lastCategories, selectedIndex: currentSelectedIndex)
        let indexToSelect = currentSelectedIndex == 0 ? currentSelectedIndex : currentSelectedIndex + 1
        changeSelectedCategory(with: indexToSelect)
    }
    
    func removeFavoriteSectionIfExists() {
        if lastCategories.contains(where: { $0.id == favoriteSectionId }) {
            lastCategories.removeFirst()
            show(items: lastCategories)
            view.reloadTags()
        }
    }
    
    func show(items: [BaseModel]) {
        
        if let categories = items as? [MapItemCategory] {
            
            let tagsDataSource = tagsDataSourceFactory.buildDataSourceConfiguration(from: categories)
            tagsDataSourceConfiguration = tagsDataSource
            
            interactor.refreshCategoryImages(for: categories, selectedIndex: 0)
            datasetItemById = tagsDataSource.selectionItems
            view.displayCategorizedTagsItems(with: tagsDataSource.dataSource)
        }
    }
}

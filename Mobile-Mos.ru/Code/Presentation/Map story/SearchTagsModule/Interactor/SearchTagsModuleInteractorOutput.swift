//
//  SearchTagsModuleInteractorOutput.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 22.08.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol SearchTagsModuleInteractorOutput: class {
    
    func didFinishSearchingTag(with result: MapItemDatasetObtainMultipleResult)
}

//
//  SearchTagsModuleInteractor.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 22.08.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import CoreLocation

class SearchTagsModuleInteractor: SearchTagsModuleInteractorInput {
    
    weak var output: SearchTagsModuleInteractorOutput!
    var mapItemsService: MapItemsService!
    var distanceStringsBuilder: DistanceStringBuilder!
    var analyticsManager: AnalyticsManager!
    
    //MARK: - SearchTagsModuleInteractorInput
    
    func searchTag(with tagName: String) {
        
        mapItemsService.searchDatasetFromBD(with: tagName) { [weak self] (result) in
           self?.output.didFinishSearchingTag(with: result)
        }
    }
    
    func save(dataset: MapItemDataset) {
        
        mapItemsService.save(mapItemsDataset: dataset) { (result) in
            
            switch result {
                
            case .success:
                return
            case .error(let error):
                
                print("Ошибка при сохранении: \(error)")
            }
        }
    }
    
    func sendDatasetSearchEvent(with datasetName: String) {
        
        let datasetEvent: DatasetEventData = (datasetName, analyticsManager.categoryName, AnalyticsConstants.emptyValueName, AnalyticsConstants.emptyValueName)
        analyticsManager.sendMapConversationEvent(with: .searchDataset(withString: AnalyticsConstants.selectDatasetEventName), aboutAppData: nil, appSharingSource: nil, datasetData: datasetEvent, mapObjectSharingData: nil, overlapData: nil)
    }
}

//
//  SearchTagsModuleInteractorInput.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 22.08.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol SearchTagsModuleInteractorInput: class {
    
    func sendDatasetSearchEvent(with datasetName: String)
    
    /// Найти тег по имени
    ///
    /// - Parameter tagName: имя тега
    func searchTag(with tagName: String)
    
    /// Сохранить датасет
    ///
    /// - Parameter dataset: датасет для сохранения
    func save(dataset: MapItemDataset)
}

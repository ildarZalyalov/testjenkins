//
//  SearchTagsModuleViewOutput.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 22.08.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol SearchTagsModuleViewOutput: class {
    
    func setupInitialState()
    
    func setupMapDataPresenter(with mapDataPresenter: MapDataPresenter?, mapUIEventHandler: MapUIEventHandler?)
    
    func didPressCancel()
    
    func didSearchTag(with name: String)
    
    func didSelectTag(with tagId: String)
    
    func willFoldSearchTags(with height: CGFloat)
    
    func unfoldSearchTags(with height: CGFloat)
    
    func didSelectTextField()
}

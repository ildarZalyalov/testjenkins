//
//  SearchTagCell.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 23.08.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import UIKit

class SearchTagCell: UITableViewCell, ConfigurableView {
    
    //MARK: - ConfigurableView
    @IBOutlet weak var nameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        nameLabel.textColor = UIColorPalette.searchStringSubstringColor
    }
    
    func configure(with object: Any) {
        
        guard let cellObject = object as? SearchTagCellObject else {
            return
        }
        
        nameLabel.attributedText = NSMutableAttributedString(with: cellObject.name, subString: cellObject.searchString, subStringColor: UIColor.black)
    }
}

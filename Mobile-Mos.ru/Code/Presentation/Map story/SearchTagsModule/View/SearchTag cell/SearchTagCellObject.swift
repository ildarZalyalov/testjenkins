//
//  SearchTagCellObject.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 23.08.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Объект ячейки поиска тегов
struct SearchTagCellObject: CellObjectWithId {
    
    /// id тега
    var itemId: String
    
    /// имя тега
    var name: String
    
    /// Текст поиска тега
    var searchString: String
}

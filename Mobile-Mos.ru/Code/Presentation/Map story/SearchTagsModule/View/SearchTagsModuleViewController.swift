//
//  SearchTagsModuleViewController.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 22.08.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import UIKit

class SearchTagsModuleViewController: BaseViewController, SearchTagsModuleViewInput, ConfigurableModuleController, UITextFieldDelegate, UITableViewDelegate, SearchModuleDelegate {
    
    var isInitialized = false
    var isTableViewDisplayed = false
    
    let tableViewAnimationDuration = 0.5
    let searchRequestTimeout = 0.5
    var objectsCount = 0
    
    let footerViewHeight: CGFloat = 40
    let tableViewDefaultBottomConstraint: CGFloat = 0
    var tableViewContentViewNormalHeight: CGFloat!
    var normalKeyboardHeight: CGFloat!
    
    weak var weakTextFieldMaskLayer: CALayer!
    weak var weakTextFieldFrameLayer: CALayer!
    weak var weakTextFieldBottomLayer: CALayer!
    
    let shakeTransform = CGAffineTransform(rotationAngle: (5 * .pi/6))
    let shakeDuration = 0.4
    let springWithDamping: CGFloat = 0.4
    let sprintInitialSpringVelocity: CGFloat = 0.5
    var isShaking = false
    lazy var searchRequestPerformer: DelayedSelectorPerformer = {
        return DelayedSelectorPerformer(target: self, selector: #selector(beginSearching(with:)))
    }()
    
    @IBOutlet weak var tableViewContentView: UIView!
    @IBOutlet weak var roundedViewWithTextField: UIView!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var tableViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomContentViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomContentViewHeightConstraint: NSLayoutConstraint!
    
    var output: SearchTagsModuleViewOutput!
    var tableViewDataDisplayManager: TableViewDataSource!
    var notificationCenter: NotificationCenter!
    
    var tableViewDefaultHeight: CGFloat {
        
        var contentHeight = bottomContentViewHeightConstraint.constant
        
        if #available(iOS 11, *) {
            contentHeight += view.safeAreaInsets.bottom
        }
        
        return contentHeight
    }
    
    //MARK: - Методы
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureTextField()
        configureTableView()
        registerForKeyboardNotifications()
    }
    
    deinit {
        notificationCenter.removeObserver(self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if !isInitialized {
            
            output.setupInitialState()
            isInitialized = true
        }
    }
    
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        willFoldSearchBar()
    }
    
    
    
    //MARK: - SearchModuleDelegate
    
    func showKeyboard() {
        
       textField.becomeFirstResponder()
    }
    
    func clearSearchFieldText() {
        textField.text = nil
    }
    
    //MARK: - SearchTagsModuleViewInput
    
    func displaySearchResult(with dataSource: TableViewDataSource) {
        
        tableViewDataDisplayManager = dataSource
        tableViewDataDisplayManager.delegate = self
        tableViewDataDisplayManager.assign(to: tableView)
        
        if dataSource.dataStructure.numberOfObjects(at: 0) == 0 {
            
            setTableViewContentViewHidden(hidden: false)
        }
        else {
            setTableViewContentViewHidden(hidden: true)
        }
        
        objectsCount = dataSource.dataStructure.numberOfObjects(at: 0)
        displaySearchTableIfNeeded()
        
        tableView.reloadData()
        refreshTableViewHeight()
    }
    
    func displayTagName(with name: String) {
        textField.text = name
    }
    
    //MARK: - ConfigurableModuleController
    
    func configureModule(with object: Any) {
        
        if let object = object as? MapDataPresenter, let handler = object as? MapUIEventHandler {
            output.setupMapDataPresenter(with: object, mapUIEventHandler: handler)
        }
    }
    
    //MARK: - UITextFields methods
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        output.didSelectTextField()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if range.location == 0 && string == " " {
            return false
        }
        
        if range.location == 0 && string.isEmpty  {
            hideSearchTableIfNeeded()
        }
        
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        
        if textField.clearButtonMode == .always {
            
            textField.clearButtonMode = .whileEditing
            textField.becomeFirstResponder()
        }
        
        hideSearchTableIfNeeded()
        
        return true
    }
    
    @objc
    func textFieldDidChange(_ textField: UITextField) {
        
        searchRequestPerformer.cancelDelayedPerform()
        
        if let searchText = textField.text, !searchText.isEmpty {
            
            searchRequestPerformer.argument = searchText
            searchRequestPerformer.perform(afterDelay: searchRequestTimeout)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if objectsCount > 1 || objectsCount == 0 {
            
            CATransaction.begin()
            CATransaction.setCompletionBlock({ [weak self] in
                self?.isShaking = false
            })
            
            shake(views: textField, roundedViewWithTextField, tableView)
            
            CATransaction.commit()
            
        }
        else {
            
            tableView.delegate?.tableView!(tableView, didSelectRowAt: IndexPath(item: 0, section: 0))
            
        }
        
        return true
    }
    
    //MARK: - UITableViewDelegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: false)
        
        let dataStructure = tableViewDataDisplayManager.dataStructure
        guard let cellObject = dataStructure.cellObject(at: indexPath) as? CellObjectWithId else {
            return
        }
        
        willFoldSearchBar()
        output.didSelectTag(with: cellObject.itemId)
    }
    
    //MARK: - Buttons action
    
    @IBAction func cancelPressed(_ sender: Any) {
        
        output.didPressCancel()
        textField.resignFirstResponder()
        textField.text = nil
        normalKeyboardHeight = nil
        hideSearchTableIfNeeded()
    }
    
    //MARK: Helpers
    
    func registerForKeyboardNotifications(){
        
        notificationCenter.addObserver(self,
                                    selector:#selector(self.keyboardWillChangeFrame(with:)),
                                    name: NSNotification.Name.UIKeyboardWillChangeFrame,
                                    object: nil)
        
        notificationCenter.addObserver(self,
                                       selector: #selector(self.keyboardWillShow(with:)),
                                       name: NSNotification.Name.UIKeyboardWillShow,
                                       object: nil)
        
        notificationCenter.addObserver(self,
                                       selector: #selector(self.keyboardWillHide(with:)),
                                       name: NSNotification.Name.UIKeyboardWillHide,
                                       object: nil)
    }
    
    @objc func keyboardWillChangeFrame(with notification: Notification) {
        
        guard let info = notification.userInfo,
              normalKeyboardHeight != nil
        else { return }
        
        let keyboardAnimationDuration = info[UIKeyboardAnimationDurationUserInfoKey] as! NSNumber
    
        if let keyboardSize = (info[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size {
            
            if keyboardSize.height > normalKeyboardHeight {
                
                view.layoutIfNeeded()
                UIView.animate(withDuration: keyboardAnimationDuration.doubleValue, animations: { [weak self] in
                    
                    guard let strongSelf = self else { return }
                    
                    strongSelf.bottomContentViewBottomConstraint.constant += keyboardSize.height - strongSelf.normalKeyboardHeight
                    
                    strongSelf.view.layoutIfNeeded()
                })
                
                normalKeyboardHeight = keyboardSize.height
            }
            else {
                
                guard keyboardSize.height != normalKeyboardHeight else { return }
                
                view.layoutIfNeeded()
                UIView.animate(withDuration: keyboardAnimationDuration.doubleValue, animations: { [weak self] in
                    
                    guard let strongSelf = self else { return }
                    
                    strongSelf.bottomContentViewBottomConstraint.constant -= strongSelf.normalKeyboardHeight - keyboardSize.height
                    strongSelf.view.layoutIfNeeded()
                })
                
                normalKeyboardHeight = keyboardSize.height
            }
        }
    }
    
    
    @objc func keyboardWillShow(with notification: Notification) {
        
        guard let info = notification.userInfo,
              let keyboardEndSize = (info[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size
        else { return }
        
        
        guard normalKeyboardHeight == nil else { return }
        
        normalKeyboardHeight = keyboardEndSize.height
        output.unfoldSearchTags(with: keyboardEndSize.height)
    }
    
    @objc func keyboardWillHide(with notification: Notification) {
        guard let info = notification.userInfo,
            let _ = (info[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size
            else { return }
        
        
        willFoldSearchBar()
    }
    
    func configureTextField() {
        
        textField.placeholder = StringsHelper.placeHolderText
        
        textField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
    }
    
    func configureTableView() {
        
        tableViewContentViewNormalHeight = tableViewContentView.height
        
        tableView.tableFooterView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.width, height: footerViewHeight))
    }
    
    func refreshTableViewHeight() {
        
        view.layoutIfNeeded()
        UIView.animate(withDuration: tableViewAnimationDuration) { [weak self] in
            guard let strongSelf = self else { return }
            
            strongSelf.tableViewHeightConstraint.constant = strongSelf.tableView.contentSize.height
            strongSelf.tableViewBottomConstraint.constant = strongSelf.tableViewDefaultBottomConstraint
            strongSelf.view.layoutIfNeeded()
        }
    }
    
    func setTableViewContentViewHidden(hidden: Bool) {
        
        tableViewContentView.isHidden = hidden
        tableViewContentView.height = hidden ? 0 : tableViewContentViewNormalHeight
        view.layoutIfNeeded()
    }
    
    
    func displaySearchTableIfNeeded() {
        
        guard !isTableViewDisplayed else { return }
        
        showTextFieldSeparator()
        
        isTableViewDisplayed = true
        tableView.isHidden = false
        
        view.layoutIfNeeded()
        UIView.animate(withDuration: tableViewAnimationDuration) { [weak self] in
            
            guard let strongSelf = self else { return }
            
            strongSelf.tableViewBottomConstraint.constant = strongSelf.tableViewDefaultBottomConstraint
            strongSelf.view.layoutIfNeeded()
        }
    }
    
    func hideSearchTableIfNeeded() {
        
        guard isTableViewDisplayed else { return }
        
        isTableViewDisplayed = false
        
        view.layoutIfNeeded()
        UIView.animate(withDuration: tableViewAnimationDuration, animations: { [weak self] in
            
            guard let strongSelf = self else { return }
            
            strongSelf.tableViewHeightConstraint.constant = strongSelf.tableViewDefaultHeight
            strongSelf.tableViewBottomConstraint.constant = strongSelf.tableViewDefaultBottomConstraint
            
            strongSelf.view.layoutIfNeeded()
            strongSelf.hideTextFieldSeparator()
            
        }) { [weak self] (isFinished) in
            
            guard isFinished else { return }
            
            self?.tableView.isHidden = true
        }
    }
    
    @objc func beginSearching(with text: String) {
        output.didSearchTag(with: text)
    }
    
    func showTextFieldSeparator() {

        roundedViewWithTextField.masksToBounds = true

        let bounds = roundedViewWithTextField.bounds
        let roundedViewSidesYPosition: CGFloat = 16
        
        let radius = roundedViewWithTextField.cornerRadius
        let maskPath = UIBezierPath(roundedRect: bounds, byRoundingCorners: [.topLeft, .topRight], cornerRadii: CGSize(width: radius, height: radius))

        let maskLayer = CAShapeLayer()
        maskLayer.frame = bounds
        maskLayer.path = maskPath.cgPath

        roundedViewWithTextField.layer.mask = maskLayer
        
        let frameLayer = CAShapeLayer()
        frameLayer.frame = bounds
        frameLayer.path = maskPath.cgPath
        frameLayer.strokeColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        frameLayer.fillColor = nil

        roundedViewWithTextField.layer.addSublayer(frameLayer)

        let bottomBorder = CALayer()
        let width = CGFloat(1.0)
        bottomBorder.borderColor = UIColor.white.cgColor
        bottomBorder.frame = CGRect(x: 0, y: bounds.size.height - width, width:  bounds.size.width, height: bounds.size.height)

        bottomBorder.borderWidth = width
        roundedViewWithTextField.layer.addSublayer(bottomBorder)
        
        let leftBorder = CALayer()
        leftBorder.borderColor = UIColor.white.cgColor
        leftBorder.frame = CGRect(x: 0, y: roundedViewSidesYPosition, width: 1, height: bounds.size.height - roundedViewSidesYPosition)
        leftBorder.borderWidth = width / 2
        
        roundedViewWithTextField.layer.addSublayer(leftBorder)
        
        let rightBorder = CALayer()
        rightBorder.borderColor = UIColor.white.cgColor
        rightBorder.frame = CGRect(x: bounds.size.width - 1, y: roundedViewSidesYPosition, width: 1, height: bounds.size.height - roundedViewSidesYPosition)
        rightBorder.borderWidth = width / 2
        
        roundedViewWithTextField.layer.addSublayer(rightBorder)
        
        weakTextFieldMaskLayer = maskLayer
        weakTextFieldFrameLayer = frameLayer
        weakTextFieldBottomLayer = bottomBorder

        view.layoutSubviews()
    }

    func hideTextFieldSeparator() {

        weakTextFieldMaskLayer.removeFromSuperlayer()
        weakTextFieldFrameLayer.removeFromSuperlayer()
        weakTextFieldBottomLayer.removeFromSuperlayer()
    }
    
    func shake(views: UIView...) {
        
        guard !isShaking else { return }
        
        isShaking = true
        
        for view in views {
            
            view.transform = shakeTransform
            UIView.animate(withDuration:shakeDuration , delay: 0, usingSpringWithDamping: springWithDamping, initialSpringVelocity: sprintInitialSpringVelocity, options: .curveEaseInOut, animations: {
                
                view.transform = .identity
            }, completion: nil)
        }
    }
    
    func willFoldSearchBar() {
        
        NSObject.cancelPreviousPerformRequests(withTarget: self)
        textField.clearButtonMode = .always
        
        output.willFoldSearchTags(with: tableViewDefaultHeight)
        textField.resignFirstResponder()
        hideSearchTableIfNeeded()
        
        normalKeyboardHeight = nil
    }
}

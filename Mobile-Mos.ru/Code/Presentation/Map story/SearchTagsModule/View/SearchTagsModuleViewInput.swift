//
//  SearchTagsModuleViewInput.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 22.08.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol SearchTagsModuleViewInput: class {
    
    /// Отобразить результаты поиска
    ///
    /// - Parameter dataSource: класс с данными для таблицы
    func displaySearchResult(with dataSource: TableViewDataSource)
    
    /// Отобразить имя тега
    ///
    /// - Parameter name: имя тега
    func displayTagName(with name: String)
}

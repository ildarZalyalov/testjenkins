//
//  SearchTagDataSourceFactoryImplementation.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 23.08.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class SearchTagDataSourceFactoryImplementation: SearchTagDataSourceFactory {
    
    //MARK: - SearchTagDataSourceFactory
    
    func buildDataSourceConfiguration(from datasets: [MapItemDataset], maxDisplayCount: Int, searchString: String) -> SearchTagDataSourceConfiguration {
        
        let dataStructure = TableViewDataSourceStructure()
        
        var selectionItems = [String : MapItemDataset]()
        
        for (i,dataset) in datasets.enumerated() where i < maxDisplayCount {
            
            let cellObject = SearchTagCellObject(itemId: String(dataset.itemId), name: dataset.name, searchString: searchString)
            
            dataStructure.appendCellObject(cellObject)
            
            selectionItems[cellObject.itemId] = dataset
        }
        
        let dataSource = TableViewDataSource(with: dataStructure)
        
        return (dataSource, selectionItems)
    }
}

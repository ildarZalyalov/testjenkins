//
//  SearchTagDataSourceFactory.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 23.08.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

typealias SearchTagDataSourceConfiguration = (dataSource: TableViewDataSource, selectionItems: [String : MapItemDataset])

protocol SearchTagDataSourceFactory {
    
    /// Получить конфигурацию с данными из датасетов
    ///
    /// - Parameter datasets: датасеты
    /// - Returns: конфигурация с данными 
    func buildDataSourceConfiguration(from datasets: [MapItemDataset], maxDisplayCount: Int, searchString: String) -> SearchTagDataSourceConfiguration
}

//
//  SearchTagsModuleRouterInput.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 22.08.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol SearchTagsModuleRouterInput: class {
    
    /// Настроить делегат модуля поиска
    func configureSearchModuleDelegate()
}

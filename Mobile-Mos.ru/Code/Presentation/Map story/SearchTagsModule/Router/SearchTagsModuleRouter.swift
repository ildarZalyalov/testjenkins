//
//  SearchTagsModuleRouter.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 22.08.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class SearchTagsModuleRouter: SearchTagsModuleRouterInput {
    
    weak var transitionHandler: SearchTagsModuleViewController!
    
    //MARK: - SearchTagsModuleRouterInput
    
    func configureSearchModuleDelegate() {
        
        guard let configurableController = transitionHandler.parent as? ConfigurableModuleController else { return }
        
        configurableController.configureModule(with: transitionHandler.output)
    }
}

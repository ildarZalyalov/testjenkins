//
//  SearchTagsModulePresenter.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 22.08.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class SearchTagsModulePresenter: SearchTagsModuleViewOutput, SearchTagsModuleInteractorOutput {
    
    let maxDisplayCount = 3
    var lastSearchString = ""
    
    weak var view: SearchTagsModuleViewInput!
    weak var mapDataPresenter: MapDataPresenter?
    weak var mapUIEventHandler: MapUIEventHandler?
    
    var router: SearchTagsModuleRouterInput!
    var interactor: SearchTagsModuleInteractorInput!
    var searchTagFactory: SearchTagDataSourceFactory!
    var searchSelectionItems:[String: MapItemDataset]!
    
    //MARK: - SearchTagsModuleViewOutput
    
    func setupInitialState() {
        router.configureSearchModuleDelegate()
    }
    
    func setupMapDataPresenter(with mapDataPresenter: MapDataPresenter?, mapUIEventHandler: MapUIEventHandler?) {
        
        self.mapDataPresenter = mapDataPresenter
        self.mapUIEventHandler = mapUIEventHandler
    }
    
    func didPressCancel() {
        mapUIEventHandler?.hideSearchModule()
    }
    
    func didSearchTag(with name: String) {
        
        lastSearchString = name
        interactor.searchTag(with: name)
    }
    
    func willFoldSearchTags(with height: CGFloat) {
        mapUIEventHandler?.foldSearchModule(with: height)
    }
    
    func unfoldSearchTags(with height: CGFloat) {
        mapUIEventHandler?.unfoldSearchModule(with: height)
    }
    
    func didSelectTag(with tagId: String) {
        
        guard let dataset = searchSelectionItems[tagId] else { return }
        
        view.displayTagName(with: dataset.name)
        mapDataPresenter?.refreshHistoryItems(with: tagId)
        interactor.sendDatasetSearchEvent(with: dataset.name)
    }
    
    func didSelectTextField() {
        mapUIEventHandler?.didSelectSearchField()
    }
    
    //MARK: - SearchTagsModuleInteractorOutput
    
    func didFinishSearchingTag(with result: MapItemDatasetObtainMultipleResult) {
        switch result {
        case .success(items: let items):
            
            
            let dataSourceConfiguration = searchTagFactory.buildDataSourceConfiguration(from: items, maxDisplayCount: maxDisplayCount, searchString: lastSearchString)
            
            
            view.displaySearchResult(with: dataSourceConfiguration.dataSource)
            searchSelectionItems = dataSourceConfiguration.selectionItems
        case .failure(error: let error):
            print("Ошибка поиска: \(error.localizedDescription)")
        }
    }
}

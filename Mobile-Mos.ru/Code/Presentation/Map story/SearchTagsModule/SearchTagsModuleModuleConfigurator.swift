//
//  SearchTagsModuleModuleConfigurator.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 22.08.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import UIKit

class SearchTagsModuleModuleConfigurator: BaseModuleConfigurator {
    
    override func configureModule(for view: UIViewController) {
        
        let viewController = view as! SearchTagsModuleViewController
        let presenter = SearchTagsModulePresenter()
        let interactor = SearchTagsModuleInteractor()
        let router = SearchTagsModuleRouter()
        
        let mapItemsServiceImplementation = UIApplication.shared.serviceBuilder.getMapItemsService()
        let distanceService = DistanceStringBuilderImplementation()
        let searchTagFactory = SearchTagDataSourceFactoryImplementation()
        let notificationCenterDefault = NotificationCenter.default
        let analyticsManager = AnalyticsManagerBuilder.getAnalyticsManager()
        
        viewController.output = presenter
        viewController.notificationCenter = notificationCenterDefault
        
        presenter.view = viewController
        presenter.interactor = interactor
        presenter.router = router
        presenter.searchTagFactory = searchTagFactory
        
        interactor.output = presenter
        interactor.mapItemsService = mapItemsServiceImplementation
        interactor.distanceStringsBuilder = distanceService
        interactor.analyticsManager = analyticsManager
        
        router.transitionHandler = viewController
    }
}

//
//  MapDataPresenter.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 13.04.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Протокол объекта, умеющего отображать на карте различные данные
protocol MapDataPresenter: class {
    
    /// Обновить карту, отобразив объекты из набора данных mos.ru
    ///
    /// - Parameter dataStructureId: идентификатор набора данных
    func updateMap(with dataset: MapItemDataset)
    
    /// Обновить карту, отобразив объекты из избранных датасета
    ///
    /// - Parameter favorites: избранные датасета
    /// - Parameter zoomOnPoints: нужно ли зумить до точек
    func updateMap(with favorites: [MapItem])
    
    /// Обновить карту, отобразив объекты из избранного
    ///
    /// - Parameter favoriteDataSet: избранный датасет
    func updateMap(with favoriteDataSet: FavoritedDataset)
    
    /// Очистить карту
    func clearMap()
    
    /// Обновить историю с id тега
    ///
    /// - Parameter tagId: id тега
    func refreshHistoryItems(with tagId: String)
}

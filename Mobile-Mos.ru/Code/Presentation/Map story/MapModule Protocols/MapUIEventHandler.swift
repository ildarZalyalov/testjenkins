//
//  MapUIEventHandler.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 13.04.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Протокол объекта, который хочет реагировать на события изменения интерфейса на карте
protocol MapUIEventHandler: class {
    
    /// Обновить лайаут категоризованных тегов с константой
    ///
    /// - Parameter constant: константа для обновления
    func updateCategorizedTagsLayout(with constant: CGFloat)
    
    /// Отобразить индикатор
    func showLoadingIndicator()
    
    /// Спрятать индикатор
    func hideLoadingIndicator()
    
    /// Отобразить плейсхолдеры категоризованных тегов
    func showCategorizedTagsPlaceholders()
    
    /// Спрятать плейсхолдеры категоризованных тегов
    func hideCategorizedTagsPlaceholders()
    
    /// Отобразить поиск по тегам
    func showSearchModule()
    
    /// Спрятать модуль поиска
    func hideSearchModule()
    
    /// Сложить модуль поиска
    func foldSearchModule(with foldedHeight: CGFloat)
    
    /// Разложить модуль поиска
    func unfoldSearchModule(with unfoldHeight: CGFloat)
    
    /// Поле поиска выбрано/активировано
    func didSelectSearchField()
}

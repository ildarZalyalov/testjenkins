//
//  CategorizedTagsDelegate.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 25.08.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Протокол отвечающий за передачу данных в категоризованные теги
protocol CategorizedTagsDelegate: class {
    
    /// Обновить истории
    func updateHistoryItems(with tagId: String)
    
    /// Обновить избранное
    func updateFavoritedItems()
    
    /// Сохранить последний выбранный датасет
    func saveLastSelectedDataset(_ dataset: MapItemDataset)
    
    /// Обновить лайаут тегов
    func reloadTagsLayout()
}

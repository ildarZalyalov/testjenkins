//
//  MapItemInfoDataDelegate.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 18.01.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Делегат данных карточки объекта
protocol MapItemInfoDataDelegate: class {
    /// Расстояние снизу для карточки
    var bottomInset: CGFloat { get }
}

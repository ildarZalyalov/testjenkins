//
//  SearchModuleDelegate.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 23.08.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Делегат модуля поиска
protocol SearchModuleDelegate: class {
    
    /// Отобразить клавиатуру
    func showKeyboard()
    
    /// Очистить текст в поисковой строке
    func clearSearchFieldText()
}

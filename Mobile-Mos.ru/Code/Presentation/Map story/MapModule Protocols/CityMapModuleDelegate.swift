//
//  CityMapModuleDelegate.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 24.11.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Делегат контроллера CityMapModuleController
protocol CityMapModuleDelegate: class {
    
    /// Получили объект конфигурации
    ///
    /// - Parameter object: объект конфигурации
    func didReceivedConfigurationObject(_ object: Any)
}

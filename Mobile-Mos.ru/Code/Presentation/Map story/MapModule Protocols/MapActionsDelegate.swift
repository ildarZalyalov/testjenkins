//
//  MapActionsDelegate.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 29.11.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Делегат модуля карты который умеет реагировать на действие модуля карт
protocol MapActionsDelegate: class {
    
    func mapCameraDidMove()
}

//
//  CityMapModuleConfigurator.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 01.03.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import UIKit
import Pulley

class CityMapModuleConfigurator: BaseModuleConfigurator {
    let drawerCornerRadius: CGFloat = 10
    
    override func configureModule(for view: UIViewController) {
        
        let pulleyController = self.viewController as! PulleyViewController
        let cityMapModuleController = pulleyController as! CityMapViewController
        
        pulleyController.drawerCornerRadius = drawerCornerRadius
        
        pulleyController.loadViewIfNeeded()
        
        let mapController  = pulleyController.primaryContentViewController as! MapViewController
        
        let mapPresenter = mapController.output as! MapPresenter
        let mapRouter = mapPresenter.router as! MapRouter
        
        mapRouter.pulley = pulleyController
        cityMapModuleController.cityMapModuleDelegate = mapPresenter
    }
}

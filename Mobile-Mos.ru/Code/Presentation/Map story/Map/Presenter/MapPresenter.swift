//
//  MapPresenter.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 21.02.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import Pulley

class MapPresenter: MapViewOutput, MapInteractorOutput, MapUIEventHandler, MapDataPresenter, CityMapModuleDelegate, OverlapsManagerDelegate {
    
    weak var view: MapViewInput!
    weak var searchModuleDelegate: SearchModuleDelegate?
    weak var categorizedTagsDelegate: CategorizedTagsDelegate?
    weak var mapActionsDelegate: MapActionsDelegate?
    weak var mapItemInfoDataDelegate: MapItemInfoDataDelegate?
    
    var router: MapRouterInput!
    var interactor: MapInteractorInput!
    var selectedDataset: MapItemDataset!
    var lastDataStructureId: String?
    var zoomOnPoints: Bool = false
    
    //MARK: MapViewOutput methods
    
    var locationServicesAreAvailable: Bool? {
        return interactor.locationServicesAreAvailable
    }
    
    var viewportBottomInset: CGFloat {
        return mapItemInfoDataDelegate?.bottomInset ?? 0
    }
    
    func didSelectMapPin(with mapItem: MapItem) {
       mapItemInfoDataDelegate = router.showMapItemDetail(for: mapItem, dataset: selectedDataset)
    }
    
    func didSelectFavoriteMapPin(with mapItem: MapItem) {
     
        guard let dataset = interactor.findDataset(contains: mapItem) else { return }
    
       mapItemInfoDataDelegate = router.showMapItemDetail(for: mapItem, dataset: dataset)
    }
    
    func didPressFindMe() {
        
        if let locationServicesAreAvailable = interactor.locationServicesAreAvailable {
            
            if locationServicesAreAvailable {
                interactor.findUserGeoPosition()
            }
            else {
                router.showGeolocationAlert()
            }
        }
        else {
            interactor.requestLocationServicesAuthorization()
        }
    }
    
    func didPressOverlaps() {
        interactor.obtainOverlaps()
        categorizedTagsDelegate?.reloadTagsLayout()
    }
    
    func refreshCategorizedTagsLayout() {
        categorizedTagsDelegate?.reloadTagsLayout()
    }
    
    func didPressOverlapsButton() {
        searchModuleDelegate?.clearSearchFieldText()
    }
    
    func didChangeCameraPosition() {
        mapActionsDelegate?.mapCameraDidMove()
        router.hideOverlapsDetail()
    }
    
    func willPerformSegue(with identifier: String) {
        router.willShowEmbeddedModule(with: identifier, configurationObject: self)
    }
    
    func configureSearchTagsDelegate(with delegate: SearchModuleDelegate?) {
        searchModuleDelegate = delegate
    }
    
    func configureCategorizedTagsDelegate(with delegate: CategorizedTagsDelegate?) {
        categorizedTagsDelegate = delegate
    }
    
    func updateFavoritedItems() {
        categorizedTagsDelegate?.updateFavoritedItems()
    }
    
    func didSelectDataset(with datasetId: String) {
        interactor.obtainMapItems(with: datasetId)
    }
    
    func checkIfMapObjectFavorited(_ mapObject: YMKMapObject?) {
        interactor.checkIfMapObjectFavorited(mapObject)
    }
    
    func didPressOverlapsWithoutRequest() {
        interactor.cancelAllRequest()
    }
    
    //MARK: MapInteractorOutput
    
    func didFinishObtainingMapItems(result: MapItemObtainMultipleResult) {

        switch result {
        case .success(let items):
            
            view.displayObjectsOnMap(with: items, and: zoomOnPoints)
            categorizedTagsDelegate?.saveLastSelectedDataset(selectedDataset)
            
        case .failure(let error):
            router.showRequestErrorAlert(with: error.localizedDescription)
            view.hideLoadingIndicator(completion: nil)
            categorizedTagsDelegate?.reloadTagsLayout()
        }
    }
    
    func didFinishObtainingOverlaps(result: MapOverlapGeoObtainMultipleResult) {
        
        switch result {
        case .success(let models):
            
            view.showOverlaps(with: models)
            
        case .failure(let error):
            router.showRequestErrorAlert(with: error.localizedDescription)
           
            view.turnOffOverlaps()
            view.hideLoadingIndicator(completion: nil)
        }
    }
    
    func didFinishFindingUserLocation(with location: Location?) {
        if let userLocation = location {
            view.showUserLocation(with: userLocation)
        }
        else {
            router.showGeolocationErrorAlert()
        }
    }
    
    func didFinishSavingDatasetToHistory(with result: DataBaseOperationResult) {
        
        switch result {
        case .error(let error):
            print("Ошибка сохранения данных: \(error.localizedDescription)")
        default:
            return
        }
    }
    
     func didFinishCheckingIfMapItemFavorited(with isFavorited: Bool, and mapObject: YMKMapObject) {
        view.didKnowMapItemIsFavorited(with: mapObject, and: isFavorited)
    }
    
    //MARK: - MapDataPresenter
    
    func updateMap(with dataset: MapItemDataset) {
        
        interactor.cancelAllRequest()
        
        let datasetId = String(dataset.itemId)
        
        lastDataStructureId = datasetId
        selectedDataset = dataset
        zoomOnPoints = true
        
        view.displayLoadingIndicator()
        interactor.obtainMapItems(with: datasetId)
        view.updateCurrentDatasetName(with: dataset.name)
    }
    
    func updateMap(with favoriteDataSet: FavoritedDataset) {
        
        interactor.cancelAllRequest()
        view.hideLoadingIndicator(completion: nil)
        view.updateCurrentDatasetName(with: favoriteDataSet.name)
        view.displayStarredObjectsOnMap(with: favoriteDataSet.favoritedItems, zoomOnPoints: true)
    }
    
    func updateMap(with favorites: [MapItem]) {
        
        interactor.cancelAllRequest()
        view.hideLoadingIndicator(completion: nil)
        view.displayStarredObjectsOnMap(with: favorites, zoomOnPoints: false)
    }
    
    func refreshHistoryItems(with tagId: String) {
        view.displaySearchLoadingIndicator()
        categorizedTagsDelegate?.updateHistoryItems(with: tagId)
    }
    
    func clearMap() {
        view.clearMapFromObjects()
    }
    
    //MARK: - MapUIEventHandler
    
    func updateCategorizedTagsLayout(with constant: CGFloat) {
        view.updateDatasetSelectionHeight(with: constant)
    }
    
    func showLoadingIndicator() {
        view.displayLoadingIndicator()
    }
    
    func hideLoadingIndicator() {
        view.hideLoadingIndicator(completion: nil)
    }
    
    func showCategorizedTagsPlaceholders() {
        view.animateTagsPlaceholders()
    }
    
    func hideCategorizedTagsPlaceholders() {
        view.stopTagsPlaceholdersAnimation()
    }
    
    func showSearchModule() {
        
        view.showDatasetSearching()
        searchModuleDelegate?.showKeyboard()
    }
    
    func hideSearchModule() {
        
        view.turnOffOverlaps()
        view.hideDatasetSearching()
    }
    
    func foldSearchModule(with foldedHeight: CGFloat) {
        view.foldSearchModule(with: foldedHeight)
    }
    
    func unfoldSearchModule(with unfoldHeight: CGFloat) {
        interactor.cancelAllRequest()
        view.unfoldSearchModule(with: unfoldHeight)
    }
    
    func didSelectSearchField() {
        view.turnOffOverlaps()
    }
    
    //MARK: - CityMapModuleDelegate
    
    func didReceivedConfigurationObject(_ object: Any) {
        
        if let dataSet = object as? MapItemDataset {
            
            updateMap(with: dataSet)
            interactor.saveDatasetToHistory(dataSet)
        }
    }
    
    //MARK: - OverlapsManagerDelegate
    
    func didSelectOverlapPin(with selectedPinData: Any) {
        guard let data = selectedPinData as? String else { return }
        
        router.showOverlapItemDetail(with: data)
    }
    
    func didOverlapCameraChanged() {
        router.hideOverlapsDetail()
    }
    
    func didHideOverlaps() {
        
        categorizedTagsDelegate?.updateFavoritedItems()
        router.hideOverlapsDetail()
    }
}

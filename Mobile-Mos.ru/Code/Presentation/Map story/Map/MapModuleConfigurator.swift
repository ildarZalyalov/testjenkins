//
//  MapModuleConfigurator.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 21.02.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import UIKit
import Pulley

class MapModuleConfigurator: BaseModuleConfigurator {
    
    override func configureModule(for view: UIViewController) {
        
        let viewController = view as! MapViewController
        let presenter = MapPresenter()
        let interactor = MapInteractor()
        let router = MapRouter()
        
        let serviceBuilder = UIApplication.shared.serviceBuilder
        
        let logoSelector = YMKMapLogoTypeSelector()
        logoSelector.preferredLanguages = NSLocale.preferredLanguages
        
        let locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        
        let iconConfigurator = MapPinIconConfigurator()
        let userPinIconConfigurator = UserPinIconConfiguration()
        let storyboardFactoryImplementation = StoryboardFactoryImplementation()
        let alertsFactory = CommonAlertsFactoryImplementation()
        
        let mapItemsService = serviceBuilder.getMapItemsService()
        let nounCountStringFormatter = NounCountStringFormatterImplementation()
        let overlapsManager = OverlapsManagerImplementation.getOverlapsManager()
        let overlapsSerivce = serviceBuilder.getOverlapsService()
        
        viewController.output = presenter
        viewController.mapLogoSelector = logoSelector
        viewController.mapPinIconConfigurator = iconConfigurator
        viewController.userPinIconConfigurator = userPinIconConfigurator
        viewController.clusterManager = YandexClusterManagerImplementation()
        viewController.nounCountStringFormatter = nounCountStringFormatter
        viewController.overlapsManager = overlapsManager
        
        presenter.view = viewController
        presenter.interactor = interactor
        presenter.router = router
        
        interactor.output = presenter
        interactor.mapItemsService = mapItemsService
        interactor.locationManager = locationManager
        interactor.overlapsSerivce = overlapsSerivce
        
        router.storyboardFactory = storyboardFactoryImplementation
        router.viewController = viewController
        router.alertTransitionHandler = viewController
        router.alertFactory = alertsFactory
    }
}

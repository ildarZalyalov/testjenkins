//
//  MapRouterInput.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 21.02.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol MapRouterInput: class {
    

    /// Отобразить детальную карточку объекта карты
    ///
    /// - Parameters:
    ///   - mapItem:  объект карты
    ///   - dataset: датасет, в котором содержится объект карты
    /// - Returns: делегат данных карточки объекта
    func showMapItemDetail(for mapItem: MapItem, dataset: MapItemDataset) -> MapItemInfoDataDelegate?
    
    /// Отобразить детальную карточку перекрытия
    func showOverlapItemDetail(with overlapId: String)
    
    /// Закрыть детальную карточку перекрытия
    func hideOverlapsDetail()
    
    /// Выполнить ДО того, как покажется встроенный модуль
    func willShowEmbeddedModule(with identifier: String, configurationObject: Any)
    
    /// Отобразить алерт геолокации
    func showGeolocationAlert()
    
    /// Отобразить алерт с ошибкой нахождения геолокации
    func showGeolocationErrorAlert()
    
    /// Отобразить алерт с ошибкой запроса
    ///
    /// - Parameter errorBody: текст ошибки запроса
    func showRequestErrorAlert(with errorBody: String)
}

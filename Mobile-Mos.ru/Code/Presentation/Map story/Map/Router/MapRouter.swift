//
//  MapRouter.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 21.02.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import UIKit
import Pulley

class MapRouter: MapRouterInput, InfoEventHandler {
    
    weak var viewController: MapViewController!
    weak var alertTransitionHandler: UIViewController!
    weak var pulley: PulleyViewController!
    
    var alertFactory: CommonAlertsFactory!
    var storyboardFactory: StoryboardFactory!
    
    let embeddedCategorizedTagsIdentifier = "categorizedTags"
    let embeddedSearchingTagsIdentifier = "searchTags"
    
    func willShowEmbeddedModule(with identifier: String, configurationObject: Any) {
        
        if identifier == embeddedCategorizedTagsIdentifier { 
            
            viewController.addModuleConfiguration(configurationClosure: { [weak self] (controller) in
                
                self?.viewController.output.configureCategorizedTagsDelegate(with: controller as? CategorizedTagsDelegate)
                controller.configureModule(with: configurationObject)
                
            }, for: embeddedCategorizedTagsIdentifier)
        }
        else if identifier == embeddedSearchingTagsIdentifier {
            
            viewController.addModuleConfiguration(configurationClosure: { [weak self] (controller) in
                
                self?.viewController.output.configureSearchTagsDelegate(with: controller as? SearchModuleDelegate)
                controller.configureModule(with: configurationObject)
                
            }, for: embeddedSearchingTagsIdentifier)
        }
    }
    
    func showMapItemDetail(for mapItem: MapItem, dataset: MapItemDataset) -> MapItemInfoDataDelegate? {
        
        let mapItemInfoController = storyboardFactory.getStoryboard(with: .mapitemInfo).instantiateInitialViewController()!
        
        if pulley.drawerContentViewController != mapItemInfoController {
            
            let drawerContent = mapItemInfoController
            pulley.setDrawerContentViewController(controller: drawerContent, animated: false)
        }
        
        guard let configurableController = pulley.drawerContentViewController as? ConfigurableModuleController else { return nil}
        
        let data: InfoEventDataInput = (item: mapItem, dataset: dataset, eventHandler: self)
        
        configurableController.configureModule(with: data)
        
        pulley.setDrawerPosition(position: .partiallyRevealed, animated: true)
        
        return configurableController as? MapItemInfoDataDelegate
    }
    
    func showOverlapItemDetail(with overlapId: String) {
        
        let overlapItemInfoController = storyboardFactory.getStoryboard(with: .map).instantiateViewController(for: .overlapItemInfo)
        
        if pulley.drawerContentViewController != overlapItemInfoController {
           
            let drawerContent = overlapItemInfoController
            pulley.setDrawerContentViewController(controller: drawerContent, animated: false)
        }
     
        guard let configurableController = pulley.drawerContentViewController as? ConfigurableModuleController else { return }
        
        let data: OverlapInfoEventDataInput = (overlapId: overlapId, eventHandler: self)
        
        configurableController.configureModule(with: data)
        pulley.setDrawerPosition(position: .partiallyRevealed, animated: true)
    }
    
    func hideOverlapsDetail() {
        pulley.setDrawerPosition(position: .collapsed, animated: true)
    }
    
    func showGeolocationAlert() {
        let alert = alertFactory.getLocationServicesNotAuthorizedAlert()
        alertTransitionHandler.present(alert, animated: true, completion: nil)
    }
    
    func showGeolocationErrorAlert() {
        let alert = alertFactory.getFailedToGetUserLocationAlert()
        alertTransitionHandler.present(alert, animated: true, completion: nil)
    }
    
    func showRequestErrorAlert(with errorBody: String) {
        
        let alert = alertFactory.getErrorAlert(with: errorBody)
        alertTransitionHandler.present(alert, animated: true, completion: nil)
    }
    
    //MARK: InfoEventHandler
    
    func setupPulleyViewController(to configurablePulley: PulleyConfigurator?) {
        configurablePulley?.setUpPulleyViewController(with: pulley)
    }
    
    func setupMapActionsDelegate(with delegate: MapActionsDelegate?) {
        
        let presenter = viewController.output as? MapPresenter
        presenter?.mapActionsDelegate = delegate
    }
}

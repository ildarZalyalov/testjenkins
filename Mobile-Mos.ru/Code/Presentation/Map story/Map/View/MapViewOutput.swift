//
//  MapViewOutput.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 21.02.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol MapViewOutput: class {
    
    /// Включен ли сервис геолокации у пользователя на устройстве
    var locationServicesAreAvailable: Bool? { get }
    
    /// Отступ снизу до видимой части карты
    var viewportBottomInset: CGFloat { get }
    
    /// Точка на карте выбрана
    ///
    /// - Parameter mapItem: объект, содержащий информацию о точке
    func didSelectMapPin(with mapItem: MapItem)
    
    /// Избранная точка на карте выбрана
    ///
    /// - Parameter mapItem: объект, содержащий информацию о точке
    func didSelectFavoriteMapPin(with mapItem: MapItem)
    
    /// Выбран датасет
    ///
    /// - Parameter datasetId: id датасета
    func didSelectDataset(with datasetId: String)
    
    /// Кнопка обноружения геолокации нажата
    func didPressFindMe()
    
    /// перекрытия активированы
    func didPressOverlaps()
    
    /// Перекрытия активированы без запроса в сеть
    func didPressOverlapsWithoutRequest()
    
    /// Кнопка перекрытия нажата
    func didPressOverlapsButton()
    
    /// Местоположение камеры изменено
    func didChangeCameraPosition()
    
    func willPerformSegue(with identifier: String)
    
    /// Обновить избранные
    func updateFavoritedItems()
    
    /// Сконфигурировать делегат модуля поиска
    ///
    /// - Parameter delegate: делегат модуля поиска
    func configureSearchTagsDelegate(with delegate: SearchModuleDelegate?)
    
    /// Сконфигурировать делегат модуля категоризованных тэгов
    ///
    /// - Parameter delegate: делегат модуля категоризованных тэгов
    func configureCategorizedTagsDelegate(with delegate: CategorizedTagsDelegate?)
    
    /// Проверить является ли точка карты избранной точкой
    ///
    /// - Parameter mapObject: точка на карте
    func checkIfMapObjectFavorited(_ mapObject: YMKMapObject?)
    
    /// Обновить лайаут категеризованных тегов
    func refreshCategorizedTagsLayout()

}

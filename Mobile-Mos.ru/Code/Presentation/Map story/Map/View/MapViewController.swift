//
//  MapViewController.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 21.02.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import UIKit
import MapKit
import Pulley

class MapViewController: BaseViewController,
                        MapViewInput,
                        YMKMapCameraListener,
                        YMKMapObjectTapListener,
                        YMKMapObjectCollectionListener,
                        YMKTrafficDelegate,
                        PulleyPrimaryContentControllerDelegate,
                        CKClusterManagerDelegate {
    
    //MARK: - Constants
    
    let zoomForUserLocation: Float = 15
    let zoomForMoscow: Float = 8
    let iconScaleForPlacemark = 1.0
    let iconForPlacemarkOnMap = #imageLiteral(resourceName: "pinIcon")
    let iconForSelectedPlacemark = #imageLiteral(resourceName: "selectedPinIcon")
    let iconForStarredPlacemark = #imageLiteral(resourceName: "pin-stared")
    let iconForSelectedStarredPlacemark = #imageLiteral(resourceName: "pin-selected-stared")
    let iconForAvailableGeo = #imageLiteral(resourceName: "findMe")
    let iconForNonAvailableGeo = #imageLiteral(resourceName: "findMe-disabled")
    let iconForGreenTraffic = #imageLiteral(resourceName: "traffic-green")
    let iconForYellowTraffic = #imageLiteral(resourceName: "traffic-yellow")
    let iconForRedTraffic = #imageLiteral(resourceName: "traffic-red")
    let iconForDisabledTraffic = #imageLiteral(resourceName: "traffic-disabled")
    
    let foldDatasetSearchingDuration = 0.3
    let minimumTrafficLevel = 2
    let maxTrafficLevel = 10
    let middleTrafficLevel:ClosedRange<Int> = 2 ... 4
    
    let datasetSelectionViewAnimationDuration = 0.2
    let currentDatasetNameViewAnimationDuration = 0.2
    
    let timeoutBeforeHideLoader = 0.3
    let timeOutIntervalForProgress = networkingTimeoutIntervalForResource
    
    //MARK: - Variables
    
    var isContainerWithSearchFolded = false
    var isItemDetailShowing = false
    var isStarredObjectsShowing = false
    var isCameraFinishMoving = false
    var isDrawerCollapsed = true
    var isOverlapButtonPressed = false
    
    var mapView: YMKMapView!
    weak var selectedPlaceMarkObject: YMKPlacemarkMapObject?
    let geoButtonTopConstraintDecrementer: CGFloat = 20
    let mapButtonsBottomConstraintDecrementer: CGFloat = 50
    var containerWithSearchDefaultHeight: CGFloat!
    var currentZoom: Float = 0
    
    var defaultIcon: UIImage = #imageLiteral(resourceName: "pinIcon")
    var selectedIcon: UIImage = #imageLiteral(resourceName: "pinIcon")
    
    //MARK: - IBOutlets
    
    @IBOutlet weak var trafficJamLabel: UILabel!
    @IBOutlet weak var geoButton: UIButton!
    @IBOutlet weak var trafficJamButton: UIButton!
    @IBOutlet weak var overlapsButton: UIButton!
    
    @IBOutlet weak var geoButtonTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var yandexLogoView: UIImageView!
    @IBOutlet weak var yandexLogoViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var datasetSelectionVisualEffect: UIVisualEffectView!
    @IBOutlet weak var datasetSelectionHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var lastTagPlaceholderView: UIView!
    @IBOutlet weak var placeholdersStackView: UIStackView!
    @IBOutlet weak var currentDatasetNameView: UIVisualEffectView!
    @IBOutlet weak var currentDatasetNameBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomDatasetNameLabel: UILabel!
    @IBOutlet weak var datasetSelectionBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var containerWithSearchModule: UIView!
    @IBOutlet weak var containerWithSearchBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var containerWithSearchHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var loadingProgress: LoadingTimeoutProgressView!
    @IBOutlet weak var searchLoadingProgress: LoadingTimeoutProgressView!
    
    @IBOutlet weak var mapButtonsContainerBottomConstraint: NSLayoutConstraint!
    
    //MARK: - Инъектированные ссылки
    
    var mapLogoSelector: YMKMapLogoTypeSelector!
    var mapPinIconConfigurator: MapPinIconConfiguratorProtocol!
    var userPinIconConfigurator: UserPinIconConfigurator!
    var clusterManager: YandexClusterManager!
    var overlapsManager: OverlapsManager!
    var nounCountStringFormatter: NounCountStringFormatter!
    var output: MapViewOutput!
    
    let chatTitleView = SubtitledNavigationTitleView.loadNib()
    
    //MARK: - Жизненный цикл контроллера
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if UIDevice.current.hasCompactScreen {
            lastTagPlaceholderView.isHidden = true
        }
        
        containerWithSearchDefaultHeight = containerWithSearchHeightConstraint.constant
        
        currentDatasetNameView.isHidden = true
        
        addMapView()
        configureClusterManager()
        refreshMapViewLogo()
        refreshGeopositionState()
        addObserverToReturningFromForeground()
        animateTagsPlaceholders()
        
        overlapsManager.configure(with: mapView, delegate: output as? OverlapsManagerDelegate)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        addSubtitleViewToNavigationBar()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    //MARK: - Перегрузка полей/методов класса UIViewController
    
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return mapConversation.style.isStatusBarDark ? .default : .lightContent
    }
    
    override var preferredStatusBarUpdateAnimation: UIStatusBarAnimation {
        return .fade
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        
        output.willPerformSegue(with: identifier)
        
        return true
    }
    
    //MARK: - Custom methods
    
    func addMapView() {
        configureMapView()
        mapView.updateMapNightMode()
        mapView.updateMapTrafficJam()
    }
    
    func configureMapView() {
        
        mapView = YMKMapView(frame: view.frame)
        mapView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        mapView.map.mapType = .vectorMap
        mapView.map.isRotateGesturesEnabled = false
        
        userPinIconConfigurator.configureUserPinIcon(with: mapView.map.userLocationLayer)
        
        mapView.moveCameraPosition(to: moscowLocation.latitude, longitute: moscowLocation.longitude, zoom:zoomForMoscow)
        mapView.moveCameraPosition(to: moscowLocation.latitude, longitute: moscowLocation.longitude, zoom: zoomForMoscow)
        
        mapView.map.trafficLayer?.addTrafficListener(withTrafficListener: self)
        
        mapView.map.addCameraListener(with: self)
        mapView.map.mapObjects?.addListener(with: self)
        
        view.insertSubview(mapView, at: 0)
    }
    
    func configureClusterManager() {
        
        clusterManager.configure(with: mapView)
        mapView.clusterManager.delegate = self
    }
    
    func refreshMapViewLogo() {
        let logoType = mapLogoSelector.logoType(for: mapView.map)
        yandexLogoView.image = UIImage(named: logoType.rawValue)
    }
    
    func configureImage(of placemarkMapObject: YMKPlacemarkMapObject?, with image: UIImage){
        
        guard let mark = placemarkMapObject else { return }
        
        let iconStyle = YMKIconStyle(scale: iconScaleForPlacemark)
        mapPinIconConfigurator.configure(with: mark, iconStyle: iconStyle, icon: image)
    }
    
    func addObserverToReturningFromForeground() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.refreshGeopositionState), name: .UIApplicationWillEnterForeground, object: nil)
    }
    
    @objc
    func refreshGeopositionState() {
        if let servicesAvailable = output.locationServicesAreAvailable, servicesAvailable {
            geoButton.setImage(iconForAvailableGeo, for: .normal)
        }
        else {
            geoButton.setImage(iconForNonAvailableGeo, for: .normal)
        }
    }
    
    func setupPinIcon(with placemark: YMKPlacemarkMapObject) {
        
        guard placemark.isValid, let mapItem = placemark.userData as? MapItem
            else { return }
        
        defaultIcon  =  mapItem.isFavorited ? iconForStarredPlacemark : iconForPlacemarkOnMap
        selectedIcon =  mapItem.isFavorited ? iconForSelectedStarredPlacemark : iconForSelectedPlacemark
        
        if let selectedObject = selectedPlaceMarkObject {
            configureImage(of: selectedObject, with: defaultIcon)
        }
        
        selectedPlaceMarkObject = placemark
        configureImage(of: placemark, with: selectedIcon)
    }
    
    func updatePinIcons(with selectedMapObject: YMKMapObject) {
        
        guard let mapItem = selectedMapObject.userData as? MapItem else { return }
        
        defaultIcon  =  mapItem.isFavorited ? iconForStarredPlacemark : iconForPlacemarkOnMap
        selectedIcon =  mapItem.isFavorited ? iconForSelectedStarredPlacemark : iconForSelectedPlacemark
    }
    
    func setupPinDetailInfo(with mapItemModel: MapItem) {
        
        if !isItemDetailShowing {
            
            if defaultIcon.hash == iconForStarredPlacemark.hash {
                output.didSelectFavoriteMapPin(with: mapItemModel)
            }
            else {
                output.didSelectMapPin(with: mapItemModel)
            }
            
            isItemDetailShowing = true
            
        } else {
            
            if defaultIcon.hash == iconForStarredPlacemark.hash {
                output.didSelectFavoriteMapPin(with: mapItemModel)
            }
            else {
                output.didSelectMapPin(with: mapItemModel)
            }
        }
    }
    
    func updateTitleView(with subtitleString: String) {
        
        guard !subtitleString.isEmpty else {
            chatTitleView.subtitle = nil
            return
        }
        
        chatTitleView.subtitle = subtitleString
    }
    
    func addSubtitleViewToNavigationBar() {
        
        guard let controller = self.parent as? PulleyViewController else { return }
        
        let titleTextAttributes = [
            NSAttributedStringKey.font : UIFont(customName: .graphikLCGBold, size: 15) as Any,
            NSAttributedStringKey.foregroundColor : UIColorPalette.conversationsPageTitleColor
        ]
        let subtitleTextAttributes = [
            NSAttributedStringKey.font : UIFont(customName: .graphikLCGMedium, size: 12) as Any,
            NSAttributedStringKey.foregroundColor : UIColorPalette.conversationsPageTitleColor.withAlphaComponent(0.8)
        ]
        
        chatTitleView.titleTextAttributes = titleTextAttributes
        chatTitleView.subtitleTextAttributes = subtitleTextAttributes
        chatTitleView.title = controller.navigationItem.title ?? ""
        
        controller.navigationController?.navigationBar.topItem?.titleView = chatTitleView
    }
    
    //MARK: - MapViewInput
    
    func showOverlaps(with overlaps: [OverlapGeo]) {
        
        hideLoadingIndicator(completion: nil)
        
        updateCurrentDatasetName(with: StringsHelper.overlapsPlaceholderText)
        showCurrentDatasetName()
        
        guard !overlapsManager.hasOverlaps else {
            
            overlapsManager.showOverlapsOnMap(with: mapView.cameraPosition.zoom, overlaps: overlaps)
            return
        }
        overlapsManager.showOverlapsOnMap(with: mapView.cameraPosition.zoom, overlaps: overlaps)
    }
    
    func turnOffOverlaps() {
        
        overlapsButton.isSelected = false
        isOverlapButtonPressed = false
        overlapsManager.hideOverlaps()
    }
    
    func displayObjectsOnMap(with mapItems: [MapItem], and zoomOnPoints: Bool) {
        
        guard !mapItems.isEmpty, !overlapsManager.hasOverlapsObjectsVisible else { return }
        
        let itemsCount = mapItems.count
        
        updateTitleView(with: StringsHelper.findedSubtitle + " \(itemsCount) " + nounCountStringFormatter.getPlaceWordForCount(count: UInt(itemsCount)))
        
        selectedPlaceMarkObject = nil
        clearMapFromObjects()
        
        isStarredObjectsShowing = false
        
        var points = [YMKPoint]()
        
        if let item = mapItems.first, item.isFavorited {
            isStarredObjectsShowing = true
        }
        
        for mapItem in mapItems {
            
            let point = YMKPoint(latitude: mapItem.location.latitude, longitude: mapItem.location.longitude)
            
            let uniqHash = point.latitude + point.longitude
            
            clusterManager.dataByPoint[uniqHash] = mapItem
        
            points.append(point)
        }
        
        mapView.clusterManager.addAnnotations(points)
        
        guard zoomOnPoints else {
            hideLoadingIndicator(completion: nil)
            return
        }
        
        hideLoadingIndicator(completion: { [weak self] in
            self?.showCurrentDatasetName()
            
            self?.mapView.updateCameraPosition(with: points)
        })
    }
    
    func displayStarredObjectsOnMap(with mapItems: [MapItem], zoomOnPoints: Bool) {
        
        guard !mapItems.isEmpty else { return }
        
        selectedPlaceMarkObject = nil

        clearMapFromObjects()
        
        var points = [YMKPoint]()
        
        for mapItem in mapItems {
            
            let point = YMKPoint(latitude: mapItem.location.latitude, longitude: mapItem.location.longitude)
            
            let placemarkMapObject = mapView.addMapObject(with: point, andObjectData: mapItem)
            configureImage(of: placemarkMapObject, with: iconForStarredPlacemark)
            
            points.append(point)
        }
        
        if zoomOnPoints {
            showCurrentDatasetName()
            mapView.updateCameraPosition(with: points)
        }
        
        isStarredObjectsShowing = true
    }
    
    func hideLoadingIndicator(completion: (() -> Void)?) {
        
        let hideTypingHandler = { [weak self] in
            
            guard let strongSelf = self else { return }
            
            strongSelf.loadingProgress.completeProgressTimer()
            strongSelf.searchLoadingProgress.completeProgressTimer()
            
            DispatchQueue.main.asyncAfter(deadline: .now() + strongSelf.timeoutBeforeHideLoader, execute: {
                completion?()
            })
        }
        
        DispatchQueue.main.async(execute: hideTypingHandler)
    }
    
    func displayLoadingIndicator() {
        loadingProgress.startProgressTimer(with: timeOutIntervalForProgress)
    }
    
    func displaySearchLoadingIndicator() {
        searchLoadingProgress.startProgressTimer(with: timeOutIntervalForProgress)
    }
    
    func animateTagsPlaceholders() {
        
        placeholdersStackView.isHidden = false
        placeholdersStackView.startLoadingAnimation()
    }
    
    func stopTagsPlaceholdersAnimation() {
        
        placeholdersStackView.stopLoadingAnimation()
        placeholdersStackView.isHidden = true
    }
    
    func displayMapItemDetailInfo(with item: MapItem) {
        output.didSelectMapPin(with: item)
    }
    
    func setupBottomUIElementsPosition(with distance: CGFloat) {
        yandexLogoViewBottomConstraint.constant = distance
    }
    
    func showUserLocation(with location: Location) {
        mapView.moveCameraPosition(to: location.latitude, longitute: location.longitude, zoom: zoomForUserLocation)
    }
    
    func updateDatasetSelectionHeight(with height: CGFloat) {
        
        datasetSelectionHeightConstraint.constant = height
        geoButtonTopConstraint.constant -= geoButtonTopConstraintDecrementer
        mapButtonsContainerBottomConstraint.constant -= mapButtonsBottomConstraintDecrementer
        
        view.layoutIfNeeded()
    }
    
    func updateCurrentDatasetName(with name: String) {
        bottomDatasetNameLabel.text = name
    }
    
    func showCurrentDatasetName() {
        
        guard containerWithSearchModule.isHidden, currentDatasetNameView.isHidden else {
            isOverlapButtonPressed = false
            return
        }
        
        view.layoutIfNeeded()
        UIView.animate(withDuration: datasetSelectionViewAnimationDuration, delay: 0, options: [.curveEaseIn], animations: {[weak self] in
            
            guard let strongSelf = self else { return }
            
            strongSelf.datasetSelectionBottomConstraint.constant -= strongSelf.view.height / 2
            strongSelf.view.layoutIfNeeded()
        }) {[weak self] (isFinished) in
            
            guard let strongSelf = self else { return }
            
            strongSelf.datasetSelectionVisualEffect.isHidden = true
            strongSelf.currentDatasetNameView.isHidden = false
            
            UIView.animate(withDuration: strongSelf.currentDatasetNameViewAnimationDuration, delay: 0, options: [.curveEaseOut], animations: {
                
                strongSelf.currentDatasetNameBottomConstraint.constant = 0
                strongSelf.view.layoutIfNeeded()
                strongSelf.isOverlapButtonPressed = false
                
            }, completion: nil)
        }
        view.setNeedsLayout()
    }
    
    func showDatasetSelection() {
        
        guard !currentDatasetNameView.isHidden else {
            isOverlapButtonPressed = false
            return
        }
        
        view.layoutIfNeeded()
        
        UIView.animate(withDuration: currentDatasetNameViewAnimationDuration, delay: 0, options: [.curveEaseInOut], animations: {[weak self] in
            
            guard let strongSelf = self else { return }
            
            strongSelf.currentDatasetNameBottomConstraint.constant -= strongSelf.currentDatasetNameView.height
            strongSelf.view.layoutIfNeeded()
            
        }) {[weak self] (isFinished) in
            
            guard isFinished, let strongSelf = self else { return }
            
            strongSelf.currentDatasetNameView.isHidden = true
            strongSelf.datasetSelectionVisualEffect.isHidden = false
            UIView.animate(withDuration: strongSelf.datasetSelectionViewAnimationDuration, delay: 0, options: [.curveEaseIn], animations: {[weak self] in
                guard let strongSelf = self else { return }
                
                strongSelf.datasetSelectionBottomConstraint.constant = 0
                strongSelf.isOverlapButtonPressed = false
                
                strongSelf.view.layoutIfNeeded()
                },completion: nil)
        }
        
    }
    
    func changeTrafficJam() {
        
        guard let trafficLayer = mapView.map.trafficLayer else { return }
        
        let isTrafficJamEnabled = !trafficLayer.isTrafficVisible()
        
        trafficLayer.setTrafficVisibleWithOn(isTrafficJamEnabled)
        
        if !isTrafficJamEnabled {
            trafficJamButton.setImage(iconForDisabledTraffic, for: .normal)
        }
    }
    
    func showDatasetSearching() {
        
        containerWithSearchModule.isHidden = false
        datasetSelectionVisualEffect.isHidden = true
    }
    
    func hideDatasetSearching() {
        
        containerWithSearchModule.isHidden = true
        datasetSelectionVisualEffect.isHidden = false
        
        if isContainerWithSearchFolded {
    
            clearMapFromObjects()
        }
    }
    
    func foldSearchModule(with height: CGFloat) {
        
        isContainerWithSearchFolded = true
        loadingProgress.isHidden = true 
        
        view.layoutIfNeeded()
        UIView.animate(withDuration: foldDatasetSearchingDuration) { [weak self] in
            
            guard let strongSelf = self else { return }
            
            strongSelf.containerWithSearchHeightConstraint.constant = height
            strongSelf.containerWithSearchBottomConstraint.constant = 0
            
            strongSelf.view.layoutIfNeeded()
        }
    }
    
    func unfoldSearchModule(with height: CGFloat) {
        
        isContainerWithSearchFolded = false
        
        searchLoadingProgress.isHidden = true
        clearMapFromObjects()
        updateTitleView(with: "")
        
        view.layoutIfNeeded()
        UIView.animate(withDuration: foldDatasetSearchingDuration) { [weak self] in
            
            guard let strongSelf = self else { return }
            
            strongSelf.containerWithSearchHeightConstraint.constant = strongSelf.containerWithSearchDefaultHeight
            strongSelf.containerWithSearchBottomConstraint.constant = height
            
            strongSelf.view.layoutIfNeeded()
        }
    }
    
    func clearMapFromObjects() {
        mapView.clusterManager.annotations.removeAll()
       
        if isStarredObjectsShowing {
            mapView.clearMapObjects()
        }
    }
    
    func removePointFromMap(_ mapObject: YMKMapObject) {
        mapView.remove(mapObject: mapObject)
    }
    
    func didKnowMapItemIsFavorited(with mapObject: YMKMapObject, and isFavorite: Bool) {
        
        guard !isStarredObjectsShowing else {
            
            if !isFavorite {
                removePointFromMap(mapObject)
            }
            else {
                configureImage(of: selectedPlaceMarkObject, with: iconForStarredPlacemark)
            }
            
            selectedPlaceMarkObject = nil
            
            return
        }
        
        if mapObject.isValid, var mapItem = mapObject.userData as? MapItem, let mark = selectedPlaceMarkObject {
            
            let hash = mapItem.location.latitude + mapItem.location.longitude
            let icon = isFavorite ? iconForStarredPlacemark : iconForPlacemarkOnMap
            
            mapItem.isFavorited = isFavorite
            clusterManager.dataByPoint[hash] = mapItem
            mapObject.userData = mapItem
            
            configureImage(of: mark, with: icon)
            updatePinIcons(with: mapObject)
            
            selectedPlaceMarkObject = nil
        }
    }
    
    //MARK: - CKClusterManagerDelegate
    
    func clustersUpdated() {
        
    }
    
    //MARK: - YMKMapCameraListener
    func onCameraPositionChanged(with map: YMKMap?, cameraPosition: YMKCameraPosition, cameraUpdateSource: YMKCameraUpdateSource, finished: Bool) {
        
        isCameraFinishMoving = false
        
        if finished {
            isCameraFinishMoving = true
            overlapsManager.changeOverlaps(withZoom: cameraPosition.zoom)
        }
        
        if cameraUpdateSource == .gestures  {
            output.didChangeCameraPosition()
        }
        
    }
    
    //MARK: - YMKMapObjectTapListener
    
    func onMapObjectTap(with mapObject: YMKMapObject?, point: YMKPoint) -> Bool {
        
        guard let mapObject = mapObject,
              let mapItemModel = mapObject.userData as? MapItem,
              let mapPlacemarkObject = mapObject as? YMKPlacemarkMapObject,
              mapPlacemarkObject.cluster?.count == 1 || isStarredObjectsShowing || mapPlacemarkObject.cluster == nil
              else { return false }
        
        setupPinIcon(with: mapPlacemarkObject)
        
        setupPinDetailInfo(with: mapItemModel)
        
        mapView.moveCameraPosition(to: mapPlacemarkObject.geometry.latitude, longitute: mapPlacemarkObject.geometry.longitude, zoom: mapView.cameraPosition.zoom, isItemInfoCardOpen: true, viewportBottomInset: output.viewportBottomInset)
        
        return true
    }
    
    //MARK: - YMKMapObjectCollectionListener
    
    func onMapObjectAdded(with mapObject: YMKMapObject?) {
        mapObject?.addTapListener(with: self)
    }
    
    func onMapObjectRemoved(with mapObject: YMKMapObject?) {
        mapObject?.removeTapListener(with: self)
    }
    
    func onMapObjectUpdated(with mapObject: YMKMapObject?) {
        
    }
    
    //MARK: - YMKTrafficDelegate
    
    func onTrafficChanged(with trafficLevel: YMKTrafficLevel?) {
        
        guard let trafficLevel = trafficLevel else {
            return
        }
        
        let level = trafficLevel.level
        
        if level < minimumTrafficLevel {
            
            trafficJamButton.setImage(iconForGreenTraffic, for: .normal)
        }
        else if middleTrafficLevel.contains(level) {
            trafficJamButton.setImage(iconForYellowTraffic, for: .normal)
        }
        else if level <= maxTrafficLevel {
            trafficJamButton.setImage(iconForRedTraffic, for: .normal)
        }
        
        trafficJamLabel.text = String(level)
    }
    
    func onTrafficExpired() {
        trafficJamButton.setImage(iconForDisabledTraffic, for: .normal)
    }
    
    func onTrafficLoading() {}
    
    
    
    //MARK: - PulleyDrawerPositionDelegate
    
    func drawerPositionDidChange(drawer: PulleyViewController, bottomSafeArea: CGFloat) {
        
        switch drawer.drawerPosition {
        case .partiallyRevealed:
            
            isDrawerCollapsed = false
            
        case .collapsed:
            
            if !isDrawerCollapsed {
                
                output.checkIfMapObjectFavorited(selectedPlaceMarkObject)
                output.updateFavoritedItems()
                overlapsManager.hideSelectedOverlap()
                
                isDrawerCollapsed = true
            }
        default :
            return
        }
    }
    
    //MARK: - Buttons Action
    
    @IBAction func trafficJamButtonPressed(_ sender: UIButton) {
        changeTrafficJam()
    }
    
    @IBAction func overlapsButtonPressed(_ sender: UIButton) {
        
        guard !isOverlapButtonPressed else { return }
        
        isOverlapButtonPressed = true
        updateTitleView(with: "")
        
        sender.isSelected = !sender.isSelected
        
        output.refreshCategorizedTagsLayout()
        
        if sender.isSelected {
            
            clearMapFromObjects()
            clusterManager.clearUnclusteredObjects()
            
            displayLoadingIndicator()
            
            if !overlapsManager.hasOverlaps {
                 output.didPressOverlaps()
            }
            else {
                output.didPressOverlapsWithoutRequest()
                showOverlaps(with: [])
            }
            output.didPressOverlapsButton()
        }
        else {
            
            overlapsManager.hideOverlaps()
            showDatasetSelection()
            hideLoadingIndicator(completion: nil)
        }
    }
    
    @IBAction func findMeButtonPressed(_ sender: UIButton) {
        refreshGeopositionState()
        output.didPressFindMe()
    }
    
    @IBAction func plusButtonPressed(_ sender: Any) {
        
         mapView.moveCameraPosition(to: mapView.map.cameraPosition.target.latitude, longitute: mapView.map.cameraPosition.target.longitude, zoom: mapView.map.cameraPosition.zoom + 1)
        
        guard let placemark = selectedPlaceMarkObject else { return }
        
        setupPinIcon(with: placemark)
    }
    
    @IBAction func minusButtonPressed(_ sender: Any) {
        
         mapView.moveCameraPosition(to: mapView.map.cameraPosition.target.latitude, longitute: mapView.map.cameraPosition.target.longitude, zoom: mapView.map.cameraPosition.zoom - 1)
        
        guard let placemark = selectedPlaceMarkObject else { return }
        
        setupPinIcon(with: placemark)
    }
    
    @IBAction func cancelButtonPressed(_ sender: Any) {
        
        guard isCameraFinishMoving else { return }
        
        selectedPlaceMarkObject = nil
        
        updateTitleView(with: "")
        
        overlapsButton.isSelected = false
        
        overlapsManager.hideOverlaps()
    
        clearMapFromObjects()
        clusterManager.clearUnclusteredObjects()
        output.updateFavoritedItems()
        showDatasetSelection()
    }
}

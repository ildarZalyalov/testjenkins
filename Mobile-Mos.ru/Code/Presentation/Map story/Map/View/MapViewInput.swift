//
//  MapViewInput.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 21.02.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol MapViewInput: class {
    
    func displayObjectsOnMap(with modelsWithPoint: [MapItem], and zoomOnPoints: Bool)
    
    func displayStarredObjectsOnMap(with mapItems: [MapItem], zoomOnPoints: Bool)
    
    func showOverlaps(with overlaps: [OverlapGeo])
    
    func turnOffOverlaps()
    
    func displayMapItemDetailInfo(with item: MapItem)
    
    func displayLoadingIndicator()
    
    func displaySearchLoadingIndicator()
    
    func hideLoadingIndicator(completion: (() -> Void)?)
    
    func animateTagsPlaceholders()
    
    func stopTagsPlaceholdersAnimation()
    
    func updateDatasetSelectionHeight(with height: CGFloat)
    
    func updateCurrentDatasetName(with name: String)
    
    func setupBottomUIElementsPosition(with distance: CGFloat)
    
    func showUserLocation(with location: Location)
    
    func changeTrafficJam()
    
    func showDatasetSearching()
    
    func hideDatasetSearching()
    
    func foldSearchModule(with height: CGFloat)
    
    func unfoldSearchModule(with height: CGFloat)
    
    func clearMapFromObjects()
    
    func didKnowMapItemIsFavorited(with mapObject: YMKMapObject, and isFavorite: Bool)
    
}

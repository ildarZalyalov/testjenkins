//
//  MapInteractorInput.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 21.02.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol MapInteractorInput: class {
    
    var locationServicesAreAvailable: Bool? { get }
    
    func obtainMapItems(with datasetId: String)
    
    func obtainMapItemDetail(with itemId: String)
    
    func obtainOverlaps()
    
    func findDataset(contains item: MapItem) -> FavoritedDataset?
    
    func requestLocationServicesAuthorization()
    
    func findUserGeoPosition()
    
    func saveDatasetToHistory(_ dataset: MapItemDataset)
    
    func checkIfMapObjectFavorited(_ mapObject: YMKMapObject?)
    
    func cancelAllRequest()
}

//
//  MapInteractor.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 21.02.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import CoreLocation

class MapInteractor: NSObject, MapInteractorInput, CLLocationManagerDelegate {
    
    weak var output: MapInteractorOutput!
    
    var mapItemsService: MapItemsService!
    var overlapsSerivce: OverlapsService!
    
    var request: HTTPRequest? {
        willSet{
            request?.cancel()
        }
    }
    
    var locationManager: CLLocationManager! {
        didSet { locationManager?.delegate = self }
    }
    
    weak var mapObject: YMKMapObject!
    
    var locationServicesAreAvailable: Bool? {
        return CLLocationManager.areLocationServicesAvailable
    }
    
    func obtainMapItems(with datasetId: String) {
        request = mapItemsService.obtainMapItems(with: datasetId, completion: {[weak self] (result) in
            self?.output.didFinishObtainingMapItems(result: result)
        })
    }
    
    func obtainMapItemDetail(with itemId: String) {
        mapItemsService.obtainMapItemDetail(with: itemId)
    }
    
    func obtainOverlaps() {
        request = overlapsSerivce.obtainOverlapsGeo(completion: { [weak self] (result) in
            self?.output.didFinishObtainingOverlaps(result: result)
        })
    }
    
    func requestLocationServicesAuthorization() {
        locationManager.requestWhenInUseAuthorization()
    }
    
    func findUserGeoPosition() {
        locationManager.requestLocation()
    }
    
    func findDataset(contains item: MapItem) -> FavoritedDataset? {
        
        let model = mapItemsService.obtainFavoritedMapItemsWithDataset().filter { $0.favoritedItems.contains{ $0.globalId == item.globalId }}.first
        
        return model
    }
    
    func saveDatasetToHistory(_ dataset: MapItemDataset) {
        
        mapItemsService.save(mapItemsDataset: dataset) { [weak self] (result) in
            self?.output.didFinishSavingDatasetToHistory(with: result)
        }
    }
    
    func checkIfMapObjectFavorited(_ mapObject: YMKMapObject?) {
        
        guard let mapObject = mapObject else { return }
        
        self.mapObject = mapObject
        
        guard let mapItem = mapObject.userData as? MapItem else { return }
        
        mapItemsService.mapItemIsFavorite(mapItem: mapItem) { [weak self] (isFavorited) in
            self?.output.didFinishCheckingIfMapItemFavorited(with: isFavorited, and: mapObject)
        }
    }
    
    func cancelAllRequest() {
        request?.cancel()
    }
    
    //MARK: - CLLocationManagerDelegate
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last ?? manager.location
        output.didFinishFindingUserLocation(with: Location(with: location))
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        output.didFinishFindingUserLocation(with: Location(with: manager.location))
    }
}

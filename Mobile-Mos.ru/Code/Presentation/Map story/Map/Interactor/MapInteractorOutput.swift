//
//  MapInteractorOutput.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 21.02.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol MapInteractorOutput: class {
    
    func didFinishObtainingMapItems(result: MapItemObtainMultipleResult)
    
    func didFinishObtainingOverlaps(result: MapOverlapGeoObtainMultipleResult)
    
    func didFinishFindingUserLocation(with location: Location?)
    
    func didFinishSavingDatasetToHistory(with result: DataBaseOperationResult)
    
    func didFinishCheckingIfMapItemFavorited(with isFavorited: Bool, and mapObject: YMKMapObject)
}

//
//  MapItemInfoPresenter.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 22.05.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class MapItemInfoPresenter: MapItemInfoViewOutput, MapItemInfoInteractorOutput, ItemInfoAlertFactoryActionEventHandler, MapActionsDelegate {
   
    let phonePrefix = "+7"
    
    weak var view: MapItemInfoViewInput!
    var router: MapItemInfoRouterInput!
    var interactor: MapItemInfoInteractorInput!
    
    var currentMapItem: MapItem!
    var currentDataset: MapItemDataset!
    var currentActionType: ActionType!
    
    //MARK: - MapItemInfoViewOutput
    
    func setupInitialState(with data: InfoEventDataInput) {
        
        currentMapItem = data.item
        currentDataset = data.dataset
        interactor.mapItemIsFavorite(mapItem: data.item)
        interactor.calculateDistance(to: data.item.location)
        interactor.checkOpenHour(with: data.item.workingHours)
        
        interactor.sendDatasetEvent(with: .viewMapObjectInfo(withString: AnalyticsConstants.viewMapObjectEventName), datasetName: data.dataset.name)
        
        data.eventHandler?.setupPulleyViewController(to: router as? PulleyConfigurator)
        data.eventHandler?.setupMapActionsDelegate(with: self)
    }
    
    func setupSafariView() {
        
        guard let website = currentMapItem.website else {
            return
        }
        router.showSafariViewController(with: website)
    }
    
    func setupShareView() {
        router.showShareViewController(with: currentMapItem.stringForSharing)
    }
    
    func setupPanoramaView() {
        router.showPanoramaView(to: currentMapItem.location)
    }
    
    func call(to numberString: String) {
        router.makeCall(to: numberString)
    }
    
    func showActionSheet(with actionType: ActionType) {
        currentActionType = actionType
        router.showActionSheet(with: actionType)
    }
    
    func closeModule() {
        router.closeCurrentModule()
    }
    
    func buildRoutToCurrentDisplayedPoint() {
        router.showBuildRoadAlert(with: currentMapItem.location)
    }
    
    func addMapItemToFavorites() {
        
        var favoritedDataset = FavoritedDataset()
        favoritedDataset.updateFrom(dataset: currentDataset)
        
        favoritedDataset.favoritedItems.append(currentMapItem)
        interactor.saveToFavoritesItem(favoritedDataset)
        
        interactor.sendDatasetEvent(with: .addMapObjectToFavorite(withString: AnalyticsConstants.addMapObjectToFavoriteEventName), datasetName: currentDataset.name)
    }
    
    func removeMapItemFromFavorites() {
        interactor.remove(mapItem: currentMapItem)
    }
    
    //MARK: - MapItemInfoInteractorOutput
    
    func didFinishCountingUserLocationToPoint(with formattedDistance: FormattedDistanceStrings?) {
        interactor.createItemDetailsDatasource(from: currentMapItem, andDistance: formattedDistance)
    }
    
    func didFinishCreatingItemDetailsDatasource(with datasource: MapItemDetailDataSource) {
        view.configureItemDetailInfo(with: datasource, datasetName: currentDataset.name)
    }
    
    func didFinishCheckingMapItemToFavorite(withBool isFavorited: Bool) {
        currentMapItem.isFavorited = isFavorited
        view.updateFavoriteState(with: isFavorited)
    }
    
    func didFinishCheckingOpenHour(with workingHour: WorkingHour) {
        view.updateIsOpenState(with: workingHour)
    }
    
    //MARK: - MapActionsDelegate
    
    func mapCameraDidMove() {
        router.closeCurrentModule()
    }
    
    //MARK: - ItemInfoAlertFactoryActionEventHandler
    
    func didSelectCopy(with type: ActionType) {
        
        switch type {
        case .callToPhone:
        
            if let phoneNumber = currentMapItem.publicPhone {
                view.copy(text: phoneNumber.hasPrefix(phonePrefix) ? phoneNumber : phonePrefix+phoneNumber)
            }
        case .openInWeb:
            view.copy(text: currentMapItem.website ?? "")
        }
    }
    
    func didSelectShare(with type: ActionType) {
        switch type {
        case .callToPhone:
            
            if let phoneNumber = currentMapItem.publicPhone {
                router.showShareViewController(with: phoneNumber.hasPrefix(phonePrefix) ? phoneNumber : phonePrefix+phoneNumber)
            }
            
        case .openInWeb:
            router.showShareViewController(with: currentMapItem.website ?? "")
        }
    }
    
    func didSelectAction(with type: ActionType) {
        switch type {
        case .callToPhone:
            
            if let phoneNumber = currentMapItem.publicPhone {
                call(to: phoneNumber.hasPrefix(phonePrefix) ? phoneNumber : phonePrefix+phoneNumber)
            }
        case .openInWeb:
            setupSafariView()
        }
    }
}

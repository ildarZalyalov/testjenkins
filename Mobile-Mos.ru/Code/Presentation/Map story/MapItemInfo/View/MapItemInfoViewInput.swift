//
//  MapItemInfoViewInput.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 22.05.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol MapItemInfoViewInput: class {
    
    /// Метод конфигурации детального окна объекта карты
    ///
    /// - Parameters:
    ///   - item: объект карты
    ///   - formattedDistance: форматтер с distanceString, distanceUnitString
    func configureItemDetailInfo(with itemDetailDatasource: MapItemDetailDataSource, datasetName: String)
    
    /// Обновляет текущий статус избранного
    func updateFavoriteState(with isFavorited: Bool)
    
    /// Обновляет текущий статус работы
    ///
    /// - Parameter workingHour: часы работы/закрытия
    func updateIsOpenState(with workingHour: WorkingHour)
    
    /// Скопировать текст
    ///
    /// - Parameter text: текст для копирования
    func copy(text: String)
}

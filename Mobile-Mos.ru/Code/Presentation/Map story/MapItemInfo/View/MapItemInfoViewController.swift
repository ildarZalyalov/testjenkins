//
//  MapItemInfoViewController.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 22.05.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import UIKit
import Pulley
import AVFoundation

class MapItemInfoViewController: BaseViewController, MapItemInfoViewInput, PulleyDrawerViewControllerDelegate, UIScrollViewDelegate, ConfigurableModuleController, MapItemInfoDataDelegate {
    
    let fullTextShowingAnimationDuration = 0.5
    let minDescriptionTextLineCount = 0
    let maxDescriptionTextLineCount = 3
    let contextMenuWidth: CGFloat = 232
    let contextMenuHeight: CGFloat = 40
    let pulleyTopConstant: CGFloat = 0
    var daysOfWeekString: String = ""
    let openedLabelHexColor = 3712880
    let topConstant: CGFloat = 24
    
    /// Диапазон допустимых значений альфы навигейшен бара
    let navigationBarAlphaRange: ClosedRange<CGFloat> = 0 ... 1
    
    /// Первоначальная высота открытия карточки
    let beginPartialRevealDrawerHeight: CGFloat = 262
    
    var bottomInset: CGFloat {
        return self.beginPartialRevealDrawerHeight
    }
    
    let smallFactorsFont = UIFont(customName: .graphikLCGBold, size: 19)
    let favoriteItemButtonFont = UIFont(customName: .graphikLCGRegular, size: 15)
    let favoritedItemButtonFont = UIFont(customName: .graphikLCGMedium, size: 15)
    let smallFactorsWidthRoadButtonConstant: CGFloat = 120
    
    var currentChosenViewWithLongTap: UIView!
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var gripperView: UIView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var detailInfoContainerStackView: UIStackView!
    
    //HeaderView properties
    @IBOutlet weak var itemNameLabel: UILabel!
    @IBOutlet weak var datasetNameLable: UILabel!
    @IBOutlet weak var distanceAndOpenStackView: UIStackView!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var isOpenedLabel: UILabel!
    @IBOutlet weak var buildRoadButton: UIButton!
    
    @IBOutlet weak var smallHeaderTitleLabel: UILabel!
    @IBOutlet weak var smallHeaderVisualEffectView: UIVisualEffectView!
    
    //Constraints
    @IBOutlet weak var buildRoadWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var buildRoadButtonTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var contentViewTopConstraint: NSLayoutConstraint!
    
    //InfoContainer properties
    @IBOutlet weak var infoContainerStackView: UIStackView!
    @IBOutlet weak var adressLabel: UILabel!
    @IBOutlet weak var cellPhoneLabel: UILabel!
    @IBOutlet weak var webAdressLabel: UILabel!
    @IBOutlet weak var adressStackView: UIStackView!
    @IBOutlet weak var cellPhoneStackView: UIStackView!
    @IBOutlet weak var webSiteStackView: UIStackView!
    
    //DescriptionContainer properties
    @IBOutlet weak var descriptionContainer: UIStackView!
    @IBOutlet weak var itemDescriptionLable: UILabel!
    
    //ScheduleContainer properties
    @IBOutlet weak var scheduleUpperSeparator: UIView!
    @IBOutlet weak var scheduleContainer: UIStackView!
    @IBOutlet weak var scheduleStackView: UIStackView!
    @IBOutlet var daysOfWeekLabels: [UILabel]!
    @IBOutlet weak var showMoreButton: UIButton!
    
    //ButtonsContainer
    @IBOutlet weak var addToFavoriteButton: UIButton!
    @IBOutlet weak var removeFavoriteButton: UIButton!
    @IBOutlet weak var panoramaStackView: UIStackView!
    
    var output: MapItemInfoViewOutput!
    
    //MARK: - Методы
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupGestureRecognizers()
        configreSmallFormFactors()
        
        //пока скрываем панорамы до поры до времени
        panoramaStackView.isHidden = true
    }
    
    override var shouldAutorotate: Bool {
        // для дочерних контроллеров, которые могут поменять ориентацию
        return true
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return .portrait
    }
    
    override func didMove(toParentViewController parent: UIViewController?) {
        
        super.didMove(toParentViewController: parent)
        
        guard let pulley = parent as? PulleyViewController else { return }
        pulley.topInset = pulleyTopConstant
    }
    
    //MARK: - MapItemInfoViewInput methods
    
    func configureItemDetailInfo(with itemDetailDatasource: MapItemDetailDataSource, datasetName: String) {
        
        itemNameLabel.text          = itemDetailDatasource.name.text
        smallHeaderTitleLabel.text  = itemDetailDatasource.name.text
        datasetNameLable.text       = datasetName 
        
        isOpenedLabel.isHidden      = itemDetailDatasource.workingHours.isHidden
        isOpenedLabel.textColor     = UIColor.fromHex(hex: openedLabelHexColor)
        scheduleContainer.isHidden  = itemDetailDatasource.workingHours.isHidden
        
        if !itemDetailDatasource.workingHours.isHidden {
            daysOfWeekLabels.enumerated().forEach{
                $0.element.text = itemDetailDatasource.workingHours.array[$0.offset]
            }
        }
        
        distanceLabel.text = itemDetailDatasource.distance
        
        adressLabel.text                = itemDetailDatasource.address.text
        adressStackView.isHidden        = itemDetailDatasource.address.isHidden
        cellPhoneLabel.text             = itemDetailDatasource.cellPhone.text
        cellPhoneStackView.isHidden     = itemDetailDatasource.cellPhone.isHidden
        webAdressLabel.text             = itemDetailDatasource.webAdress.text
        webSiteStackView.isHidden       = itemDetailDatasource.webAdress.isHidden
        infoContainerStackView.isHidden = itemDetailDatasource.isInfoViewHidden
        
        itemDescriptionLable.attributedText = itemDetailDatasource.description.attributedString
        itemDescriptionLable.textAlignment = .left
        
        ///-------- Убираем пока до поры до времени бизнес решение
//        descriptionContainer.isHidden = itemDetailDatasource.description.isHidden
        
        descriptionContainer.isHidden = true
        
        ///---------/////
        
        scheduleUpperSeparator.isHidden = itemDetailDatasource.description.isHidden
        
        
        view.layoutIfNeeded()
        
        contentViewTopConstraint.constant = headerView.frame.maxY + detailInfoContainerStackView.y
        
        view.layoutIfNeeded()
    }
    
    func updateFavoriteState(with isFavorited: Bool) {
        addToFavoriteButton.isSelected = isFavorited
        removeFavoriteButton.isHidden = !isFavorited
    }
    
    func updateIsOpenState(with workingHour: WorkingHour) {
        
        let time = workingHour.time
        
        guard time.lowercased() != StringsHelper.conveniencePlaceName,
              time.lowercased() != StringsHelper.closedPlaceName,
              time.lowercased() != StringsHelper.weekendString else {
            
            isOpenedLabel.isHighlighted = !workingHour.isOpen
            isOpenedLabel.text = time
            
            return
        }
        
        if workingHour.isOpen {
            
            isOpenedLabel.isHighlighted = false
            isOpenedLabel.text = StringsHelper.objectOpenedTitle + workingHour.time
        }
        else {
            
            isOpenedLabel.isHighlighted = true
            isOpenedLabel.text = StringsHelper.objectClosedTitle + workingHour.time
        }
    }
    
    func copy(text: String) {
        UIPasteboard.general.string = text
    }
    
    
    //MARK: - ConfigurableModuleController
    
    func configureModule(with object: Any) {
        if let data = object as? InfoEventDataInput {
            output.setupInitialState(with: data)
        }
    }
    
    //MARK: - PulleyDrawerViewControllerDelegate methods
    
    func drawerPositionDidChange(drawer: PulleyViewController, bottomSafeArea: CGFloat) {
        
        setNeedsStatusBarAppearanceUpdate()
        
        switch drawer.drawerPosition {
        case .open:
            scrollView.isScrollEnabled = true
            navigationController?.interactivePopGestureRecognizer?.isEnabled = false
            navigationController?.navigationBar.isUserInteractionEnabled = false
        case .partiallyRevealed:
            scrollView.isScrollEnabled = false
            navigationController?.interactivePopGestureRecognizer?.isEnabled = true
            
        case .collapsed:
            scrollView.scrollToTop(animated: false)
            navigationController?.interactivePopGestureRecognizer?.isEnabled = true

        default:
            return
        }
    }
    
    func drawerChangedDistanceFromBottom(drawer: PulleyViewController, distance: CGFloat, bottomSafeArea: CGFloat) {
        
        let safeAreaInsets = drawer.backwardCompatibleSafeAreaInsets
        let viewHeight = drawer.view.height - safeAreaInsets.top
        let revealDrawerHeight = beginPartialRevealDrawerHeight + bottomSafeArea
        let totalScroll = viewHeight - (pulleyTopConstant + revealDrawerHeight)
        let offset = distance - revealDrawerHeight
        let percentage = offset / totalScroll
        let alpha = (1 - percentage).clamped(to: navigationBarAlphaRange)

        let navigationBar = navigationController?.navigationBar
        
        if let tintColor = navigationBar?.tintColor {
            navigationBar?.tintColor = tintColor.withAlphaComponent(alpha)
        }
        
        if let titleView = navigationBar?.topItem?.titleView {
            titleView.alpha = alpha
        }
        
        navigationController?.navigationBar.isUserInteractionEnabled = true
        navigationController?.stateManager?.currentNavigationBarBackgroundView.alpha = alpha
    }
    
    func collapsedDrawerHeight(bottomSafeArea: CGFloat) -> CGFloat {
        return 0
    }
    
    func partialRevealDrawerHeight(bottomSafeArea: CGFloat) -> CGFloat {
        return beginPartialRevealDrawerHeight + bottomSafeArea
    }
    
    func supportedDrawerPositions() -> [PulleyPosition] {
        return [.open, .collapsed, .partiallyRevealed]
    }
    
    //MARK: - ScrollViewDelegate
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let currentMenuController = UIMenuController.shared
        
        if currentMenuController.isMenuVisible {
            
           currentMenuController.setMenuVisible(false, animated: false)
        }
        
        let isHiddenState = !(scrollView.contentOffset.y > itemNameLabel.height + topConstant)
        smallHeaderVisualEffectView.isHidden = isHiddenState
        headerView.isHidden = !isHiddenState
    }
    
    
    //MARK: - Helpers
    
    func configreSmallFormFactors() {
        
        if UIDevice.current.hasConfinedScreen {
            
            itemNameLabel.font = smallFactorsFont
            buildRoadButton.setTitle(StringsHelper.smallFactorsRoadText, for: .normal)
            buildRoadWidthConstraint.constant = smallFactorsWidthRoadButtonConstant
        }
    }
    
    func setupGestureRecognizers() {
        adressLabel.isUserInteractionEnabled = true
        
        let showFullTextTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.showFullTextButtonPressed(_:)))
        let openWebAdressTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.openWebsitePressed(_:)))
        let callToNumberTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.cellPhoneButtonPressed(_:)))
        
        let adressLongPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(self.setupSecondaryMenuController(with:)))
        let scheduleLongPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(self.setupSecondaryMenuController(with:)))
        let descriptionLongPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(self.setupSecondaryMenuController(with:)))
        
        let webAdressLongPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(self.showActionSheet(with:)))
        let cellPhoneLongPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(self.showActionSheet(with:)))
        
        
        
        cellPhoneStackView.addGestureRecognizer(callToNumberTapGesture)
        webSiteStackView.addGestureRecognizer(openWebAdressTapGesture)
        descriptionContainer.addGestureRecognizer(showFullTextTapGesture)
        
        adressStackView.addGestureRecognizer(adressLongPressGesture)
        scheduleContainer.addGestureRecognizer(scheduleLongPressGesture)
        descriptionContainer.addGestureRecognizer(descriptionLongPressGesture)
        webSiteStackView.addGestureRecognizer(webAdressLongPressGesture)
        cellPhoneStackView.addGestureRecognizer(cellPhoneLongPressGesture)
    }
    
    @objc
    func setupSecondaryMenuController(with sender: UILongPressGestureRecognizer) {
        
        guard sender.state == .began,
            let tappedView = sender.view
            else { return }
        
        tappedView.becomeFirstResponder()
        
        let menu = UIMenuController.shared
        
        let copyItem = UIMenuItem(title: StringsHelper.menuItemCopyTitle, action: #selector(self.copyToPasteboard(_:)))
        let speakItem = UIMenuItem(title: StringsHelper.menuItemSpeakTitle, action: #selector(self.speak(_:)))
    
        
        var rect = CGRect()
        var showView = UIView()
        
        if tappedView == adressStackView {
            rect = adressLabel.frame
            showView = adressLabel
        }
        else if tappedView == scheduleContainer {
            rect = scheduleStackView.frame
            rect.origin.y -= rect.origin.y
            showView = scheduleStackView
        }
        else if tappedView == descriptionContainer {
            rect = itemDescriptionLable.frame
            showView = itemDescriptionLable
        }
        
        currentChosenViewWithLongTap = showView
        
        menu.menuItems = [copyItem,speakItem]
        menu.setTargetRect(CGRect(x: rect.midX - contextMenuWidth, y: rect.origin.y, width: contextMenuWidth, height: contextMenuHeight), in: showView.superview!)
        menu.setMenuVisible(true, animated: true)
    }
    
    @objc
    func copyToPasteboard(_ menuController: UIMenuController) {
        
        var stringToCopy: String? = ""
        
        if currentChosenViewWithLongTap == adressLabel {
            
            stringToCopy = adressLabel.text
        }
        else if currentChosenViewWithLongTap == itemDescriptionLable {
            stringToCopy = itemDescriptionLable.text
        }
        else if currentChosenViewWithLongTap == scheduleStackView {
            
            stringToCopy = daysOfWeekString
        }
        
        copy(text: stringToCopy ?? "")
    }
    
    @objc
    func speak(_ menuController: UIMenuController) {
        
        var textToSpeak: String? = ""
        
        if currentChosenViewWithLongTap == adressLabel {
            
            textToSpeak = adressLabel.text
        }
        else if currentChosenViewWithLongTap == itemDescriptionLable {
            
            textToSpeak = itemDescriptionLable.text
        }
        else if currentChosenViewWithLongTap == scheduleStackView {
            
            textToSpeak = daysOfWeekString
        }
        
        let currentLanguageCode = AVSpeechSynthesisVoice.currentLanguageCode()
        let utterance = AVSpeechUtterance(string: textToSpeak ?? "")
        utterance.voice = AVSpeechSynthesisVoice(language: currentLanguageCode)
        
        let synth = AVSpeechSynthesizer()
        synth.speak(utterance)
    }
    
    @objc
    func showActionSheet(with gesture: UILongPressGestureRecognizer) {
        
        guard gesture.state == .began,
            let tappedView = gesture.view
            else { return }
        
        if tappedView == cellPhoneStackView {
            output.showActionSheet(with: .callToPhone)
        }
        else if tappedView == webSiteStackView {
            output.showActionSheet(with: .openInWeb)
        }
    }
    
    //MARK: - Buttons action
    
    @IBAction func closeButtonPressed(_ sender: Any) {
        output.closeModule()
        navigationController?.interactivePopGestureRecognizer?.isEnabled = true
    }
    
    @IBAction func favoriteButtonPressed(_ sender: UIButton) {
        
        if !sender.isSelected{
            sender.isSelected = true
            removeFavoriteButton.isHidden = false
            sender.titleLabel?.font = favoritedItemButtonFont
            output.addMapItemToFavorites()
        }
        else {
            sender.isSelected = false
            sender.titleLabel?.font = favoriteItemButtonFont
            removeFavoriteButton.isHidden = true
            output.removeMapItemFromFavorites()
        }
    }
    
    @IBAction func removeFavoriteButtonPressed(_ sender: Any) {
        favoriteButtonPressed(addToFavoriteButton)
    }
    
    @IBAction func shareButtonPressed(_ sender: Any) {
        output.setupShareView()
    }
    
    @IBAction func showFullTextButtonPressed(_ sender: UIButton) {
        
        let currentNumberOfLinese = itemDescriptionLable.numberOfLines
        let buttonText = currentNumberOfLinese == minDescriptionTextLineCount ? StringsHelper.showFullTextTitle : StringsHelper.hideFullTextTitle
        
        UIView.animate(withDuration: fullTextShowingAnimationDuration) { [weak self] in
            
            guard let strongSelf = self else { return }
            
            strongSelf.itemDescriptionLable.numberOfLines = currentNumberOfLinese == strongSelf.minDescriptionTextLineCount ? strongSelf.maxDescriptionTextLineCount : strongSelf.minDescriptionTextLineCount
            strongSelf.showMoreButton.setTitle(buttonText, for: .normal)
            
            strongSelf.view.layoutIfNeeded()
        }
       
    }
    
    @IBAction func openWebsitePressed(_ sender: Any) {
        
        output.setupSafariView()
    }
    
    @IBAction func cellPhoneButtonPressed(_ sender: Any) {
        output.call(to: cellPhoneLabel.text ?? "")
    }
    
    @IBAction func buildRoutPressed(_ sender: Any) {
        output.buildRoutToCurrentDisplayedPoint()
    }
    
    @IBAction func openThePanoramaPressed(_ sender: Any) {
        output.setupPanoramaView()
    }
}

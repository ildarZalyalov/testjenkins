//
//  MapItemInfoViewOutput.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 22.05.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol MapItemInfoViewOutput: class {
    
    /// Инициализация контроллера с точкой карты
    ///
    /// - Parameter mapItemModelWithPoint: модель включающая MapItem и YMKPoint
    func setupInitialState(with data: InfoEventDataInput)
    
    /// Инициализация экрана Safari для отображаниея web сайта
    ///
    /// - Parameter webSiteUrl: URL сайта
    func setupSafariView()
    
    /// Инициализация нативного экрана шаринга
    ///
    /// - Parameter shareString: текст для шаринга
    func setupShareView()
    
    /// Инициализация панорамы YMKMapKit
    func setupPanoramaView()
    
    /// Позвонить на номер
    ///
    /// - Parameter numberString: номер для звонка
    func call(to numberString: String)
    
    /// Отобразить action sheet
    ///
    /// - Parameter chosenText: текст, полученный из нажатой секции
    func showActionSheet(with actionType: ActionType)
    
    /// Закрыть модуль
    func closeModule()
    /// Создание пути до точки от текущего местоположение пользователя
    func buildRoutToCurrentDisplayedPoint()
    
    /// Добавить точку в Избранного
    func addMapItemToFavorites()
    
    /// Удалить точку из Избранного
    func removeMapItemFromFavorites()
}

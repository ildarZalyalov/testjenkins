//
//  MapItemInfoModuleConfigurator.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 22.05.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import UIKit
import Pulley

class MapItemInfoModuleConfigurator: BaseModuleConfigurator {
    
    override func configureModule(for view: UIViewController) {
        
        let viewController = view as! MapItemInfoViewController
        let presenter = MapItemInfoPresenter()
        let interactor = MapItemInfoInteractor()
        let router = MapItemInfoRouter()
    
        let distanceService = DistanceStringBuilderImplementation()
        let mapItemsService = UIApplication.shared.serviceBuilder.getMapItemsService()
        let storyboardFactoryImplementation = StoryboardFactoryImplementation()
        let itemInfoAlertFactoryImplementation = ItemInfoAlertFactoryImplementation()
        let commonAlertsFactoryImplementation = CommonAlertsFactoryImplementation()
        let workingHourBuilderImplementation = WorkingHourBuilderImplementation()
        let itemDetailBuilderImplementation = MapItemDetailsBuilderImplementation()
        let locationManager = CLLocationManager()
        let analyticsManager = AnalyticsManagerBuilder.getAnalyticsManager()
        
        viewController.output = presenter
        
        presenter.view = viewController
        presenter.interactor = interactor
        presenter.router = router
        
        interactor.output = presenter
        interactor.mapItemsService = mapItemsService
        interactor.distanceStringsBuilder = distanceService
        interactor.workingHourBuilder = workingHourBuilderImplementation
        interactor.itemDetailBuilder = itemDetailBuilderImplementation
        interactor.locationManager = locationManager
        interactor.analyticsManager = analyticsManager
        
        router.storyboardFactory = storyboardFactoryImplementation
        router.itemInfoAlertFactory = itemInfoAlertFactoryImplementation
        router.transitionHandler = viewController
        router.commonAlerts = commonAlertsFactoryImplementation
        router.locationManager = locationManager
        router.analyticsManager = analyticsManager
    }
}

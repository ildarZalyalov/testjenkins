//
//  MapItemInfoInteractorInput.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 22.05.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol MapItemInfoInteractorInput: class {
    

    /// Подсчет расстояния от текущего местоположения пользователя до точки
    ///
    /// - Parameter point: YMKPoint - точка, до которой нужно подсчитать расстояние
    func calculateDistance(to location: Location)
    
    /// Создать датасурс для карточки объекта
    ///
    /// - Parameters:
    ///   - item: объекта карты
    ///   - formattedDistance: форматированная строка с расстояниями
    func createItemDetailsDatasource(from item: MapItem, andDistance formattedDistance: FormattedDistanceStrings?)
    
    /// Проверить открыт ли объект от рабочего времени и текущего времени
    ///
    /// - Parameter workingHours: массив рабочих дней/часов
    func checkOpenHour(with workingHours: [MapItemWorkingHour])
    
    /// Сохранить датасет с выбранным избранным объектом
    ///
    /// - Parameter dataset: датасет выбранных объектов
    func saveToFavoritesItem(_ dataset: MapItemDataset)
    
    /// Удалить точку карты из бд
    ///
    /// - Parameter mapItem: точка карты
    func remove(mapItem: MapItem)
    
    
    /// Обновить точку карты если она есть в бд
    ///
    /// - Parameter mapItem: точка карты
    func mapItemIsFavorite(mapItem: MapItem)
    
    func sendDatasetEvent(with event: MapSearchEvents, datasetName: String)
}

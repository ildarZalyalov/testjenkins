//
//  MapItemInfoInteractorOutput.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 22.05.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol MapItemInfoInteractorOutput: class {
    
    /// Окончание подсчета расстояния до точки от текущего местоположения пользователя
    ///
    /// - Parameter formattedDistance: FormattedDistanceStrings включающая distanceString, distanceUnitString
    func didFinishCountingUserLocationToPoint(with formattedDistance: FormattedDistanceStrings?)
    
    /// Окончание создание датасурса детальной карточки объекта
    ///
    /// - Parameter datasource: датасурс с готовой информацией
    func didFinishCreatingItemDetailsDatasource(with datasource: MapItemDetailDataSource)
    
    /// Окончание проверки map item-а находится ли данная точка в избранных
    ///
    /// - Parameter completion: блок возврата с булеаном
    func didFinishCheckingMapItemToFavorite(withBool isFavorited: Bool)
    
    /// Окончание проверки времени работы заведения
    ///
    /// - Parameter workingHour: тупль с булеаном (открыт/закрыт) и временем закрытия
    func didFinishCheckingOpenHour(with workingHour: WorkingHour)
}

//
//  MapItemInfoInteractor.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 22.05.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import CoreLocation

class MapItemInfoInteractor: MapItemInfoInteractorInput {
    
    weak var output: MapItemInfoInteractorOutput!
    
    var distanceStringsBuilder: DistanceStringBuilder!
    var mapItemsService: MapItemsService!
    var workingHourBuilder: WorkingHourBuilder!
    var itemDetailBuilder: ItemDetailsBuilder!
    var locationManager: CLLocationManager!
    var analyticsManager: AnalyticsManager!
    
    //MARK: - MapItemInfoInteractorInput
    
    func calculateDistance(to pointLocation: Location) {
        
        var formattedDistanceString: FormattedDistanceStrings? = distanceStringsBuilder.unknownDistanceStrings
        
        defer {
            output.didFinishCountingUserLocationToPoint(with: formattedDistanceString)
        }
        
        guard let location = locationManager.location else { return }
        let placeLocation = CLLocation(latitude: pointLocation.latitude, longitude: pointLocation.longitude)
        
        formattedDistanceString = distanceStringsBuilder.distanceStrings(forDistance: placeLocation.distance(from: location))
    }
    
    func createItemDetailsDatasource(from item: MapItem, andDistance formattedDistance: FormattedDistanceStrings?) {
        
       let datasource =  itemDetailBuilder.createItemDetails(from: item, formattedDistance: formattedDistance)
        output.didFinishCreatingItemDetailsDatasource(with: datasource)
    }
    
    func checkOpenHour(with workingHours: [MapItemWorkingHour]) {
        
        guard let workingHour = workingHourBuilder.checkOpenHour(with: workingHours) else {
            return
        }
        
        output.didFinishCheckingOpenHour(with: workingHour)
    }
    
    func mapItemIsFavorite(mapItem: MapItem) {
        
        mapItemsService.mapItemIsFavorite(mapItem: mapItem) {[weak self] (isFavorited) in
            self?.output.didFinishCheckingMapItemToFavorite(withBool: isFavorited)
        }
    }
    
    func saveToFavoritesItem(_ dataset: MapItemDataset) {
        mapItemsService.saveToFavorites(dataset)
    }
    
    func remove(mapItem: MapItem) {
        mapItemsService.remove(mapItem: mapItem)
    }
    
    func sendDatasetEvent(with event: MapSearchEvents, datasetName: String) {
        
        let datasetEvent: DatasetEventData = (datasetName, analyticsManager.categoryName, AnalyticsConstants.emptyValueName, AnalyticsConstants.emptyValueName)
        
        analyticsManager.setMapObjectDatasetName(with: datasetName)
        
        analyticsManager.sendMapConversationEvent(with: event, aboutAppData: nil, appSharingSource: nil, datasetData: datasetEvent, mapObjectSharingData: nil, overlapData: nil)
    }
    
}

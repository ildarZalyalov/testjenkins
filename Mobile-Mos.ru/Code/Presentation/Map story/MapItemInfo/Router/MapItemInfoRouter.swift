//
//  MapItemInfoRouter.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 22.05.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import SafariServices
import Pulley

class MapItemInfoRouter: NSObject, MapItemInfoRouterInput, PulleyConfigurator, SFSafariViewControllerDelegate {
    
    fileprivate let protocolPrefix = "http://"
    fileprivate let protectedProtocolPrefix = "https://"
    fileprivate let phonePrefix = "+"
    
    weak var transitionHandler: MapItemInfoViewController!
    weak var pulley: PulleyViewController!
    
    var storyboardFactory: StoryboardFactory!
    var itemInfoAlertFactory: ItemInfoAlertFactory!
    var commonAlerts: CommonAlertsFactory!
    var locationManager: CLLocationManager!
    var analyticsManager: AnalyticsManager!
    
    //MARK: - MapItemInfoRouterInput
    
    func setUpPulleyViewController(with object: Any?) {
        guard let controller = object as? PulleyViewController else { return }
        pulley = controller
    }
    
    func showSafariViewController(with websiteString: String) {
       
        guard var urlString = websiteString.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlPathAllowed) else { return }
        
        if !urlString.hasPrefix(protocolPrefix) && !urlString.hasPrefix(protectedProtocolPrefix) {
            
            urlString = protocolPrefix + urlString
        }
        
        
        guard let websiteUrl = URL(string: urlString) else {
            return
        }
        
        let safariController = SFSafariViewController(url: websiteUrl, entersReaderIfAvailable: true)
        safariController.delegate = self
        
        if #available(iOS 10.0, *) {
            safariController.preferredControlTintColor = mapConversation.style.controlsTextColor
        }
        
        transitionHandler.present(safariController, animated: true, completion: nil)
    }
    
    func showShareViewController(with shareString: String) {
        
        let activityController = UIActivityViewController(activityItems: [shareString], applicationActivities: nil)
        
        activityController.completionWithItemsHandler = { [weak self] activityType, completed, items, error  in
            
            guard let strongSelf = self else { return }
            
            let errorValue = error?.localizedDescription ?? AnalyticsConstants.emptyValueName
            
            if completed {
                
                let mapObjectInfoEventData: MapObjectInfoEventData = (activityType?.rawValue ?? AnalyticsConstants.emptyValueName, strongSelf.analyticsManager.datasetName, strongSelf.analyticsManager.categoryName, AnalyticsConstants.emptyValueName, errorValue)
                strongSelf.analyticsManager.sendMapConversationEvent(with: .shareMapObjectInfo(withString: AnalyticsConstants.sharingMapObjectInfoEventName), aboutAppData: nil, appSharingSource: nil, datasetData: nil, mapObjectSharingData:mapObjectInfoEventData, overlapData: nil)
            }
        }
        transitionHandler.present(activityController, animated: true, completion: nil)
        
    }
    
    func showPanoramaView(to chosenPointLocation: Location) {
        
        let point = YMKPoint(latitude: chosenPointLocation.latitude, longitude: chosenPointLocation.longitude)
        
        let panoramaViewController = storyboardFactory.getStoryboard(with: .mapitemInfo).instantiateViewController(for: .mapItemPanorama)
        
        guard let destination = panoramaViewController as? ConfigurableModuleController else { return }
        
        panoramaViewController.view.setNeedsLayout()
        
        transitionHandler.show(panoramaViewController, sender: nil)
        
        destination.configureModule(with: point)
        
        panoramaViewController.view.layoutIfNeeded()

    }
    
    func showBuildRoadAlert(with pointLocation: Location) {
        
        guard let location = Location(with: locationManager.location) else {
            showGeolocationErrorAlert()
            return
        }
        
        let alert = commonAlerts.getCreateRouteActionSheet(for: location, and: pointLocation)
        transitionHandler.present(alert, animated: true, completion: nil)
    }
    
    func showGeolocationErrorAlert() {
        let alert = commonAlerts.getFailedToGetUserLocationAlert()
        transitionHandler.present(alert, animated: true, completion: nil)
    }
    
    func showActionSheet(with actionType: ActionType) {
        
       let alert = itemInfoAlertFactory.actionSheet(with: actionType, eventHandler: transitionHandler.output as? ItemInfoAlertFactoryActionEventHandler)
        
        transitionHandler.present(alert, animated: true, completion: nil)
    }
    
    func makeCall(to numberString: String) {
        let numberToCall = numberString.components(separatedBy: CharacterSet.decimalDigits.inverted).joined(separator: "")
        UIApplication.shared.attempt(calling: phonePrefix+numberToCall)
    }
    
    func closeCurrentModule() {
        pulley.setDrawerPosition(position: .collapsed, animated: true)
    }
}

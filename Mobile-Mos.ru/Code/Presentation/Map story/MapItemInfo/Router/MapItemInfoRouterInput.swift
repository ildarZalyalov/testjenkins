//
//  MapItemInfoRouterInput.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 22.05.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

protocol MapItemInfoRouterInput: class {
    
    /// Отображение экрана Safari
    ///
    /// - Parameter websiteURL: URL сайта для отображения
    func showSafariViewController(with websiteURL: String)
    
    /// Отображение нативного экрана шаринга
    ///
    /// - Parameter shareString: String для шаринга
    func showShareViewController(with shareString: String)
    
    /// Отобразить панораму
    ///
    /// - Parameter chosenPointLocation: локация на которой отображается пулей
    func showPanoramaView(to chosenPointLocation: Location)
    
    /// Отобразить action sheet
    ///
    /// - Parameter actionType: тип действия из перечисления
    func showActionSheet(with actionType: ActionType)
    
    /// Отобразить алерт для построение маршрута
    ///
    /// - Parameter pointLocation: локация объекта карты
    func showBuildRoadAlert(with pointLocation: Location)
    
    /// Сделать телефонный звонок
    ///
    /// - Parameter numberString: номер телефона
    func makeCall(to numberString: String)
    
    /// Закрыть текущий модуль
    func closeCurrentModule()
}

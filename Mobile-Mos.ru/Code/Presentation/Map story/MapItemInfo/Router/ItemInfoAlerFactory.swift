//
//  ItemInfoAlerFactory.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 05.09.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Перечисление с типом экшена
///
/// - callToPhone: сделать звонок
/// - openInWeb: открыть в вебе
enum ActionType{
    case callToPhone
    case openInWeb
}

/// Обработчик нажатий
protocol ItemInfoAlertFactoryActionEventHandler {
    
    /// Выбрать кнопку "скопировать"
    func didSelectCopy(with type: ActionType)
    
    /// Выбрать кнопку "поделиться"
    func didSelectShare(with type: ActionType)
    
    /// Выбрать действие из перечисление
    ///
    /// - Parameter type: тип из перечисления
    func didSelectAction(with type: ActionType)
}

/// Фабрика alert-ов для модуля с картой
protocol ItemInfoAlertFactory {
    
    /// Создать action sheet с выбором действий
    ///
    /// - Parameters:
    ///   - actionType: тип уникального действия
    ///   - eventHandler: обработчик выбора
    /// - Returns: контроллер с action sheet
    func actionSheet(with actionType: ActionType, eventHandler: ItemInfoAlertFactoryActionEventHandler?) -> UIViewController
    
}

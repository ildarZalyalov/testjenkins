//
//  ItemInfoAlertFactoryImplementation.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 05.09.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class ItemInfoAlertFactoryImplementation: ItemInfoAlertFactory {
    
    func actionSheet(with actionType: ActionType, eventHandler: ItemInfoAlertFactoryActionEventHandler?) -> UIViewController {
        
        let alert = UIAlertController(title:nil, message:nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Копировать", style: .default, handler: { (alert: UIAlertAction!) in
            eventHandler?.didSelectCopy(with: actionType)
        }))
        
        alert.addAction(UIAlertAction(title: "Поделиться", style: .default, handler: { (alert: UIAlertAction!) in
            eventHandler?.didSelectShare(with: actionType)
        }))
        
        switch actionType {
        case .callToPhone:
            
            alert.addAction(UIAlertAction(title: "Позвонить", style: .default, handler: { (alert: UIAlertAction!) in
                eventHandler?.didSelectAction(with: .callToPhone)
            }))
        case .openInWeb:
            
            alert.addAction(UIAlertAction(title: "Открыть в браузере", style: .default, handler: { (alert: UIAlertAction!) in
                eventHandler?.didSelectAction(with: .openInWeb)
            }))
        }
        
        alert.addAction(UIAlertAction(title: "отмена", style: .cancel, handler: nil))
        
        return alert
    }
}

//
//  InfoEventHandler.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 01.06.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

typealias InfoEventDataInput = (item: MapItem, dataset: MapItemDataset, eventHandler: InfoEventHandler?)

typealias OverlapInfoEventDataInput = (overlapId: String, eventHandler: InfoEventHandler?)

/// Конфигуратор пуллея для любого модуля где он нужен из вне
protocol PulleyConfigurator {
    func setUpPulleyViewController(with object: Any?)
}

protocol InfoEventHandler: class {
    /// Настройка пулей контроллера роутера
    ///
    /// - Parameter router: роутер карточки с детальной информацией
    func setupPulleyViewController(to configurablePulley: PulleyConfigurator?)
    
    /// Настройка делегата карты для отлова действий с карты в карточке объекта
    ///
    /// - Parameter delegate: делегат
    func setupMapActionsDelegate(with delegate: MapActionsDelegate?)
}

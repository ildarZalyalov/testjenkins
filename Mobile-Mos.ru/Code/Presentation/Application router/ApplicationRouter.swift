//
//  ApplicationRouter.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 11.09.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Роутер для глобальных (не в рамках одной истории) переходов в приложении
protocol ApplicationRouter {
    
    /// Заблокировать переходы пока оображается определенный экран
    ///
    /// - Parameter viewController: блокирующий переходы экран
    func blockTransitions(whileOnScreen viewController: UIViewController)
    
    /// Отобразить модуль чата для диалога
    ///
    /// - Parameter conversation: диалог
    func showChatModule(for conversation: Conversation)
    
    /// Отобразить модуль чата для диалога и отобразить в нем сообщение
    ///
    /// - Parameters:
    ///   - conversation: диалог
    ///   - message: сообщение
    func showChatModule(for conversation: Conversation, and message: ChatMessage)
    
    /// Отобразить модуль карт с объектом конфигурации
    ///
    /// - Parameters:
    ///   - configurationObject: объект конфигурации
    func showMapModule(with configurationObject: Any)
    
    /// Отобразить модуль с информацией о выходе новой версии приложения
    ///
    /// - Parameter versionInfo: информация о новой версии
    func showAppNewVersionModule(with versionInfo: iTunesVersionLookupState)
    
    /// Отобразить модуль авторизации
    func showAuthorizationModule()
}

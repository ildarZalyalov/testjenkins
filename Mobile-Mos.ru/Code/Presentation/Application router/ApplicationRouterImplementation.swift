//
//  ApplicationRouterImplementation.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 11.09.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

class ApplicationRouterImplementation: ApplicationRouter, AuthorizationProcessHandler {
    
    var storyboardFactory: StoryboardFactory!
    var commonAlertsFactory: CommonAlertsFactory!
    
    fileprivate typealias Router = ApplicationRouterImplementation
    fileprivate weak static var transitionsBlockerController: UIViewController?
    
    //MARK: - Приватные методы
    
    fileprivate func showChatModule(for conversation: Conversation, with configurationObject: Any) {
        
        guard let window = UIApplication.shared.delegate?.window else { return }
        guard let navigationController = window?.rootViewController as? UINavigationController else { return }
        
        let conversationsStoryboard = storyboardFactory.getStoryboard(with: .conversations)
        var newNavigationStack = [UIViewController]()
        
        // либо список чатов уже стоит корневым экраном, либо создаем его
        var conversationsModule: UIViewController? = navigationController.viewControllers.first as? ConversationsViewController
        if conversationsModule == nil {
            conversationsModule = conversationsStoryboard.instantiateViewController(for: .conversationList)
        }
        
        newNavigationStack.append(conversationsModule!)
        
        if conversation.itemId == mapConversation.itemId {
            
            guard let mapModule = storyboardFactory.getStoryboard(with: .map).instantiateInitialViewController()
                else { return }
            
            newNavigationStack.append(mapModule)
            
            if let configurableModule = mapModule as? ConfigurableModuleController {
                
                configurableModule.configureModule(with: configurationObject)
            }
        }
        else {
            
            let chatModule = conversationsStoryboard.instantiateViewController(for: .chat)
            if let configurableModule = chatModule as? ConfigurableModuleController {
                configurableModule.configureModule(with: configurationObject)
            }
            
            newNavigationStack.append(chatModule)
        }
        
        navigationController.dismiss(animated: true, completion: nil)
        navigationController.setViewControllers(newNavigationStack, animated: true)
    }
    
    //MARK: - ApplicationRouter
    
    func blockTransitions(whileOnScreen viewController: UIViewController) {
        Router.transitionsBlockerController = viewController
    }
    
    func showChatModule(for conversation: Conversation) {
        guard Router.transitionsBlockerController == nil else { return }
        let configuration = ChatModuleConfiguration(conversation: conversation, message: nil)
        showChatModule(for: conversation, with: configuration)
    }
    
    func showChatModule(for conversation: Conversation, and message: ChatMessage) {
        guard Router.transitionsBlockerController == nil else { return }
        let configuration = ChatModuleConfiguration(conversation: conversation, message: message)
        showChatModule(for: conversation, with: configuration)
    }
    
    func showMapModule(with configurationObject: Any) {
        guard Router.transitionsBlockerController == nil else { return }
        showChatModule(for: mapConversation, with: configurationObject)
    }
    
    func showAppNewVersionModule(with versionInfo: iTunesVersionLookupState) {
        
        guard Router.transitionsBlockerController == nil else { return }
        
        guard let version = versionInfo.version else { return }
        guard let releaseNotes = versionInfo.releaseNotes else { return }
        
        guard let window = UIApplication.shared.delegate?.window else { return }
        
        var topController = window?.rootViewController
        while topController?.presentedViewController != nil {
            topController = topController?.presentedViewController
        }
        
        guard topController != nil else { return }
        
        let alert = commonAlertsFactory.getNewApplicationVersionAlert(with: version, and: releaseNotes)
        topController!.present(alert, animated: true, completion: nil)
    }
    
    func showAuthorizationModule() {
        
        guard Router.transitionsBlockerController == nil else { return }
        guard let window = UIApplication.shared.delegate?.window else { return }
        guard let topController = window?.rootViewController else { return }
        
        let storyboard = storyboardFactory.getStoryboard(with: .authorization)
        guard let authorizationModule = storyboard.instantiateInitialViewController() else { return }
        
        if let configurableModule = authorizationModule as? ConfigurableModuleController {
            configurableModule.configureModule(with: self)
        }
        
        topController.present(authorizationModule, animated: true, completion: nil)
    }
    
    //MARK: - AuthorizationProcessHandler
    
    func handleCancelAuthorization() -> Bool {
        
        guard let window = UIApplication.shared.delegate?.window else { return false }
        guard let topController = window?.rootViewController else { return false }
        
        topController.dismiss(animated: true, completion: nil)
        
        return true
    }
    
    func handleFinishAuthorization() {
        
        guard let window = UIApplication.shared.delegate?.window else { return }
        guard let navigationController = window?.rootViewController as? UINavigationController else { return }
        
        let conversationsStoryboard = storyboardFactory.getStoryboard(with: .conversations)
        let conversationsModule = conversationsStoryboard.instantiateViewController(for: .conversationList)
        
        navigationController.setViewControllers([conversationsModule], animated: false)
    }
}

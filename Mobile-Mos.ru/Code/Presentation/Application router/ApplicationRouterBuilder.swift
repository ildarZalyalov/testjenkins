//
//  ApplicationRouterBuilder.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 09.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Билдер ApplicationRouter, когда он используется вне модулей
class ApplicationRouterBuilder {
    
    /// Создать ApplicationRouter
    static func getApplicationRouter() -> ApplicationRouter {
        
        let applicationRouter = ApplicationRouterImplementation()
        
        applicationRouter.storyboardFactory = StoryboardFactoryImplementation()
        applicationRouter.commonAlertsFactory = CommonAlertsFactoryImplementation()
        
        return applicationRouter
    }
}

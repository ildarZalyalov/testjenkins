//
//  PhotoGalleryNetworkingIntegration.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 08.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation
import SDWebImage
import AXPhotoViewer
import MobileCoreServices

/// Интеграция галереи фото и SDWebImage с поддержкой GIF
class PhotoGalleryNetworkingIntegration: NSObject, AXNetworkIntegrationProtocol {
    
    fileprivate var downloadOperations = NSMapTable<AXPhotoProtocol, SDWebImageOperation>(keyOptions: .strongMemory, valueOptions: .strongMemory)
    
    fileprivate func beginLoad(for photo: AXPhotoProtocol, options: SDWebImageOptions = []) {
        
        guard let url = photo.url else { return }
        
        let progress: SDWebImageDownloaderProgressBlock = { [weak self] (receivedSize, expectedSize, targetURL) in
            
            guard let strongSelf = self else {
                return
            }
            
            strongSelf.delegate?.networkIntegration?(strongSelf, didUpdateLoadingProgress: CGFloat(receivedSize) / CGFloat(expectedSize), for: photo)
        }
        
        let completion: SDInternalCompletionBlock = { [weak self] (image, data, error, cacheType, finished, imageURL) in
            
            guard let strongSelf = self else {
                return
            }
            
            strongSelf.downloadOperations.removeObject(forKey: photo)
            
            if let error = error {
                strongSelf.delegate?.networkIntegration(strongSelf, loadDidFailWith: error, for: photo)
            }
            else {
                
                if let imageData = data {
                    photo.imageData = imageData
                }
                else if let image = image {
                    
                    if image.cgImage?.utType == kUTTypeGIF {
                        strongSelf.beginLoad(for: photo, options: SDWebImageOptions.queryDataWhenInMemory)
                        return
                    }
                    else {
                        photo.image = image
                    }
                }
                
                strongSelf.delegate?.networkIntegration(strongSelf, loadDidFinishWith: photo)
            }
        }
        
        guard let operation = SDWebImageManager.shared().loadImage(with: url, options: options, progress: progress, completed: completion) else { return }
        
        downloadOperations.setObject(operation, forKey: photo)
    }
    
    //MARK: - NetworkIntegrationProtocol
    
    weak var delegate: AXNetworkIntegrationDelegate?
    
    func loadPhoto(_ photo: AXPhotoProtocol) {
        
        guard photo.image == nil && photo.imageData == nil else {
            delegate?.networkIntegration(self, loadDidFinishWith: photo)
            return
        }
        
        guard photo.url != nil else { return }
        
        beginLoad(for: photo)
    }
    
    func cancelLoad(for photo: AXPhotoProtocol) {
        guard let downloadOperation = downloadOperations.object(forKey: photo) else { return }
        downloadOperation.cancel()
    }
    
    func cancelAllLoads() {
        
        let enumerator = downloadOperations.objectEnumerator()
        
        while let downloadOperation = enumerator?.nextObject() as? SDWebImageOperation {
            downloadOperation.cancel()
        }
        
        downloadOperations.removeAllObjects()
    }
}

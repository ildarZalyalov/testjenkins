//
//  PhotoGalleryControllerFactory.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 02.02.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation
import AXPhotoViewer

/// Источник данных о фото
class PhotoGallerySource: NSObject {
    
    /// Используется одно из полей
    
    /// Байты данных фото
    var imageData: Data?
    
    /// Фото
    var image: UIImage?
    
    /// Ссылка на фото
    var url: URL?
    
    init(url: URL) {
        self.url = url
    }
    
    init(image: UIImage) {
        self.image = image
    }
}

extension PhotoGallerySource: AXPhotoProtocol {}

/// Фабрика создания экрана с галереей фото
protocol PhotoGalleryControllerFactory {
    
    /// Создать галерею из источников
    ///
    /// - Parameter sources: набор источников данных о фото
    /// - Returns: UIViewController галереи
    func createPhotoGallery(for sources: [PhotoGallerySource]) -> UIViewController
}

class PhotoGalleryControllerFactoryImplementation: PhotoGalleryControllerFactory {
    
    func createPhotoGallery(for sources: [PhotoGallerySource]) -> UIViewController {
        let dataSource = AXPhotosDataSource(photos: sources)
        return AXPhotosViewController(dataSource: dataSource, networkIntegration: PhotoGalleryNetworkingIntegration())
    }
}

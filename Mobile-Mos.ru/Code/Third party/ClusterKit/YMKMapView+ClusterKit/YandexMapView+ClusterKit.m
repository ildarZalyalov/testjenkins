//
//  YandexMapView+ClusterKit.m
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 07.11.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

#import <objc/runtime.h>
#import "YandexMapView+ClusterKit.h"

@implementation YMKPlacemarkMapObject (ClusterKit)

- (CKCluster *)cluster {
    return objc_getAssociatedObject(self, @selector(cluster));
}

- (void)setCluster:(CKCluster *)cluster {
    objc_setAssociatedObject(self, @selector(cluster), cluster, OBJC_ASSOCIATION_ASSIGN);
}

@end

@interface YMKMapView ()

@property (nonatomic, readonly) NSMapTable <CKCluster *, YMKPlacemarkMapObject *> *markers;
@property (nonatomic, weak) YMKPlacemarkMapObject *selectedMarker;

@end

@implementation YMKMapView (ClusterKit)

- (CKClusterManager *) clusterManager {
    
    CKClusterManager *clusterManager = objc_getAssociatedObject(self, @selector(clusterManager));
    if (!clusterManager) {
        clusterManager = [CKClusterManager new];
        clusterManager.map = self;
        objc_setAssociatedObject(self, @selector(clusterManager), clusterManager, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    }
    return clusterManager;
}

- (id<YMKMapViewDataSource>)dataSource {
    return objc_getAssociatedObject(self, @selector(dataSource));
}

- (void)setDataSource:(id<YMKMapViewDataSource>)dataSource {
    objc_setAssociatedObject(self, @selector(dataSource), dataSource, OBJC_ASSOCIATION_ASSIGN);
}

- (NSMapTable<CKCluster *,  YMKPlacemarkMapObject *> *)markers {
    NSMapTable *markers = objc_getAssociatedObject(self, @selector(markers));
    if (!markers) {
        markers = [NSMapTable strongToStrongObjectsMapTable];
        objc_setAssociatedObject(self, @selector(markers), markers, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    }

    return markers;
}

- (YMKPlacemarkMapObject *)markerForCluster:(CKCluster *)cluster {
    return [self.markers objectForKey:cluster];
}

- (MKMapRect)visibleMapRect {
    
    MKMapPoint sw = MKMapPointForCoordinate(CLLocationCoordinate2DMake(self.focusRegion.bottomLeft.latitude, self.mapWindow.focusRegion.bottomLeft.longitude));
    MKMapPoint ne = MKMapPointForCoordinate(CLLocationCoordinate2DMake(self.focusRegion.topRight.latitude, self.mapWindow.focusRegion.topRight.longitude));
    
    double x = sw.x;
    double y = ne.y;
    double width = ne.x - sw.x;
    double height = sw.y - ne.y;
    return MKMapRectMake(x, y, width, height);
}

- (double)zoom {
    
    MKCoordinateSpan span = MKCoordinateSpanMake(
                                                 self.focusRegion.topRight.latitude - self.focusRegion.bottomLeft.latitude,
                                                 self.focusRegion.topRight.longitude - self.focusRegion.bottomLeft.longitude);
    
    return [self worldZoomFrom:span];
}

- (void)addCluster:(CKCluster *)cluster {
    
    YMKPlacemarkMapObject *marker = nil;
    if ([self.dataSource respondsToSelector:@selector(mapView:markerForCluster:)]) {
        
        marker = [self.dataSource mapView:self markerForCluster: cluster];
    } else {
        YMKPoint *point = [YMKPoint pointWithLatitude:cluster.coordinate.latitude longitude:cluster.coordinate.longitude];
        
        marker = [self.mapWindow.map.mapObjects addPlacemarkWithPoint: point];
    }
    
    marker.cluster = cluster;
    marker.zIndex = 1;
    [self.markers setObject:marker forKey:cluster];
}

- (void)addClusters:(nonnull NSArray<CKCluster *> *)clusters {
    for (CKCluster *cluster in clusters) {
        [self addCluster:cluster];
    }
}

- (void)removeCluster:(CKCluster *)cluster {
    
    YMKPlacemarkMapObject *marker = [self.markers objectForKey:cluster];
    
    if (marker == nil) {
        [self.markers removeObjectForKey:cluster];
    }
    else {
        
        [self.mapWindow.map.mapObjects removeWithMapObject:marker];
        [self.markers removeObjectForKey:cluster];
    }
}

- (void)removeClusters:(NSArray<CKCluster *> *)clusters {
    for (CKCluster *cluster in clusters) {
        [self removeCluster:cluster];
    }
}

- (void)performAnimations:(NSArray<CKClusterAnimation *> *)animations completion:(void (^__nullable)(BOOL finished))completion {
    
    
    for (CKClusterAnimation *animation in animations) {
        animation.cluster.coordinate = animation.from;
    }
    
    void (^animationsBlock)(void) = ^{};
    
    for (CKClusterAnimation *animation in animations) {
        animationsBlock = ^{
            animationsBlock();
            animation.cluster.coordinate = animation.to;
        };
    }
    
    if ([self.clusterManager.delegate respondsToSelector:@selector(clusterManager:performAnimations:completion:)]) {
        
        [self.clusterManager.delegate clusterManager:self.clusterManager
                                   performAnimations:animationsBlock
                                          completion:^(BOOL finished) {
                                              if (completion) completion(finished);
                                          }];
    } else {
        [UIView animateWithDuration:self.clusterManager.animationDuration
                              delay:0
                            options:self.clusterManager.animationOptions
                         animations:animationsBlock
                         completion:completion];
    }
}

- (void)deselectCluster:(nonnull CKCluster *)cluster animated:(BOOL)animated {
    
    YMKPlacemarkMapObject *marker = [self.markers objectForKey:cluster];

    if (marker == self.selectedMarker) {
        self.selectedMarker = nil;
    }
}

- (void)selectCluster:(nonnull CKCluster *)cluster animated:(BOOL)animated {
    
    YMKPlacemarkMapObject *marker = [self.markers objectForKey:cluster];
    
    if (marker != self.selectedMarker) {
        [self.mapWindow.map.mapObjects addPlacemarkWithPoint:marker.geometry];
        self.selectedMarker = marker;
    }
}

#pragma mark - Private methods

- (YMKVisibleRegion *)focusRegion {
    return self.mapWindow.focusRegion;
}

- (YMKVisibleRegion *)visibleRegion {
    return [self.mapWindow.map visibleRegionWithCameraPosition:self.mapWindow.map.cameraPosition];
}

- (double)worldZoomFrom:(MKCoordinateSpan)span {
    
    //с помощью span текущей видимой области получаем zoom lvl
    return log2(360 * ((self.frame.size.width/256) / span.longitudeDelta));
}


@end


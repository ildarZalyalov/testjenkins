//
//  YandexMapView+ClusterKit.h
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 07.11.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ClusterKit.h"
#import <YandexMapKit/YMKMapKitFactory.h>
#import <YandexMapKit/YMKMapView.h>
#import <YandexMapKit/YMKMapObject.h>
#import <YandexMapKit/YMKPlacemarkMapObject.h>
#import <YandexMapKit/YMKMapWindow.h>
#import <YandexMapKit/YMKMap.h>
#import <YandexMapKit/YMKBoundingBox.h>

NS_ASSUME_NONNULL_BEGIN

@protocol YMKMapViewDataSource <NSObject>

- (__kindof YMKPlacemarkMapObject *)mapView:(YMKMapView *)mapView markerForCluster:(CKCluster *)cluster;

@end

@interface YMKPlacemarkMapObject (ClusterKit)

@property (nonatomic, weak, nullable) CKCluster *cluster;

@end

@interface YMKMapView (ClusterKit) <CKMap>

@property (nonatomic, weak) IBOutlet id<YMKMapViewDataSource> dataSource;

- (nullable __kindof YMKPlacemarkMapObject *)markerForCluster:(CKCluster *)cluster;

@end

NS_ASSUME_NONNULL_END

//
//  BasePulleyViewController.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 16.05.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import Pulley

/// Базовый UITabBarController
class BasePulleyViewController: PulleyViewController, DataTransferModuleController {
    
    let collapsedDrawerPositions: Set<PulleyPosition> = [.closed, .collapsed]
    
    var activeViewController: UIViewController {
        return collapsedDrawerPositions.contains(drawerPosition) ? primaryContentViewController : drawerContentViewController
    }
    
    //MARK: - DataTransferModuleController
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        performModuleConfigurationIfNeeded(for: segue)
    }
    
    func performSegue(with identifier: String, sender: AnyObject?, configurationClosure: ModuleConfigurationClosure?) {
        
        if let closure = configurationClosure {
            addModuleConfiguration(configurationClosure: closure, for: identifier)
        }
        
        performSegue(withIdentifier: identifier, sender: sender)
    }
    
    /// Обеспечивает управление текущей ориентацией, видом статус бара выбранному контроллеру
    
    override var shouldAutorotate: Bool {
        return activeViewController.shouldAutorotate
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return activeViewController.supportedInterfaceOrientations
    }
    
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return activeViewController.preferredInterfaceOrientationForPresentation
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return activeViewController.preferredStatusBarStyle
    }
    
    override var prefersStatusBarHidden: Bool {
        return activeViewController.prefersStatusBarHidden
    }
    
    override var preferredStatusBarUpdateAnimation: UIStatusBarAnimation {
        return activeViewController.preferredStatusBarUpdateAnimation
    }
}

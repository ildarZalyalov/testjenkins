//
//  NetworkingConstants.swift
//  Portable
//
//  Created by Ivan Erasov on 23.11.16.
//  Copyright © 2016. All rights reserved.
//

import Foundation

/// таймаут до получения ответа от сервера
let networkingTimeoutIntervalForRequest: TimeInterval = 15

/// таймаут до завершения работы с запросом
let networkingTimeoutIntervalForResource: TimeInterval = 40

/// таймаут до получения ответа от сервера для оплаты
let networkingPaymentTimeoutIntervalForRequest: TimeInterval = 60

/// таймаут до завершения работы с запросом для оплаты
let networkingPaymentTimeoutIntervalForResource: TimeInterval = 90

enum HTTPRequestContentType: String {
    case json = "application/json"
    case xml = "application/xml"
    case html = "text/html"
}

enum HTTPRequestError: Error {
    
    //валидация
    case statusCodeNotValid
    case contentTypeNotValid
    case emptyContentError
    
    //парсинг
    case jsonParsingNoResponseDataError
    case jsonParsingUnknownError
    
    //маппинг
    case jsonMappingNotExpectedStructure
}

//
//  HTTPDataRequestDelegate.swift
//  Portable
//
//  Created by Ivan Erasov on 08.11.16.
//  Copyright © 2016. All rights reserved.
//

import Foundation

class HTTPDataRequestDelegate: HTTPRequestDelegate, URLSessionDataDelegate {
    
    private var data = Data()

    override var taskData: Data? { return data }
    
    //MARK: - URLSessionDataDelegate
    
    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive data: Data) {
        self.data.append(data)
    }
}

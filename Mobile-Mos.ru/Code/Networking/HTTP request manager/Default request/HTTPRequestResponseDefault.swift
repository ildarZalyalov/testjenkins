//
//  HTTPRequestResponseDefault.swift
//  Portable
//
//  Created by Ivan Erasov on 09.11.16.
//  Copyright © 2016. All rights reserved.
//

import Foundation

struct HTTPRequestResponseDefault: HTTPRequestResponse {
    
    private let task: URLSessionTask
    private let responseData: Data?
    private let responseError: Error?
    
    init(with task: URLSessionTask, and delegate: HTTPTaskDelegate) {
        self.task = task
        self.responseData = delegate.taskData
        self.responseError = delegate.taskError
    }
    
    //MARK: - HTTPRequestResponse
    
    var urlRequest: URLRequest? { return task.originalRequest }
    
    var httpUrlResponse: HTTPURLResponse? {
        
        guard let response = task.response else {
            return nil
        }
        
        return response as? HTTPURLResponse
    }
    
    var data: Data? { return responseData }
    
    var error: Error? { return responseError }
}

//
//  HTTPRequestDefault.swift
//  Portable
//
//  Created by Ivan Erasov on 09.11.16.
//  Copyright © 2016. All rights reserved.
//

import Foundation

class HTTPRequestDefault: HTTPRequest {
    
    private let task: URLSessionTask
    private let delegate: HTTPTaskDelegate
    
    //MARK: - HTTPRequest
    
    init(with task: URLSessionTask, and delegate: HTTPTaskDelegate) {
        self.task = task
        self.delegate = delegate
    }
    
    var requestIdentifier: Int { return task.taskIdentifier }
    
    var originalUrlRequest: URLRequest? { return task.originalRequest }
    
    var currentUrlRequest: URLRequest? { return task.currentRequest }
    
    var requestDescription: String? { return task.currentRequest?.debugDescription }
    
    var error: Error? {
        get { return delegate.taskError }
        set { delegate.taskError = error }
    }
    
    var isRunning: Bool { return delegate.isTaskRunning }
    
    var isSuspended: Bool { return delegate.isTaskSuspended }
    
    func suspend() {
        delegate.suspendTask()
    }
    
    func resume() {
        delegate.resumeTask()
    }
    
    func cancel() {
        delegate.cancelTask()
    }
    
    func response(responseHandler: @escaping (HTTPRequestResponse) -> Void) -> Self {
        
        delegate.addTaskCompletionHandler { [weak self] in
            
            guard let strongSelf = self else {
                return
            }

            let response = HTTPRequestResponseDefault(with: strongSelf.task, and: strongSelf.delegate)
            responseHandler(response)
            
            return
        }
        
        return self
    }
}

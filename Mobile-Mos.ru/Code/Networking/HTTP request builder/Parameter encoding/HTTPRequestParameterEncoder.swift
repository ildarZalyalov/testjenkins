//
//  HTTPRequestParameterEncoder.swift
//  Portable
//
//  Created by Ivan Erasov on 09.02.16.
//  Copyright © 2016. All rights reserved.
//

import Foundation

/// Способ кодирования параметров в запрос
///
/// - url:          в виде query string
/// - httpBody:     в теле запроса
/// - json:         в теле запроса в виде json
/// - propertyList: в теле запроса в виде property list
/// - custom->:     свой способ (передается closure)
enum HTTPRequestParameterEncoding {
    case url
    case httpBody
    case json
    case propertyList(PropertyListSerialization.PropertyListFormat, PropertyListSerialization.WriteOptions)
    case custom((URLRequest, [String: Any]?) -> (URLRequest, Error?))
}

/// Результат работы кодировщика - запрос с параметрами и, возможно, ошибка
typealias HTTPRequestParameterEncoderResult = (request: URLRequest, error: Error?)

/// Протокол кодировщика параметров запроса
protocol HTTPRequestParameterEncoder {
    
    /// Используемый способ кодирования
    var encoding: HTTPRequestParameterEncoding { get set }
    /// Разделитель, отделяющий query string от url
    var queryStringDelimiter: String { get }
    /// Разделитель, отделяющий параметры внутри query string
    var queryStringComponentsDelimiter: String { get }
    
    /// Кодируем параметры в запрос
    ///
    /// - parameter request:    запрос
    /// - parameter parameters: параметры, которые хотим закодировать
    ///
    /// - returns: запрос с параметрами и (возможно) ошибка
    func encode(_ request: URLRequest, parameters: [String: Any]?) -> HTTPRequestParameterEncoderResult
    
    /// Кодируем параметры в query string
    ///
    /// - parameter parameters: параметры, которые хотим закодировать
    ///
    /// - returns: query string
    func encodeQueryString(_ parameters: [String: Any]) -> String
}

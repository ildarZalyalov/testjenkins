//
//  MappingOperator.swift
//  Portable
//
//  Created by Ivan Erasov on 15.12.15.
//  Copyright © 2015. All rights reserved.
//

import Foundation

//оператор маппинга
infix operator <<

public func << <MappedType>(left: inout MappedType?, right: MappedType) {
    left = right
}

public func << <MappedType>(left: inout MappedType, right: Any?) {

    //гарантируем вызов реализации оператора с left: MappedType?
    var leftInQuestion: MappedType? = left
    leftInQuestion << right
    
    if let leftValue = leftInQuestion {
        left = leftValue
    }
}

public func << <MappedType>(left: inout MappedType?, right: Any?) {
    
    //пробуем просто привести к нужному типу
    if let castedRight = right as? MappedType {
        left = castedRight
    }
    else if let number = right as? NSNumber {
        
        //справа у нас число, если слева строка, то делаем преобразование
        if let _ = left as? String {
            left = number.stringValue as? MappedType
        }
        else if let _ = left as? String? {
            left = number.stringValue as? MappedType
        }
    }
    else if let string = right as? String {
        
        //справа у нас строка, слева НЕ строка
        //пытаемся привести по очереди к Int64 и Double (чтобы не потерять точность), кладем в NSNumber
        //при приведении к типу, куда маппим NSNumber преобразуется в число нужного типа или в nil
        if let longLongValue = Int64(string) {
            let number = NSNumber(value: longLongValue)
            left = number as? MappedType
        }
        else if let doubleValue = Double(string) {
            let number = NSNumber(value: doubleValue)
            left = number as? MappedType
        }
    }
}

//операторы с переданным преобразователем значений, тут все просто - преобразователь выполняет всю работу

public func << <MappedType, Transform: MappedValueTransform>(left: inout MappedType, right: (Any?, Transform)) where Transform.MappedValueType == MappedType {
    if let transformedRight = right.1.transformToMappedType(right.0) {
        left = transformedRight
    }
}

public func << <MappedType, Transform: MappedValueTransform>(left: inout MappedType?, right: (Any?, Transform)) where Transform.MappedValueType == MappedType {
    let transformedRight: MappedType? = right.1.transformToMappedType(right.0)
    left = transformedRight
}

public func << <MappedType, Transform: MappedValueTransform>(left: inout [MappedType], right: (Any?, Transform)) where Transform.MappedValueType == MappedType {
    
    //гарантируем вызов реализации оператора с left: MappedType?
    var leftInQuestion: [MappedType]? = left
    leftInQuestion << right
    
    //а почему? а потому, что в милом нашему сердцу Swift полно типов, передающихся не по ссылке :)
    if let leftValue = leftInQuestion {
        left = leftValue
    }
}

public func << <MappedType, Transform: MappedValueTransform>(left: inout [MappedType]?, right: (Any?, Transform)) where Transform.MappedValueType == MappedType {
    
    if let array = right.0 as? NSArray {

        left = [MappedType]()
        
        for object in array {
            let mappedObject = right.1.transformToMappedType(object)
            if mappedObject != nil {
                left?.append(mappedObject!)
            }
        }
    }
}

//
//  KeychainAccessStorage.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 16.01.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation
import KeychainAccess

/// Реализация хранилища данных в Keychain при помощи библиотеки KeychainAccess
class KeychainAccessStorage: KeychainStorage {
    
    /// Очередь для чтения из Keychain не на главном потоке
    fileprivate static var backGroundReadQueue: OperationQueue = {
        var queue = OperationQueue()
        queue.name = "backGroundReadQueue"
        return queue
    }()
    
    /// Очередь для записи в Keychain не на главном потоке
    fileprivate static var backGroundWriteQueue: OperationQueue = {
        var queue = OperationQueue()
        queue.name = "backGroundWriteQueue"
        return queue
    }()
    
    /// Очередь для удаления из Keychain не на главном потоке
    fileprivate static var backGroundDeleteQueue: OperationQueue = {
        var queue = OperationQueue()
        queue.name = "backGroundDeleteQueue"
        return queue
    }()
    
    //MARK: - Приватные методы
    
    /// Получить значение возможности доступа к записи для библиотеки KeychainAccess
    ///
    /// - Parameter accessibility: исходное значение возможности доступа к записи
    /// - Returns: значение возможности доступа к записи для библиотеки KeychainAcces
    fileprivate func keychainAccessAccessibility(from accessibility: KeychainNewEntryContext.Accessibility) -> Accessibility {
        switch accessibility {
        case .whenUnlocked:
            return .whenUnlocked
        case .afterFirstUnlock:
            return .afterFirstUnlock
        case .always:
            return .always
        case .whenPasscodeSetThisDeviceOnly:
            return .whenPasscodeSetThisDeviceOnly
        case .whenUnlockedThisDeviceOnly:
            return .whenUnlockedThisDeviceOnly
        case .afterFirstUnlockThisDeviceOnly:
            return .afterFirstUnlockThisDeviceOnly
        case .alwaysThisDeviceOnly:
            return .alwaysThisDeviceOnly
        }
    }
    
    /// Получить значение режима аутентификации для доступа к записи для библиотеки KeychainAccess
    ///
    /// - Parameter policy: исходное значение режима аутентификации для доступа к записи
    /// - Returns: значение режима аутентификации для доступа к записи для библиотеки KeychainAccess
    fileprivate func keychainAccessAuthenticationPolicy(from policy: KeychainNewEntryContext.AuthenticationPolicy) -> AuthenticationPolicy {
        switch policy {
        case .userPresence:
            return .userPresence
        case .biometricsAny:
            return .touchIDAny
        case .biometricsCurrentSetup:
            return .touchIDCurrentSet
        case .devicePasscode:
            return .devicePasscode
        }
    }
    
    /// Получить объект доступа к Keychain из библиотеки KeychainAccess
    ///
    /// - Parameter context: контекст создания записи
    /// - Returns: объект доступа к Keychain из библиотеки KeychainAccess
    fileprivate func keychainAccess(from context: KeychainNewEntryContext) -> Keychain {
        if let accessGroup = context.accessGroup {
            return Keychain(service: context.service, accessGroup: accessGroup)
        }
        else {
            return Keychain(service: context.service)
        }
    }
    
    /// Получить объект доступа к Keychain из библиотеки KeychainAccess
    ///
    /// - Parameter context: контекст поиска записи
    /// - Returns: объект доступа к Keychain из библиотеки KeychainAccess
    fileprivate func keychainAccess(from context: KeychainEntryContext) -> Keychain {
        if let accessGroup = context.accessGroup {
            return Keychain(service: context.service, accessGroup: accessGroup)
        }
        else {
            return Keychain(service: context.service)
        }
    }
    
    //MARK: - KeychainStorage
    
    func saveEntry(with context: KeychainNewEntryContext, for key: String) throws {
        
        var keychain = keychainAccess(from: context)
        
        if let label = context.label {
            keychain = keychain.label(label)
        }
        
        if let comment = context.comment {
            keychain = keychain.comment(comment)
        }
        
        keychain = keychain.synchronizable(context.synchronizable)
        
        if let policy = context.authenticationPolicy {
            keychain = keychain.accessibility(keychainAccessAccessibility(from: context.accessibility), authenticationPolicy: keychainAccessAuthenticationPolicy(from: policy))
        }
        else {
            keychain = keychain.accessibility(keychainAccessAccessibility(from: context.accessibility))
        }
        
        if let prompt = context.authenticationPrompt {
            keychain = keychain.authenticationPrompt(prompt)
        }
        
        // KeychainNewEntryContext не дает конструктора, в котором можно не задать значение, поэтому нет проверок
        
        if let value = context.value {
            try keychain.set(value, key: key)
        }
        else if let data = context.data {
            try keychain.set(data, key: key)
        }
    }
    
    func saveEntryAsync(with context: KeychainNewEntryContext, for key: String, completion: KeychainStorageOperationResultBlock?) {
        
        let backGroundWriteQueue = KeychainAccessStorage.backGroundWriteQueue
        backGroundWriteQueue.addOperation { [weak self] in
            
            guard let strongSelf = self else { return }
            
            var result: KeychainStorageOperationResult
            
            defer {
                if completion != nil {
                    DispatchQueue.main.async {
                        completion!(result)
                    }
                }
            }
            
            do {
                try strongSelf.saveEntry(with: context, for: key)
                result = .success
            }
            catch(let error) {
                result = .error(error)
            }
        }
    }
    
    func fetchData(with context: KeychainEntryContext) throws -> Data? {
        let keychain = keychainAccess(from: context)
        return try keychain.getData(context.key)
    }
    
    func fetchValue(with context: KeychainEntryContext) throws -> String? {
        let keychain = keychainAccess(from: context)
        return try keychain.getString(context.key)
    }
    
    func fetchEntryAttributes(with context: KeychainEntryContext) -> KeychainEntryAttributes? {
        
        let keychain = keychainAccess(from: context)
        guard let attrs = keychain[attributes: context.key] else {
            return nil
        }
        
        return KeychainEntryAttributes(data: attrs.data,
                                       reference: attrs.ref,
                                       persistentReference: attrs.persistentRef,
                                       accessible: attrs.accessible,
                                       accessControl: attrs.accessControl,
                                       accessGroup: attrs.accessGroup,
                                       synchronizable: attrs.synchronizable,
                                       creationDate: attrs.creationDate,
                                       modificationDate: attrs.modificationDate,
                                       attributeDescription: attrs.attributeDescription,
                                       comment: attrs.comment,
                                       creator: attrs.creator,
                                       type: attrs.type,
                                       label: attrs.label,
                                       service: attrs.service)
    }
    
    func fetchDataAsync(with context: KeychainEntryContext, completion: @escaping KeychainStorageFetchResultBlock<Data>) {
        
        let backGroundReadQueue = KeychainAccessStorage.backGroundReadQueue
        backGroundReadQueue.addOperation { [weak self] in
            
            guard let strongSelf = self else { return }
            
            var result: KeychainStorageFetchResult<Data>
            
            defer {
                DispatchQueue.main.async {
                    completion(result)
                }
            }
            
            do {
                let data = try strongSelf.fetchData(with: context)
                result = .success(data)
            }
            catch(let error) {
                result = .error(error)
            }
        }
    }
    
    func fetchValueAsync(with context: KeychainEntryContext, completion: @escaping KeychainStorageFetchResultBlock<String>) {
        
        let backGroundReadQueue = KeychainAccessStorage.backGroundReadQueue
        backGroundReadQueue.addOperation { [weak self] in
            
            guard let strongSelf = self else { return }
            
            var result: KeychainStorageFetchResult<String>
            
            defer {
                DispatchQueue.main.async {
                    completion(result)
                }
            }
            
            do {
                let value = try strongSelf.fetchValue(with: context)
                result = .success(value)
            }
            catch(let error) {
                result = .error(error)
            }
        }
    }
    
    func fetchEntryAttributesAsync(with context: KeychainEntryContext, completion: @escaping KeychainStorageFetchResultBlock<KeychainEntryAttributes>) {
        
        let backGroundReadQueue = KeychainAccessStorage.backGroundReadQueue
        backGroundReadQueue.addOperation { [weak self] in
            
            guard let strongSelf = self else { return }
            
            let attributes = strongSelf.fetchEntryAttributes(with: context)
            let result: KeychainStorageFetchResult<KeychainEntryAttributes> = .success(attributes)
            
            DispatchQueue.main.async {
                completion(result)
            }
        }
    }
    
    func deleteEntry(for context: KeychainEntryContext) throws {
        let keychain = keychainAccess(from: context)
        try keychain.remove(context.key)
    }
    
    func deleteEntryAsync(for context: KeychainEntryContext, completion: KeychainStorageOperationResultBlock?) {
        
        let backGroundDeleteQueue = KeychainAccessStorage.backGroundDeleteQueue
        backGroundDeleteQueue.addOperation { [weak self] in
            
            guard let strongSelf = self else { return }
            
            var result: KeychainStorageOperationResult
            
            defer {
                if completion != nil {
                    DispatchQueue.main.async {
                        completion!(result)
                    }
                }
            }
            
            do {
                try strongSelf.deleteEntry(for: context)
                result = .success
            }
            catch(let error) {
                result = .error(error)
            }
        }
    }
}

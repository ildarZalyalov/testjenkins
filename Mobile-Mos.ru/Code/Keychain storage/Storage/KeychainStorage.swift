//
//  KeychainStorage.swift
//  Portable
//
//  Created by Ivan Erasov on 15.01.2018.
//  Copyright © 2018. All rights reserved.
//

import Foundation

/// Результат операции в Keychain
///
/// - success: успех
/// - error: ошибка
enum KeychainStorageOperationResult {
    case success
    case error(Error)
}

/// Результат поиска записи в Keychain
///
/// - success: успех, связанное значение - данные найденной записи или nil
/// - error: ошибка
enum KeychainStorageFetchResult<EntryType> {
    case success(EntryType?)
    case error(Error)
}

/// Обработчик завершения операции в Keychain
typealias KeychainStorageOperationResultBlock = (KeychainStorageOperationResult) -> Void

/// Обработчик завершения поиска записи в Keychain
typealias KeychainStorageFetchResultBlock<EntryType> = (KeychainStorageFetchResult<EntryType>) -> Void

/// Хранилище данных в Keychain
protocol KeychainStorage {
    
    /// Сохранить запись
    ///
    /// - Parameters:
    ///   - context: контекст новой записи
    ///   - key: идентификатор записи
    /// - Throws: ошибка сохранения записи
    func saveEntry(with context: KeychainNewEntryContext, for key: String) throws
    
    /// Асинхронно сохранить запись
    ///
    /// - Parameters:
    ///   - context: контекст новой записи
    ///   - key: идентификатор записи
    ///   - completion: обработчик завершения операции в Keychain
    func saveEntryAsync(with context: KeychainNewEntryContext, for key: String, completion: KeychainStorageOperationResultBlock?)

    /// Найти данные в Keychain
    ///
    /// - Parameter context: контекст записи
    /// - Returns: данные или nil, если нет связанной с контекстом записи
    /// - Throws: ошибка доступа к записи
    func fetchData(with context: KeychainEntryContext) throws -> Data?
    
    /// Найти значение в Keychain
    ///
    /// - Parameter context: контекст записи
    /// - Returns: значение или nil, если нет связанной с контекстом записи
    /// - Throws: ошибка доступа к записи
    func fetchValue(with context: KeychainEntryContext) throws -> String?
    
    /// Получить атрибуты записи в Keychain
    ///
    /// - Parameter context: контекст записи
    /// - Returns: атрибуты или nil, если нет связанной с контекстом записи
    func fetchEntryAttributes(with context: KeychainEntryContext) -> KeychainEntryAttributes?
    
    /// Асинхронно найти данные в Keychain
    ///
    /// - Parameters:
    ///   - context: контекст записи
    ///   - completion: обработчик завершения поиска записи в Keychain
    func fetchDataAsync(with context: KeychainEntryContext, completion: @escaping KeychainStorageFetchResultBlock<Data>)
    
    /// Асинхронно найти значение в Keychain
    ///
    /// - Parameters:
    ///   - context: контекст записи
    ///   - completion: обработчик завершения поиска записи в Keychain
    func fetchValueAsync(with context: KeychainEntryContext, completion: @escaping KeychainStorageFetchResultBlock<String>)
    
    /// Асинхронно получить атрибуты записи в Keychain
    ///
    /// - Parameters:
    ///   - context: контекст записи
    ///   - completion: обработчик завершения поиска записи в Keychain
    func fetchEntryAttributesAsync(with context: KeychainEntryContext, completion: @escaping KeychainStorageFetchResultBlock<KeychainEntryAttributes>)
    
    /// Удалить запись в Keychain
    ///
    /// - Parameter context: контекст записи
    /// - Throws: ошибка удаления записи
    func deleteEntry(for context: KeychainEntryContext) throws
    
    /// Асинхронно удалить запись в Keychain
    ///
    /// - Parameters:
    ///   - context: контекст записи
    ///   - completion: обработчик завершения операции в Keychain
    func deleteEntryAsync(for context: KeychainEntryContext, completion: KeychainStorageOperationResultBlock?)
}

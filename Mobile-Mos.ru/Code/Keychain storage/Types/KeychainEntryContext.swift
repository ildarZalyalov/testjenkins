//
//  KeychainEntryContext.swift
//  Portable
//
//  Created by Ivan Erasov on 16.01.2018.
//  Copyright © 2018. All rights reserved.
//

import Foundation

/// Контекст для идентификации записи в Keychain
struct KeychainEntryContext {
    
    /// Сервис, для которого добавляется запись
    let service: String
    
    /// Идентификатор записи
    let key: String
    
    /// Группа доступа к записи
    var accessGroup: String?
    
    init(with service: String, and key: String) {
        self.service = service
        self.key = key
    }
}

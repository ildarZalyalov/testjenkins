//
//  KeychainEntryAttributes.swift
//  Portable
//
//  Created by Ivan Erasov on 16.01.2018.
//  Copyright © 2018. All rights reserved.
//

import Foundation

/// Атрибуты записи в Keychain
struct KeychainEntryAttributes {
    
    /// Данные записи
    var data: Data?
    
    /// Ссылка на запись
    var reference: Data?
    
    /// Постоянная ссылка на запись
    var persistentReference: Data?
    
    /// Доступна ли запись в текущих условиях
    var accessible: String?
    
    /// Возможность доступа к записи
    var accessControl: SecAccessControl?
    
    /// Группа доступа к записи
    var accessGroup: String?
    
    /// Синхронизируется ли с iCloud Keychain
    var synchronizable: Bool?
    
    /// Дата создания записи
    var creationDate: Date?
    
    /// Дата модификации записи
    var modificationDate: Date?
    
    /// Описание атрибутов
    var attributeDescription: String?
    
    /// Комментарий к записи
    var comment: String?
    
    /// Создатель записи
    var creator: String?
    
    /// Тип записи
    var type: String?
    
    /// Пометка для записи
    var label: String?
    
    /// Сервис, для которого добавлена запись
    var service: String?
}

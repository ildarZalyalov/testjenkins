//
//  YandexGeocoderResponseMapping.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 19.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

extension YandexGeocoderBoundedBy: MappableJsonResource {
    
    mutating func mapFromJson(_ json: NSDictionary) {
        
        var lowerCornerString: String?
        var upperCornerString: String?
        
        lowerCornerString << json["lowerCorner"]
        upperCornerString << json["upperCorner"]
        
        guard lowerCornerString != nil && upperCornerString != nil else {
            return
        }
        
        let separator = " "
        let lowerCornerSplit = lowerCornerString!.components(separatedBy: separator)
        let upperCornerSplit = upperCornerString!.components(separatedBy: separator)
        
        guard lowerCornerSplit.count == 2 && upperCornerSplit.count == 2 else {
            return
        }
        
        let lowerCornerLatitude = Double(lowerCornerSplit.last!)
        let lowerCornerLongitude = Double(lowerCornerSplit.first!)
        let upperCornerLatitude = Double(upperCornerSplit.last!)
        let upperCornerLongitude = Double(upperCornerSplit.first!)
        
        guard lowerCornerLatitude != nil &&
            lowerCornerLongitude != nil &&
            upperCornerLatitude != nil &&
            upperCornerLongitude != nil else {
            return
        }
        
        lowerCorner = Location(latitude: lowerCornerLatitude!, longitude: lowerCornerLongitude!)
        upperCorner = Location(latitude: upperCornerLatitude!, longitude: upperCornerLongitude!)
    }
}

extension YandexGeocoderResponse: MappableJsonResource {
    
    mutating func mapFromJson(_ json: NSDictionary) {
        
        guard let resultsJson = json["response", "GeoObjectCollection", "featureMember"] as? [NSDictionary] else { return }
        guard let firstResultsJson = resultsJson.first else { return }
        
        text << firstResultsJson["GeoObject", "metaDataProperty", "GeocoderMetaData", "text"]
        boundedBy << firstResultsJson["GeoObject", "boundedBy", "Envelope"]
    }
}

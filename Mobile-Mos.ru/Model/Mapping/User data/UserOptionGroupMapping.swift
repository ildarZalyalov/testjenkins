//
//  UserOptionGroupMapping.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 20.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

extension UserOptionGroup: MappableJsonResource {
    
    mutating func mapFromJson(_ json: NSDictionary) {
        itemId << json["group_id"]
        type << json["group_type"]
        name << json["group_name"]
        description << json["group_description"]
        alias << json["group_alias"]
        options << json["options"]
    }
}

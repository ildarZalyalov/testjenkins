//
//  UserOptionMapping.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 20.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

extension UserOption: MappableJsonResource {
    
    mutating func mapFromJson(_ json: NSDictionary) {
        itemId << json["option_id"]
        type << json["type"]
        value << json["value"]
    }
}

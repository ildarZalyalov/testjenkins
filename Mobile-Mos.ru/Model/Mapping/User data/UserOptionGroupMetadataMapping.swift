//
//  UserOptionGroupMetadataMapping.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 20.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

extension UserOptionGroupMetadata: MappableJsonResource {
    
    mutating func mapFromJson(_ json: NSDictionary) {
        type << json["type"]
        name << json["name"]
        isAliasAllowed << json["alias_allowed"]
        aliasName << json["alias_name"]
        aliasHelpText << json["alias_help_text"]
        aliasMaxLength << json["alias_max_length"]
        description << json["description"]
        options << json["options"]
        statsData << json["stats_data"]
    }
}

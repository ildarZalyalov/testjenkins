//
//  UserOptionsObtainResponseMapping.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 20.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

extension UserOptionsObtainResponse: MappableJsonResource {
    
    mutating func mapFromJson(_ json: NSDictionary) {
        optionsData << json["user_info"]
        errorCode << json["error_code"]
        errorMessage << json["error_msg"]
    }
}

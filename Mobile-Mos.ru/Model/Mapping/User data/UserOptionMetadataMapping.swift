//
//  UserOptionMetadataMapping.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 20.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

extension UserOptionMetadata: MappableJsonResource {
    
    mutating func mapFromJson(_ json: NSDictionary) {
        type << json["type"]
        name << json["name"]
        regex << json["regexp"]
        example << json["example"]
        keyboard << (json["keyboard"], MapToEnumTransform<UserOptionMetadata.Keyboard>())
        helpText << json["help_text"]
        description << json["description"]
        dateFormat << json["date_format"]
        mandatory << json["mandatory"]
        mandatoryUnless << json["mandatory_unless"]
        multiple << json["multiple"]
        unique << json["unique"]
        uniqueProfile << json["unique_profile"]
        statsData << json["stats_data"]
    }
}

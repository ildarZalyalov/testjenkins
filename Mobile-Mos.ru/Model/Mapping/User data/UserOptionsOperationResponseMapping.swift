//
//  UserOptionsOperationResponseMapping.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 21.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

extension UserOptionsOperationResponse: MappableJsonResource {
    
    mutating func mapFromJson(_ json: NSDictionary) {
        errorCode << json["error_code"]
        errorMessage << json["error_msg"]
    }
}

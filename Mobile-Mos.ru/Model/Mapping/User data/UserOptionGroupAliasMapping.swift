//
//  UserOptionGroupAliasMapping.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 23.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

extension UserOptionGroupAlias: MappableJsonResource {
    
    mutating func mapFromJson(_ json: NSDictionary) {
        itemId << json["option_id"]
        value << json["value"]
    }
}

//
//  UserOptionsDataMapping.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 20.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

extension UserOptionsData: MappableJsonResource {
    
    mutating func mapFromJson(_ json: NSDictionary) {
        groupsMetadata << json["groups_metadata"]
        optionsMetadata << json["options_metadata"]
        groups << json["groups"]
        options << json["options"]
        grouping << json["grouping"]
    }
}

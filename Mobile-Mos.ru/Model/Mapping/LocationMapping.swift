//
//  LocationMapping.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 17.08.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

extension Location: MappableJsonResource {
    
    mutating func mapFromJson(_ json: NSDictionary) {
        latitude << json["latitude"]
        longitude << json["longitude"]
    }
}

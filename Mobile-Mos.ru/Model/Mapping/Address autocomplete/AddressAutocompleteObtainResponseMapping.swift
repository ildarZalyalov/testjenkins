//
//  AddressAutocompleteObtainResponseMapping.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 02.04.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

extension AddressAutocompleteObtainResponse: MappableJsonResource {
    
    mutating func mapFromJson(_ json: NSDictionary) {
        addresses << json["result"]
        errorCode << json["error_code"]
        errorMessage << json["error_msg"]
    }
}

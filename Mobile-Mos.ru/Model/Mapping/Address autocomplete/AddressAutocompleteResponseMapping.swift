//
//  AddressAutocompleteResponseMapping.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 02.04.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

extension AddressAutocompleteResponse: MappableJsonResource {
    
    mutating func mapFromJson(_ json: NSDictionary) {
        address << json["name"]
        addressData << json["value"]
    }
}

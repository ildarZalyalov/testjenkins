//
//  NewsSearchMapping.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 25.01.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

extension NewsSearchModel: MappableJsonResource {
    
    mutating func mapFromJson(_ json: NSDictionary) {
        
        title          << json["title"]
        imageStringUrl << json["image"]
        newsStringUrl  << json["url"]
        
        if let typeString = json["index"] as? String, let newsType = NewsType.init(rawValueOptional: typeString) {
            type = newsType
        }
        
        if let dateSeconds = json["date"] as? Int, let timeInterval = TimeInterval(exactly: dateSeconds) {
            date = Date(timeIntervalSince1970: timeInterval)
        }
        
        if let dateSeconds = json["date_from_timestamp"] as? Int, let timeInterval = TimeInterval(exactly: dateSeconds) {
            dateFrom = Date(timeIntervalSince1970: timeInterval)
        }
        
    }
}



//
//  SearchTopWordsMapping.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 01.02.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

extension SearchTopWords: MappableJsonResource {
    
    mutating func mapFromJson(_ json: NSDictionary) {
        
        title << json["name"]
    }
}

//
//  PaymentRedirectInfoMapping.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 18.05.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

extension PaymentRedirectInfo: MappableJsonResource {
    
    mutating func mapFromJson(_ json: NSDictionary) {
        redirectUrl << (json["LocationPost"], MapToUrlFromStringTransform())
        paReq << json["PaReq"]
        md << json["MD"]
        terminationUrl << (json["TermUrl"], MapToUrlFromStringTransform())
    }
}

//
//  PaymentClientNameMapping.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 17.05.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

extension PaymentClientName: MappableJsonResource {
    
    mutating func mapFromJson(_ json: NSDictionary) {
        firstName << json["firstName"]
        lastName << json["lastName"]
        middleName << json["middleName"]
    }
}

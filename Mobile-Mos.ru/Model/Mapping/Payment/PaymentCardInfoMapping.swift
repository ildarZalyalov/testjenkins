//
//  PaymentCardInfoMapping.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 17.05.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

extension PaymentCardInfo: MappableJsonResource {
    
    mutating func mapFromJson(_ json: NSDictionary) {
        expiryDate << json["expiryDate"]
        cardholderName << json["cardholderName"]
        pan << json["pan"]
        cvvc << json["cvvc"]
    }
}

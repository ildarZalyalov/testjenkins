//
//  PaymentCardMapping.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 17.05.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

extension PaymentCard: MappableJsonResource {
    
    mutating func mapFromJson(_ json: NSDictionary) {
        cardId << json["id"]
        type << (json["type"], MapToEnumTransform<PaymentCardType>())
        title << json["title"]
        needRegister << json["needRegister"]
        cardInfo << json["cardInfo"]
        extendedCardInfo << json["extendedCardInfo"]
    }
}

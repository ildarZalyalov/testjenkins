//
//  PaymentComissionMapping.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 17.05.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

extension PaymentComission: MappableJsonResource {
    
    mutating func mapFromJson(_ json: NSDictionary) {
        socialCard << json["commissionSCM"]
        mtsCard << json["commissionMTS"]
        bankCard << json["commissionCard"]
    }
}

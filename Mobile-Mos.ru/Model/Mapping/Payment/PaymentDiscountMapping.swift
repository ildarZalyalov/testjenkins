//
//  PaymentDiscountMapping.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 17.05.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

extension PaymentDiscount: MappableJsonResource {
    
    mutating func mapFromJson(_ json: NSDictionary) {
        percent << json["amount"]
        date << (json["date"], MapToDateWithCustomFormatTransform(formatString: "yyyy-MM-dd'T'HH:mm:ss"))
    }
}

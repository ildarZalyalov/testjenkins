//
//  PaymentCardExtendedInfoMapping.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 17.05.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

extension PaymentCardExtendedInfo: MappableJsonResource {
    
    mutating func mapFromJson(_ json: NSDictionary) {
        bankId << json["id"]
        bankDescription << json["bankDescr"]
        brand << (json["brand"], MapToEnumTransform<PaymentCardExtendedInfo.Brand>())
        type << (json["type"], MapToEnumTransform<PaymentCardType>())
        bankFillingColor << (json["fillingColor"], MapToUIColorFromHexStringTransform())
        bankTextColor << (json["textColor"], MapToUIColorFromHexStringTransform())
        bankLogoUrl << (json["logoFile"], MapToUrlFromStringTransform())
    }
}

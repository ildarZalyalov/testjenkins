//
//  PaymentMapping.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 17.05.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

extension Payment: MappableJsonResource {
    
    mutating func mapFromJson(_ json: NSDictionary) {
        paymentId << json["paymentUID"]
        status << (json["status"], MapToEnumTransform<Payment.Status>())
        narrative << json["narrative"]
        isWithInsurance << json["withInsurance"]
        clientName << json["clientName"]
        amount << json["amount"]
        discount << json["discount"]
        comission << json["commission"]
        comissionWithInsurance << json["addCommission"]
        failReason << json["reason"]
        failReasonDescription << json["reasonFail"]
    }
}

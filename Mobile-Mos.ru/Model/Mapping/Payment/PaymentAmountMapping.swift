//
//  PaymentAmountMapping.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 17.05.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

extension PaymentAmount: MappableJsonResource {
    
    mutating func mapFromJson(_ json: NSDictionary) {
        sum << json["amount"]
        sumWithInsurance << json["addAmount"]
        sumBeforeDiscount << json["oldAmount"]
        comment << json["amountComment"]
    }
}

//
//  PaymentBatchMapping.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 17.05.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

extension PaymentBatch: MappableJsonResource {
    
    mutating func mapFromJson(_ json: NSDictionary) {
        batchId << json["batchId"]
        status << (json["batchStatus"], MapToEnumTransform<PaymentBatch.Status>())
        payments << json["payments"]
    }
}

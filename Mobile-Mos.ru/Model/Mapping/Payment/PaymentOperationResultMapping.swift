//
//  PaymentOperationResultMapping.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 17.05.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

extension PaymentOperationResult: MappableJsonResource {
    
    mutating func mapFromJson(_ json: NSDictionary) {
        status << (json["result"], MapToEnumTransform<PaymentOperationResult.Status>())
        newTitle << json["newCardTitle"]
    }
}

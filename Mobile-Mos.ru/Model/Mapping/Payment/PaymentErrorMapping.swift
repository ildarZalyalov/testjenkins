//
//  PaymentErrorMapping.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 16.05.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

extension PaymentError: MappableJsonResource {
    
    mutating func mapFromJson(_ json: NSDictionary) {
        code << json["errorCode"]
        description << json["errorDescription"]
    }
}

//
//  WeatherMapping.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 10.04.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

extension Weather: MappableJsonResource {
    
    mutating func mapFromJson(_ json: NSDictionary) {
        
       if let temperature = json["Temperature"] as? NSDictionary,
          let metric = temperature["Metric"] as? NSDictionary,
          let celciumTemperature = metric["Value"] as? Double {
        
            temperatureInCelcius = Int(celciumTemperature.rounded())
        }
        
        weatherIconId << json["WeatherIcon"]
        weatherText << json["WeatherText"]
    }
    
}

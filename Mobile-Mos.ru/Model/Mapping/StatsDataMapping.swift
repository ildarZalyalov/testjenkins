//
//  StatsDataMapping.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 02.07.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

extension StatsData: MappableJsonResource {
    
    mutating func mapFromJson(_ json: NSDictionary) {
        
        eventName     << json["event_name"]
        eventFunction << json["event_function"]
        
        if let json = json["event_json"] as? NSDictionary {
            
            do {
                let jsonData = try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
                eventJson = String(bytes: jsonData, encoding: String.Encoding.utf8) ?? ""
            } catch {
                print(error.localizedDescription)
            }
        }
    }
}

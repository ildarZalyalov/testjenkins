//
//  ServiceStatsDataMapping.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 03.07.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

extension ServiceStatsData: MappableJsonResource {
    
    mutating func mapFromJson(_ json: NSDictionary) {
        
        editEventName << json["edit_event_name"]
        addEventName  << json["add_event_name"]
    }
}

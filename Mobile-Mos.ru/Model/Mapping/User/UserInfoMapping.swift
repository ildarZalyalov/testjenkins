//
//  UserInfoMapping.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 24.11.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

extension UserInfo: MappableJsonResource {
    
    mutating func mapFromJson(_ json: NSDictionary) {
        email << json["email"]
        firstName << json["first_name"]
        lastName << json["last_name"]
        patronymicName << json["patronymic_name"]
        birthDate << (json["birth_date"], MapToDateWithCustomFormatTransform(formatString: "yyyy-MM-dd"))
        phone << json["phone"]
        snils << json["snils"]
        serviceInfo << json["groups"]
    }
}

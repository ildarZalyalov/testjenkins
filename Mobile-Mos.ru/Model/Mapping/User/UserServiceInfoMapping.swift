//
//  UserServiceInfoMapping.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 21.02.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

extension UserServiceInfo: MappableJsonResource {
    
    mutating func mapFromJson(_ json: NSDictionary) {
        title << json["name"]
        slug << json["slug"]
        chatId << json["chat_id"]
    }
}

//
//  UserInfoResponseMapping.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 24.11.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

extension UserInfoResponse: MappableJsonResource {
    
    mutating func mapFromJson(_ json: NSDictionary) {
        userInfo << json["user_info"]
        errorCode << json["error_code"]
        errorMessage << json["error_msg"]
    }
}

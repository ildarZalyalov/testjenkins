//
//  ChatMessageDocumentObjectMapping.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 12.09.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

extension ChatMessageDocumentObject: MappableJsonResource {
    
    mutating func mapFromJson(_ json: NSDictionary) {
        fileUrl << (json["url"], MapToUrlFromStringTransform())
        fileName << json["file_name"]
        fileSize << json["file_size"]
        statsData << json["stats_data"]
    }
}

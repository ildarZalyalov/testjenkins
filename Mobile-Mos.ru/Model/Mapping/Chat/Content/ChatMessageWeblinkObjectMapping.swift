//
//  ChatMessageWeblinkObjectMapping.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 12.09.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

extension ChatMessageWeblinkObject: MappableJsonResource {
    
    mutating func mapFromJson(_ json: NSDictionary) {
        linkUrl << (json["url"], MapToUrlFromStringTransform())
        type << (json["type"], MapToEnumTransform<ChatMessageWeblinkObject.LinkType>())
        disablePreview << json["disable_preview"]
        preview << json["preview"]
        statsData << json["stats_data"]
    }
}

//
//  ChatMessageScrollObjectMapping.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 21.09.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

extension ChatMessageScrollObject: MappableJsonResource {
    
    mutating func mapFromJson(_ json: NSDictionary) {
        type << (json["display_type"], MapToEnumTransform<ChatMessageScrollObject.EmbeddingType>())
        messages << json["messages"]
    }
}

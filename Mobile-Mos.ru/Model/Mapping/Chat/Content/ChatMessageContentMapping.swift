//
//  ChatMessageContentMapping.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 13.06.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

extension ChatMessageContent: MappableJsonResource {
    
    mutating func mapFromJson(_ json: NSDictionary) {
        
        var messageType: ChatMessageContent.ContentType = .text
        
        text << json["text"]
        
        if let locationJson = json["location"] as? NSDictionary {
            
            mapObject = ChatMessageMapObject()
            mapObject << locationJson
            
            var location = Location()
            location << locationJson
            
            mapObject?.location = location
        }
        
        photoObject << json["photo"]
        weblinkObject << json["weblink"]
        documentObject << json["document"]
        messageScrollObject << json["embedded_messages"]
        messageGroupObject << json["embedded_message_groups"]
        
        if mapObject != nil {
            messageType = .map
        }
        else if photoObject != nil {
            messageType = .photo
        }
        else if weblinkObject != nil {
            messageType = .weblink
        }
        else if documentObject != nil {
            messageType = .document
        }
        else if messageGroupObject != nil {
            messageType = .messageGroup
        }
        else if messageScrollObject != nil {
            messageType = .messageScroll
        }
        
        type << messageType
    }
}

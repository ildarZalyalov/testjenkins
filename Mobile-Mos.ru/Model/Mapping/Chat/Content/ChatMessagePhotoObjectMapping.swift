//
//  ChatMessagePhotoObjectMapping.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 12.09.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

extension ChatMessagePhotoObject: MappableJsonResource {
    
    mutating func mapFromJson(_ json: NSDictionary) {
        
        photoUrl << (json["url"], MapToUrlFromStringTransform())
        
        var width: Double?
        var height: Double?
        
        width << json["width"]
        height << json["height"]
        
        if width != nil && height != nil {
            photoSize = CGSize(width: width!, height: height!)
        }
    }
}

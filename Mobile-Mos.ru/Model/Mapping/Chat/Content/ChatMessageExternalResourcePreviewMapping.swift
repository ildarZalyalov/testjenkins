//
//  ChatMessageExternalResourcePreviewMapping.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 05.02.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

extension ChatMessageExternalResourcePreview: MappableJsonResource {
    
    mutating func mapFromJson(_ json: NSDictionary) {
        photoUrls << (json["photos"], MapToUrlFromStringTransform())
        title << json["title"]
        description << json["description"]
        hideSource << json["hide_source"]
        publicationDate << (json["date"], MapToDateFromTimestampTransform())
    }
}

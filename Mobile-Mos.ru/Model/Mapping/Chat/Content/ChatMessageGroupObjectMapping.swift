//
//  ChatMessageGroupObjectMapping.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 15.09.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

extension ChatMessageGroupObject: MappableJsonResource {
    
    mutating func mapFromJson(_ json: NSDictionary) {
        messageGroups << json["messageGroups"]
    }
}

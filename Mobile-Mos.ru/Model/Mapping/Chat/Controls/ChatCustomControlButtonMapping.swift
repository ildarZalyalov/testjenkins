//
//  ChatCustomControlButtonMapping.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 10.07.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

extension ChatCustomControlButton: MappableJsonResource {
    
    mutating func mapFromJson(_ json: NSDictionary) {
        title << json["title"]
        action << json["action"]
    }
}

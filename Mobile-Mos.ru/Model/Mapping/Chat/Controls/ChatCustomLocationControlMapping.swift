//
//  ChatCustomLocationControlMapping.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 07.12.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

extension ChatCustomLocationControl: MappableJsonResource {
    
    mutating func mapFromJson(_ json: NSDictionary) {
        type << (json["type"], MapToEnumTransform<ChatCustomLocationControl.ControlType>())
    }
}

//
//  ChatMessageTimetableControlMapping.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 10.07.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

extension ChatMessageTimetableControl: MappableJsonResource {
    
    mutating func mapFromJson(_ json: NSDictionary) {
        
        title << json["title"]
        submitTitle << json["submit_title"]
        dateOutputFormat << json["date_format"]
        timeOutputFormat << json["time_format"]
        dateTimeSeparatorString << json["date_time_separator"]
        
        guard let datesJson = json["dates"] as? [NSDictionary] else { return }
        
        datesAndTimes.removeAll()
        
        for dateJson in datesJson {
            
            let dateParser = MapToDateWithCustomFormatTransform(formatString: "dd.MM.yyyy")
            
            guard let dateString = dateJson.allKeys.first as? String else { continue }
            guard let date = dateParser.transformToMappedType(dateString) else { continue }
            
            let timeParser = MapToDateWithCustomFormatTransform(formatString: "dd.MM.yyyy HH.mm")
            
            guard let timesValues = dateJson[dateString] as? [NSDictionary] else { continue }
            
            var times = [ChatMessageTimetableControl.TimeValue]()
            for timeValueJson in timesValues {
                
                guard var timeString = timeValueJson["time"] as? String else { continue }
                timeString = dateString + " " + timeString
                
                guard let time = timeParser.transformToMappedType(timeString) else { continue }
                let data = timeValueJson["data"] as? String
                
                let timeValue = ChatMessageTimetableControl.TimeValue(time: time, data: data)
                times.append(timeValue)
            }
            
            guard !times.isEmpty else { continue }
            
            datesAndTimes[date] = times
        }
    }
}

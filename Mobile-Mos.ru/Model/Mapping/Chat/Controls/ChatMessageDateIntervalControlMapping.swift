//
//  ChatMessageDateIntervalControlMapping.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 22.11.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

extension ChatMessageDateIntervalControl: MappableJsonResource {
    
    mutating func mapFromJson(_ json: NSDictionary) {
        
        let dateParser = MapToDateWithCustomFormatTransform(formatString: "dd.MM.yyyy")
        
        title << json["title"]
        submitTitle << json["submit_title"]
        initialDateFrom << (json["date_input_value_from"], dateParser)
        initialFloatingDateFrom << (json["date_input_value_from"], MapToEnumTransform<ChatMessageCalendarFloatingDate>())
        initialDateTo << (json["date_input_value_to"], dateParser)
        initialFloatingDateTo << (json["date_input_value_to"], MapToEnumTransform<ChatMessageCalendarFloatingDate>())
        minimumDate << (json["date_min_value"], dateParser)
        minimumFloatingDate << (json["date_min_value"], MapToEnumTransform<ChatMessageCalendarFloatingDate>())
        maximumDate << (json["date_max_value"], dateParser)
        maximumFloatingDate << (json["date_max_value"], MapToEnumTransform<ChatMessageCalendarFloatingDate>())
        dateOutputFormat << json["date_format"]
        dateSeparatorString << json["date_separator"]
        disableSelectDays << json["disable_select_days"]
        disableSelectYear << json["disable_select_year"]
    }
}

//
//  ChatMessageTextInputControlMapping.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 21.08.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

extension ChatMessageTextInputControl.TextInput: MappableJsonResource {
    
    mutating func mapFromJson(_ json: NSDictionary) {
        inputId << json["id"]
        title << json["title"]
        text << json["text"]
        placeholder << json["placeholder"]
        type << (json["keyboard"], MapToEnumTransform<ChatMessageTextInputControl.TextInput.InputType>())
        formatRegex << json["regex"]
        isSecure << json["isSecure"]
    }
}

extension ChatMessageTextInputControl: MappableJsonResource {
    
    mutating func mapFromJson(_ json: NSDictionary) {
        title << json["title"]
        inputs << json["inputs"]
    }
}

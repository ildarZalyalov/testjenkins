//
//  ChatMessageAlertControlMapping.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 10.07.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

extension ChatMessageAlertControl: MappableJsonResource {
    
    mutating func mapFromJson(_ json: NSDictionary) {
        title << json["title"]
        text << json["text"]
        buttons << json["buttons"]
    }
}

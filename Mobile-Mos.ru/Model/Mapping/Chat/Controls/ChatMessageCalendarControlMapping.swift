//
//  ChatMessageCalendarControlMapping.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 10.07.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

extension ChatMessageCalendarControl: MappableJsonResource {
    
    mutating func mapFromJson(_ json: NSDictionary) {
        
        let dateParser = MapToDateWithCustomFormatTransform(formatString: "dd.MM.yyyy")
        
        title << json["title"]
        submitTitle << json["submit_title"]
        initialDate << (json["date_input_value"], dateParser)
        initialFloatingDate << (json["date_input_value"], MapToEnumTransform<ChatMessageCalendarFloatingDate>())
        minimumDate << (json["date_min_value"], dateParser)
        minimumFloatingDate << (json["date_min_value"], MapToEnumTransform<ChatMessageCalendarFloatingDate>())
        maximumDate << (json["date_max_value"], dateParser)
        maximumFloatingDate << (json["date_max_value"], MapToEnumTransform<ChatMessageCalendarFloatingDate>())
        dateOutputFormat << json["date_format"]
        disableSelectDays << json["disable_select_days"]
        disableSelectYear << json["disable_select_year"]
    }
}

//
//  ChatMessageShareControlMapping.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 27.02.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

extension ChatMessageShareControl: MappableJsonResource {
    
    mutating func mapFromJson(_ json: NSDictionary) {
        text << json["text"]
    }
}

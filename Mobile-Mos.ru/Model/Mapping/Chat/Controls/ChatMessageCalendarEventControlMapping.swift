//
//  ChatMessageCalendarEventControlMapping.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 08.12.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

extension ChatMessageCalendarEventControl: MappableJsonResource {
    
    /// Специальный парсер даты в таймзоне Москвы
    /// Даты будут использованы для добавления в календарь, а он требует даты в GMT
    /// поэтому парсим, указывая, что время Москвоское
    fileprivate static let eventDateParser: DateFormatter = {
        
        let localeIdentifier = "en_US_POSIX"
        
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: localeIdentifier)
        formatter.timeZone = moscowTimezone
        formatter.dateFormat = "dd.MM.yyyy HH.mm"
        
        return formatter
    }()
        
    
    mutating func mapFromJson(_ json: NSDictionary) {
        
        title << json["title"]
        
        guard let dateString = json["date"] as? String else { return }
        guard let startTimeString = json["start_time"] as? String else { return }
        
        let dateParser = ChatMessageCalendarEventControl.eventDateParser
        let startDateString = dateString + " " + startTimeString
        
        guard let startDateValue = dateParser.date(from: startDateString) else { return }
        
        startDate = startDateValue
        
        if let endTimeString = json["end_time"] as? String {
            let endDateString = dateString + " " + endTimeString
            endDate = dateParser.date(from: endDateString)
        }
    }
}

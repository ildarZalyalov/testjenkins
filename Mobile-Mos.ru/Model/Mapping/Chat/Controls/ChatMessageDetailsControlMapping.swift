//
//  ChatMessageDetailsControlMapping.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 12.09.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

extension ChatMessageDetailsControl: MappableJsonResource {
    
    mutating func mapFromJson(_ json: NSDictionary) {
        
        title << json["title"]
        templateUrl << (json["template_url"], MapToUrlFromStringTransform())
        buttons << json["buttons"]
        
        guard let valuesDictionary = json["values"] as? NSDictionary else { return }
        guard let data = try? JSONSerialization.data(withJSONObject: valuesDictionary, options: []) else { return }
        
        valuesJsonString << String(data: data, encoding: .utf8)
    }
}

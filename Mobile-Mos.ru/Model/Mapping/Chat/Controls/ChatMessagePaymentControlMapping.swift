//
//  ChatMessagePaymentControlMapping.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 22.01.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

extension ChatMessagePaymentControl: MappableJsonResource {
    
    mutating func mapFromJson(_ json: NSDictionary) {
        subject << json["payment_subject_title"]
        sum << json["payment_sum"]
        parameters << json["payment_params"]
    }
}

//
//  ChatMessageMapping.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 12.05.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

extension ChatMessage: MappableJsonResource {
    
    mutating func mapFromJson(_ json: NSDictionary) {

        messageId << json["message_id"]
        chatId << json["chat_id"]
        
        isIncoming = json["from"] == nil
        date << (json["date"], MapToDateFromTimestampTransform())
        
        scenarioTitle << json["scenario_title"]
        
        content << json
        
        replyMarkup << json["reply_markup"]
        optionsMarkup << json["options_markup"]
        
        statsData << json["stats_data"]
    }
    
    static func chatIdFromJson(_ json: NSDictionary) -> String? {
        return json["chat_id"] as? String
    }
}

//
//  ChatNotificationMapping.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 14.12.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

extension ChatNotification: MappableJsonResource {
    
    mutating func mapFromJson(_ json: NSDictionary) {
        chatId << json["message", "chat_id"]
        type << (json["message", "chat_notification", "type"], MapToEnumTransform<ChatNotification.NotificationType>())
        text << json["message", "chat_notification", "text"]
    }
}

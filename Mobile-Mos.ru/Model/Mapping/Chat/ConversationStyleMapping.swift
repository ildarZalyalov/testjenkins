//
//  ConversationStyleMapping.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 11.08.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

extension ConversationStyle: MappableJsonResource {
    
    mutating func mapFromJson(_ json: NSDictionary) {
        
        isStatusBarDark << json["header", "status_bar"]
        backButtonColor << (json["header", "arrow"], MapToUIColorFromHexStringTransform())
        titleColor << (json["header", "title"], MapToUIColorFromHexStringTransform())
        gradientColor << (json["header", "gradient"], MapToUIColorFromHexStringTransform())
        headerColor << (json["header", "header"], MapToUIColorFromHexStringTransform())
        headerShadowColor << (json["header", "shadow"], MapToUIColorFromHexStringTransform())
        
        outgoingMessageTextColor << (json["output_msg", "text"], MapToUIColorFromHexStringTransform())
        outgoingMessageBackgroundColor << (json["output_msg", "background"], MapToUIColorFromHexStringTransform())
        
        controlsTextColor << (json["controls", "text"], MapToUIColorFromHexStringTransform())
        controlsShadowColor << (json["controls", "shadow"], MapToUIColorFromHexStringTransform())
    }
}

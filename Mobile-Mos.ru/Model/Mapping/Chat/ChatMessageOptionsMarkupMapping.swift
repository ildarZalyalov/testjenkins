//
//  ChatMessageOptionsMarkupMapping.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 16.11.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

extension ChatMessageOptionsMarkup: MappableJsonResource {
    
    mutating func mapFromJson(_ json: NSDictionary) {
        shareCallbackData << json["share_callback_data"]
    }
}

//
//  ChatReplyKeyboardMarkupMapping.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 22.05.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

extension ChatReplyKeyboardMarkup: MappableJsonResource {
    
    mutating func mapFromJson(_ json: NSDictionary) {
        
        //resizeKeyboard << json["resize_keyboard"]
        
        // пока всегда true по просьбе продуктов
        resizeKeyboard = true
    }
}

//
//  ChatMessageButtonActionMapping.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 10.07.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

extension ChatMessageButtonAction: MappableJsonResource {
    
    mutating func mapFromJson(_ json: NSDictionary) {
        type << (json["type"], MapToEnumTransform<ChatMessageButtonAction.ActionType>())
        phone << json["data", "phone"]
        url << (json["data", "url"], MapToUrlFromStringTransform())
        callbackData << json["data", "callback_data"]
        messageContext << json["data", "message_context"]
        shareData << json["data", "share_data"]
        alert << json["data", "alert"]
        textInput << json["data", "text_input"]
        calendar << json["data", "calendar"]
        dateIntervalCalendar << json["data", "date_interval"]
        timetable << json["data", "timetable"]
        mapObject << json["data", "map_object"]
        customLocationObject << json["data", "custom_location"]
        details << json["data", "details"]
        calendarEvent << json["data", "calendar_event"]
        payment << json["data", "payment_data"]
        statsData << json["stats_data"]
    }
}

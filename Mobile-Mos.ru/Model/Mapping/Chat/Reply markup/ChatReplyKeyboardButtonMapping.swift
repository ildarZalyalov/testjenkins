//
//  ChatReplyKeyboardButtonMapping.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 22.05.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

extension ChatReplyKeyboardButton: MappableJsonResource {
    
    mutating func mapFromJson(_ json: NSDictionary) {
        title << json["style", "title"]
        isAlwaysActive << json["style", "always_active"]
        action << json["action"]
    }
}

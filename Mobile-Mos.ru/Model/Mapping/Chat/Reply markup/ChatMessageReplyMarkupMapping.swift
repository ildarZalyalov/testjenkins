//
//  ChatMessageReplyMarkupMapping.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 11.07.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

extension ChatMessageReplyMarkup: MappableJsonResource {
    
    mutating func mapFromJson(_ json: NSDictionary) {
        
        showTextKeyboard << json["show_keyboard"]
        hideTypingIndicator << json["hide_typing_indicator"]
        
        guard let buttonsJson = json["buttons"] as? [NSDictionary] else { return }
        
        let buttonsByType = buttonsJson.group { $0["style", "type"] as? String }
        
        guard !buttonsByType.isEmpty else { return }
        
        let inlineButtonsStyleType = "inline"
        let inlineOutsideButtonsStyleType = "inline_outside"
        let commandButtonsStyleType = "command"
        let stylesByType = json["styles"] as? NSDictionary
        
        if let inlineButtonsJson = buttonsByType[inlineButtonsStyleType] {
            inlineMarkup = ChatInlineKeyboardMarkup(buttons: buttons(from: inlineButtonsJson))
        }
        
        if let outsideInlineButtonsJson = buttonsByType[inlineOutsideButtonsStyleType] {
            inlineOutsideMarkup = ChatInlineKeyboardMarkup(buttons: buttons(from: outsideInlineButtonsJson))
        }
        
        if let commandButtonsJson = buttonsByType[commandButtonsStyleType] {
            
            commandsMarkup = ChatReplyKeyboardMarkup(buttons: buttons(from: commandButtonsJson),
                                                     resizeKeyboard: true)
            
            if let styleJson = stylesByType?[commandButtonsStyleType] as? NSDictionary {
                commandsMarkup << styleJson
            }
        }
    }
    
    fileprivate func buttons(from buttonsJson: [NSDictionary]) -> [[ChatReplyKeyboardButton]] {
        
        var buttonsByRowAndColumn = [Int : [Int : ChatReplyKeyboardButton]]()
        
        for (index, buttonJson) in buttonsJson.enumerated() {
            
            let row = buttonJson["style", "row"] as? Int ?? index
            let column = buttonJson["style", "column"] as? Int ?? 0
            
            var button = ChatReplyKeyboardButton()
            button << buttonJson
            
            if buttonsByRowAndColumn[row] == nil {
                buttonsByRowAndColumn[row] = [Int : ChatReplyKeyboardButton]()
            }
            
            // использую ! т.к. выше добавляю словарь, если его не было
            buttonsByRowAndColumn[row]![column] = button
        }
        
        var buttons = [[ChatReplyKeyboardButton]]()
        
        // использую ! т.к. беру ключи из самих словарей
        
        for row in buttonsByRowAndColumn.keys.sorted(by: <) {
            
            var buttonsRow = [ChatReplyKeyboardButton]()
            
            for column in buttonsByRowAndColumn[row]!.keys.sorted(by: <) {
                buttonsRow.append(buttonsByRowAndColumn[row]![column]!)
            }
            
            buttons.append(buttonsRow)
        }
        
        return buttons
    }
}

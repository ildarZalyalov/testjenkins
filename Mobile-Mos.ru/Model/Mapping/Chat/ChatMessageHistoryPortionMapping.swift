//
//  ChatMessageHistoryPortionMapping.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 20.06.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

extension ChatMessageHistoryPortion: MappableJsonResource {
    
    mutating func mapFromJson(_ json: NSDictionary) {
        
        chatId << json["chat_id"]
        
        messages.removeAll()
        
        guard let messagesJson = json["message_list"] as? [NSDictionary] else { return }
        guard !messagesJson.isEmpty else { return }
        
        for messageJson in messagesJson {
            
            guard let messageId = messageJson["id"] as? Int64 else { continue }
            
            var message = ChatMessage()
            message << messageJson["message"]
            message.messageId = messageId
            
            messages.append(message)
        }
        
        messages.sort { $0.messageId < $1.messageId }
    }
}

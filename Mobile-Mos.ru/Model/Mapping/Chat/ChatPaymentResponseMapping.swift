//
//  ChatPaymentResponseMapping.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 23.05.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

extension ChatPaymentResponse: MappableJsonResource {
    
    mutating func mapFromJson(_ json: NSDictionary) {
        paymentId << json["payment_uuid"]
        errorCode << json["error_code"]
        errorMessage << json["error_msg"]
    }
}

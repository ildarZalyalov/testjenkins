//
//  ConversationMapping.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 13.07.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

extension Conversation: MappableJsonResource {
    
    mutating func mapFromJson(_ json: NSDictionary) {
        
        itemId << json["chat_id"]
        title << json["title"]
        descriptionText << json["description"]
        avatarIconUrl << (json["avatar_url"], MapToUrlFromStringTransform())
        avatarSmallIconUrl << (json["avatar_small_url"], MapToUrlFromStringTransform())
        avatarMiniatureUrl << (json["avatar_miniature_url"], MapToUrlFromStringTransform())
        type << (json["chat_type"], MapToEnumTransform<Conversation.ConversationType>())
        unseenMessagesCount << json["unread_msg"]
        style << json["chat_style"]
        
        var greetingMessageText: String?
        greetingMessageText << json["activation_msg"]
        
        if greetingMessageText != nil {
            greetingMessagesTexts.append(greetingMessageText!)
        }
        else if !descriptionText.isEmpty {
            greetingMessagesTexts.append(descriptionText)
        }
    }
}

//
//  ChatKeyWordsMapping.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 14.02.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

extension ChatKeywords: MappableJsonResource {
    
    mutating func mapFromJson(_ json: NSDictionary) {
        chatId << json["chat_id"]
        keywords << json["keywords"]
    }
}

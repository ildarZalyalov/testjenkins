//
//  ChatMessageCallbackResponseMapping.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 27.06.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

extension ChatMessageCallbackResponse: MappableJsonResource {
    
    mutating func mapFromJson(_ json: NSDictionary) {
        chatId << json["message", "chat_id"]
        queryId << json["message", "callback_query_response", "callback_query_id"]
        text << json["message", "callback_query_response", "text"]
    }
}

//
//  OverlapGeoMapping.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 28.02.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

extension OverlapGeo: MappableJsonResource {
    
    mutating func mapFromJson(_ json: NSDictionary) {
        
        id << json["id"]
        
        var geometries = [Location]()
        if let geometry = json["geometry"] as? NSDictionary, let coordinates = geometry["coordinates"] as? [[Double]] {
            
            if let typeString = geometry["type"] as? String, let lType = OverlapType(rawValue: typeString) {
                type = lType
            }
            
            for coordinate in coordinates {
                
                let location = Location(with: coordinate)
                geometries.append(location)
            }
        }
        
        geometry = geometries
    }
}

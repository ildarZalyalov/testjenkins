//
//  OverlapMapping.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 26.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

extension Overlap: MappableJsonResource {
    
    mutating func mapFromJson(_ json: NSDictionary) {
        
        id              << json["id"]
        name            << json["name"]
        address         << json["address"]
        phone           << json["telephone_number"]
        customer        << json["customer"]
        requester       << json["requester"]
        okrugName       << json["okrug"]
        eventType       << json["event_type"]
        description     << json["description"]
        lanesAvailable  << json["lanes_available"]
        lanesClosed     << json["lanes_closed"]
        toDate          << (json["ts_to"], MapToDateFromTimestampTransform())
        fromDate        << (json["ts_from"], MapToDateFromTimestampTransform())
        lastUpdateDate  << (json["last_update_date"], MapToDateFromTimestampTransform())
    }
}

//
//  UserLoginResponseMapping.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 10.11.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

extension UserLoginResponse: MappableJsonResource {
    
    mutating func mapFromJson(_ json: NSDictionary) {
        userId << json["user_id"]
        errorCode << json["error_code"]
        errorMessage << json["error_msg"]
    }
}

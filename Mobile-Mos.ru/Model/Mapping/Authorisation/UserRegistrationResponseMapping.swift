//
//  UserRegistrationResponseMapping.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 06.12.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

extension UserRegistrationResponse: MappableJsonResource {
    
    mutating func mapFromJson(_ json: NSDictionary) {
        userId << json["user_id"]
        errorCode << json["error_code"]
        errorMessage << json["error_msg"]
    }
}



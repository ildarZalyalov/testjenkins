//
//  UserSetNewPasswordResponseMapping.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 02.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

extension UserSetNewPasswordResponse: MappableJsonResource {
    
    mutating func mapFromJson(_ json: NSDictionary) {
        timeout      << json["timeout"]
        errorCode    << json["error_code"]
        errorMessage << json["error_msg"]
    }
}

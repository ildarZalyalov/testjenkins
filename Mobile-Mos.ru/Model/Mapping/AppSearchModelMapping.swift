//
//  AppSearchModelMapping.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 07.02.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

extension AppSearchModel: MappableJsonResource {
    
    mutating func mapFromJson(_ json: NSDictionary) {
        
        title               << json["trackName"]
        description         << json["description"]
        imageLinkString     << json["artworkUrl100"]
        appStoreLinkString  << json["trackViewUrl"]
        
        linkFromApp = appStoreLinkString
    }
}

//
//  MapItemCategoryMapping.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 27.04.17.
//  Copyright © 2017 mos.ru. All rights reserved.

import Foundation

extension MapItemCategory: MappableJsonResource {
    
    mutating func mapFromJson(_ json: NSDictionary) {
        
        id              << json["id"]
        name            << json["name"]
        datasetsCount   << json["datasets_app_count"]
        datasets        << json["datasets"]
    }
}

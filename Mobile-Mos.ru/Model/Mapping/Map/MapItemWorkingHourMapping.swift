//
//  MapItemWorkingHourMapping.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 01.09.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

extension MapItemWorkingHour: MappableJsonResource {
    
    mutating func mapFromJson(_ json: NSDictionary) {
       
        if let hourFrom = json["work_from"] as? String, let hourTo = json["work_to"] as? String {
           
            if hourTo == StringsHelper.closedPlaceName {
                hours = StringsHelper.closedPlaceName
            }
            else {
                hours = hourFrom + "-" + hourTo
            }
        }
        
        if let isDayOff = json["day_off"] as? Bool, isDayOff {
            hours = StringsHelper.weekendString
        }
    }
}


//
//  MapItemMapping.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 27.04.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

extension MapItem: MappableJsonResource {
    
    mutating func mapFromJson(_ json: NSDictionary) {
        
        globalId     << json["global_id"]
        name         << json["name"]
        website      << json["url"]
        workingHours << json["worktime", "schedule"]
        
        addDayOfWeekNames(to: &workingHours)
        
        if let addresses = json["address"] as? [String],
            let firstAddress = addresses.first {
            
            address = firstAddress
        }
        
        if let phones = json["phone"] as? [String],
            let firstPhone = phones.first {
            
            publicPhone = firstPhone
        }
        
        if let coodinates = (json["coordinates"] as? [NSDictionary])?.first,
            let latitute = coodinates["latitude"] as? String,
            let longitute = coodinates["longitude"] as? String  {
        
            location = Location(latitude: Double(latitute)!, longitude: Double(longitute)!)
        }
    }
    
    fileprivate func addDayOfWeekNames(to workingHours: inout [MapItemWorkingHour]) {
        
        typealias DaysEnum = StringsHelper.WeekDayNames
        
        workingHours.enumerated().forEach { (offset, _) in
            
            var dayName: String
            
            switch offset {
            case 0: dayName = DaysEnum.monday.rawValue
            case 1: dayName = DaysEnum.tuesday.rawValue
            case 2: dayName = DaysEnum.wednesday.rawValue
            case 3: dayName = DaysEnum.thursday.rawValue
            case 4: dayName = DaysEnum.friday.rawValue
            case 5: dayName = DaysEnum.saturday.rawValue
            case 6: dayName = DaysEnum.sunday.rawValue
            default:
                return
            }
            
            workingHours[offset].dayOfWeek = dayName
        }
    }
}

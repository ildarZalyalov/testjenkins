//
//  MapItemCategoryMapping.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 09.03.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

extension Dataset: MappableJsonResource{
    
    mutating func mapFromJson(_ json: NSDictionary) {
        
        itemId          << json["id"]
        name            << json["name"]
        itemsCount      << json["objects_count"]
        categoryId      << json["category_id"]
    }
}

//
//  BaseModel.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 07.06.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Базовый протокол для всех моделей
protocol BaseModel {}

/// Протокол модели, у которой есть значения по умолчанию
protocol BaseModelWithDefaults: BaseModel {
    
    /// Установить значения по умолчанию для полей, которые их имеют
    mutating func assignDefaultValues()
}

extension Array where Element: BaseModelWithDefaults {
    
    /// Установить значения по умолчанию для элементов в массиве
    mutating func assignDefaultValues() {
        self = self.map {
            var elementWithDefaults = $0
            elementWithDefaults.assignDefaultValues()
            return elementWithDefaults
        }
    }
}

//
//  iTunesVersionLookupState.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 08.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Информация о проверке версии приложения в App Store
struct iTunesVersionLookupState {
    
    /// Версия приложения на момент проверки
    var version: String?
    
    /// Описание версии приложения (Что нового?) на момент проверки
    var releaseNotes: String?
    
    /// Дата проверки
    var date: Date?
}

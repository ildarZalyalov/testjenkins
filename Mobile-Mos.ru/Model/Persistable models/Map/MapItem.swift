//
//  MapItem.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 21.02.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Объект на карте
struct MapItem: BaseModel {
    
    /// Глобальнай Id объекта
    var globalId: Int = 0
    
    /// Имя объекта карты
    var name: String = String()
    
    /// Адрес объекта карты
    var address: String?
    
    /// Описание объекта
    var itemDescription: String?
    
    /// Публичный телефон объекта карты
    var publicPhone: String?
    
    /// Вебсайт объекта карты
    var website: String?
    
    /// Время работы объекта карты
    var workingHours: [MapItemWorkingHour] = []
    
    /// Местоположение объекта
    var location: Location = moscowLocation
    
    /// Добавлен ли данный объект в избранное
    var isFavorited: Bool = false
    
    /// Строка для шаринга
    lazy var stringForSharing: String = {
        
        var shareString = self.name + "\n\n"
        
        let contactsString = self.publicPhone
        
        if var contactPhoneString = contactsString, !contactPhoneString.isEmpty {
            let phonePrefix = "+7 "
            let prefix = "8"
            
            contactPhoneString = contactPhoneString.hasPrefix(prefix) ? String(contactPhoneString.removeFirst()) : contactPhoneString
            contactPhoneString = contactPhoneString.hasPrefix(phonePrefix) ? contactPhoneString : phonePrefix+contactPhoneString
            
            shareString += contactPhoneString + "\n"
        }
        
        if let websiteString = self.website  {
            shareString += websiteString + "\n"
        }
        
        if let addressString = self.address, !addressString.isEmpty {
            shareString += addressString + "\n"
        }
        
        if !self.workingHours.isEmpty {
            
            self.workingHours.forEach{ shareString += $0.dayOfWeek + ":" + $0.hours + "\n" }
        }
        
        return shareString
    }()
}

extension MapItem:Equatable, Hashable {
    
    var hashValue: Int {
        return  self.name.hashValue &+
                self.location.hashValue &+
                (self.address?.hashValue ?? 0)
    }
    
    static func ==(lhs: MapItem, rhs: MapItem) -> Bool {
        return  lhs.name == rhs.name &&
            lhs.location == rhs.location &&
            lhs.address == rhs.address &&
            lhs.publicPhone == rhs.publicPhone &&
            lhs.website == rhs.website &&
            lhs.itemDescription == rhs.itemDescription
    }
}

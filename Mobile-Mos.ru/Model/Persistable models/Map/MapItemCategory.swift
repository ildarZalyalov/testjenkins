//
//  MapItemCategory.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 12.05.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Категория объектов на карте (тематический набор датасетов)
struct MapItemCategory: BaseModel {
    
    /// Идентификатор категории
    var id: Int = 0
    
    /// Наименование категории
    var name: String!
    
    /// Кол-во элементов в датасете (для мобилки) 
    var datasetsCount: Int = 0
    
    /// Объекты категории
    var datasets:[Dataset] = []
    
    /// Объекты избранной категории
    var favoritedDatasets:[FavoritedDataset] = []
    
    /// Сохранена ли категория в БД
    var isSaved: Bool = false
    
}

//
//  MapItemWorkingHour.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 23.05.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Описание рабочих часов для одного дня
struct MapItemWorkingHour: BaseModel {
    
    /// Название дня недели
    var dayOfWeek: String = String()
    
    /// Временной промежуток работы. Прим: "09:00-23:00"
    var hours: String = String()
}

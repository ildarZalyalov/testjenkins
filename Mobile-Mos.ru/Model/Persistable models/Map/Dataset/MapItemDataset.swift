//
//  MapItemDataset.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 25.01.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Общий протокол для избранных/не избранных датасетов
protocol MapItemDataset: BaseModel {
    
    /// Идентификатор набора данных
    var itemId: Int { get }
    
    /// Наименование набора данных
    var name: String { get }
}

//
//  Dataset.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 09.03.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Датасет (тематический набор объектов на карте)
struct Dataset: MapItemDataset {
    
    /// Идентификатор набора данных
    var itemId: Int = 0
    
    /// Наименование набора данных
    var name: String = " - "
    
    /// Кол-во объектов в датасете
    var itemsCount: Int = 0
    
    /// Идентификатор тематической категории, которой соответствует набор данных
    var categoryId: Int = 0
    
    /// Дата последнего сохранения объекта
    var saveDate: Date?
    
    /// Сохранен ли датасет в кэш
    var isSaved: Bool = false
    
    /// Находится ли объект в избранном
    var isInFavorite: Bool = false
    
}


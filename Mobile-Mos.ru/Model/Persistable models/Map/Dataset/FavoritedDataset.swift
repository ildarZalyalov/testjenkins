//
//  FavoritedDataset.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 25.01.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Датасет который в избранном с избранным (тематический набор объектов на карте)
struct FavoritedDataset: MapItemDataset {
    
    /// Идентификатор набора данных
    var itemId: Int = 0
    
    /// Наименование набора данных
    var name: String = " - "
    
    /// Избранные Объекты датасета
    var favoritedItems: [MapItem] = []
    
    /// Обновить избранный датасет из датасета обычного
    ///
    /// - Parameter dataset: объект категории не из избранного
    mutating func updateFrom(dataset: MapItemDataset) {
        itemId = dataset.itemId
        name = dataset.name
    }
}

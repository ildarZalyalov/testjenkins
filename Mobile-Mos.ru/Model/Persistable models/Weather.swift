//
//  Weather.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 10.04.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

struct Weather: BaseModel {
    
    var weatherIconId: Int = 0
    var weatherText: String = ""
    var temperatureInCelcius: Int = 0
}

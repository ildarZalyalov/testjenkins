//
//  ChatKeywords.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 14.02.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Модель, которая описывает ключевые слова для чата
struct ChatKeywords: BaseModel {
    
    /// id чата
    var chatId: String = ""
    
    /// ключевые поисковые слова для чатов разделенные через \n
    var keywords: String = ""
}

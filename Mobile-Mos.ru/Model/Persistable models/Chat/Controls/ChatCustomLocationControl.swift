//
//  ChatCustomLocationControl.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 07.12.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Информация о локации
struct ChatCustomLocationControl {
    
    /// Тип выбора кастомной локации
    ///
    /// - onMap: через карту
    /// - fromAdressSearching: через поиск адреса
    enum ControlType: String {
        case onMap = "map"
        case fromAdressSearching = "address_search"
    }
    
    /// Тип контрола
    var type: ControlType = .onMap
}

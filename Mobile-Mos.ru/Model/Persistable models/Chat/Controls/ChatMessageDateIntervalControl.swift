//
//  ChatMessageDateIntervalControl.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 22.11.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Информация о календаре для выбора периода времени, отображаемом в чате
struct ChatMessageDateIntervalControl {
    
    /// Заголовок в календаре
    var title: String = String()
    
    /// Заголовок кнопки завершения работы с календарем и выбора значения
    var submitTitle: String = String()
    
    /// Выбранная дата начала периода времени при показе календаря
    var initialDateFrom: Date?
    
    /// Плавающее ограничение выбранной даты начала периода времени
    var initialFloatingDateFrom: ChatMessageCalendarFloatingDate?
    
    /// Выбранная дата окончания периода времени при показе календаря
    var initialDateTo: Date?
    
    /// Плавающее ограничение выбранной даты окончания периода времени
    var initialFloatingDateTo: ChatMessageCalendarFloatingDate?
    
    /// Минимально возможная для выбора дата в календаре
    var minimumDate: Date?
    
    /// Плавающее ограничение минимально возможной для выбора даты
    var minimumFloatingDate: ChatMessageCalendarFloatingDate?
    
    /// Максимально возможная для выбора дата в календаре
    var maximumDate: Date?
    
    /// Плавающее ограничение максимально возможной для выбора даты
    var maximumFloatingDate: ChatMessageCalendarFloatingDate?
    
    /// Формат для дат в строке с результатом, которая будет отправлена на сервер
    var dateOutputFormat: String = "dd.MM.yyyy"
    
    /// Строка-разделитель между датами в строке с результатом, которая будет отправлена на сервер
    var dateSeparatorString: String = "-"
    
    /// Заблокировать ли выбор дня
    var disableSelectDays: Bool = false
    
    /// Заблокировать ли выбор года
    var disableSelectYear: Bool = false
}

//
//  ChatMessageCalendarControl.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 07.07.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Плавающее ограничение дат (учитывает текущую дату на момент показа контрола)
///
/// - today: сегодня
/// - previousMonth: предыдущий месяц
/// - nextMonth: следующий месяц
enum ChatMessageCalendarFloatingDate: String {
    case today
    case previousMonth
    case nextMonth
}

/// Информация о календаре, отображаемом в чате
struct ChatMessageCalendarControl {
    
    /// Заголовок в календаре
    var title: String = String()
    
    /// Заголовок кнопки завершения работы с календарем и выбора значения
    var submitTitle: String = String()
    
    /// Выбранная дата при показе календаря
    var initialDate: Date?
    
    /// Плавающее ограничение выбранной даты
    var initialFloatingDate: ChatMessageCalendarFloatingDate?
    
    /// Минимально возможная для выбора дата в календаре
    var minimumDate: Date?
    
    /// Плавающее ограничение минимально возможной для выбора даты
    var minimumFloatingDate: ChatMessageCalendarFloatingDate?
    
    /// Максимально возможная для выбора дата в календаре
    var maximumDate: Date?
    
    /// Плавающее ограничение максимально возможной для выбора даты
    var maximumFloatingDate: ChatMessageCalendarFloatingDate?
    
    /// Формат для строки с датой, которая будет отправлена на сервер
    var dateOutputFormat: String = "dd.MM.yyyy"
    
    /// Заблокировать ли выбор дня
    var disableSelectDays: Bool = false
    
    /// Заблокировать ли выбор года
    var disableSelectYear: Bool = false
}

//
//  ChatMessageShareControl.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 27.02.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Дополнительная информация для действия "поделиться сообщением"
struct ChatMessageShareControl {
    
    /// Текст, который добавляется к базовой информации, которой делятся
    var text: String = String()
}

//
//  ChatMessageAlertControl.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 06.07.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Информация об алерте, отображаемом в чате
struct ChatMessageAlertControl {
    
    /// Заголовок в алерте
    var title: String = String()
    
    /// Сообщение в алерте
    var text: String?
    
    /// Набор кнопок
    var buttons: [ChatCustomControlButton] = []
}
 

//
//  ChatMessageTextInputControl.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 21.08.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Информация о полях для ввода текста, отображаемых в чате
struct ChatMessageTextInputControl {
    
    /// Информация о текстовом поле
    struct TextInput {
        
        /// Тип клавиатуры для текстового поля
        ///
        /// - text: текстовая клавиатура
        /// - digits: цифровая клавиатура
        /// - date: барабан с выбором даты
        enum InputType: String {
            case text
            case digits
            case date
        }
        
        /// Идентификатор поля
        var inputId: String = String()
        
        /// Подсказка для конкретного поля
        var title: String?
        
        /// Текст в текстовом поле
        var text: String?
        
        /// Плейсхолдер в текстовом поле
        var placeholder: String?
        
        /// Тип клавиатуры для текстового поля
        var type: InputType = .text
        
        /// Регулярное выражение для ограничения вводимых символов
        var formatRegex: String?
        
        /// Использовать ли secure ввод для поля (скрывать ли вводимое значение)
        var isSecure: Bool = false
    }
    
    /// Общий заголовок для всей формы
    var title: String?
    
    /// Набор текстовых полей
    var inputs: [TextInput] = []
}

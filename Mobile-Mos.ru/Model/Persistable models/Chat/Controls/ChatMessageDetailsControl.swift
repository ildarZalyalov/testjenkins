//
//  ChatMessageDetailsControl.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 12.09.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Информация о подробностях, отображемых в чате на отдельном экране
struct ChatMessageDetailsControl {
    
    /// Заголовок экрана с подробностями
    var title: String = String()
    
    /// Ссылка на шаблон html
    var templateUrl: URL = URL(fileURLWithPath: String())
    
    /// Json со значениями для подстановки в шаблон
    var valuesJsonString: String = String()
    
    /// Кнопки на экране с подробностями
    var buttons: [ChatCustomControlButton] = []
}

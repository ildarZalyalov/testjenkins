//
//  ChatMessagePaymentControl.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 22.01.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Информация для оплаты в чате
struct ChatMessagePaymentControl {
    
    /// Название того, что оплачивается
    var subject: String = String()
    
    /// Сумма к оплате в рублях
    var sum: Float = 0
    
    /// Параметры оплаты с сервера
    var parameters: String = String()
}

extension ChatMessagePaymentControl {
    
    /// Сумма к оплате в минорных единицах (копейках)
    var sumInMinorUnits: Int64 {
        return Int64(sum * 100)
    }
}

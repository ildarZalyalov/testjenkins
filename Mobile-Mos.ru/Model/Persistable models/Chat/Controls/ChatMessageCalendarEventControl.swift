//
//  ChatMessageCalendarEventControl.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 08.12.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Информация для добавления события в календарь на устройстве
struct ChatMessageCalendarEventControl {
    
    /// Заголовок события
    var title: String = String()
    
    /// Дата начала события
    var startDate: Date = Date()
    
    /// Дата окончания события (если есть)
    var endDate: Date?
}

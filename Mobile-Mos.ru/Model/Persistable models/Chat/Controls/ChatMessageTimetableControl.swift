//
//  ChatMessageTimetableControl.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 07.07.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Информация о расписании, отображаемом в чате
struct ChatMessageTimetableControl {
    
    /// Значение для времени
    struct TimeValue {
        
        /// Время
        var time: Date = Date()
        
        /// Данные для значения (если есть)
        var data: String?
    }
    
    /// Заголовок в расписании
    var title: String = String()
    
    /// Заголовок кнопки завершения работы с расписанием и выбора значения
    var submitTitle: String = String()
    
    /// Набор доступных для выбора часов по датам
    var datesAndTimes: [Date : [TimeValue]] = [:]
    
    /// Формат для строки с датой, которая будет отправлена на сервер
    var dateOutputFormat: String = "dd.MM.yyyy"
    
    /// Формат для строки со временем, которая будет отправлена на сервер
    var timeOutputFormat: String = "hh:mm"
    
    /// Строка-разделитель между датой и временем в строке с результатом, которая будет отправлена на сервер
    var dateTimeSeparatorString: String = "-"
}

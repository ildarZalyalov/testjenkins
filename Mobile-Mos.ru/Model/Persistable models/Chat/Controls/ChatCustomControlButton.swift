//
//  ChatCustomControlButton.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 07.07.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Кнопка в custom control в чате
struct ChatCustomControlButton {
    
    /// Идентификатор кнопки
    var itemId: String = UUID().uuidString.lowercased()
    
    /// Заголовок кнопки
    var title: String = String()
    
    /// Информация о действии, которое надо выполнить при нажатии на кнопку
    var action: ChatMessageButtonAction?
}

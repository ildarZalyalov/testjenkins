//
//  ChatMessageReplyMarkup.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 06.07.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Разметка кнопок для ответа на сообщение (командные/инлайны и другие)
struct ChatMessageReplyMarkup {
    
    /// Разметка инлайн клавиатуры (если есть)
    var inlineMarkup: ChatInlineKeyboardMarkup?
    
    /// Разметка внешней инлайн клавиатуры (если есть)
    var inlineOutsideMarkup: ChatInlineKeyboardMarkup?
    
    /// Разметка командной клавиатуры для ответа на сообщение (если есть)
    var commandsMarkup: ChatReplyKeyboardMarkup?
    
    /// Показывать ли клавиатуру для ввода текста
    var showTextKeyboard: Bool = false
    
    /// Спрятать ли индикатор "вам отвечают"
    /// 1) при отправке ответа на сообщение, если сообщение входящее
    /// 2) при отправке самого сообщения, если сообщение исходящее
    var hideTypingIndicator: Bool = false
}

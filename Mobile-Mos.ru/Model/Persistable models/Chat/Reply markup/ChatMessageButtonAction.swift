//
//  ChatMessageButtonAction.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 06.07.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Информация о действии, которое надо выполнить при нажатии на кнопку
struct ChatMessageButtonAction {
    
    /// Тип действия при нажатии на кнопку
    ///
    /// - phoneCall: позвонить по номеру
    /// - url: перейти по ссылке
    /// - share: поделиться сообщением
    /// - calendarEvent: добавить событие в календарь на устройстве
    /// - userLocation: отправить местоположение пользователя
    /// - customLocation: отправить местоположение, которое выберет пользователь
    /// - addressAutocomplete: отправить адрес, который выберет пользователь из предложенных
    /// - callback: отправить callback запрос на сервер
    /// - sendMessage: отправить сообщения от пользователя
    /// - nextMessage: показать очередное вложенное сообщение
    /// - alert: показать алерт
    /// - calendar: показать календарь
    /// - dateInterval: показать календарь для выбора периода времени
    /// - timetable: показать расписание
    /// - map: показать карту с точкой
    /// - details: показать подробную информацию
    /// - payment: запуск сценария оплаты
    enum ActionType: String {
        case phoneCall           = "phone_call"
        case url                 = "url"
        case share               = "share"
        case calendarEvent       = "calendar_event"
        case userLocation        = "user_location"
        case customLocation      = "custom_location"
        case addressAutocomplete = "address_autocomplete"
        case callback            = "callback"
        case sendMessage         = "send_message"
        case nextMessage         = "next_message"
        case alert               = "alert"
        case textInput           = "text_input"
        case calendar            = "calendar"
        case dateInterval        = "date_interval"
        case timetable           = "timetable"
        case map                 = "map"
        case details             = "details"
        case payment             = "payment"
    }

    
    /// Тип действия при нажатии на кнопку
    var type: ActionType = .sendMessage
    
    /// Номер телефона
    var phone: String?
    
    /// Веб-ссылка
    var url: URL?
    
    /// Данные для callback запроса на сервер
    var callbackData: String?
    
    /// Контекст для исходящего сообщения (данные для переключения диалога из одного состояния в другое)
    var messageContext: String?
    
    /// Дополнительные данные для действия "поделиться сообщением"
    var shareData: ChatMessageShareControl?
    
    /// Данные для отображения алерта
    var alert: ChatMessageAlertControl?
    
    /// Данные для отображения полей для ввода текста
    var textInput: ChatMessageTextInputControl?
    
    /// Данные для отображения календаря для ввода даты
    var calendar: ChatMessageCalendarControl?
    
    /// Данные для отображения календаря для ввода периода времени
    var dateIntervalCalendar: ChatMessageDateIntervalControl?
    
    /// Данные для отображения расписания для ввода даты и времени
    var timetable: ChatMessageTimetableControl?
    
    /// Данные для отображения объекта на карте
    var mapObject: ChatMessageMapObject?
    
    /// Данные для отображения карты для выбора геопозиции
    var customLocationObject: ChatCustomLocationControl?
    
    /// Данные для отображения подробной информации
    var details: ChatMessageDetailsControl?
    
    /// Данные для добавления события в календарь на устройстве
    var calendarEvent: ChatMessageCalendarEventControl?
    
    /// Информация для оплаты в чате
    var payment: ChatMessagePaymentControl?
    
    /// Информация статистики
    var statsData: StatsData?
}

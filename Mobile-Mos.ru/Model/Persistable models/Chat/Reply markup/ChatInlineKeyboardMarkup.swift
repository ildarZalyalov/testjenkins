//
//  ChatInlineKeyboardMarkup.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 24.05.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Разметка инлайн клавиатуры
struct ChatInlineKeyboardMarkup {
    
    /// Набор рядов кнопок
    var buttons: [[ChatReplyKeyboardButton]] = []
}

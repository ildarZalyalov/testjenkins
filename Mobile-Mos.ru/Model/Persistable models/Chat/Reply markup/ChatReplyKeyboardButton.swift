//
//  ChatReplyKeyboardButton.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 19.05.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Кнопка на клавиатуре с командами/инлайнами
struct ChatReplyKeyboardButton {
    
    /// Идентификатор кнопки
    var itemId: String = UUID().uuidString.lowercased()
    
    /// Заголовок кнопки
    var title: String = String()
    
    /// Остается ли кнопка активной, ели сообщение уходит в историю (становится не последним входящим)
    var isAlwaysActive: Bool = false
    
    /// Информация о действии, которое надо выполнить при нажатии на кнопку
    var action: ChatMessageButtonAction = ChatMessageButtonAction()
}

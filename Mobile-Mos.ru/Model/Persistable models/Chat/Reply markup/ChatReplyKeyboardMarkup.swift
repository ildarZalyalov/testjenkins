//
//  ChatReplyKeyboardMarkup.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 19.05.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Разметка клавиатуры с командами
struct ChatReplyKeyboardMarkup {
    
    /// Набор рядов кнопок
    var buttons: [[ChatReplyKeyboardButton]] = []
    
    /// Подогнать ли размер клавиатуры под кол-во кнопок (если полученная высота меньше стандартной высоты клавиатуры)
    var resizeKeyboard: Bool = true
}

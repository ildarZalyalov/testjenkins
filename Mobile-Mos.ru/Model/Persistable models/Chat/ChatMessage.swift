//
//  ChatMessage.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 02.05.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Сообщение в чате
struct ChatMessage {
    
    /// Статус отправки сообщения
    ///
    /// - failed: отправка провалилась
    /// - sending: идет отправка
    /// - success: успешная отправка
    enum Status: Int {
        case failed
        case sending
        case success
    }
    
    /// Некоторые возможные значения поля messageContext
    ///
    /// - text: ввод текста с клавиатуры
    /// - start: начало диалога
    /// - inputNotPossible: отсутствует возможность для пользователя отправить ответ (нет ни одной клавиатуры)
    enum MessageContextValues: String {
        case text = "TEXT"
        case start = "START"
        case inputNotPossible = "USER_INPUT_NOT_POSSIBLE_ERROR"
    }
    
    /// Уникальный идентификатор сообщения (не меняется, служит первичным ключом)
    var uid: String = UUID().uuidString.lowercased()
    
    /// Идентификатор родительского сообщения (заполнен для вложенных сообщений)
    var parentMessageUid: String?
    
    /// Идентификатор сообщения на сервере (для исходящих сообщений, которые не были получены сервером равен Int64.max)
    /// Уникален только в рамках пары пользователь + диалог
    var messageId: Int64 = Int64.max
    
    /// Контекст сообщения (данные для переключения диалога из одного состояния в другое)
    var messageContext: String = ChatMessage.MessageContextValues.text.rawValue
    
    /// Идентификатор пользователя (отсутствует, если пользователь не залогинен)
    var userId: String?
    
    /// Идентификатор сессии (понимается как идентификатор устройства, используется, когда пользователь не залогинен)
    var sessionId: String = String()
    
    /// Идентификатор чата
    var chatId: String = String()
    
    /// Является ли сообщение входящим
    var isIncoming: Bool = false
    
    /// Дата отправки/получения сообщения
    var date: Date = Date()
    
    /// Статус отправки сообщения
    var status: Status = .sending
    
    /// Название текущего сценария в диалоге
    var scenarioTitle: String?
    
    /// Содержимое сообщения
    var content: ChatMessageContent = ChatMessageContent()
    
    /// Разметка кнопок для ответа на сообщение (командные/инлайны и другие)
    var replyMarkup: ChatMessageReplyMarkup = ChatMessageReplyMarkup()
    
    /// Разметка опций взаимодействия с сообщением (например, шаринг)
    var optionsMarkup: ChatMessageOptionsMarkup = ChatMessageOptionsMarkup()
    
    /// Данные для статистики
    var statsData: StatsData?
}

extension ChatMessage {
    
    /// Идентификатор отправителя
    var senderId: String {
        return isIncoming ? chatId : (userId ?? sessionId)
    }
    
    /// Может ли сообщение быть обновлено при хранении в кэше истории
    var canBeUpdatedInHistory: Bool {
        return content.type != .messageGroup
    }
    
    /// Дата отправки/получения сообщения с учетом истории показанных вложенных сообщений
    var dateBasedOnMessageGroupHistory: Date {
        return content.messageGroupObject?.messageGroupHistory.last?.date ?? date
    }
}

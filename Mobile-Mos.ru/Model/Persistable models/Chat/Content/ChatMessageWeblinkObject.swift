//
//  ChatMessageWeblinkObject.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 12.09.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Информация для отображения ссылки на внешний ресурс в чате
struct ChatMessageWeblinkObject {
    
    /// Тип ссылки
    ///
    /// - regular: ссылка на внешний ресурс
    /// - social: ссылка на пост в социальной сети
    enum LinkType: String {
        case regular
        case social
    }
    
    /// Ссылка на внешний ресурс
    var linkUrl: URL = URL(fileURLWithPath: String())
    
    /// Тип ссылки
    var type: LinkType = .regular
    
    /// Отключить ли превью ссылки
    var disablePreview: Bool = false
    
    /// Данные для превью
    var preview: ChatMessageExternalResourcePreview?
    
    /// Информация статистики
    var statsData: StatsData = StatsData()
}

extension ChatMessageWeblinkObject {
    
    /// Текст для превью диалога (например, отображается в списке диалогов, также по нему идет поиск сообщений)
    var previewString: String {
        
        var previewString = String()
        
        if let title = preview?.title {
            previewString += title + " "
        }
        
        if let description = preview?.description {
            previewString += description
        }
        
        previewString = previewString.trimmingCharacters(in: CharacterSet.whitespaces)
        
        return previewString.isEmpty ? linkUrl.absoluteString : previewString
    }
    
    /// Данные для шаринга сообщения
    var sharingItems: [Any] {
        
        var items = [Any]()
        
        if let title = preview?.title {
            items.append(title)
        }
        
        if let description = preview?.description {
            items.append(description)
        }
        
        items.append(linkUrl)
        
        return items
    }
    
    /// Ссылки на картинки, которые надо предзагрузить и закешировать
    var imageUrlsToPrefetch: [URL] {
        return preview?.photoUrls ?? []
    }
}

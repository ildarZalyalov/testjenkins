//
//  ChatMessageContent.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 13.06.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Содержимое сообщения в чате
struct ChatMessageContent {
    
    /// Тип содержимого сообщения
    ///
    /// - text: текстовое (текст может быть в любом типе контента, данный тип контента указывает, что в сообщении ТОЛЬКО текст)
    /// - map: карта
    /// - photo: фото
    /// - weblink: ссылка на внешний ресурс
    /// - document: файл
    /// - messageGroup: сценарий из нескольких вложенных сообщений
    /// - messageScroll: несколько сообщений, показываемых одновременно с горизонтальным скроллом
    enum ContentType: String {
        case text
        case map
        case photo
        case weblink
        case document
        case messageGroup
        case messageScroll
    }
    
    /// Тип содержимого сообщения (значение этого поля указывает как интерпретировать значения других полей)
    var type: ContentType = .text
    
    /// Текст сообщения
    var text: String = String()
    
    /// Данные о форматировании текста сообщения (если есть форматирование)
    var textFormatting: ChatMessageTextFormatting?
    
    /// Информация для отображения на карте (если есть)
    var mapObject: ChatMessageMapObject?
    
    /// Информация для отображения фото
    var photoObject: ChatMessagePhotoObject?
    
    /// Информация для отображения ссылки на внешний ресурс
    var weblinkObject: ChatMessageWeblinkObject?
    
    /// Информация для отображения файла
    var documentObject: ChatMessageDocumentObject?
    
    /// Информация о вложенных сообщениях в виде "группы" (сценарий показа одно за другим)
    var messageGroupObject: ChatMessageGroupObject?
    
    /// Информация о вложенных сообщениях в виде "карусели" (сценарий горизонтального скролла)
    var messageScrollObject: ChatMessageScrollObject?
}

extension ChatMessageContent {
    
    /// Текст сообщения без тегов форматирования
    var textWithoutFormatting: String {
        return textFormatting?.trimmedText ?? text
    }
    
    /// Текст для превью диалога (например, отображается в списке диалогов, также по нему идет поиск сообщений)
    var previewString: String {
        
        if type != .messageGroup && !text.isEmpty {
            return textWithoutFormatting
        }
        
        switch type {
        case .text:
            return textWithoutFormatting
        case .map:
            return mapObject?.previewString ?? StringsHelper.chatMessagePreviewMapString
        case .photo:
            return StringsHelper.chatMessagePreviewPhotoString
        case .weblink:
            return weblinkObject?.previewString ?? StringsHelper.chatMessagePreviewExternalLinkString
        case .document:
            return documentObject?.fileName ?? StringsHelper.chatMessagePreviewDocumentString
        case .messageGroup:
            let lastInHistory = messageGroupObject?.messageGroupHistory.last
            return lastInHistory?.content.previewString ?? textWithoutFormatting
        case .messageScroll:
            let firstInScroll = messageScrollObject?.messages.first
            return firstInScroll?.content.previewString ?? textWithoutFormatting
        }
    }
    
    /// Данные для шаринга сообщения
    var sharingItems: [Any] {
        
        var items = [Any]()
        
        if !text.isEmpty {
            items.append(textWithoutFormatting)
        }
        
        switch type {
        case .map:
            items += mapObject?.sharingItems ?? []
        case .photo:
            items += photoObject?.sharingItems ?? []
        case .weblink:
            items += weblinkObject?.sharingItems ?? []
        case .document:
            items += documentObject?.sharingItems ?? []
        case .text, .messageGroup, .messageScroll:
            break
        }
        
        return items
    }
    
    /// Ссылки на картинки, которые надо предзагрузить и закешировать
    var imageUrlsToPrefetch: [URL] {
        
        var urls = [URL]()
        
        urls += photoObject?.imageUrlsToPrefetch ?? []
        urls += weblinkObject?.imageUrlsToPrefetch ?? []
        
        urls += messageGroupObject?.imageUrlsToPrefetch ?? []
        urls += messageScrollObject?.imageUrlsToPrefetch ?? []
        
        return urls
    }
}

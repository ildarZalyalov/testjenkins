//
//  ChatMessageGroupObject.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 15.09.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Информация о вложенных группах сообщений (сценарий показа одно за другим)
struct ChatMessageGroupObject {
    
    /// Группы сообщений (тут лежат еще не показанные группы сообщений)
    var messageGroups: [[ChatMessage]] = []
    
    /// История вложенных сообщений (тут лежат уже показанные сообщения из групп и другие сообщения, которые появились в чате в результате выполнения сценария, например сообщения пользователя)
    var messageGroupHistory: [ChatMessage] = []
}

extension ChatMessageGroupObject {
    
    /// Ссылки на картинки, которые надо предзагрузить и закешировать
    var imageUrlsToPrefetch: [URL] {
        
        var urls = [URL]()
        
        for messageGroup in messageGroups {
            urls += messageGroup.reduce(into: [], { $0 += $1.content.imageUrlsToPrefetch })
        }
        
        return urls
    }
}

//
//  ChatMessageMapObject.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 07.07.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Информация об объекте на карте, отображаемом в чате
struct ChatMessageMapObject {
    
    /// Информация о координатах объекта
    var location: Location = Location()
    
    /// Название объекта
    var title: String?
    
    /// Описание объекта
    var description: String?
}

extension ChatMessageMapObject {
    
    /// Текст для превью диалога (например, отображается в списке диалогов, также по нему идет поиск сообщений)
    var previewString: String {
        
        var previewString = String()
        
        if title != nil {
            previewString += title! + " "
        }
        
        if description != nil {
            previewString += description!
        }
        
        previewString = previewString.trimmingCharacters(in: CharacterSet.whitespaces)
        
        return previewString.isEmpty ? StringsHelper.chatMessagePreviewMapString : previewString
    }
    
    /// Данные для шаринга сообщения
    var sharingItems: [Any] {
        
        var items = [Any]()
        
        if title != nil {
            items.append(title!)
        }
        if description != nil {
            items.append(description!)
        }
        
        let coordinateString = "\(location.latitude), \(location.longitude)"
        items.append(coordinateString)
        
        return items
    }
}

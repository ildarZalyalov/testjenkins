//
//  ChatMessagePhotoObject.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 12.09.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Информация для отображения фото в чате
struct ChatMessagePhotoObject {
    
    /// Ссылка на фото
    var photoUrl: URL = URL(fileURLWithPath: String())
    
    /// Размер фото
    var photoSize: CGSize?
}

extension ChatMessagePhotoObject {
    
    /// Данные для шаринга сообщения
    var sharingItems: [Any] {
        return [photoUrl]
    }
    
    /// Ссылки на картинки, которые надо предзагрузить и закешировать
    var imageUrlsToPrefetch: [URL] {
        return [photoUrl]
    }
}

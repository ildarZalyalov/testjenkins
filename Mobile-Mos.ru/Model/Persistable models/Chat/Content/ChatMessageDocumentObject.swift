//
//  ChatMessageDocumentObject.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 12.09.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Состояние загрузки документа
class ChatMessageDocumentDownloadState {
    
    /// Статус загрузки
    ///
    /// - notDownloaded: не загружен
    /// - downloading: загружается
    /// - downloadFinished: загрузка окончена
    enum Status {
        case notDownloaded
        case downloading
        case downloadFinished
    }
    
    /// Статус загрузки
    var status: ThreadSafeObservable<Status> = ThreadSafeObservable(.notDownloaded)
    
    /// Обновляемый прогресс загрузки документа
    var progress: ThreadSafeObservable<Progress?> = ThreadSafeObservable(nil)
    
    /// Ссылка на файл в кэше (если файл загружен)
    var fileCacheUrl: URL?
}

/// Информация для отображения файла в чате
struct ChatMessageDocumentObject {
    
    /// Ссылка для скачивания файла
    var fileUrl: URL = URL(fileURLWithPath: String())
    
    /// Имя файла вместе с расширением
    var fileName: String = String()
    
    /// Размер файла в килобайтах
    var fileSize: Float?
    
    /// Состояние загрузки документа
    let downloadState = ChatMessageDocumentDownloadState()
    
    /// Информация статистики
    var statsData: StatsData = StatsData()
}

extension ChatMessageDocumentObject {
    
    /// Данные для шаринга сообщения
    var sharingItems: [Any] {
        return [fileName, fileUrl]
    }
}

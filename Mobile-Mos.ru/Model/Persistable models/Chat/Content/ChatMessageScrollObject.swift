//
//  ChatMessageScrollObject.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 21.09.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Информация о вложенных сообщениях
struct ChatMessageScrollObject {
    
    /// Тип показа вложенных сообщений
    ///
    /// - horizontalScroll: в виде "карусели" (сценарий горизонтального скролла)
    enum EmbeddingType: String {
        case horizontalScroll = "horizontal_scroll"
    }
    
    /// Тип показа вложенных сообщений
    var type: EmbeddingType = .horizontalScroll
    
    /// Вложенные сообщения
    var messages: [ChatMessage] = []
}

extension ChatMessageScrollObject {
    
    /// Ссылки на картинки, которые надо предзагрузить и закешировать
    var imageUrlsToPrefetch: [URL] {
        return messages.reduce(into: [], { $0 += $1.content.imageUrlsToPrefetch })
    }
}

//
//  ChatMessageExternalResourcePreview.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 13.09.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Данные для превью ссылки на внешний ресурс
struct ChatMessageExternalResourcePreview {
    
    /// Ссылки на фото
    var photoUrls: [URL] = []
    
    /// Заголовок
    var title: String?
    
    /// Описание
    var description: String?
    
    /// Прятать ли источник ссылки
    var hideSource: Bool = false
    
    /// Дата публикации ссылки
    var publicationDate: Date?
}

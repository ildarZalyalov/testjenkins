//
//  ChatMessageTextFormatting.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 05.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Данные о форматировании текста сообщения
struct ChatMessageTextFormatting {
    
    /// Текст без тегов форматирования
    var trimmedText: String = String()
    
    /// Распарсенные данные о форматировании участков текста сообщения
    var formattingRanges: [ChatTextRangeFormatting] = []
    
    /// Цвет фона всего сообщения (если был задан)
    var messageBackgroundColor: UIColor?
}

extension ChatMessageTextFormatting {
    
    init(formatting: ChatTextFormatting) {
        self.init()
        self.trimmedText = formatting.trimmedText
        self.formattingRanges = formatting.formattingRanges
        self.messageBackgroundColor = formatting.messageBackgroundColor
    }
    
    func convertToChatTextFormatting() -> ChatTextFormatting {
        return ChatTextFormatting(trimmedText: trimmedText,
                                  formattingRanges: formattingRanges,
                                  messageBackgroundColor: messageBackgroundColor)
    }
}

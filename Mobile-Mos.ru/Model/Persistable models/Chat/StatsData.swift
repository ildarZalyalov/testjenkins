//
//  StatsData.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 02.07.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Модель данной статистики
struct StatsData {
    
    /// JSON тело события
    var eventJson: String = ""
    
    /// Название события
    var eventName: String = ""
    
    /// Название функции (есть либо eventJson, либо event Function)
    var eventFunction: String = ""
}

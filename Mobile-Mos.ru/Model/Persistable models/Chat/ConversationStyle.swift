//
//  ConversationStyle.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 08.08.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Информация о брендировании чата
struct ConversationStyle {
    
    /// Нужно ли делать статус-бар темным
    var isStatusBarDark: Bool = false
    
    /// Цвет кнопки Назад
    var backButtonColor: UIColor = UIColor.white
    
    /// Цвет текста в заголовке
    var titleColor: UIColor = UIColor.white
    
    /// Цвет градиента в заголовке
    var gradientColor: UIColor = UIColor.fromHex(hex: 0x2194F3)
    
    /// Цвет заголовка
    var headerColor: UIColor = UIColor.fromHex(hex: 0x22C3F1)
    
    /// Цвет тени под заголовком
    var headerShadowColor: UIColor = UIColor.fromHex(hex: 0x2194F3)

    /// Цвет текста исходящего сообщения
    var outgoingMessageTextColor: UIColor = UIColor.white
    
    /// Цвет фона исходящего сообщения
    var outgoingMessageBackgroundColor: UIColor = UIColor.fromHex(hex: 0x22AFF2)
    
    /// Цвет текста в контролах (инлайнах и командной клавиатуре)
    var controlsTextColor: UIColor = UIColor.fromHex(hex: 0x22AFF2)
    
    /// Цвет тени в контролах (инлайнах и командной клавиатуре)
    var controlsShadowColor: UIColor = UIColor.fromHex(hex: 0x5B82A2)
}

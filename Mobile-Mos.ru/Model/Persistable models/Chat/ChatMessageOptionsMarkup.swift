//
//  ChatMessageOptionsMarkup.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 16.11.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Разметка опций для взаимодействия с сообщением
struct ChatMessageOptionsMarkup {
    
    /// Данные для коллбэка опции шаринга сообщения (если он есть, то доступна опция шаринга)
    var shareCallbackData: String?
}

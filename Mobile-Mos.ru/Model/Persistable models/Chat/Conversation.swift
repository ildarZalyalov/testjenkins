//
//  Conversation.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 10.02.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Типы чатов с id
enum ConversationIdentificator: String {
    case cityNews   = "a5cb2213-8e5c-4f8b-82bf-af2c7242398f"
    case children   = "f30ce822-a21e-44e7-bf25-5af70eedc0de"
    case transport  = "acc160e7-79b1-4eb2-9b09-7c5f58bdfc87"
    case myHome     = "ed7230ca-93c6-416f-a39f-1667632e937d"
    case health     = "8d04e7f3-d15d-4f45-b449-5a986f4883d4"
    case qa         = "3a2ebae9-bb08-4ad5-80d9-e3135f5550d2"
    case testChat   = "3a2ebae9-bb08-4ad5-80d9-e3135f553456"
    case ourTown    = "83b720bb-487f-49c1-866d-f6a45b1d95ee"
    case activeCitizen = "6c0352c9-7353-4a2e-afce-d16ac60676b6"
    
    static var allCasesRawValues: [String] {
       
       return [self.cityNews.rawValue, self.children.rawValue,
               self.transport.rawValue, self.myHome.rawValue,
               self.myHome.rawValue, self.health.rawValue,
               self.qa.rawValue, self.testChat.rawValue]
    }
}

/// Информация о чате
struct Conversation {
    
    /// Тип чата
    ///
    /// - service: сервисный (предоставляет услуги)
    /// - media: медиа (предоставляет новости)
    enum ConversationType: String {
        case service = "service"
        case media = "media"
    }

    /// Идентификатор чата
    var itemId: String = UUID().uuidString.lowercased()
    
    /// Название чата
    var title: String = String()
    
    /// Описание чата
    var descriptionText: String = String()
    
    /// Тексты приветственных сообщения чата
    var greetingMessagesTexts: [String] = []
    
    /// Веб-ссылка для загрузки аватарки
    var avatarIconUrl: URL?
    
    /// Веб-ссылка для загрузки аватарки малого размера
    var avatarSmallIconUrl: URL?
    
    /// Веб-ссылка для загрузки миниатюрной аватарки
    var avatarMiniatureUrl: URL?
    
    /// Имя файла с плейсхолдером аватарки
    var avatarPlaceholderIconName: String? {
        didSet {
            guard avatarPlaceholderIconName != nil else { return }
            avatarPlaceholderSmallIconName = avatarPlaceholderIconName! + "Regular"
        }
    }
    
    /// Имя файла с плейсхолдером аватарки малого размера
    var avatarPlaceholderSmallIconName: String?
    
    /// Имя файла с плейсхолдером миниатюрной аватарки
    var avatarPlaceholderMiniatureName: String?
    
    /// Тип чата
    var type: ConversationType = .service {
        didSet {
            // медиа-каналы по умолчанию обновляют историю при заходе в чат
            if type == .media {
                shouldUpdateHistory = true
            }
        }
    }
    
    /// Количество непрочитанных сообщений
    var unreadMessagesCount: Int = 0
    
    /// Количество сообщений, которые были отправлены пока пользователь был оффлайн
    var unseenMessagesCount: Int = 0
    
    /// Флаг указывает нужно ли при заходе в чат обновлять его историю т.к. могут быть новые сообщения
    /// По умолчанию false для новых моделей т.к. это указывает, что кэш диалогов был обновлен, значит вся история и так будет загружена с сервера
    var shouldUpdateHistory = false
    
    /// Брендирование чата
    var style: ConversationStyle = ConversationStyle()
    
    init() {}
    
    init(title: String, avatarPlaceholderIconName: String?, descriptionText: String) {
        self.title = title
        self.avatarPlaceholderIconName = avatarPlaceholderIconName
        self.descriptionText = descriptionText
    }
}

extension Conversation: BaseModelWithDefaults {
    
    /// Обновить хранимые локально поля из закешированной модели
    ///
    /// - Parameter conversation: закешированная модель диалога
    mutating func updatePersistentFields(from conversation: Conversation) {
        unreadMessagesCount = conversation.unreadMessagesCount
        shouldUpdateHistory = conversation.shouldUpdateHistory
    }
    
    mutating func assignDefaultValues() {
        
        let avatarPlaceholdersById: [String : (String, String)] = [
            ConversationIdentificator.cityNews.rawValue : ("chatAvatarNews", "userpic24News"),
            ConversationIdentificator.qa.rawValue : ("chatAvatarQA", "userpic24QA"),
            ConversationIdentificator.health.rawValue : ("chatAvatarHealth", "userpic24Health"),
            ConversationIdentificator.transport.rawValue : ("chatAvatarTransport", "userpic24Transport"),
            ConversationIdentificator.ourTown.rawValue : ("chatAvatarOurTown", "userpic24NashGorod"),
            ConversationIdentificator.children.rawValue : ("chatAvatarChildren", "userpic24School"),
            ConversationIdentificator.activeCitizen.rawValue : ("chatAvatarAG", "userpic24Ag"),
            ConversationIdentificator.myHome.rawValue : ("chatAvatarMyHome", "userpic24Home"),
            ConversationIdentificator.testChat.rawValue : ("chatAvatarTesting", "userpic24QA")
        ]
        
        avatarPlaceholderIconName = avatarPlaceholdersById[itemId]?.0
        avatarPlaceholderMiniatureName = avatarPlaceholdersById[itemId]?.1
    }
}

//
//  AddressAutocompleteResponse.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 02.04.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Модель, содержащая данные ответа автокомплита адресов с мобильного бэкенда
struct AddressAutocompleteResponse {
    
    /// Адрес
    var address: String = String()
    
    /// Дополнительные данные об адресе
    var addressData: String = String()
}

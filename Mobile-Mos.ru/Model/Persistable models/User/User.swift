//
//  User.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 21.02.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Пользователь
struct User {
    
    /// Идентификатор сессии
    var sessionId: String = UUID().uuidString.lowercased()
    
    /// Идентификатор пользователя (если пользователь авторизовался)
    var userId: String?
    
    /// Логин (если пользователь авторизовался)
    var login: String?
    
    /// Пароль (если пользователь авторизовался)
    var password: String?
    
    /// Личные данные (если пользователь авторизовался и удалось их получить)
    var userInfo: UserInfo?
    
    /// Настройки уведомлений для чатов
    var notificationsSettings: [UserNotificationsSetting] = []
}

extension User {
    
    /// Авторизован ли пользователь
    var isLoggedIn: Bool {
        return userId != nil
    }
}

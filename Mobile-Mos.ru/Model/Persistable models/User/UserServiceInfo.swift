//
//  UserServiceInfo.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 21.02.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Личные данные пользователя, используемые в сервисах
struct UserServiceInfo {
    
    /// Название группы данных
    var title: String = String()
    
    /// Slug группы данных
    var slug: String = String()
    
    /// Идентификатор чата, скоторым связана группа данных
    var chatId: String?
}

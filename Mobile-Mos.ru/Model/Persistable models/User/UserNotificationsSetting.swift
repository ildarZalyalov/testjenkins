//
//  UserNotificationsSetting.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 13.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Данные о настройке уведомлений для чата
struct UserNotificationsSetting {
    
    /// Идентификатор чата
    var chatId: String = String()
    
    /// Включены ли уведомления
    var areNotificationsEnabled: Bool = true
}

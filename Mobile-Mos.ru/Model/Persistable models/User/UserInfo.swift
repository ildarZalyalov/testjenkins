//
//  UserInfo.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 21.02.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Личные данные пользователя
struct UserInfo {
    
    /// Электронная почта (если есть)
    var email: String?
    
    /// Имя (если есть)
    var firstName: String?
    
    /// Фамилия (если есть)
    var lastName: String?
    
    /// Отчество (если есть)
    var patronymicName: String?
    
    /// Дата рождения (если есть)
    var birthDate: Date?
    
    /// Телефон (если есть)
    var phone: String?
    
    /// СНИЛС (если есть)
    var snils: String?
    
    /// Данные для сервисов
    var serviceInfo: [UserServiceInfo] = []
}

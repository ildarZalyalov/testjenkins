//
//  SearchTopWords.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 01.02.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Структура описывающая выдачу топовых слов поиска
struct SearchTopWords: BaseModel, SearchSuggest {
    
    var title: String = ""
}

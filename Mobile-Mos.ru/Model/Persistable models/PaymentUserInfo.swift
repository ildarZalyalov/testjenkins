//
//  PaymentUserInfo.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 01.06.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Данные пользователя для платежной квитанции
struct PaymentUserInfo {
    
    /// Электронная почта
    var email: String = String()
    
    /// Имя
    var firstName: String = String()
    
    /// Фамилия
    var lastName: String = String()
    
    /// Отчество
    var middleName: String = String()
}

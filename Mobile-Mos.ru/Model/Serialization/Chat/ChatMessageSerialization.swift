//
//  ChatMessageSerialization.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 12.05.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

extension ChatMessage: SerializableJsonResource {
    
    func serializeToJson() -> NSDictionary {
        
        let userIdValue: Any = userId ?? NSNull()
        
        return [
            "user_id": userIdValue,
            "session_id": sessionId,
            "chat_id": chatId,
            "message": serializeMessageContentToJson()
        ]
    }
    
    func serializeMessageContentToJson() -> NSDictionary {
        
        var messageContentToJson: [String : Any] = [
            "date": Int(date.timeIntervalSince1970),
            "text": content.text,
            "message_context": messageContext,
            "message_id": messageId,
            "from": [
                "id": senderId,
            ],
            "chat_id" : chatId
        ]
        
        if let mapObject = content.mapObject {
            
            var locationJson: [String : Any] = [
                    "latitude": mapObject.location.latitude,
                    "longitude": mapObject.location.longitude
            ]
            
            if let title = mapObject.title {
                locationJson += ["title" : title]
            }
            
            if let description = mapObject.description {
                locationJson += ["description" : description]
            }
            
            messageContentToJson["location"] = locationJson
        }
        
        return messageContentToJson as NSDictionary
    }
}

//
//  ChatMessageHistoryPortionRequestSerialization.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 22.06.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

extension ChatMessageHistoryPortionRequest: SerializableJsonResource {
    
    func serializeToJson() -> NSDictionary {
        
        let userIdValue: Any = userId ?? NSNull()
        
        return [
            "user_id": userIdValue,
            "session_id": sessionId,
            "chat_id": chatId,
            "slug": messageId ?? 1,
            "limit": limit,
        ]
    }
}

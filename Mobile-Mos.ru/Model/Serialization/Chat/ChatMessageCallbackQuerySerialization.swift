//
//  ChatMessageCallbackQuerySerialization.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 27.06.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

extension ChatMessageCallbackQuery: SerializableJsonResource {
    
    func serializeToJson() -> NSDictionary {
        
        let userIdValue: Any = user.userId ?? NSNull()
        let messageValue: Any = message?.serializeMessageContentToJson() ?? NSNull()
        let senderId: Any = user.userId ?? user.sessionId
        
        var callbackQueryToJson: [String : Any] = [
            "id": queryId,
            "data": queryData,
            "message": messageValue,
            "chat_id" : conversation.itemId,
            "from": [
                "id": senderId,
            ]
        ]
        
        if let embeddedMessageId = embeddedMessage?.messageId {
            callbackQueryToJson["embedded_message_id"] = embeddedMessageId
        }
        
        return [
            "user_id": userIdValue,
            "session_id": user.sessionId,
            "chat_id": conversation.itemId,
            "callback_query": callbackQueryToJson
        ]
    }
}

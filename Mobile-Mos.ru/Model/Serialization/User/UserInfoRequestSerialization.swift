//
//  UserInfoRequestSerialization.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 24.11.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

extension UserInfoRequest: SerializableJsonResource {
    
    func serializeToJson() -> NSDictionary {
        return [
            "user_id": userId,
            "session_id": sessionId,
        ]
    }
}

//
//  UserRegisterRequestSerialization.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 05.12.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

extension UserRegisterRequest: SerializableJsonResource {
    
    func serializeToJson() -> NSDictionary {
        return [
            "session_id": sessionId,
            "phone"     : phone,
            "password"  : password,
            "code"      : code
        ]
    }
}

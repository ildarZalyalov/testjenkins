//
//  UserLoginRequestSerialization.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 10.11.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

extension UserLoginRequest: SerializableJsonResource {
    
    func serializeToJson() -> NSDictionary {
        return [
            "session_id": sessionId,
            "phone": phone,
            "password": password,
        ]
    }
}

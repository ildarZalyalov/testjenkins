//
//  UserNewPasswordRequestSerialization.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 02.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

extension UserNewPasswordRequest: SerializableJsonResource {
    
    func serializeToJson() -> NSDictionary {
        return [
            "session_id"  : sessionId,
            "code"        : code,
            "new_password": newPassword
        ]
    }
}

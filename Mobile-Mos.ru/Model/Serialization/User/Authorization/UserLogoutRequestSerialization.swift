//
//  UserLogoutRequestSerialization.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 14.11.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

extension UserLogoutRequest: SerializableJsonResource {
    
    func serializeToJson() -> NSDictionary {
        return [
            "user_id": userId,
            "session_id_old": sessionId,
            "session_id_new": newSessionId,
        ]
    }
}

//
//  UserNotificationsSettingChangeRequestSerialization.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 14.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

extension UserNotificationsSettingChangeRequest: SerializableJsonResource {
    
    func serializeToJson() -> NSDictionary {
        return [
            "session_id": sessionId,
            "user_id": userId,
            "chat_id": chatId,
            "enable": isEnabled,
        ]
    }
}

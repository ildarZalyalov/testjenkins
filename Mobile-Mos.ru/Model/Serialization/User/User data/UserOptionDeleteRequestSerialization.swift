//
//  UserOptionDeleteRequestSerialization.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 21.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

extension UserOptionDeleteRequest: SerializableJsonResource {
    
    func serializeToJson() -> NSDictionary {
        return [
            "user_id": userId,
            "session_id": sessionId,
            "id": itemId,
            "type": itemType
        ]
    }
}

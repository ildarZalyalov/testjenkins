//
//  UserOptionAddRequestSerialization.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 28.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

extension UserOptionAddRequest: SerializableJsonResource {
    
    func serializeToJson() -> NSDictionary {
        return [
            "user_id": userId,
            "session_id": sessionId,
            "type": optionType,
            "value": optionValue,
        ]
    }
}

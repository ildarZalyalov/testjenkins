//
//  UserOptionGroupEditRequestSerialization.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 29.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

extension UserOptionGroupEditRequest: SerializableJsonResource {
    
    func serializeToJson() -> NSDictionary {
        
        let requestJson: NSMutableDictionary = [
            "user_id": userId,
            "session_id": sessionId,
            "id": groupId,
            "type": groupType,
        ]
        
        if let alias = groupAlias {
            
            let aliasJson: [String : Any] = [
                "id" : alias.itemId ?? NSNull(),
                "value" : alias.value ?? NSNull()
            ]
            
            requestJson.setValue(aliasJson, forKey: "alias")
        }
        
        var optionValues = [[String : Any]]()
        
        for option in optionChanges {
            optionValues.append([
                "id" : option.itemId ?? NSNull(),
                "type" : option.type,
                "value" : option.value ?? NSNull()
            ])
        }
        
        if !optionValues.isEmpty {
            requestJson.setValue(optionValues, forKey: "options")
        }
        
        return requestJson
    }
}

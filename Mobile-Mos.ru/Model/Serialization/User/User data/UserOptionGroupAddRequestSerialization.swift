//
//  UserOptionGroupAddRequestSerialization.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 28.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

extension UserOptionGroupAddRequest: SerializableJsonResource {
    
    func serializeToJson() -> NSDictionary {
        
        let requestJson: NSMutableDictionary = [
            "user_id": userId,
            "session_id": sessionId,
            "type": groupType,
        ]
        
        if let alias = groupAlias {
            requestJson.setValue(alias, forKey: "alias")
        }
        
        var optionValues = [[String : Any]]()
        
        for option in options {
            optionValues.append([
                "type" : option.type,
                "value" : option.value
            ])
        }
        
        if !optionValues.isEmpty {
            requestJson.setValue(optionValues, forKey: "options")
        }
        
        return requestJson
    }
}

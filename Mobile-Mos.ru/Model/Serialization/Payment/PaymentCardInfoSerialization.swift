//
//  PaymentCardInfoSerialization.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 17.05.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

extension PaymentCardInfo: SerializableJsonResource {
    
    func serializeToJson() -> NSDictionary {
        
        let infoJson = NSMutableDictionary()
        
        if let date = expiryDate {
            infoJson.setValue(date, forKey: "expiryDate")
        }
        if let name = cardholderName {
            infoJson.setValue(name, forKey: "cardholderName")
        }
        if let number = pan {
            infoJson.setValue(number, forKey: "pan")
        }
        if let code = cvvc {
            infoJson.setValue(code, forKey: "cvvc")
        }
        
        return infoJson
    }
}

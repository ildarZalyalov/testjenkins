//
//  PaymentCardSerialization.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 17.05.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

extension PaymentCard: SerializableJsonResource {
    
    func serializeToJson() -> NSDictionary {
        
        let cardJson: NSMutableDictionary = ["needRegister" : needRegister]
        
        if let itemId = cardId {
            cardJson.setValue(itemId, forKey: "id")
        }
        if let typeId = type?.rawValue {
            cardJson.setValue(typeId, forKey: "type")
        }
        if let name = title {
            cardJson.setValue(name, forKey: "title")
        }
        if let info = cardInfo {
            cardJson.setValue(info.serializeToJson(), forKey: "cardInfo")
        }
        if let info = extendedCardInfo {
            cardJson.setValue(info.serializeToJson(), forKey: "extendedCardInfo")
        }
        
        return cardJson
    }
}

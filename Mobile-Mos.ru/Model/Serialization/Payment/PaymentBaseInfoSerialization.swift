//
//  PaymentBaseInfoSerialization.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 17.05.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

extension PaymentBaseInfo: SerializableJsonResource {
    
    func serializeToJson() -> NSDictionary {
        
        let infoJson: NSMutableDictionary = ["withInsurance" : withInsurance]
        
        if let itemId = paymentId {
            infoJson.setValue(itemId, forKey: "paymentId")
        }
        
        return infoJson
    }
}

//
//  PaymentRedirectInfoSerialization.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 18.05.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

extension PaymentRedirectInfo: SerializableJsonResource {
    
    func serializeToJson() -> NSDictionary {
        
        let infoJson = NSMutableDictionary()
        
        if let mdValue = md {
            infoJson.setValue(mdValue, forKey: "MD")
        }
        if let paReqValue = paReq {
            infoJson.setValue(paReqValue, forKey: "PaReq")
        }
        if let terminationUrlValue = terminationUrl?.absoluteString {
            infoJson.setValue(terminationUrlValue, forKey: "TermUrl")
        }
        
        return infoJson
    }
}

//
//  PaymentRequestSerialization.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 17.05.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

extension PaymentRequest: SerializableJsonResource {
    
    func serializeToJson() -> NSDictionary {
        
        let requestJson: NSMutableDictionary = [
            "paymentsId" : payments.map { $0.serializeToJson() },
            "paymentToolType": toolType.rawValue,
            "clientEmail" : clientEmail
        ]
        
        if let client = clientName {
            requestJson.setValue(client.serializeToJson(), forKey: "clientName")
        }
        
        if let portal = portalId {
            requestJson.setValue(portal, forKey: "portalId")
        }
        
        if let sum = amount {
            requestJson.setValue(sum, forKey: "amount")
        }
        if let sum = fullAmount {
            requestJson.setValue(sum, forKey: "fullAmount")
        }
        
        if let card = bankCard {
            requestJson.setValue(card.serializeToJson(), forKey: "cardPaymentTool")
        }
        
        return requestJson
    }
}

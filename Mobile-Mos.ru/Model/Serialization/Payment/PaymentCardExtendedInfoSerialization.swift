//
//  PaymentCardExtendedInfoSerialization.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 17.05.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

extension PaymentCardExtendedInfo: SerializableJsonResource {
    
    func serializeToJson() -> NSDictionary {
        
        let infoJson = NSMutableDictionary()
        
        if let itemId = bankId {
            infoJson.setValue(itemId, forKey: "id")
        }
        if let description = bankDescription {
            infoJson.setValue(description, forKey: "bankDescr")
        }
        if let brandId = brand?.rawValue {
            infoJson.setValue(brandId, forKey: "brand")
        }
        if let typeId = type?.rawValue {
            infoJson.setValue(typeId, forKey: "type")
        }
        
        return infoJson
    }
}

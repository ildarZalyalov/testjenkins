//
//  PaymentClientNameSerialization.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 17.05.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

extension PaymentClientName: SerializableJsonResource {
    
    func serializeToJson() -> NSDictionary {
        return [
            "firstName" : firstName as Any,
            "lastName" : lastName as Any,
            "middleName" : middleName as Any
        ]
    }
}

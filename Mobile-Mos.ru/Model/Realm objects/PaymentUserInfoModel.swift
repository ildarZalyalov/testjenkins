//
//  PaymentUserInfoModel.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 01.06.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers
class PaymentUserInfoModel: Object {
    
    dynamic var email: String = String()
    dynamic var firstName: String = String()
    dynamic var lastName: String = String()
    dynamic var middleName: String = String()
}

extension PaymentUserInfo: Persistable {
    
    func toPersistableObject() -> PaymentUserInfoModel {
        
        let object = PaymentUserInfoModel()
        
        object.email = email
        object.firstName = firstName
        object.lastName = lastName
        object.middleName = middleName
        
        return object
    }
    
    mutating func updateFromPersistableObject(object: PaymentUserInfoModel) {
        email = object.email
        firstName = object.firstName
        lastName = object.lastName
        middleName = object.middleName
    }
}

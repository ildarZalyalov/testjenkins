//
//  WeatherModel.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 11.04.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers
class WeatherModel: Object {
    
    dynamic var nonUniqId: Int = 0
    dynamic var weatherIconId: Int = 0
    dynamic var weatherText: String = String()
    dynamic var temperatureInCelcius: Int = 0
    
    override static func primaryKey() -> String? {
        return #keyPath(nonUniqId)
    }
}

extension Weather: Persistable {
    
    func toPersistableObject() -> WeatherModel {
        
        let object = WeatherModel()
        object.weatherIconId = weatherIconId
        object.weatherText = weatherText
        object.temperatureInCelcius = temperatureInCelcius
        
        return object
    }
    
    mutating func updateFromPersistableObject(object: WeatherModel) {
       
        weatherIconId = object.weatherIconId
        weatherText = object.weatherText
        temperatureInCelcius = object.temperatureInCelcius
    }
}

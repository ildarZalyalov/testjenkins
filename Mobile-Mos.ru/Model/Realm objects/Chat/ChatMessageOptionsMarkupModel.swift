//
//  ChatMessageOptionsMarkupModel.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 16.11.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers
class ChatMessageOptionsMarkupModel: Object {
    
    dynamic var shareCallbackData: String?
}

extension ChatMessageOptionsMarkup: Persistable {
    
    func toPersistableObject() -> ChatMessageOptionsMarkupModel {
        
        let object = ChatMessageOptionsMarkupModel()
        object.shareCallbackData = shareCallbackData
        
        return object
    }
    
    mutating func updateFromPersistableObject(object: ChatMessageOptionsMarkupModel) {
        shareCallbackData = object.shareCallbackData
    }
}

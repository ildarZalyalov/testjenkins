//
//  ConversationModel.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 13.07.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers
class ConversationModel: Object {
    
    dynamic var itemId:                         String = UUID().uuidString.lowercased()
    dynamic var title:                          String = String()
    dynamic var descriptionText:                String = String()
            let greetingMessagesTexts:          List<String> = List()
    dynamic var avatarIconUrl:                  String?
    dynamic var avatarSmallIconUrl:             String?
    dynamic var avatarMiniatureUrl:             String?
    dynamic var avatarPlaceholderIconName:      String?
    dynamic var avatarPlaceholderSmallIconName: String?
    dynamic var avatarPlaceholderMiniatureName: String?
    dynamic var typeRawValue:                   String = Conversation.ConversationType.service.rawValue
    dynamic var unreadMessagesCount:            Int = 0
    dynamic var unseenMessagesCount:            Int = 0
    dynamic var shouldUpdateHistory:            Bool = true
    dynamic var styleModel:                     ConversationStyleModel!
    
    override static func primaryKey() -> String? {
        return #keyPath(itemId)
    }
}

extension Conversation: Persistable {
    
    func toPersistableObject() -> ConversationModel {
        
        let object = ConversationModel()
        
        object.itemId = itemId
        object.title = title
        object.descriptionText = descriptionText
        object.greetingMessagesTexts.append(objectsIn: greetingMessagesTexts)
        object.avatarIconUrl = avatarIconUrl?.absoluteString
        object.avatarSmallIconUrl = avatarSmallIconUrl?.absoluteString
        object.avatarMiniatureUrl = avatarMiniatureUrl?.absoluteString
        object.avatarPlaceholderIconName = avatarPlaceholderIconName
        object.avatarPlaceholderSmallIconName = avatarPlaceholderSmallIconName
        object.avatarPlaceholderMiniatureName = avatarPlaceholderMiniatureName
        object.typeRawValue = type.rawValue
        object.unreadMessagesCount = unreadMessagesCount
        object.unseenMessagesCount = unseenMessagesCount
        object.shouldUpdateHistory = shouldUpdateHistory
        object.styleModel = style.toPersistableObject()
        
        return object
    }
    
    mutating func updateFromPersistableObject(object: ConversationModel) {
        itemId = object.itemId
        title = object.title
        descriptionText = object.descriptionText
        greetingMessagesTexts = Array(object.greetingMessagesTexts)
        if let urlString = object.avatarIconUrl { avatarIconUrl = URL(string: urlString) }
        if let urlString = object.avatarSmallIconUrl { avatarSmallIconUrl = URL(string: urlString) }
        if let urlString = object.avatarMiniatureUrl { avatarMiniatureUrl = URL(string: urlString) }
        avatarPlaceholderIconName = object.avatarPlaceholderIconName
        avatarPlaceholderSmallIconName = object.avatarPlaceholderSmallIconName
        avatarPlaceholderMiniatureName = object.avatarPlaceholderMiniatureName
        type = ConversationType(rawValue: object.typeRawValue) ?? .service
        unreadMessagesCount = object.unreadMessagesCount
        unseenMessagesCount = object.unseenMessagesCount
        shouldUpdateHistory = object.shouldUpdateHistory
        style = ConversationStyle(with: object.styleModel)
    }
}

//
//  ConversationStyleModel.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 17.08.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers
class ConversationStyleModel: Object {
    
    dynamic var isStatusBarDark:                   Bool = false
    dynamic var backButtonColorHex:                Int = 0
    dynamic var titleColorHex:                     Int = 0
    dynamic var gradientColorHex:                  Int = 0
    dynamic var headerColorHex:                    Int = 0
    dynamic var headerShadowColorHex:              Int = 0
    dynamic var outgoingMessageTextColorHex:       Int = 0
    dynamic var outgoingMessageBackgroundColorHex: Int = 0
    dynamic var controlsTextColorHex:              Int = 0
    dynamic var controlsShadowColorHex:            Int = 0
}

extension ConversationStyle: Persistable {
    
    func toPersistableObject() -> ConversationStyleModel {
        
        let object = ConversationStyleModel()
        
        object.isStatusBarDark = isStatusBarDark
        object.backButtonColorHex = backButtonColor.toHex()
        object.titleColorHex = titleColor.toHex()
        object.gradientColorHex = gradientColor.toHex()
        object.headerColorHex = headerColor.toHex()
        object.headerShadowColorHex = headerShadowColor.toHex()
        object.outgoingMessageTextColorHex = outgoingMessageTextColor.toHex()
        object.outgoingMessageBackgroundColorHex = outgoingMessageBackgroundColor.toHex()
        object.controlsTextColorHex = controlsTextColor.toHex()
        object.controlsShadowColorHex = controlsShadowColor.toHex()
        
        return object
    }
    
    mutating func updateFromPersistableObject(object: ConversationStyleModel) {
        isStatusBarDark = object.isStatusBarDark
        backButtonColor = UIColor.fromHex(hex: object.backButtonColorHex)
        titleColor = UIColor.fromHex(hex: object.titleColorHex)
        gradientColor = UIColor.fromHex(hex: object.gradientColorHex)
        headerColor = UIColor.fromHex(hex: object.headerColorHex)
        headerShadowColor = UIColor.fromHex(hex: object.headerShadowColorHex)
        outgoingMessageTextColor = UIColor.fromHex(hex: object.outgoingMessageTextColorHex)
        outgoingMessageBackgroundColor = UIColor.fromHex(hex: object.outgoingMessageBackgroundColorHex)
        controlsTextColor = UIColor.fromHex(hex: object.controlsTextColorHex)
        controlsShadowColor = UIColor.fromHex(hex: object.controlsShadowColorHex)
    }
}

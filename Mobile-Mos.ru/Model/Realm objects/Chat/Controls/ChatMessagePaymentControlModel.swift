//
//  ChatMessagePaymentControlModel.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 22.01.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers
class ChatMessagePaymentControlModel: Object {
    
    dynamic var subject:    String = String()
    dynamic var sum:        Float = 0
    dynamic var parameters: String = String()
}

extension ChatMessagePaymentControl: Persistable {
    
    func toPersistableObject() -> ChatMessagePaymentControlModel {
        
        let object = ChatMessagePaymentControlModel()
        
        object.subject = subject
        object.sum = sum
        object.parameters = parameters
        
        return object
    }
    
    mutating func updateFromPersistableObject(object: ChatMessagePaymentControlModel) {
        subject = object.subject
        sum = object.sum
        parameters = object.parameters
    }
}

//
//  ChatCustomLocationControlModel.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 11.12.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers
class ChatCustomLocationControlModel: Object {
    
    dynamic var typeRawValue: String = ChatCustomLocationControl.ControlType.onMap.rawValue
}

extension ChatCustomLocationControl: Persistable {
    
    func toPersistableObject() -> ChatCustomLocationControlModel {
        
        let object = ChatCustomLocationControlModel()
        object.typeRawValue = type.rawValue
        
        return object
    }
    
    mutating func updateFromPersistableObject(object: ChatCustomLocationControlModel) {
        type = ControlType(rawValue: object.typeRawValue) ?? .onMap
    }
}

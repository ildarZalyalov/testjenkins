//
//  ChatMessageDetailsControlModel.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 12.09.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers
class ChatMessageDetailsControlModel: Object {
    
    dynamic var title:            String = String()
    dynamic var templateUrl:      String = String()
    dynamic var valuesJsonString: String = String()
            let buttonModels:     List<ChatCustomControlButtonModel> = List()
}

extension ChatMessageDetailsControl: Persistable {
    
    func toPersistableObject() -> ChatMessageDetailsControlModel {
        
        let object = ChatMessageDetailsControlModel()
        
        object.title = title
        object.templateUrl = templateUrl.absoluteString
        object.valuesJsonString = valuesJsonString
        object.buttonModels.append(objectsIn: buttons.map { $0.toPersistableObject() })
        
        return object
    }
    
    mutating func updateFromPersistableObject(object: ChatMessageDetailsControlModel) {
        title = object.title
        templateUrl = URL(string: object.templateUrl) ?? URL(fileURLWithPath: String())
        valuesJsonString = object.valuesJsonString
        buttons = object.buttonModels.toPonsoArray()
    }
}

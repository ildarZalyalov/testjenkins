//
//  ChatCustomControlButtonModel.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 07.07.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers
class ChatCustomControlButtonModel: Object {
    
    dynamic var itemId:      String = String()
    dynamic var title:       String = String()
    dynamic var actionModel: ChatMessageButtonActionModel?
    
    override static func primaryKey() -> String? {
        return #keyPath(itemId)
    }
}

extension ChatCustomControlButton: Persistable {
    
    func toPersistableObject() -> ChatCustomControlButtonModel {
        
        let object = ChatCustomControlButtonModel()
        
        object.itemId = itemId
        object.title = title
        object.actionModel = action?.toPersistableObject()
        
        return object
    }
    
    mutating func updateFromPersistableObject(object: ChatCustomControlButtonModel) {
        itemId = object.itemId
        title = object.title
        if let actionModel = object.actionModel { action = ChatMessageButtonAction(with: actionModel) }
    }
}

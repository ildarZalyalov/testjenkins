//
//  ChatMessageCalendarControlModel.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 10.07.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers
class ChatMessageCalendarControlModel: Object {
    
    dynamic var title:                       String = String()
    dynamic var submitTitle:                 String = String()
    dynamic var initialDate:                 Date?
    dynamic var initialFloatingDateRawValue: String?
    dynamic var minimumDate:                 Date?
    dynamic var minimumFloatingDateRawValue: String?
    dynamic var maximumDate:                 Date?
    dynamic var maximumFloatingDateRawValue: String?
    dynamic var dateOutputFormat:            String = String()
    dynamic var disableSelectDays:           Bool = false
    dynamic var disableSelectYear:           Bool = false
}

extension ChatMessageCalendarControl: Persistable {
    
    func toPersistableObject() -> ChatMessageCalendarControlModel {
        
        let object = ChatMessageCalendarControlModel()
        
        object.title = title
        object.submitTitle = submitTitle
        object.initialDate = initialDate
        object.initialFloatingDateRawValue = initialFloatingDate?.rawValue
        object.minimumDate = minimumDate
        object.minimumFloatingDateRawValue = minimumFloatingDate?.rawValue
        object.maximumDate = maximumDate
        object.maximumFloatingDateRawValue = maximumFloatingDate?.rawValue
        object.dateOutputFormat = dateOutputFormat
        object.disableSelectDays = disableSelectDays
        object.disableSelectYear = disableSelectYear
        
        return object
    }
    
    mutating func updateFromPersistableObject(object: ChatMessageCalendarControlModel) {
        
        title = object.title
        submitTitle = object.submitTitle
        initialDate = object.initialDate
        initialFloatingDate = ChatMessageCalendarFloatingDate(rawValueOptional: object.initialFloatingDateRawValue)
        minimumDate = object.minimumDate
        minimumFloatingDate = ChatMessageCalendarFloatingDate(rawValueOptional: object.minimumFloatingDateRawValue)
        maximumDate = object.maximumDate
        maximumFloatingDate = ChatMessageCalendarFloatingDate(rawValueOptional: object.maximumFloatingDateRawValue)
        dateOutputFormat = object.dateOutputFormat
        disableSelectDays = object.disableSelectDays
        disableSelectYear = object.disableSelectYear
    }
}

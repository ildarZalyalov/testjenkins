//
//  ChatMessageShareControlModel.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 27.02.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers
class ChatMessageShareControlModel: Object {
    
    dynamic var text: String = String()
}

extension ChatMessageShareControl: Persistable {
    
    func toPersistableObject() -> ChatMessageShareControlModel {
        
        let object = ChatMessageShareControlModel()
        object.text = text
        
        return object
    }
    
    mutating func updateFromPersistableObject(object: ChatMessageShareControlModel) {
        text = object.text
    }
}

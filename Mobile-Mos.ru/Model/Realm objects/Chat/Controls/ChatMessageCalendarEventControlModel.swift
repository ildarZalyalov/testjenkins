//
//  ChatMessageCalendarEventControlModel.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 08.12.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers
class ChatMessageCalendarEventControlModel: Object {
    
    dynamic var title:     String = String()
    dynamic var startDate: Date = Date()
    dynamic var endDate:   Date?
}

extension ChatMessageCalendarEventControl: Persistable {
    
    func toPersistableObject() -> ChatMessageCalendarEventControlModel {
        
        let object = ChatMessageCalendarEventControlModel()
        
        object.title = title
        object.startDate = startDate
        object.endDate = endDate
        
        return object
    }
    
    mutating func updateFromPersistableObject(object: ChatMessageCalendarEventControlModel) {
        title = object.title
        startDate = object.startDate
        endDate = object.endDate
    }
}

//
//  ChatMessageTextInputControlModel.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 21.08.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers
class ChatMessageTextInputControlModel: Object {
    
    dynamic var title:       String?
            let inputModels: List<ChatMessageTextInputModel> = List()
}

@objcMembers
class ChatMessageTextInputModel: Object {
    
    dynamic var inputId:      String = String()
    dynamic var title:        String?
    dynamic var text:         String?
    dynamic var placeholder:  String?
    dynamic var typeRawValue: String = ChatMessageTextInputControl.TextInput.InputType.text.rawValue
    dynamic var formatRegex:  String?
    dynamic var isSecure:     Bool = false
}

extension ChatMessageTextInputControl: Persistable {
    
    func toPersistableObject() -> ChatMessageTextInputControlModel {
        
        let object = ChatMessageTextInputControlModel()
        
        object.title = title
        object.inputModels.append(objectsIn: inputs.map { $0.toPersistableObject() })
        
        return object
    }
    
    mutating func updateFromPersistableObject(object: ChatMessageTextInputControlModel) {
        title = object.title
        inputs = object.inputModels.toPonsoArray()
    }
}

extension ChatMessageTextInputControl.TextInput: Persistable {
    
    func toPersistableObject() -> ChatMessageTextInputModel {
        
        let object = ChatMessageTextInputModel()
        
        object.inputId = inputId
        object.title = title
        object.text = text
        object.placeholder = placeholder
        object.typeRawValue = type.rawValue
        object.formatRegex = formatRegex
        object.isSecure = isSecure
        
        return object
    }
    
    mutating func updateFromPersistableObject(object: ChatMessageTextInputModel) {
        inputId = object.inputId
        title = object.title
        text = object.text
        placeholder = object.placeholder
        type = InputType(rawValue: object.typeRawValue) ?? .text
        formatRegex = object.formatRegex
        isSecure = object.isSecure
    }
}

//
//  ChatMessageTimetableControlModel.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 07.07.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers
class ChatMessageTimetableControlModel: Object {
    
    dynamic var title:                   String = String()
    dynamic var submitTitle:             String = String()
            let datesAndTimesModels:     List<ChatMessageTimetableDateTimeModel> = List()
    dynamic var dateOutputFormat:        String = String()
    dynamic var timeOutputFormat:        String = String()
    dynamic var dateTimeSeparatorString: String = String()
}

@objcMembers
class ChatMessageTimetableDateTimeModel: Object {
    
    dynamic var date:       Date = Date()
            let timeModels: List<ChatMessageTimetableTimeValueModel> = List()
    
    convenience init(with date: Date, and times: [ChatMessageTimetableControl.TimeValue]) {
        self.init()
        self.date = date
        self.timeModels.append(objectsIn: times.toPersistableObjectsArray())
    }
}

@objcMembers
class ChatMessageTimetableTimeValueModel: Object {
    
    dynamic var time: Date = Date()
    dynamic var data: String?
}

extension ChatMessageTimetableControl: Persistable {
    
    func toPersistableObject() -> ChatMessageTimetableControlModel {
        
        let object = ChatMessageTimetableControlModel()
        
        object.title = title
        object.submitTitle = submitTitle
        object.dateOutputFormat = dateOutputFormat
        object.timeOutputFormat = timeOutputFormat
        object.dateTimeSeparatorString = dateTimeSeparatorString
        
        object.datesAndTimesModels.append(objectsIn: datesAndTimes.map {
            ChatMessageTimetableDateTimeModel(with: $0.key, and: $0.value)
        })
        
        return object
    }
    
    mutating func updateFromPersistableObject(object: ChatMessageTimetableControlModel) {
        
        title = object.title
        submitTitle = object.submitTitle
        dateOutputFormat = object.dateOutputFormat
        timeOutputFormat = object.timeOutputFormat
        dateTimeSeparatorString = object.dateTimeSeparatorString
        
        for datesAndTimesModel in object.datesAndTimesModels {
            datesAndTimes[datesAndTimesModel.date] = datesAndTimesModel.timeModels.toPonsoArray()
        }
    }
}

extension ChatMessageTimetableControl.TimeValue: Persistable {
    
    func toPersistableObject() -> ChatMessageTimetableTimeValueModel {
        
        let object = ChatMessageTimetableTimeValueModel()
        object.time = time
        object.data = data
        
        return object
    }
    
    mutating func updateFromPersistableObject(object: ChatMessageTimetableTimeValueModel) {
        time = object.time
        data = object.data
    }
}

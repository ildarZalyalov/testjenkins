//
//  ChatMessageAlertControlModel.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 10.07.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers
class ChatMessageAlertControlModel: Object {
    
    dynamic var title:        String = String()
    dynamic var text:         String?
            let buttonModels: List<ChatCustomControlButtonModel> = List()
}

extension ChatMessageAlertControl: Persistable {
    
    func toPersistableObject() -> ChatMessageAlertControlModel {
        
        let object = ChatMessageAlertControlModel()
        
        object.title = title
        object.text = text
        object.buttonModels.append(objectsIn: buttons.map { $0.toPersistableObject() })
        
        return object
    }
    
    mutating func updateFromPersistableObject(object: ChatMessageAlertControlModel) {
        title = object.title
        text = object.text
        buttons = object.buttonModels.toPonsoArray()
    }
}

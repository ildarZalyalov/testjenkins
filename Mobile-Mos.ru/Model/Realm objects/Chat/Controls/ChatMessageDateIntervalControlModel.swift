//
//  ChatMessageDateIntervalControlModel.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 22.11.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers
class ChatMessageDateIntervalControlModel: Object {
    
    dynamic var title:                           String = String()
    dynamic var submitTitle:                     String = String()
    dynamic var initialDateFrom:                 Date?
    dynamic var initialFloatingDateFromRawValue: String?
    dynamic var initialDateTo:                   Date?
    dynamic var initialFloatingDateToRawValue:   String?
    dynamic var minimumDate:                     Date?
    dynamic var minimumFloatingDateRawValue:     String?
    dynamic var maximumDate:                     Date?
    dynamic var maximumFloatingDateRawValue:     String?
    dynamic var dateOutputFormat:                String = String()
    dynamic var dateSeparatorString:             String = String()
    dynamic var disableSelectDays:               Bool = false
    dynamic var disableSelectYear:               Bool = false
}

extension ChatMessageDateIntervalControl: Persistable {
    
    func toPersistableObject() -> ChatMessageDateIntervalControlModel {
        
        let object = ChatMessageDateIntervalControlModel()
        
        object.title = title
        object.submitTitle = submitTitle
        object.initialDateFrom = initialDateFrom
        object.initialFloatingDateFromRawValue = initialFloatingDateFrom?.rawValue
        object.initialDateTo = initialDateTo
        object.initialFloatingDateToRawValue = initialFloatingDateTo?.rawValue
        object.minimumDate = minimumDate
        object.minimumFloatingDateRawValue = minimumFloatingDate?.rawValue
        object.maximumDate = maximumDate
        object.maximumFloatingDateRawValue = maximumFloatingDate?.rawValue
        object.dateOutputFormat = dateOutputFormat
        object.dateSeparatorString = dateSeparatorString
        object.disableSelectDays = disableSelectDays
        object.disableSelectYear = disableSelectYear
        
        return object
    }
    
    mutating func updateFromPersistableObject(object: ChatMessageDateIntervalControlModel) {
        title = object.title
        submitTitle = object.submitTitle
        initialDateFrom = object.initialDateFrom
        initialFloatingDateFrom = ChatMessageCalendarFloatingDate(rawValueOptional: object.initialFloatingDateFromRawValue)
        initialDateTo = object.initialDateTo
        initialFloatingDateTo = ChatMessageCalendarFloatingDate(rawValueOptional: object.initialFloatingDateToRawValue)
        minimumDate = object.minimumDate
        minimumFloatingDate = ChatMessageCalendarFloatingDate(rawValueOptional: object.minimumFloatingDateRawValue)
        maximumDate = object.maximumDate
        maximumFloatingDate = ChatMessageCalendarFloatingDate(rawValueOptional: object.maximumFloatingDateRawValue)
        dateOutputFormat = object.dateOutputFormat
        dateSeparatorString = object.dateSeparatorString
        disableSelectDays = object.disableSelectDays
        disableSelectYear = object.disableSelectYear
    }
}

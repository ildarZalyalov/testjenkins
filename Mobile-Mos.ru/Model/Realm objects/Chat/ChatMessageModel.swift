//
//  ChatMessageModel.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 02.05.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers
class ChatMessageModel: Object {

    dynamic var uid:                String = UUID().uuidString.lowercased()
    dynamic var parentMessageUid:   String?
    dynamic var messageId:          Int64 = Int64.max
    dynamic var messageContext:     String = String()
    dynamic var userId:             String?
    dynamic var sessionId:          String = String()
    dynamic var chatId:             String = String()
    dynamic var isIncoming:         Bool = false
    dynamic var date:               Date = Date()
    dynamic var statusRawValue:     Int = ChatMessage.Status.sending.rawValue
    dynamic var scenarioTitle:      String?
    dynamic var statsData:          StatsDataModel?
    dynamic var contentModel:       ChatMessageContentModel!
    dynamic var replyMarkupModel:   ChatMessageReplyMarkupModel!
    dynamic var optionsMarkupModel: ChatMessageOptionsMarkupModel!
    
    override static func primaryKey() -> String? {
        return #keyPath(uid)
    }
    
    override static func indexedProperties() -> [String] {
        return [#keyPath(messageId),
                #keyPath(statusRawValue)]
    }
}

extension ChatMessage: Persistable {
    
    func toPersistableObject() -> ChatMessageModel {
        
        let object = ChatMessageModel()
        
        object.uid = uid
        object.parentMessageUid = parentMessageUid
        object.messageId = messageId
        object.messageContext = messageContext
        object.userId = userId
        object.sessionId = sessionId
        object.chatId = chatId
        object.isIncoming = isIncoming
        object.date = date
        object.statusRawValue = status.rawValue
        object.scenarioTitle = scenarioTitle
        object.contentModel = content.toPersistableObject()
        object.replyMarkupModel = replyMarkup.toPersistableObject()
        object.optionsMarkupModel = optionsMarkup.toPersistableObject()
        object.statsData = statsData?.toPersistableObject()
        
        return object
    }
    
    mutating func updateFromPersistableObject(object: ChatMessageModel) {
        
        uid = object.uid
        parentMessageUid = object.parentMessageUid
        messageId = object.messageId
        messageContext = object.messageContext
        userId = object.userId
        sessionId = object.sessionId
        chatId = object.chatId
        isIncoming = object.isIncoming
        date = object.date
        status = Status(rawValue: object.statusRawValue) ?? .sending
        scenarioTitle = object.scenarioTitle
        content = ChatMessageContent(with: object.contentModel)
        replyMarkup = ChatMessageReplyMarkup(with: object.replyMarkupModel)
        optionsMarkup = ChatMessageOptionsMarkup(with: object.optionsMarkupModel)
        if let data = object.statsData { statsData = StatsData(with: data) }
    }
}

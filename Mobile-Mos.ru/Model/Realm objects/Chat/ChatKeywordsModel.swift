//
//  ChatKeywordsModel.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 14.02.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers
class ChatKeywordsModel: Object {
    
    dynamic var chatId:   String = String()
    dynamic var keywords: String = String()
    
    override static func primaryKey() -> String? {
        return #keyPath(chatId)
    }
}

extension ChatKeywords: Persistable {
    
    func toPersistableObject() -> ChatKeywordsModel {
        
        let object = ChatKeywordsModel()
        object.chatId = chatId
        object.keywords = keywords
        
        return object
    }
    
    mutating func updateFromPersistableObject(object: ChatKeywordsModel) {
        chatId = object.chatId
        keywords = object.keywords
    }
}

//
//  ChatMessageGroupObjectModel.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 15.09.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers
class ChatMessageGroupModel: Object {
    
    let messageGroupModel: List<ChatMessageModel> = List()
    
    convenience init(messageModels: [ChatMessageModel]) {
        self.init()
        messageGroupModel.append(objectsIn: messageModels)
    }
}

@objcMembers
class ChatMessageGroupObjectModel: Object {
    
    let messageGroupModels:        List<ChatMessageGroupModel> = List()
    let messageGroupHistoryModels: List<ChatMessageModel> = List()
}

extension ChatMessageGroupObject: Persistable {
    
    func toPersistableObject() -> ChatMessageGroupObjectModel {
 
        let object = ChatMessageGroupObjectModel()
        object.messageGroupModels.append(objectsIn: messageGroups.map { ChatMessageGroupModel(messageModels: $0.toPersistableObjectsArray()) })
        object.messageGroupHistoryModels.append(objectsIn: messageGroupHistory.toPersistableObjectsArray())
        
        return object
    }
    
    mutating func updateFromPersistableObject(object: ChatMessageGroupObjectModel) {
        messageGroups = object.messageGroupModels.map { $0.messageGroupModel.toPonsoArray() }
        messageGroupHistory = object.messageGroupHistoryModels.toPonsoArray()
    }
}

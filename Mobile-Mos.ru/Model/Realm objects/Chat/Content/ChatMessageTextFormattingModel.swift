//
//  ChatMessageTextFormattingModel.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 05.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers
class ChatTextFormattingTagAttributesModel: Object {
    
    dynamic var tag:   String = String()
    dynamic var value: String = String()
    
    convenience init(tag: String, value: String) {
        self.init()
        self.tag = tag
        self.value = value
    }
}

@objcMembers
class ChatTextRangeFormattingModel: Object {
    
    dynamic var tag:                      String = String()
    dynamic var rangeLocation:            Int = 0
    dynamic var rangeLength:              Int = 0
            let tagAttributesModel:       List<ChatTextFormattingTagAttributesModel> = List()
            let paragraphAttributesModel: List<ChatTextFormattingTagAttributesModel> = List()
}

@objcMembers
class ChatMessageTextFormattingModel: Object {
    
    dynamic var trimmedText:               String = String()
            let formattingRangesModel:     List<ChatTextRangeFormattingModel> = List()
    dynamic var messageBackgroundColorHex: RealmOptional<Int> = RealmOptional()
}

extension ChatMessageTextFormatting: Persistable {
    
    //MARK: Persistable

    func toPersistableObject() -> ChatMessageTextFormattingModel {

        let object = ChatMessageTextFormattingModel()
        object.trimmedText = trimmedText
        object.formattingRangesModel.append(objectsIn: formattingRanges.map { convertToPersistableObject(formatting: $0) })
        object.messageBackgroundColorHex.value = messageBackgroundColor?.toHex(encodingAlpha: true)

        return object
    }

    mutating func updateFromPersistableObject(object: ChatMessageTextFormattingModel) {
        trimmedText = object.trimmedText
        formattingRanges = object.formattingRangesModel.map { convertToPonsoObject(object: $0) }
        if let messageBackgroundColorHex = object.messageBackgroundColorHex.value { messageBackgroundColor = UIColor.fromHex(hex: messageBackgroundColorHex, decodingAlpha: true) }
    }
    
    //MARK: Хелперы
    
    fileprivate func convertToPersistableObject(formatting: ChatTextRangeFormatting) -> ChatTextRangeFormattingModel {
        
        let object = ChatTextRangeFormattingModel()
        object.tag = formatting.tag
        object.rangeLocation = formatting.range.location
        object.rangeLength = formatting.range.length
        
        object.tagAttributesModel.append(objectsIn: formatting.tagAttributes.map {
            ChatTextFormattingTagAttributesModel(tag: $0.key.rawValue, value: $0.value)
        })
        object.paragraphAttributesModel.append(objectsIn: formatting.paragraphAttributes.map {
            ChatTextFormattingTagAttributesModel(tag: $0.key.rawValue, value: $0.value)
        })
        
        return object
    }
    
    fileprivate func convertToPonsoObject(object: ChatTextRangeFormattingModel) -> ChatTextRangeFormatting {
        
        let range = NSMakeRange(object.rangeLocation, object.rangeLength)
        var tagAttributes = [ChatTextFormattingTagAttributes : String]()
        var paragraphAttributes = [ChatTextFormattingParagraphAttributes : String]()
        
        for tagModel in object.tagAttributesModel {
            guard let attribute = ChatTextFormattingTagAttributes(rawValue: tagModel.tag) else { continue }
            tagAttributes[attribute] = tagModel.value
        }
        
        for paragraphModel in object.paragraphAttributesModel {
            guard let attribute = ChatTextFormattingParagraphAttributes(rawValue: paragraphModel.tag) else { continue }
            paragraphAttributes[attribute] = paragraphModel.value
        }
        
        return ChatTextRangeFormatting(tag: object.tag, range: range, tagAttributes: tagAttributes, paragraphAttributes: paragraphAttributes)
    }
}


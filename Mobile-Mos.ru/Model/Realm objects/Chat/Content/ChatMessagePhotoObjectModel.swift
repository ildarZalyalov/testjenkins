//
//  ChatMessagePhotoObjectModel.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 12.09.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers
class ChatMessagePhotoObjectModel: Object {
    
    dynamic var photoUrl:    String = String()
            let photoWidth:  RealmOptional<Double> = RealmOptional()
            let photoHeight: RealmOptional<Double> = RealmOptional()
}

extension ChatMessagePhotoObject: Persistable {
    
    func toPersistableObject() -> ChatMessagePhotoObjectModel {
        
        let object = ChatMessagePhotoObjectModel()
        object.photoUrl = photoUrl.absoluteString
        
        if let size = photoSize {
            object.photoWidth.value = Double(size.width)
            object.photoHeight.value = Double(size.height)
        }
        
        return object
    }
    
    mutating func updateFromPersistableObject(object: ChatMessagePhotoObjectModel) {
        
        photoUrl = URL(string: object.photoUrl) ?? URL(fileURLWithPath: String())
        
        if let width = object.photoWidth.value, let height = object.photoHeight.value {
            photoSize = CGSize(width: width, height: height)
        }
    }
}

//
//  ChatMessageMapObjectModel.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 07.07.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers
class ChatMessageMapObjectModel: Object {
    
    dynamic var latitute:          Double = 0
    dynamic var longitute:         Double = 0
    dynamic var title:             String?
    dynamic var descriptionString: String?
}

extension ChatMessageMapObject: Persistable {
    
    func toPersistableObject() -> ChatMessageMapObjectModel {
        
        let object = ChatMessageMapObjectModel()
        
        object.latitute = location.latitude
        object.longitute = location.longitude
        object.title = title
        object.descriptionString = description
        
        return object
    }
    
    mutating func updateFromPersistableObject(object: ChatMessageMapObjectModel) {
        location = Location(latitude: object.latitute, longitude: object.longitute)
        title = object.title
        description = object.descriptionString
    }
}

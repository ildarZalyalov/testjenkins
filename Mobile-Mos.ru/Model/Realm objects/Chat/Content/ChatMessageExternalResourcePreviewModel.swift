//
//  ChatMessageExternalResourcePreviewModel.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 13.09.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers
class ChatMessageExternalResourcePreviewModel: Object {
    
            let photoUrls:         List<String> = List()
    dynamic var title:             String?
    dynamic var descriptionString: String?
    dynamic var hideSource:        Bool = false
    dynamic var publicationDate:   Date?
}

extension ChatMessageExternalResourcePreview: Persistable {
    
    func toPersistableObject() -> ChatMessageExternalResourcePreviewModel {
        
        let object = ChatMessageExternalResourcePreviewModel()
        object.photoUrls.append(objectsIn: photoUrls.map { $0.absoluteString })
        object.title = title
        object.descriptionString = description
        object.hideSource = hideSource
        object.publicationDate = publicationDate
        
        return object
    }
    
    mutating func updateFromPersistableObject(object: ChatMessageExternalResourcePreviewModel) {
        
        photoUrls = object.photoUrls.compactMap {
            return URL(string: $0) ?? URL(fileURLWithPath: String())
        }
        
        title = object.title
        description = object.descriptionString
        hideSource = object.hideSource
        publicationDate = object.publicationDate
    }
}

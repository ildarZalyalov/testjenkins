//
//  ChatMessageScrollObjectModel.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 21.09.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers
class ChatMessageScrollObjectModel: Object {
    
    dynamic var typeRawValue:   String = ChatMessageScrollObject.EmbeddingType.horizontalScroll.rawValue
            let messagesModels: List<ChatMessageModel> = List()
}

extension ChatMessageScrollObject: Persistable {
    
    func toPersistableObject() -> ChatMessageScrollObjectModel {
        
        let object = ChatMessageScrollObjectModel()
        object.typeRawValue = type.rawValue
        object.messagesModels.append(objectsIn: messages.toPersistableObjectsArray())
        
        return object
    }
    
    mutating func updateFromPersistableObject(object: ChatMessageScrollObjectModel) {
        type = ChatMessageScrollObject.EmbeddingType(rawValue: object.typeRawValue) ?? .horizontalScroll
        messages = object.messagesModels.toPonsoArray()
    }
}

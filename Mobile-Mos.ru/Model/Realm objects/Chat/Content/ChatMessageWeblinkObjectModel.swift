//
//  ChatMessageWeblinkObjectModel.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 12.09.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers
class ChatMessageWeblinkObjectModel: Object {
    
    dynamic var linkUrl:        String = String()
    dynamic var typeRawValue:   String = ChatMessageWeblinkObject.LinkType.regular.rawValue
    dynamic var disablePreview: Bool = false
    dynamic var previewModel:   ChatMessageExternalResourcePreviewModel?
    dynamic var statsData: StatsDataModel!
}

extension ChatMessageWeblinkObject: Persistable {
    
    func toPersistableObject() -> ChatMessageWeblinkObjectModel {
        
        let object = ChatMessageWeblinkObjectModel()
        object.linkUrl = linkUrl.absoluteString
        object.typeRawValue = type.rawValue
        object.disablePreview = disablePreview
        object.previewModel = preview?.toPersistableObject()
        object.statsData = statsData.toPersistableObject()
        
        return object
    }
    
    mutating func updateFromPersistableObject(object: ChatMessageWeblinkObjectModel) {
        linkUrl = URL(string: object.linkUrl) ?? URL(fileURLWithPath: String())
        type = ChatMessageWeblinkObject.LinkType(rawValue: object.typeRawValue) ?? .regular
        disablePreview = object.disablePreview
        statsData = StatsData(with:  object.statsData)
        if let previewModel = object.previewModel { preview = ChatMessageExternalResourcePreview(with: previewModel) }
    }
}

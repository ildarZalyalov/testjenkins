//
//  ChatMessageContentModel.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 13.06.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers
class ChatMessageContentModel: Object {
    
    dynamic var typeRawValue:             String = ChatMessageContent.ContentType.text.rawValue
    dynamic var text:                     String = String()
    dynamic var textFormattingModel:      ChatMessageTextFormattingModel?
    dynamic var mapObjectModel:           ChatMessageMapObjectModel?
    dynamic var photoObjectModel:         ChatMessagePhotoObjectModel?
    dynamic var weblinkObjectModel:       ChatMessageWeblinkObjectModel?
    dynamic var documentObjectModel:      ChatMessageDocumentObjectModel?
    dynamic var messageGroupObjectModel:  ChatMessageGroupObjectModel?
    dynamic var messageScrollObjectModel: ChatMessageScrollObjectModel?
    
    //поле превью сохраняется, но не читается из базы, но по нему идет поиск сообщений
    dynamic var previewString: String = String()
}

extension ChatMessageContent: Persistable {
    
    func toPersistableObject() -> ChatMessageContentModel {
        
        let object = ChatMessageContentModel()
        
        object.typeRawValue = type.rawValue
        object.text = text
        object.textFormattingModel = textFormatting?.toPersistableObject()
        object.mapObjectModel = mapObject?.toPersistableObject()
        object.photoObjectModel = photoObject?.toPersistableObject()
        object.weblinkObjectModel = weblinkObject?.toPersistableObject()
        object.documentObjectModel = documentObject?.toPersistableObject()
        object.messageGroupObjectModel = messageGroupObject?.toPersistableObject()
        object.messageScrollObjectModel = messageScrollObject?.toPersistableObject()
        
        object.previewString = previewString
        
        return object
    }
    
    mutating func updateFromPersistableObject(object: ChatMessageContentModel) {
        type = ContentType(rawValue: object.typeRawValue) ?? .text
        text = object.text
        if let textFormattingModel = object.textFormattingModel { textFormatting = ChatMessageTextFormatting(with: textFormattingModel) }
        if let mapObjectModel = object.mapObjectModel { mapObject = ChatMessageMapObject(with: mapObjectModel) }
        if let photoObjectModel = object.photoObjectModel { photoObject = ChatMessagePhotoObject(with: photoObjectModel) }
        if let weblinkObjectModel = object.weblinkObjectModel { weblinkObject = ChatMessageWeblinkObject(with: weblinkObjectModel) }
        if let documentObjectModel = object.documentObjectModel { documentObject = ChatMessageDocumentObject(with: documentObjectModel) }
        if let messageGroupObjectModel = object.messageGroupObjectModel { messageGroupObject = ChatMessageGroupObject(with: messageGroupObjectModel) }
        if let messageScrollObjectModel = object.messageScrollObjectModel { messageScrollObject = ChatMessageScrollObject(with: messageScrollObjectModel) }
    }
}

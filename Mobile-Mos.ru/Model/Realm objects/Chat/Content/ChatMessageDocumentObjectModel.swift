//
//  ChatMessageDocumentObjectModel.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 12.09.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers
class ChatMessageDocumentObjectModel: Object {
    
    dynamic var fileUrl:  String = String()
    dynamic var fileName: String = String()
    dynamic var statsData: StatsDataModel!
            let fileSize: RealmOptional<Float> = RealmOptional()
}

extension ChatMessageDocumentObject: Persistable {
    
    func toPersistableObject() -> ChatMessageDocumentObjectModel {
        
        let object = ChatMessageDocumentObjectModel()
        object.fileUrl = fileUrl.absoluteString
        object.fileName = fileName
        object.fileSize.value = fileSize
        object.statsData = statsData.toPersistableObject()
        
        return object
    }
    
    mutating func updateFromPersistableObject(object: ChatMessageDocumentObjectModel) {
        fileUrl = URL(string: object.fileUrl) ?? URL(fileURLWithPath: String())
        fileName = object.fileName
        fileSize = object.fileSize.value
        statsData = StatsData(with: object.statsData)
    }
}

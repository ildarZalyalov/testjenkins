//
//  ChatMessageButtonActionModel.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 07.07.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers
class ChatMessageButtonActionModel: Object {
    
    dynamic var typeRawValue:              String = String()
    dynamic var phone:                     String?
    dynamic var url:                       String?
    dynamic var callbackData:              String?
    dynamic var messageContext:            String?
    dynamic var shareDataModel:            ChatMessageShareControlModel?
    dynamic var alertModel:                ChatMessageAlertControlModel?
    dynamic var textInputModel:            ChatMessageTextInputControlModel?
    dynamic var calendarModel:             ChatMessageCalendarControlModel?
    dynamic var dateIntervalCalendarModel: ChatMessageDateIntervalControlModel?
    dynamic var timetableModel:            ChatMessageTimetableControlModel?
    dynamic var mapObjectModel:            ChatMessageMapObjectModel?
    dynamic var customLocationObjectModel: ChatCustomLocationControlModel?
    dynamic var detailsModel:              ChatMessageDetailsControlModel?
    dynamic var calendarEventModel:        ChatMessageCalendarEventControlModel?
    dynamic var paymentModel:              ChatMessagePaymentControlModel?
    dynamic var statsData:                 StatsDataModel?
}

extension ChatMessageButtonAction: Persistable {
    
    func toPersistableObject() -> ChatMessageButtonActionModel {
        
        let object = ChatMessageButtonActionModel()
        
        object.typeRawValue = type.rawValue
        object.phone = phone
        object.url = url?.absoluteString
        object.callbackData = callbackData
        object.messageContext = messageContext
        object.shareDataModel = shareData?.toPersistableObject()
        object.alertModel = alert?.toPersistableObject()
        object.textInputModel = textInput?.toPersistableObject()
        object.calendarModel = calendar?.toPersistableObject()
        object.dateIntervalCalendarModel = dateIntervalCalendar?.toPersistableObject()
        object.timetableModel = timetable?.toPersistableObject()
        object.mapObjectModel = mapObject?.toPersistableObject()
        object.customLocationObjectModel = customLocationObject?.toPersistableObject()
        object.detailsModel = details?.toPersistableObject()
        object.calendarEventModel = calendarEvent?.toPersistableObject()
        object.paymentModel = payment?.toPersistableObject()
        object.statsData = statsData?.toPersistableObject()
        
        return object
    }
    
    mutating func updateFromPersistableObject(object: ChatMessageButtonActionModel) {
        type = ActionType(rawValue: object.typeRawValue) ?? .sendMessage
        phone = object.phone
        if let urlString = object.url { url = URL(string: urlString) }
        callbackData = object.callbackData
        messageContext = object.messageContext

        if let statsDataModel = object.statsData { statsData =  StatsData(with: statsDataModel) }
        if let shareDataModel = object.shareDataModel { shareData = ChatMessageShareControl(with: shareDataModel) }
        if let alertModel = object.alertModel { alert = ChatMessageAlertControl(with: alertModel) }
        if let textInputModel = object.textInputModel { textInput = ChatMessageTextInputControl(with: textInputModel) }
        if let calendarModel = object.calendarModel { calendar = ChatMessageCalendarControl(with: calendarModel) }
        if let dateIntervalCalendarModel = object.dateIntervalCalendarModel { dateIntervalCalendar = ChatMessageDateIntervalControl(with: dateIntervalCalendarModel) }
        if let timetableModel = object.timetableModel { timetable = ChatMessageTimetableControl(with: timetableModel) }
        if let mapObjectModel = object.mapObjectModel { mapObject = ChatMessageMapObject(with: mapObjectModel) }
        if let customLocationObjectModel = object.customLocationObjectModel { customLocationObject = ChatCustomLocationControl(with: customLocationObjectModel) }
        if let detailsModel = object.detailsModel { details = ChatMessageDetailsControl(with: detailsModel) }
        if let calendarEventModel = object.calendarEventModel { calendarEvent = ChatMessageCalendarEventControl(with: calendarEventModel) }
        if let paymentModel = object.paymentModel { payment = ChatMessagePaymentControl(with: paymentModel) }
    }
}

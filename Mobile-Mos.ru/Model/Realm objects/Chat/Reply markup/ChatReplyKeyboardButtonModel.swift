//
//  ChatReplyKeyboardButtonModel.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 24.05.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers
class ChatReplyKeyboardButtonModel: Object {
    
    dynamic var itemId:         String = String()
    dynamic var title:          String = String()
    dynamic var isAlwaysActive: Bool = false
    dynamic var actionModel:    ChatMessageButtonActionModel!
    
    override static func primaryKey() -> String? {
        return #keyPath(itemId)
    }
}

extension ChatReplyKeyboardButton: Persistable {
    
    func toPersistableObject() -> ChatReplyKeyboardButtonModel {
        
        let object = ChatReplyKeyboardButtonModel()
        
        object.itemId = itemId
        object.title = title
        object.isAlwaysActive = isAlwaysActive
        object.actionModel = action.toPersistableObject()
        return object
    }
    
    mutating func updateFromPersistableObject(object: ChatReplyKeyboardButtonModel) {
        itemId = object.itemId
        title = object.title
        isAlwaysActive = object.isAlwaysActive
        action = ChatMessageButtonAction(with: object.actionModel)
    }
}

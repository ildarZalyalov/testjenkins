//
//  ChatMessageReplyMarkupModel.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 10.07.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers
class ChatMessageReplyMarkupModel: Object {
    
    dynamic var inlineMarkupModel:        ChatInlineKeyboardMarkupModel?
    dynamic var inlineOutsideMarkupModel: ChatInlineKeyboardMarkupModel?
    dynamic var commandsMarkupModel:      ChatReplyKeyboardMarkupModel?
    dynamic var showTextKeyboard:         Bool = false
    dynamic var hideTypingIndicator:      Bool = false
}

extension ChatMessageReplyMarkup: Persistable {
    
    func toPersistableObject() -> ChatMessageReplyMarkupModel {
        
        let object = ChatMessageReplyMarkupModel()
        
        object.inlineMarkupModel = inlineMarkup?.toPersistableObject()
        object.inlineOutsideMarkupModel = inlineOutsideMarkup?.toPersistableObject()
        object.commandsMarkupModel = commandsMarkup?.toPersistableObject()
        object.showTextKeyboard = showTextKeyboard
        object.hideTypingIndicator = hideTypingIndicator
        
        return object
    }
    
    mutating func updateFromPersistableObject(object: ChatMessageReplyMarkupModel) {
        if let inlineMarkupModel = object.inlineMarkupModel { inlineMarkup = ChatInlineKeyboardMarkup(with: inlineMarkupModel) }
        if let inlineOutsideMarkupModel = object.inlineOutsideMarkupModel { inlineOutsideMarkup = ChatInlineKeyboardMarkup(with: inlineOutsideMarkupModel) }
        if let commandsMarkupModel = object.commandsMarkupModel { commandsMarkup = ChatReplyKeyboardMarkup(with: commandsMarkupModel) }
        showTextKeyboard = object.showTextKeyboard
        hideTypingIndicator = object.hideTypingIndicator
    }
}

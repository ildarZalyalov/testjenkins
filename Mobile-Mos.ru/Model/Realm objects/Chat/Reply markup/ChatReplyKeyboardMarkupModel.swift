//
//  ChatReplyKeyboardMarkupModel.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 22.05.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers
class ChatReplyKeyboardMarkupModel: Object {

            let buttonRows:        List<ChatReplyKeyboardButtonRowModel> = List()
    dynamic var isOneTimeKeyboard: Bool = false
    dynamic var resizeKeyboard:    Bool = true
    dynamic var hideKeyboard:      Bool = false
}

class ChatReplyKeyboardButtonRowModel: Object {
    
    let buttons: List<ChatReplyKeyboardButtonModel> = List()
    
    convenience init(with buttonModels: [ChatReplyKeyboardButtonModel]) {
        self.init()
        buttons.append(objectsIn: buttonModels)
    }
}

extension ChatReplyKeyboardMarkup: Persistable {
    
    func toPersistableObject() -> ChatReplyKeyboardMarkupModel {
        
        let object = ChatReplyKeyboardMarkupModel()
        
        let buttonRows = buttons.map { ChatReplyKeyboardButtonRowModel(with: $0.toPersistableObjectsArray()) }
        object.buttonRows.append(objectsIn: buttonRows)
        object.resizeKeyboard = resizeKeyboard
        
        return object
    }
    
    mutating func updateFromPersistableObject(object: ChatReplyKeyboardMarkupModel) {
        buttons = object.buttonRows.map { $0.buttons.toPonsoArray() }
        resizeKeyboard = object.resizeKeyboard
    }
}

//
//  ChatInlineKeyboardMarkupModel.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 24.05.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import RealmSwift

class ChatInlineKeyboardMarkupModel: Object {
    
    let buttonRows: List<ChatInlineKeyboardButtonRowModel> = List()
}

class ChatInlineKeyboardButtonRowModel: Object {
    
    let buttons: List<ChatReplyKeyboardButtonModel> = List()
    
    convenience init(with buttonModels: [ChatReplyKeyboardButtonModel]) {
        self.init()
        buttons.append(objectsIn: buttonModels)
    }
}

extension ChatInlineKeyboardMarkup: Persistable {
    
    func toPersistableObject() -> ChatInlineKeyboardMarkupModel {
        
        let object = ChatInlineKeyboardMarkupModel()
        
        let buttonRows = buttons.map { ChatInlineKeyboardButtonRowModel(with: $0.toPersistableObjectsArray()) }
        object.buttonRows.append(objectsIn: buttonRows)
        
        return object
    }
    
    mutating func updateFromPersistableObject(object: ChatInlineKeyboardMarkupModel) {
        buttons = object.buttonRows.map { $0.buttons.toPonsoArray() }
    }
}

//
//  SearchTopWordsModel.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 15.02.2018.
//Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers
class SearchTopWordsModel: Object {
    
    dynamic var title: String = String()
    
    override static func primaryKey() -> String? {
        return #keyPath(title)
    }
}

extension SearchTopWords: Persistable {
    
    func toPersistableObject() -> SearchTopWordsModel {
        
        let object = SearchTopWordsModel()
        object.title = title
        
        return object
    }
    
    mutating func updateFromPersistableObject(object: SearchTopWordsModel) {
        title = object.title
    }
}

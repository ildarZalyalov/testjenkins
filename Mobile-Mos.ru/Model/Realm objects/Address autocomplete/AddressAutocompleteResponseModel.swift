//
//  AddressAutocompleteResponseModel.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 02.04.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers
class AddressAutocompleteResponseModel: Object {
    
    dynamic var address:     String = String()
    dynamic var addressData: String = String()
    
    override static func primaryKey() -> String? {
        return #keyPath(address)
    }
}

extension AddressAutocompleteResponse: Persistable {
    
    func toPersistableObject() -> AddressAutocompleteResponseModel {
        
        let object = AddressAutocompleteResponseModel()
        object.address = address
        object.addressData = addressData
        
        return object
    }
    
    mutating func updateFromPersistableObject(object: AddressAutocompleteResponseModel) {
        address = object.address
        addressData = object.addressData
    }
}


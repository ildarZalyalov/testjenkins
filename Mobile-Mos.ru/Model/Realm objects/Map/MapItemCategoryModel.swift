//
//  MapItemCategoryModel.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 12.05.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers
class MapItemCategoryModel: Object {
    
    dynamic var id:             Int     = 0
    dynamic var name:           String  = " - "
    dynamic var datasetsCount:  Int     = 0
    dynamic var isSaved:        Bool    = false
    let datasets:               List<MapItemDatasetModel> = List()
    
    override static func primaryKey() -> String? {
        return #keyPath(id)
    }

}

extension MapItemCategory: Persistable {
    
    func toPersistableObject() -> MapItemCategoryModel {
        
        let object = MapItemCategoryModel()
        object.id = id
        object.datasets.append(objectsIn: datasets.toPersistableObjectsArray())
        object.name = name
        object.datasetsCount = datasetsCount
        object.isSaved = isSaved
        
        return object
    }
    
    mutating func updateFromPersistableObject(object: MapItemCategoryModel) {
        
        id = object.id
        name = object.name
        datasetsCount = object.datasetsCount
        datasets = object.datasets.toPonsoArray()
        isSaved = object.isSaved
    }
}

//
//  MapItemWorkingHourModel.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 23.05.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers
class MapItemWorkingHourModel: Object {
    
    dynamic var dayOfWeek: String = String()
    dynamic var hours:     String = String()
}

extension MapItemWorkingHour: Persistable {
    
    func toPersistableObject() -> MapItemWorkingHourModel {
        
        let object = MapItemWorkingHourModel()
        object.dayOfWeek = dayOfWeek
        object.hours = hours
        
        return object
    }
    
    mutating func updateFromPersistableObject(object: MapItemWorkingHourModel) {
        dayOfWeek = object.dayOfWeek
        hours = object.hours
    }
}

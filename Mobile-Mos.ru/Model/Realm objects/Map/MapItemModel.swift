//
//  MapItemswift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 27.04.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers
class MapItemModel: Object {
    
    dynamic var globalId:                  Int = 0
    dynamic var name:                      String = String()
    dynamic var itemDescription:           String?
    dynamic var address:                   String?
    dynamic var publicPhone:               String?
    dynamic var website:                   String?
            let workingHours:              List<MapItemWorkingHourModel> = List()
    dynamic var workingHoursClarification: String?
    dynamic var locationLatitute:          Double = 0
    dynamic var locationLongitute:         Double = 0
    dynamic var isFavorited:               Bool = false
    
    override static func primaryKey() -> String? {
        return #keyPath(globalId)
    }
}

extension MapItem: Persistable {

    func toPersistableObject() -> MapItemModel {
        
        let object = MapItemModel()
        
        object.globalId = globalId
        object.name = name
        object.itemDescription = itemDescription
        object.address = address
        object.publicPhone = publicPhone
        object.website = website
        object.workingHours.append(objectsIn: workingHours.toPersistableObjectsArray())
        object.locationLatitute = location.latitude
        object.locationLongitute = location.longitude
        object.isFavorited = isFavorited
        
        return object
    }
    
    mutating func updateFromPersistableObject(object: MapItemModel) {
        
        globalId = object.globalId
        name = object.name
        itemDescription = object.itemDescription
        address = object.address
        publicPhone = object.publicPhone
        website = object.website
        workingHours = object.workingHours.toPonsoArray()
        location = Location(with: [object.locationLongitute, object.locationLatitute])
        isFavorited = object.isFavorited
    }
}

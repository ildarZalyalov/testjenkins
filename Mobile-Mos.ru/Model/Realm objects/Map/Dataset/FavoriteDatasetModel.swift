//
//  FavoriteDatasetModel.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 25.01.2018.
//Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers
class FavoriteDatasetModel: Object {
    
    dynamic var itemId  :Int = 0
    dynamic var name    :String = " - "
    let favoritedItems  :List<MapItemModel> = List()
    
    override static func primaryKey() -> String? {
        return #keyPath(itemId)
    }
}

extension FavoritedDataset: Persistable {
    
    func toPersistableObject() -> FavoriteDatasetModel {
        
        let object = FavoriteDatasetModel()
        
        object.itemId       = itemId
        object.name         = name
        object.favoritedItems.append(objectsIn: favoritedItems.toPersistableObjectsArray())
        
        return object
    }
    
    mutating func updateFromPersistableObject(object: FavoriteDatasetModel) {
        
        itemId      = object.itemId
        name        = object.name
        favoritedItems = object.favoritedItems.toPonsoArray()
    }
}

//
//  MapItemDatasetModel.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 27.04.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers
class MapItemDatasetModel: Object {

    dynamic var itemId:          Int = 0
    dynamic var name:            String = " - "
    dynamic var categoryId:      Int = 0
    dynamic var itemsCount:      Int = 0
    dynamic var saveDate:        Date?
    dynamic var isSaved:         Bool = false
    
    override static func primaryKey() -> String? {
        return #keyPath(itemId)
    }
}

extension Dataset: Persistable {
    
    func toPersistableObject() -> MapItemDatasetModel {
        
        let object = MapItemDatasetModel()
        
        object.itemId       = itemId
        object.name         = name
        object.categoryId   = categoryId
        object.itemsCount   = itemsCount
        object.saveDate     = saveDate
        object.isSaved      = isSaved
        
        return object
    }
    
    mutating func updateFromPersistableObject(object: MapItemDatasetModel) {
    
        itemId      = object.itemId
        name        = object.name
        categoryId  = object.categoryId
        itemsCount  = object.itemsCount
        saveDate    = object.saveDate
        isSaved     = object.isSaved
     }
}

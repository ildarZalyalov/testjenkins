//
//  StatsDataModel.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 02.07.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers
class StatsDataModel: Object {
    
    dynamic var uid:               String = UUID().uuidString.lowercased()
    dynamic var eventJson:         String = String()
    dynamic var eventName:         String = String()
    dynamic var eventFunction:     String = String()
    
    override static func primaryKey() -> String? {
        return #keyPath(uid)
    }
}

extension StatsData: Persistable {
    
    func toPersistableObject() -> StatsDataModel {
        
        let object = StatsDataModel()
        object.eventJson = eventJson
        object.eventName = eventName
        object.eventFunction = eventFunction
        
        return object
    }
    
    mutating func updateFromPersistableObject(object: StatsDataModel) {
        
        eventJson = object.eventJson
        eventName = object.eventName
        eventFunction = object.eventFunction
    }
}

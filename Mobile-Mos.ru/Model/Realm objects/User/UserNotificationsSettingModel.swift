//
//  UserNotificationsSettingModel.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 13.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers
class UserNotificationsSettingModel: Object {
    
    dynamic var chatId:                  String = String()
    dynamic var areNotificationsEnabled: Bool = true
    
    override static func primaryKey() -> String? {
        return #keyPath(chatId)
    }
}

extension UserNotificationsSetting: Persistable {
    
    func toPersistableObject() -> UserNotificationsSettingModel {
        
        let object = UserNotificationsSettingModel()
        
        object.chatId = chatId
        object.areNotificationsEnabled = areNotificationsEnabled
        
        return object
    }
    
    mutating func updateFromPersistableObject(object: UserNotificationsSettingModel) {
        chatId = object.chatId
        areNotificationsEnabled = object.areNotificationsEnabled
    }
}

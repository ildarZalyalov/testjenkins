//
//  UserServiceInfoModel.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 21.02.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers
class UserServiceInfoModel: Object {
    
    dynamic var title:  String = String()
    dynamic var slug:   String = String()
    dynamic var chatId: String?
}

extension UserServiceInfo: Persistable {
    
    func toPersistableObject() -> UserServiceInfoModel {
        
        let object = UserServiceInfoModel()
        
        object.title = title
        object.slug = slug
        object.chatId = chatId
        
        return object
    }
    
    mutating func updateFromPersistableObject(object: UserServiceInfoModel) {
        title = object.title
        slug = object.slug
        chatId = object.chatId
    }
}

//
//  UserModel.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 21.02.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers
class UserModel: Object {
    
    dynamic var sessionId:                   String = String()
    dynamic var userId:                      String?
    dynamic var login:                       String?
    dynamic var password:                    String?
    dynamic var userInfoModel:               UserInfoModel?
            let notificationsSettingsModels: List<UserNotificationsSettingModel> = List()
    
    override static func primaryKey() -> String? {
        return #keyPath(sessionId)
    }
}

extension User: Persistable {
    
    func toPersistableObject() -> UserModel {
        
        let object = UserModel()
        
        object.sessionId = sessionId
        object.userId = userId
        object.login = login
        object.password = password
        object.userInfoModel = userInfo?.toPersistableObject()
        object.notificationsSettingsModels.append(objectsIn: notificationsSettings.toPersistableObjectsArray())
        
        return object
    }
    
    mutating func updateFromPersistableObject(object: UserModel) {
        sessionId = object.sessionId
        userId = object.userId
        login = object.login
        password = object.password
        if let userInfoModel = object.userInfoModel { userInfo = UserInfo(with: userInfoModel) }
        notificationsSettings = object.notificationsSettingsModels.toPonsoArray()
    }
}

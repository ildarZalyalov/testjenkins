//
//  UserInfoModel.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 21.02.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers
class UserInfoModel: Object {
    
    dynamic var email:             String?
    dynamic var firstName:         String?
    dynamic var lastName:          String?
    dynamic var patronymicName:    String?
    dynamic var birthDate:         Date?
    dynamic var phone:             String?
    dynamic var snils:             String?
            let serviceInfoModels: List<UserServiceInfoModel> = List()
}

extension UserInfo: Persistable {
    
    func toPersistableObject() -> UserInfoModel {
        
        let object = UserInfoModel()
        
        object.email = email
        object.firstName = firstName
        object.lastName = lastName
        object.patronymicName = patronymicName
        object.birthDate = birthDate
        object.phone = phone
        object.snils = snils
        object.serviceInfoModels.append(objectsIn: serviceInfo.toPersistableObjectsArray())
        
        return object
    }
    
    mutating func updateFromPersistableObject(object: UserInfoModel) {
        email = object.email
        firstName = object.firstName
        lastName = object.lastName
        patronymicName = object.patronymicName
        birthDate = object.birthDate
        phone = object.phone
        snils = object.snils
        serviceInfo = object.serviceInfoModels.toPonsoArray()
    }
}

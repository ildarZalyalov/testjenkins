//
//  iTunesVersionLookupStateModel.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 08.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers
class iTunesVersionLookupStateModel: Object {
    
    dynamic var version:      String?
    dynamic var releaseNotes: String?
    dynamic var date:         Date?
    
    override static func primaryKey() -> String? {
        return #keyPath(version)
    }
}

extension iTunesVersionLookupState: Persistable {
    
    func toPersistableObject() -> iTunesVersionLookupStateModel {
        
        let object = iTunesVersionLookupStateModel()
        object.version = version
        object.releaseNotes = releaseNotes
        object.date = date
        
        return object
    }
    
    mutating func updateFromPersistableObject(object: iTunesVersionLookupStateModel) {
        version = object.version
        releaseNotes = object.releaseNotes
        date = object.date
    }
}

//
//  ApplicationShortcutItemModel.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 15.12.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Модель для отображения экшенов 3д тача
struct ApplicationShortcutItemModel {
    
    /// название экшена 3д тача
    var name: String
    
    /// уникальный идентификатор экшена (обычно bundle itentifier + название экшена)
    var typeName: String
    
    /// уникальный id для внутреннего различия кнопок или соотвествия другому элементу с таким же id
    var itemId: String
    
    /// иконка экшена
    var iconName: UIApplicationShortcutIcon
}

//
//  UserInfoRequest.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 24.11.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Запрос данных о пользователе
struct UserInfoRequest {
    
    /// Идентификатор сессии
    var sessionId: String
    
    /// Идентификатор пользователя
    var userId: String
}

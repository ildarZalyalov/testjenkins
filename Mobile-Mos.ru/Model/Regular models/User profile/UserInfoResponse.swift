//
//  UserInfoResponse.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 24.11.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Результат запроса данных о пользователе
struct UserInfoResponse {
    
    /// Коды ошибок
    ///
    /// - noError: нет ошибки
    enum ErrorCode: Int {
        case noError = 0
    }
    
    /// Информация о пользователе (если есть)
    var userInfo: UserInfo?
    
    /// Код ошибки (если есть)
    var errorCode: Int?
    
    /// Описание ошибки (если есть)
    var errorMessage: String?
}

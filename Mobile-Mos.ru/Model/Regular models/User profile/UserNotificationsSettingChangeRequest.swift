//
//  UserNotificationsSettingChangeRequest.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 14.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Запрос на изменение настройки уведомлений для чата
struct UserNotificationsSettingChangeRequest {
    
    /// Идентификатор сессии
    var sessionId: String
    
    /// Идентификатор пользователя
    var userId: String
    
    /// Идентификатор чата
    var chatId: String
    
    /// Значение настройки (включены ли уведомления)
    var isEnabled: Bool
}

//
//  EventModel.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 26.06.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

struct Event {
    
    var type: CategoryType
    var functionName: String
    var firstParameterName: String
    var firstParameterValue: String
    var actionValue: String
    
    var multipleValueFirstParameter: [[AnyHashable : Any]]
    
    init(type: CategoryType = .none, functionName: String = "", firstParameterName: String = "", firstParameterValue: String = "", secondParameterName: String = "", actionValue: String = "", multipleValueFirstParameter: [[AnyHashable : Any]] = []) {
        
        self.type = type
        self.functionName = functionName
        self.firstParameterName = firstParameterName
        self.firstParameterValue = firstParameterValue
        self.actionValue = actionValue
        self.multipleValueFirstParameter = multipleValueFirstParameter
    }
}

//
//  FeedbackInfo.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 13.06.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Модель информации обратной связи
struct FeedbackInfo {
    
    /// Заголовок (тема обратной связи)
    var title: String
    
    /// Email почта отправителя
    var email: String
    
    /// Описание проблемы
    var feedBackDescription: String
    
    /// Телефон пользователя
    var userPhone: String
    
    /// Информация устройства
    var deviceInfo: String = "Device model: \(UIDevice.current.model)\nDevice system version: \(UIDevice.current.systemVersion)\nDevice system name: \(UIDevice.current.systemName)\nApp version: \(Bundle.main.releaseVersionNumber ?? "-")"
    
    init(title: String, email: String, feedBackDescription: String, userPhone: String) {
        
        self.title = title
        self.email = email
        self.feedBackDescription = feedBackDescription
        self.userPhone = userPhone
    }
}

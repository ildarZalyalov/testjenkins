//
//  ServicesAndAppsModel.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 24.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Модель данных для сервиса, отображаемого на странице "Наши сервисы и приложения"
struct ServicesAndAppsModel: ServicesAndApps {
    
    /// Иконка
    var icon: UIImage
    
    /// Заголовок
    var title: String
    
    /// Подзаголовок
    var subtitle: String
    
    /// Заголовок описания
    var descriptionHeader: String
    
    /// Описание
    var description: String
    
    /// Ссылка на сервис
    var linkFromApp: String
    
    /// Заголовок кнопки для взаимодействия с сервисом
    var buttonTitle: String
}

//
//  AppSearchModel.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 07.02.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

struct AppSearchModel: ServicesAndApps {
    
    /// Заголовок приложения
    var title: String = ""
    
    /// Описание приложения
    var description: String = ""
    
    /// Ссылка на аватарку приложения
    var imageLinkString: String = ""
    
    /// Ссылка на приложение в эппсторе
    var appStoreLinkString: String = ""
    
    var linkFromApp: String = ""
}

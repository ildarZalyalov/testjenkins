//
//  ServicesAndApps.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 07.02.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Протокол объедяющий Сервисы и Приложения
protocol ServicesAndApps {
    
    /// Ссылка на приложение
    var linkFromApp: String { get set }
    /// Заголовок приложения
    var title: String { get set }
}

//
//  GlobalSearchModel.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 04.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Модель поиска - отражает текущие результаты поиска
/// (при вводе конкретной строки поиска, а также при заходе на экран поиска)
struct GlobalSearchModel {
    
    /// Информация о найденных диалогах
    var conversations = [Conversation]()
    
    /// Информация о найденных новостях
    var news = [NewsSearchModel]()
    
    /// Есть ли данные в модели
    var isEmpty: Bool {
        return conversations.isEmpty && news.isEmpty
    }
}

//
//  SearchSuggest.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 01.02.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Протокол объединяющий топ поисковых слов + саджесты
protocol SearchSuggest {
    
    /// Заголовок саджеста
    var title: String { get set }
}

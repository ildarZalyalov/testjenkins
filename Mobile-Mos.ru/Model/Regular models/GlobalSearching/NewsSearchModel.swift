//
//  NewsSearchModel.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 25.01.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Тип новостей
///
/// - calendar: событие - имеет дату начала/конца
/// - news: новость - имеет только конечную дату
enum NewsType: String {
    
    case calendar = "calendar"
    case news = "news"
    
    var localizedName: String {
        switch self {
        case .news      :return "Новости"
        case .calendar  :return "Афиша"
        }
    }
}

/// Модель поиска новости
struct NewsSearchModel {
    
    /// Заголовок новости
    var title: String = ""
    
    /// Ccылка на картинку новости
    var imageStringUrl: String = ""
    
    /// Дата размещения новости
    var date: Date = Date()
    
    /// Дата начала события
    var dateFrom: Date = Date()
    
    /// Тип новости
    var type: NewsType = .news
    
    /// Ссылка на новость
    var newsStringUrl: String = ""
}

//
//  UserOptionMetadata.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 19.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Метаданные об опции в данных пользователя
/// Опция - типизированная единица данных пользователя, содержит номер документа/счетчика воды/другое
struct UserOptionMetadata {
    
    /// Тип клавиатуры для ввода значения опции
    ///
    /// - normal: обычная текстовая
    /// - digits: цифровая
    /// - digitsAndPunctuation: цифровая с возможностью ввода букв
    /// - date: ввод даты
    /// - autocomplete: автокомплит
    enum Keyboard: String {
        case normal
        case digits
        case digitsAndPunctuation = "digits_and_punctuation"
        case date
        case autocomplete
    }
    
    /// Тип опции (например, ВУ или полис ОМС)
    var type: String = String()
    
    /// Описание опции
    var name: String = String()
    
    /// Регулярное выражение для проверки значения опции
    var regex: String = String()
    
    /// Пример значения опции (если есть)
    var example: String?
    
    /// Клавиатура для ввода значения опции
    var keyboard: Keyboard = .normal
    
    /// Текст с подсказкой по опции
    var helpText: String?
    
    /// Описание опции, выводимое на форме добавления/редактирования опции
    var description: String?
    
    /// Формат для даты при использовании keyboard = .date (если есть, по умолчанию - dd.MM.YYYY)
    var dateFormat: String?
    
    /// Обязательное ли поле
    var mandatory: Bool = false
    
    /// Массив типов опций. Если во всех них нет значений, то поле считается обязательным. Если массив пуст, поле необязательное. Если mandatory = true, то это поле не обрабатывается.
    var mandatoryUnless: [String] = []
    
    /// Можно ли создавать несколько опций данного типа в профиле
    var multiple: Bool = false
    
    /// Должно ли быть значение опции данного типа уникальным по всей системе
    var unique: Bool = false
    
    /// Должно ли быть значение опции данного типа уникальным в профиле
    var uniqueProfile: Bool = false
    
    /// Информация для статистики
    var statsData: ServiceStatsData?
}

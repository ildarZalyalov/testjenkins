//
//  UserOptionGroupMetadata.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 20.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Метаданные о группе опций в данных пользователя
/// Опция - типизированная единица данных пользователя, содержит номер документа/счетчика воды/другое
struct UserOptionGroupMetadata {
    
    /// Тип группы (например, автомобиль или квартира)
    var type: String = String()
    
    /// Название группы
    var name: String = String()
    
    /// Разрешено давать названия (алиасы) группе опций
    var isAliasAllowed: Bool = false
    
    /// Имя поля с названием (если нет, то выводится стандартное)
    var aliasName: String?
    
    /// Текст с подсказкой для поля с названием
    var aliasHelpText: String?
    
    /// Максимальная длина названия группы (если есть)
    var aliasMaxLength: Int?
    
    /// Описание группы, выводимое на форме добавления/редактирования группы
    var description: String?
    
    /// Опции группы
    var options: [UserOptionMetadata] = []
    
    /// Информация статистики
    var statsData: ServiceStatsData?
}

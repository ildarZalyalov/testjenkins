//
//  UserOptionGroupGrouping.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 21.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Информация о группировке групп опций
struct UserOptionGroupGrouping {
    
    /// Название группы групп опций
    var name: String = String()
    
    /// Идентификаторы групп опций в группе
    var groupIds: [String] = []
}

//
//  UserOptionAddRequest.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 28.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Запрос на добавление опции личных данных пользователя
struct UserOptionAddRequest {
    
    /// Идентификатор сессии
    var sessionId: String
    
    /// Идентификатор пользователя
    var userId: String
    
    /// Тип опции
    var optionType: String
    
    /// Значение опции
    var optionValue: String
}

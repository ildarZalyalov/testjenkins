//
//  UserOptionsOperationResponse.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 21.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Результат запроса на изменение/удаление данных (опций) пользователя
struct UserOptionsOperationResponse {
    
    /// Коды ошибок
    ///
    /// - noError: нет ошибки
    enum ErrorCode: Int {
        case noError = 0
    }
    
    /// Код ошибки
    var errorCode: Int = ErrorCode.noError.rawValue
    
    /// Описание ошибки (если есть)
    var errorMessage: String?
}

//
//  UserOptionsObtainResponse.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 20.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Результат запроса данных (опций) пользователя
struct UserOptionsObtainResponse {
    
    /// Коды ошибок
    ///
    /// - noError: нет ошибки
    enum ErrorCode: Int {
        case noError = 0
    }
    
    /// Данные об опциях (если есть)
    var optionsData: UserOptionsData?
    
    /// Код ошибки
    var errorCode: Int = ErrorCode.noError.rawValue
    
    /// Описание ошибки (если есть)
    var errorMessage: String?
}

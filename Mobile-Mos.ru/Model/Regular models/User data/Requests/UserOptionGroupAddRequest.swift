//
//  UserOptionGroupAddRequest.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 28.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Запрос на добавление группы опций личных данных пользователя
struct UserOptionGroupAddRequest {
    
    /// Идентификатор сессии
    var sessionId: String
    
    /// Идентификатор пользователя
    var userId: String
    
    /// Тип группы опций
    var groupType: String
    
    /// Название (алиас) группы (если есть)
    var groupAlias: String?
    
    /// Массив опций в группе
    var options: [UserOption]
}

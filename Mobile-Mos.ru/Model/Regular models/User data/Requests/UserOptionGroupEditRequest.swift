//
//  UserOptionGroupEditRequest.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 29.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Изменение опции пользовательских данных
struct UserOptionChange {
    
    /// Идентификатор опции (если нет, значит добавляем опцию)
    var itemId: String?
    
    /// Тип опции (например, ВУ или полис ОМС)
    var type: String = String()
    
    /// Значение опции (если нет, значит удаляем опцию)
    var value: String?
}

/// Запрос на изменение группы опций личных данных пользователя
struct UserOptionGroupEditRequest {
    
    /// Идентификатор сессии
    var sessionId: String
    
    /// Идентификатор пользователя
    var userId: String
    
    /// Идентификатор группы опций
    var groupId: String
    
    /// Тип группы опций
    var groupType: String
    
    /// Изменение названия (алиаса) группы опций (если есть)
    var groupAlias: UserOptionChange?
    
    /// Изменения опций в группе
    var optionChanges: [UserOptionChange]
}

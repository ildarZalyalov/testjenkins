//
//  UserOptionDeleteRequest.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 21.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Запрос на удаление опции/группы опций данных из личных данных пользователя
struct UserOptionDeleteRequest {
    
    /// Идентификатор сессии
    var sessionId: String
    
    /// Идентификатор пользователя
    var userId: String
    
    /// Идентификатор опции/группы опций
    var itemId: String
    
    /// Тип опции/группы опций
    var itemType: String
}

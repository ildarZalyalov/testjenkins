//
//  UserOptionsObtainRequest.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 20.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Запрос личных данных пользователя
struct UserOptionsObtainRequest {
    
    /// Идентификатор сессии
    var sessionId: String
    
    /// Идентификатор пользователя
    var userId: String
    
    /// Slug группы данных
    var slug: String
}

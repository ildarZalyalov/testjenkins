//
//  UserOptionsData.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 20.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Данные об опциях в данных пользователя
/// Опция в данных пользователя - типизированная единица данных пользователя, содержит номер документа/счетчика воды/другое
struct UserOptionsData {
    
    /// Метаданные групп опций
    var groupsMetadata: [UserOptionGroupMetadata] = []
    
    /// Метаданные опций, находящихся вне групп
    var optionsMetadata: [UserOptionMetadata] = []
    
    /// Группы опций
    var groups: [UserOptionGroup] = []
    
    /// Опции, находящиеся вне групп
    var options: [UserOption] = []
    
    /// Информация о группировке групп опций
    var grouping: [UserOptionGroupGrouping] = []
}

//
//  UserOptionsAutocompleteValue.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 29.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Результат выдачи автокомплита для опций данных пользователя
struct UserOptionsAutocompleteValue {
    
    /// Название значения для вывода
    var name: String = String()
    
    /// Значение, передаваемое на сервер при выборе
    var value: String = String()
}

//
//  UserOptionsAutocompleteResponse.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 29.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Результат запроса автокомплита для опции пользователя
struct UserOptionsAutocompleteResponse {
    
    /// Коды ошибок
    ///
    /// - noError: нет ошибки
    enum ErrorCode: Int {
        case noError = 0
    }
    
    /// Результаты автокомплита
    var values: [UserOptionsAutocompleteValue] = []
    
    /// Код ошибки
    var errorCode: Int = ErrorCode.noError.rawValue
    
    /// Описание ошибки (если есть)
    var errorMessage: String?
}

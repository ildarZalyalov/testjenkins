//
//  UserOption.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 20.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Опция в данных пользователя - типизированная единица данных пользователя, содержит номер документа/счетчика воды/другое
struct UserOption {
    
    /// Идентификатор опции
    var itemId: String = String()
    
    /// Тип опции (например, ВУ или полис ОМС)
    var type: String = String()
    
    /// Значение опции
    var value: String = String()
}

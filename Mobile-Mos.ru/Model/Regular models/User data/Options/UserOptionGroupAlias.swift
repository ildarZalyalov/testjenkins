//
//  UserOptionGroupAlias.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 23.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Специальная опция, в которой хранится пользовательское название (алиас) группы опций
struct UserOptionGroupAlias {
    
    /// Идентификатор опции
    var itemId: String = String()
    
    /// Значение опции
    var value: String = String()
}

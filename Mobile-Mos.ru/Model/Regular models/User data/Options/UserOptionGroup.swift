//
//  UserOptionGroup.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 20.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Группа опций в данных пользователя
/// Опция - типизированная единица данных пользователя, содержит номер документа/счетчика воды/другое
struct UserOptionGroup {
    
    /// Идентификатор группы
    var itemId: String = String()
    
    /// Тип группы (например, автомобиль или квартира)
    var type: String = String()
    
    /// Название группы
    var name: String = String()
    
    /// Описание группы
    var description: String = String()
    
    /// Пользовательское название (алиас) группы (если есть)
    var alias: UserOptionGroupAlias?
    
    /// Опции в группе
    var options: [UserOption] = []
}

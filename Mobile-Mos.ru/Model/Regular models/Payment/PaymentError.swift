//
//  PaymentError.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 16.05.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Ошибка оплаты
struct PaymentError: Error {
    
    /// Тип ошибки
    ///
    /// - unknown: неизвестная ошибка сервера
    /// - invalidParameters: некорректные параметры запроса
    /// - operationForbidden: выполение операции запрещено
    /// - objectNotFound: запрашиваемый объект не найден
    /// - serviceOverload: сервис перегружен, необходимо повторить запрос позже
    /// - unexpectedRedirect: получен запрос на редирект, не ожидаемый в данном вызове
    /// - unexpectedResponse: ответ метода был пустым или некорректного формата
    enum ErrorType {
        case unknown
        case invalidParameters
        case operationForbidden
        case objectNotFound
        case serviceOverload
        case unexpectedRedirect
        case unexpectedResponse
    }
    
    /// Тип ошибки
    var type: ErrorType = .unknown
    
    /// Код ошибки (если есть)
    var code: Int?
    
    /// Описание ошибки
    var description: String = String()
}

extension PaymentError {
    
    init(error: NSError) {
        code = error.code
        description = error.localizedDescription
    }
}

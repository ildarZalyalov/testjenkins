//
//  PaymentBatch.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 16.05.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Пакет платежей
struct PaymentBatch {
    
    /// Статус пакета платежей
    ///
    /// - waitingPaymentTool: ожидаются данные о платежном инструменте
    /// - processing: пакет платежей выполняется
    /// - finish: обработка пакета платежей завершена
    enum Status: String {
        case waitingPaymentTool = "WAITING_PAYMENT_TOOL"
        case processing = "PROCESSING"
        case finish = "FINISH"
    }
    
    /// UID пакета платежей
    var batchId: String = String()
    
    /// Статус пакета платежей
    var status: Status?
    
    /// Платежи в пакете
    var payments: [Payment] = []
}

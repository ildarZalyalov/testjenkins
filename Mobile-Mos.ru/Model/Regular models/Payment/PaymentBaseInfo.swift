//
//  PaymentBaseInfo.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 17.05.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Базовая информация о платеже
struct PaymentBaseInfo {
    
    /// Идентификатор платежа
    var paymentId: String?
    
    /// Признак платежа со страховкой
    var withInsurance: Bool = false
}

extension PaymentBaseInfo {
    
    /// Создать базовую информацию о платеже из полной
    ///
    /// - Parameter payment: полная информация о платеже
    init(payment: Payment) {
        paymentId = payment.paymentId
        withInsurance = payment.isWithInsurance
    }
}

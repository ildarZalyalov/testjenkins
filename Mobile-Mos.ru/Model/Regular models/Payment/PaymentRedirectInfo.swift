//
//  PaymentRedirectInfo.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 18.05.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Информация для совершения редиректа в процессе обработки оплаты
struct PaymentRedirectInfo {
    
    /// Ссылка для перехода
    var redirectUrl: URL?
    
    /// Данные для передачи при переходе
    var paReq: String?
    
    /// Данные для передачи при переходе
    var md: String?
    
    /// Ссылка, на которую будет совершен переход после завершения работы страницы редиректа
    var terminationUrl: URL?
}

extension PaymentRedirectInfo {
    
    /// Валиден ли объект для совершения редиректа
    var isValid: Bool {
        return redirectUrl != nil && paReq != nil && md != nil && terminationUrl != nil
    }
}

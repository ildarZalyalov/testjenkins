//
//  PaymentCardInfo.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 17.05.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Информация о карте
struct PaymentCardInfo {
    
    /// Срок действия карты (пример - 1707)
    var expiryDate: String?
    
    /// Имя держателя карты
    var cardholderName: String?
    
    /// PAN (номер) карты
    var pan: String?
    
    /// CVV/CVC карты
    var cvvc: String?
}

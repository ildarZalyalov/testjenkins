//
//  PaymentCard.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 17.05.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Описание банковской карты
struct PaymentCard {
    
    /// Идентификатор карты
    var cardId: Int64?
    
    /// Тип карты
    var type: PaymentCardType?
    
    /// Имя карты, назначенное абонентом (если имя не назначено передается маскированный пан)
    var title: String?
    
    /// Флаг, определяющий необходимость регистрации карты
    var needRegister: Bool = false
    
    /// Информация о карте
    var cardInfo: PaymentCardInfo?
    
    /// Описание банка, к которому принадлежит карта
    var extendedCardInfo: PaymentCardExtendedInfo?
}

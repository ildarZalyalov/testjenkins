//
//  PaymentCardExtendedInfo.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 17.05.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Тип карты
///
/// - scm: социальная карта москвича (СКМ)
/// - mts: карта МТС Банка
/// - other: другие карты
enum PaymentCardType: String {
    case scm = "CARD_SCM"
    case mts = "CARD_MTS"
    case other = "CARD_OTHER"
}

/// Описание банка к которому принадлежит карта
struct PaymentCardExtendedInfo {
    
    /// Бренд карты
    ///
    /// - visa: платежная система VISA
    /// - mastercard: платежная система MASTECARD
    /// - mir: платежная система MIR
    /// - maestro: платежная система MAESTRO
    /// - other: Остальные бренды
    enum Brand: String {
        case visa = "VISA"
        case mastercard = "MC"
        case mir = "MIR"
        case maestro = "MAESTRO"
        case other = "OTHER"
    }

    /// Идентификатор банка в Национальная Система Платежных Карт (НСПК)
    var bankId: String?
    
    /// Описание банка
    var bankDescription: String?
    
    /// Бренд карты
    var brand: Brand?
    
    /// Тип карты
    var type: PaymentCardType?
    
    /// Цвет заливки банка
    var bankFillingColor: UIColor?
    
    /// Цвет текста банка
    var bankTextColor: UIColor?
    
    /// Ссылка на лого банка
    var bankLogoUrl: URL?
}

extension PaymentCardExtendedInfo {
    
    /// Название банка (совпадает с описанием банка за исключением ряда случаев)
    var bankTitle: String? {
        
        guard var description = bankDescription else { return nil }
        
        if type == .scm {
            description = StringsHelper.paymentToolBankSocialCardPrefix + description
        }
        
        return description
    }
}

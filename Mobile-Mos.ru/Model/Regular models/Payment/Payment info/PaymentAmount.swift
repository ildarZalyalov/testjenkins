//
//  PaymentAmount.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 16.05.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Сумма платежа
struct PaymentAmount {
    
    /// Сумма платежа в минорных единицах (копейках)
    var sum: Int64?
    
    /// Сумма платежа вместе со страховкой в минорных единицах (копейках)
    var sumWithInsurance: Int64?
    
    /// Сумма платежа до льготного периода или скидки в минорных единицах (копейках)
    var sumBeforeDiscount: Int64?
    
    /// Комментарий для суммы платежа
    var comment: String?
}

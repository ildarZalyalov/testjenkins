//
//  PaymentDiscount.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 16.05.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Информация о скидке
struct PaymentDiscount {
    
    /// Процент скидки
    var percent: Int64?
    
    /// Дата, по которую действует скидка
    var date: Date?
}

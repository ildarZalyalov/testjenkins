//
//  PaymentComission.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 16.05.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Данные о комиссии
struct PaymentComission {
    
    /// Сумма комиссии абонента в минорных единицах (копейках) для социальной карты москвича
    var socialCard: Int64?
    
    /// умма комиссии абонента в минорных единицах (копейках) для карты МТС банка
    var mtsCard: Int64?
    
    /// Сумма комиссии абонента в минорных единицах (копейках) для остальных карт
    var bankCard: Int64?
}

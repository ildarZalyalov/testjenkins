//
//  PaymentClientName.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 16.05.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Фамилия, имя и отчество плательщика
struct PaymentClientName {
    
    /// Имя
    var firstName: String?
    
    /// Фамилия
    var lastName: String?
    
    /// Отчество
    var middleName: String?
}

//
//  Payment.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 16.05.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Платеж
struct Payment {
    
    /// Статус платежа
    ///
    /// - waitingPaymentTool: ожидаются данные о платежном инструменте
    /// - waitingProcessing: платеж ожидает выполнения
    /// - processing: платеж выполняется
    /// - success: платеж проведен успешно
    /// - failure: платеж провести не удалось
    enum Status: String {
        case waitingPaymentTool = "WAITING_PAYMENT_TOOL"
        case waitingProcessing = "WAITING_PROCESSING"
        case processing = "PROCESSING"
        case success = "SUCCESS"
        case failure = "FAIL"
    }
    
    /// UID платежа
    var paymentId: String?
    
    /// Статус платежа
    var status: Status?
    
    /// Назначение платежа
    var narrative: String?
    
    /// Признак платежа со страховкой
    var isWithInsurance: Bool = false
    
    /// Имя клиента
    var clientName: PaymentClientName?
    
    /// Сумма платежа
    var amount: PaymentAmount?
    
    /// Информация о скидке
    var discount: PaymentDiscount?
    
    /// Рассчитанные комиссии для основной суммы платежа
    var comission: PaymentComission?
    
    /// Рассчитанные комиссии для суммы платежа со страховкой
    var comissionWithInsurance: PaymentComission?
    
    /// Код описывающий причину неуспешного завершения транзакции
    var failReason: String?
    
    /// Текстовое описание ошибки
    var failReasonDescription: String?
}


extension Payment {
    
    /// Реальная сумма платежа, учитывая страховку
    var amountSum: Int64? {
        return isWithInsurance ? amount?.sumWithInsurance : amount?.sum
    }
    
    /// Получить комиссию для конкретной банковской карты
    ///
    /// - Parameter cardType: тип банковской карты
    /// - Returns: значение комиссии в минорных единицах (копейках)
    func comission(for cardType: PaymentCardType?) -> Int64? {
        
        let comissionValues = isWithInsurance ? comissionWithInsurance : comission
        guard comissionValues != nil else { return nil }
        
        switch cardType {
        case .some(.mts):
            return comissionValues?.mtsCard
        case .some(.scm):
            return comissionValues?.socialCard
        default:
            return comissionValues?.bankCard
        }
    }
 }

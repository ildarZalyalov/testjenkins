//
//  PaymentOperationResult.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 17.05.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Результат выполнения операции с платежным инструментом
struct PaymentOperationResult {
    
    /// Статус выполнения операции
    ///
    /// - unknown: неизвестен
    /// - notFound: платежный инструмент не найден
    /// - success: успех
    /// - failure: провал
    enum Status: String {
        case unknown
        case notFound = "NOT_FOUND"
        case success = "SUCCESS"
        case failure = "FAIL"
    }
    
    /// Статус выполнения операции
    var status: Status = .unknown
    
    /// Новое название платежного инструмента (для операции переименования платежного инструмента)
    var newTitle: String?
}

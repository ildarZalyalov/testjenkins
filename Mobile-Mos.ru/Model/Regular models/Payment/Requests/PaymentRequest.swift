//
//  PaymentRequest.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 17.05.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Запрос на старт платежа
struct PaymentRequest {
    
    /// Тип платежного инструмента
    ///
    /// - card: банковская карта
    /// - account: лицевой счет мобильного оператора
    /// - eWallet: электронные кошельки
    /// - applePay: электронный кошелек apple
    enum ToolType: String {
        case card = "CARD"
        case account = "ACCOUNT"
        case eWallet = "E_WALLET"
        case applePay = "APPLE_PAY"
    }
    
    /// Базовая информация о платежах
    var payments: [PaymentBaseInfo] = []
    
    /// Тип платежного инструмента
    var toolType: ToolType = .card
    
    /// Фамилия, имя и отчество плательщика
    var clientName: PaymentClientName?
    
    /// Адрес электронной почты клиента
    var clientEmail: String = String()
    
    /// Идентификатор в платежной системе
    var portalId: String?
    
    /// Сумма платежа в минорных единицах (копейках) (для авансовых платежей)
    var amount: Int64?
    
    /// Сумма платежа с комиссией в минорных единицах (копейках) (для авансовых платежей)
    var fullAmount: Int64?
    
    /// Описание банковской карты
    var bankCard: PaymentCard?
}

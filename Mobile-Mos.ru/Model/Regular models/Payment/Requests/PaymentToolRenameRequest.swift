//
//  PaymentToolRenameRequest.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 17.05.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Запрос на переименование платежного инструмента
struct PaymentToolRenameRequest {
    
    /// Новое название платежного инструмента
    let newTitle: String
}

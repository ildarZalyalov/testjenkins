//
//  PaymentProvider.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 24.01.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Способ платежа - унифицированная сущность для всех вариантов оплаты
/// (содержит данные для отображения)
struct PaymentProvider {
    
    /// Тип платежа
    ///
    /// - visa: карта Visa
    /// - mastercard: карта MasterCard
    /// - mir: карта МИР
    /// - maestro: карта Maestro
    /// - otherBankCard: карта неизвестной платежной системы
    /// - applePay: платеж через ApplePay
    enum PaymentSystem {
        case visa
        case mastercard
        case mir
        case maestro
        case otherBankCard
        case applePay
    }
    
    /// Банки, для которых в приложении есть лого и цвет
    enum Bank: Int {
        case alfabank = 1020
        case binbank = 1015
        case mts = 1006
        case otkritie = 1024
        case promsvyazbank = 1094
        case raiffeisen = 1087
        case sberbank = 1114
        case tinkoffBank = 1007
        case rosselhozbank = 1098
        case mcb = 1267
        case unicredit = 1083
        case yandexMoney = 1268
        case qiwi = 1385
        case vtb = 1095
        case vtb24 = 1266
        case veb = 1265
        case socialCard = 1
        case uralsib = 1093
        case europe = 1088
        case minb = 1021
        case otherBank = -1
    }
    
    /// Тип платежа
    let type: PaymentSystem
    
    /// Банк
    let bank: Bank
    
    /// Название способа платежа (например, последние цифры номера карты)
    let title: String
    
    /// Название иконки типа платежа
    var typeIconName: String?
    
    /// Название иконки лого банка (если есть)
    var bankIconName: String?
    
    /// Ссылка на лого банка (если есть)
    var bankIconUrl: URL?
    
    /// Цвет брендирования банка (если есть)
    var bankColor: UIColor?
    
    /// Цвет брендирования банка для текста (если есть)
    var bankTextColor: UIColor?
    
    init(type: PaymentSystem, bank: Bank, title: String) {
        self.type = type
        self.bank = bank
        self.title = title
    }
}

extension PaymentProvider: BaseModelWithDefaults {
    
    mutating func assignDefaultValues() {
        
        let iconNamesByType: [PaymentSystem : String] = [
            .visa: "payProviderVisa",
            .mastercard : "payProviderMastercard",
            .mir : "payProviderMir",
            .maestro : "payProviderMaestro",
            .applePay : "payProviderApplePay",
        ]
        
        let iconNamesByBank: [Bank : String] = [
            .alfabank: "bankLogoAlfabank",
            .binbank : "bankLogoBinbank",
            .mts : "bankLogoMts",
            .otkritie : "bankLogoOtkritie",
            .promsvyazbank : "bankLogoPromsvyazbank",
            .raiffeisen: "bankLogoRaiffeisen",
            .sberbank : "bankLogoSberbank",
            .tinkoffBank : "bankLogoTinkoffBank",
            .rosselhozbank : "bankLogoRosselhozbank",
            .mcb : "bankLogoMcb",
            .unicredit : "bankLogoUnicredit",
            .yandexMoney : "bankLogoYandexMoney",
            .qiwi : "bankLogoQiwi",
            .vtb : "bankLogoVtb",
            .vtb24 : "bankLogoVtb",
            .veb : "bankLogoVeb",
            .socialCard : "bankLogoSkm",
            .uralsib : "bankLogoUralsib",
            .europe : "bankLogoEurope",
            .minb: "bankLogoMinb"
        ]
        
        let colorsByBank: [Bank : UIColor] = [
            .alfabank: UIColor.fromHex(hex: 0xEE2A23),
            .binbank : UIColor.fromHex(hex: 0x394A87),
            .mts : UIColor.fromHex(hex: 0xED1C24),
            .otkritie : UIColor.fromHex(hex: 0x00BEE5),
            .promsvyazbank : UIColor.fromHex(hex: 0xF46A40),
            .raiffeisen: UIColor.fromHex(hex: 0xFFF200),
            .sberbank : UIColor.fromHex(hex: 0x299E30),
            .tinkoffBank : UIColor.fromHex(hex: 0xFFE635),
            .rosselhozbank : UIColor.fromHex(hex: 0x008B4D),
            .mcb : UIColor.fromHex(hex: 0xB2002C),
            .unicredit : UIColor.fromHex(hex: 0xFF0000),
            .yandexMoney : UIColor.fromHex(hex: 0xFFCB34),
            .qiwi : UIColor.fromHex(hex: 0xFF8C00),
            .vtb : UIColor.fromHex(hex: 0x002476),
            .vtb24 : UIColor.fromHex(hex: 0x002476),
            .veb : UIColor.fromHex(hex: 0x008bc5),
            .socialCard : UIColor.fromHex(hex: 0x178f8e),
            .uralsib : UIColor.fromHex(hex: 0x1d3a8d),
            .europe : UIColor.fromHex(hex: 0x1c297b),
            .minb : UIColor.fromHex(hex: 0x8f0e12)
            ]
        
        typeIconName = iconNamesByType[type]
        bankIconName = iconNamesByBank[bank]
        bankColor = colorsByBank[bank]
    }
}

extension PaymentProvider {
    
    init(card: PaymentCard) {
        
        switch card.extendedCardInfo?.brand {
        case .some(.visa):
            type = .visa
        case .some(.mastercard):
            type = .mastercard
        case .some(.maestro):
            type = .maestro
        case .some(.mir):
            type = .mir
        default:
            type = .otherBankCard
        }
        
        if card.type == .scm {
            bank = .socialCard
        }
        else if let bankIdString = card.extendedCardInfo?.bankId {
            bank = Bank(rawValueOptional: Int(bankIdString)) ?? .otherBank
        }
        else {
            bank = .otherBank
        }
        
        if card.type == .scm && card.title != nil {
            title = StringsHelper.paymentToolBankSocialCardPrefix + card.title!
        }
        else {
            title = card.title ?? card.extendedCardInfo?.bankTitle ?? StringsHelper.paymentToolTitlePlaceholder
        }
        
        bankColor = card.extendedCardInfo?.bankFillingColor
        
        assignDefaultValues()
    }
}

//
//  UserRegistrationResponse.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 06.12.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

struct UserRegistrationResponse {
    
    /// Идентификатор пользователя (если есть)
    var userId: String?
    
    /// Код ошибки (если есть)
    var errorCode: Int?
    
    /// Описание ошибки (если есть)
    var errorMessage: String?
}

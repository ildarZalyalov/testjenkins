//
//  UserSetNewPasswordResponse.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 02.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// ответ на изменения пароля пользователем
struct UserSetNewPasswordResponse {
    
    /// таймаут для пароля
    var timeout: Int = 0
    
    /// Код ошибки, если нет - 0
    var errorCode: Int = 0
    
    /// текст ошибки, если нет - пустая строка
    var errorMessage: String = ""
}

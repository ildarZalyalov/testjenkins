//
//  UserRegisterRequest.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 05.12.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

struct UserRegisterRequest {
    
    /// Уникальный идентификатор пользователя(не зарегистрированного)
    var sessionId: String
    
    /// телефон пользователя
    var phone: String
    
    /// пароль пользователя
    var password: String
    
    /// код пользователя полученный через смс
    var code: Int
}

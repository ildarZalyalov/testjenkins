//
//  UserNewPasswordRequest.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 02.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

struct UserNewPasswordRequest {
    
    /// Идентификатор сессии
    var sessionId: String
    
    /// код из SMS
    var code: String
    
    /// новый пароль пользователя
    var newPassword: String
}

//
//  UserGetPhoneCode.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 05.12.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

struct UserGetPhoneCode {
    
    /// Уникальный идентификатор пользователя(не зарегистрированного)
    var sessionId: String
    
    /// Номер телефона польльзователя.
    var phone: String
}

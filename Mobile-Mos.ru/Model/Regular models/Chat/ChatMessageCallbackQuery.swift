//
//  ChatMessageCallbackQuery.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 27.06.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Запрос на обратную связь при взаимодействии с сообщением, например нажатии на кнопку инлайн-клавиатуры
struct ChatMessageCallbackQuery {
    
    /// Идентификатор запроса
    var queryId: String = UUID().uuidString.lowercased()
    
    /// Данные запроса
    var queryData: String
    
    /// Текущий пользователь
    var user: User
    
    /// Текущий диалог
    var conversation: Conversation
    
    /// Сообщение, с которым взаимодействует пользователь, если есть
    var message: ChatMessage?
    
    /// Вложенное сообщение, если сообщение является каруселью
    var embeddedMessage: ChatMessage?
    
    init(with queryData: String, user: User, conversation: Conversation, message: ChatMessage?, embeddedMessage: ChatMessage?) {
        self.queryData = queryData
        self.user = user
        self.conversation = conversation
        self.message = message
        self.embeddedMessage = embeddedMessage
    }
}

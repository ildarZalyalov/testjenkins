//
//  ChatMessageCallbackResponse.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 27.06.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Ответ на запрос на обратную связь при взаимодействии с сообщением
struct ChatMessageCallbackResponse {

    /// Идентификатор чата
    var chatId: String = String()
    
    /// Идентификатор запроса
    var queryId: String = String()
    
    /// Текст ответа
    var text: String = String()
}

//
//  ChatPaymentRequest.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 22.05.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Способ оплаты
///
/// - mobilePhone: со счета мобильного телефона
/// - bankCard: с банковской карты
enum ChatPaymentMethod: String {
    case mobilePhone = "MC"
    case bankCard = "CC"
}

/// Запрос на начало оплаты из чата
struct ChatPaymentRequest {
    
    /// Параметры платежа, полученные с сервера
    let params: String
    
    /// Способ оплаты
    let method: ChatPaymentMethod
    
    /// Электронная почта, на которую будет отправлена квитанция
    let email: String
    
    /// Имя для квитанции
    let firstName: String
    
    /// Фамилия для квитанции
    let lastName: String
    
    /// Отчество для квитанции
    let middleName: String
    
    /// Идентификатор пользователя (если пользователь авторизован)
    var userId: String?
    
    // Данные для коллбэк запроса (если есть)
    var callbackData: String?
    
    init(params: String,
         method: ChatPaymentMethod,
         email: String,
         firstName: String,
         lastName: String,
         middleName: String) {
        self.params = params
        self.method = method
        self.email = email
        self.firstName = firstName
        self.lastName = lastName
        self.middleName = middleName
    }
}

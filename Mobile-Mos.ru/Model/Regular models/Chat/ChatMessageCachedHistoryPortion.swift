//
//  ChatMessageCachedHistoryPortion.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 07.12.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Часть закешированной истории сообщений чата
struct ChatMessageCachedHistoryPortion {
    
    /// Есть еще более ранние закешированные сообщения
    let hasMorePrevious: Bool
    
    /// Есть еще более поздние закешированные сообщения
    let hasMoreNext: Bool
    
    /// Список сообщений из истории сообщений
    let messages: [ChatMessage]
}

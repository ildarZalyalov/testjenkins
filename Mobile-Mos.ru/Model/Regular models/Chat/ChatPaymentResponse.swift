//
//  ChatPaymentResponse.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 22.05.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Результат запроса на начало оплаты
struct ChatPaymentResponse {
    
    /// Коды ошибок
    ///
    /// - noError: нет ошибки
    enum ErrorCode: Int {
        case noError = 0
    }
    
    /// Идентификатор платежа (если есть)
    var paymentId: String?
    
    /// Код ошибки (если есть)
    var errorCode: Int?
    
    /// Описание ошибки (если есть)
    var errorMessage: String?
}

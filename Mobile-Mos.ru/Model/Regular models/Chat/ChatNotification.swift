//
//  ChatNotification.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 14.12.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Уведомление для конкретного диалога
struct ChatNotification {
    
    /// Тип уведомления
    ///
    /// - error: ошибка
    /// - warning: важная информация
    enum NotificationType: String {
        case error
        case warning
    }
    
    /// Идентификатор чата
    var chatId: String = String()
    
    /// Тип уведомления
    var type: NotificationType = .warning
    
    /// Текст уведомления
    var text: String = String()
}

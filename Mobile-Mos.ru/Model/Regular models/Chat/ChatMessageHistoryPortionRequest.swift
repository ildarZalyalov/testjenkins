//
//  ChatMessageHistoryPortionRequest.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 22.06.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Запрос на получение части истории сообщений
struct ChatMessageHistoryPortionRequest {
    
    /// Серверный идентификатор сообщения, относительно которого запрашивается история - идет выборка сообщений, которые были получены/отправлены до этого сообщения; если не передан, то идет запрос самых свежих сообщений в истории
    var messageId: Int64?
    
    /// Идентификатор диалога
    var chatId: String
    
    /// Идентификатор пользователя
    var userId: String?
    
    /// Идентификатор сессии
    var sessionId: String
    
    /// Ограничение на кол-во сообщений в ответе
    var limit: Int
}

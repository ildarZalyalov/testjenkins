//
//  ChatMessageTypingEvent.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 13.04.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Запрос от сервера на отображение/скрытие события "собеседник печатает"
struct ChatMessageTypingEvent {
    
    /// Идентификатор чата
    var chatId: String = String()
    
    /// Таймаут, после которого нужно перестать отображать событие
    var timeout: Int?
}

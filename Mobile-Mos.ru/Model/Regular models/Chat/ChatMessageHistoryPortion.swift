//
//  ChatMessageHistoryPortion.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 20.06.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Часть истории сообщений чата
struct ChatMessageHistoryPortion {
    
    /// Идентификатор чата, к которому относится история сообщений
    var chatId: String = String()
    
    /// Список сообщений из истории сообщений
    var messages: [ChatMessage] = []
}

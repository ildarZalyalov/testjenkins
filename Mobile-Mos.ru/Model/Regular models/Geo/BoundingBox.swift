//
//  BoundingBox.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 27.04.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Структура для удобной работы с BoundingBox YMKMapView
struct BoundingBox {
    
    var northEastLatitute:  Double
    var northEastLongitute: Double
    var southWestLatitute:  Double
    var southWestLongitute: Double
    
    init(northEastLatitute: Double, northEastLongitute: Double, southWestLatitute: Double, southWestLongitute: Double) {
        self.northEastLatitute = northEastLatitute
        self.northEastLongitute = northEastLongitute
        self.southWestLatitute = southWestLatitute
        self.southWestLongitute = southWestLongitute
    }
    
    init(with visibleRegion: YMKVisibleRegion) {
        
        northEastLatitute  = visibleRegion.topRight.latitude
        northEastLongitute = visibleRegion.topRight.longitude
        
        southWestLatitute  = visibleRegion.bottomLeft.latitude
        southWestLongitute = visibleRegion.bottomLeft.longitude
    }
    
    func toStringDescribingPoints() -> String {
        return "\(String(northEastLongitute)),\(String(northEastLatitute)),\(String(southWestLongitute)),\(String(southWestLatitute))"
    }
    
    func toArrayDescribingPoints() -> Array<[Double]> {
        return [[northEastLongitute, northEastLatitute],[southWestLongitute, southWestLatitute]]
    }
}

//
//  YandexGeocoderResponse.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 19.10.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Структура, описывающая прямоугольник на карте, полученный из сервиса геокодинга Яндекса
struct YandexGeocoderBoundedBy {
    
    /// Нижняя правая точка
    var lowerCorner: Location = moscowLocation
    
    /// Верхняя левая точка
    var upperCorner: Location = moscowLocation
}

/// Структура, описывающая ответ из сервиса геокодинга Яндекса
struct YandexGeocoderResponse {
    
    /// Текст (обычно содержит адрес)
    var text: String = String()
    
    /// Прямоугольник на карте, описывающий объект
    var boundedBy: YandexGeocoderBoundedBy = YandexGeocoderBoundedBy()
}

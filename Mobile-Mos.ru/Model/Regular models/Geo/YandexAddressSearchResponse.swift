//
//  YandexAddressSearchResponse.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 16.12.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Модель содержащая данные ответа поиска в яндексе
struct YandexAddressSearchResponse {
    
    /// Текстовый адрес локации точки
    var address: String = ""
    
    /// Геолокация точки
    var location: Location = moscowLocation
}

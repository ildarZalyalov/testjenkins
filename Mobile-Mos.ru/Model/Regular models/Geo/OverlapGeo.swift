//
//  Overlap.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 28.02.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Перечисление типов форматов гео объекта из формата Geojson
///
/// - point: простая точка
/// - lineString: линия из массива точек
/// - reactangle: прямоугольник из массива точек
/// - polygon: многоугольник из массива точек
/// - cicle: круг из массива точек
enum OverlapType: String {
    case point = "Point"
    case lineString = "LineString"
    case reactangle = "Reactangle"
    case polygon = "Polygon"
    case cicle = "Circle"
}

/// Модель geoJson для перекрытия
struct OverlapGeo {
    
    /// уникальный id
    var id: String = ""
    
    /// тип перекрытия из geoJson
    var type: OverlapType = .point
    
    /// локация перекрытия
    var geometry: [Location] = []
}

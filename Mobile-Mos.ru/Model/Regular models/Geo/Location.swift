//
//  Location.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 27.04.17.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Структура для удобной работы с локацией MapItem
struct Location: BaseModel {
    
    var latitude: Double
    var longitude: Double
    
    init() {
        latitude = moscowLocation.latitude
        longitude = moscowLocation.longitude
    }
    
    init(latitude: Double, longitude: Double) {
        self.latitude = latitude
        self.longitude = longitude
    }
    
    init(with coordinates: [Double]) {
        latitude = coordinates[1]
        longitude = coordinates[0]
    }
    
    init(with clLocation: CLLocation) {
        latitude = clLocation.coordinate.latitude
        longitude = clLocation.coordinate.longitude
    }
}

extension Location {
    
    init?(with clLocation: CLLocation?) {
        guard let location = clLocation else { return nil }
        latitude = location.coordinate.latitude
        longitude = location.coordinate.longitude
    }
    
    func toYandexPoint() -> YMKPoint {
        return YMKPoint(latitude: latitude, longitude: longitude)
    }
    
    static func +=(lhs: inout Location, double: Double) {
        
        lhs.latitude += double
        lhs.longitude += double
    }
}

extension Location: Hashable, Equatable {
    
    var hashValue: Int {
        return self.latitude.hashValue &+ self.longitude.hashValue
    }
    
    static func ==(lhs: Location, rhs: Location) -> Bool {
        return lhs.latitude == rhs.latitude && lhs.longitude == rhs.longitude
    }
}

//
//  Overlap.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 26.03.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Модель перекрытия с данными
struct Overlap {
    
    /// Id перекрытия
    var id: String = ""
    
    /// Заголовок
    var name: String = ""
    
    /// Адрес
    var address: String = ""
    
    /// Дата начала работ
    var fromDate: Date = Date()
    
    /// Дата конца работ
    var toDate: Date = Date()
    
    /// Дата последнего обновления
    var lastUpdateDate: Date = Date()
    
    /// Телефон для связи
    var phone: String = ""
    
    /// Клиент
    var customer: String = ""
    
    /// Заказчик
    var requester: String = ""
    
    /// Название округа
    var okrugName: String = ""
    
    /// Название причины/тип перекрытия
    var eventType: String = ""
    
    /// Описание
    var description: String = ""
    
    /// Кол-во открытых полос движения
    var lanesAvailable: Int = 0
    
    /// Кол-во закрытых полос движения
    var lanesClosed: Int = 0
    
}


//
//  CTMosRuDeviceType.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 06.07.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Тип устройства в сервисе "Чудо техники"
struct CTMosRuDeviceType {
    
    /// Значение, отсылаемое в сервис
    enum Value: String {
        case phone = "Смартфон"
        case pad = "Планшет"
        case notebook = "Ноутбук"
        case unibodyComputer = "Моноблок"
        case computer = "Настольный компьютер"
        case printer = "Принтер/МФУ"
    }
    
    /// Иконка устройства
    let icon: UIImage
    
    /// Название типа устройства
    let title: String
    
    /// Значение, отсылаемое в сервис
    let value: Value
}

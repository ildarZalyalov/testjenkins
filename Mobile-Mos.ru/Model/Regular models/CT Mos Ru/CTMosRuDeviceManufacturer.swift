//
//  CTMosRuDeviceManufacturer.swift
//  Mobile-Mos.ru
//
//  Created by Иван Ерасов on 06.07.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Производитель устройства в сервисе "Чудо техники"
struct CTMosRuDeviceManufacturer {
    
    /// Название производителя устройства
    let title: String
    
    /// Значение, отсылаемое в сервис
    let value: String
}

//
//  UserLoginResponse.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 10.11.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Результат логина
struct UserLoginResponse {
    
    /// Коды ошибок
    ///
    /// - noError: запрос выполнен без ошибок
    enum ErrorCode: Int {
        case noError = 0
    }
    
    /// Идентификатор пользователя (если есть)
    var userId: String?
    
    /// Код ошибки (если есть)
    var errorCode: Int = 0
    
    /// Описание ошибки (если есть)
    var errorMessage: String = ""
}

//
//  UserLogoutRequest.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 14.11.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Запрос на деавторизацию
struct UserLogoutRequest {
    
    /// Идентификатор сессии
    var sessionId: String
    
    /// Новый идентификатор сессии, который будет использован при успешной деаторизации
    var newSessionId: String
    
    /// Идентификатор пользователя
    var userId: String
}

//
//  UserLoginRequest.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 10.11.2017.
//  Copyright © 2017 mos.ru. All rights reserved.
//

import Foundation

/// Запрос логина
struct UserLoginRequest {
    
    /// Идентификатор сессии
    var sessionId: String
    
    /// Телефон (логин)
    var phone: String
    
    /// Пароль
    var password: String
}

//
//  ServiceStatsData.swift
//  Mobile-Mos.ru
//
//  Created by Ildar Zalyalov on 03.07.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Информация статистики
struct ServiceStatsData {
    
    /// Название события "изменения"
    var editEventName: String = ""
    
    /// Название события "Добавления"
    var addEventName: String = ""
}

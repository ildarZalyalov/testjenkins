//
//  AddressAutocompleteObtainResponse.swift
//  Mobile-Mos.ru
//
//  Created by Ivan Erasov on 02.04.2018.
//  Copyright © 2018 mos.ru. All rights reserved.
//

import Foundation

/// Результат запроса автокомплита адресов
struct AddressAutocompleteObtainResponse {
    
    /// Коды ошибок
    ///
    /// - noError: нет ошибки
    enum ErrorCode: Int {
        case noError = 0
    }
    
    /// Результаты автокомплита
    var addresses: [AddressAutocompleteResponse] = []
    
    /// Код ошибки
    var errorCode: Int = ErrorCode.noError.rawValue
    
    /// Описание ошибки (если есть)
    var errorMessage: String?
}

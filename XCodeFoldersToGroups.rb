#!/usr/bin/ruby

#  XCodeFoldersToGroups.rb
#
#  Created by Ivan Erasov on 21.03.17.
#  Copyright © 2017. All rights reserved.

require 'xcodeproj'

source_root = ENV['SRCROOT']
project_name = ENV['PROJECT_NAME']
project_path = "#{source_root}/#{project_name}.xcodeproj"

$project = Xcodeproj::Project.open(project_path)
$folder_file_type = "folder"

$user_confirmed_updating_project = false
$found_at_least_one_folder = false

def osascript(script)
    parsed_script = script.split(/\n/).map { |line| ["-e", "'" + line.strip + "'"] }.flatten.join(" ")
    return `osascript #{parsed_script}`
end

def is_folder? (file_reference) 
    return file_reference.last_known_file_type == $folder_file_type
end

def show_dialog_folder_found_if_needed()

    unless !$user_confirmed_updating_project
        return
    end

    $user_confirmed_updating_project = true

    dialog_result_prefix = "button returned:"
    dialog_abort = "Abort build"
    dialog_cancel = "Do not Convert"
    dialog_proceed = "Convert"

    dialog_result = osascript <<-EOF
        tell application "XCode"
            set theAlertText to "There are folder links in the project that can be converted into groups. Carry out the conversion?"
            set theAlertMessage to "The project will be changed, and its structure in the Project Navigator will be collapsed"
            set abortTitle to "#{dialog_abort}"
            set cancelTitle to "#{dialog_cancel}"
            set defaultTitle to "#{dialog_proceed}"
            display alert theAlertText message theAlertMessage as critical buttons { abortTitle, cancelTitle, defaultTitle } default button defaultTitle
        end tell
    EOF

    dialog_result.slice! dialog_result_prefix
    dialog_result.strip!

    if dialog_result == dialog_abort
        abort
    end

    if dialog_result == dialog_cancel
        puts "===== Folders to groups conversion cancelled ====="
        exit(0)
    end

end

def add_file_to_group(group, file_path, target)

    file_reference = group.new_reference(file_path)
    file_reference.set_explicit_file_type

    if File.directory?(file_path)
        file_reference.set_last_known_file_type($folder_file_type)    
    end

    unless target != nil
        return
    end

    target.add_file_references([file_reference])
end

def remove_folder_from_group(group, folder_reference)

    $project.native_targets.each do |target|
        target.build_phases.each do |phase|
            phase.remove_file_reference(folder_reference)
        end
    end

    folder_reference.remove_from_project
    
end

def find_folder_target(folder_reference)

    $project.native_targets.each do |target|
        phases = target.build_phases.select { |phase| phase.include?(folder_reference) }
        return target if !phases.empty?
    end

    return nil

end

def process_folder(group, folder_reference, target)

    puts "Found folder #{folder_reference.display_name}"

    show_dialog_folder_found_if_needed()
    $found_at_least_one_folder = true

    folder_group = group.new_group(folder_reference.display_name)
    folder_group.path = folder_reference.path
    folder_contents = Dir.entries(folder_reference.real_path)

    folder_contents.each do |file_path|
        next if file_path.start_with?(".")
        add_file_to_group(folder_group, folder_reference.real_path + file_path, target)
    end

    remove_folder_from_group(group, folder_reference)

    puts "Transformed to group"

end

def process_group(group)
    
    unless !group.empty?
        return
    end

    unless !ARGV.include? group.display_name
        return
    end

    group_path = group.hierarchy_path
    group_path ||= group.display_name
    puts "Processing #{group_path} ..."

    group.files.each do |file_reference|
        if is_folder?(file_reference)
            target = find_folder_target(file_reference)
            process_folder(group, file_reference, target)
        end
    end

    group.groups.each do |subgroup|
        process_group(subgroup)
    end

end

puts "===== Starting folders to groups conversion ====="
process_group($project.main_group)

if $found_at_least_one_folder
    $project.save
end
